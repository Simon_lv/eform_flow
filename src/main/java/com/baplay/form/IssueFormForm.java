package com.baplay.form;

import java.util.Set;

import com.baplay.entity.IssueForm;

public class IssueFormForm extends IssueForm {
	private static final long serialVersionUID = 1L;	
	private Integer pageNo;
	private int pageSize;	
	private boolean export;	
	private String issueTimeStart;
	private String issueTimeEnd;
	private String repairDateStart;
	private String repairDateEnd;
	private Integer gameDataId;
	private Set<String> formRole;
	private Long loginUser;
	private String formType;
		
	public Set<String> getFormRole() {
		return formRole;
	}
	public void setFormRole(Set<String> formRole) {
		this.formRole = formRole;
	}	
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public boolean isExport() {
		return export;
	}
	public void setExport(boolean export) {
		this.export = export;
	}	
	public String getIssueTimeStart() {
		return issueTimeStart;
	}
	public void setIssueTimeStart(String issueTimeStart) {
		this.issueTimeStart = issueTimeStart;
	}
	public String getIssueTimeEnd() {
		return issueTimeEnd;
	}
	public void setIssueTimeEnd(String issueTimeEnd) {
		this.issueTimeEnd = issueTimeEnd;
	}
	public Long getLoginUser() {
		return loginUser;
	}
	public void setLoginUser(Long loginUser) {
		this.loginUser = loginUser;
	}
	public String getFormType() {
		return formType;
	}
	public void setFormType(String formType) {
		this.formType = formType;
	}
	public Integer getGameDataId() {
		return gameDataId;
	}
	public void setGameDataId(Integer gameDataId) {
		this.gameDataId = gameDataId;
	}
	public String getRepairDateStart() {
		return repairDateStart;
	}
	public void setRepairDateStart(String repairDateStart) {
		this.repairDateStart = repairDateStart;
	}
	public String getRepairDateEnd() {
		return repairDateEnd;
	}
	public void setRepairDateEnd(String repairDateEnd) {
		this.repairDateEnd = repairDateEnd;
	}	
}