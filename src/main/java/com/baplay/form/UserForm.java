package com.baplay.form;

import java.util.List;

public class UserForm {
	private Long id;
	private Long roleId1;
	private Long roleId2;
	private Long formRoleId1;
	private Long formRoleId2;
	private Long formRoleId3;
	private Long formRoleId4;
	private Long formRoleId5;
	private Long formRoleId6;
	private String userName;	
	private String loginAccount;
	private Long employeeId;
	private List<Long> employeeMapping;
	private String password;
	private Long creator;
	private boolean duplicateCheck = true;
	private Integer formRoleDeptLimit;
	
	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Long getCreator() {
		return creator;
	}

	public void setCreator(Long creator) {
		this.creator = creator;
	}

	public Long getFormRoleId1() {
		return formRoleId1;
	}

	public void setFormRoleId1(Long formRoleId1) {
		this.formRoleId1 = formRoleId1;
	}

	public Long getFormRoleId2() {
		return formRoleId2;
	}

	public void setFormRoleId2(Long formRoleId2) {
		this.formRoleId2 = formRoleId2;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLoginAccount() {
		return loginAccount;
	}

	public void setLoginAccount(String loginAccount) {
		this.loginAccount = loginAccount;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}	

	public Long getRoleId1() {
		return roleId1;
	}

	public void setRoleId1(Long roleId1) {
		this.roleId1 = roleId1;
	}

	public Long getRoleId2() {
		return roleId2;
	}

	public void setRoleId2(Long roleId2) {
		this.roleId2 = roleId2;
	}

	public List<Long> getEmployeeMapping() {
		return employeeMapping;
	}

	public void setEmployeeMapping(List<Long> employeeMapping) {
		this.employeeMapping = employeeMapping;
	}

	public Long getFormRoleId3() {
		return formRoleId3;
	}

	public void setFormRoleId3(Long formRoleId3) {
		this.formRoleId3 = formRoleId3;
	}

	public Long getFormRoleId4() {
		return formRoleId4;
	}

	public void setFormRoleId4(Long formRoleId4) {
		this.formRoleId4 = formRoleId4;
	}

	public Long getFormRoleId5() {
		return formRoleId5;
	}

	public void setFormRoleId5(Long formRoleId5) {
		this.formRoleId5 = formRoleId5;
	}

	public Long getFormRoleId6() {
		return formRoleId6;
	}

	public void setFormRoleId6(Long formRoleId6) {
		this.formRoleId6 = formRoleId6;
	}

	public boolean isDuplicateCheck() {
		return duplicateCheck;
	}

	public void setDuplicateCheck(boolean duplicateCheck) {
		this.duplicateCheck = duplicateCheck;
	}

	public Integer getFormRoleDeptLimit() {
		return formRoleDeptLimit == null ? 1 : formRoleDeptLimit;
	}

	public void setFormRoleDeptLimit(Integer formRoleDeptLimit) {
		this.formRoleDeptLimit = formRoleDeptLimit;
	}	
}