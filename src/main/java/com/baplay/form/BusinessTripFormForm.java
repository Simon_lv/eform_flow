package com.baplay.form;

import java.util.Set;

import com.baplay.entity.BusinessTripForm;

public class BusinessTripFormForm extends BusinessTripForm {
	private static final long serialVersionUID = 1L;	
	private Integer pageNo;
	private int pageSize;	
	private boolean export;
	private String requestDateStart;
	private String requestDateEnd;	
	private Set<String> formRole;
	private Long loginUser;
	private String formType;
	private Set<String> employeeIdList;
	
	public String getRequestDateStart() {
		return requestDateStart;
	}
	public void setRequestDateStart(String requestDateStart) {
		this.requestDateStart = requestDateStart;
	}
	public String getRequestDateEnd() {
		return requestDateEnd;
	}
	public void setRequestDateEnd(String requestDateEnd) {
		this.requestDateEnd = requestDateEnd;
	}
	public Set<String> getFormRole() {
		return formRole;
	}
	public void setFormRole(Set<String> formRole) {
		this.formRole = formRole;
	}	
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public boolean isExport() {
		return export;
	}
	public void setExport(boolean export) {
		this.export = export;
	}
	public Long getLoginUser() {
		return loginUser;
	}
	public void setLoginUser(Long loginUser) {
		this.loginUser = loginUser;
	}
	public String getFormType() {
		return formType;
	}
	public void setFormType(String formType) {
		this.formType = formType;
	}
	public Set<String> getEmployeeIdList() {
		return employeeIdList;
	}
	public void setEmployeeIdList(Set<String> employeeIdList) {
		this.employeeIdList = employeeIdList;
	}	
}