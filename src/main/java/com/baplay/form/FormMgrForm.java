package com.baplay.form;

public class FormMgrForm {

	private Integer pageNo;
	private int pageSize;	
	private boolean export;
	private Integer status;
	private Long companyId;
	private Long requestDepartment;
	private String requestDateStart;
	private String requestDateEnd;
	private String formType;
	
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public boolean isExport() {
		return export;
	}
	public void setExport(boolean export) {
		this.export = export;
	}	
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getRequestDepartment() {
		return requestDepartment;
	}
	public void setRequestDepartment(Long requestDepartment) {
		this.requestDepartment = requestDepartment;
	}
	public String getRequestDateStart() {
		return requestDateStart;
	}
	public void setRequestDateStart(String requestDateStart) {
		this.requestDateStart = requestDateStart;
	}
	public String getRequestDateEnd() {
		return requestDateEnd;
	}
	public void setRequestDateEnd(String requestDateEnd) {
		this.requestDateEnd = requestDateEnd;
	}
	public String getFormType() {
		return formType;
	}
	public void setFormType(String formType) {
		this.formType = formType;
	}	
}