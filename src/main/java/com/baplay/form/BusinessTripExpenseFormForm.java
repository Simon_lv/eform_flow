package com.baplay.form;

import java.util.Set;

import com.baplay.entity.BusinessTripExpenseForm;

public class BusinessTripExpenseFormForm extends BusinessTripExpenseForm {
	private static final long serialVersionUID = 1L;	
	private Integer pageNo;
	private int pageSize;	
	private boolean export;
	private String requestDateStart;
	private String requestDateEnd;
	private String paymentDateStart;
	private String paymentDateEnd;
	private Set<String> formRole;
	private Long loginUser;
	private String formType;
	
	public String getRequestDateStart() {
		return requestDateStart;
	}
	public void setRequestDateStart(String requestDateStart) {
		this.requestDateStart = requestDateStart;
	}
	public String getRequestDateEnd() {
		return requestDateEnd;
	}
	public void setRequestDateEnd(String requestDateEnd) {
		this.requestDateEnd = requestDateEnd;
	}
	public int getPageNo() {
		return pageNo;
	}
	public Set<String> getFormRole() {
		return formRole;
	}
	public void setFormRole(Set<String> formRole) {
		this.formRole = formRole;
	}	
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public boolean isExport() {
		return export;
	}
	public void setExport(boolean export) {
		this.export = export;
	}
	public Long getLoginUser() {
		return loginUser;
	}
	public void setLoginUser(Long loginUser) {
		this.loginUser = loginUser;
	}
	public String getFormType() {
		return formType;
	}
	public void setFormType(String formType) {
		this.formType = formType;
	}
	public String getPaymentDateStart() {
		return paymentDateStart;
	}
	public void setPaymentDateStart(String paymentDateStart) {
		this.paymentDateStart = paymentDateStart;
	}
	public String getPaymentDateEnd() {
		return paymentDateEnd;
	}
	public void setPaymentDateEnd(String paymentDateEnd) {
		this.paymentDateEnd = paymentDateEnd;
	}
}