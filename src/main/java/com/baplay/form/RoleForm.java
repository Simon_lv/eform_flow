package com.baplay.form;

import java.util.List;

import com.baplay.entity.RoleFunction;

public class RoleForm {
	private String name;
	private Long roleId;
	private List<RoleFunction> roleFunctions;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public List<RoleFunction> getRoleFunctions() {
		return roleFunctions;
	}
	public void setRoleFunctions(List<RoleFunction> roleFunctions) {
		this.roleFunctions = roleFunctions;
	}
}
