package com.baplay.form;

import com.baplay.entity.Receipt;

public class ReceiptForm extends Receipt {
	private static final long serialVersionUID = 1L;	
	private Integer pageNo;
	private int pageSize;	
	private boolean export;
	private Integer status;
	private String requestDateStart;
	private String requestDateEnd;
		
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getRequestDateStart() {
		return requestDateStart;
	}
	public void setRequestDateStart(String requestDateStart) {
		this.requestDateStart = requestDateStart;
	}
	public String getRequestDateEnd() {
		return requestDateEnd;
	}
	public void setRequestDateEnd(String requestDateEnd) {
		this.requestDateEnd = requestDateEnd;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public boolean isExport() {
		return export;
	}
	public void setExport(boolean export) {
		this.export = export;
	}
	
}