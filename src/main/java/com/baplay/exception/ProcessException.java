package com.baplay.exception;


public class ProcessException extends RuntimeException {

	private static final long serialVersionUID = 1490309003072431176L;

	private String errCode;
	private String errMsg;
	private long workFlowDetailId;

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}		

	public long getWorkFlowDetailId() {
		return workFlowDetailId;
	}

	public void setWorkFlowDetailId(long workFlowDetailId) {
		this.workFlowDetailId = workFlowDetailId;
	}

	public ProcessException(String errCode, String errMsg, long workFlowDetailId) {
		this.errCode = errCode;
		this.errMsg = errMsg;
		this.workFlowDetailId = workFlowDetailId;
	}
}