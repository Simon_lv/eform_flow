package com.baplay.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.baplay.dto.BaseDto;
import com.baplay.dto.BaseForm;

public class BusinessTripExpenseForm extends BaseDto implements BaseForm {

	private static final long serialVersionUID = 1L;

	private Long companyId;
	private Integer requestDepartment;
	private Long employeeId;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date requestDate;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date remitDate;
	private Integer remitted;
	private String serialNo;
	private String purpose;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date durationStart;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date durationEnd;
	private String payee;
	private String payeeBank;
	private String payeeAccount;
	private Integer genericExpensesTotal;
	private Integer entertainmentExpensesTotal;
	private Integer businessCardConsumption;
	private Integer amountPrepaid;
	private Integer amountReimbursable;
	private String remark;
	private Long workFlowId;

	private String companyName;
	private String departmentName;
	private String creatorName;
	private Integer creator;
	private Integer status;
	private String employeeName;

	public Long getWorkFlowId() {
		return workFlowId;
	}

	public void setWorkFlowId(Long workFlowId) {
		this.workFlowId = workFlowId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public String getPayee() {
		return payee;
	}

	public void setPayee(String payee) {
		this.payee = payee;
	}

	public String getPayeeBank() {
		return payeeBank;
	}

	public void setPayeeBank(String payeeBank) {
		this.payeeBank = payeeBank;
	}

	public String getPayeeAccount() {
		return payeeAccount;
	}

	public void setPayeeAccount(String payeeAccount) {
		this.payeeAccount = payeeAccount;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Integer getRequestDepartment() {
		return requestDepartment;
	}

	public void setRequestDepartment(Integer requestDepartment) {
		this.requestDepartment = requestDepartment;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public Date getDurationStart() {
		return durationStart;
	}

	public void setDurationStart(Date durationStart) {
		this.durationStart = durationStart;
	}

	public Date getDurationEnd() {
		return durationEnd;
	}

	public void setDurationEnd(Date durationEnd) {
		this.durationEnd = durationEnd;
	}

	public Integer getGenericExpensesTotal() {
		return genericExpensesTotal;
	}

	public void setGenericExpensesTotal(Integer genericExpensesTotal) {
		this.genericExpensesTotal = genericExpensesTotal;
	}

	public Integer getEntertainmentExpensesTotal() {
		return entertainmentExpensesTotal;
	}

	public void setEntertainmentExpensesTotal(Integer entertainmentExpensesTotal) {
		this.entertainmentExpensesTotal = entertainmentExpensesTotal;
	}

	public Integer getBusinessCardConsumption() {
		return businessCardConsumption;
	}

	public void setBusinessCardConsumption(Integer businessCardConsumption) {
		this.businessCardConsumption = businessCardConsumption;
	}

	public Integer getAmountPrepaid() {
		return amountPrepaid;
	}

	public void setAmountPrepaid(Integer amountPrepaid) {
		this.amountPrepaid = amountPrepaid;
	}

	public Integer getAmountReimbursable() {
		return amountReimbursable;
	}

	public void setAmountReimbursable(Integer amountReimbursable) {
		this.amountReimbursable = amountReimbursable;
	}

	public Integer getCreator() {
		return creator;
	}

	public void setCreator(Integer creator) {
		this.creator = creator;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getRemitDate() {
		return remitDate;
	}

	public void setRemitDate(Date remitDate) {
		this.remitDate = remitDate;
	}

	public Integer getRemitted() {
		return remitted;
	}

	public void setRemitted(Integer remitted) {
		this.remitted = remitted;
	}		
}