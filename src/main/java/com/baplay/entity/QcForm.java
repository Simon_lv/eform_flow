package com.baplay.entity;

import com.baplay.dto.BaseDto;
import com.baplay.dto.BaseForm;

public class QcForm extends BaseDto implements BaseForm {

	private static final long serialVersionUID = 1L;
	
	private Long companyId;
	private String serialNo;
	private Long schemaId;
	private String schemaVersion;
	private Long fieldId;
	private Integer qcResult;
	private Integer pmResult;
	private String remark;
	private String subTitle;
	private Long workFlowId;	
	private Long formId;
	private Long titleId;
	
	private String companyName;	
	private String creatorName;
	private Integer creator;
	private String formName;
	private String subTitleList;
	
	private Integer requestDepartment;
	
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	private Integer status;
	
	public Long getCompanyId() {
		return companyId;
	}	
	public Long getFormId() {
		return formId;
	}
	public void setFormId(Long formId) {
		this.formId = formId;
	}	
	public Long getTitleId() {
		return titleId;
	}
	public void setTitleId(Long titleId) {
		this.titleId = titleId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public String getCreatorName() {
		return creatorName;
	}
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public Long getSchemaId() {
		return schemaId;
	}
	public void setSchemaId(Long schemaId) {
		this.schemaId = schemaId;
	}
	public String getSchemaVersion() {
		return schemaVersion;
	}
	public void setSchemaVersion(String schemaVersion) {
		this.schemaVersion = schemaVersion;
	}
	public Long getFieldId() {
		return fieldId;
	}
	public void setFieldId(Long fieldId) {
		this.fieldId = fieldId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getQcResult() {
		return qcResult;
	}
	public void setQcResult(Integer qcResult) {
		this.qcResult = qcResult;
	}
	public Integer getPmResult() {
		return pmResult;
	}
	public void setPmResult(Integer pmResult) {
		this.pmResult = pmResult;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getSubTitle() {
		return subTitle;
	}
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}
	public Long getWorkFlowId() {
		return workFlowId;
	}
	public void setWorkFlowId(Long workFlowId) {
		this.workFlowId = workFlowId;
	}
	public String getSubTitleList() {
		return subTitleList;
	}
	public void setSubTitleList(String subTitleList) {
		this.subTitleList = subTitleList;
	}
	public Integer getCreator() {
		return creator;
	}
	public void setCreator(Integer creator) {
		this.creator = creator;
	}
	public Integer getRequestDepartment() {
		return requestDepartment;
	}
	public void setRequestDepartment(Integer requestDepartment) {
		this.requestDepartment = requestDepartment;
	}	
}