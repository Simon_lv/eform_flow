package com.baplay.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.baplay.dto.BaseDto;

public class ContractReviewMemo extends BaseDto {

	private static final long serialVersionUID = 1L;
	
	private Long contractReviewFormId;
	private Long attachmentId;
	private String memo;
	private Long creator;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	
	private String url;
	private String description;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date attachmentCreateTime;
	
	public Long getContractReviewFormId() {
		return contractReviewFormId;
	}
	public void setContractReviewFormId(Long contractReviewFormId) {
		this.contractReviewFormId = contractReviewFormId;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Long getCreator() {
		return creator;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Date getAttachmentCreateTime() {
		return attachmentCreateTime;
	}
	public void setAttachmentCreateTime(Date attachmentCreateTime) {
		this.attachmentCreateTime = attachmentCreateTime;
	}
	public Long getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(Long attachmentId) {
		this.attachmentId = attachmentId;
	}
	public void setCreator(Long creator) {
		this.creator = creator;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}