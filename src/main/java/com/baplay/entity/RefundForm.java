package com.baplay.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.baplay.dto.BaseDto;
import com.baplay.dto.BaseForm;

public class RefundForm extends BaseDto implements BaseForm {
	
	private static final long serialVersionUID = 1L;
	
	private Long companyId;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date requestDate;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date remitDate;
	private Integer remitted;
	private String serialNo;
	private String subject;
	private String reason;
	private Integer gameDataId;
	private String gameAccount;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date payTime;
	private String orderId;
	private float twCoin;
	private Integer payTypeId;
	private String userName;
	private String userId;
	private String contactTel;
	private String contactAddress;
	private String paymentWay;
	private String remark;
	private Integer notifyAgency;
	private String opsProcess;	
	private String opsProcessMemo;	
	private String finProcess;
	private String finProcessMemo;
	private String receiptNo;
	private String acctProcess;
	private String acctProcessMemo;
	private Long workFlowId;
	
	private String companyName;
	private Integer status;
	private String creatorName;
	private Integer creator;
	private String gameName;
	private String payType;
	
	private Integer requestDepartment;
		
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getOpsProcessMemo() {
		return opsProcessMemo;
	}
	public void setOpsProcessMemo(String opsProcessMemo) {
		this.opsProcessMemo = opsProcessMemo;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Integer getNotifyAgency() {
		return notifyAgency;
	}
	public void setNotifyAgency(Integer notifyAgency) {
		this.notifyAgency = notifyAgency;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public String getAcctProcess() {
		return acctProcess;
	}
	public void setAcctProcess(String acctProcess) {
		this.acctProcess = acctProcess;
	}
	public String getAcctProcessMemo() {
		return acctProcessMemo;
	}
	public void setAcctProcessMemo(String acctProcessMemo) {
		this.acctProcessMemo = acctProcessMemo;
	}
	public String getOpsProcess() {
		return opsProcess;
	}
	public void setOpsProcess(String opsProcess) {
		this.opsProcess = opsProcess;
	}	
	public String getFinProcess() {
		return finProcess;
	}
	public void setFinProcess(String finProcess) {
		this.finProcess = finProcess;
	}
	public String getFinProcessMemo() {
		return finProcessMemo;
	}
	public void setFinProcessMemo(String finProcessMemo) {
		this.finProcessMemo = finProcessMemo;
	}
	public void setTwCoin(Float twCoin) {
		this.twCoin = twCoin;
	}	
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getGameAccount() {
		return gameAccount;
	}
	public void setGameAccount(String gameAccount) {
		this.gameAccount = gameAccount;
	}
	public Date getPayTime() {
		return payTime;
	}
	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public float getTwCoin() {
		return twCoin;
	}
	public void setTwCoin(float twCoin) {
		this.twCoin = twCoin;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getContactTel() {
		return contactTel;
	}
	public void setContactTel(String contactTel) {
		this.contactTel = contactTel;
	}
	public String getContactAddress() {
		return contactAddress;
	}
	public void setContactAddress(String contactAddress) {
		this.contactAddress = contactAddress;
	}
	public String getPaymentWay() {
		return paymentWay;
	}
	public void setPaymentWay(String paymentWay) {
		this.paymentWay = paymentWay;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Long getWorkFlowId() {
		return workFlowId;
	}
	public void setWorkFlowId(Long workFlowId) {
		this.workFlowId = workFlowId;
	}
	public String getCreatorName() {
		return creatorName;
	}
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}
	public Integer getGameDataId() {
		return gameDataId;
	}
	public void setGameDataId(Integer gameDataId) {
		this.gameDataId = gameDataId;
	}
	public Integer getPayTypeId() {
		return payTypeId;
	}
	public void setPayTypeId(Integer payTypeId) {
		this.payTypeId = payTypeId;
	}
	public String getGameName() {
		return gameName;
	}
	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public Integer getCreator() {
		return creator;
	}
	public void setCreator(Integer creator) {
		this.creator = creator;
	}	
	public Integer getRequestDepartment() {
		return requestDepartment;
	}
	public void setRequestDepartment(Integer requestDepartment) {
		this.requestDepartment = requestDepartment;
	}
	public Date getRemitDate() {
		return remitDate;
	}
	public void setRemitDate(Date remitDate) {
		this.remitDate = remitDate;
	}
	public Integer getRemitted() {
		return remitted;
	}
	public void setRemitted(Integer remitted) {
		this.remitted = remitted;
	}	
}