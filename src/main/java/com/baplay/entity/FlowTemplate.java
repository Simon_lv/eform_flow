package com.baplay.entity;

import com.baplay.dto.BaseDto;

public class FlowTemplate extends BaseDto {

	private static final long serialVersionUID = 1L;
	
	private String templateName;
	private Integer flowSchemaId;
	
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public Integer getFlowSchemaId() {
		return flowSchemaId;
	}
	public void setFlowSchemaId(Integer flowSchemaId) {
		this.flowSchemaId = flowSchemaId;
	}
	
}
