package com.baplay.entity;

import java.util.Date;

import com.baplay.dto.BaseDto;

public class WorkFlow extends BaseDto {

	private static final long serialVersionUID = 1L;
	
	private Integer flowSchemaId;
	private Integer status;
	private Long companyId;
	private Integer departmentId;
	private Long creator;
	private Date createTime;
	private Long closer;
	private Date closeTime;
	private Long formId;
	private String serialNo;
	private String formType;
	private Integer stepStatus;
	private Long stepOwner;
	private String formName;
	
	public Integer getFlowSchemaId() {
		return flowSchemaId;
	}
	public void setFlowSchemaId(Integer flowSchemaId) {
		this.flowSchemaId = flowSchemaId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getCreator() {
		return creator;
	}
	public void setCreator(Long creator) {
		this.creator = creator;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Long getCloser() {
		return closer;
	}
	public void setCloser(Long closer) {
		this.closer = closer;
	}
	public Date getCloseTime() {
		return closeTime;
	}
	public void setCloseTime(Date closeTime) {
		this.closeTime = closeTime;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}	
	public String getFormType() {
		return formType;
	}
	public void setFormType(String formType) {
		this.formType = formType;
	}
	public Integer getStepStatus() {
		return stepStatus;
	}
	public void setStepStatus(Integer stepStatus) {
		this.stepStatus = stepStatus;
	}
	public Long getStepOwner() {
		return stepOwner;
	}
	public void setStepOwner(Long stepOwner) {
		this.stepOwner = stepOwner;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public Long getFormId() {
		return formId;
	}
	public void setFormId(Long formId) {
		this.formId = formId;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public Integer getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}	
}