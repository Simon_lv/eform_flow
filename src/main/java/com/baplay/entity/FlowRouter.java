package com.baplay.entity;

import com.baplay.dto.BaseDto;

public class FlowRouter extends BaseDto {

	private static final long serialVersionUID = -797024171711645836L;
	
	private String condition;
	private Integer nextStep;
	
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public Integer getNextStep() {
		return nextStep;
	}
	public void setNextStep(Integer nextStep) {
		this.nextStep = nextStep;
	}	
}