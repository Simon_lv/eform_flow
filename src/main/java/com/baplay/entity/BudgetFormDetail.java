package com.baplay.entity;

import com.baplay.dto.BaseDto;

public class BudgetFormDetail extends BaseDto {
	
	private static final long serialVersionUID = 1L;
	
	private Long budgetFormId;
	private String itemTitle;
	private String itemDescription;
	private Integer amount;
	private Integer additional;
	
	public Long getBudgetFormId() {
		return budgetFormId;
	}
	public void setBudgetFormId(Long budgetFormId) {
		this.budgetFormId = budgetFormId;
	}
	public String getItemTitle() {
		return itemTitle;
	}
	public void setItemTitle(String itemTitle) {
		this.itemTitle = itemTitle;
	}
	
	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Integer getAdditional() {
		return additional;
	}
	public void setAdditional(Integer additional) {
		this.additional = additional;
	}	
	
}
