package com.baplay.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.baplay.dto.BaseDto;
import com.baplay.dto.BaseForm;

public class IssueForm extends BaseDto implements BaseForm {
	
	private static final long serialVersionUID = 1L;
	
	private Long companyId;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date requestDate;
	private Integer requestDepartment;
	private String serialNo;
	private String issueTitle;
	private Integer gameDataId;
	private Integer level; 
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date issueStartTime;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date issueFeedbackTime;
	private Long issueFeedbackOperator;
	private Integer issueFeedbackChannel;
	private String issueFeedbackDescription;	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date repairTime;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date repairFeedbackTime;
	private Long repairFeedbackOperator;
	private Integer repairFeedbackChannel;	
	private String accountablity;
	private String owner;
	private String issueDescription;
	private String repairDescription;
	private String preventionDescription;
	private Integer issueType;
	private Integer issueLevel;
	private Integer preventionLevel;	
	private Long workFlowId;
	
	private String companyName;
	private String departmentName;
	private Integer status;
	private String issueOperatorName;
	private String repairOperatorName;
	private String creatorName;
	private Integer creator;
	private String gameName;
	private String modifierName;
	private String memo;
	private Long memoOperator;
	
	public Integer getRepairFeedbackChannel() {
		return repairFeedbackChannel;
	}
	public void setRepairFeedbackChannel(Integer repairFeedbackChannel) {
		this.repairFeedbackChannel = repairFeedbackChannel;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public String getCreatorName() {
		return creatorName;
	}
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getIssueTitle() {
		return issueTitle;
	}
	public String getIssueOperatorName() {
		return issueOperatorName;
	}
	public void setIssueOperatorName(String issueOperatorName) {
		this.issueOperatorName = issueOperatorName;
	}
	public String getRepairOperatorName() {
		return repairOperatorName;
	}
	public void setRepairOperatorName(String repairOperatorName) {
		this.repairOperatorName = repairOperatorName;
	}
	public void setIssueTitle(String issueTitle) {
		this.issueTitle = issueTitle;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public Long getIssueFeedbackOperator() {
		return issueFeedbackOperator;
	}
	public void setIssueFeedbackOperator(Long issueFeedbackOperator) {
		this.issueFeedbackOperator = issueFeedbackOperator;
	}
	public Long getRepairFeedbackOperator() {
		return repairFeedbackOperator;
	}
	public void setRepairFeedbackOperator(Long repairFeedbackOperator) {
		this.repairFeedbackOperator = repairFeedbackOperator;
	}
	public Integer getRequestDepartment() {
		return requestDepartment;
	}
	public void setRequestDepartment(Integer requestDepartment) {
		this.requestDepartment = requestDepartment;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public Date getIssueStartTime() {
		return issueStartTime;
	}
	public void setIssueStartTime(Date issueStartTime) {
		this.issueStartTime = issueStartTime;
	}
	public Date getIssueFeedbackTime() {
		return issueFeedbackTime;
	}
	public void setIssueFeedbackTime(Date issueFeedbackTime) {
		this.issueFeedbackTime = issueFeedbackTime;
	}	
	public Integer getIssueFeedbackChannel() {
		return issueFeedbackChannel;
	}
	public void setIssueFeedbackChannel(Integer issueFeedbackChannel) {
		this.issueFeedbackChannel = issueFeedbackChannel;
	}
	public String getIssueFeedbackDescription() {
		return issueFeedbackDescription;
	}
	public void setIssueFeedbackDescription(String issueFeedbackDescription) {
		this.issueFeedbackDescription = issueFeedbackDescription;
	}
	public Date getRepairTime() {
		return repairTime;
	}
	public void setRepairTime(Date repairTime) {
		this.repairTime = repairTime;
	}
	public Date getRepairFeedbackTime() {
		return repairFeedbackTime;
	}
	public void setRepairFeedbackTime(Date repairFeedbackTime) {
		this.repairFeedbackTime = repairFeedbackTime;
	}
	public String getIssueDescription() {
		return issueDescription;
	}
	public void setIssueDescription(String issueDescription) {
		this.issueDescription = issueDescription;
	}
	public String getRepairDescription() {
		return repairDescription;
	}
	public void setRepairDescription(String repairDescription) {
		this.repairDescription = repairDescription;
	}
	public String getPreventionDescription() {
		return preventionDescription;
	}
	public void setPreventionDescription(String preventionDescription) {
		this.preventionDescription = preventionDescription;
	}
	public Integer getIssueType() {
		return issueType;
	}
	public void setIssueType(Integer issueType) {
		this.issueType = issueType;
	}
	public Integer getIssueLevel() {
		return issueLevel;
	}
	public void setIssueLevel(Integer issueLevel) {
		this.issueLevel = issueLevel;
	}
	public Integer getPreventionLevel() {
		return preventionLevel;
	}
	public void setPreventionLevel(Integer preventionLevel) {
		this.preventionLevel = preventionLevel;
	}
	public Long getWorkFlowId() {
		return workFlowId;
	}
	public void setWorkFlowId(Long workFlowId) {
		this.workFlowId = workFlowId;
	}
	public Integer getCreator() {
		return creator;
	}
	public void setCreator(Integer creator) {
		this.creator = creator;
	}
	public Integer getGameDataId() {
		return gameDataId;
	}
	public void setGameDataId(Integer gameDataId) {
		this.gameDataId = gameDataId;
	}
	public String getGameName() {
		return gameName;
	}
	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
	public String getModifierName() {
		return modifierName;
	}
	public void setModifierName(String modifierName) {
		this.modifierName = modifierName;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Long getMemoOperator() {
		return memoOperator;
	}
	public void setMemoOperator(Long memoOperator) {
		this.memoOperator = memoOperator;
	}
	public String getAccountablity() {
		return accountablity;
	}
	public void setAccountablity(String accountablity) {
		this.accountablity = accountablity;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	public String getIssueTitle2() {
		int max = 20;
		// 換行
		if(getIssueTitle() == null || getIssueTitle().length() <= max) {
			// Do Nothing
			return getIssueTitle();
		}
		else {
			StringBuilder sb = new StringBuilder();
			int count = 0;
			
			String tmp = getIssueTitle();
			
			for(int i = 0; i < tmp.length(); i++) {
				String x = tmp.substring(i, i + 1);
				
				if(x.equals("\n")) {
					count = 0;
					sb.append("<br>");
				}
				
				if(count == max) {
					count = 0;
					sb.append("<br>");
				}
				
				sb.append(x);
				
				count++;
			}
			
			return sb.toString();
		}
	}
	
	public String getIssueFeedbackDescription2() {
		int max = 50;
		// 換行
		if(getIssueFeedbackDescription() == null || getIssueFeedbackDescription().length() <= max) {
			// Do Nothing
			return getIssueFeedbackDescription();
		}
		else {
			StringBuilder sb = new StringBuilder();
			int count = 0;
			
			// replace all br
			String tmp = getIssueFeedbackDescription().replaceAll("<br>", "\n").replaceAll("<BR>", "\n").replaceAll("<br/>", "\n").replaceAll("<BR/>", "\n");
			
			for(int i = 0; i < tmp.length(); i++) {
				String x = tmp.substring(i, i + 1);
				
				if(x.equals("\n")) {
					count = 0;
					sb.append("<br>");
				}
				
				if(count == max) {
					count = 0;
					sb.append("<br>");
				}
				
				sb.append(x);
				
				count++;
			}
			
			return sb.toString();
		}
	}
	public String getIssueDescription2() {
		int max = 50;
		// 換行
		if(getIssueDescription() == null || getIssueDescription().length() <= max) {
			// Do Nothing
			return getIssueDescription();
		}
		else {
			StringBuilder sb = new StringBuilder();
			int count = 0;
			
			// replace all br
			String tmp = getIssueDescription().replaceAll("<br>", "\n").replaceAll("<BR>", "\n").replaceAll("<br/>", "\n").replaceAll("<BR/>", "\n");
			
			for(int i = 0; i < tmp.length(); i++) {
				String x = tmp.substring(i, i + 1);
				
				if(x.equals("\n")) {
					count = 0;
					sb.append("<br>");
				}
				
				if(count == max) {
					count = 0;
					sb.append("<br>");
				}
				
				sb.append(x);
				
				count++;
			}
			
			return sb.toString();
		}
	}
	
	public String getRepairDescription2() {
		int max = 50;
		// 換行
		if(getRepairDescription() == null || getRepairDescription().length() <= max) {
			// Do Nothing
			return getRepairDescription();
		}
		else {
			StringBuilder sb = new StringBuilder();
			int count = 0;
			
			// replace all br
			String tmp = getRepairDescription().replaceAll("<br>", "\n").replaceAll("<BR>", "\n").replaceAll("<br/>", "\n").replaceAll("<BR/>", "\n");
			
			for(int i = 0; i < tmp.length(); i++) {
				String x = tmp.substring(i, i + 1);
				
				if(x.equals("\n")) {
					count = 0;
					sb.append("<br>");
				}
				
				if(count == max) {
					count = 0;
					sb.append("<br>");
				}
				
				sb.append(x);
				
				count++;
			}
			
			return sb.toString();
		}
	}
	
	public String getPreventionDescription2() {
		int max = 50;
		// 換行
		if(getPreventionDescription() == null || getPreventionDescription().length() <= max) {
			// Do Nothing
			return getPreventionDescription();
		}
		else {
			StringBuilder sb = new StringBuilder();
			int count = 0;
			
			// replace all br
			String tmp = getPreventionDescription().replaceAll("<br>", "\n").replaceAll("<BR>", "\n").replaceAll("<br/>", "\n").replaceAll("<BR/>", "\n");
			
			for(int i = 0; i < tmp.length(); i++) {
				String x = tmp.substring(i, i + 1);
				
				if(x.equals("\n")) {
					count = 0;
					sb.append("<br>");
				}
				
				if(count == max) {
					count = 0;
					sb.append("<br>");
				}
				
				sb.append(x);
				
				count++;
			}
			
			return sb.toString();
		}
	}
}