package com.baplay.entity;

import com.baplay.dto.BaseDto;

public class FormUserRole extends BaseDto {

	private static final long serialVersionUID = -747995650988482889L;
	
	private Long userId;
	private Long roleId;
	private Integer sameDepartment;
	private Integer status;
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}	
	public Integer getSameDepartment() {
		return sameDepartment;
	}
	public void setSameDepartment(Integer sameDepartment) {
		this.sameDepartment = sameDepartment;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}		
}