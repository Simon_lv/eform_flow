package com.baplay.entity;

import com.baplay.dto.BaseDto;

public class FormType extends BaseDto {

	private static final long serialVersionUID = 1L;

	private String name;
	private String code;
	private Long flowSchema;
	private String groupName;
	private String groupCode;
	private String initPage;
	private String processPage;
	private String viewPage;
	private String allFlowSchema;  // 含子公司
	private String fullCode;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public String getProcessPage() {
		return processPage;
	}
	public void setProcessPage(String processPage) {
		this.processPage = processPage;
	}
	public String getInitPage() {
		return initPage;
	}
	public void setInitPage(String initPage) {
		this.initPage = initPage;
	}
	public String getViewPage() {
		return viewPage;
	}
	public void setViewPage(String viewPage) {
		this.viewPage = viewPage;
	}
	public Long getFlowSchema() {
		return flowSchema;
	}
	public void setFlowSchema(Long flowSchema) {
		this.flowSchema = flowSchema;
	}
	public String getAllFlowSchema() {
		return allFlowSchema;
	}
	public void setAllFlowSchema(String allFlowSchema) {
		this.allFlowSchema = allFlowSchema;
	}
	public String getFullCode() {
		return fullCode;
	}
	public void setFullCode(String fullCode) {
		this.fullCode = fullCode;
	}	
}