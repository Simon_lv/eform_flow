package com.baplay.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.baplay.dto.BaseDto;
import com.baplay.dto.BaseForm;

public class BudgetForm extends BaseDto implements BaseForm {
	
	private static final long serialVersionUID = 1L;
	
	private Long companyId;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date requestDate;
	private Integer requestDepartment;
	private String serialNo;
	private String yearMonth;
	private Integer totalAmount;
	private Long additionalReferenceId;
	private String additionalReason;
	private Long workFlowId;
	
	private Integer status;
	private String companyName;
	private String departmentName;
	private Integer additionalAmount;
	private Integer additionalCount;
	private String creatorName;
	private Integer creator;
	
	public Integer getAdditionalAmount() {
		return additionalAmount;
	}
	public void setAdditionalAmount(Integer additionalAmount) {
		this.additionalAmount = additionalAmount;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public String getCreatorName() {
		return creatorName;
	}
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}
	public String getAdditionalReason() {
		return additionalReason;
	}
	public void setAdditionalReason(String additionalReason) {
		this.additionalReason = additionalReason;
	}	
	public Integer getAdditionalCount() {
		return additionalCount;
	}
	public void setAdditionalCount(Integer additionalCount) {
		this.additionalCount = additionalCount;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public Integer getRequestDepartment() {
		return requestDepartment;
	}
	public void setRequestDepartment(Integer requestDepartment) {
		this.requestDepartment = requestDepartment;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getYearMonth() {
		return yearMonth;
	}
	public void setYearMonth(String yearMonth) {
		this.yearMonth = yearMonth;
	}
	public Integer getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}	
	public Long getAdditionalReferenceId() {
		return additionalReferenceId;
	}
	public void setAdditionalReferenceId(Long additionalReferenceId) {
		this.additionalReferenceId = additionalReferenceId;
	}
	public Long getWorkFlowId() {
		return workFlowId;
	}
	public void setWorkFlowId(Long workFlowId) {
		this.workFlowId = workFlowId;
	}
	public Integer getCreator() {
		return creator;
	}
	public void setCreator(Integer creator) {
		this.creator = creator;
	}		
}