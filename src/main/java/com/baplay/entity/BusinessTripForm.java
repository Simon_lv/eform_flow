package com.baplay.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.baplay.dto.BaseDto;
import com.baplay.dto.BaseForm;

public class BusinessTripForm extends BaseDto implements BaseForm {
	
	private static final long serialVersionUID = 1L;
	
	private Long companyId; 
	private Integer requestDepartment;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date requestDate;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private String serialNo;
	private Integer oversea;
	private Integer jobTitleId;
	private Long employeeId;
	private String tripMatter;
	private String tripLocation;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date outboundDepartureTime;	
	private String outboundFlight;
	private String outboundFlightNo;
	private String outboundCountry;
	private String outboundCity;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date outboundLandingTime;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date inboundDepartureTime;
	private String inboundFlight;
	private String inboundFlightNo;	
	private String inboundCountry;
	private String inboundCity;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date inboundLandingTime;
	private Integer agentPassport;
	private Integer agentVisa;
	private Integer agentMtp;;
	private Integer estimatedCostAirfare;
	private Integer estimatedCostAgent;
	private Integer estimatedCostLodging;
	private Integer estimatedCostPickup;
	private Integer cashAdvance;
	private String advanceCurrency;
	private Integer advanceAmount;
	private Integer dailyAllowance;
	private String memo;
	private Long workFlowId;
	
	private String companyName;
	private String departmentName;
	private String creatorName;
	private Integer creator;
	private String jobTitle;
	private Integer status;
	private String employeeName;
		
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public void setOutboundDepartureTime(Date outboundDepartureTime) {
		this.outboundDepartureTime = outboundDepartureTime;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getCreatorName() {
		return creatorName;
	}
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public Integer getRequestDepartment() {
		return requestDepartment;
	}
	public void setRequestDepartment(Integer requestDepartment) {
		this.requestDepartment = requestDepartment;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public Integer getOversea() {
		return oversea;
	}
	public void setOversea(Integer oversea) {
		this.oversea = oversea;
	}
	public Integer getJobTitleId() {
		return jobTitleId;
	}
	public void setJobTitleId(Integer jobTitleId) {
		this.jobTitleId = jobTitleId;
	}
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	public String getTripMatter() {
		return tripMatter;
	}
	public void setTripMatter(String tripMatter) {
		this.tripMatter = tripMatter;
	}
	public String getTripLocation() {
		return tripLocation;
	}
	public void setTripLocation(String tripLocation) {
		this.tripLocation = tripLocation;
	}
	public Date getOutboundDepartureTime() {
		return outboundDepartureTime;
	}
	public void setOutBoundDepartureTime(Date outboundDepartureTime) {
		this.outboundDepartureTime = outboundDepartureTime;
	}
	public String getOutboundFlight() {
		return outboundFlight;
	}
	public void setOutboundFlight(String outboundFlight) {
		this.outboundFlight = outboundFlight;
	}
	public String getOutboundFlightNo() {
		return outboundFlightNo;
	}
	public void setOutboundFlightNo(String outboundFlightNo) {
		this.outboundFlightNo = outboundFlightNo;
	}
	public String getOutboundCountry() {
		return outboundCountry;
	}
	public void setOutboundCountry(String outboundCountry) {
		this.outboundCountry = outboundCountry;
	}
	public String getOutboundCity() {
		return outboundCity;
	}
	public void setOutboundCity(String outboundCity) {
		this.outboundCity = outboundCity;
	}
	public Date getOutboundLandingTime() {
		return outboundLandingTime;
	}
	public void setOutboundLandingTime(Date outboundLandingTime) {
		this.outboundLandingTime = outboundLandingTime;
	}
	public Date getInboundDepartureTime() {
		return inboundDepartureTime;
	}
	public void setInboundDepartureTime(Date inboundDepartureTime) {
		this.inboundDepartureTime = inboundDepartureTime;
	}
	public String getInboundFlight() {
		return inboundFlight;
	}
	public void setInboundFlight(String inboundFlight) {
		this.inboundFlight = inboundFlight;
	}
	public String getInboundFlightNo() {
		return inboundFlightNo;
	}
	public void setInboundFlightNo(String inboundFlightNo) {
		this.inboundFlightNo = inboundFlightNo;
	}
	public String getInboundCountry() {
		return inboundCountry;
	}
	public void setInboundCountry(String inboundCountry) {
		this.inboundCountry = inboundCountry;
	}
	public String getInboundCity() {
		return inboundCity;
	}
	public void setInboundCity(String inboundCity) {
		this.inboundCity = inboundCity;
	}
	public Date getInboundLandingTime() {
		return inboundLandingTime;
	}
	public void setInboundLandingTime(Date inboundLandingTime) {
		this.inboundLandingTime = inboundLandingTime;
	}	
	public Integer getAgentPassport() {
		return agentPassport;
	}
	public void setAgentPassport(Integer agentPassport) {
		this.agentPassport = agentPassport;
	}
	public Integer getAgentVisa() {
		return agentVisa;
	}
	public void setAgentVisa(Integer agentVisa) {
		this.agentVisa = agentVisa;
	}
	public Integer getAgentMtp() {
		return agentMtp;
	}
	public void setAgentMtp(Integer agentMtp) {
		this.agentMtp = agentMtp;
	}
	public Integer getEstimatedCostAirfare() {
		return estimatedCostAirfare;
	}
	public void setEstimatedCostAirfare(Integer estimatedCostAirfare) {
		this.estimatedCostAirfare = estimatedCostAirfare;
	}
	public Integer getEstimatedCostAgent() {
		return estimatedCostAgent;
	}
	public void setEstimatedCostAgent(Integer estimatedCostAgent) {
		this.estimatedCostAgent = estimatedCostAgent;
	}
	public Integer getEstimatedCostLodging() {
		return estimatedCostLodging;
	}
	public void setEstimatedCostLodging(Integer estimatedCostLodging) {
		this.estimatedCostLodging = estimatedCostLodging;
	}
	public Integer getEstimatedCostPickup() {
		return estimatedCostPickup;
	}
	public void setEstimatedCostPickup(Integer estimatedCostPickup) {
		this.estimatedCostPickup = estimatedCostPickup;
	}
	public Integer getCashAdvance() {
		return cashAdvance;
	}
	public void setCashAdvance(Integer cashAdvance) {
		this.cashAdvance = cashAdvance;
	}
	public String getAdvanceCurrency() {
		return advanceCurrency;
	}
	public void setAdvanceCurrency(String advanceCurrency) {
		this.advanceCurrency = advanceCurrency;
	}
	public Integer getAdvanceAmount() {
		return advanceAmount;
	}
	public void setAdvanceAmount(Integer advanceAmount) {
		this.advanceAmount = advanceAmount;
	}
	public Integer getDailyAllowance() {
		return dailyAllowance;
	}
	public void setDailyAllowance(Integer dailyAllowance) {
		this.dailyAllowance = dailyAllowance;
	}
	public Long getWorkFlowId() {
		return workFlowId;
	}
	public void setWorkFlowId(Long workFlowId) {
		this.workFlowId = workFlowId;
	}
	public Integer getCreator() {
		return creator;
	}
	public void setCreator(Integer creator) {
		this.creator = creator;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}	
}