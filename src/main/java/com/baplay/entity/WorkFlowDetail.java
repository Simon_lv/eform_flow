package com.baplay.entity;

import java.util.Date;

import com.baplay.dto.BaseDto;

public class WorkFlowDetail extends BaseDto {

	private static final long serialVersionUID = 1L;
	
	private Long workFlowId;
	private Long stepId;
	private Long creator;
	private Date createTime;
	private Integer status;
	private Long ownerId;
	private Date processTime;
	private String memo;
	private Integer stepType;
	private Integer iteration;
	private String countersign;
	private Integer sameDepartment;
	private Integer departmentId;
	private Integer countersignRole;
	
	public Long getWorkFlowId() {
		return workFlowId;
	}
	public void setWorkFlowId(Long workFlowId) {
		this.workFlowId = workFlowId;
	}
	public Long getStepId() {
		return stepId;
	}
	public void setStepId(Long stepId) {
		this.stepId = stepId;
	}
	public Long getCreator() {
		return creator;
	}
	public void setCreator(Long creator) {
		this.creator = creator;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	public Date getProcessTime() {
		return processTime;
	}
	public void setProcessTime(Date processTime) {
		this.processTime = processTime;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Integer getStepType() {
		return stepType;
	}
	public void setStepType(Integer stepType) {
		this.stepType = stepType;
	}
	public Integer getIteration() {
		return iteration;
	}
	public void setIteration(Integer iteration) {
		this.iteration = iteration;
	}
	public String getCountersign() {
		return countersign;
	}
	public void setCountersign(String countersign) {
		this.countersign = countersign;
	}
	public Integer getSameDepartment() {
		return sameDepartment;
	}
	public void setSameDepartment(Integer sameDepartment) {
		this.sameDepartment = sameDepartment;
	}
	public Integer getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}
	public Integer getCountersignRole() {
		return countersignRole;
	}
	public void setCountersignRole(Integer countersignRole) {
		this.countersignRole = countersignRole;
	}	
}