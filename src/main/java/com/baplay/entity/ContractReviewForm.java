package com.baplay.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.baplay.dto.BaseDto;
import com.baplay.dto.BaseForm;

public class ContractReviewForm extends BaseDto implements BaseForm {

	private static final long serialVersionUID = 1L;
	
	private Long companyId;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date requestDate;
	private Integer requestDepartment;
	private String serialNo;
	private String contractTitle;
	private String contractTarget;
	private Long organizer;
	private String organizerExtension;
	private Integer urgentDispatch;
	private String urgentDispatchReason;
	private String signWay;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date definiteTerm;
	private String keyPoint;
	private Long workFlowId;
	
	private String companyName;
	private String departmentName;
	private Integer status;
	private String organizerName;
	private String creatorName;
	private Integer creator;
	
	public String getCreatorName() {
		return creatorName;
	}
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}
	public String getOrganizerName() {
		return organizerName;
	}
	public void setOrganizerName(String organizerName) {
		this.organizerName = organizerName;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public Integer getRequestDepartment() {
		return requestDepartment;
	}
	public void setRequestDepartment(Integer requestDepartment) {
		this.requestDepartment = requestDepartment;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getContractTitle() {
		return contractTitle;
	}
	public void setContractTitle(String contractTitle) {
		this.contractTitle = contractTitle;
	}
	public String getContractTarget() {
		return contractTarget;
	}
	public void setContractTarget(String contractTarget) {
		this.contractTarget = contractTarget;
	}
	public Long getOrganizer() {
		return organizer;
	}
	public void setOrganizer(Long organizer) {
		this.organizer = organizer;
	}
	public String getOrganizerExtension() {
		return organizerExtension;
	}
	public void setOrganizerExtension(String organizerExtension) {
		this.organizerExtension = organizerExtension;
	}
	public Integer getUrgentDispatch() {
		return urgentDispatch;
	}
	public void setUrgentDispatch(Integer urgentDispatch) {
		this.urgentDispatch = urgentDispatch;
	}
	public String getUrgentDispatchReason() {
		return urgentDispatchReason;
	}
	public void setUrgentDispatchReason(String urgentDispatchReason) {
		this.urgentDispatchReason = urgentDispatchReason;
	}	
	public String getSignWay() {
		return signWay;
	}
	public void setSignWay(String signWay) {
		this.signWay = signWay;
	}
	public Date getDefiniteTerm() {
		return definiteTerm;
	}
	public void setDefiniteTerm(Date definiteTerm) {
		this.definiteTerm = definiteTerm;
	}
	public String getKeyPoint() {
		return keyPoint;
	}
	public void setKeyPoint(String keyPoint) {
		this.keyPoint = keyPoint;
	}
	public Long getWorkFlowId() {
		return workFlowId;
	}
	public void setWorkFlowId(Long workFlowId) {
		this.workFlowId = workFlowId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getCreator() {
		return creator;
	}
	public void setCreator(Integer creator) {
		this.creator = creator;
	}	
}