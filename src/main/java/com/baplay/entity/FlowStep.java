package com.baplay.entity;

import com.baplay.dto.BaseDto;

public class FlowStep extends BaseDto {

	private static final long serialVersionUID = 1L;
	
	private Integer flowSchemaId;
	private Integer stepType;
	private Integer stepOrder;
	private Integer roleId;
	private Integer iteration;
	private String countersign;
	private Integer sameDepartment;
	private String routerId;
	private Integer notifyFlag;
	
	private String role;
	private String countersignRole;
	private String router;
	
	private String roleName;
	
	public Integer getFlowSchemaId() {
		return flowSchemaId;
	}
	public void setFlowSchemaId(Integer flowSchemaId) {
		this.flowSchemaId = flowSchemaId;
	}
	public Integer getStepType() {
		return stepType;
	}
	public void setStepType(Integer stepType) {
		this.stepType = stepType;
	}
	public Integer getStepOrder() {
		return stepOrder;
	}
	public void setStepOrder(Integer stepOrder) {
		this.stepOrder = stepOrder;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public Integer getIteration() {
		return iteration;
	}
	public void setIteration(Integer iteration) {
		this.iteration = iteration;
	}
	public String getCountersign() {
		return countersign;
	}
	public void setCountersign(String countersign) {
		this.countersign = countersign;
	}
	public Integer getSameDepartment() {
		return sameDepartment;
	}
	public void setSameDepartment(Integer sameDepartment) {
		this.sameDepartment = sameDepartment;
	}
	public String getRouterId() {
		return routerId;
	}
	public void setRouterId(String routerId) {
		this.routerId = routerId;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getCountersignRole() {
		return countersignRole;
	}
	public void setCountersignRole(String countersignRole) {
		this.countersignRole = countersignRole;
	}
	public String getRouter() {
		return router;
	}
	public void setRouter(String router) {
		this.router = router;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Integer getNotifyFlag() {
		return notifyFlag;
	}
	public void setNotifyFlag(Integer notifyFlag) {
		this.notifyFlag = notifyFlag;
	}	
}