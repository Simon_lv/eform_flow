package com.baplay.entity;

import com.baplay.dto.BaseDto;

public class UserEmployee extends BaseDto {

	private static final long serialVersionUID = -8870060496847298119L;
	
	private Long userId;
	private Long employeeId;
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}		
}