package com.baplay.entity;

import com.baplay.dto.BaseDto;

public class QcFormDetailField extends BaseDto {

	private static final long serialVersionUID = 1L;
	
	private Long schemaId;
	private String schemaVersion;
	private Integer fieldOrder;
	private String groupLabel;
	private String fieldLabel;
	
	private Integer qcResult;
	private Integer pmResult;
	private String remark;
	
	public Long getSchemaId() {
		return schemaId;
	}
	public void setSchemaId(Long schemaId) {
		this.schemaId = schemaId;
	}
	public String getSchemaVersion() {
		return schemaVersion;
	}
	public void setSchemaVersion(String schemaVersion) {
		this.schemaVersion = schemaVersion;
	}
	public Integer getFieldOrder() {
		return fieldOrder;
	}
	public Integer getQcResult() {
		return qcResult;
	}
	public void setQcResult(Integer qcResult) {
		this.qcResult = qcResult;
	}
	public Integer getPmResult() {
		return pmResult;
	}
	public void setPmResult(Integer pmResult) {
		this.pmResult = pmResult;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public void setFieldOrder(Integer fieldOrder) {
		this.fieldOrder = fieldOrder;
	}
	public String getGroupLabel() {
		return groupLabel;
	}
	public void setGroupLabel(String groupLabel) {
		this.groupLabel = groupLabel;
	}
	public String getFieldLabel() {
		return fieldLabel;
	}
	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}
		
}
