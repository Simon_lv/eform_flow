package com.baplay.entity;

import com.baplay.dto.BaseDto;

public class SequenceData extends BaseDto {
	
	private static final long serialVersionUID = 1L;
	
	private String name;
	private Long currentValue;
	private Integer increment;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getCurrentValue() {
		return currentValue;
	}
	public void setCurrentValue(Long currentValue) {
		this.currentValue = currentValue;
	}
	public Integer getIncrement() {
		return increment;
	}
	public void setIncrement(Integer increment) {
		this.increment = increment;
	}
	
}
