/**
 * 
 */
package com.baplay.entity;

import com.baplay.dto.BaseDto;

/**
 * @author Haozi
 *
 */
public class UserRole extends BaseDto {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4042164623475560859L;
	private Long userId;
	private Long roleId;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
}
