package com.baplay.entity;

import com.baplay.dto.BaseDto;

public class QcFormTitleField extends BaseDto {

	private static final long serialVersionUID = 1L;
	
	private Long schemaId;
	private String schemaVersion;
	private Integer fieldOrder;
	private String fieldLabel;
	
	private String subTitle;
	
	public Long getSchemaId() {
		return schemaId;
	}
	public void setSchemaId(Long schemaId) {
		this.schemaId = schemaId;
	}
	public String getSchemaVersion() {
		return schemaVersion;
	}
	public void setSchemaVersion(String schemaVersion) {
		this.schemaVersion = schemaVersion;
	}	
	
	public String getSubTitle() {
		return subTitle;
	}
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}
	public Integer getFieldOrder() {
		return fieldOrder;
	}
	public void setFieldOrder(Integer fieldOrder) {
		this.fieldOrder = fieldOrder;
	}
	public String getFieldLabel() {
		return fieldLabel;
	}
	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}
	
}
