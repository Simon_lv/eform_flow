package com.baplay.entity;

import java.io.Serializable;
import java.util.Date;

import com.baplay.dto.BaseDto;

/**
 * TEfunuser entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TEfunuser extends BaseDto implements Serializable {

	private static final long serialVersionUID = 1L;
	// Fields
	private Long id;	
	private Integer status;	
	private String loginAccount;
	private Long employeeId;	
	private String userName;
	private Long creator;
	private String password;
	private String remark;
	private String icon;
	private String name;
	
	private Integer supervisor;
		
	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	private Date createTime;
	// Constructors

	/** default constructor */
	public TEfunuser() {
	}

	/** full constructor */
	public TEfunuser(String loginAccount, String userName, Integer status, Long creator, Date createTime, 
			String password, String remark, String icon) {
		this.loginAccount = loginAccount;	
		this.userName = userName;
		this.status = status;
		this.creator = creator;
		this.createTime = createTime;
		this.password = password;
		this.remark = remark;		
		this.icon = icon;
	}

	// Property accessors
	
	public Integer getStatus() {
		return status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}	

	public Long getCreator() {
		return creator;
	}

	public void setCreator(Long creator) {
		this.creator = creator;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getLoginAccount() {
		return loginAccount;
	}

	public void setLoginAccount(String loginAccount) {
		this.loginAccount = loginAccount;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Integer supervisor) {
		this.supervisor = supervisor;
	}		
}