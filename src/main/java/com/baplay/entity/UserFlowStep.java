package com.baplay.entity;

import com.baplay.dto.BaseDto;

public class UserFlowStep extends BaseDto {

	private static final long serialVersionUID = -9098792146508290830L;
	
	private Long workFlowId;
	private Long stepId;
	private String userId;
	
	public Long getStepId() {
		return stepId;
	}
	public void setStepId(Long stepId) {
		this.stepId = stepId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Long getWorkFlowId() {
		return workFlowId;
	}
	public void setWorkFlowId(Long workFlowId) {
		this.workFlowId = workFlowId;
	}	
}