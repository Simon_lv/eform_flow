package com.baplay.entity;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.format.annotation.DateTimeFormat;

import com.baplay.dto.BaseDto;

public class Company extends BaseDto {
	
	private static final long serialVersionUID = 1L;
	
	private String name;
	private String taxNumber;
	private String address;
	private String phone;
	private String owner;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date establishDate;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")	
	private Date updateTime;	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	
	private Long modifier;
	private Long creator;
	
	private Integer status;
	
	private String creatorName;
	private String modifierName;
	private Map<String, String> jobMap;
	private Map<String, String> dptMap;
	private List<Department> department;	
	
	public String getModifierName() {
		return modifierName;
	}
	public void setModifierName(String modifierName) {
		this.modifierName = modifierName;
	}
	public String getCreatorName() {
		return creatorName;
	}
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}	
	public Long getModifier() {
		return modifier;
	}
	public void setModifier(Long modifier) {
		this.modifier = modifier;
	}
	public Long getCreator() {
		return creator;
	}
	public void setCreator(Long creator) {
		this.creator = creator;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getTaxNumber() {
		return taxNumber;
	}
	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public Date getEstablishDate() {
		return establishDate;
	}
	public void setEstablishDate(Date establishDate) {
		this.establishDate = establishDate;
	}
	public Map<String, String> getJobMap() {
		return jobMap;
	}
	public void setJobMap(Map<String, String> jobMap) {
		this.jobMap = jobMap;
	}
	public Map<String, String> getDptMap() {
		return dptMap;
	}
	public void setDptMap(Map<String, String> dptMap) {
		this.dptMap = dptMap;
	}
	public List<Department> getDepartment() {
		return department;
	}
	public void setDepartment(List<Department> department) {
		this.department = department;
	}	
}