package com.baplay.entity;

import com.baplay.dto.BaseDto;

public class Role extends BaseDto {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3831231286567616850L;
	private String name;
	private Integer status;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
