package com.baplay.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springside.modules.orm.Page;

import com.baplay.constant.Config;
import com.baplay.constant.Flow;
import com.baplay.constant.Form;
import com.baplay.constant.URL;
import com.baplay.dao.mapper.FormUser;
import com.baplay.dto.Stamp;
import com.baplay.entity.Attachment;
import com.baplay.entity.FlowStep;
import com.baplay.entity.IssueForm;
import com.baplay.entity.IssueMemo;
import com.baplay.entity.TEfunuser;
import com.baplay.entity.UserFlowStep;
import com.baplay.entity.WorkFlow;
import com.baplay.entity.WorkFlowDetail;
import com.baplay.exception.IllegalException;
import com.baplay.form.IssueFormForm;
import com.baplay.service.IAttachmentManager;
import com.baplay.service.ICompanyManager;
import com.baplay.service.IEmployeeManager;
import com.baplay.service.IFlowManager;
import com.baplay.service.IGameDataManager;
import com.baplay.service.IIssueFormManager;
import com.baplay.service.INotifyManager;
import com.baplay.util.DateUtil;
import com.baplay.util.HandleFileUpload;
import com.baplay.util.ReportExcel;
import com.baplay.util.StringUtil;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/issueForm")
public class IssueFormController extends BaseController {
	
	private static final String FUNCTION_NAME = "重大事故通報單";
	private static final int PAGE_SIZE = 25;
	@Autowired
	private IIssueFormManager issueFormManager;
	@Autowired
	private ICompanyManager companyManager;
	@Autowired
	private IEmployeeManager employeeManager;
	@Autowired
	private IFlowManager flowManager;
	@Autowired
	private INotifyManager notifyManager;
	@Autowired
	private IGameDataManager gameDataManager;
	@Autowired
	private IAttachmentManager attachmentManager;
	
	/* (non-Javadoc)
	 * @see com.baplay.web.controller.BaseController#getFormType()
	 */
	protected String getFormType() {
		return Form.ISSUE.getValue();
	}
	
	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		request.setAttribute("allGameData", gameDataManager.findAllGameData());
		request.setAttribute("companys", companyManager.findAllGameCompany());
		
		return "/issueForm/init";
	}
	
	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		request.setAttribute("companys", companyManager.findAllGameCompany());
		request.setAttribute("allGameData", gameDataManager.findAllGameData());
		
		if(id == null) {
			request.setAttribute("requestDepartment", getEmployee().getDepartmentId());
			request.setAttribute("feedbackOperator", getEmployee().getName());
		}
		else {
			IssueForm form = issueFormManager.findOneById(id);
			request.setAttribute("cp", form);
			
			List<Attachment> attachment = attachmentManager.findFormAttachment(id, getFormType());
			request.setAttribute("attachment", attachment);
			
			List<IssueMemo> memo = issueFormManager.findMemo(id);
			request.setAttribute("memo", memo);
			
			request.setAttribute("feedbackOperator", form.getCreatorName());
		}
		
		return "/issueForm/edit";
	}
	
	@RequestMapping("/get")
	public String get(HttpServletRequest request, Long id) {
		IssueForm form = issueFormManager.findOneById(id);
		request.setAttribute("cp", form);
		
		Map<String, Stamp> stamp = flowManager.findFormStamp(form.getWorkFlowId());
		request.setAttribute("stamp", stamp);
		
		// 註銷
		WorkFlow workFlow = flowManager.findFormFlow(form.getWorkFlowId());
		
		if(workFlow.getStatus() == Flow.USER_TERMINATE.getValue()) {
			WorkFlowDetail workFlowDetail = flowManager.findLastOneById(form.getWorkFlowId());
			String memo = String.format("用戶於%s註銷(%s)", DateUtil.convertDate2Str(workFlowDetail.getProcessTime()), workFlowDetail.getMemo());
			request.setAttribute("memo", memo);
		}
		
		// 附件
		List<Attachment> attachment = attachmentManager.findFormAttachment(id, getFormType());
		request.setAttribute("attachment", attachment);
		
		return "/issueForm/get";
	}
	
	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, IssueForm issueForm) {	
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;
		
		String url[] = request.getParameterValues("url");
		String desc[] = request.getParameterValues("fileName");
		log.debug("url:" + StringUtils.join(url, ","));
		
		String errMsg = "";
		String rtnCode = "";
		
		try {
			if(issueForm != null) {
				if(issueForm.getId() == null || issueForm.getId() <= 0) {
					issueForm.setIssueFeedbackOperator(user.getId());
					issueForm.setMemoOperator(user.getId());
					issueForm = issueFormManager.add(issueForm, url, desc, user, false, getFormType(), getFormType());				
					flag = (issueForm!=null);				
				} 
				else {
					issueForm.setIssueFeedbackOperator(user.getId());
					issueForm.setMemoOperator(user.getId());
					flag = (issueFormManager.update(issueForm, url, desc, user, false, getFormType(), getFormType()) > 0);			
				}
			}
		}
		catch(IllegalException e) {
			log.error("err:", e);
			
			rtnCode = e.getErrCode();
			errMsg = e.getErrMsg();
		}
		catch(Exception e) {
			log.error("err:", e);
			
			rtnCode = "0001";
			errMsg = e.getLocalizedMessage();
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");			
		} 
		else {
			result.put("code", rtnCode);
			result.put("message", String.format("保存失敗！(%s)", errMsg));	
		}
		
		return result;				
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, IssueFormForm issueFormForm) {
		issueFormForm.setPageSize(PAGE_SIZE);
		issueFormForm.setExport(false);
		
		// Auth
		issueFormForm.setFormRole(getUserFormRole());
		issueFormForm.setLoginUser(getLoginUser().getId());
		issueFormForm.setFormType(getFormType());
		
		Page<IssueForm> page = issueFormManager.list(issueFormForm);
		request.setAttribute("page", page);
		
		return "/issueForm/list";
	}
	
	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> deleteCP(Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		if(id != null && id > 0) {
			flag = (issueFormManager.delete(id) > 0);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		
		return result;
	}
	
	@RequestMapping("/export")
	public void export(HttpServletRequest request, HttpServletResponse response, IssueFormForm issueFormForm) {		
		// Auth
		issueFormForm.setFormRole(getUserFormRole());
		issueFormForm.setLoginUser(getLoginUser().getId());
		issueFormForm.setFormType(getFormType());				
		
		String[] dataTitles = {"表單編號", "公司", "重大事故標題", "遊戲", "級別", "責任歸屬", "事故開始時間", "事故通報時間", "事故通報客服", "事故通報方式", "事故描述", "修復時間", "修復通報時間", "修復通報客服", "修復通報方式", "事故原因說明", "修復方式", "未來預防方式", "原因分類", "事故等級", "未來防備等級", "狀態"};  
		issueFormForm.setExport(true);
		Page<IssueForm> page = issueFormManager.list(issueFormForm);
		List<Object[]> list = new ArrayList<Object[]>();

		if(page.getResult() != null && !page.getResult().isEmpty()) {
			for(IssueForm nl : page.getResult()) {
				Object[] o = new Object[dataTitles.length];
				o[0] = nl.getSerialNo();
				o[1] = nl.getCompanyName();
				o[2] = nl.getIssueTitle();
				o[3] = StringUtils.isEmpty(nl.getGameName()) ? "平台" : nl.getGameName();
				
				if(nl.getLevel() == null) {
					o[4] = "";
				}
				else {
					o[4] = StringUtil.getChineseNumber(nl.getLevel()) + "級";
				}			
								
				o[5] = StringUtils.defaultIfBlank(nl.getAccountablity(), StringUtils.EMPTY) + StringUtils.defaultIfBlank(nl.getOwner(), StringUtils.EMPTY);
				o[6] = DateUtil.getStringOfDate(nl.getIssueStartTime());
				o[7] = DateUtil.getStringOfDate(nl.getIssueFeedbackTime());
				o[8] = nl.getIssueOperatorName();
				o[9] = StringUtil.getChannelDesc(nl.getIssueFeedbackChannel());
				o[10] = nl.getIssueFeedbackDescription();
				o[11] = DateUtil.getStringOfDate(nl.getRepairTime());
				o[12] = DateUtil.getStringOfDate(nl.getRepairFeedbackTime());
				o[13] = nl.getRepairOperatorName();
				o[14] = StringUtil.getChannelDesc(nl.getRepairFeedbackChannel());
				o[15] = nl.getIssueDescription();
				o[16] = nl.getRepairDescription();
				o[17] = nl.getPreventionDescription();
				
				if(nl.getIssueType() != null){
					switch(nl.getIssueType()){
					case 1:
						o[18] = "外部人為疏失";
						break;
					case 2:
						o[18] = "內部人為疏失";
						break;
					case 3:
						o[18] = "外部駭客攻擊";
						break;
					}
					
					if(nl.getIssueType() == 3){
						switch(nl.getPreventionLevel()){
						case 1:
							o[20] = "完整 ";
							break;
						case 2:
							o[20] = "一般 ";
							break;
						case 3:
							o[20] = "不足";
							break;
						default:
							o[20] = "";
						}
					}else{
						switch(nl.getPreventionLevel()){
						case 1:
							o[20] = "高 ";
							break;
						case 2:
							o[20] = "中";
							break;
						case 3:
							o[20] = "低";
							break;
						default:
							o[20] = "";
						}
					}
					
				}else{
					o[18] = "";
				}
				
				if(nl.getIssueLevel() != null){
					switch(nl.getIssueLevel()){
					case 1:
						o[19] = "重大";
						break;
					case 2:
						o[19] = "一般";
						break;
					case 3:
						o[19] = "輕微";
						break;
					}
				}else{
					o[19] = "";
				}
				
				o[21] = StringUtil.getFlowStatusDesc(nl.getStatus());
				
				list.add(o);
			}
		}
		
		try {
			ReportExcel.reportExcel(request, response, FUNCTION_NAME + DateUtil.getCurrentTimeStr("yyyyMMddHHmmss"), "", dataTitles, list);
		} 
		catch(Exception e) {
			log.error("err:" , e);
		}
	}
	
	/**
	 * 審放頁面
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/process")
	public String process(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		TEfunuser user = getLoginUser();
		// Flow
		WorkFlowDetail workFlowDetail = flowManager.findFormFlowStep(workFlowDetailId);
		flowStep(request, workFlowDetail);
				
		// Form
		IssueForm form = issueFormManager.findOneByFlow(workFlowDetail.getWorkFlowId());		
		request.setAttribute("form", form);
		
		Map<String, Stamp> stamp = flowManager.findFormStamp(form.getWorkFlowId());
		request.setAttribute("stamp", stamp);
		
		// UserFlowStep
		UserFlowStep userFlowStep = flowManager.findUserFlowStep(workFlowId, workFlowDetail.getStepId(), user.getId());
		
		if(userFlowStep != null) {
			FlowStep flowStep = flowManager.findFlowStepById(workFlowDetail.getStepId());
			request.setAttribute("assign", flowStep.getRoleName());			
		}
		
		// 附件
		List<Attachment> attachment = attachmentManager.findFormAttachment(form.getId(), getFormType());
		request.setAttribute("attachment", attachment);
				
		return "/issueForm/flow";
	}
	
	/**
	 * 送審
	 * 
	 * @param request
	 * @param paymentForm
	 * @return
	 */
	@RequestMapping("/submit")
	public @ResponseBody Map<String, Object> doSubmit(HttpServletRequest request, IssueForm issueForm) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;
		
		String url[] = request.getParameterValues("url");
		String desc[] = request.getParameterValues("fileName");
		log.debug("url:" + StringUtils.join(url, ","));
		
		try {
			if(issueForm != null) {
				if(issueForm.getId() == null || issueForm.getId() <= 0) {
					issueForm.setIssueFeedbackOperator(user.getId());
					issueForm.setMemoOperator(user.getId());
					issueForm = issueFormManager.add(issueForm, url, desc, user, true, getFormType(), getFormType());				
					flag = (issueForm!=null);			
				} 
				else {
					issueForm.setIssueFeedbackOperator(user.getId());
					issueForm.setMemoOperator(user.getId());
					flag = (issueFormManager.update(issueForm, url, desc, user, true, getFormType(), getFormType()) > 0);			
				}
				
				if(flag) {
					notifyManager.notify(issueForm.getWorkFlowId());
				}
			}
		}
		catch(Exception e) {
			log.error("err:", e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "送審成功！");			
		} 
		else {
			result.put("code", "0001");
			result.put("message", "送審失敗！");	
		}
		
		return result;
	}
	
	/**
	 * 審核
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/verify")
	public @ResponseBody Map<String, Object> doVerify(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId,
			@RequestParam(value="repairTime", required=false, defaultValue="") String repairTime,
			@RequestParam(value="repairFeedbackTime", required=false, defaultValue="") String repairFeedbackTime,
			@RequestParam(value="repairFeedbackChannel", required=false, defaultValue="") String repairFeedbackChannel,
			@RequestParam(value="issueDescription", required=false, defaultValue="") String issueDescription,
			@RequestParam(value="repairDescription", required=false, defaultValue="") String repairDescription,
			@RequestParam(value="preventionDescription", required=false, defaultValue="") String preventionDescription,
			@RequestParam(value="issueType", required=false, defaultValue="") String issueType,
			@RequestParam(value="issueLevel", required=false, defaultValue="") String issueLevel,
			@RequestParam(value="preventionLevel", required=false, defaultValue="") String preventionLevel,
			@RequestParam(value="accountablity", required=false, defaultValue="") String accountablity,
			@RequestParam(value="owner", required=false, defaultValue="") String owner,
			@RequestParam(value="assignUser", required=false, defaultValue="") String assignUser) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			doProcess(workFlowDetailId, repairTime, repairFeedbackTime, repairFeedbackChannel, 
					issueDescription, repairDescription, preventionDescription, 
					issueType, issueLevel, preventionLevel, accountablity, owner, assignUser);
			flowManager.verifyFlow(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "審核成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "審核失敗！");
		}
		
		return result;
	}
	
	/**
	 * 放行
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/pass")
	public @ResponseBody Map<String, Object> doPass(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.passFlow(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "放行成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "放行失敗！");
		}
		
		return result;
	}
	
	/**
	 * 退件
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/reject")
	public @ResponseBody Map<String, Object> doReject(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId, 
			@RequestParam("memo") String memo) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.rejectFlow(workFlowDetailId, user.getId(), memo);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "退件成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "退件失敗！");
		}
		
		return result;
	}
	
	/**
	 * 取消
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/abort")
	public @ResponseBody Map<String, Object> abort(HttpServletRequest request, 
			@RequestParam("id") Long workFlowDetailId) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.lockAbort(workFlowDetailId);
			flag = true;
		}
		catch(Exception e) {
			 log.error("err:", e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "取消成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "取消失敗！");
		}
		
		return result;
	}
	
	/**
	 * 審放
	 * 
	 * @param workFlowDetailId
	 * @param repairTime
	 * @param repairFeedbackTime
	 * @param repairFeedbackChannel
	 * @param issueDescription
	 * @param repairDescription
	 * @param preventionDescription
	 * @param issueType
	 * @param issueLevel
	 * @param preventionLevel
	 * @throws Exception
	 */
	private void doProcess(Long workFlowDetailId,
		String repairTime, String repairFeedbackTime, String repairFeedbackChannel,
		String issueDescription, String repairDescription, String preventionDescription,
		String issueType, String issueLevel, String preventionLevel, 
		String accountablity, String owner, String assignUser) throws Exception {
		
		WorkFlowDetail workFlowDetail = flowManager.findFormFlowStep(workFlowDetailId);
		IssueForm form = issueFormManager.findOneByFlow(workFlowDetail.getWorkFlowId());
		
		if(StringUtils.isNotEmpty(repairTime)) {
			form.setRepairTime(DateUtil.parse(repairTime, null));
			form.setRepairFeedbackTime(DateUtil.parse(repairFeedbackTime, null));
			form.setRepairFeedbackChannel(Integer.parseInt(repairFeedbackChannel));
			form.setRepairFeedbackOperator(getLoginUser().getId());
		}
		
		if(StringUtils.isNotEmpty(issueDescription)) {
			form.setIssueDescription(issueDescription);			
		}
		
		if(StringUtils.isNotEmpty(repairDescription)) {
			String history = "";
			// TODO 退件時無法處理多人修復人歷史資料
			/*
			if(StringUtils.isNotEmpty(form.getRepairDescription())) {
				history = form.getRepairDescription() + "<br>";
			}
			*/
			form.setRepairDescription(history + repairDescription);
		}
		
		if(StringUtils.isNotEmpty(preventionDescription)) {
			form.setPreventionDescription(preventionDescription);
		}
		
		if(StringUtils.isNotEmpty(issueType) &&
				StringUtils.isNotEmpty(issueLevel) &&
				StringUtils.isNotEmpty(preventionLevel)) {
			form.setIssueType(Integer.parseInt(issueType));
			form.setIssueLevel(Integer.parseInt(issueLevel));
			form.setPreventionLevel(Integer.parseInt(preventionLevel));
		}
		
		if(StringUtils.isNotEmpty(accountablity)) {
			form.setAccountablity(accountablity);
		}
		
		if(StringUtils.isNotEmpty(owner)) {
			form.setOwner(owner);
		}
		
		if(StringUtils.isNotEmpty(assignUser)) {
			FlowStep flowStep = flowManager.findNextStepById(workFlowDetail.getStepId());
			// User work flow
			UserFlowStep step = new UserFlowStep();
			step.setWorkFlowId(workFlowDetail.getWorkFlowId());
			step.setStepId(flowStep.getId());
			step.setUserId(assignUser);
			
			flowManager.addUserFlowStep(step);
		}
		
		issueFormManager.update(form);
	}
	
	/**
	 * 備註
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/memo")
	public @ResponseBody Map<String, Object> memo(HttpServletRequest request, 
			@RequestParam("id") Long id, @RequestParam("memo") String memo) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			TEfunuser user = getLoginUser();
			issueFormManager.memo(id, user.getId(), memo);
			flag = true;
		}
		catch(Exception e) {
			 log.error("err:", e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "備註成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "備註失敗！");
		}
		
		return result;
	}
	
	/**
	 * 指派人員選單
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/assignList")
	public @ResponseBody Map<String, Object> assignList(HttpServletRequest request,
			@RequestParam("role[]") Integer[] role) {
		Map<String, Object> result = Maps.newHashMap();
		
		try {
			List<FormUser> formUsers = employeeManager.findIssueRepairUser(role);
			
			result.put("code", "0000");
			result.put("message", "載入成功！");
			result.put("formUsers", formUsers);			
		}
		catch(Exception e) {
			log.error("err:", e);
			
			result.put("code", "0001");
			result.put("message", "載入失敗！");
		}
		
		return result;
	}
	
	/**
	 * 附件上傳
	 * 
	 * @param file
	 * @param json
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody HashMap<String, String> handleFileUpload(@RequestParam("file") MultipartFile file,
			@RequestParam(value = "json", required = false, defaultValue = "") String json) throws IOException, Exception {		
		HashMap<String, String> result = new HashMap<String, String>();
		log.debug("[handleFileUpload][filename=" + file.getOriginalFilename() + "]");
		String url = HandleFileUpload.upload(file, URL.URL_IMG_ISSUEFORM.toString(), URL.FTP_IMG_ISSUEFORM.toString(), Config.UPLOAD_FILE_LIMIT);
		if (StringUtils.isNotEmpty(json)) {
			log.debug("json:" + json);
			JSONObject jsonObj = new JSONObject(json);
			log.debug("jsonObj:" + jsonObj.toString());
		}
		result.put("url", url);
		result.put("description", file.getOriginalFilename());
		return result;
	}
	
	/**
	 * 備註記錄
	 * 
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/memoList")
	public @ResponseBody Map<String, Object> memoList(HttpServletRequest request, 
			@RequestParam("id") Long id) {
		Map<String, Object> result = Maps.newHashMap();		
		
		try {		
			List<IssueMemo> list = issueFormManager.findMemo(id);
			result.put("list", list);
		}
		catch(Exception e) {
			log.error("err:", e);
			result.put("list", new ArrayList<IssueMemo>());
		}				
		
		return result;
	}
	
	/**
	 * 附件刪除
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping("/deleteAttachment")
	public @ResponseBody Map<String, Object> deleteAttachment(Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;

		if (id != null && id > 0) {
			flag = attachmentManager.delete(id) > 0;			
			// TODO remove CDN file
		}

		if(flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}
}