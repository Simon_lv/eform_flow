package com.baplay.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import com.baplay.entity.Company;
import com.baplay.entity.TEfunuser;
import com.baplay.form.CompanyForm;
import com.baplay.service.ICompanyManager;
import com.baplay.util.DateUtil;
import com.baplay.util.ReportExcel;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/company")
public class CompanyController extends BaseController {
	
	private static final String FUNCTION_NAME = "公司資料";
	private static final int PAGE_SIZE = 25;
	@Autowired
	private ICompanyManager companyManager;
	
	/* (non-Javadoc)
	 * @see com.baplay.web.controller.BaseController#getFormType()
	 */
	protected String getFormType() {
		return StringUtils.EMPTY;
	}
	
	@RequestMapping("/init")
	public String init() {
		return "/company/init";
	}
	
	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		if(id != null) {
			request.setAttribute("cp", companyManager.findOneById(id));
		}
		
		return "/company/edit";
	}
	
	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(Company company) {	
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;
		
		if(company != null) {
			if(company.getId() == null || company.getId() <= 0) {
				company.setCreator(user.getId());
				
				company = companyManager.add(company);				
				flag = (company!=null);				
			} 
			else {
				company.setModifier(user.getId());
				flag = (companyManager.update(company) > 0);			
			}
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");			
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");	
		}
		
		return result;				
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, CompanyForm companyForm) {
		companyForm.setPageSize(PAGE_SIZE);
		companyForm.setExport(false);		
		Page<Company> page = companyManager.list(companyForm);
		request.setAttribute("page", page);
		
		return "/company/list";
	}
	
	@RequestMapping("/deleteCP")
	public @ResponseBody Map<String, Object> deleteCP(Long id) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;
		
		if(id != null && id > 0) {
			flag = (companyManager.delete(id, user.getId()) > 0);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		
		return result;
	}
	
	@RequestMapping("/export")
	public void export(HttpServletRequest request, HttpServletResponse response, CompanyForm companyForm) {		
		String[] dataTitles = {"ID", "公司名稱", "統編", "地址", "電話", "負責人", "成立時間", "修改者", "修改時間", "建立者", "建立時間"};  
		companyForm.setExport(true);
		Page<Company> page = companyManager.list(companyForm);
		List<Object[]> list = new ArrayList<Object[]>();

		if(page.getResult() != null && !page.getResult().isEmpty()) {
			for(Company nl : page.getResult()) {
				Object[] o = new Object[dataTitles.length];
				o[0] = nl.getId();
				o[1] = nl.getName();
				o[2] = nl.getTaxNumber();
				o[3] = nl.getAddress();
				o[4] = nl.getPhone();
				o[5] = nl.getOwner();
				o[6] = DateUtil.getStringOfDate(nl.getEstablishDate(), DateUtil.defaultDatePatternStr, null);		
				o[7] = nl.getModifier();
				o[8] = DateUtil.getStringOfDate(nl.getUpdateTime());
				o[9] = nl.getCreator();
				o[10] = DateUtil.getStringOfDate(nl.getCreateTime());
				
				list.add(o);
			}
		}
		
		try {
			ReportExcel.reportExcel(request, response, FUNCTION_NAME + System.currentTimeMillis(), "", dataTitles, list);
		} 
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}