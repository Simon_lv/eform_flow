package com.baplay.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springside.modules.orm.Page;

import com.baplay.dto.FormFlow;
import com.baplay.entity.FormType;
import com.baplay.entity.WorkFlow;
import com.baplay.form.FormMgrForm;
import com.baplay.service.ICompanyManager;
import com.baplay.service.IFlowManager;

@Controller
@RequestMapping("/formMgr")
public class FormMgrController extends BaseController {

	private static final int PAGE_SIZE = 25;
	
	@Autowired
	private IFlowManager flowManager;
	@Autowired
	private ICompanyManager companyManager;

	/* (non-Javadoc)
	 * @see com.baplay.web.controller.BaseController#getFormType()
	 */
	@Override
	protected String getFormType() {
		return StringUtils.EMPTY;
	}
	
	/**
	 * 查詢頁
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		request.setAttribute("formTypeList", flowManager.findAllFormType());
		request.setAttribute("companyList", companyManager.findAllCompany());
		
		return "/formMgr/init";
	}
	
	/**
	 * 清單頁
	 * 
	 * @param request
	 * @param form
	 * @return
	 */
	@RequestMapping("/list")
	public String list(HttpServletRequest request, FormMgrForm form) {
		form.setPageSize(PAGE_SIZE);
		form.setExport(false);		
		Page<FormFlow> page = flowManager.list(form);
		request.setAttribute("page", page);
		
		return "/formMgr/list";
	}
	
	/**
	 * 明細
	 * 
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/get")
	public String get(HttpServletRequest request, HttpServletResponse response, Long id) {
		WorkFlow workFlow = flowManager.findFormFlow(id);
		String[] type = workFlow.getFormType().split(",");
		FormType formType = flowManager.findFomType(type[1], type[0], workFlow.getFlowSchemaId());
		String url = String.format("%s?id=%s", formType.getViewPage(), workFlow.getFormId());
		
		return "redirect:" + url;
	}
}