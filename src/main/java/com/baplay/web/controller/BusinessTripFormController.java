package com.baplay.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springside.modules.orm.Page;

import com.baplay.constant.Config;
import com.baplay.constant.Form;
import com.baplay.constant.URL;
import com.baplay.dto.Stamp;
import com.baplay.entity.Attachment;
import com.baplay.entity.BusinessTripForm;
import com.baplay.entity.Employee;
import com.baplay.entity.TEfunuser;
import com.baplay.entity.WorkFlowDetail;
import com.baplay.form.BusinessTripFormForm;
import com.baplay.service.IAttachmentManager;
import com.baplay.service.IBusinessTripFormManager;
import com.baplay.service.ICompanyManager;
import com.baplay.service.IEmployeeManager;
import com.baplay.service.IFlowManager;
import com.baplay.service.IFormAuthManager;
import com.baplay.service.INotifyManager;
import com.baplay.util.DateUtil;
import com.baplay.util.HandleFileUpload;
import com.baplay.util.PdfUtil;
import com.baplay.util.ReportExcel;
import com.baplay.util.StringUtil;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/businessTripForm")
public class BusinessTripFormController extends BaseController {
	
	private static final String FUNCTION_NAME = "出差申請單";
	private static final int PAGE_SIZE = 25;
	@Autowired
	private IBusinessTripFormManager businessTripFormManager;
	@Autowired
	private ICompanyManager companyManager;	
	@Autowired
	private IEmployeeManager employeeManager;
	@Autowired
	private IFlowManager flowManager;
	@Autowired
	private INotifyManager notifyManager;
	@Autowired
	private IFormAuthManager formAuthManager;
	@Autowired
	private IAttachmentManager attachmentManager;
	
	/* (non-Javadoc)
	 * @see com.baplay.web.controller.BaseController#getFormType()
	 */
	protected String getFormType() {
		return Form.BUSINESS_TRIP.getValue();
	}
	
	@RequestMapping("/init")
	public String init(HttpServletRequest request) {		
		boolean auth = formAuthManager.formAuth(getFormType(), getUserFormRole());
		request.setAttribute("companys", auth ? companyManager.findAllCompany() : getUserCompany());
		return "/businessTripForm/init";
	}
	
	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		List<Employee> employeeList = getEmployee() == null ? employeeManager.findAllEmployee() : employeeManager.findAllEmployee(getEmployee().getCompanyId().intValue());
		request.setAttribute("companyEmployee", employeeList);
		
		if(id != null) {
			request.setAttribute("cp", businessTripFormManager.findOneById(id));
			
			List<Attachment> attachment = attachmentManager.findFormAttachment(id, getFormType());
			request.setAttribute("attachment", attachment);
		}
		
		return "/businessTripForm/edit";
	}
	
	@RequestMapping("/get")
	public String get(HttpServletRequest request, Long id) {
		BusinessTripForm form = businessTripFormManager.findOneById(id);
		request.setAttribute("cp", form);
		
		Map<String, Stamp> stamp = flowManager.findFormStamp(form.getWorkFlowId());
		request.setAttribute("stamp", stamp);
		
		// 附件
		List<Attachment> attachment = attachmentManager.findFormAttachment(id, getFormType());
		request.setAttribute("attachment", attachment);
		
		return "/businessTripForm/get";
	}
	
	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, BusinessTripForm businessTripForm) {	
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;
		String url[] = request.getParameterValues("url");
		String desc[] = request.getParameterValues("fileName");
		
		try {
			if(businessTripForm != null) {
				if(businessTripForm.getId() == null || businessTripForm.getId() <= 0) {
					businessTripForm = businessTripFormManager.add(businessTripForm, url, desc, user, false, getFormType(), getFormType());
					
					flag = (businessTripForm!=null);				
				} 
				else {				
					flag = (businessTripFormManager.update(businessTripForm, url, desc, user, false, getFormType(), getFormType()) > 0);				
				}
			}
		}
		catch(Exception e) {
			log.error("err:", e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");			
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");	
		}
		
		return result;				
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, BusinessTripFormForm businessTripFormForm) {
		businessTripFormForm.setPageSize(PAGE_SIZE);
		businessTripFormForm.setExport(false);
		
		// Auth
		businessTripFormForm.setFormRole(getUserFormRole());
		businessTripFormForm.setLoginUser(getLoginUser().getId());
		businessTripFormForm.setFormType(getFormType());
		businessTripFormForm.setEmployeeIdList(getUserAllEmployeeId());
		
		Page<BusinessTripForm> page = businessTripFormManager.list(businessTripFormForm);
		request.setAttribute("page", page);
		
		return "/businessTripForm/list";
	}
	
	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> deleteCP(Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		if(id != null && id > 0) {
			flag = (businessTripFormManager.delete(id) > 0);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		
		return result;
	}
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody HashMap<String, String> handleFileUpload(@RequestParam("file") MultipartFile file,
			@RequestParam(value = "json", required = false, defaultValue = "") String json) throws IOException, Exception {		
		HashMap<String, String> result = new HashMap<String, String>();
		log.debug("[BusinessTripFormController.handleFileUpload][filename=" + file.getOriginalFilename() + "]");
		String url = HandleFileUpload.upload(file, URL.URL_IMG_BUSINESSTRIPFORM.toString(), URL.FTP_IMG_BUSINESSTRIPFORM.toString(), Config.UPLOAD_FILE_LIMIT);
		if (StringUtils.isNotEmpty(json)) {
			log.debug("json:" + json);
			JSONObject jsonObj = new JSONObject(json);
			log.debug("jsonObj:" + jsonObj.toString());
		}
		result.put("url", url);
		result.put("description", file.getOriginalFilename());
		return result;
	}
	
	@RequestMapping("/export")
	public void export(HttpServletRequest request, HttpServletResponse response, BusinessTripFormForm businessTripFormForm) {		
		// Auth
		businessTripFormForm.setFormRole(getUserFormRole());
		businessTripFormForm.setLoginUser(getLoginUser().getId());
		businessTripFormForm.setFormType(getFormType());
		businessTripFormForm.setEmployeeIdList(getUserAllEmployeeId());				
		
		String[] dataTitles = {"表單編號", "公司名稱", "部門名稱", "申請人", "申請日期", "國內外", "職稱", "出差事由", "出差地點", "去程航班日期", "去程航空公司", "去程航班代號", "去程到達國家(地區)", "去程到達城市", "去程到達時間", "回程航班日期", "回程航空公司", "回程航班代號", "回程到達國家(地區)", "回程到達城市", "回程到達時間", "旅行社行程表(圖片上傳URL)", "辦理護照", "辦理簽證", "辦理台胞證", "機票費用預估", "辦理文件費用預估", "住宿費用預估", "機場接送預估", "出差費用預估合計", "是否預支", "預支幣別", "預支金額", "出差日支費", "備註", "狀態"};  
		businessTripFormForm.setExport(true);
		Page<BusinessTripForm> page = businessTripFormManager.list(businessTripFormForm);
		List<Object[]> list = new ArrayList<Object[]>();

		if(page.getResult() != null && !page.getResult().isEmpty()) {
			for(BusinessTripForm nl : page.getResult()) {
				Object[] o = new Object[dataTitles.length];
				o[0] = nl.getSerialNo();
				o[1] = nl.getCompanyName();
				o[2] = nl.getDepartmentName();
				o[3] = nl.getCreatorName();
				o[4] = DateUtil.getStringOfDate(nl.getRequestDate(), DateUtil.defaultDatePatternStr, null);
				
				if(nl.getOversea() == 1){
					o[5] = "國內";
				}else{
					o[5] = "國外";
				}
				
				o[6] = nl.getJobTitle();
				o[7] = nl.getTripMatter();
				o[8] = nl.getTripLocation();
				o[9] = DateUtil.getStringOfDate(nl.getOutboundDepartureTime(), DateUtil.defaultDatePatternStr, null);
				o[10] = nl.getOutboundFlight();
				o[11] = nl.getOutboundFlightNo();
				o[12] = nl.getOutboundCountry();
				o[13] = nl.getOutboundCity();
				o[14] = DateUtil.getStringOfDate(nl.getOutboundLandingTime());
				o[15] = DateUtil.getStringOfDate(nl.getInboundDepartureTime(), DateUtil.defaultDatePatternStr, null);
				o[16] = nl.getInboundFlight();
				o[17] = nl.getInboundFlightNo();
				o[18] = nl.getInboundCountry();
				o[19] = nl.getInboundCity();
				o[20] = DateUtil.getStringOfDate(nl.getInboundLandingTime());
				
				List<Attachment> attachments = attachmentManager.findFormAttachment(nl.getId(), getFormType());
				List<String> memo = new LinkedList<String>();
				
				for(Attachment attachment : attachments) {
					if(StringUtils.isEmpty(attachment.getDescription())) {
						String[] url = attachment.getUrl().split("/");
						memo.add(url[url.length - 1]);
					}
					else {
						memo.add(attachment.getDescription());
					}
				}
				
				o[21] = StringUtils.join(memo, "\n");				
				
				o[22] = StringUtil.getYesNoDesc(nl.getAgentPassport());
				o[23] = StringUtil.getYesNoDesc(nl.getAgentVisa());
				o[24] = StringUtil.getYesNoDesc(nl.getAgentMtp());
				o[25] = nl.getEstimatedCostAirfare();
				o[26] = nl.getEstimatedCostAgent();
				o[27] = nl.getEstimatedCostLodging();
				o[28] = nl.getEstimatedCostPickup();
				o[29] = (nl.getEstimatedCostAirfare() == null ? 0 : nl.getEstimatedCostAirfare()) +
						(nl.getEstimatedCostAgent() == null ? 0 : nl.getEstimatedCostAgent()) +
						(nl.getEstimatedCostLodging() == null ? 0 : nl.getEstimatedCostLodging()) + 
						(nl.getEstimatedCostPickup() == null ? 0 : nl.getEstimatedCostPickup());
				o[30] = StringUtil.getYesNoDesc(nl.getCashAdvance());
				o[31] = StringUtil.getCurrencyDesc(nl.getAdvanceCurrency());
				o[32] = nl.getAdvanceAmount();
				o[33] = (nl.getDailyAllowance() == 1) ? "600/日" : "1000/日" ; 
				o[34] = nl.getMemo();
				o[35] = StringUtil.getFlowStatusDesc(nl.getStatus());				
				
				list.add(o);
			}
		}
		
		try {
			ReportExcel.reportExcel(request, response, FUNCTION_NAME + DateUtil.getCurrentTimeStr("yyyyMMddHHmmss"), "", dataTitles, list);
		} 
		catch(Exception e) {
			log.error("err:" , e);
		}
	}
	
	/**
	 * 審放頁面
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/process")
	public String process(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		// Flow
		WorkFlowDetail workFlowDetail = flowManager.findFormFlowStep(workFlowDetailId);
		flowStep(request, workFlowDetail);
				
		// Form
		BusinessTripForm form = businessTripFormManager.findOneByFlow(workFlowDetail.getWorkFlowId());		
		request.setAttribute("form", form);
		
		// 附件
		List<Attachment> attachment = attachmentManager.findFormAttachment(form.getId(), getFormType());
		request.setAttribute("attachment", attachment);				
		
		Map<String, Stamp> stamp = flowManager.findFormStamp(form.getWorkFlowId());
		request.setAttribute("stamp", stamp);
			
		return "/businessTripForm/flow";
	}
	
	/**
	 * 送審
	 * 
	 * @param request
	 * @param paymentForm
	 * @return
	 */
	@RequestMapping("/submit")
	public @ResponseBody Map<String, Object> doSubmit(HttpServletRequest request, BusinessTripForm businessTripForm) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;
		String url[] = request.getParameterValues("url");
		String desc[] = request.getParameterValues("fileName");
		
		try {
			if(businessTripForm != null) {
				if(businessTripForm.getId() == null || businessTripForm.getId() <= 0) {
					businessTripForm = businessTripFormManager.add(businessTripForm, url, desc, user, true, getFormType(), getFormType());				
					flag = (businessTripForm!=null);			
				} 
				else {				
					flag = (businessTripFormManager.update(businessTripForm, url, desc, user, true, getFormType(), getFormType()) > 0);			
				}
				
				if(flag) {
					notifyManager.notify(businessTripForm.getWorkFlowId());
				}	
			}
		}
		catch(Exception e) {
			log.error("err:", e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "送審成功！");			
		} 
		else {
			result.put("code", "0001");
			result.put("message", "送審失敗！");	
		}
		
		return result;
	}
	
	/**
	 * 審核
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/verify")
	public @ResponseBody Map<String, Object> doVerify(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.verifyFlow(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "審核成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "審核失敗！");
		}
		
		return result;
	}
	
	/**
	 * 放行
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/pass")
	public @ResponseBody Map<String, Object> doPass(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.passFlow(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "放行成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "放行失敗！");
		}
		
		return result;
	}
	
	/**
	 * 退件
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/reject")
	public @ResponseBody Map<String, Object> doReject(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId, 
			@RequestParam("memo") String memo) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.rejectFlow(workFlowDetailId, user.getId(), memo);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "退件成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "退件失敗！");
		}
		
		return result;
	}
	
	/**
	 * 取消
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/abort")
	public @ResponseBody Map<String, Object> abort(HttpServletRequest request, 
			@RequestParam("id") Long workFlowDetailId) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.lockAbort(workFlowDetailId);
			flag = true;
		}
		catch(Exception e) {
			 log.error("err:", e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "取消成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "取消失敗！");
		}
		
		return result;
	}

	/**
	 * 會簽
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/countersign")
	public @ResponseBody Map<String, Object> doCountersign(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.countersign(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			 log.error("err:", e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "會簽成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "會簽失敗！");
		}
		
		return result;
	}
	
	/**
	 * 刪除附件
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping("/deleteAttachment")
	public @ResponseBody Map<String, Object> deleteAttachment(Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;

		if (id != null && id > 0) {
			flag = attachmentManager.delete(id) > 0;			
			// TODO remove CDN file
		}

		if(flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}
	
	/**
	 * PDF
	 * 
	 * @param request
	 * @param response
	 * @param formId
	 */
	@RequestMapping("/pdf")
	public void pdf(HttpServletRequest request, HttpServletResponse response, Long formId) {
		BusinessTripForm form = businessTripFormManager.findOneById(formId);
		PdfUtil pdf = new PdfUtil();
		String content = notifyManager.getFormContent(getFormType(), formId);
		pdf.setHtmlContent(pdf.getBaseUrl(request), FUNCTION_NAME, content);
		//log.debug(content);
		String fileName = FUNCTION_NAME + form.getSerialNo();
		String subject = String.format("%s %s %s", 
				form.getCreatorName(), DateUtil.format(form.getRequestDate(), DateUtil.chineseDatePatternStr), FUNCTION_NAME);
		
		try {
			pdf.export(response, fileName, subject);
		} 
		catch(Exception e) {
			log.error("err:" , e);
		}	
	}
}