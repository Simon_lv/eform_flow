package com.baplay.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.baplay.entity.Company;
import com.baplay.entity.Department;
import com.baplay.entity.Employee;
import com.baplay.entity.FormRole;
import com.baplay.entity.Function;
import com.baplay.entity.Role;
import com.baplay.entity.TEfunuser;
import com.baplay.form.RoleForm;
import com.baplay.form.UserForm;
import com.baplay.service.ICompanyManager;
import com.baplay.service.IDepartmentManager;
import com.baplay.service.IEmployeeManager;
import com.baplay.service.IPermissionManager;
import com.baplay.util.PrintUtil;
import com.sun.tools.jdi.LinkedHashMap;

@Controller
@RequestMapping("/permission")
public class PermissionController {
	protected static final Logger LOG = LoggerFactory.getLogger(PermissionController.class);
	private static final String TAG = "PermissionController";

	@Autowired
	private IPermissionManager manager;
	@Autowired
	private ICompanyManager companyManager;
	@Autowired
	private IEmployeeManager employeeManager;
	@Autowired
	private IDepartmentManager departmentManager;

	@RequestMapping("/roleList")
	public String roleList(HttpSession session, Model model) {
		List<Role> roles = null;
		List<Function> functions = null;
		functions = manager.getFunctions(0, 1);
		roles = manager.getRoles(0);
		model.addAttribute("roles", roles);
		model.addAttribute("functions", functions);
		return "permission/roleList";
	}

	@RequestMapping("/getFunctions")
	@ResponseBody
	public List<Function> getFunctions(@RequestParam(required = true) Long roleId) {
		List<Function> functions = null;
		functions = manager.getFunctions(roleId, 0);
		return functions;
	}

	@RequestMapping("/addRole")
	@ResponseBody
	public Map<String, String> addRole(RoleForm form) {
		manager.addRole(form);
		Map<String, String> map = new HashMap<String, String>();
		map.put("code", "0");
		map.put("msg", "success");
		return map;
	}

	@RequestMapping("/updateRole")
	@ResponseBody
	public Map<String, String> updateRole(RoleForm form) {
		manager.updateRole(form);
		Map<String, String> map = new HashMap<String, String>();
		map.put("code", "0");
		map.put("msg", "success");
		return map;
	}

	@RequestMapping("/updateRoleStatus")
	public String updateRoleStatus(@RequestParam(required = true) Long roleId, @RequestParam(required = true) Integer status) {
		manager.updateRoleStatus(roleId, status);
		return "redirect:/permission/roleList";
	}

	@RequestMapping("/userList")
	public String userList(HttpSession session, String userName, Model model, HttpServletRequest request) throws Exception {
		PrintUtil.printRequest(TAG, "userList", request);
		List<TEfunuser> list = manager.getUsers(userName);

		model.addAttribute("list", list);
		return "permission/userList";
	}

	@RequestMapping("/updateUserStatus")
	public String updateUserStatus(@RequestParam(required = true) Integer status, @RequestParam(required = true) Long userId) {
		manager.updateUserStatus(status, userId);
		return "redirect:/permission/userList";
	}

	@RequestMapping("/getUser")
	@SuppressWarnings("unchecked")
	public ModelAndView getUser(ModelAndView mv, Long id) {
		UserForm user = id == null ? null : manager.getUser(id);
		List<Role> roles = manager.getRoles(1);
		List<FormRole> froles = manager.getFormRoles(1);		
		
		Map<String, List<Employee>> companyEmployeeList = new LinkedHashMap();;
		// Company
		List<Company> companyList = companyManager.findAllCompany();
		
		// Employee of company
		for(Company company : companyList) {
			List<Employee> employeeList = employeeManager.findAllEmployee(company.getId().intValue());
			
			companyEmployeeList.put(company.getName(), employeeList);
		}
		
		// employee mapping
		if(id != null) {
			List<Long> mapping = manager.findMoreEmployeeByUID(id);
			mapping.add(0, user.getEmployeeId());  // 主要員工ID
			
			user.setEmployeeMapping(mapping);
			
			mv.addObject("user", user);
		}
		
		mv.addObject("roles", roles);
		mv.addObject("froles", froles);
		mv.addObject("companyEmployeeList", companyEmployeeList);
		
		mv.setViewName("permission/userDetail");
		
		return mv;
	}

	@RequestMapping("/saveOrUpdate")
	@ResponseBody
	public Map<String, String> addUser(UserForm form, HttpServletRequest request) throws Exception {
		PrintUtil.printRequest(TAG, "addUser", request);
		TEfunuser user = (TEfunuser) SecurityUtils.getSubject().getSession().getAttribute("loginUser");
		
		if (form.getId() == null) {
			form.setCreator(user.getId());
			manager.addUser(form);
		} else {
			manager.updateUser(form);
		}
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("code", "0");
		map.put("msg", "success");
		return map;
	}

	@RequestMapping("/updateUserPwdInit")
	public String updateUserPwdInit() {
		return "permission/updateUserPwd";
	}

	@RequestMapping("/updateUserPwd")
	@ResponseBody
	public Map<String, String> updateUserPwd(@RequestParam(required = true) Long userId, @RequestParam(required = true) String password) {
		manager.updatePwd(userId, password);
		Map<String, String> map = new HashMap<String, String>();
		map.put("code", "0");
		map.put("msg", "success");
		return map;
	}
	
	/**
	 * 主管部門
	 * 
	 * @param mv
	 * @param id
	 * @return
	 */
	@RequestMapping("/getUserManageDept")
	public ModelAndView getUserManageDept(ModelAndView mv, Long id) {
		// Company
		List<Company> companyList = companyManager.findAllCompany();
		
		// Employee of company
		for(Company company : companyList) {
			List<Department> department = departmentManager.findAllDepartment(company.getId());
			company.setDepartment(department);
		}
		
		List<Long> managerDeptList = employeeManager.findUserManageDept(id);		
		
		mv.addObject("company", companyList);
		mv.addObject("managed", managerDeptList);
		mv.addObject("uid", id);
		
		mv.setViewName("permission/userManageDept");
		
		return mv;
	}
	
	/**
	 * 用戶主管部門
	 * 
	 * @param uid
	 * @param department
	 * @return
	 */
	@RequestMapping("/updateUserManageDept")
	@ResponseBody
	public Map<String, String> updateUseManagerDept(
			@RequestParam(value="uid") Long uid,
			@RequestParam(required=false, value="department") String[] department) {
		TEfunuser user = (TEfunuser) SecurityUtils.getSubject().getSession().getAttribute("loginUser");
		Employee employee = employeeManager.findOneByUID(uid.intValue());
		String cloneEmpNo = "X" + employee.getEmployeeNo();
		
		if(department == null) {
			// 移掉主管權限
			LOG.info("*** no department => remove authorized");
		}
		
		// Delete X employee
		int deleted = employeeManager.delete(uid, "X" + employee.getEmployeeNo(), user.getId());
		LOG.info("Remove clone x " + deleted);
		
		// Insert X employee
		if(department != null) {
			for(String d : department) {
				employee.setEmployeeNo(cloneEmpNo);
				employee.setDepartmentId(Integer.parseInt(d));
				
				Employee clone = employeeManager.addEmployee(employee);								
				manager.addUserEmpMapping(uid, clone.getId());
			}
		}
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("code", "0");
		map.put("msg", "success");
		return map;
	}
}