package com.baplay.web.controller;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.baplay.entity.Company;
import com.baplay.entity.Employee;
import com.baplay.entity.TEfunuser;
import com.baplay.entity.WorkFlowDetail;
import com.baplay.exception.ProcessException;
import com.baplay.service.IFlowManager;

public abstract class BaseController {
	
	protected static final Logger log = LoggerFactory.getLogger(BaseController.class);
	
	@Autowired
	protected IFlowManager flowManager;
	
	protected String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
			if(ip.equals("127.0.0.1")){
				InetAddress inet = null;
				try {
					inet = InetAddress.getLocalHost();
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
				ip = inet.getHostAddress();
			}
		}
		return ip.indexOf(",") > -1 ? ip.substring(0, ip.indexOf(",")) : ip;
	}
	
	protected TEfunuser getLoginUser() {
		return (TEfunuser) SecurityUtils.getSubject().getSession().getAttribute("loginUser");
	}
	
	@SuppressWarnings("unchecked")
	protected Set<String> getUserFormRole() {
		return (Set<String>) SecurityUtils.getSubject().getSession().getAttribute("formRole");
	}
	
	protected Employee getEmployee() {
		return (Employee) SecurityUtils.getSubject().getSession().getAttribute("employee");
	}
	
	@SuppressWarnings("unchecked")
	protected List<Company> getUserCompany() {
		return (List<Company>) SecurityUtils.getSubject().getSession().getAttribute("company");
	}
	
	@SuppressWarnings("unchecked")
	protected Set<Long> getUserDepartmentId() {
		return (Set<Long>) SecurityUtils.getSubject().getSession().getAttribute("department");
	}
	
	@SuppressWarnings("unchecked")
	protected List<Employee> getUserMoreEmployee() {
		return (List<Employee>) SecurityUtils.getSubject().getSession().getAttribute("moreEmployee");
	}
	
	/**
	 * 用戶所有員工鍵值
	 * 
	 * @return
	 */
	protected Set<String> getUserAllEmployeeId() {
		Employee employee = getEmployee();
		List<Employee> more = getUserMoreEmployee();
		more.add(employee);
		
		Set<String> id = new TreeSet<String>();
		
		for(Employee e : more) {
			id.add(e.getId().toString());
		}
		
		return id;
	}
	
	/**
	 * 審放頁流程判斷
	 * 
	 * @param request
	 * @param workFlowDetail
	 */
	protected void flowStep(HttpServletRequest request, WorkFlowDetail workFlowDetail) {
		boolean isVerify = false;
		boolean isCountersign = false;
		boolean isPass = false;
		
		if(workFlowDetail.getCountersignRole() == null) {
			if(workFlowDetail.getStepType() == 3) {
				isPass = true;
			}
			
			if(workFlowDetail.getStepType() == 2) {
				isVerify = true;
			}	
		}
		else {
			isCountersign = true;
		}		
		
		request.setAttribute("isVerify", isVerify);
		request.setAttribute("isCountersign", isCountersign);
		request.setAttribute("isPass", isPass);
	}
	
	/**
	 * 解除鎖定
	 */
	protected void releaseLock(long workFlowDetailId) {
		// Unlock
		try {
			flowManager.lockAbort(workFlowDetailId);
		}
		catch(Exception ex) {
			// Do Nothing
		}
	}
	
	/**
	 * 異常控制
	 * 
	 * @param e
	 * @return
	 */
	@ExceptionHandler(value = {ProcessException.class})
	public void processErrorHandler(ProcessException e) {
		releaseLock(e.getWorkFlowDetailId());
    }
		
	/**
	 * 回傳表單代碼(form.grouo_code) <p>
	 * 非表單交易回傳空字串
	 * 
	 * @return
	 */
	protected abstract String getFormType();
}