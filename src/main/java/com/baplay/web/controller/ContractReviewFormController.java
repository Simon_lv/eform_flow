package com.baplay.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springside.modules.orm.Page;

import com.baplay.constant.Config;
import com.baplay.constant.Form;
import com.baplay.constant.URL;
import com.baplay.dto.Stamp;
import com.baplay.entity.ContractReviewForm;
import com.baplay.entity.ContractReviewMemo;
import com.baplay.entity.TEfunuser;
import com.baplay.entity.WorkFlowDetail;
import com.baplay.form.ContractReviewFormForm;
import com.baplay.service.IAttachmentManager;
import com.baplay.service.ICompanyManager;
import com.baplay.service.IContractReviewFormManager;
import com.baplay.service.IContractReviewMemoManager;
import com.baplay.service.IFlowManager;
import com.baplay.service.IFormAuthManager;
import com.baplay.service.INotifyManager;
import com.baplay.util.DateUtil;
import com.baplay.util.HandleFileUpload;
import com.baplay.util.ReportExcel;
import com.baplay.util.StringUtil;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/contractReviewForm")
public class ContractReviewFormController extends BaseController {

	private static final String FUNCTION_NAME = "合約審閱申請單";
	private static final int PAGE_SIZE = 25;
	@Autowired
	private IContractReviewFormManager contractReviewFormManager;
	@Autowired
	private ICompanyManager companyManager;
	@Autowired
	private IFlowManager flowManager;
	@Autowired
	private IContractReviewMemoManager contractReviewMemoManager;
	@Autowired
	private IAttachmentManager attachmentManager;
	@Autowired
	private INotifyManager notifyManager;
	@Autowired
	private IFormAuthManager formAuthManager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.web.controller.BaseController#getFormType()
	 */
	protected String getFormType() {
		return Form.CONTRACT_REVIEW.getValue();
	}

	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		boolean auth = formAuthManager.formAuth(getFormType(), getUserFormRole());
		request.setAttribute("companys", auth ? companyManager.findAllCompany() : getUserCompany());
		return "/contractReviewForm/init";
	}

	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {		
		if(id != null) {
			ContractReviewForm bf = contractReviewFormManager.findOneById(id);			
			List<ContractReviewMemo> crm = contractReviewMemoManager.findContractReviewMemo(bf.getId());
			request.setAttribute("cp", bf);	
			request.setAttribute("crm", crm);
		}

		return "/contractReviewForm/edit";
	}
	
	@RequestMapping("/get")
	public String get(HttpServletRequest request, Long id) {		
		ContractReviewForm bf = contractReviewFormManager.findOneById(id);		
		List<ContractReviewMemo> crm = contractReviewMemoManager.findContractReviewMemo(bf.getId());
		
		Map<String, Stamp> stamp = flowManager.findFormStamp(bf.getWorkFlowId());
		request.setAttribute("stamp", stamp);
		
		request.setAttribute("cp", bf);
		request.setAttribute("crm", crm);
		return "/contractReviewForm/get";
	}

	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, ContractReviewForm contractReviewForm) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;
				
		String url[] = request.getParameterValues("url");
		String desc[] = request.getParameterValues("fileName");
		String memo[] = request.getParameterValues("memo");

		try {
			if (contractReviewForm != null) {
				if (contractReviewForm.getId() == null || contractReviewForm.getId() <= 0) {
					contractReviewForm = contractReviewFormManager.add(contractReviewForm, url, desc, memo, user, false, getFormType(), getFormType());
					flag = (contractReviewForm != null);
				} else {
					flag = (contractReviewFormManager.update(contractReviewForm, url, desc, memo, user, false, getFormType(), getFormType()) > 0);
				}
			}
		} catch (Exception e) {
			log.error("err:", e);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}

	@RequestMapping("/list")
	public String list(HttpServletRequest request, ContractReviewFormForm contractReviewFormForm) {
		contractReviewFormForm.setPageSize(PAGE_SIZE);
		contractReviewFormForm.setExport(false);
		
		// Auth
		contractReviewFormForm.setFormRole(getUserFormRole());
		contractReviewFormForm.setLoginUser(getLoginUser().getId());
		contractReviewFormForm.setFormType(getFormType());
		contractReviewFormForm.setEmployeeIdList(getUserAllEmployeeId());
		
		Page<ContractReviewForm> page = contractReviewFormManager.list(contractReviewFormForm);
		request.setAttribute("page", page);
		return "/contractReviewForm/list";
	}

	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> deleteCP(Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;

		if (id != null && id > 0) {
			flag = (contractReviewFormManager.delete(id) > 0);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}
	
	@RequestMapping("/deleteAttachment")
	public @ResponseBody Map<String, Object> deleteAttachment(Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;

		if (id != null && id > 0) {
			flag = (attachmentManager.delete(id) > 0);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody HashMap<String, String> handleFileUpload(@RequestParam("file") MultipartFile file,
			@RequestParam(value = "json", required = false, defaultValue = "") String json) throws IOException, Exception {		
		HashMap<String, String> result = new HashMap<String, String>();
		log.debug("[ContractReviewFormController.handleFileUpload][filename=" + file.getOriginalFilename() + "]");
		String url = HandleFileUpload.upload(file, URL.URL_IMG_CONTRACTREVIEWFORM.toString(), URL.FTP_IMG_CONTRACTREVIEWFORM.toString(), Config.UPLOAD_FILE_LIMIT);
		if (StringUtils.isNotEmpty(json)) {
			log.debug("json:" + json);
			JSONObject jsonObj = new JSONObject(json);
			log.debug("jsonObj:" + jsonObj.toString());
		}
		result.put("url", url);
		result.put("description", file.getOriginalFilename());
		return result;
	}

	@RequestMapping("/export")
	public void export(HttpServletRequest request, HttpServletResponse response, ContractReviewFormForm contractReviewFormForm) {
		// Auth
		contractReviewFormForm.setFormRole(getUserFormRole());
		contractReviewFormForm.setLoginUser(getLoginUser().getId());
		contractReviewFormForm.setFormType(getFormType());
		contractReviewFormForm.setEmployeeIdList(getUserAllEmployeeId());				
		
		String[] dataTitles = { "表單編號", "公司名稱", "申請日期", "合約名稱", "簽約對象", "承辦人", "承辦人分機", "送件等級", "送件等級理由", "簽署方式", "特定日期要求", "重點條款說明", "狀態" };
		contractReviewFormForm.setExport(true);
		Page<ContractReviewForm> page = contractReviewFormManager.list(contractReviewFormForm);
		List<Object[]> list = new ArrayList<Object[]>();

		if (page.getResult() != null && !page.getResult().isEmpty()) {
			for (ContractReviewForm nl : page.getResult()) {
				Object[] o = new Object[dataTitles.length];
				o[0] = nl.getSerialNo();
				o[1] = nl.getCompanyName();
				o[2] = DateUtil.getStringOfDate(nl.getRequestDate(), DateUtil.defaultDatePatternStr, null);
				o[3] = nl.getContractTitle();
				o[4] = nl.getContractTarget();
				o[5] = nl.getOrganizerName();
				o[6] = nl.getOrganizerExtension();
				
				switch(nl.getUrgentDispatch()){
				case 1:
					o[7] = "特急";
					break;
				case 2:
					o[7] = "急件";
					break;
				case 3:
					o[7] = "普通";
					break;
				}
				o[8] = nl.getUrgentDispatchReason();
				
				StringBuilder signWay = new StringBuilder(""); 
				
				if(nl.getSignWay().contains("1")){
					signWay.append("經濟部大小章,");
				}
				if(nl.getSignWay().contains("2")){
					signWay.append("授權代表簽名,");
				}
				if(nl.getSignWay().contains("3")){
					signWay.append("發票章");
				}
				o[9] = signWay.toString();				
				o[10] = DateUtil.getStringOfDate(nl.getDefiniteTerm(), DateUtil.defaultDatePatternStr, null);
				o[11] = nl.getKeyPoint();
				o[12] = StringUtil.getFlowStatusDesc(nl.getStatus());				

				list.add(o);
			}
		}

		try {
			ReportExcel.reportExcel(request, response, FUNCTION_NAME + DateUtil.getCurrentTimeStr("yyyyMMddHHmmss"), "", dataTitles,
					list);
		} catch (Exception e) {
			log.error("err:" , e);
		}
	}
	
	/**
	 * 審放頁面
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/process")
	public String process(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		// Flow
		WorkFlowDetail workFlowDetail = flowManager.findFormFlowStep(workFlowDetailId);
		flowStep(request, workFlowDetail);
		
		// Form
		ContractReviewForm form = contractReviewFormManager.findOneByFlow(workFlowDetail.getWorkFlowId());		
		request.setAttribute("form", form);	
		// Memo
		List<ContractReviewMemo> crm = contractReviewMemoManager.findContractReviewMemo(form.getId());
		request.setAttribute("crm", crm);
		// Stamp
		Map<String, Stamp> stamp = flowManager.findFormStamp(form.getWorkFlowId());
		request.setAttribute("stamp", stamp);
		
		return "/contractReviewForm/flow";
	}
	
	/**
	 * 送審
	 * 
	 * @param request
	 * @param paymentForm
	 * @return
	 */
	@RequestMapping("/submit")
	public @ResponseBody Map<String, Object> doSubmit(HttpServletRequest request, ContractReviewForm contractReviewForm) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;
		
		String url[] = request.getParameterValues("url");
		String desc[] = request.getParameterValues("fileName");
		String memo[] = request.getParameterValues("memo");
		
		log.debug("url:" + StringUtils.join(url, ","));
		log.debug("memo" + StringUtils.join(memo, ","));
		
		try {
			if(contractReviewForm != null) {
				if(contractReviewForm.getId() == null || contractReviewForm.getId() <= 0) {
					contractReviewForm = contractReviewFormManager.add(contractReviewForm, url, desc, memo, user, true, getFormType(), getFormType());				
					flag = (contractReviewForm!=null);			
				} 
				else {				
					flag = (contractReviewFormManager.update(contractReviewForm, url, desc, memo, user, true, getFormType(), getFormType()) > 0);			
				}
				
				if(flag) {
					notifyManager.notify(contractReviewForm.getWorkFlowId());
				}	
			}
		}
		catch(Exception e) {
			log.error("err:", e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "送審成功！");			
		} 
		else {
			result.put("code", "0001");
			result.put("message", "送審失敗！");	
		}
		
		return result;
	}
	
	/**
	 * 審核
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/verify")
	public @ResponseBody Map<String, Object> doVerify(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId,
			@RequestParam("url") String url,
			@RequestParam("memo") String memo) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			WorkFlowDetail workFlowDetail = flowManager.findFormFlowStep(workFlowDetailId);
			ContractReviewForm form = contractReviewFormManager.findOneByFlow(workFlowDetail.getWorkFlowId());
			contractReviewMemoManager.add(form.getId(), user.getId(), getFormType(), 
					StringUtils.split(url, ","), StringUtils.split(memo, ","));			
			flowManager.verifyFlow(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			 log.error("err:", e);
			 releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "審核成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "審核失敗！");
		}
		
		return result;
	}
	
	/**
	 * 放行
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/pass")
	public @ResponseBody Map<String, Object> doPass(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.passFlow(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "放行成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "放行失敗！");
		}
		
		return result;
	}
	
	/**
	 * 退件
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/reject")
	public @ResponseBody Map<String, Object> doReject(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId, 
			@RequestParam("memo") String memo) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.rejectFlow(workFlowDetailId, user.getId(), memo);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "退件成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "退件失敗！");
		}
		
		return result;
	}
	
	/**
	 * 取消
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/abort")
	public @ResponseBody Map<String, Object> abort(HttpServletRequest request, 
			@RequestParam("id") Long workFlowDetailId) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.lockAbort(workFlowDetailId);
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "取消成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "取消失敗！");
		}
		
		return result;
	}	
}