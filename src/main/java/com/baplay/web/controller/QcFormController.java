package com.baplay.web.controller;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import com.baplay.constant.Form;
import com.baplay.dto.Stamp;
import com.baplay.entity.Department;
import com.baplay.entity.QcForm;
import com.baplay.entity.TEfunuser;
import com.baplay.entity.WorkFlowDetail;
import com.baplay.form.QcFormForm;
import com.baplay.service.ICompanyManager;
import com.baplay.service.IDepartmentManager;
import com.baplay.service.IEmployeeManager;
import com.baplay.service.IFormAuthManager;
import com.baplay.service.IGameDataManager;
import com.baplay.service.INotifyManager;
import com.baplay.service.IQcFormDetailFieldManager;
import com.baplay.service.IQcFormManager;
import com.baplay.service.IQcFormSchemaManager;
import com.baplay.service.IQcFormTitleFieldManager;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/qcForm")
public class QcFormController extends BaseController {

	private static final int PAGE_SIZE = 25;
	
	@Autowired
	private IQcFormManager qcFormManager;
	@Autowired
	private IQcFormSchemaManager qcFormSchemaManager;
	@Autowired
	private IQcFormTitleFieldManager qcFormTitleFieldManager;
	@Autowired
	private IQcFormDetailFieldManager qcFormDetailFieldManager;
	@Autowired
	private ICompanyManager companyManager;
	@Autowired 
	IEmployeeManager employeeManager;
	@Autowired
	private INotifyManager notifyManager;
	@Autowired
	private IGameDataManager gameDataManager;
	@Autowired
	private IFormAuthManager formAuthManager;
	@Autowired
	private IDepartmentManager departmentManager;
	
	@Override
	protected String getFormType() {
		return Form.QUALITY_CONTROL.getValue();		
	}
	
	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		boolean auth = formAuthManager.formAuth(getFormType(), getUserFormRole());
		request.setAttribute("companys", auth ? companyManager.findAllCompany() : getUserCompany());
		return "/qcForm/init";
	}
		
	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id, Long schemaId, Long company) {
		log.info("edit id=" + id + "," + schemaId + "," + company);
		// 全產品
		request.setAttribute("allGameData", gameDataManager.findAllGameData());
		
		if(id != null || schemaId != null) {
			Long workFlowId = 0L;
			String serialNo = "";
			long companyId = 0;
			long departmentId = 0;
			
			if(schemaId == null) {
				List<QcForm> qcForm = qcFormManager.findAllByFormId(id);
				schemaId = qcForm.get(0).getSchemaId();
				workFlowId = qcForm.get(0).getWorkFlowId();
				serialNo = qcForm.get(0).getSerialNo();
				companyId = qcForm.get(0).getCompanyId();				
			}
			
			if(company != null) {
				Set<Long> userDept = getUserDepartmentId();
				
				for(Long dept : userDept) {
					Department department = departmentManager.findOneById(dept);
					
					if(department.getCompanyId() == company) {
						departmentId = dept;
						break;
					}
				}
			}
			
			request.setAttribute("formSchema", qcFormSchemaManager.findOneById(schemaId));
			request.setAttribute("formTitleField", qcFormTitleFieldManager.findAll(schemaId, id));
			request.setAttribute("formDetailField", qcFormDetailFieldManager.findAll(schemaId, id));
			request.setAttribute("fieldCount", qcFormDetailFieldManager.findAllFieldCount(schemaId));
			request.setAttribute("formId", id);
			request.setAttribute("workFlowId", workFlowId);
			request.setAttribute("serialNo", serialNo);
			request.setAttribute("companyId", companyId);			
			request.setAttribute("departmentId", departmentId);
		}
		else {			
			request.setAttribute("formSchemas", qcFormSchemaManager.findAllFormSchema());
		}
		
		return "/qcForm/edit";
	}
	
	@RequestMapping("/get")
	public String get(HttpServletRequest request, Long id) {
		List<QcForm> qcForm = qcFormManager.findAllByFormId(id);
			
		request.setAttribute("formSchema", qcFormSchemaManager.findOneById(qcForm.get(0).getSchemaId()));
		request.setAttribute("formTitleField", qcFormTitleFieldManager.findAll(qcForm.get(0).getSchemaId(), id));
		request.setAttribute("formDetailField", qcFormDetailFieldManager.findAll(qcForm.get(0).getSchemaId(), id));
//		request.setAttribute("groupCount", qcFormDetailFieldManager.findGroupCount(qcForm.get(0).getSchemaId()));
		request.setAttribute("fieldCount", qcFormDetailFieldManager.findAllFieldCount(qcForm.get(0).getSchemaId()));
		
		Map<String, Stamp> stamp = flowManager.findFormStamp(qcForm.get(0).getWorkFlowId());
		request.setAttribute("stamp", stamp);
		
		request.setAttribute("companyName", companyManager.findOneById(qcForm.get(0).getCompanyId()).getName());
		request.setAttribute("serialNo", qcForm.get(0).getSerialNo());
		
		return "/qcForm/get";
	}
	
	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(QcForm qcForm, 
			String[] subTitle, String[] qcResult, String[] pmResult, String[] remark, 
			String formCode) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;

		try {
			if(qcForm != null){
				if(qcForm.getFormId() == null){
					qcForm = qcFormManager.add(qcForm, subTitle, qcResult, pmResult, remark, user, false, getFormType(), formCode);
					
					flag = (qcForm != null);
				}else{
					flag = (qcFormManager.update(qcForm, subTitle, qcResult, pmResult, remark, user, false, getFormType(), formCode) > 0);
				}
			}			
		} catch (Exception e) {
			log.error("err:", e);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, QcFormForm qcFormForm) {
		qcFormForm.setPageSize(PAGE_SIZE);
		qcFormForm.setExport(false);
		
		// Auth
		qcFormForm.setFormRole(getUserFormRole());
		qcFormForm.setLoginUser(getLoginUser().getId());
		qcFormForm.setFormType(getFormType());
		
		Page<QcForm> page = qcFormManager.list(qcFormForm);
		request.setAttribute("page", page);
		
		return "/qcForm/list";
	}
	
	/**
	 * 審放頁面
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/process")
	public String process(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		// Flow
		WorkFlowDetail workFlowDetail = flowManager.findFormFlowStep(workFlowDetailId);
		flowStep(request, workFlowDetail);
				
		// QcForm
		List<QcForm> qcForm = qcFormManager.findAllByFlow(workFlowDetail.getWorkFlowId());
		
		request.setAttribute("formSchema", qcFormSchemaManager.findOneById(qcForm.get(0).getSchemaId()));
		request.setAttribute("formTitleField", qcFormTitleFieldManager.findAll(qcForm.get(0).getSchemaId(), qcForm.get(0).getFormId()));
		request.setAttribute("formDetailField", qcFormDetailFieldManager.findAll(qcForm.get(0).getSchemaId(), qcForm.get(0).getFormId()));
		request.setAttribute("groupCount", qcFormDetailFieldManager.findGroupCount(qcForm.get(0).getSchemaId()));
		request.setAttribute("fieldCount", qcFormDetailFieldManager.findAllFieldCount(qcForm.get(0).getSchemaId()));
		request.setAttribute("formId", qcForm.get(0).getFormId());
		request.setAttribute("workFlowId", qcForm.get(0).getWorkFlowId());
		// type2		
		Map<String, Stamp> stamp = flowManager.findFormStamp(qcForm.get(0).getWorkFlowId());
		request.setAttribute("stamp", stamp);
		
		request.setAttribute("companyName", companyManager.findOneById(qcForm.get(0).getCompanyId()).getName());
		request.setAttribute("serialNo", qcForm.get(0).getSerialNo());
		
		return "/qcForm/flow";
	}
	
	/**
	 * 送審
	 * 
	 * @param request
	 * @param paymentForm
	 * @return
	 */
	@RequestMapping("/submit")
	public @ResponseBody Map<String, Object> doSubmit(HttpServletRequest request, QcForm qcForm, String formCode) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;	
		
		try {
			if(qcForm != null) {
				String subTitle[] = request.getParameterValues("subTitle");
				String qcResult[] = request.getParameterValues("qcResult");
				String pmResult[] = request.getParameterValues("pmResult");
				String remark[] = request.getParameterValues("remark");
				
				if(qcForm.getFormId() == null) {
					qcForm = qcFormManager.add(qcForm, subTitle, qcResult, pmResult, remark, user, true, getFormType(), formCode);				
					flag = (qcForm!=null);			
				} 
				else {
					flag = (qcFormManager.update(qcForm, subTitle, qcResult, pmResult, remark, user, true, getFormType(), formCode) > 0);			
				}
				
				if(flag) {
					notifyManager.notify(qcForm.getWorkFlowId());
				}
			}
		}
		catch(Exception e) {
			log.error("err:", e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "送審成功！");			
		} 
		else {
			result.put("code", "0001");
			result.put("message", "送審失敗！");	
		}
		
		return result;
	}
	
	/**
	 * 審核
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/verify")
	public @ResponseBody Map<String, Object> doVerify(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId,
			@RequestParam(value="qcResult", required=false, defaultValue="") String qcResult,
			@RequestParam(value="pmResult", required=false, defaultValue="") String pmResult,
			@RequestParam(value="remark", required=false, defaultValue="") String remark,
			@RequestParam(value="fieldId", required=false, defaultValue="") String fieldId) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		log.info("id=" + workFlowDetailId);
		log.info("qcResult=" + qcResult);
		log.info("pmResult=" + pmResult);
		log.info("remark=" + remark);
		log.info("fieldId=" + fieldId);
		try {			
			WorkFlowDetail workFlowDetail = flowManager.findFormFlowStep(workFlowDetailId);
			List<QcForm> formDataList = qcFormManager.findAllByFlow(workFlowDetail.getWorkFlowId());
			
			if(StringUtils.isNotEmpty(pmResult)) {  // PM			
				// Update(2nd)
				if(StringUtils.isNotEmpty(fieldId)) {
					long fid = Long.parseLong(fieldId);
					
					for(QcForm form : formDataList) {
						if(form.getFieldId() == null) {  // title
							continue;
						}
						
						if(form.getFieldId().longValue() == fid) {
							form.setPmResult(Integer.parseInt(pmResult));
							form.setRemark(remark);
							
							qcFormManager.update(form);
							break;
						}
					}
				}
				else {  // 1st
					String[] pm = StringUtils.splitByWholeSeparatorPreserveAllTokens(pmResult, "|");
					String[] rm = StringUtils.splitByWholeSeparatorPreserveAllTokens(remark, "|");
					
					int index = 0;
					
					for(QcForm form : formDataList) {
						if(form.getFieldId() == null) {  // title
							continue;
						}
												
						form.setPmResult(Integer.parseInt(pm[index]));
						form.setRemark(rm[index]);						
						
						if(index >= pm.length - 1) {  // 退件重送時 data 已有 2nd 的欄位
							break;
						}
						
						index++;
					}
					
					qcFormManager.update(formDataList);
				}
			}
			else {  // QC 2nd
				if(StringUtils.isNotEmpty(fieldId) && StringUtils.isNotEmpty(qcResult)) {
					long fid = Long.parseLong(fieldId);
					boolean notNound = true;
					
					for(QcForm form : formDataList) {
						if(form.getFieldId() == null) {  // title
							continue;
						}
						
						if(form.getFieldId().longValue() == fid) {  // 退件重送
							form.setQcResult(Integer.parseInt(qcResult));
							form.setRemark(remark);
							
							qcFormManager.update(form);
							notNound = false;
							break;
						}
					}
					
					if(notNound) {
						QcForm form = new QcForm();
						QcForm current = formDataList.get(0);
						
						// Copy
						form.setFormId(current.getFormId());
						form.setCompanyId(current.getCompanyId());
						form.setSerialNo(current.getSerialNo());
						form.setSchemaId(current.getSchemaId());
						form.setSchemaVersion(current.getSchemaVersion());
						form.setWorkFlowId(current.getWorkFlowId());
						form.setFieldId(fid);
						form.setQcResult(Integer.parseInt(qcResult));
						form.setRemark(remark);
						
						qcFormManager.add(form);
					}					
				}
			}							
			
			flowManager.verifyFlow(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			 log.error("err:", e);
			 releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "審核成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "審核失敗！");
		}
		
		return result;
	}
	
	/**
	 * 放行
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/pass")
	public @ResponseBody Map<String, Object> doPass(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.passFlow(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			 log.error("err:", e);
			 releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "放行成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "放行失敗！");
		}
		
		return result;
	}
	
	/**
	 * 退件
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/reject")
	public @ResponseBody Map<String, Object> doReject(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId, 
			@RequestParam("memo") String memo) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.rejectFlow(workFlowDetailId, user.getId(), memo);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			 log.error("err:", e);
			 releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "退件成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "退件失敗！");
		}
		
		return result;
	}
	
	/**
	 * 取消
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/abort")
	public @ResponseBody Map<String, Object> abort(HttpServletRequest request, 
			@RequestParam("id") Long workFlowDetailId) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.lockAbort(workFlowDetailId);
			flag = true;
		}
		catch(Exception e) {
			 log.error("err:", e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "取消成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "取消失敗！");
		}
		
		return result;
	}	
}