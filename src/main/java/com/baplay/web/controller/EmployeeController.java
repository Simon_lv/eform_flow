package com.baplay.web.controller;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springside.modules.orm.Page;

import com.baplay.entity.Company;
import com.baplay.entity.Department;
import com.baplay.entity.Employee;
import com.baplay.entity.FormRole;
import com.baplay.entity.JobTitle;
import com.baplay.entity.Role;
import com.baplay.entity.TEfunuser;
import com.baplay.form.EmployeeForm;
import com.baplay.form.UserForm;
import com.baplay.service.ICompanyManager;
import com.baplay.service.IDepartmentManager;
import com.baplay.service.IEmployeeManager;
import com.baplay.service.IJobTitleManager;
import com.baplay.service.IPermissionManager;
import com.baplay.util.DateUtil;
import com.baplay.util.ExcelReader;
import com.baplay.util.PrintUtil;
import com.baplay.util.ReportExcel;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/employee")
public class EmployeeController extends BaseController {
	private static final String FUNCTION_NAME = "員工資料";
	private static final int PAGE_SIZE = 25;
	@Autowired
	private IEmployeeManager employeeManager;
	@Autowired
	private IJobTitleManager jobTitleManager;
	@Autowired
	private IDepartmentManager departmentManager;
	@Autowired
	private ICompanyManager companyManager;
	@Autowired
	private IPermissionManager permissionManager;
	
	// 預設密碼
	private final String DEFAULT_PASSWORD = "123456";
	// 客服帳號
	private static Map<String, String> CS_ACCOUNT = new HashMap<String, String>();
	// 特殊帳號
	private static Map<String, String> EMAIL_ACCOUNT = new HashMap<String, String>();
	
	static {
		// 樂服
		CS_ACCOUNT.put("吳秉翰", "cs027");
		CS_ACCOUNT.put("吳菩予", "cs019");
		CS_ACCOUNT.put("林宇驥", "cs025");
		CS_ACCOUNT.put("趙閎宇", "cs032");
		CS_ACCOUNT.put("許迪凱", "cs036");
		CS_ACCOUNT.put("李俊諺", "cs038");
		CS_ACCOUNT.put("彭子函", "cs040");
		CS_ACCOUNT.put("林宜學", "cs043");
//		CS_ACCOUNT.put("胡丞修", "cs044");
		CS_ACCOUNT.put("莊世楷", "cs045");
		CS_ACCOUNT.put("陳舒宇", "cs022");
//		CS_ACCOUNT.put("莊書碩", "cs023");
		CS_ACCOUNT.put("孔正賢", "cs001");
		CS_ACCOUNT.put("謝耀璋", "cs041");
//		CS_ACCOUNT.put("吳忠穎", "cs050");
		CS_ACCOUNT.put("黃舶暉", "cs051");
		CS_ACCOUNT.put("鄭自明", "cs052");
//		CS_ACCOUNT.put("陳浩瑀", "cs053");		
		CS_ACCOUNT.put("呂孟軒", "cs054");
		CS_ACCOUNT.put("賴偉凡", "cs055");
		CS_ACCOUNT.put("吳東益", "cs056");
		CS_ACCOUNT.put("林映男", "cs058");
		CS_ACCOUNT.put("張純維", "cs059");
		CS_ACCOUNT.put("韓文茹", "cs060");
		CS_ACCOUNT.put("許雯順", "cs061");
		// 風靡
		CS_ACCOUNT.put("郭少慶", "cs901");
		// 韓國
		CS_ACCOUNT.put("吳其融", "rex"); 
		CS_ACCOUNT.put("劉宛欣", "amber.liu");
		
		// 帳號轉號
		EMAIL_ACCOUNT.put("jay@evatarkorea.com", "jay.kim");
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.web.controller.BaseController#getFormType()
	 */
	protected String getFormType() {
		return StringUtils.EMPTY;
	}
	
	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		request.setAttribute("allCompany", companyManager.findAllCompany());
		return "/employee/init";
	}
	
	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {				
		if(id != null) {
			Employee employee = employeeManager.findOneById(id);
			request.setAttribute("allJobTitle", jobTitleManager.jobList(employee.getCompanyId()));
			request.setAttribute("allDepartment", departmentManager.deptList(employee.getCompanyId()));
			request.setAttribute("o", employee);
			
			Long userId = permissionManager.findByEmployeeId(id);			
			request.setAttribute("user", permissionManager.getUser(userId));
		}
		else {
			request.setAttribute("allJobTitle", jobTitleManager.findAllJobTitle());
			request.setAttribute("allDepartment", departmentManager.findAllDepartment());
			request.setAttribute("allCompany", companyManager.findAllCompany());
		}
		
		return "/employee/edit";
	}
	
	@RequestMapping("/saveEmployee")
	public @ResponseBody Map<String, Object> save(Employee employee) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		if(employee != null) {
			if(employeeManager.checkEmpNoExist(employee.getCompanyId(), employee.getId(), employee.getEmployeeNo())) {
				result.put("code", "0003");
				result.put("message", "員工編號已存在！");
				return result;
			}
			
			if(employee.getId() == null || employee.getId() <= 0) {
				employee.setCreator(getLoginUser().getId());
				employee = employeeManager.addEmployee(employee);
				
				flag = (employee!=null);				
			} 
			else {
				employee.setModifier(getLoginUser().getId());
				flag = (employeeManager.updateEmployee(employee) > 0);								
			}
			
			// t_efun_user
			if(StringUtils.isNotEmpty(employee.getEmail()) && employee.getQuitTime() == null) {
				String account = employee.getEmail().substring(0, employee.getEmail().indexOf("@"));
				TEfunuser user = permissionManager.findByUserName(account);
				
				if(user == null) {
					log.info("create user default account");
					permissionManager.addUser(newUserForm(employee, account));
				}				
			}
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");			
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");	
		}
		
		return result;
	}		
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, EmployeeForm employeeForm) {
		employeeForm.setPageSize(PAGE_SIZE);
		employeeForm.setExport(false);
		Page<Employee> page = employeeManager.list(employeeForm);
		request.setAttribute("page", page);
		
		return "/employee/list";
	}
	
	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> delete(Long id) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;
		
		if (id != null && id > 0) {
			flag = (employeeManager.delete(id, user.getId()) > 0);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		
		return result;
	}
	
	@RequestMapping("/export")
	public void export(HttpServletRequest request, HttpServletResponse response, EmployeeForm employeeForm) {		
		String[] dataTitles = {"員工姓名/員工編號", "手機", "家裡電話/地址", "分機", "職稱", "部門", "公司", "到職時間", "修改者", "修改時間", "建立者", "建立時間" };  
		employeeForm.setExport(true);
		Page<Employee> page = employeeManager.list(employeeForm);
		List<Object[]> list = new ArrayList<Object[]>();

		if(page.getResult() != null && !page.getResult().isEmpty()) {
			for(Employee ee : page.getResult()) {
				Object[] o = new Object[dataTitles.length];
				o[0] = ee.getName()+"/"+ee.getId();
				o[1] = ee.getMobile();
				o[2] = ee.getPhone()+"/"+ee.getAddress();
				o[3] = ee.getPhoneExt();
				o[4] = ee.getJobTitleName();
				o[5] = ee.getDepartmentName();
				o[6] = ee.getCompanyName();
				o[7] = DateUtil.getStringOfDate(ee.getOnboardTime());
				o[8] = ee.getModifierName();
				o[9] = DateUtil.getStringOfDate(ee.getUpdateTime());
				o[10] = ee.getCreatorName();
				o[11] = DateUtil.getStringOfDate(ee.getCreateTime());
				list.add(o);
			}
		}
		
		try {
			ReportExcel.reportExcel(request, response, FUNCTION_NAME + System.currentTimeMillis(), "", dataTitles, list);
		} 
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 用戶員工資訊
	 * 
	 * @param id
	 * @param request
	 * @return deptId, deptName, jobTitleId, jobTitleName, employeeId, employeeName
	 * @throws Exception
	 */
	@RequestMapping("/userInfo")
	@ResponseBody
	public String userInfo(@RequestParam(required=true) int id,
			HttpServletRequest request) throws Exception {
		PrintUtil.printRequest(this.getClass().getSimpleName(), "userInfo", request);
		String rtn = "";
		Set<Long> userDept = getUserDepartmentId();
		
		for(Long dept : userDept) {
			Department department = departmentManager.findOneById(dept);
			
			if(department.getCompanyId() == id) {
				rtn = String.format("%s,%s,%s", dept, department.getName(), getEmployee(id));
				break;
			}
		}
				
		log.info("return " + rtn);
		return rtn;
	}
	
	private String getEmployee(int compnayId) {
		List<Employee> more = getUserMoreEmployee();
		Employee main = super.getEmployee();
		
		if(more.isEmpty() || main.getCompanyId() == compnayId) {			
			return String.format("%s,%s,%s,%s", main.getJobTitleId(), main.getJobTitleName(), main.getId(), main.getName());
		}
		else {
			for(Employee e : more) {
				if(e.getCompanyId() == compnayId) {
					return String.format("%s,%s,%s,%s", e.getJobTitleId(), e.getJobTitleName(), e.getId(), e.getName());
				}
			}
		}
		
		return null;
	}
	
	/**
	 * 員工資訊
	 * 
	 * @param id
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/employeeInfo")
	@ResponseBody
	public String employeeInfo(@RequestParam(required=true) Long id,
			HttpServletRequest request) throws Exception {
		Employee employee = employeeManager.findOneById(id);
		
		return String.format("%s,%s,%s,%s,%s,%s", 
				employee.getDepartmentId(), employee.getDepartmentName(),
				employee.getJobTitleId(), employee.getJobTitleName(), 
				employee.getId(), employee.getName());
	}
	
	/**
	 * 公司員工
	 * 
	 * @param id
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/companyEmployee")
	@ResponseBody
	public List<Employee> companyEmployee(@RequestParam(required=true) Long id,
			HttpServletRequest request) throws Exception {
		List<Employee> list = employeeManager.findAllEmployee(id.intValue());				
		
		return list;
	}
	
	@RequestMapping("/importExcel")
	public @ResponseBody Map<String, Object> importExcel(HttpServletRequest request, 
			MultipartFile excelFile) {
		Map<String, Object> result = Maps.newHashMap();
		String realPath = System.getProperty("java.io.tmpdir");
		log.info("realPath=" + realPath);		
		StringBuilder errMsg = new StringBuilder();
				
		if(excelFile != null && !excelFile.isEmpty()) {
			String type = excelFile.getContentType();
			log.info("File ContentType =" + type);
			String ext = null;
			
			if(type.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
				ext = ".xlsx";
			} 
			else if(type.equals("application/vnd.ms-excel")) {
				ext = ".xls";				
			} 
			else if(type.equals("application/octet-stream")) {
				ext = ".xls";
			} 
			else {
				log.error("文件類型不正確");
				result.put("code", "1001");
				result.put("message", "文件類型不正確！");			
			}
			
			if(StringUtils.isNotEmpty(ext)) {				
				try {
					ExcelReader excelReader = new ExcelReader(excelFile.getInputStream(), ext);
					Map<String, List<Object[]>> sheets = excelReader.readExcelContent();
					List<Employee> employeeList = employeeManager.findAllEmployee();
					List<Company> companyList = companyManager.findAllCompany();	
					List<TEfunuser> userList = permissionManager.getUsers(null);
					Map<Long, List<Long>> mappingList = permissionManager.allUserEmpMapping(userList);
					
					List<FormRole> formRoleList = permissionManager.getFormRoles(1);
					List<Role> roleList = permissionManager.getRoles(1);					
					
					Map<String, Employee> empMap = new HashMap<String, Employee>();
					Map<String, Company> comMap = new HashMap<String, Company>();
					Map<String, Role> roleMap = new HashMap<String, Role>();
					Map<String, FormRole> formRoleMap = new HashMap<String, FormRole>();
					
					// Map
					for(Employee employee : employeeList) {
						String key = StringUtils.isEmpty(employee.getEmployeeNo())
								? employee.getMobile()
								: employee.getEmployeeNo();
						empMap.put(key, employee);
					}
					
					for(Company company : companyList) {
						// Name, No
						Map<String, String> jobMap = new HashMap<String, String>();
						Map<String, String> dptMap = new HashMap<String, String>();
						// Dept
						List<Department> dptList = departmentManager.findAllDepartment(company.getId());
						
						for(Department dpt : dptList) {
							dptMap.put(dpt.getName(), dpt.getDeptNo());							
						}
						
						company.setDptMap(dptMap);
												
						// Job
						List<JobTitle> titleList = jobTitleManager.findAllJobTitle(company.getId());
						
						for(JobTitle job : titleList) {
							jobMap.put(job.getName().toUpperCase(), job.getTitleNo());							
						}
						
						company.setJobMap(jobMap);
						
						comMap.put(company.getName(), company);
					}
					
					for(Role role : roleList) {
						roleMap.put(role.getName(), role);
					}
					
					for(FormRole formRole : formRoleList) {
						formRoleMap.put(formRole.getName(), formRole);
					}
							
					for(Entry<String, List<Object[]>> sheet : sheets.entrySet()) {
						String sheetName = sheet.getKey();
						
						if(!StringUtils.equals(sheetName, "人事資料(更新)")) {
							log.info("skip sheet " + sheetName);
							continue;
						}
						
						List<Object[]> rows = sheet.getValue();
						// row 1 header
						log.info("parse " + sheetName + " x " + rows.size());
						
						Map<Long, Employee> exClone = new HashMap<Long, Employee>();
						Map<Long, Employee> csClone = new HashMap<Long, Employee>();
						Map<String, Employee> xClone = new HashMap<String, Employee>();
						Map<Long, Employee> tomatoClone = new HashMap<Long, Employee>();
						
						List<String> report = new LinkedList<String>();
						
						for(Object[] row : rows) {							
							convert(row, empMap, comMap, userList, mappingList, roleMap, formRoleMap, 
									exClone, csClone, xClone, tomatoClone, report);
						}
						
						log.info(StringUtils.join(report, "\n"));
						
						// 子公司 clone 行政部(財會)
						if(!exClone.isEmpty()) {												
							log.info("clone ex staff...");
							cloneExStaff(exClone);
						}
						
						if(!csClone.isEmpty()) {
							// 總公司 clone 客服部
							log.info("clone cs staff...");
							cloneCsStaff(csClone);
						}
						// 第一次上線用，之後用管理介面操作
						/*
						// 廣州部門 clone 主管
						log.info("clone supervisor for cn...");
						cloneCnSupervisor();
						// 總公司 clone 跨部門主管
						log.info("clone supervisor for cross dept...");
						cloneSupervisor(xClone);
						*/
						// 牛番茄 TW clone HK
						if(!tomatoClone.isEmpty()) {
							log.info("clone tomato staff...");
							cloneTomatoStaff(tomatoClone);
						}
					}												
					
					if(StringUtils.isEmpty(errMsg.toString())) {
						result.put("code", "1000");
						result.put("message", "上傳成功！");
					}
					else {
						result.put("code", "1002");
						result.put("message", "上傳成功但有錯誤\n" + errMsg);											
					}					
				} 
				catch(Exception e) {
					log.error("文件上傳失敗", e);
					result.put("code", "1002");
					result.put("message", "文件上傳失敗!" + e.getLocalizedMessage());					
				} 	
			}
		}
		else {
			result.put("code", "1003");
			result.put("message", "文件不存在！");
		}
		
		return result;
	}
	
	private void convert(Object[] row, 
			Map<String, Employee> empMap, Map<String, Company> comMap, 
			List<TEfunuser> userList, Map<Long, List<Long>> mappingList,
			Map<String, Role> roleMap, Map<String, FormRole> formRoleMap,
			Map<Long, Employee> exClone, Map<Long, Employee> csClone, 
			Map<String, Employee> xClone, Map<Long, Employee> tomatoClone, 
			List<String> report) throws Exception {
		TEfunuser login_user = getLoginUser();
		log.info("process " + StringUtils.join(row, ","));
		
		String empNo = "";
		String dptName = "";
		String empName = "";
		String title = "";
		String onboard = "";
		String offboard = "";
		String phone = "";
		String mobile = "";
		String mobile2 = "";
		String address = "";
		String email = "";
		String engName = "";
		
		// 員工編號, 部, 姓名, 英文名, 職位【對內】, 到職日, 離職日, 家庭聯繫電話, 個人聯繫電話, 個人聯繫電話, 戶口所在地(身分證地址)
		for(int i = 0; i < row.length; i++) {
			String raw = row[i] instanceof Date ? DateUtil.getStringOfDate((Date) row[i], "yyyy/MM/dd", null) : row[i].toString();
			//log.info(String.format("%s=%s", i, row.get(i)));
			
			switch(i) {
				case 0:  // 序號
					// Do Nothing
					break;
				case 1:  // 員工編號
					if(StringUtils.isNotEmpty(raw)) {
						empNo = raw;						
					}										
					
					break;
				case 2:  // 部
					if(StringUtils.equals(raw, "部")) {
						log.info("skip empty line...");
						return;
					}
					
					dptName = raw;
					
					break;
				case 3:  // 姓名
					if(StringUtils.isEmpty(raw) || StringUtils.equals(raw, "姓名")) {
						log.info("skip empty line...");
						return;
					}
					
					empName = raw;
					
					break;
				case 4:  // 英文名
					engName = raw.toLowerCase().replaceAll("\\(.*\\)", "");  // rip (...)
					engName = raw.toLowerCase().replaceAll("\\s", ".");  // space to dot
					break;
				case 5:  // 職位【對內】
					title = raw.toUpperCase();
					break;
				case 6:  // 到職日
					onboard = raw;
					break;
				case 7:  // 離職日
					offboard = raw;				
					break;
				case 8:  // 家庭聯繫電話
					phone = raw;				
					break;
				case 9:  // 個人聯繫電話
					mobile = raw;				
					break;
				case 10:  // 大陸手機
					mobile2 = raw;				
					break;	
				case 11:  // 戶口所在地(身分證地址)
					address = raw;				
					break;
				case 12:  // Email
					email = raw.toLowerCase();  // 小寫				
					break;	
			}								
		}
		
		// Department
		Company company = getCompany(comMap, empNo, email, dptName);
		
		if(company == null) {
			log.warn("company not found...skip");
			return;
		}
		
		log.info(String.format("compamy=[%s][%s]", company.getId(), company.getName()));
		Map<String, String> dptMap = company.getDptMap();
		Department department = null;
		
		if(dptMap.containsKey(dptName)) {
			department = departmentManager.findOneByDeptNo(dptMap.get(dptName), company.getId());
		}
		else {
			log.info("New department..." + dptName);
			department = new Department();
			boolean isTop = false;
			String dptNo = "";
			
			if(StringUtils.equals(dptName, "總管理處") 
					|| StringUtils.equals(dptName, "總管理處(香港)")
					|| StringUtils.equals(dptName, "總管理處(廣州)")
					|| StringUtils.equals(dptName, "總管理處(韓國)")) {
				isTop = true;
				dptNo = getNo(dptMap, "A");							
			}
			else {
				dptNo = getNo(dptMap, "C");							
			}
			
			department.setDeptNo(dptNo);
			department.setName(dptName);
			department.setDeptLevel(isTop ? 1 : 3);
			
			if(!isTop) {
				Department top = null;
				
				if(StringUtils.contains(dptName, "香港")) {
					top = departmentManager.findOneByFullName("總管理處(香港)", company.getId());
				}
				else if(StringUtils.contains(dptName, "廣州")) {
					top = departmentManager.findOneByFullName("總管理處(廣州)", company.getId());
				}
				else if(StringUtils.contains(dptName, "韓國")) {
					top = departmentManager.findOneByFullName("總管理處(韓國)", company.getId());
				}
				else {
					top = departmentManager.findOneByFullName("總管理處", company.getId()); 
				}
				
				if(top == null) {
					top = new Department();
					String topNo = getNo(dptMap, "A");
					
					top.setDeptNo(topNo);
					
					String topName = null;
					
					if(StringUtils.contains(dptName, "香港")) {
						topName = "總管理處(香港)";
					}
					else if(StringUtils.contains(dptName, "廣州")) {
						topName = "總管理處(廣州)";
					}
					else if(StringUtils.contains(dptName, "韓國")) {
						topName = "總管理處(韓國)";
					}
					else {
						topName = "總管理處"; 
					}
					
					top.setName(topName);
					top.setCompanyId(company.getId());
					top.setDeptLevel(1);
					top.setCreator(login_user.getId());
					top = departmentManager.add(top);
					
					dptMap.put(topName, topNo);
				}
												
				department.setParentId(top.getId().intValue());				
			}
			
			department.setCreator(login_user.getId());
			department.setCompanyId(company.getId());
			
			department = departmentManager.add(department);
			dptMap.put(dptName, dptNo);
			// 客服部
			if(StringUtils.equals(dptName, "客服部")) {
				Company evatar = getCompany(comMap, null, email, dptName);
				Map<String, String> evatarDptMap = evatar.getDptMap();
				// Clone 到總公司
				Department cs = new Department();
				cs.setCompanyId(evatar.getId());
				cs.setParentId(1);
				
				String csDeptNo = getNo(evatarDptMap, "C");
				cs.setDeptNo(csDeptNo);
				
				cs.setDeptLevel(3);
				cs.setName(dptName);
				cs.setCreator(department.getCreator());
				
				departmentManager.add(cs);
				evatarDptMap.put(dptName, csDeptNo);
			}
		}
		
		// Employee
		Employee employee = null;
		
		if(StringUtils.isEmpty(empNo)) {
			employee = empMap.get(mobile);
		}
		else {			
			employee = empMap.get(empNo);
			// TODO 樂服跟伊凡達重號
			if(employee != null && !StringUtils.equals(employee.getName(), empName)) {
				throw new Exception(String.format("員工編號重複[%s][%s][%s]", empNo, employee.getName(), empName));
			}
		}
				
		boolean isNew = false;
		
		if(employee == null) {
			isNew = true;
			log.info("New employee: " + empName);
			employee = new Employee();
			employee.setCreator(login_user.getId());
			employee.setCompanyId(company.getId());
		}
		else {
			employee.setModifier(login_user.getId());
		}
		
		employee.setEmployeeNo(empNo);
		employee.setName(empName);
		
		Map<String, String> jobMap = company.getJobMap();
		JobTitle job = null;
		
		if(jobMap.containsKey(title)) {
			job = jobTitleManager.findOneByNo(jobMap.get(title), company.getId());
		}
		else {
			log.info("New job: " + title);
			job = new JobTitle();
			
			String jobNo = getNo(jobMap, "D");
			
			job.setTitleNo(jobNo);			
			job.setName(title);
			job.setCreator(login_user.getId());
			job.setCompanyId(company.getId());
			
			job = jobTitleManager.add(job);
			jobMap.put(title, jobNo);
		}
		
		employee.setDepartmentId(department.getId().intValue());
		employee.setDepartmentName(dptName);
		employee.setJobTitleId(job.getId().intValue());
		employee.setJobTitleName(title);
		
		String date = convertDate(onboard);
		
		if(StringUtils.isNotEmpty(date)) {
			employee.setOnboardTime(DateUtil.parse(date, DateUtil.defaultDatePatternStr));
		}
		
		if(StringUtils.isNotEmpty(offboard)) {
			date = convertDate(offboard);
			
			if(StringUtils.isNotEmpty(date)) {
				employee.setQuitTime(DateUtil.parse(date, DateUtil.defaultDatePatternStr));
			}
		}
		
		if(StringUtils.isNotEmpty(phone)) {
			employee.setPhone(phone);
		}
		
		if(StringUtils.isNotEmpty(mobile)) {
			employee.setMobile(mobile);
		}
		
		if(StringUtils.isNotEmpty(mobile2)) {
			employee.setMobile2(mobile2);
		}
		
		if(StringUtils.isNotEmpty(address)) {
			employee.setAddress(address);
		}
		
		if(StringUtils.isNotEmpty(email)) {
			employee.setEmail(email);
		}
		
		if(StringUtils.isNotEmpty(engName)) {
			employee.setEnglishName(engName);
		}
		
		if(isNew) {
			employee = employeeManager.addEmployee(employee);
			log.info("add employee=" + ToStringBuilder.reflectionToString(employee));					
		}
		else {
			employeeManager.updateEmployee(employee);
			log.info("update employee=" + ToStringBuilder.reflectionToString(employee));			
			// 離職員工由排程刪除帳號
		}
		
		// t_erp_user(在職+有email)
		if(StringUtils.isNotEmpty(email) && 
				(employee.getQuitTime() == null || DateUtil.compareDateIgnoreTime(new Date(), employee.getQuitTime()) == -1)) {
			if(email.indexOf("@") == -1) {
				log.info("no email...skip user data");
				return;
			}
			
			final String account;
			final String userName;
			String emailName = email.substring(0, email.indexOf("@")).toLowerCase().replaceAll("^\\s+", "").replaceAll("\\s+$", ""); // left,right trim
			
			if(StringUtils.equals(emailName, "service") || StringUtils.equals(emailName, "cs_ms")) {  // 客服
				account = getCsAccount(employee.getName());
				
				if(StringUtils.isEmpty(account)) {
					log.info("cs not found...skip user data");
					return;
				}
			}
			else {
				// 英文名稱重複帳號
				if(EMAIL_ACCOUNT.containsKey(email)) {
					account = EMAIL_ACCOUNT.get(email);
				}
				else {
					account = emailName;
				}
			}
			
			userName = account + empName;
			
			TEfunuser user = (TEfunuser) CollectionUtils.find(userList, new Predicate() {			    
				@Override
				public boolean evaluate(Object obj) {
					TEfunuser user = (TEfunuser) obj;
					// account + employee name
					return StringUtils.equals(userName, user.getLoginAccount() 
							+ (StringUtils.isEmpty(user.getUserName()) ? "" : user.getUserName().replaceFirst("\\(.*\\)", "")));  // Remove (X)
				}
			});
			
			
			if(user == null) {
				UserForm userForm = newUserForm(employee, account);
				
				// User Role
				userAuth(userForm, employee, roleMap, formRoleMap, xClone);
				
				user = permissionManager.addUser(userForm);
				// Update list
				user.setUserName(empName); // 自定義欄位
				userList.add(user);
				mappingList.put(user.getId(), userForm.getEmployeeMapping());								
				
				report.add("add user=" + account);
				
				// 總公司行政部(財會)
				if(employee.getCompanyId() == 1 && StringUtils.equals(dptName, "總管理處(財會)")) {
					employee.setJobTitleName(title);
					// account, employee
					exClone.put(user.getId(), employee);
				}
				
				// 客服
				if(StringUtils.equals(dptName, "客服部")) {
					employee.setJobTitleName(title);
					employee.setDepartmentName(dptName);
					// account, employee
					csClone.put(user.getId(), employee);
				}
				
				// 牛番茄TW
				if(employee.getCompanyId() == 6) {
					employee.setJobTitleName(title);
					employee.setDepartmentName(dptName);
					// account, employee
					tomatoClone.put(user.getId(), employee);
				}
			}
			else {
				// UserEmployee mapping
				List<Long> employeeMapping = mappingList.get(user.getId());
				log.info("dump mapping=" + employeeMapping);
				
				if(!employeeMapping.contains(employee.getId())) {
					permissionManager.addUserEmpMapping(user.getId(), employee.getId());
					// update list
					employeeMapping.add(employee.getId());
					
					report.add(String.format("add user[%s] employee[%s][%s] mapping", user.getId(), employee.getId(), employee.getName()));
				}
			}
		}
	}
	
	/**
	 * 民國年轉西元年
	 * 
	 * @param rocDate
	 * @return
	 */
	private String convertDate(String rocDate) {
		if(rocDate.indexOf("(") != -1) {  // yyy/MM/dd(....)  drop (...)
			rocDate = rocDate.substring(0, rocDate.indexOf("(") + 1);
		}
		
		StringBuilder sb = new StringBuilder();
		String[] tmp = rocDate.split("[^\\d]+");  // 不管格式拆數字
		
		if(tmp.length < 3) {
			return null;
		}		
		
		String year = tmp[0];
		String month = tmp[1];
		String date = tmp[2];
		boolean isROC = Integer.parseInt(year) < 1911;		
		
		sb.append(Integer.parseInt(year) + (isROC ? 1911 : 0)).append("-").append(month).append("-").append(date);
		
		log.info(String.format("Date %s to %s", rocDate, sb.toString()));
		return sb.toString(); 
	}
	
	private Company getCompany(Map<String, Company> comMap, String empNo, String email, String deptName) {
		// 伊凡達(1CHK), 牛番茄(T), 牛番茄香港(TK), 提樂(L), 風靡(F), 樂服(客服部)
		String keyword = "";
		boolean local = true;
		
		if(StringUtils.isBlank(empNo)) {
			if(StringUtils.isEmpty(email)) {
				log.warn("Empty email...skip");
				return null;
			}
			/*
			if(StringUtils.contains(email, "evatarkorea")) {  // 韓辦有TW,KR
				keyword = "伊凡達.*\\(韓國\\).*";	
				local = false;
			}
			else */if(StringUtils.contains(email, "fameapp")) {
				keyword = "風靡.*";
			}
			else if(StringUtils.contains(email, "quantum123")) {
				keyword = "牛番茄.*";
			}
			else if(deptName.indexOf("香港") != -1) {
				keyword = deptName.indexOf("遊戲") == -1 ? "伊凡達.*\\(香港\\)股份.*" : "伊凡達.*\\(香港\\)遊戲.*";	
				local = false;
			}
			else {
				keyword = "伊凡達.*";
			}
		}
		else if(empNo.matches("^[0-9]+$")) {
			if(StringUtils.contains(deptName, "客服部")) {
				keyword = "樂服.*";
			}			
			else {
				keyword = "伊凡達.*";
			}			
		}
		else if(empNo.matches("^C.*$")) {
			keyword = "伊凡達.*";
		}
		else if(empNo.matches("^HG.*$")) {
			keyword = "伊凡達.*\\(香港\\)遊戲.*";
			local = false;
		}
		else if(empNo.matches("^H.*$")) {
			keyword = "伊凡達.*\\(香港\\)股份.*";
			local = false;
		}
		else if(empNo.matches("^K.*$")) {
			/*
			keyword = "伊凡達.*\\(韓國\\).*";	
			local = false;
			*/
			keyword = "伊凡達.*";
		}
		else if(empNo.matches("^TK.*$")) {
			keyword = "牛番茄.*\\(香港\\).*";
			local = false;
		}
		else if(empNo.matches("^T.*$")) {
			keyword = "牛番茄.*";
		}/*
		else if(empNo.matches("^L.*$")) {
			keyword = "提樂.*";
		}*/
		else if(empNo.matches("^F.*$")) {
			keyword = "風靡.*";
		}
		else if(empNo.matches("^J.*$")) {
			keyword = "鼎客.*";
		}
		else {
			log.warn("Unknown employee no format:" + empNo);			
		}
		
		Company com = null;
		
		for(Entry<String, Company> e : comMap.entrySet()) {
			String name = e.getKey();
			
			// 排掉福委會
			if(name.indexOf("福利委員會") != -1) {
				continue;
			}			
			
			if(local) {
				if(name.matches(keyword) && !name.matches(".*\\(.*\\).*")) {					
					com = e.getValue();
					break;
				}
			}
			else {
				if(name.matches(keyword)) {
					com = e.getValue();
					break;
				}
			}
		}
		
		if(com == null) {
			log.warn("Unknown employee no format:" + empNo + ",keyword=" + keyword);			
		}				
		
		return com;
	}
	
	private String getNo(Map<String, String> map, String prefix) {
		int index = 1;
		Set<String> noList = new TreeSet<String>();

		for(Entry<String, String> entry : map.entrySet()) {
			String no = entry.getValue();
			
			if(no.startsWith(prefix)) {
				noList.add(no);
			}
		}
		
		if(!noList.isEmpty()) {
			log.info("current noList: " + noList);
			
			String[] no = new String[noList.size()];
			noList.toArray(no);
			index = Integer.parseInt(no[no.length - 1].substring(1)) + 1;  // A01
		}
		
		NumberFormat formatter = new DecimalFormat("00");
		
		return prefix + formatter.format(index);
	}
	
	private UserForm newUserForm(Employee employee, String account) {
		List<Long> employeeMapping = new LinkedList<Long>();
		employeeMapping.add(employee.getId());
		
		UserForm form = new UserForm();
		form.setLoginAccount(account);
		form.setPassword(DEFAULT_PASSWORD);
		form.setEmployeeMapping(employeeMapping);
		form.setCreator(getLoginUser().getId());
		form.setDuplicateCheck(false);
							
		return form;
	}
	
	/**
	 * 用戶權限
	 * 
	 * @param userForm
	 * @param role
	 * @param formRoleList
	 */
	private void userAuth(UserForm userForm, List<FormRole> formRoleList, Role... roles) {
		if(roles.length > 0) {
			userForm.setRoleId1(roles[0].getId());
			
			if(roles.length > 1) {
				userForm.setRoleId2(roles[1].getId());
			}			
		}
		
		if(!formRoleList.isEmpty()) {
			
			if(formRoleList.size() > 0) {
				userForm.setFormRoleId1(formRoleList.get(0).getId());
			}
			
			if(formRoleList.size() > 1) {
				userForm.setFormRoleId2(formRoleList.get(1).getId());
			}
			
			if(formRoleList.size() > 2) {
				userForm.setFormRoleId3(formRoleList.get(2).getId());
			}
			
			if(formRoleList.size() > 3) {
				userForm.setFormRoleId4(formRoleList.get(3).getId());
			}
			
			if(formRoleList.size() > 4) {
				userForm.setFormRoleId5(formRoleList.get(4).getId());
			}
			
			if(formRoleList.size() > 5) {
				userForm.setFormRoleId6(formRoleList.get(5).getId());
			}
		}
	}
	
	/**
	 * 用戶權限 Korea
	 * 
	 * @param userForm
	 * @param employee
	 * @param roleMap
	 * @param formRoleMap
	 */
	private void userAuthKorea(UserForm userForm, Employee employee,
			Map<String, Role> roleMap, Map<String, FormRole> formRoleMap) {
		Role role = roleMap.get("韓國辦公室");
		Role role2 = null;
		List<FormRole> formRoleList = new LinkedList<FormRole>();
		String key = "";
		
		if(StringUtils.equals(employee.getJobTitleName(), "總經理")) {			
			// 韓辦掛在伊凡達TW下
			key = "部門主管";			
		}
		
		if(StringUtils.contains(employee.getJobTitleName(), "客服")) {
			role2 = roleMap.get("客服");
			key = "客服";			
		}
		
		if(StringUtils.equals(employee.getJobTitleName(), "營運總監")) {
			key = "營運主管";			
		}
		
		if(StringUtils.equals(employee.getJobTitleName(), "技術長")) {
			key = "技術主管";			
		}
		
		if(StringUtils.equals(employee.getJobTitleName(), "行銷經理")) {
			key = "行銷主管";			
		}
		
		if(StringUtils.contains(employee.getJobTitleName(), "總務")) {
			key = "總務";	
			
			// 韓辦限定
			userForm.setFormRoleDeptLimit(2);
		}
		
		if(StringUtils.contains(employee.getJobTitleName(), "產品")) {
			key = "產品";			
		}
				
		if(formRoleMap.containsKey(key)) {
			formRoleList.add(formRoleMap.get(key));
		}
		
		// 行政專員
		if(StringUtils.equals(employee.getJobTitleName(), "行政專員")) {
			formRoleList.add(formRoleMap.get("總務"));
			formRoleList.add(formRoleMap.get("出納"));
			formRoleList.add(formRoleMap.get("人資"));						
			formRoleList.add(formRoleMap.get("會計"));
			formRoleList.add(formRoleMap.get("會計專員"));
			
			// 韓辦限定
			userForm.setFormRoleDeptLimit(2);
		}		
		
		// jay
		if(StringUtils.equals(userForm.getLoginAccount(), "jay")) {
			userForm.setLoginAccount("jay.kim");
		}
				
		if(role2 == null) {
			userAuth(userForm, formRoleList, role);
		}
		else {
			userAuth(userForm, formRoleList, role, role2);
		}
		
		
		return;
	}
	
	/**
	 * 用戶權限 Tomato
	 * 
	 * @param userForm
	 * @param employee
	 * @param roleMap
	 * @param formRoleMap
	 */
	private void userAuthTomato(UserForm userForm, Employee employee,
			Map<String, Role> roleMap, Map<String, FormRole> formRoleMap) {
		Role role = roleMap.get("牛番茄");
		List<FormRole> formRoleList = new LinkedList<FormRole>();
		
		if(StringUtils.equals(employee.getJobTitleName(), "總經理")) {
			if(formRoleMap.containsKey("總經理")) {
				formRoleList.add(formRoleMap.get("總經理"));
			}
		}		
		
		userAuth(userForm, formRoleList, role);
		
		return;
	}
	
	/**
	 * 用戶權限 子公司
	 * 
	 * @param userForm
	 * @param employee
	 * @param roleMap
	 * @param formRoleMap
	 */
	private void userAuthSub(UserForm userForm, Employee employee,
			Map<String, Role> roleMap, Map<String, FormRole> formRoleMap) {
		String roleName = "";
		long companyKey = employee.getCompanyId();
		
		if(companyKey == 2) {
			roleName = "風靡";
		}
		else if(companyKey == 11) {
			roleName = "鼎客";
		}		
		
		if(StringUtils.isEmpty(roleName)) {
			log.warn(roleName + " role not found");
			return;
		}
		
		Role role = roleMap.get(roleName);
		List<FormRole> formRoleList = new LinkedList<FormRole>();
		userAuth(userForm, formRoleList, role);
	}
			
	/**
	 * Clone 行政部員工
	 * 
	 * @param companyId
	 * @param uid
	 * @param employee
	 */
	private void cloneEmployee(Long companyId, Long uid, Employee employee) {
		List<Department> deptList = departmentManager.findAllDepartment(companyId);
		List<JobTitle> jobList = jobTitleManager.findAllJobTitle(companyId);
		Department top = null;
		Department ex = null;
		int deptNo = deptList.isEmpty() ? 1 : Integer.parseInt(deptList.get(deptList.size() - 1).getDeptNo().substring(1)) + 1;
		int jobNo = jobList.isEmpty() ? 1 : Integer.parseInt(jobList.get(jobList.size() - 1).getTitleNo().substring(1)) + 1;
		NumberFormat formatter = new DecimalFormat("00");
		boolean isHK = companyId == 5 || companyId == 7;
		
		if(deptList.isEmpty()) {
			// 空殼
			top = new Department();
			
			top.setDeptNo("A01");
			top.setName("總管理處" + (isHK ? "(香港)" : ""));
			top.setCompanyId(companyId);
			top.setDeptLevel(1);
			top.setCreator(employee.getCreator());
			top = departmentManager.add(top);
			
			ex = new Department();
			ex.setDeptNo("C01");
			ex.setName("總管理處(財會)" + (isHK ? "(香港)" : ""));
			ex.setCompanyId(companyId);
			ex.setDeptLevel(3);
			ex.setParentId(top.getId().intValue());
			ex.setCreator(employee.getCreator());
			ex = departmentManager.add(ex);
		}
		else {
			Optional<Department> dept = deptList.stream().filter(d -> d.getName().equals(isHK ? "總管理處(財會)(香港)" : "總管理處(財會)")).findAny();
			
			if(dept.isPresent()) {
				ex = dept.get();
			}
			else {
				top = deptList.get(0);
				
				ex = new Department();
				ex.setDeptNo("C" + formatter.format(deptNo));
				ex.setName("總管理處(財會)" + (isHK ? "(香港)" : ""));
				ex.setCompanyId(companyId);
				ex.setDeptLevel(3);
				ex.setParentId(top.getId().intValue());
				ex.setCreator(employee.getCreator());
				ex = departmentManager.add(ex);						
			}			
		}
				
		Optional<JobTitle> title = jobList.stream().filter(j -> j.getName().equals(employee.getJobTitleName())).findAny();
		JobTitle job = null;
		
		if(title.isPresent()) {
			job = title.get();
		}
				
		if(job == null) {
			job = new JobTitle();
			
			job.setTitleNo("D" + formatter.format(jobNo));			
			job.setCompanyId(companyId);
			job.setName(employee.getJobTitleName());
			job.setCreator(employee.getCreator());
			
			job = jobTitleManager.add(job);			
		}
				
		employee.setCompanyId(companyId);
		employee.setEmployeeNo("X" + employee.getEmployeeNo().replaceFirst("X", ""));  // 吃掉前一次clone產生的X
		employee.setDepartmentId(ex.getId().intValue());		
		employee.setJobTitleId(job.getId().intValue());						
		
		Employee clone = employeeManager.addEmployee(employee);
		permissionManager.addUserEmpMapping(uid, clone.getId());		
	}
	
	/**
	 * 行政部門分身
	 * 
	 * @param exClone
	 */
	private void cloneExStaff(Map<Long, Employee> exClone) {
		for(Entry<Long, Employee> entry : exClone.entrySet()) {
			Long uid = entry.getKey();
			Employee employee = entry.getValue();
			
			// 牛番茄6
			cloneEmployee(6L, uid, employee);
			// 牛番茄HK5
			cloneEmployee(5L, uid, employee);
			// 樂服3
			cloneEmployee(3L, uid, employee);
			// 傾國10
			cloneEmployee(10L, uid, employee);
			// 鼎客9
			cloneEmployee(9L, uid, employee);
			// 風靡2
			cloneEmployee(2L, uid, employee);
			// 香港商伊凡達台灣分公司7
			cloneEmployee(7L, uid, employee);
		}		
	}
	
	/**
	 * 客服部門分身 => 伊凡達TW
	 * 
	 * @param csClone
	 */
	private void cloneCsStaff(Map<Long, Employee> csClone) {
		Department department = departmentManager.findOneByFullName("客服部", 1);  // 伊凡達TW
		List<JobTitle> jobList = jobTitleManager.findAllJobTitle(1L);
		
		int no = Integer.parseInt(jobList.get(jobList.size() - 1).getTitleNo().substring(1)) + 1;
		NumberFormat formatter = new DecimalFormat("00");
		
		for(Entry<Long, Employee> entry : csClone.entrySet()) {
			Long uid = entry.getKey();
			Employee employee = entry.getValue();
						
			employee.setCompanyId(1L);
			employee.setEmployeeNo("X" + employee.getEmployeeNo().replaceFirst("X", ""));  // 吃掉前一次clone產生的X
			employee.setDepartmentId(department.getId().intValue());
			
			Optional<JobTitle> title = jobList.stream().filter(j -> j.getName().equals(employee.getJobTitleName())).findFirst();
			JobTitle job = null;
			
			if(title.isPresent()) {
				job = title.get();
			}
			else {			
				job = new JobTitle();
				
				job.setTitleNo("D" + formatter.format(no));			
				job.setCompanyId(1L);
				job.setName(employee.getJobTitleName());
				job.setCreator(employee.getCreator());
				
				job = jobTitleManager.add(job);
				
				no++;
				jobList.add(job);
			}
			
			employee.setJobTitleId(job.getId().intValue());						
			
			Employee clone = employeeManager.addEmployee(employee);
			permissionManager.addUserEmpMapping(uid, clone.getId());
		}		
	}
	
	/**
	 * CN部門主管=TW主管
	 */
	private void cloneCnSupervisor() {
		// CN部門
		List<Department> cnDeptList = departmentManager.findDepartmentCN();
		
		for(Department cnDept : cnDeptList) {
			String fullName = cnDept.getName().replaceAll("\\(廣州\\)", "");
			Department twDept = departmentManager.findOneByFullName(fullName, cnDept.getCompanyId());
			
			// TW主管
			List<Employee> supervisor = employeeManager.findSupervisor(twDept.getId());
			
			for(Employee employee : supervisor) {
				employee.setEmployeeNo("X" + employee.getEmployeeNo().replaceFirst("X", ""));  // 吃掉前一次clone產生的X
				employee.setDepartmentId(cnDept.getId().intValue());		
				
				long uid = employee.getUserId();
				Employee clone = employeeManager.addEmployee(employee);
				permissionManager.addUserEmpMapping(uid, clone.getId());
			}
		}
	}
	
	/**
	 * clone 主管
	 * 
	 * @param xClone
	 */
	private void cloneSupervisor(Map<String, Employee> xClone) throws Exception {
		for(Entry<String, Employee> x : xClone.entrySet()) {
			String deptName = x.getKey();
			Employee employee = x.getValue();
			Department department = departmentManager.findOneByFullName(deptName, employee.getCompanyId());
			
			if(department != null) {
				String empNo = employee.getEmployeeNo().replaceFirst("X", "");  // 吃掉前一次clone產生的X
				employee.setEmployeeNo("X" + empNo);  
				employee.setDepartmentId(department.getId().intValue());
				//Long uid  = permissionManager.findByEmployeeId(employee.getId());
				
				Employee master = employeeManager.findOneByEmployeeNo(empNo);
				Long uid  = permissionManager.findByEmployeeId(master.getId());
				
				if(uid == 0) {
					throw new Exception("NULL UID");
				}
				
				Employee clone = employeeManager.addEmployee(employee);				
				
				permissionManager.addUserEmpMapping(uid, clone.getId());
			}
		}
	}
	
	/**
	 * 牛番茄TW => HK
	 * 
	 * @param tomatoClone
	 */
	private void cloneTomatoStaff(Map<Long, Employee> tomatoClone) {
		List<Employee> hkEmpList = employeeManager.findAllEmployee(5);		
		List<Department> deptList = departmentManager.findAllDepartment(5L);
		List<JobTitle> jobList = jobTitleManager.findAllJobTitle(5L);
		
		int jobNo = Integer.parseInt(jobList.get(jobList.size() - 1).getTitleNo().substring(1)) + 1;
		int deptNo = Integer.parseInt(deptList.get(deptList.size() - 1).getDeptNo().substring(1)) + 1;
		
		NumberFormat formatter = new DecimalFormat("00");
		
		for(Entry<Long, Employee> entry : tomatoClone.entrySet()) {
			Long uid = entry.getKey();
			Employee employee = entry.getValue();
			
			Optional<Employee> hkEmp = hkEmpList.stream().filter(x -> x.getName().equals(employee.getName())).findFirst();
			
			if(hkEmp.isPresent()) {
				log.info("skip " + employee.getName());
				continue;
			}						
						
			employee.setCompanyId(5L);
			employee.setEmployeeNo("X" + employee.getEmployeeNo().replaceFirst("X", ""));  // 吃掉前一次clone產生的X
						
			// Department
			Optional<Department> hkDept = deptList.stream().filter(x -> x.getName().equals(employee.getDepartmentName() + "(香港)")).findFirst();
			Department department = null;
			
			if(hkDept.isPresent()) {
				department = hkDept.get();
			}
			else {
				department = new Department();
				
				department.setDeptNo("C" + formatter.format(deptNo));
				department.setName(employee.getDepartmentName() + "(香港)");
				department.setCompanyId(5L);
				department.setDeptLevel(3);
				department.setParentId(deptList.get(0).getId().intValue());
				department.setCreator(employee.getCreator());
				department = departmentManager.add(department);
				
				deptNo++;
				deptList.add(department);
			}
			
			employee.setDepartmentId(department.getId().intValue());
			
			// Job
			Optional<JobTitle> title = jobList.stream().filter(j -> j.getName().equals(employee.getJobTitleName())).findFirst();
			JobTitle job = null;
			
			if(title.isPresent()) {
				job = title.get();
			}
			else {			
				job = new JobTitle();
				
				job.setTitleNo("D" + formatter.format(jobNo));			
				job.setCompanyId(5L);
				job.setName(employee.getJobTitleName());
				job.setCreator(employee.getCreator());
				
				job = jobTitleManager.add(job);
				
				jobNo++;
				jobList.add(job);
			}
			
			employee.setJobTitleId(job.getId().intValue());						
			
			Employee clone = employeeManager.addEmployee(employee);
			permissionManager.addUserEmpMapping(uid, clone.getId());
		}				
	}
		
	/**
	 * 用戶權限
	 * 
	 * @param userForm
	 * @param employee
	 * @param roleMap
	 * @param formRoleMap
	 */
	private void userAuth(UserForm userForm, Employee employee,
			Map<String, Role> roleMap, Map<String, FormRole> formRoleMap, 
			Map<String, Employee> xClone) {						
		// 子公司
		if(employee.getCompanyId() == 4  // 伊凡達科技(香港)遊戲
				|| employee.getCompanyId() == 7) {  // 香港商伊凡達台灣分公司
			log.warn("子公司人員,人工設定權限");
			return;
		}
		
		// 風靡,鼎客
		if(employee.getCompanyId() == 2 || employee.getCompanyId() == 11) {
			userAuthSub(userForm, employee, roleMap, formRoleMap);
			return;
		}		
		
		// 牛番茄
		if(employee.getCompanyId() == 5 || employee.getCompanyId() == 6) {  
			userAuthTomato(userForm, employee, roleMap, formRoleMap);
			return;
		}
		
		// KR
		if(StringUtils.startsWith(employee.getEmployeeNo(), "K") ||
				StringUtils.contains(employee.getDepartmentName(), "韓國")) {
			userAuthKorea(userForm, employee, roleMap, formRoleMap);
			return;
		}
		// 伊凡達TW
		Role role = null;
		List<FormRole> formRoleList = new LinkedList<FormRole>();
		// 總監
		if(StringUtils.equals(employee.getJobTitleName(), "技術總監")
				|| (StringUtils.equals("商務部", employee.getDepartmentName()) && StringUtils.equals(employee.getJobTitleName(), "商務總監"))) {
			String prefix = employee.getJobTitleName().substring(0, 2);
			role = roleMap.get(prefix + "主管");
			
			// XX主管
			if(formRoleMap.containsKey(prefix + "主管")) {
				formRoleList.add(formRoleMap.get(prefix + "主管"));
			}
			// 部門主管			
			if(formRoleMap.containsKey("部門主管")) {
				formRoleList.add(formRoleMap.get("部門主管"));
			}
			
			userAuth(userForm, formRoleList, role);
			
			return;
		}
		
		if(StringUtils.equals(employee.getJobTitleName(), "產品總監")) {
			String prefix = "營運";
			role = roleMap.get(prefix + "主管");
			
			// XX主管
			if(formRoleMap.containsKey(prefix + "主管")) {
				formRoleList.add(formRoleMap.get(prefix + "主管"));
			}
			// 部門主管			
			if(formRoleMap.containsKey("部門主管")) {
				formRoleList.add(formRoleMap.get("部門主管"));
			}
			
			userAuth(userForm, formRoleList, role);
			
			return;
		}		
		
		// 財務長 => 行政主管
		if(StringUtils.equals(employee.getJobTitleName(), "財務長") ||
				StringUtils.equals(employee.getJobTitleName(), "財會經理")) {
			role = roleMap.get("行政主管");
			
			if(formRoleMap.containsKey("行政主管")) {
				formRoleList.add(formRoleMap.get("行政主管"));
			}
						
			userAuth(userForm, formRoleList, role);
			
			return;
		}
		
		// 系統架構師 => 資訊,新創主管
		if(StringUtils.equals(employee.getJobTitleName(), "系統架構師")) {
			role = roleMap.get("資訊主管");
			
			if(formRoleMap.containsKey("部門主管")) {
				formRoleList.add(formRoleMap.get("部門主管"));
			}
			
			if(formRoleMap.containsKey("重大負責人")) {
				formRoleList.add(formRoleMap.get("重大負責人"));
			}
			
			if(formRoleMap.containsKey("重大修復人")) {
				formRoleList.add(formRoleMap.get("重大修復人"));
			}
			
			if(formRoleMap.containsKey("重大執行人")) {
				formRoleList.add(formRoleMap.get("重大執行人"));
			}
			
			userAuth(userForm, formRoleList, role);
			
			return;
		}
		
		// CS 客服部
		if(StringUtils.equals(employee.getDepartmentName(), "客服部") || StringUtils.contains(employee.getEmail(), "service")) {
			String key = "客服";
			role = roleMap.get(key);
			
			if(StringUtils.equals(employee.getJobTitleName(), "總經理")) {
				key = "總經理";
				
				if(formRoleMap.containsKey("客服")) {
					formRoleList.add(formRoleMap.get("客服"));
				}
				
				if(formRoleMap.containsKey("客服主管")) {
					formRoleList.add(formRoleMap.get("客服主管"));
				}
			}						
			
			if(formRoleMap.containsKey(key)) {
				formRoleList.add(formRoleMap.get(key));
			}			
			
			userAuth(userForm, formRoleList, role);
			// 客服
			return;
		}
		
		// RD/IT/QC 研發部,運維部(含廣州、香港)
		if(StringUtils.contains(employee.getDepartmentName(), "研發部") || StringUtils.contains(employee.getDepartmentName(), "運維部")) {
			if(StringUtils.contains(employee.getJobTitleName(), "測試")) {  // QC
				// 測試
				role = roleMap.get("測試");
				
				if(formRoleMap.containsKey("測試")) {
					formRoleList.add(formRoleMap.get("測試"));
				}
			}
			else {
				// 技術(RD/IT)
				role = roleMap.get("技術");

				if(formRoleMap.containsKey("重大負責人")) {
					formRoleList.add(formRoleMap.get("重大負責人"));
				}
				
				if(formRoleMap.containsKey("重大修復人")) {
					formRoleList.add(formRoleMap.get("重大修復人"));
				}
				
				if(formRoleMap.containsKey("重大執行人")) {
					formRoleList.add(formRoleMap.get("重大執行人"));
				}				
			}
			
			userAuth(userForm, formRoleList, role);
			
			return;
		}
						
		// PM(含廣州、香港)
		if(StringUtils.contains(employee.getDepartmentName(), "產品部") || StringUtils.contains(employee.getJobTitleName(), "產品")) {
			// 產品
			role = roleMap.get("產品");
			
			if(formRoleMap.containsKey("產品")) {
				formRoleList.add(formRoleMap.get("產品"));
			}
			
			if(formRoleMap.containsKey("重大負責人")) {
				formRoleList.add(formRoleMap.get("重大負責人"));
			}
			
			if(formRoleMap.containsKey("重大修復人")) {
				formRoleList.add(formRoleMap.get("重大修復人"));
			}
			
			if(formRoleMap.containsKey("重大執行人")) {
				formRoleList.add(formRoleMap.get("重大執行人"));
			}
			
			// power部門主管=資深XXX
			if(StringUtils.containsIgnoreCase(employee.getDepartmentName(), "Power") 
					&& StringUtils.equals(employee.getJobTitleName(), "資深產品經理")) {
				if(formRoleMap.containsKey("部門主管")) {
					formRoleList.add(formRoleMap.get("部門主管"));
				}
			}
			
			userAuth(userForm, formRoleList, role);
			
			return;
		}
								
		// 顧問 => 總經理
		if(StringUtils.equals(employee.getJobTitleName(), "顧問")) {
			// 總經理
			role = roleMap.get("總經理");
			
			if(formRoleMap.containsKey("總經理")) {
				formRoleList.add(formRoleMap.get("總經理"));
			}
			
			userAuth(userForm, formRoleList, role);
			
			return;
		}
		// 董事長兼任總經理
		if(StringUtils.equals(employee.getJobTitleName(), "董事長兼任總經理")) {
			// 董事長
			role = roleMap.get("董事長");
			
			if(formRoleMap.containsKey("董事長")) {
				formRoleList.add(formRoleMap.get("董事長"));
			}
			
			if(formRoleMap.containsKey("部門主管")) {
				formRoleList.add(formRoleMap.get("部門主管"));
			}						
			
			userAuth(userForm, formRoleList, role);
			
			// 財會主管
			xClone.put("總管理處(財會)", employee);
			
			return;
		}
		// 副總經理 => 行銷,社群,儲訓部主管
		if(StringUtils.equals(employee.getJobTitleName(), "副總經理")) {
			// 副總經理
			role = roleMap.get("副總經理");
			
			if(formRoleMap.containsKey("副總經理")) {
				formRoleList.add(formRoleMap.get("副總經理"));
			}
			
			// 部門主管			
			if(formRoleMap.containsKey("部門主管")) {
				formRoleList.add(formRoleMap.get("部門主管"));
			}
			
			userAuth(userForm, formRoleList, role);
			
			// 行銷部主管
			xClone.put("行銷部(設計)", employee);
			xClone.put("行銷部(行銷)", employee);
			// 社群部主管
			//xClone.put("社群遊戲部", employee);
			// 儲訓部主管
			xClone.put("總管理處(儲訓)", employee);
			
			return;
		}
		
		// 電商副總
		if(StringUtils.equals(employee.getJobTitleName(), "電子商務副總經理")) {
			// 副總經理
			role = roleMap.get("副總經理");
			
			// 部門主管			
			if(formRoleMap.containsKey("部門主管")) {
				formRoleList.add(formRoleMap.get("部門主管"));
			}
						
			userAuth(userForm, formRoleList, role);
			
			// 電商主管
			xClone.put("總管理處(電商)", employee);
			
			return;
		}
		
		// 總管理處(行政)主管
		if(StringUtils.equals(employee.getJobTitleName(), "資深行政經理")) {
			role = roleMap.get("一般");
			
			// 部門主管			
			if(formRoleMap.containsKey("部門主管")) {
				formRoleList.add(formRoleMap.get("部門主管"));
			}
						
			userAuth(userForm, formRoleList, role);
			
			return;
		}
		
		// 出納
		if(StringUtils.contains(employee.getJobTitleName(), "出納")) {
			// 出納
			role = roleMap.get("出納");
			
			if(formRoleMap.containsKey("出納")) {
				formRoleList.add(formRoleMap.get("出納"));
			}
			
			userAuth(userForm, formRoleList, role);
			
			return;
		}
		// 會計
		if(StringUtils.contains(employee.getJobTitleName(), "會計") 
				|| StringUtils.contains(employee.getJobTitleName(), "管會") 
				|| StringUtils.contains(employee.getJobTitleName(), "財會") 
				|| StringUtils.equals(employee.getJobTitleName(), "行政管理總監")) {
			role = roleMap.get("會計");
			// 都開			
			if(formRoleMap.containsKey("會計")) {
				formRoleList.add(formRoleMap.get("會計"));
			}
						
			if(formRoleMap.containsKey("會計專員")) {
				formRoleList.add(formRoleMap.get("會計專員"));
			}						
			
			userAuth(userForm, formRoleList, role);
			
			return;
		}
		// 法務
		if(StringUtils.contains(employee.getJobTitleName(), "法務")) {
			// 法務
			role = roleMap.get("法務");
			
			if(formRoleMap.containsKey("法務")) {
				formRoleList.add(formRoleMap.get("法務"));
			}
			
			userAuth(userForm, formRoleList, role);
			
			return;
		}
		// 總務
		if(StringUtils.contains(employee.getJobTitleName(), "總務")) {
			// 總務
			role = roleMap.get("總務");
			
			if(formRoleMap.containsKey("總務")) {
				formRoleList.add(formRoleMap.get("總務"));
			}
			
			userAuth(userForm, formRoleList, role);
			
			return;
		}
		// 人資
		if(StringUtils.contains(employee.getJobTitleName(), "人資")) {
			// 人資
			role = roleMap.get("人資");
			
			if(formRoleMap.containsKey("人資")) {
				formRoleList.add(formRoleMap.get("人資"));
			}
			
			userAuth(userForm, formRoleList, role);
			
			return;
		}		
		
		// 一般
		role = roleMap.get("一般");
		userAuth(userForm, formRoleList, role);				
	}
	
	/**
	 * 客服帳號
	 * 
	 * @param employeeName
	 * @return
	 */
	private String getCsAccount(String employeeName) {
		return CS_ACCOUNT.get(employeeName);		
	}
	
	@RequestMapping("/employeeList")
	@ResponseBody
	public List<Employee> employeeList(@RequestParam(required=true) int cid,
			@RequestParam(required=true) int did,
			HttpServletRequest request) throws Exception {
		PrintUtil.printRequest(this.getClass().getSimpleName(), "employeeList", request);
		List<Employee> employeeList = employeeManager.findAllEmployee(cid, did);
		log.info("return " + employeeList);
		return employeeList;
	}
	
	@RequestMapping("/clone")
	public String clone(HttpServletRequest request) {								
		request.setAttribute("allCompany", companyManager.findAllCompany());
		
		return "/employee/clone";
	}
	
	@RequestMapping("/cloneEmployee")
	public @ResponseBody Map<String, Object> saveClone(@RequestParam(required=true) Long srcComId,
			@RequestParam(required=true) Long srcDptId, @RequestParam(required=false) Long srcEmpId,
			@RequestParam(required=true) Long destComId, @RequestParam(required=true) Long destDptId) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		log.info(String.format("clone [%s][%s][%s] => [%s][%s]", srcComId, srcDptId, srcEmpId, destComId, destDptId));
		List<Employee> employeeList = new LinkedList<Employee>();
		
		if(srcEmpId == null) {
			List<Employee> list = employeeManager.findAllEmployee(srcComId.intValue(), srcDptId.intValue());
			employeeList.addAll(list);
		}
		else {
			Employee employee = employeeManager.findOneById(srcEmpId);
			employeeList.add(employee);							
		}
		
		try {			
			for(Employee employee : employeeList) {
				cloneEmployee(destComId, srcDptId, destDptId, employee);
			}
						
			flag = true;
		}
		catch(Exception e) {
			log.error("err:" , e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");			
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");	
		}
		
		return result;
	}
	
	private void cloneEmployee(Long companyId, Long srcDepartmentId, Long destDepartmentId, Employee employee) {
		Company company = companyManager.findOneById(companyId);
		List<Department> deptList = departmentManager.findAllDepartment(companyId);
		List<JobTitle> jobList = jobTitleManager.findAllJobTitle(companyId);
		Department top = null;
		Department cloneDept = null;
		
		int deptNo = deptList.isEmpty() ? 1 : Integer.parseInt(deptList.get(deptList.size() - 1).getDeptNo().substring(1)) + 1;
		int jobNo = jobList.isEmpty() ? 1 : Integer.parseInt(jobList.get(jobList.size() - 1).getTitleNo().substring(1)) + 1;
		NumberFormat formatter = new DecimalFormat("00");
		boolean isHK = company.getName().indexOf("香港") != -1;
		
		if(deptList.isEmpty()) {
			// 空殼
			top = new Department();
			
			top.setDeptNo("A01");
			top.setName("總管理處" + (isHK ? "(香港)" : ""));
			top.setCompanyId(companyId);
			top.setDeptLevel(1);
			top.setCreator(employee.getCreator());
			top = departmentManager.add(top);			
		}
		else {			
			top = deptList.get(0);				
		}
		
		if(destDepartmentId == null) {
			Department department = departmentManager.findOneById(srcDepartmentId);
			String deptName = department.getName() + (isHK ? "(香港)" : "");
			
			// Duplicate check
			cloneDept = departmentManager.findOneByFullName(deptName, companyId);
			
			if(cloneDept == null) {
				cloneDept = new Department();
				
				cloneDept.setDeptNo("C" + formatter.format(deptNo));
				cloneDept.setName(deptName);
				cloneDept.setCompanyId(companyId);
				cloneDept.setDeptLevel(department.getDeptLevel());
				cloneDept.setParentId(top.getId().intValue());
				cloneDept.setCreator(employee.getCreator());
				cloneDept = departmentManager.add(cloneDept);
			}
		}
		else {
			cloneDept = departmentManager.findOneById(destDepartmentId);
		}
		
		destDepartmentId = cloneDept.getId();
		
		// Duplicate check
		String empNo = "X" + employee.getEmployeeNo().replaceFirst("X", "");  // 吃掉前一次clone產生的X
		
		if(employeeManager.findOneByEmployeeNo(companyId, empNo) != null) {
			log.warn("Duplicate employee " + empNo);
			return;
		}
		
		Optional<JobTitle> title = jobList.stream().filter(j -> j.getName().equals(employee.getJobTitleName())).findAny();
		JobTitle job = null;
		
		if(title.isPresent()) {
			job = title.get();
		}
				
		if(job == null) {
			job = new JobTitle();
			
			job.setTitleNo("D" + formatter.format(jobNo));			
			job.setCompanyId(companyId);
			job.setName(employee.getJobTitleName());
			job.setCreator(employee.getCreator());
			
			job = jobTitleManager.add(job);			
		}
				
		employee.setCompanyId(companyId);
		employee.setEmployeeNo(empNo);  
		employee.setDepartmentId(cloneDept.getId().intValue());		
		employee.setJobTitleId(job.getId().intValue());
		long uid = employee.getUserId();
		
		Employee clone = employeeManager.addEmployee(employee);
		permissionManager.addUserEmpMapping(uid, clone.getId());				
	}
}