package com.baplay.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import com.baplay.constant.Form;
import com.baplay.dto.Stamp;
import com.baplay.entity.BusinessCardForm;
import com.baplay.entity.Receipt;
import com.baplay.entity.TEfunuser;
import com.baplay.entity.WorkFlowDetail;
import com.baplay.form.BusinessCardFormForm;
import com.baplay.service.IBusinessCardFormManager;
import com.baplay.service.ICompanyManager;
import com.baplay.service.IEmployeeManager;
import com.baplay.service.IFormAuthManager;
import com.baplay.service.INotifyManager;
import com.baplay.service.impl.FlowManager;
import com.baplay.service.impl.ReceiptManager;
import com.baplay.util.DateUtil;
import com.baplay.util.PdfUtil;
import com.baplay.util.ReportExcel;
import com.baplay.util.StringUtil;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/businessCardForm")
public class BusinessCardFormController extends BaseController {

	private static final String FUNCTION_NAME = "公司卡消費通知單";
	private static final int PAGE_SIZE = 25;
	@Autowired
	private IBusinessCardFormManager businessCardFormManager;
	@Autowired
	private ICompanyManager companyManager;
	@Autowired
	private IEmployeeManager employeeManager;
	@Autowired
	private ReceiptManager receiptManager;
	@Autowired
	private FlowManager flowManager;
	@Autowired
	private INotifyManager notifyManager;
	@Autowired
	private IFormAuthManager formAuthManager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.web.controller.BaseController#getFormType()
	 */
	protected String getFormType() {
		return Form.BUSINESS_CARD.getValue();
	}

	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		boolean auth = formAuthManager.formAuth(getFormType(), getUserFormRole());
		request.setAttribute("companys", auth ? companyManager.findAllCompany() : getUserCompany());
		return "/businessCardForm/init";
	}

	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		TEfunuser user = getLoginUser();

		if (id != null) {
			request.setAttribute("cp", businessCardFormManager.findOneById(id));
			List<Receipt> receipt = receiptManager.findAllByForm(id, this.getFormType(), 1);

			request.setAttribute("receipts", receipt);
		} else {
			request.setAttribute("employee", employeeManager.findOneById(user.getEmployeeId()));
		}

		return "/businessCardForm/edit";
	}

	@RequestMapping("/get")
	public String get(HttpServletRequest request, Long id, String action) {
		BusinessCardForm form = businessCardFormManager.findOneById(id);
		request.setAttribute("cp", form);
		
		Map<String, Stamp> stamp = flowManager.findFormStamp(form.getWorkFlowId());
		request.setAttribute("stamp", stamp);
		
		List<Receipt> receipt = receiptManager.findAllByForm(id, this.getFormType(), 1, action);

		request.setAttribute("receipts", receipt);
		return "/businessCardForm/get";
	}

	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, BusinessCardForm businessCardForm) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;

		try {
			if (businessCardForm != null) {
				if (businessCardForm.getId() == null || businessCardForm.getId() <= 0) {
					businessCardForm = businessCardFormManager.add(request, businessCardForm, user, false,
							getFormType(), getFormType());

					flag = (businessCardForm != null);
				} else {
					flag = (businessCardFormManager.update(request, businessCardForm, user, false, getFormType(),
							getFormType()) > 0);
				}
			}
		} catch (Exception e) {
			log.error("err:", e);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}

	@RequestMapping("/list")
	public String list(HttpServletRequest request, BusinessCardFormForm businessCardFormForm) {
		businessCardFormForm.setPageSize(PAGE_SIZE);
		businessCardFormForm.setExport(false);
		
		// Auth
		businessCardFormForm.setFormRole(getUserFormRole());
		businessCardFormForm.setLoginUser(getLoginUser().getId());
		businessCardFormForm.setFormType(getFormType());
		businessCardFormForm.setEmployeeIdList(getUserAllEmployeeId());
		
		Page<BusinessCardForm> page = businessCardFormManager.list(businessCardFormForm);
		request.setAttribute("page", page);

		return "/businessCardForm/list";
	}

	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> deleteCP(Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;

		if (id != null && id > 0) {
			flag = (businessCardFormManager.delete(id) > 0);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}
	
	@RequestMapping("/deleteReceipt")
	public @ResponseBody Map<String, Object> delete(Long id, Long editId) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		if(id != null && id > 0) {
			flag = (businessCardFormManager.updateTotal(id, editId, getFormType()) > 0);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		
		return result;
	}

	@RequestMapping("/export")
	public void export(HttpServletRequest request, HttpServletResponse response,
			BusinessCardFormForm businessCardFormForm) {
		// Auth
		businessCardFormForm.setFormRole(getUserFormRole());
		businessCardFormForm.setLoginUser(getLoginUser().getId());
		businessCardFormForm.setFormType(getFormType());
		businessCardFormForm.setEmployeeIdList(getUserAllEmployeeId());
		
		String[] dataTitles = { "表單編號", "公司", "部門", "申請人", "申請日期", "聯絡人", "聯絡人電話", "憑證總金額", "狀態" };
		businessCardFormForm.setExport(true);
		
		businessCardFormForm.setFormRole(getUserFormRole());
		businessCardFormForm.setLoginUser(getLoginUser().getId());
		businessCardFormForm.setFormType(getFormType());
		businessCardFormForm.setEmployeeIdList(getUserAllEmployeeId());
		
		Page<BusinessCardForm> page = businessCardFormManager.list(businessCardFormForm);
		List<Object[]> list = new ArrayList<Object[]>();

		if (page.getResult() != null && !page.getResult().isEmpty()) {
			for (BusinessCardForm nl : page.getResult()) {
				Object[] o = new Object[dataTitles.length];				
				o[0] = nl.getSerialNo();
				o[1] = nl.getCompanyName();
				o[2] = nl.getDepartmentName();
				o[3] = nl.getCreatorName();
				o[4] = DateUtil.getStringOfDate(nl.getRequestDate(), DateUtil.defaultDatePatternStr, null);
				o[5] = nl.getContact();
				o[6] = nl.getContactTel();
				o[7] = nl.getTotalAmount();
				o[8] = StringUtil.getFlowStatusDesc(nl.getStatus());

				list.add(o);
			}
		}

		try {
			ReportExcel.reportExcel(request, response, FUNCTION_NAME + DateUtil.getCurrentTimeStr("yyyyMMddHHmmss"), "", dataTitles,
					list);
		} catch (Exception e) {
			log.error("err:" , e);
		}
	}
	
	/**
	 * 送審
	 * 
	 * @param request
	 * @param paymentForm
	 * @return
	 */
	@RequestMapping("/submit")
	public @ResponseBody Map<String, Object> doSubmit(HttpServletRequest request, BusinessCardForm businessCardForm) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;	
		
		try {
			if(businessCardForm != null) {
				if(businessCardForm.getId() == null || businessCardForm.getId() <= 0) {
					businessCardForm = businessCardFormManager.add(request, businessCardForm, user, true, getFormType(), getFormType());				
					flag = (businessCardForm!=null);			
				} 
				else {				
					flag = (businessCardFormManager.update(request, businessCardForm, user, true, getFormType(), getFormType()) > 0);			
				}
				
				if(flag) {
					notifyManager.notify(businessCardForm.getWorkFlowId());
				}	
			}
		}
		catch(Exception e) {
			log.error("err:", e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "送審成功！");			
		} 
		else {
			result.put("code", "0001");
			result.put("message", "送審失敗！");	
		}
		
		return result;
	}
	
	/**
	 * 審放頁面
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/process")
	public String process(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		// Flow
		WorkFlowDetail workFlowDetail = flowManager.findFormFlowStep(workFlowDetailId);
		flowStep(request, workFlowDetail);
				
		// Form
		BusinessCardForm form = businessCardFormManager.findOneByFlow(workFlowDetail.getWorkFlowId());		
		request.setAttribute("form", form);
		
		// Receipt
		List<Receipt> receipt = receiptManager.findAllByForm(form.getId(), this.getFormType(), 1);
		request.setAttribute("receipts", receipt);
		
		Map<String, Stamp> stamp = flowManager.findFormStamp(form.getWorkFlowId());
		request.setAttribute("stamp", stamp);
		
		return "/businessCardForm/flow";
	}
	
	/**
	 * 審核
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/verify")
	public @ResponseBody Map<String, Object> doVerify(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.verifyFlow(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "審核成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "審核失敗！");
		}
		
		return result;
	}
	
	/**
	 * 放行
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/pass")
	public @ResponseBody Map<String, Object> doPass(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.passFlow(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "放行成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "放行失敗！");
		}
		
		return result;
	}
	
	/**
	 * 退件
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/reject")
	public @ResponseBody Map<String, Object> doReject(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId,
			@RequestParam("memo") String memo) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.rejectFlow(workFlowDetailId, user.getId(), memo);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "退件成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "退件失敗！");
		}
		
		return result;
	}
	
	/**
	 * 取消
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/abort")
	public @ResponseBody Map<String, Object> abort(HttpServletRequest request, 
			@RequestParam("id") Long workFlowDetailId) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.lockAbort(workFlowDetailId);
			flag = true;
		}
		catch(Exception e) {
			 log.error("err:", e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "取消成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "取消失敗！");
		}
		
		return result;
	}
	
	/**
	 * PDF
	 * 
	 * @param request
	 * @param response
	 * @param formId
	 */
	@RequestMapping("/pdf")
	public void pdf(HttpServletRequest request, HttpServletResponse response, Long formId) {
		BusinessCardForm form = businessCardFormManager.findOneById(formId);
		PdfUtil pdf = new PdfUtil();
		String content = notifyManager.getFormContent(getFormType(), formId, PdfUtil.ACTION);
		pdf.setHtmlContent(pdf.getBaseUrl(request), FUNCTION_NAME, content);
		
		//log.debug(content);
		String fileName = FUNCTION_NAME + form.getSerialNo();
		String subject = String.format("%s %s %s", 
				form.getCreatorName(), DateUtil.format(form.getRequestDate(), DateUtil.chineseDatePatternStr), FUNCTION_NAME);		
		
		try {
			pdf.export(response, fileName, subject);
		} 
		catch(Exception e) {
			log.error("err:" , e);
		}	
	}
}