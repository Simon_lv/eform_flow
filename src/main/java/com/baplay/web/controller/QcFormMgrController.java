package com.baplay.web.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import com.baplay.entity.QcFormDetailField;
import com.baplay.entity.QcFormSchema;
import com.baplay.entity.QcFormTitleField;
import com.baplay.entity.TEfunuser;
import com.baplay.form.QcFormMgrForm;
import com.baplay.service.IQcFormDetailFieldManager;
import com.baplay.service.IQcFormSchemaManager;
import com.baplay.service.IQcFormTitleFieldManager;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/qcFormMgr")
public class QcFormMgrController extends BaseController {

	private static final int PAGE_SIZE = 25;
	
	@Autowired
	private IQcFormSchemaManager qcFormSchemaManager;
	@Autowired
	private IQcFormTitleFieldManager qcFormTitleFieldManager;
	@Autowired
	private IQcFormDetailFieldManager qcFormDetailFieldManager;
	
	@Override
	protected String getFormType() {
		return StringUtils.EMPTY;
	}
	
	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		request.setAttribute("formSchemas", qcFormSchemaManager.findAllFormSchema());
		return "/qcFormMgr/init";
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, QcFormMgrForm qcFormMgrForm) {
		qcFormMgrForm.setPageSize(PAGE_SIZE);
		qcFormMgrForm.setExport(false);		
		Page<QcFormSchema> page = qcFormSchemaManager.list(qcFormMgrForm);
		request.setAttribute("page", page);		
		return "/qcFormMgr/list";
	}
	
	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		if (id != null) {
			request.setAttribute("schema", qcFormSchemaManager.findOneById(id));
			request.setAttribute("titleField", qcFormTitleFieldManager.findAll(id, 0L));
			request.setAttribute("groupLabels", qcFormDetailFieldManager.findAllGroupLabel(id));
			request.setAttribute("fieldLabels", qcFormDetailFieldManager.findAll(id, 0L));
			request.setAttribute("fieldCount", qcFormDetailFieldManager.findAllFieldCount(id));
		}else{
			
		}
		
		return "/qcFormMgr/edit";
	}
	
	@RequestMapping("/saveSchema")
	public @ResponseBody Map<String, Object> saveSchema(HttpServletRequest request, QcFormSchema qcFormSchema) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;

		try {
			if (qcFormSchema != null) {
				if (qcFormSchema.getId() == null || qcFormSchema.getId() <= 0) {
					qcFormSchema.setCreator(user.getId());
					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");			    	
					qcFormSchema.setVersion(sdf.format(new Date()));
					qcFormSchema = qcFormSchemaManager.add(qcFormSchema);
					flag = (qcFormSchema != null);
				} else {
					flag = (qcFormSchemaManager.update(qcFormSchema) > 0);
				}
			}
		} catch (Exception e) {
			log.error("err:", e);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");
			result.put("id", qcFormSchema.getId());
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
			result.put("id", qcFormSchema.getId());
		}

		return result;
	}
	
	@RequestMapping("/saveTitle")
	public @ResponseBody Map<String, Object> saveTitle(HttpServletRequest request, QcFormTitleField qcFormTitleField) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;

		try {
			if (qcFormTitleField != null) {				
				qcFormTitleField = qcFormTitleFieldManager.add(request, qcFormTitleField);
				flag = (qcFormTitleField != null);				
			}
		} catch (Exception e) {
			log.error("err:", e);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");
			result.put("id", qcFormTitleField.getSchemaId());
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
			result.put("id", qcFormTitleField.getSchemaId());
		}

		return result;
	}
	
	@RequestMapping("/saveDetail")
	public @ResponseBody Map<String, Object> saveDetail(HttpServletRequest request, QcFormDetailField qcFormDetailField) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;

		try {
			if (qcFormDetailField != null) {				
				qcFormDetailField = qcFormDetailFieldManager.add(request, qcFormDetailField);
				flag = (qcFormDetailField != null);				
			}
		} catch (Exception e) {
			log.error("err:", e);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");
			result.put("id", qcFormDetailField.getSchemaId());
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
			result.put("id", qcFormDetailField.getSchemaId());
		}

		return result;
	}
	
	@RequestMapping("/deleteTitle")
	public @ResponseBody Map<String, Object> deleteTitle(HttpServletRequest request, Long id) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		int flag = 0;

		try {
			if (id != null) {
				flag = qcFormTitleFieldManager.delete(id);		
			}
		} catch (Exception e) {
			log.error("err:", e);
		}

		if (flag > 0) {
			result.put("code", "0000");
			result.put("message", "保存成功！");			
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");			
		}

		return result;
	}
	
	@RequestMapping("/deleteGroup")
	public @ResponseBody Map<String, Object> deleteGroup(HttpServletRequest request, Long id) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		int flag = 0;

		try {
			if (id != null) {
				flag = qcFormDetailFieldManager.deleteGroup(id);		
			}
		} catch (Exception e) {
			log.error("err:", e);
		}

		if (flag > 0) {
			result.put("code", "0000");
			result.put("message", "保存成功！");			
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");			
		}

		return result;
	}
	
	@RequestMapping("/deleteDetail")
	public @ResponseBody Map<String, Object> deleteDetail(HttpServletRequest request, Long id) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		int flag = 0;

		try {
			if (id != null) {
				flag = qcFormDetailFieldManager.delete(id);		
			}
		} catch (Exception e) {
			log.error("err:", e);
		}

		if (flag > 0) {
			result.put("code", "0000");
			result.put("message", "保存成功！");			
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");			
		}

		return result;
	}
	
	@RequestMapping("/addField")
	@ResponseBody
	public String addColumn(HttpServletRequest request, @RequestParam(required=true)Long id){
		StringBuffer html = new StringBuffer("");
		
		List<QcFormDetailField> qdf = qcFormDetailFieldManager.findAllGroupLabel(id);
		
		html.append("<tr>");
		html.append("<td></td>");
		html.append("<td>");
		html.append("<input type='hidden' name='id' value=''/>");
		html.append("<select name='groupLabel'><option value=''>選擇</option>");
		
		for(int i=0;i<qdf.size();i++){
			html.append("<option value='"+qdf.get(i).getGroupLabel()+"'>"+qdf.get(i).getGroupLabel()+"</option>");
		}
		html.append("</select>");
		html.append("</td>");
		html.append("<td><input name='fieldLabel' value='' size='60'/></td>");
		html.append("<td><input type='number' name='fieldOrder' value='' style='width: 50px;'/></td>");
		html.append("<td><input class='btn delete' type='button' value='刪除'/></td>");
		html.append("</tr>");
		
		return html.toString();
	}

	@RequestMapping("/get")
	public String get(HttpServletRequest request, Long id) {
		request.setAttribute("formSchema", qcFormSchemaManager.findOneById(id));
		request.setAttribute("formTitleField", qcFormTitleFieldManager.findAll(id));
		request.setAttribute("formDetailField", qcFormDetailFieldManager.findAll(id));
		request.setAttribute("fieldCount", qcFormDetailFieldManager.findAllFieldCount(id));
				
		return "/qcFormMgr/get";
	}
}