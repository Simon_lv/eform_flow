package com.baplay.web.controller;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.baplay.constant.Flow;
import com.baplay.dto.FormFlow;
import com.baplay.entity.Company;
import com.baplay.entity.Employee;
import com.baplay.entity.FormType;
import com.baplay.entity.Role;
import com.baplay.entity.TEfunuser;
import com.baplay.entity.WorkFlow;
import com.baplay.entity.WorkFlowDetail;
import com.baplay.service.IEmployeeManager;
import com.baplay.service.IFlowManager;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/flow")
public class FlowController extends BaseController {

	@Autowired
	private IFlowManager flowManager;
	@Autowired
	private IEmployeeManager employeeManager;
	
	/* (non-Javadoc)
	 * @see com.baplay.web.controller.BaseController#getFormType()
	 */
	protected String getFormType() {
		return StringUtils.EMPTY;
	}
	
	/**
	 * 代辦事項(表單類別)
	 * 
	 * @param request
	 * @param type
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/init")
	public String init(HttpServletRequest request, @RequestParam("type") String formType) {
		TEfunuser user = getLoginUser();
		List<Role> roles = (List<Role>) SecurityUtils.getSubject().getSession().getAttribute("role");
		Employee employee = employeeManager.findOneById(user.getEmployeeId());
		
		if(employee != null) {
			Set<Long> cid = new TreeSet<Long>();
			List<Company> userCompany = getUserCompany();
			
			for(Company c : userCompany) {
				cid.add(c.getId());
			}
						
			List<FormFlow> todoListType = flowManager.todoListType(cid, getUserDepartmentId(), user.getId(), roles, formType);
			request.setAttribute("todoListType", todoListType);
		}
		
		request.setAttribute("formType", formType);
		
		return "/flow/init";
	}
	
	/**
	 * 案件鎖定
	 * 
	 * @param request
	 * @param workFlowId
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/lock")
	public @ResponseBody Map<String, Object> lock(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		Map<String, Object> result = Maps.newHashMap();		
		TEfunuser user = getLoginUser();						
		
		try {
			// Lock		
			boolean lockOK = flowManager.lockFlow(workFlowId, workFlowDetailId, user.getId());
			
			if(lockOK) {
				result.put("code", "0000");
				result.put("message", "鎖定成功！");
			}
			else {
				result.put("code", "0001");
				result.put("message", "案件已被鎖定！");
			}
		}
		catch(Exception e) {
			log.error("err:", e);
			
			result.put("code", "0002");
			result.put("message", "鎖定失敗！");
		}
		
		return result;
	}
	
	/**
	 * 審放
	 * 
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/process")
	public String process(HttpServletRequest request,
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {		
		// Forward		
		WorkFlow workFlow = flowManager.findFormFlow(workFlowId);
		String[] codes = workFlow.getFormType().split(",");
		FormType formType = flowManager.findFomType(codes[codes.length > 0 ? 1 : 0], codes[0]);
		
		return "forward:" + formType.getProcessPage() + "?id1=" + workFlowId + "&id2=" + workFlowDetailId;		
	}
	
	/**
	 * 審核記錄
	 * 
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/list")
	public String list(HttpServletRequest request, @RequestParam("id") Long workFlowId) {
		WorkFlow workFlow = flowManager.findFormFlow(workFlowId);
		request.setAttribute("workFlow", workFlow);
		
		List<FormFlow> list = flowManager.flowHistory(workFlowId);
		request.setAttribute("list", list);
		
		return "/flow/list";
	}
	
	/**
	 * 編輯草稿
	 * 
	 * @param redirectAttributes
	 * @param workFlowId
	 * @param formType
	 * @return
	 */
	@RequestMapping("/draft")
	public String draft(RedirectAttributes redirectAttributes, 
			@RequestParam("id") Long workFlowId, @RequestParam("formType") String formType) {
		// WorkFlow
		WorkFlow workFlow = flowManager.findFormFlow(workFlowId);
		FormType type = flowManager.findFomType(null, formType);
		
		// Redirect to form edit page
		redirectAttributes.addFlashAttribute("formId", workFlow.getFormId());
		
		return "redirect:" + type.getInitPage();
	}
	
	/**
	 * 檢視
	 * 
	 * @param request
	 * @param workFlowId
	 * @return
	 */
	@RequestMapping("/get")
	public String get(HttpServletRequest request, @RequestParam("id") Long workFlowId) {
		// Forward		
		WorkFlow workFlow = flowManager.findFormFlow(workFlowId);
		String[] codes = workFlow.getFormType().split(",");
		FormType formType = flowManager.findFomType(codes[codes.length > 0 ? 1 : 0], codes[0]);
		
		return "forward:" + formType.getViewPage() + "?id=" + workFlow.getFormId();		
	}
	
	/**
	 * 直接關閉視窗(取消鎖定)
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/abort")
	public @ResponseBody Map<String, Object> abort(HttpServletRequest request, 
			@RequestParam("id") Long workFlowDetailId) {
		Map<String, Object> result = Maps.newHashMap();
		
		try {
			// Abort		
			flowManager.lockAbort(workFlowDetailId);
			
			result.put("code", "0000");
			result.put("message", "取消成功！");
		}
		catch(Exception e) {
			log.error("err:", e);
			
			result.put("code", "0001");
			result.put("message", "取消失敗！");
		}
		
		return result;
	}
	
	/**
	 * 註銷
	 * 
	 * @param request
	 * @param workFlowId
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/terminate")
	public @ResponseBody Map<String, Object> terminate(HttpServletRequest request, 
			@RequestParam(value="memo", required=false, defaultValue="") String memo,
			@RequestParam("id1") Long workFlowId, 
			@RequestParam(value="id2", required=false, defaultValue="0") Long workFlowDetailId) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		
		try {
			WorkFlowDetail workFlowDetail = null;
			
			if(workFlowDetailId == 0) {
				// Find last one
				workFlowDetail = flowManager.findLastOneById(workFlowId);
				workFlowDetailId = workFlowDetail.getId();				
			}
			else {
				workFlowDetail = flowManager.findFormFlowStep(workFlowDetailId);
			}
			
			// Check lock
			if(workFlowDetail.getStatus() == Flow.LOCK.getValue()) {
				result.put("code", "0001");
				result.put("message", "案件已被鎖定！");
			}
			else {
				// Terminate		
				flowManager.terminateFlow(workFlowId, workFlowDetailId, user.getId(), StringUtils.isEmpty(memo) ? "用戶取消" : memo);
				
				result.put("code", "0000");
				result.put("message", "註銷成功！");
			}
		}
		catch(Exception e) {
			log.error("err:", e);
			
			result.put("code", "0001");
			result.put("message", "註銷失敗！");
		}
		
		return result;
	}
}