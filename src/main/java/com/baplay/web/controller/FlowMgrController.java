package com.baplay.web.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import com.baplay.entity.FlowSchema;
import com.baplay.entity.FlowStep;
import com.baplay.entity.FormRole;
import com.baplay.form.FlowMgrForm;
import com.baplay.service.ICompanyManager;
import com.baplay.service.IFlowManager;
import com.baplay.service.IFlowSchemaManager;
import com.baplay.service.IPermissionManager;
import com.baplay.util.PrintUtil;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/flowMgr")
public class FlowMgrController extends BaseController {

	private static final int PAGE_SIZE = 25;
	
	@Autowired
	private IFlowSchemaManager flowSchemaManager;
	@Autowired
	private IPermissionManager permissionManager;
	@Autowired
	private IFlowManager flowManager;
	@Autowired
	private ICompanyManager companyManager;
	
	/* (non-Javadoc)
	 * @see com.baplay.web.controller.BaseController#getFormType()
	 */
	@Override
	protected String getFormType() {
		return StringUtils.EMPTY;
	}
	
	/**
	 * 查詢頁
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		request.setAttribute("companyList", companyManager.findAllCompany());
		//request.setAttribute("formFlowList", flowSchemaManager.findAllFlowSchema());
		return "/flowMgr/init";
	}
	
	/**
	 * 清單頁
	 * 
	 * @param request
	 * @param form
	 * @return
	 */
	@RequestMapping("/list")
	public String list(HttpServletRequest request, FlowMgrForm form) {
		form.setPageSize(PAGE_SIZE);
		form.setExport(false);		
		Page<FlowSchema> page = flowSchemaManager.list(form);
		request.setAttribute("page", page);		
		return "/flowMgr/list";
	}
	
	/**
	 * 明細
	 * 
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/get")
	public String get(HttpServletRequest request, Long id) {
		FlowSchema flowSchema = flowSchemaManager.findOneById(id);
		List<FlowStep> flowStepList = flowSchemaManager.findFlowStepBySchema(id);
		
		request.setAttribute("flowSchema", flowSchema);
		request.setAttribute("flowStepList", flowStepList);		
		
		return "/flowMgr/get";
	}
	
	/**
	 * 編輯
	 * 
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		FlowSchema flowSchema = flowSchemaManager.findOneById(id);
		List<FlowStep> flowStepList = flowSchemaManager.findFlowStepBySchema(id);
		List<FormRole> formRoles = permissionManager.getFormRoles(1);
		
		request.setAttribute("formRoles", formRoles);
		request.setAttribute("flowSchema", flowSchema);
		request.setAttribute("flowStepList", flowStepList);		
		
		return "/flowMgr/edit";
	}
	
	/**
	 * 變更角色
	 * 
	 * @param request
	 * @param flowStepId
	 * @param formRoleId
	 * @return
	 */
	@RequestMapping("/updateRole")
	public @ResponseBody Map<String, Object> updateRole(HttpServletRequest request, 
			@RequestParam("id1") Long flowStepId,
			@RequestParam("id2") Long formRoleId) {
		Map<String, Object> result = Maps.newHashMap();		
		
		try {
			boolean rtnFlag = flowSchemaManager.updateRole(flowStepId, formRoleId);
			
			if(rtnFlag) {
				result.put("code", "0000");
				result.put("message", "變更成功！");
			}
			else {
				result.put("code", "0001");
				result.put("message", "變更失敗！");
			}
		}
		catch(Exception e) {
			log.error("err:", e);
			
			result.put("code", "0001");
			result.put("message", "變更失敗！");
		}
		
		return result;
	}
	
	/**
	 * 會簽變更
	 * 
	 * @param request
	 * @param flowStepId
	 * @return
	 */
	@RequestMapping("/updateCountersign")
	public @ResponseBody Map<String, Object> updateCountersign(HttpServletRequest request, 
			@RequestParam("id") Long flowStepId) {
		Map<String, Object> result = Maps.newHashMap();		
		
		try {
			List<FormRole> formRoles = permissionManager.getFormRoles(1);
			FlowStep flowStep = flowManager.findFlowStepById(flowStepId);
			
			result.put("code", "0000");
			result.put("message", "載入成功！");
			result.put("formRoles", formRoles);
			result.put("flowStep", flowStep);			
		}
		catch(Exception e) {
			log.error("err:", e);
			
			result.put("code", "0001");
			result.put("message", "載入失敗！");
		}
		
		return result;
	}
	
	/**
	 * 會簽變更保存
	 * 
	 * @param request
	 * @param flowStepId
	 * @param countersign
	 * @return
	 */
	@RequestMapping("/saveCountersign")
	public @ResponseBody Map<String, Object> saveCountersign(HttpServletRequest request, 
			@RequestParam("id1") Long flowStepId,
			@RequestParam("id2") String countersign) {
		Map<String, Object> result = Maps.newHashMap();		
		
		try {
			boolean rtnFlag = flowSchemaManager.updateCountersign(flowStepId, countersign);
			
			if(rtnFlag) {
				result.put("code", "0000");
				result.put("message", "變更成功！");
			}
			else {
				result.put("code", "0001");
				result.put("message", "變更失敗！");
			}
		}
		catch(Exception e) {
			log.error("err:", e);
			
			result.put("code", "0001");
			result.put("message", "變更失敗！");
		}
		
		return result;
	}
	
	/**
	 * 公司流程
	 * 
	 * @param id
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/flowList")
	@ResponseBody
	public List<FlowSchema> flowList(@RequestParam(required=true) long id,
			HttpServletRequest request) throws Exception {
		PrintUtil.printRequest(this.getClass().getSimpleName(), "flowList", request);
		List<FlowSchema> list = flowSchemaManager.findAllFlowSchema(id);
		
		return list;
	}
}