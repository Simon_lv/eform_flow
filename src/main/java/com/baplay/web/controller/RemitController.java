package com.baplay.web.controller;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baplay.constant.Form;
import com.baplay.entity.BusinessTripExpenseForm;
import com.baplay.entity.PaymentForm;
import com.baplay.entity.RefundForm;
import com.baplay.entity.WorkFlow;
import com.baplay.service.IBusinessTripExpenseFormManager;
import com.baplay.service.ICompanyManager;
import com.baplay.service.IDepartmentManager;
import com.baplay.service.IEmployeeManager;
import com.baplay.service.IFlowManager;
import com.baplay.service.IPaymentFormManager;
import com.baplay.service.IRefundFormManager;
import com.baplay.service.IRemitManager;
import com.baplay.util.DateUtil;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/remit")
public class RemitController extends BaseController {
	
	@Autowired
	private IRemitManager remitManager;
	@Autowired
	private IBusinessTripExpenseFormManager businessTripExpenseFormManager;
	@Autowired
	private IRefundFormManager refundFormManager;
	@Autowired
	private IPaymentFormManager paymentFormManager;
	@Autowired
	private ICompanyManager companyManager;	
	@Autowired
	private IEmployeeManager employeeManager;
	@Autowired
	private IDepartmentManager departmentManager;
	@Autowired
	private IFlowManager flowManager;

	/**
	 * @return
	 */
	protected String getFormType() {
		return StringUtils.EMPTY;
	}
	
	@RequestMapping("/confirmRemitDate")
	public @ResponseBody Map<String, Object> confirmRemitDate(String formType, Long id) {
		Map<String, Object> result = Maps.newHashMap();
		int cnt = remitManager.confirmRemitDate(formType, id);
		
		if(cnt == 1) {
			result.put("code", "0000");
			result.put("message", "保存成功！");
		}
		else {
			result.put("code", "9999");
			result.put("message", "保存失敗！");
		}
		
		return result;
	}
	
	@RequestMapping("/openRemitDate")
	public String openRemitDate(HttpServletRequest request, String formType, Long id) {
		StringBuilder description = new StringBuilder();
		Map<String, String> amounts = new LinkedHashMap<String, String>();
		String remitDate = "";
		String company = "";
		String department = "";
		String employee = "";
		String serialNo = "";
		
		if(Form.PAYMENT.getValue().equalsIgnoreCase(formType)) {
			PaymentForm form = paymentFormManager.findOneById(id);
			
			request.setAttribute("id", form.getId());
			request.setAttribute("formType", formType);
			
			company = companyManager.findOneById(form.getCompanyId()).getName();
			department = departmentManager.findOneById(form.getRequestDepartment().longValue()).getName();
			employee = employeeManager.findOneById(form.getEmployeeId()).getName();
			serialNo = form.getSerialNo();
			remitDate = DateUtil.format(form.getRemitDate(), DateUtil.defaultDatePatternStr);
			amounts.put("憑證總金額", String.valueOf(form.getTotalAmount()));
			
			description.append(String.format("%s 受款銀行[%s] 帳號[%s] 受款人[%s]", 
				"請款單", form.getPayeeBank(), form.getPayeeAccount(), form.getPayee()));
		}
				
		if(Form.BUSINESS_TRIP_EXPENSE.getValue().equalsIgnoreCase(formType)) {
			BusinessTripExpenseForm form = businessTripExpenseFormManager.findOneById(id);
			
			request.setAttribute("id", form.getId());
			request.setAttribute("formType", formType);
			
			company = companyManager.findOneById(form.getCompanyId()).getName();
			department = departmentManager.findOneById(form.getRequestDepartment().longValue()).getName();
			employee = employeeManager.findOneById(form.getEmployeeId()).getName();
			serialNo = form.getSerialNo();
			remitDate = DateUtil.format(form.getRemitDate(), DateUtil.defaultDatePatternStr);
			
			amounts.put("應付金額", String.valueOf(form.getAmountReimbursable()));
						
			description.append(String.format("%s 事由[%s] 日期[%s~%s]", 
					"出差報支單",
					form.getPurpose(),  
					DateUtil.format(form.getDurationStart(), DateUtil.defaultDatePatternStr),
					DateUtil.format(form.getDurationEnd(), DateUtil.defaultDatePatternStr)));			
		}
		
		if(Form.REFUND.getValue().equalsIgnoreCase(formType)) {
			RefundForm form = refundFormManager.findOneById(id);
			WorkFlow flow = flowManager.findFormFlow(form.getWorkFlowId());
			
			request.setAttribute("id", form.getId());
			request.setAttribute("formType", formType);
			
			company = companyManager.findOneById(form.getCompanyId()).getName();
			department = departmentManager.findOneById(flow.getDepartmentId().longValue()).getName();
			employee = employeeManager.findOneByUID(flow.getCreator().intValue()).getName();
			
			serialNo = form.getSerialNo();
			remitDate = DateUtil.format(form.getRemitDate(), DateUtil.defaultDatePatternStr);
						
			amounts.put("儲值金額", String.valueOf(form.getTwCoin()));
			
			description.append(String.format("%s 遊戲[%s] 訂單編號[%s] 信件標題[%s]", 
					"退款單",
					form.getGameName(),  
					form.getOrderId(),
					form.getSubject()));
		}
		
		// Company
		request.setAttribute("company", company);
		// Department
		request.setAttribute("department", department);
		// Employee
		request.setAttribute("employee", employee);
		// SerialNo
		request.setAttribute("serialNo", serialNo);
		// RemitDate
		request.setAttribute("remitDate", remitDate);
		// Amount List
		request.setAttribute("amounts", amounts);
		// Description
		request.setAttribute("description", description.toString());
		
		return "/remit/remitDate";
	}
	
	@RequestMapping("/updateRemitDate")
	public @ResponseBody Map<String, Object> updateRemitDate(String formType, Long id, 
			@DateTimeFormat(pattern="yyyy-MM-dd") Date remitDate) {
		Map<String, Object> result = Maps.newHashMap();
		int cnt = remitManager.updateRemitDate(formType, id, remitDate);
		
		if(cnt == 1) {
			result.put("code", "0000");
			result.put("message", "保存成功！");
		}
		else {
			result.put("code", "9999");
			result.put("message", "保存失敗！");
		}
		
		return result;	
	}
}