package com.baplay.web.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baplay.util.BankNo;

@Controller
@RequestMapping("/bank")
public class BankController extends BaseController {

	/* (non-Javadoc)
	 * @see com.baplay.web.controller.BaseController#getFormType()
	 */
	protected String getFormType() {
		return StringUtils.EMPTY;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping("/twBank")
	public @ResponseBody List<?> twBank(HttpServletRequest request) {
		BankNo bankNo = BankNo.getInstance();
		// sort
		List sortedKeys = new ArrayList(bankNo.getTwBank().keySet());
		Collections.sort(sortedKeys);
		return sortedKeys;	
	}
	
	@RequestMapping("/twBranch")
	public @ResponseBody List<?> branch(HttpServletRequest request, 
			@RequestParam("code") String bankCode) {		
		BankNo bankNo = BankNo.getInstance();
		List branchList = bankNo.getTwBranch(bankCode);
		log.debug("count: " + branchList.size());
		if(branchList.isEmpty()) {
			// bank
			log.debug("no branch: " + bankNo.getTwBank(bankCode));
			branchList.add(bankNo.getTwBank(bankCode));
		}
		
		return branchList;			
	}
}