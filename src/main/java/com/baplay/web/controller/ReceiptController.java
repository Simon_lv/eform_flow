package com.baplay.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import com.baplay.entity.Receipt;
import com.baplay.entity.TEfunuser;
import com.baplay.form.ReceiptForm;
import com.baplay.service.IReceiptManager;
import com.baplay.util.ReportExcel;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/receipt")
public class ReceiptController extends BaseController {
	
	private static final String FUNCTION_NAME = "公司資料";
	private static final int PAGE_SIZE = 25;
	@Autowired
	private IReceiptManager receiptManager;
	
	/* (non-Javadoc)
	 * @see com.baplay.web.controller.BaseController#getFormType()
	 */
	protected String getFormType() {
		return StringUtils.EMPTY;
	}
	
	@RequestMapping("/init")
	public String init() {
		return "/receipt/init";
	}
	
	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		if(id != null) {
			request.setAttribute("cp", receiptManager.findOneById(id));
		}
		
		return "/receipt/edit";
	}
	
	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(Receipt receipt) {	
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;
		
		if(receipt != null) {
			if(receipt.getId() == null || receipt.getId() <= 0) {
				receipt = receiptManager.add(receipt);				
				flag = (receipt!=null);				
			} 
			else {				
				flag = (receiptManager.update(receipt) > 0);			
			}
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");			
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");	
		}
		
		return result;				
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, ReceiptForm receiptForm) {
		receiptForm.setPageSize(PAGE_SIZE);
		receiptForm.setExport(false);		
		Page<Receipt> page = receiptManager.list(receiptForm);
		request.setAttribute("page", page);
		
		return "/receipt/list";
	}
	
	@RequestMapping("/export")
	public void export(HttpServletRequest request, HttpServletResponse response, ReceiptForm receiptForm) {		
		String[] dataTitles = {"ID", "公司名稱", "統編", "地址", "電話", "負責人", "成立時間", "修改者", "修改時間", "建立者", "建立時間"};  
		receiptForm.setExport(true);
		Page<Receipt> page = receiptManager.list(receiptForm);
		List<Object[]> list = new ArrayList<Object[]>();

		if(page.getResult() != null && !page.getResult().isEmpty()) {
			for(Receipt nl : page.getResult()) {
				Object[] o = new Object[dataTitles.length];
				o[0] = nl.getId();
				
				
				list.add(o);
			}
		}
		
		try {
			ReportExcel.reportExcel(request, response, FUNCTION_NAME + System.currentTimeMillis(), "", dataTitles, list);
		} 
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}