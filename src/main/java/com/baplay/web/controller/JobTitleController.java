package com.baplay.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import com.baplay.entity.JobTitle;
import com.baplay.entity.TEfunuser;
import com.baplay.form.JobTitleForm;
import com.baplay.service.ICompanyManager;
import com.baplay.service.IJobTitleManager;
import com.baplay.util.DateUtil;
import com.baplay.util.PrintUtil;
import com.baplay.util.ReportExcel;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/jobTitle")
public class JobTitleController extends BaseController {
	private static final String FUNCTION_NAME = "職稱資料";
	private static final int PAGE_SIZE = 25;
	@Autowired
	private IJobTitleManager jobTitleManager;
	
	@Autowired
	private ICompanyManager companyManager;
	
	/* (non-Javadoc)
	 * @see com.baplay.web.controller.BaseController#getFormType()
	 */
	protected String getFormType() {
		return StringUtils.EMPTY;
	}
	
	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		request.setAttribute("companys", companyManager.findAllCompany());
		return "/jobTitle/init";
	}
	
	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		request.setAttribute("all", jobTitleManager.findAllJobTitle());
		request.setAttribute("company", companyManager.findAllCompany());
		
		if(id != null) {
			request.setAttribute("o", jobTitleManager.findOneById(id));
		}
		
		return "/jobTitle/edit";
	}
	
	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(JobTitle jobTitle) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;
		
		if(jobTitle != null) {		
			if(jobTitleManager.checkSerialNumberExist(jobTitle.getId(), jobTitle.getTitleNo())) {
				result.put("code", "0003");
				result.put("message", "編號已存在！");
				return result;
			}
			
			if(jobTitle.getId() == null || jobTitle.getId() <= 0) {
				jobTitle.setCreator(user.getId());				
				jobTitle = jobTitleManager.add(jobTitle);				
				flag = (jobTitle!=null);				
			} 
			else {
				jobTitle.setModifier(user.getId());
				flag = (jobTitleManager.update(jobTitle) > 0);				
			}
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");			
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");	
		}
		
		return result;
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, JobTitleForm jobTitleForm) {
		jobTitleForm.setPageSize(PAGE_SIZE);
		jobTitleForm.setExport(false);
		Page<JobTitle> page = jobTitleManager.list(jobTitleForm);
		request.setAttribute("page", page);
		
		return "/jobTitle/list";
	}
	
	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> delete(Long id) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;
		
		if(id != null && id > 0) {
			flag = (jobTitleManager.delete(id, user.getId()) > 0);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		
		return result;
	}
	
	@RequestMapping("/export")
	public void export(HttpServletRequest request, HttpServletResponse response, JobTitleForm jobTitleForm) {		
		String[] dataTitles = {"ID", "職稱編號", "職稱", "職級", "職等", "修改者", "修改時間", "建立者", "建立時間"};  
		jobTitleForm.setExport(true);
		Page<JobTitle> page = jobTitleManager.list(jobTitleForm);
		List<Object[]> list = new ArrayList<Object[]>();

		if(page.getResult() != null && !page.getResult().isEmpty()) {
			for(JobTitle nl : page.getResult()) {
				Object[] o = new Object[dataTitles.length];
				o[0] = nl.getId();
				o[1] = nl.getTitleNo();
				o[2] = nl.getName();				
				o[3] = nl.getLevel();
				o[4] = nl.getGrade();
				o[5] = nl.getModifier();
				o[6] = DateUtil.getStringOfDate(nl.getUpdateTime(), null, null);
				o[7] = nl.getCreator();
				o[8] = DateUtil.getStringOfDate(nl.getCreateTime(), null, null);
								
				list.add(o);
			}
		}
		
		try {
			ReportExcel.reportExcel(request, response, FUNCTION_NAME + System.currentTimeMillis(), "", dataTitles, list);
		} 
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/jobList")
	@ResponseBody
	public List<Map<String, Object>> jobList(@RequestParam(required=true) int id,
			HttpServletRequest request) throws Exception {
		PrintUtil.printRequest(this.getClass().getSimpleName(), "jobList", request);
		List<Map<String, Object>> jobList = jobTitleManager.jobList(id);
		log.info("return " + jobList);
		return jobList;
	}
}