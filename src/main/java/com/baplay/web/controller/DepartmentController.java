package com.baplay.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import com.baplay.entity.Department;
import com.baplay.entity.TEfunuser;
import com.baplay.form.DepartmentForm;
import com.baplay.service.ICompanyManager;
import com.baplay.service.IDepartmentManager;
import com.baplay.util.DateUtil;
import com.baplay.util.PrintUtil;
import com.baplay.util.ReportExcel;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/department")
public class DepartmentController extends BaseController {
	private static final String FUNCTION_NAME = "部門資料";
	private static final int PAGE_SIZE = 25;
	@Autowired
	private IDepartmentManager departmentManager;
	@Autowired
	private ICompanyManager companyManager;
	
	/* (non-Javadoc)
	 * @see com.baplay.web.controller.BaseController#getFormType()
	 */
	protected String getFormType() {
		return StringUtils.EMPTY;
	}
	
	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		request.setAttribute("companys", companyManager.findAllCompany());
		return "/department/init";
	}
	
	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		if(id != null) {
			Department department = departmentManager.findOneById(id);
			request.setAttribute("o", department);
			int level = department.getDeptLevel();
			
			List<Department> list = new ArrayList<Department>();
			// 該單位層級以上
			for(int i = 1; i < level; i++) {
				list.addAll(departmentManager.findAllByLevel(i));
			}
			
			request.setAttribute("parents", list);						
		}		
		
		request.setAttribute("companys", companyManager.findAllCompany());
		return "/department/edit";
	}
	
	@RequestMapping("/changeParent")
	public @ResponseBody HashMap<String, Object> changeParent(int level) {
		HashMap<String, Object> result = new HashMap<String, Object>();

		List<Department> list = new ArrayList<Department>();
		
		for(int i = 1; i < level; i++) {
			list.addAll(departmentManager.findAllByLevel(i));
		}
		
		result.put("parent", list);
		return result;
	}
	
	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(Department department) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;
		
		if(department != null) {
			if(departmentManager.checkDeptNoExist(department.getCompanyId(), department.getId(), department.getDeptNo())) {
				result.put("code", "0003");
				result.put("message", "編號已存在！");
				return result;
			}
			
			if(department.getId() == null || department.getId() <= 0) {
				department.setCreator(user.getId());
				department = departmentManager.add(department); 
				flag = (department!=null);				
			} 
			else {
				department.setModifier(user.getId());
				flag = (departmentManager.update(department) > 0);			
			}
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");			
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");	
		}
		
		return result;
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, DepartmentForm departmentForm) {
		departmentForm.setPageSize(PAGE_SIZE);
		departmentForm.setExport(false);
		Page<Department> page = departmentManager.list(departmentForm);
		request.setAttribute("page", page);
		
		return "/department/list";
	}
	
	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> delete(Long id) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;
		
		if(departmentManager.countLiveChild(id) > 0) {
			result.put("code", "0002");
			result.put("message", "請先刪除所有子部門");
			return result;
		}
		
		if(id != null && id > 0) {
			flag = (departmentManager.delete(id, user.getLoginAccount()) > 0);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		
		return result;
	}
	
	@RequestMapping("/export")
	public void export(HttpServletRequest request, HttpServletResponse response, DepartmentForm departmentForm) {		
		String[] dataTitles = {"ID", "部門編號", "部門名稱", "部門階層", "父部門名稱", "公司名稱", "修改者", "修改時間", "建立者", "建立時間"};  
		departmentForm.setExport(true);
		Page<Department> page = departmentManager.list(departmentForm);
		List<Object[]> list = new ArrayList<Object[]>();

		if(page.getResult() != null && !page.getResult().isEmpty()) {
			for(Department nl : page.getResult()) {
				Object[] o = new Object[dataTitles.length];
				o[0] = nl.getId();
				o[1] = nl.getDeptNo();
				o[2] = nl.getName();				
				
				String levelDesc = ""; 
				
				switch(nl.getDeptLevel()) {
					case 1:
						levelDesc = "處";
						break;
					case 2:
						levelDesc = "中心";
						break;
					case 3:	
						levelDesc = "部門";
						break;
					default:
						levelDesc = "" + nl.getDeptLevel();
						break;
				}
				
				o[3] = levelDesc;
				
				o[4] = nl.getParentName();
				o[5] = nl.getCompanyName();
				o[6] = nl.getModifier();
				o[7] = DateUtil.getStringOfDate(nl.getUpdateTime(), null, null);
				o[8] = nl.getCreator();
				o[9] = DateUtil.getStringOfDate(nl.getCreateTime(), null, null);
								
				list.add(o);
			}
		}
		
		try {
			ReportExcel.reportExcel(request, response, FUNCTION_NAME + System.currentTimeMillis(), "", dataTitles, list);
		} 
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/deptList")
	@ResponseBody
	public List<Map<String, Object>> deptList(@RequestParam(required=true) int id,
			HttpServletRequest request) throws Exception {
		PrintUtil.printRequest(this.getClass().getSimpleName(), "deptList", request);
		List<Map<String, Object>> deptList = departmentManager.deptList(id);
		log.info("return " + deptList);
		return deptList;
	}
}