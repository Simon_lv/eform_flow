package com.baplay.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import com.baplay.constant.Form;
import com.baplay.dto.Stamp;
import com.baplay.entity.BusinessTripExpenseForm;
import com.baplay.entity.Employee;
import com.baplay.entity.Receipt;
import com.baplay.entity.TEfunuser;
import com.baplay.entity.WorkFlowDetail;
import com.baplay.form.BusinessTripExpenseFormForm;
import com.baplay.service.IBusinessTripExpenseFormManager;
import com.baplay.service.ICompanyManager;
import com.baplay.service.IEmployeeManager;
import com.baplay.service.IFlowManager;
import com.baplay.service.IFormAuthManager;
import com.baplay.service.INotifyManager;
import com.baplay.service.impl.ReceiptManager;
import com.baplay.util.DateUtil;
import com.baplay.util.PdfUtil;
import com.baplay.util.ReportExcel;
import com.baplay.util.StringUtil;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/businessTripExpenseForm")
public class BusinessTripExpenseFormController extends BaseController {

	private static final String FUNCTION_NAME = "出差報支單";
	private static final int PAGE_SIZE = 25;
	@Autowired
	private IBusinessTripExpenseFormManager businessTripExpenseFormManager;
	@Autowired
	private ICompanyManager companyManager;
	@Autowired
	private IEmployeeManager employeeManager;
	@Autowired
	private ReceiptManager receiptManager;
	@Autowired
	private IFlowManager flowManager;
	@Autowired
	private INotifyManager notifyManager;
	@Autowired
	private IFormAuthManager formAuthManager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.web.controller.BaseController#getFormType()
	 */
	protected String getFormType() {
		return Form.BUSINESS_TRIP_EXPENSE.getValue();
	}

	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		boolean auth = formAuthManager.formAuth(getFormType(), getUserFormRole());
		request.setAttribute("companys", auth ? companyManager.findAllCompany() : getUserCompany());

		return "/businessTripExpenseForm/init";
	}

	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		List<Employee> employeeList = getEmployee() == null ? employeeManager.findAllEmployee() : employeeManager.findAllEmployee(getEmployee().getCompanyId().intValue());
		request.setAttribute("companyEmployee", employeeList);
		request.setAttribute("remitDateItems", DateUtil.getRemitDate(new Date()));

		if(id != null) {
			request.setAttribute("cp", businessTripExpenseFormManager.findOneById(id));
			List<Receipt> receiptCostType1 = receiptManager.findAllByForm(id, this.getFormType(), 1);
			List<Receipt> receiptCostType2 = receiptManager.findAllByForm(id, this.getFormType(), 2);
			request.setAttribute("receiptCostType1", receiptCostType1);
			request.setAttribute("receiptCostType2", receiptCostType2);
		}				

		return "/businessTripExpenseForm/edit";
	}

	@RequestMapping("/get")
	public String get(HttpServletRequest request, Long id, String action) {
		BusinessTripExpenseForm form = businessTripExpenseFormManager.findOneById(id);
		request.setAttribute("cp", form);
		List<Receipt> receiptCostType1 = receiptManager.findAllByForm(id, this.getFormType(), 1, action);
		List<Receipt> receiptCostType2 = receiptManager.findAllByForm(id, this.getFormType(), 2, action);

		request.setAttribute("receiptCostType1", receiptCostType1);
		request.setAttribute("receiptCostType2", receiptCostType2);
		
		Map<String, Stamp> stamp = flowManager.findFormStamp(form.getWorkFlowId());
		request.setAttribute("stamp", stamp);
		
		return "/businessTripExpenseForm/get";
	}

	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request,
			BusinessTripExpenseForm businessTripExpenseForm) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;

		try {
			if (businessTripExpenseForm != null) {
				if (businessTripExpenseForm.getId() == null || businessTripExpenseForm.getId() <= 0) {
					try {
						businessTripExpenseForm = businessTripExpenseFormManager.add(request, businessTripExpenseForm,user,false,getFormType(),getFormType());
					} catch (Exception e) {
						e.printStackTrace();
					}

					flag = (businessTripExpenseForm != null);
				} else {
					try {
						flag = (businessTripExpenseFormManager.update(request, businessTripExpenseForm,user,false,getFormType(),getFormType()) > 0);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			log.error("err:", e);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}

	@RequestMapping("/list")
	public String list(HttpServletRequest request, BusinessTripExpenseFormForm businessTripExpenseFormForm) {
		businessTripExpenseFormForm.setPageSize(PAGE_SIZE);
		businessTripExpenseFormForm.setExport(false);
		
		// Auth
		businessTripExpenseFormForm.setFormRole(getUserFormRole());
		businessTripExpenseFormForm.setLoginUser(getLoginUser().getId());
		businessTripExpenseFormForm.setFormType(getFormType());
		
		Page<BusinessTripExpenseForm> page = businessTripExpenseFormManager.list(businessTripExpenseFormForm);
		request.setAttribute("page", page);

		return "/businessTripExpenseForm/list";
	}

	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> deleteCP(Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;

		if (id != null && id > 0) {
			flag = (businessTripExpenseFormManager.delete(id) > 0);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}
	
	@RequestMapping("/deleteReceipt")
	public @ResponseBody Map<String, Object> deleteReceipt(Long id, Long editId, int costType) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;

		if (id != null && id > 0) {
			flag = (businessTripExpenseFormManager.updateTotal(id, editId, costType, getFormType()) > 0);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}

	@RequestMapping("/export")
	public void export(HttpServletRequest request, HttpServletResponse response,
			BusinessTripExpenseFormForm businessTripExpenseFormForm) {
		// Auth
		businessTripExpenseFormForm.setFormRole(getUserFormRole());
		businessTripExpenseFormForm.setLoginUser(getLoginUser().getId());
		businessTripExpenseFormForm.setFormType(getFormType());
		
		String[] dataTitles = { "表單編號", "出款日", "公司", "部門", "出差人", "申請人", "申請日期", "差旅計畫", "差旅期間(起)", "差旅期間(迄)", "一般費用總計", "交際費總計", "公司卡消費金額", "預支金額", "應付金額", "狀態" };
		businessTripExpenseFormForm.setExport(true);
		Page<BusinessTripExpenseForm> page = businessTripExpenseFormManager.list(businessTripExpenseFormForm);
		List<Object[]> list = new ArrayList<Object[]>();

		if (page.getResult() != null && !page.getResult().isEmpty()) {
			for (BusinessTripExpenseForm nl : page.getResult()) {
				Object[] o = new Object[dataTitles.length];
				o[0] = nl.getSerialNo();
				o[1] = DateUtil.getStringOfDate(nl.getRemitDate(), DateUtil.defaultDatePatternStr, null);
				o[2] = nl.getCompanyName();
				o[3] = nl.getDepartmentName();
				o[4] = nl.getEmployeeName();
				o[5] = nl.getCreatorName();
				o[6] = DateUtil.getStringOfDate(nl.getRequestDate(),DateUtil.defaultDatePatternStr,null);
				o[7] = nl.getPurpose();
				o[8] = DateUtil.getStringOfDate(nl.getDurationStart(),DateUtil.defaultDatePatternStr,null);
				o[9] = DateUtil.getStringOfDate(nl.getDurationEnd(),DateUtil.defaultDatePatternStr,null);
				o[10] = nl.getGenericExpensesTotal();
				o[11] = nl.getEntertainmentExpensesTotal();
				o[12] = nl.getBusinessCardConsumption();
				o[13] = nl.getAmountPrepaid();
				o[14] = nl.getAmountReimbursable();
				o[15] = StringUtil.getFlowStatusDesc(nl.getStatus());
				
				list.add(o);
			}
		}

		try {
			ReportExcel.reportExcel(request, response, FUNCTION_NAME + DateUtil.getCurrentTimeStr("yyyyMMddHHmmss"), "", dataTitles,
					list);
		} catch (Exception e) {
			log.error("err:", e);
		}
	}
	
	/**
	 * 審放頁面
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/process")
	public String process(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		// Flow
		WorkFlowDetail workFlowDetail = flowManager.findFormFlowStep(workFlowDetailId);
		flowStep(request, workFlowDetail);
				
		// Form
		BusinessTripExpenseForm form = businessTripExpenseFormManager.findOneByFlow(workFlowDetail.getWorkFlowId());		
		request.setAttribute("form", form);
		
		// Receipt
		List<Receipt> receiptCostType1 = receiptManager.findAllByForm(form.getId(), this.getFormType(), 1);
		List<Receipt> receiptCostType2 = receiptManager.findAllByForm(form.getId(), this.getFormType(), 2);
		request.setAttribute("receiptCostType1", receiptCostType1);
		request.setAttribute("receiptCostType2", receiptCostType2);
		
		Map<String, Stamp> stamp = flowManager.findFormStamp(form.getWorkFlowId());
		request.setAttribute("stamp", stamp);
		
		return "/businessTripExpenseForm/flow";
	}
	
	/**
	 * 送審
	 * 
	 * @param request
	 * @param paymentForm
	 * @return
	 */
	@RequestMapping("/submit")
	public @ResponseBody Map<String, Object> doSubmit(HttpServletRequest request, BusinessTripExpenseForm businessTripExpenseForm) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;	
		
		try {
			if(businessTripExpenseForm != null) {
				if(businessTripExpenseForm.getId() == null || businessTripExpenseForm.getId() <= 0) {
					businessTripExpenseForm = businessTripExpenseFormManager.add(request, businessTripExpenseForm, user, true, getFormType(), getFormType());				
					flag = (businessTripExpenseForm!=null);			
				} 
				else {				
					flag = (businessTripExpenseFormManager.update(request, businessTripExpenseForm, user, true, getFormType(), getFormType()) > 0);			
				}
				
				if(flag) {
					notifyManager.notify(businessTripExpenseForm.getWorkFlowId());
				}	
			}
		}
		catch(Exception e) {
			log.error("err:", e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "送審成功！");			
		} 
		else {
			result.put("code", "0001");
			result.put("message", "送審失敗！");	
		}
		
		return result;
	}
	
	/**
	 * 審核
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/verify")
	public @ResponseBody Map<String, Object> doVerify(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.verifyFlow(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "審核成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "審核失敗！");
		}
		
		return result;
	}
	
	/**
	 * 放行
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/pass")
	public @ResponseBody Map<String, Object> doPass(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId,
			@RequestParam("remark") String remark) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			// Remark
			if(StringUtils.isNotBlank(remark)) {
				businessTripExpenseFormManager.remark(workFlowId, remark);			
			}	
						
			flowManager.passFlow(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "放行成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "放行失敗！");
		}
		
		return result;
	}
	
	/**
	 * 退件
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/reject")
	public @ResponseBody Map<String, Object> doReject(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId,
			@RequestParam("memo") String memo) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.rejectFlow(workFlowDetailId, user.getId(), memo);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "退件成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "退件失敗！");
		}
		
		return result;
	}
	
	/**
	 * 取消
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/abort")
	public @ResponseBody Map<String, Object> abort(HttpServletRequest request, 
			@RequestParam("id") Long workFlowDetailId) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.lockAbort(workFlowDetailId);
			flag = true;
		}
		catch(Exception e) {
			 log.error("err:", e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "取消成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "取消失敗！");
		}
		
		return result;
	}
	
	/**
	 * PDF
	 * 
	 * @param request
	 * @param response
	 * @param formId
	 */
	@RequestMapping("/pdf")
	public void pdf(HttpServletRequest request, HttpServletResponse response, Long formId) {
		BusinessTripExpenseForm form = businessTripExpenseFormManager.findOneById(formId);
		PdfUtil pdf = new PdfUtil();
		String content = notifyManager.getFormContent(getFormType(), formId, PdfUtil.ACTION);
		pdf.setHtmlContent(pdf.getBaseUrl(request), FUNCTION_NAME, content);
		
		//log.debug(content);
		String fileName = FUNCTION_NAME + form.getSerialNo();
		String subject = String.format("%s %s %s", 
				form.getCreatorName(), DateUtil.format(form.getRequestDate(), DateUtil.chineseDatePatternStr), FUNCTION_NAME);		
		
		try {
			pdf.export(response, fileName, subject);
		} 
		catch(Exception e) {
			log.error("err:" , e);
		}	
	}
}