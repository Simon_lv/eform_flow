package com.baplay.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import com.baplay.entity.GameData;
import com.baplay.form.GameDataForm;
import com.baplay.service.IGameDataManager;
import com.baplay.util.ReportExcel;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/gameData")
public class GameDataController extends BaseController {

	private static final String FUNCTION_NAME = "遊戲資料";
	private static final int PAGE_SIZE = 25;
	@Autowired
	private IGameDataManager gameDataManager;	

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.web.controller.BaseController#getFormType()
	 */
	protected String getFormType() {
		return StringUtils.EMPTY;
	}

	@RequestMapping("/init")
	public String init() {		
		return "/gameData/init";
	}

	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {		
		if (id != null) {
			request.setAttribute("cp", gameDataManager.findOneById(id));			
		}

		return "/gameData/edit";
	}

	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, GameData gameData) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;

		try {
			if (gameData != null) {
				if (gameData.getId() == null || gameData.getId() <= 0) {					
					gameData = gameDataManager.add(gameData);
					flag = (gameData != null);
				} else {
					flag = (gameDataManager.update(gameData) > 0);
				}
			}
		} catch (Exception e) {
			log.error("err:", e);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}

	@RequestMapping("/list")
	public String list(HttpServletRequest request, GameDataForm gameDataForm) {
		gameDataForm.setPageSize(PAGE_SIZE);
		gameDataForm.setExport(false);
		Page<GameData> page = gameDataManager.list(gameDataForm);
		request.setAttribute("page", page);
		return "/gameData/list";
	}	

	@RequestMapping("/export")
	public void export(HttpServletRequest request, HttpServletResponse response, GameDataForm gameDataForm) {
		String[] dataTitles = { "ID", "	遊戲名稱", "狀態" };
		gameDataForm.setExport(true);
		Page<GameData> page = gameDataManager.list(gameDataForm);
		List<Object[]> list = new ArrayList<Object[]>();

		if (page.getResult() != null && !page.getResult().isEmpty()) {
			for (GameData nl : page.getResult()) {
				Object[] o = new Object[dataTitles.length];
				o[0] = nl.getId();
				o[1] = nl.getName();
				switch(nl.getStatus()){
				case 1:
					o[2] = "正常";
					break;
				case 2:
					o[2] = "停權";
					break;
				}
				list.add(o);
			}
		}

		try {
			ReportExcel.reportExcel(request, response, FUNCTION_NAME + System.currentTimeMillis(), "", dataTitles,
					list);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}