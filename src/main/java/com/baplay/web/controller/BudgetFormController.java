package com.baplay.web.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import com.baplay.constant.Form;
import com.baplay.dto.Stamp;
import com.baplay.entity.BudgetForm;
import com.baplay.entity.TEfunuser;
import com.baplay.entity.WorkFlowDetail;
import com.baplay.form.BudgetFormForm;
import com.baplay.infra.utils.DateUtil;
import com.baplay.service.IBudgetFormDetailManager;
import com.baplay.service.IBudgetFormManager;
import com.baplay.service.ICompanyManager;
import com.baplay.service.IEmployeeManager;
import com.baplay.service.IFlowManager;
import com.baplay.service.IFormAuthManager;
import com.baplay.service.INotifyManager;
import com.baplay.util.ReportExcel;
import com.baplay.util.StringUtil;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/budgetForm")
public class BudgetFormController extends BaseController {

	private static final String FUNCTION_NAME = "部門固定成本預算表";
	private static final int PAGE_SIZE = 25;
	@Autowired
	private IBudgetFormManager budgetFormManager;
	@Autowired
	private ICompanyManager companyManager;
	@Autowired
	private IEmployeeManager employeeManager;
	@Autowired
	private IFlowManager flowManager;
	@Autowired
	private IBudgetFormDetailManager budgetFormDetailManager;
	@Autowired
	private INotifyManager notifyManager;
	@Autowired
	private IFormAuthManager formAuthManager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.web.controller.BaseController#getFormType()
	 */
	protected String getFormType() {
		return Form.BUDGET.getValue();
	}

	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		boolean auth = formAuthManager.formAuth(getFormType(), getUserFormRole());
		request.setAttribute("companys", auth ? companyManager.findAllCompany() : getUserCompany());
		return "/budgetForm/init";
	}

	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id, boolean additional) {
		TEfunuser user = getLoginUser();

		if(id != null){
			Long tmpId = id;		
			ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();		

			BudgetForm budgetForm = budgetFormManager.findOneById(tmpId);			
			while (budgetForm != null) {				
				Map<String, Object> tmpMap = new HashMap<String, Object>();				
				
				tmpMap.put("cp", budgetForm);
				tmpMap.put("notAdditional", budgetFormDetailManager.findFormDetail(tmpId, 2));
				tmpMap.put("isAdditional", budgetFormDetailManager.findFormDetail(tmpId, 1));
				
				Map<String, Stamp> stamp = flowManager.findFormStamp(budgetForm.getWorkFlowId());
				tmpMap.put("stamp", stamp);
				
				list.add(tmpMap);
				
				tmpId = budgetForm.getAdditionalReferenceId();
				
				if(tmpId == null) {
					break;
				}
				
				budgetForm = budgetFormManager.findOneById(tmpId);
			}
			request.setAttribute("id", ((BudgetForm)list.get(0).get("cp")).getId());
			request.setAttribute("additionalReferenceId", ((BudgetForm)list.get(0).get("cp")).getAdditionalReferenceId());
			request.setAttribute("workFlowId", ((BudgetForm)list.get(0).get("cp")).getWorkFlowId());
			request.setAttribute("serialNo", ((BudgetForm)list.get(0).get("cp")).getSerialNo());
			Collections.reverse(list);
			request.setAttribute("budgetForm", list);
		}else{
			request.setAttribute("employee", employeeManager.findOneById(user.getEmployeeId()));
		}
	
		request.setAttribute("additional", additional);
		return "/budgetForm/edit";
	}
	
	@RequestMapping("/get")
	public String get(HttpServletRequest request, Long id) {
		
		Long tmpId = id;		
		ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();		

		BudgetForm budgetForm = budgetFormManager.findOneById(tmpId);
		
		while (budgetForm != null) {			
			Map<String, Object> tmpMap = new HashMap<String, Object>();				
			
			tmpMap.put("cp", budgetForm);
			tmpMap.put("notAdditional", budgetFormDetailManager.findFormDetail(tmpId, 2));
			tmpMap.put("isAdditional", budgetFormDetailManager.findFormDetail(tmpId, 1));
			
			Map<String, Stamp> stamp = flowManager.findFormStamp(budgetForm.getWorkFlowId());
			tmpMap.put("stamp", stamp);
			
			list.add(tmpMap);
			
			tmpId = budgetForm.getAdditionalReferenceId();
			
			if(tmpId == null) {
				break;
			}
			
			budgetForm = budgetFormManager.findOneById(tmpId);
		}
		
		request.setAttribute("id", ((BudgetForm)list.get(0).get("cp")).getId());
		request.setAttribute("workFlowId", ((BudgetForm)list.get(0).get("cp")).getWorkFlowId());						
		Collections.reverse(list);
		request.setAttribute("budgetForm", list);			

		return "/budgetForm/get";
	}

	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, BudgetForm budgetForm,
			boolean additional) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;

		try {
			if (budgetForm != null) {
				if (additional) {// 追加
					if (budgetForm.getAdditionalReferenceId() == null) {// 追加未存檔
						BudgetForm oldbf = budgetFormManager.findOneById(budgetForm.getId());
						budgetForm.setYearMonth(oldbf.getYearMonth());
						budgetForm.setAdditionalReferenceId(budgetForm.getId());
						budgetForm.setWorkFlowId(null);
						budgetForm = budgetFormManager.add(request, budgetForm, user, false, additional, getFormType(),
								getFormType());
						flag = (budgetForm != null);
					} else {// 追加編輯
						flag = (budgetFormManager.update(request, budgetForm, user, false, additional, getFormType(),
								getFormType()) > 0);
					}
				} else {
					if (budgetForm.getId() == null || budgetForm.getId() <= 0) {
						budgetForm = budgetFormManager.add(request, budgetForm, user, false, additional, getFormType(),
								getFormType());
						flag = (budgetForm != null);
					} else {
						flag = (budgetFormManager.update(request, budgetForm, user, false, additional, getFormType(),
								getFormType()) > 0);
					}
				}
			}
		} catch (Exception e) {
			log.error("err:", e);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}

	@RequestMapping("/list")
	public String list(HttpServletRequest request, BudgetFormForm budgetFormForm) {
		budgetFormForm.setPageSize(PAGE_SIZE);
		budgetFormForm.setExport(false);
		
		// Auth
		budgetFormForm.setFormRole(getUserFormRole());
		budgetFormForm.setLoginUser(getLoginUser().getId());
		budgetFormForm.setFormType(getFormType());
		
		Page<BudgetForm> page = budgetFormManager.list(budgetFormForm);
		request.setAttribute("page", page);
		return "/budgetForm/list";
	}

	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> deleteCP(Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;

		if (id != null && id > 0) {
			flag = (budgetFormManager.delete(id) > 0);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}

	@RequestMapping("/deleteDetail")
	public @ResponseBody Map<String, Object> deleteDetail(Long id, Long editId) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;

		if (editId != null && editId > 0) {
			flag = (budgetFormDetailManager.delete(id, editId) > 0);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}

	@RequestMapping("/export")
	public void export(HttpServletRequest request, HttpServletResponse response, BudgetFormForm budgetFormForm) {
		// Auth
		budgetFormForm.setFormRole(getUserFormRole());
		budgetFormForm.setLoginUser(getLoginUser().getId());
		budgetFormForm.setFormType(getFormType());			
		
		String[] dataTitles = { "表單編號", "公司名稱", "部門", "申請人", "申請日期", "預算年月", "預算總金額", "追加總金額", "追加預算原因", "狀態" };
		budgetFormForm.setExport(true);
		Page<BudgetForm> page = budgetFormManager.list(budgetFormForm);
		List<Object[]> list = new ArrayList<Object[]>();

		if (page.getResult() != null && !page.getResult().isEmpty()) {
			for (BudgetForm nl : page.getResult()) {
				Object[] o = new Object[dataTitles.length];
				o[0] = nl.getSerialNo();
				o[1] = nl.getCompanyName();
				o[2] = nl.getDepartmentName();
				o[3] = nl.getCreatorName();
				o[4] = DateUtil.getStringOfDate(nl.getRequestDate(), DateUtil.defaultDatePatternStr, null);
				o[5] = nl.getYearMonth();
				
				if(nl.getAdditionalReferenceId() == null) {
					o[6] = nl.getTotalAmount();
					o[7] = "0";
				}
				else {
					o[6] = nl.getAdditionalAmount();
					o[7] = nl.getTotalAmount();
				}				
				
				o[8] = nl.getAdditionalReason();
				o[9] = StringUtil.getFlowStatusDesc(nl.getStatus());

				list.add(o);
			}
		}

		try {
			ReportExcel.reportExcel(request, response, FUNCTION_NAME + DateUtil.getCurrentTimeStr("yyyyMMddHHmmss"), "", dataTitles,
					list);
		} catch (Exception e) {
			log.error("err:" , e);
		}
	}

	/**
	 * 審放頁面
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/process")
	public String process(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		// Flow
		WorkFlowDetail workFlowDetail = flowManager.findFormFlowStep(workFlowDetailId);
		flowStep(request, workFlowDetail);
		
		BudgetForm bf = budgetFormManager.findOneByFlow(workFlowDetail.getWorkFlowId());
		
		Long tmpId = bf.getId();	
		ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();		

		BudgetForm budgetForm = budgetFormManager.findOneById(tmpId);
		
		while (budgetForm != null) {			
			Map<String, Object> tmpMap = new HashMap<String, Object>();				
			
			tmpMap.put("cp", budgetForm);
			tmpMap.put("notAdditional", budgetFormDetailManager.findFormDetail(tmpId, 2));
			tmpMap.put("isAdditional", budgetFormDetailManager.findFormDetail(tmpId, 1));
			
			Map<String, Stamp> stamp = flowManager.findFormStamp(budgetForm.getWorkFlowId());
			tmpMap.put("stamp", stamp);
			
			list.add(tmpMap);
			
			tmpId = budgetForm.getAdditionalReferenceId();
			
			if(tmpId == null) {
				break;
			}
			
			budgetForm = budgetFormManager.findOneById(tmpId);
		}
		
		Collections.reverse(list);
		request.setAttribute("budgetForm", list);

		return "/budgetForm/flow";
	}

	/**
	 * 送審
	 * 
	 * @param request
	 * @param paymentForm
	 * @return
	 */
	@RequestMapping("/submit")
	public @ResponseBody Map<String, Object> doSubmit(HttpServletRequest request, BudgetForm budgetForm,
			boolean additional) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;

		try {
			if (budgetForm != null) {
				if (additional) {// 追加
					if (budgetForm.getAdditionalReferenceId() == null) {// 追加未存檔
						BudgetForm oldbf = budgetFormManager.findOneById(budgetForm.getId());
						budgetForm.setYearMonth(oldbf.getYearMonth());
						budgetForm.setAdditionalReferenceId(budgetForm.getId());
						budgetForm.setWorkFlowId(null);
						budgetForm = budgetFormManager.add(request, budgetForm, user, true, additional, getFormType(),
								getFormType());
						flag = (budgetForm != null);
					} else {// 追加編輯
						flag = (budgetFormManager.update(request, budgetForm, user, true, additional, getFormType(),
								getFormType()) > 0);
					}
				} else {
					if (budgetForm.getId() == null || budgetForm.getId() <= 0) {
						budgetForm = budgetFormManager.add(request, budgetForm, user, true, additional, getFormType(),
								getFormType());
						flag = (budgetForm != null);
					} else {
						flag = (budgetFormManager.update(request, budgetForm, user, true, additional, getFormType(),
								getFormType()) > 0);
					}
				}
				
				if(flag) {
					notifyManager.notify(budgetForm.getWorkFlowId());
				}
			}
		} catch (Exception e) {
			log.error("err:", e);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "送審成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "送審失敗！");
		}

		return result;
	}

	/**
	 * 審核
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/verify")
	public @ResponseBody Map<String, Object> doVerify(HttpServletRequest request,
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;

		try {
			flowManager.verifyFlow(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
			
			flag = true;
		} catch (Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "審核成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "審核失敗！");
		}

		return result;
	}
	
	/**
	 * 會簽
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/countersign")
	public @ResponseBody Map<String, Object> doCountersign(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.countersign(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			 log.error("err:", e);
			 releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "會簽成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "會簽失敗！");
		}
		
		return result;
	}

	/**
	 * 放行
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/pass")
	public @ResponseBody Map<String, Object> doPass(HttpServletRequest request,
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;

		try {
			flowManager.passFlow(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
			
			flag = true;
		} catch (Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "放行成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "放行失敗！");
		}

		return result;
	}

	/**
	 * 退件
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/reject")
	public @ResponseBody Map<String, Object> doReject(HttpServletRequest request,
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId, 
			@RequestParam("memo") String memo) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;

		try {
			flowManager.rejectFlow(workFlowDetailId, user.getId(), memo);
			notifyManager.notify(workFlowId);
			
			flag = true;
		} catch (Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "退件成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "退件失敗！");
		}

		return result;
	}

	/**
	 * 取消
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/abort")
	public @ResponseBody Map<String, Object> abort(HttpServletRequest request,
			@RequestParam("id") Long workFlowDetailId) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;

		try {
			flowManager.lockAbort(workFlowDetailId);
			flag = true;
		} catch (Exception e) {
			log.error("err:", e);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "取消成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "取消失敗！");
		}

		return result;
	}

}