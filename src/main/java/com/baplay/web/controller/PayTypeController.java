package com.baplay.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import com.baplay.entity.PayType;
import com.baplay.entity.TEfunuser;
import com.baplay.form.PayTypeForm;
import com.baplay.service.IPayTypeManager;
import com.baplay.util.ReportExcel;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/payType")
public class PayTypeController extends BaseController {

	private static final String FUNCTION_NAME = "金流資料";
	private static final int PAGE_SIZE = 25;
	@Autowired
	private IPayTypeManager payTypeManager;	

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.web.controller.BaseController#getFormType()
	 */
	protected String getFormType() {
		return StringUtils.EMPTY;
	}

	@RequestMapping("/init")
	public String init() {		
		return "/payType/init";
	}

	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		TEfunuser user = getLoginUser();

		if (id != null) {
			request.setAttribute("cp", payTypeManager.findOneById(id));			
		}

		return "/payType/edit";
	}

	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, PayType payType) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;

		try {
			if (payType != null) {
				if (payType.getId() == null || payType.getId() <= 0) {					
					payType = payTypeManager.add(payType);
					flag = (payType != null);
				} else {
					flag = (payTypeManager.update(payType) > 0);
				}
			}
		} catch (Exception e) {
			log.error("err:", e);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}

	@RequestMapping("/list")
	public String list(HttpServletRequest request, PayTypeForm payTypeForm) {
		payTypeForm.setPageSize(PAGE_SIZE);
		payTypeForm.setExport(false);
		Page<PayType> page = payTypeManager.list(payTypeForm);
		request.setAttribute("page", page);
		return "/payType/list";
	}

	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> deleteCP(Long id) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;

		if (id != null && id > 0) {
			flag = (payTypeManager.delete(id) > 0);
		}

		if (flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}

	@RequestMapping("/export")
	public void export(HttpServletRequest request, HttpServletResponse response, PayTypeForm payTypeForm) {
		String[] dataTitles = { "ID", "	金流名稱", "狀態" };
		payTypeForm.setExport(true);
		Page<PayType> page = payTypeManager.list(payTypeForm);
		List<Object[]> list = new ArrayList<Object[]>();

		if (page.getResult() != null && !page.getResult().isEmpty()) {
			for (PayType nl : page.getResult()) {
				Object[] o = new Object[dataTitles.length];
				o[0] = nl.getId();
				o[1] = nl.getName();
				switch(nl.getStatus()){
				case 1:
					o[2] = "正常";
					break;
				case 2:
					o[2] = "停權";
					break;
				}
				list.add(o);
			}
		}

		try {
			ReportExcel.reportExcel(request, response, FUNCTION_NAME + System.currentTimeMillis(), "", dataTitles,
					list);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}