package com.baplay.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springside.modules.orm.Page;

import com.baplay.constant.Config;
import com.baplay.constant.Form;
import com.baplay.constant.URL;
import com.baplay.dto.Stamp;
import com.baplay.entity.Attachment;
import com.baplay.entity.Employee;
import com.baplay.entity.PaymentForm;
import com.baplay.entity.Receipt;
import com.baplay.entity.TEfunuser;
import com.baplay.entity.WorkFlowDetail;
import com.baplay.form.PaymentFormForm;
import com.baplay.service.IAttachmentManager;
import com.baplay.service.ICompanyManager;
import com.baplay.service.IEmployeeManager;
import com.baplay.service.IFlowManager;
import com.baplay.service.IFormAuthManager;
import com.baplay.service.INotifyManager;
import com.baplay.service.IPaymentFormManager;
import com.baplay.service.IReceiptManager;
import com.baplay.util.DateUtil;
import com.baplay.util.HandleFileUpload;
import com.baplay.util.PdfUtil;
import com.baplay.util.ReportExcel;
import com.baplay.util.StringUtil;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/paymentForm")
public class PaymentFormController extends BaseController {
	
	private static final String FUNCTION_NAME = "請款單";
	private static final int PAGE_SIZE = 25;
	@Autowired
	private IPaymentFormManager paymentFormManager;	
	@Autowired
	private IReceiptManager receiptManager;
	@Autowired
	private ICompanyManager companyManager;
	@Autowired
	private IEmployeeManager employeeManager;
	@Autowired
	private IFlowManager flowManager;
	@Autowired
	private INotifyManager notifyManager;
	@Autowired
	private IFormAuthManager formAuthManager;
	@Autowired
	private IAttachmentManager attachmentManager;
	
	/* (non-Javadoc)
	 * @see com.baplay.web.controller.BaseController#getFormType()
	 */
	protected String getFormType() {
		return Form.PAYMENT.getValue();
	}
	
	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		boolean auth = formAuthManager.formAuth(getFormType(), getUserFormRole());
		request.setAttribute("companys", auth ? companyManager.findAllCompany() : getUserCompany());
		return "/paymentForm/init";
	}
	
	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		List<Employee> employeeList = getEmployee() == null ? employeeManager.findAllEmployee() : employeeManager.findAllEmployee(getEmployee().getCompanyId().intValue());
		request.setAttribute("companyEmployee", employeeList);
		request.setAttribute("remitDateItems", DateUtil.getRemitDate(new Date()));
		
		if(id != null) {
			request.setAttribute("cp", paymentFormManager.findOneById(id));
			List<Receipt> receipt = receiptManager.findAllByForm(id, this.getFormType(), 1);
			
			request.setAttribute("receipts", receipt);
			
			List<Attachment> attachment = attachmentManager.findFormAttachment(id, getFormType());
			request.setAttribute("attachment", attachment);
		}
		
		return "/paymentForm/edit";
	}
		
	@RequestMapping("/get")
	public String get(HttpServletRequest request, Long id, String action) {
		PaymentForm form = paymentFormManager.findOneById(id);
		request.setAttribute("cp", form);
		
		Map<String, Stamp> stamp = flowManager.findFormStamp(form.getWorkFlowId());
		request.setAttribute("stamp", stamp);
		
		List<Receipt> receipt = receiptManager.findAllByForm(id, this.getFormType(), 1, action);			
		request.setAttribute("receipts", receipt);
		
		// 附件
		List<Attachment> attachment = attachmentManager.findFormAttachment(id, getFormType());
		request.setAttribute("attachment", attachment);
		
		return "/paymentForm/get";
	}
	
	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, PaymentForm paymentForm) {	
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;
		
		try {
			if(paymentForm != null) {
				if(paymentForm.getId() == null || paymentForm.getId() <= 0) {
					paymentForm = paymentFormManager.add(request,paymentForm,user,false,getFormType(),getFormType());				
					
					flag = (paymentForm!=null);			
				} 
				else {				
					flag = paymentFormManager.update(request,paymentForm,user,false,getFormType(),getFormType()) > 0;			
				}
			}
		}
		catch(Exception e) {
			log.error("err:", e);
		}	
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");			
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");	
		}
		
		return result;				
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, PaymentFormForm paymentFormForm) {
		paymentFormForm.setPageSize(PAGE_SIZE);
		paymentFormForm.setExport(false);
		// Auth
		paymentFormForm.setFormRole(getUserFormRole());
		paymentFormForm.setLoginUser(getLoginUser().getId());
		paymentFormForm.setFormType(getFormType());
		paymentFormForm.setEmployeeIdList(getUserAllEmployeeId());
		
		Page<PaymentForm> page = paymentFormManager.list(paymentFormForm);
		request.setAttribute("page", page);
		
		return "/paymentForm/list";
	}
	
	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> delete(Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		if(id != null && id > 0) {
			flag = (paymentFormManager.delete(id, getFormType()) > 0);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		
		return result;
	}
	
	@RequestMapping("/export")
	public void export(HttpServletRequest request, HttpServletResponse response, PaymentFormForm paymentFormForm) {		
		// Auth
		paymentFormForm.setFormRole(getUserFormRole());
		paymentFormForm.setLoginUser(getLoginUser().getId());
		paymentFormForm.setFormType(getFormType());
		paymentFormForm.setEmployeeIdList(getUserAllEmployeeId());				
		
		String[] dataTitles = {"表單編號", "公司", "部門", "申請日期", "申請人", "出款日", "付款方式", "受款人", "受款人銀行", "受款人帳號", "銀行交換代號", "附言或指示", "憑證總金額", "狀態"};  
		paymentFormForm.setExport(true);
		paymentFormForm.setFormRole(getUserFormRole());
		paymentFormForm.setLoginUser(getLoginUser().getId());
		paymentFormForm.setFormType(getFormType());
		paymentFormForm.setEmployeeIdList(getUserAllEmployeeId());
		
		Page<PaymentForm> page = paymentFormManager.list(paymentFormForm);
		List<Object[]> list = new ArrayList<Object[]>();

		if(page.getResult() != null && !page.getResult().isEmpty()) {
			for(PaymentForm nl : page.getResult()) {
				Object[] o = new Object[dataTitles.length];
				o[0] = nl.getSerialNo();
				o[1] = nl.getCompanyName();
				o[2] = nl.getDepartmentName();
				o[3] = DateUtil.getStringOfDate(nl.getRequestDate(),DateUtil.defaultDatePatternStr, null);
				o[4] = nl.getCreatorName();
				o[5] = DateUtil.getStringOfDate(nl.getRemitDate(), DateUtil.defaultDatePatternStr, null);
				
				switch(nl.getPaymentWay()){
					case 1:
						o[6] = "現金";
						break;
					case 2:
						o[6] = "匯款";
						break;
					case 3:
						o[6] = "支票";
						break;
					case 4:
						o[6] = "沖預付";
						break;
				}
				
				o[7] = nl.getPayee();
				o[8] = nl.getPayeeBank();
				o[9] = nl.getPayeeAccount();
				o[10] = nl.getBankClearingCode();
				o[11] = nl.getMessage();
				o[12] = nl.getTotalAmount();
				o[13] = StringUtil.getFlowStatusDesc(nl.getStatus());
						
				list.add(o);
			}
		}
		
		try {
			ReportExcel.reportExcel(request, response, FUNCTION_NAME + DateUtil.getCurrentTimeStr("yyyyMMddHHmmss"), "", dataTitles, list);
		} 
		catch(Exception e) {
			log.error("err:" , e);
		}	
	}
	
	/**
	 * 審放頁面
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/process")
	public String process(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		// Flow
		WorkFlowDetail workFlowDetail = flowManager.findFormFlowStep(workFlowDetailId);
		flowStep(request, workFlowDetail);
				
		// Payment
		PaymentForm form = paymentFormManager.findOneByFlow(workFlowDetail.getWorkFlowId());		
		request.setAttribute("form", form);
		
		// Receipt
		List<Receipt> receipt = receiptManager.findAllByForm(form.getId(), this.getFormType(), 1);
		request.setAttribute("receipts", receipt);
		
		// 附件
		List<Attachment> attachment = attachmentManager.findFormAttachment(form.getId(), getFormType());
		request.setAttribute("attachment", attachment);
		
		Map<String, Stamp> stamp = flowManager.findFormStamp(form.getWorkFlowId());
		request.setAttribute("stamp", stamp);
		
		return "/paymentForm/flow";
	}
	
	/**
	 * 送審
	 * 
	 * @param request
	 * @param paymentForm
	 * @return
	 */
	@RequestMapping("/submit")
	public @ResponseBody Map<String, Object> doSubmit(HttpServletRequest request, PaymentForm paymentForm) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;				
		
		try {
			if(paymentForm != null) {
				if(paymentForm.getId() == null || paymentForm.getId() <= 0) {
					paymentForm = paymentFormManager.add(request, paymentForm, user, true, getFormType(), getFormType());				
					flag = (paymentForm!=null);			
				} 
				else {				
					flag = (paymentFormManager.update(request, paymentForm, user, true, getFormType(), getFormType()) > 0);			
				}
				
				if(flag) {
					notifyManager.notify(paymentForm.getWorkFlowId());
				}	
			}
		}
		catch(Exception e) {
			log.error("err:", e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "送審成功！");			
		} 
		else {
			result.put("code", "0001");
			result.put("message", "送審失敗！");	
		}
		
		return result;
	}
	
	/**
	 * 審核
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/verify")
	public @ResponseBody Map<String, Object> doVerify(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.verifyFlow(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "審核成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "審核失敗！");
		}
		
		return result;
	}
	
	/**
	 * 放行
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/pass")
	public @ResponseBody Map<String, Object> doPass(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.passFlow(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
						
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "放行成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "放行失敗！");
		}
		
		return result;
	}
	
	/**
	 * 退件
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/reject")
	public @ResponseBody Map<String, Object> doReject(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId, 
			@RequestParam("memo") String memo) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.rejectFlow(workFlowDetailId, user.getId(), memo);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "退件成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "退件失敗！");
		}
		
		return result;
	}
	
	/**
	 * 取消
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/abort")
	public @ResponseBody Map<String, Object> abort(HttpServletRequest request, 
			@RequestParam("id") Long workFlowDetailId) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.lockAbort(workFlowDetailId);
			flag = true;
		}
		catch(Exception e) {
			 log.error("err:", e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "取消成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "取消失敗！");
		}
		
		return result;
	}
	
	/**
	 * 附件上傳
	 * 
	 * @param file
	 * @param json
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody HashMap<String, String> handleFileUpload(@RequestParam("file") MultipartFile file,
			@RequestParam(value = "json", required = false, defaultValue = "") String json) throws IOException, Exception {		
		HashMap<String, String> result = new HashMap<String, String>();
		log.debug("[handleFileUpload][filename=" + file.getOriginalFilename() + "]");
		String url = HandleFileUpload.upload(file, URL.URL_IMG_PAYMENTFORM.toString(), URL.FTP_IMG_PAYMENTFORM.toString(), Config.UPLOAD_FILE_LIMIT);
		
		if(StringUtils.isNotEmpty(json)) {
			log.debug("json:" + json);
			JSONObject jsonObj = new JSONObject(json);
			log.debug("jsonObj:" + jsonObj.toString());
		}
		
		result.put("url", url);
		result.put("description", file.getOriginalFilename());
		
		return result;
	}
	
	/**
	 * 附件刪除
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping("/deleteAttachment")
	public @ResponseBody Map<String, Object> deleteAttachment(Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;

		if (id != null && id > 0) {
			flag = attachmentManager.delete(id) > 0;			
			// TODO remove CDN file
		}

		if(flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}
	
	/**
	 * PDF
	 * 
	 * @param request
	 * @param response
	 * @param formId
	 */
	@RequestMapping("/pdf")
	public void pdf(HttpServletRequest request, HttpServletResponse response, Long formId) {
		PaymentForm form = paymentFormManager.findOneById(formId);
		PdfUtil pdf = new PdfUtil();
		
		String content = notifyManager.getFormContent(getFormType(), formId, PdfUtil.ACTION);
		pdf.setHtmlContent(pdf.getBaseUrl(request), FUNCTION_NAME, content);
		
		//log.debug(content);
		String fileName = FUNCTION_NAME + form.getSerialNo();
		String subject = String.format("%s %s %s", 
				form.getCreatorName(), DateUtil.format(form.getRequestDate(), DateUtil.chineseDatePatternStr), FUNCTION_NAME);		
		
		try {
			pdf.export(response, fileName, subject);
		} 
		catch(Exception e) {
			log.error("err:" , e);
		}	
	}	
}