package com.baplay.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import com.baplay.constant.Form;
import com.baplay.dto.Stamp;
import com.baplay.entity.RefundForm;
import com.baplay.entity.TEfunuser;
import com.baplay.entity.WorkFlowDetail;
import com.baplay.exception.IllegalException;
import com.baplay.form.RefundFormForm;
import com.baplay.service.ICompanyManager;
import com.baplay.service.IFlowManager;
import com.baplay.service.IFormAuthManager;
import com.baplay.service.IGameDataManager;
import com.baplay.service.INotifyManager;
import com.baplay.service.IPayTypeManager;
import com.baplay.service.IRefundFormManager;
import com.baplay.util.DateUtil;
import com.baplay.util.PdfUtil;
import com.baplay.util.ReportExcel;
import com.baplay.util.StringUtil;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/refundForm")
public class RefundFormController extends BaseController {
	
	private static final String FUNCTION_NAME = "退款申請單";
	private static final int PAGE_SIZE = 25;
	@Autowired
	private IRefundFormManager refundFormManager;
	@Autowired
	private ICompanyManager companyManager;
	@Autowired
	private IFlowManager flowManager;
	@Autowired
	private IGameDataManager gameDataManager;
	@Autowired
	private IPayTypeManager payTypeManager;
	@Autowired
	private INotifyManager notifyManager;
	@Autowired
	private IFormAuthManager formAuthManager;
	
	/* (non-Javadoc)
	 * @see com.baplay.web.controller.BaseController#getFormType()
	 */
	protected String getFormType() {
		return Form.REFUND.getValue();
	}
	
	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		boolean auth = formAuthManager.formAuth(getFormType(), getUserFormRole());
		request.setAttribute("companys", auth ? companyManager.findAllCompany() : getUserCompany());
		return "/refundForm/init";
	}
	
	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		request.setAttribute("companys", companyManager.findAllGameCompany());
		request.setAttribute("allGameData", gameDataManager.findAllGameData());
		request.setAttribute("allPayType", payTypeManager.findAllPayType());
		request.setAttribute("remitDateItems", DateUtil.getRemitDate(new Date()));
		
		if(id != null) {
			request.setAttribute("cp", refundFormManager.findOneById(id));
		}
		
		return "/refundForm/edit";
	}
	
	@RequestMapping("/get")
	public String get(HttpServletRequest request, Long id) {
		RefundForm form = refundFormManager.findOneById(id);
		request.setAttribute("cp", form);
		
		Map<String, Stamp> stamp = flowManager.findFormStamp(form.getWorkFlowId());
		request.setAttribute("stamp", stamp);
		
		request.setAttribute("allGameData", gameDataManager.findAllGameData());
		request.setAttribute("allPayType", payTypeManager.findAllPayType());
		return "/refundForm/get";
	}
	
	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, RefundForm refundForm) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;
		
		String errMsg = "";
		String rtnCode = "";
		
		try {
			if(refundForm != null) {
				if(refundForm.getId() == null || refundForm.getId() <= 0) {				
					refundForm = refundFormManager.add(refundForm,user,false,getFormType(),getFormType());
					
					flag = (refundForm!=null);				
				} 
				else {				
					flag = (refundFormManager.update(refundForm,user,false,getFormType(),getFormType()) > 0);			
				}
			}
		}
		catch(IllegalException e) {
			log.error("err:", e);
			
			rtnCode = e.getErrCode();
			errMsg = e.getErrMsg();
		}
		catch(Exception e) {
			log.error("err:", e);
			
			rtnCode = "0001";
			errMsg = e.getLocalizedMessage();
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");			
		} 
		else {
			result.put("code", rtnCode);
			result.put("message", String.format("保存失敗！(%s)", errMsg));	
		}
		
		return result;				
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, RefundFormForm refundFormForm) {
		refundFormForm.setPageSize(PAGE_SIZE);
		refundFormForm.setExport(false);
		
		// Auth
		refundFormForm.setFormRole(getUserFormRole());
		refundFormForm.setLoginUser(getLoginUser().getId());
		refundFormForm.setFormType(getFormType());
		
		Page<RefundForm> page = refundFormManager.list(refundFormForm);
		request.setAttribute("page", page);
		
		return "/refundForm/list";
	}
	
	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> deleteCP(Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		if(id != null && id > 0) {
			flag = (refundFormManager.delete(id) > 0);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		
		return result;
	}
	
	@RequestMapping("/export")
	public void export(HttpServletRequest request, HttpServletResponse response, RefundFormForm refundFormForm) {		
		// Auth
		refundFormForm.setFormRole(getUserFormRole());
		refundFormForm.setLoginUser(getLoginUser().getId());
		refundFormForm.setFormType(getFormType());
				
		String[] dataTitles = {"表單編號", "公司名稱", "申請人", "填表日期", "信件標題", "出款日", "退款原因", "遊戲", "遊戲帳號", "儲值時間", "訂單編號", "儲值金額", "金流通路", "會員姓名", "身份證字號", "聯絡電話", "聯絡地址", "退款方式", "其他說明 ", "通知金流商", "營運處理事項", "營運處理事項其他", "財務處理事項", "財務處理事項其他", "手續費發票號碼", "會計處理事項", "會計處理事項其他", "狀態" };
		refundFormForm.setExport(true);
		Page<RefundForm> page = refundFormManager.list(refundFormForm);
		List<Object[]> list = new ArrayList<Object[]>();

		if(page.getResult() != null && !page.getResult().isEmpty()) {
			for(RefundForm nl : page.getResult()) {
				Object[] o = new Object[dataTitles.length];
				o[0] = nl.getSerialNo();
				o[1] = nl.getCompanyName();
				o[2] = nl.getCreatorName();
				o[3] = DateUtil.getStringOfDate(nl.getRequestDate(), DateUtil.defaultDatePatternStr, null);
				o[4] = nl.getSubject();
				o[5] = DateUtil.getStringOfDate(nl.getRemitDate(), DateUtil.defaultDatePatternStr, null);
				o[6] = nl.getReason();
				o[7] = nl.getGameName();
				o[8] = nl.getGameAccount();
				o[9] = DateUtil.getStringOfDate(nl.getPayTime());
				o[10] = nl.getOrderId();
				o[11] = nl.getTwCoin();
				o[12] = nl.getPayType();
				o[13] = nl.getUserName();
				o[14] = nl.getUserId();
				o[15] = nl.getContactTel();
				o[16] = nl.getContactAddress();
				
				StringBuffer paymentWay = new StringBuffer(""); 
				if(nl.getPaymentWay().contains("1")){
					paymentWay.append("匯款帳號,");
				}
				if(nl.getPaymentWay().contains("2")){
					paymentWay.append("信用卡刷退,");
				}
				if(nl.getPaymentWay().contains("3")){
					paymentWay.append("收回折讓單,");
				}
				if(nl.getPaymentWay().contains("4")){
					paymentWay.append("填請款單,");
				}
				if(nl.getPaymentWay().contains("5")){
					paymentWay.append("收取手續費,");
				}
				if(nl.getPaymentWay().contains("9")){
					paymentWay.append("其他");
				}
				o[17] = paymentWay.toString();				
				o[18] = nl.getRemark();
				o[19] = StringUtil.getYesNoDesc(nl.getNotifyAgency());				
				
				StringBuffer opp = new StringBuffer("");
				if(nl.getOpsProcess().contains("1")){
					opp.append("客服資料確認,");
				}
				if(nl.getOpsProcess().contains("2")){
					opp.append("收回遊戲幣及道具,");
				}
				if(nl.getOpsProcess().contains("9")){
					opp.append("其他");
				}
				o[20] = opp.toString();
				o[21] = nl.getOpsProcessMemo();
				
				StringBuffer fp = new StringBuffer("");
				if(nl.getFinProcess().contains("1")){
					fp.append("客服資料確認,");
				}
				if(nl.getFinProcess().contains("2")){
					fp.append("後台退款操作,");
				}
				if(nl.getFinProcess().contains("3")){
					fp.append("發票作廢或開立折讓單,");
				}
				if(nl.getFinProcess().contains("4")){
					fp.append("開立手續費發票,");
				}
				if(nl.getFinProcess().contains("9")){
					fp.append("其他,");
				}
				if(nl.getFinProcess().contains("5")){
					fp.append("轉會計");
				}
				o[22] = fp.toString();
				o[23] = nl.getReceiptNo();
				o[24] = nl.getFinProcessMemo();
				
				StringBuffer ap = new StringBuffer("");
				if(nl.getAcctProcess().contains("1")){
					ap.append("退款或扣帳款,");
				}
				if(nl.getAcctProcess().contains("9")){
					ap.append("其他");
				}
				o[25] = ap.toString();
				o[26] = nl.getAcctProcessMemo();
				o[27] = StringUtil.getFlowStatusDesc(nl.getStatus());				
				
				list.add(o);
			}
		}
		
		try {
			ReportExcel.reportExcel(request, response, FUNCTION_NAME + DateUtil.getCurrentTimeStr("yyyyMMddHHmmss"), "", dataTitles, list);
		} 
		catch(Exception e) {
			log.error("err:" , e);
		}
	}
	
	/**
	 * 審放頁面
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/process")
	public String process(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		// Flow
		WorkFlowDetail workFlowDetail = flowManager.findFormFlowStep(workFlowDetailId);
		flowStep(request, workFlowDetail);
				
		// Form
		RefundForm form = refundFormManager.findOneByFlow(workFlowDetail.getWorkFlowId());		
		request.setAttribute("form", form);
		request.setAttribute("allGameData", gameDataManager.findAllGameData());
		request.setAttribute("allPayType", payTypeManager.findAllPayType());
		
		Map<String, Stamp> stamp = flowManager.findFormStamp(form.getWorkFlowId());
		request.setAttribute("stamp", stamp);
		
		return "/refundForm/flow";
	}
	
	/**
	 * 送審
	 * 
	 * @param request
	 * @param paymentForm
	 * @return
	 */
	@RequestMapping("/submit")
	public @ResponseBody Map<String, Object> doSubmit(HttpServletRequest request, RefundForm refundForm) {
		Map<String, Object> result = Maps.newHashMap();
		TEfunuser user = getLoginUser();
		boolean flag = false;	
		
		try {
			if(refundForm != null) {
				if(refundForm.getId() == null || refundForm.getId() <= 0) {
					refundForm = refundFormManager.add(refundForm, user, true, getFormType(), getFormType());				
					flag = (refundForm!=null);			
				} 
				else {				
					flag = (refundFormManager.update(refundForm, user, true, getFormType(), getFormType()) > 0);			
				}
				
				if(flag) {
					notifyManager.notify(refundForm.getWorkFlowId());
				}
			}
		}
		catch(Exception e) {
			log.error("err:", e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "送審成功！");			
		} 
		else {
			result.put("code", "0001");
			result.put("message", "送審失敗！");	
		}
		
		return result;
	}
	
	/**
	 * 審核
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/verify")
	public @ResponseBody Map<String, Object> doVerify(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId,
			@RequestParam(value="opsProcess", required=false, defaultValue="") String opsProcess,
			@RequestParam(value="opsProcessMemo", required=false, defaultValue="") String opsProcessMemo,
			@RequestParam(value="finProcess", required=false, defaultValue="") String finProcess,
			@RequestParam(value="finProcessMemo", required=false, defaultValue="") String finProcessMemo,
			@RequestParam(value="receiptNo", required=false, defaultValue="") String receiptNo,
			@RequestParam(value="acctProcess", required=false, defaultValue="") String acctProcess,
			@RequestParam(value="acctProcessMemo", required=false, defaultValue="") String acctProcessMemo) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			doProcess(workFlowDetailId, 
					opsProcess, opsProcessMemo, 
					finProcess, finProcessMemo, receiptNo, 
					acctProcess, acctProcessMemo);					
			flowManager.verifyFlow(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "審核成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "審核失敗！");
		}
		
		return result;
	}
	
	/**
	 * 放行
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/pass")
	public @ResponseBody Map<String, Object> doPass(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			doProcess(workFlowDetailId, 
					null, null, 
					null, null, null, 
					null, null);
			flowManager.passFlow(workFlowDetailId, user);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "放行成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "放行失敗！");
		}
		
		return result;
	}
	
	/**
	 * 退件
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @param memo
	 * @return
	 */
	@RequestMapping("/reject")
	public @ResponseBody Map<String, Object> doReject(HttpServletRequest request, 
			@RequestParam("id1") Long workFlowId,
			@RequestParam("id2") Long workFlowDetailId, 
			@RequestParam("memo") String memo) {
		TEfunuser user = getLoginUser();
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.rejectFlow(workFlowDetailId, user.getId(), memo);
			notifyManager.notify(workFlowId);
			
			flag = true;
		}
		catch(Exception e) {
			log.error("err:", e);
			releaseLock(workFlowDetailId);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "退件成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "退件失敗！");
		}
		
		return result;
	}
	
	/**
	 * 取消
	 * 
	 * @param request
	 * @param workFlowDetailId
	 * @return
	 */
	@RequestMapping("/abort")
	public @ResponseBody Map<String, Object> abort(HttpServletRequest request, 
			@RequestParam("id") Long workFlowDetailId) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try {
			flowManager.lockAbort(workFlowDetailId);
			flag = true;
		}
		catch(Exception e) {
			 log.error("err:", e);
		}
		
		if(flag) {
			result.put("code", "0000");
			result.put("message", "取消成功！");
		} 
		else {
			result.put("code", "0001");
			result.put("message", "取消失敗！");
		}
		
		return result;
	}

	/**
	 * 退款流程
	 * 
	 * @param workFlowDetailId
	 * @param opsProcess
	 * @param opsProcessMemo
	 * @param finProcess
	 * @param finProcessMemo
	 * @param receiptNo
	 * @param acctProcess
	 * @param acctProcessMemo
	 */
	private void doProcess(long workFlowDetailId,
			String opsProcess, String opsProcessMemo,
			String finProcess, String finProcessMemo, String receiptNo,
			String acctProcess, String acctProcessMemo) {
		WorkFlowDetail workFlowDetail = flowManager.findFormFlowStep(workFlowDetailId);
		RefundForm refundForm = refundFormManager.findOneByFlow(workFlowDetail.getWorkFlowId());
		
		if(StringUtils.isNotEmpty(opsProcess)) {
			refundForm.setOpsProcess(opsProcess);
			refundForm.setOpsProcessMemo(opsProcessMemo);
		}
		
		if(StringUtils.isNotEmpty(finProcess)) {
			refundForm.setFinProcess(finProcess);
			refundForm.setFinProcessMemo(finProcessMemo);
			refundForm.setReceiptNo(receiptNo);
		}
		
		if(StringUtils.isNotEmpty(acctProcess)) {
			refundForm.setAcctProcess(acctProcess);
			refundForm.setAcctProcessMemo(acctProcessMemo);
		}
		
		refundFormManager.update(refundForm);
	}
	
	/**
	 * PDF
	 * 
	 * @param request
	 * @param response
	 * @param formId
	 */
	@RequestMapping("/pdf")
	public void pdf(HttpServletRequest request, HttpServletResponse response, Long formId) {
		RefundForm form = refundFormManager.findOneById(formId);
		PdfUtil pdf = new PdfUtil();
		String content = notifyManager.getFormContent(getFormType(), formId);
		pdf.setHtmlContent(pdf.getBaseUrl(request), FUNCTION_NAME, content);
		
		//log.debug(content);
		String fileName = FUNCTION_NAME + form.getSerialNo();
		String subject = String.format("%s %s %s", 
				form.getCreatorName(), DateUtil.format(form.getRequestDate(), DateUtil.chineseDatePatternStr), FUNCTION_NAME);		
		
		try {
			pdf.export(response, fileName, subject);
		} 
		catch(Exception e) {
			log.error("err:" , e);
		}	
	}
}