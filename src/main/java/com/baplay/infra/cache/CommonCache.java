package com.baplay.infra.cache;

import java.util.Date;

import com.danga.MemCached.MemCachedClient;

public class CommonCache<T> implements Cache<T> {

	  private static MemCachedClient memCachedClient = null;
	  private String base = null;
	  
	  CommonCache(Class<T> t, MemCachedClient client)
	  {
	    memCachedClient = client;
	    this.base = (t.getSimpleName() + "-");
	  }
	  
	  public T get(String key)
	  {
	    return (T) memCachedClient.get(this.base + key);
	  }
	  
	  public boolean set(String key, T value)
	  {
	    return memCachedClient.set(this.base + key, value);
	  }
	  
	  public boolean update(String key, T value)
	  {
	    return memCachedClient.replace(this.base + key, value);
	  }
	  
	  public boolean delete(String key)
	  {
	    return memCachedClient.delete(this.base + key);
	  }
	  
	  public boolean add(String key, T value)
	  {
	    return memCachedClient.add(this.base + key, value);
	  }
	  
	  public boolean add(String key, T value, Date expiry)
	  {
	    return memCachedClient.add(this.base + key, value, expiry);
	  }
	  
	  public boolean set(String key, T value, Date expiry)
	  {
	    return memCachedClient.set(this.base + key, value, expiry);
	  }
	  
	  public boolean update(String key, T value, Date expiry)
	  {
	    return memCachedClient.replace(this.base + key, value, expiry);
	  }
	  
	  public boolean deleteAll(){
		  return memCachedClient.flushAll();
	  }

}
