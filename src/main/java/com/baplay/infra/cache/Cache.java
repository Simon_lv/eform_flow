package com.baplay.infra.cache;

import java.util.Date;

public interface Cache<T> {

	public abstract T get(String paramString);

	public abstract boolean add(String paramString, T paramT);

	public abstract boolean add(String paramString, T paramT, Date paramDate);

	public abstract boolean set(String paramString, T paramT);

	public abstract boolean set(String paramString, T paramT, Date paramDate);

	public abstract boolean update(String paramString, T paramT);

	public abstract boolean update(String paramString, T paramT, Date paramDate);

	public abstract boolean delete(String paramString);

	public abstract boolean deleteAll();
}
