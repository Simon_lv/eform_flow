package com.baplay.infra.utils;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Joiner;
import com.google.common.base.MoreObjects;

public class SpringMVCPrintRequestUtil implements HandlerInterceptor {

	private static final Logger logger = LoggerFactory
			.getLogger(SpringMVCPrintRequestUtil.class);
	private static final int MAX_OUTPUT_LENGTH=500;
	@SuppressWarnings("unchecked")
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		String uri = request.getRequestURI();
	/*	if (uri != null && uri.contains(".")) {
			return true;
		}*/
		logger.info("*****************************************************************");
		logger.info("Input URL：" + uri);
		Enumeration<String> paramNames = request.getParameterNames();
		String[] emptyValues = new String[0];
		while (paramNames.hasMoreElements()) {// 遍历Enumeration
			String name = (String) paramNames.nextElement();// 取出下一个元素
			String[] value = MoreObjects.firstNonNull(
					request.getParameterValues(name), emptyValues);// 获取元素的值
			logger.info("{} = {}", name, Joiner.on(",").join(value));

		}
		logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		if(modelAndView!=null) {
			String viewName = modelAndView.getViewName();
			logger.info("viewName = {}", viewName+".jsp");
			Map<String, Object> resultMap = modelAndView.getModel();
			Iterator it = resultMap.entrySet().iterator();
			logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//			logger.info("output result：");
//			while (it.hasNext()) {
//				Map.Entry entry = (Map.Entry) it.next();
//				String key = (String)entry.getKey();
//				Object value = entry.getValue();
//				outputItem(key,value,"output");
//			}
		}
		
	}
	private static void outputItem(String name,Object value,String source)
	{
		if(name.length()<=9)
		{
			name = name+"		=	";
		}else
		{
			name = name+"	=	";
	
		}
		String val = "";
		if(value!=null)
		{
			val = value.toString();
		}
		int length = val.length();
		if(length>MAX_OUTPUT_LENGTH)
		{
			val = val.substring(0,MAX_OUTPUT_LENGTH)+"......";
		}
		logger.info(name+val);
	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		logger.info("*****************************************************************");
	}

}
