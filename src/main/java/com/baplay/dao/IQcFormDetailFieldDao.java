package com.baplay.dao;

import java.util.HashMap;
import java.util.List;

import com.baplay.entity.QcFormDetailField;

public interface IQcFormDetailFieldDao {

	public int add(HashMap<String, Object> map);	
	public QcFormDetailField add(QcFormDetailField qcFormDetailField);
	public int update(QcFormDetailField qcFormDetailField);
	public int updateGroup(QcFormDetailField qcFormDetailField);
	public List<QcFormDetailField> findAll(Long id, Long formId);
	public List<QcFormDetailField> findAll(Long id);
	public List<QcFormDetailField> findAllGroupLabel(Long id);
	public int findGroupCount(Long id);
	public List<Integer> findAllFieldCount(Long id);
	public int deleteGroup(Long id);
	public int delete(Long id);
}
