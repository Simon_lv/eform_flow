package com.baplay.dao;

import org.springside.modules.orm.Page;

import com.baplay.entity.BusinessCardForm;
import com.baplay.form.BusinessCardFormForm;

public interface IBusinessCardFormDao {
	public BusinessCardForm add(BusinessCardForm businessCardForm);
	public int update(BusinessCardForm businessCardForm);
	public int updateTotal(Long id, Long editId, String formType);
	public BusinessCardForm findOneById(Long id);
	public Page<BusinessCardForm> list(BusinessCardFormForm businessCardFormForm);
	public int delete(Long id);
	public BusinessCardForm findOneByFlow(Long workFlowId);
}
