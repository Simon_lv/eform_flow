package com.baplay.dao;

import java.util.List;

import org.springside.modules.orm.Page;

import com.baplay.entity.Receipt;
import com.baplay.form.ReceiptForm;

public interface IReceiptDao {
	public Receipt add(Receipt receipt);
	public int update(Receipt receipt);
	public Receipt findOneById(Long id);
	public Page<Receipt> list(ReceiptForm receiptForm);
	public int delete(Long id);
	public int deleteAll(Long formId, String formType);
	public List<Receipt> findAllByForm(Long id, String formType, Integer costType);
}
