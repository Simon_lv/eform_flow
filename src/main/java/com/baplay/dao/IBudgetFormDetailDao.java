package com.baplay.dao;

import java.util.List;

import com.baplay.entity.BudgetFormDetail;

public interface IBudgetFormDetailDao {
	public BudgetFormDetail add(BudgetFormDetail budgetFormDetail);
	public int deleteAll(Long budgetFormId);
	public int delete(Long id);
	public List<BudgetFormDetail> findFormDetail(Long budgetFormId, int additional);
}
