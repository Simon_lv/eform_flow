package com.baplay.dao;

import java.util.List;

import com.baplay.entity.Attachment;

public interface IAttachmentDao {

	public Attachment add(Attachment attachment);
	public int deleteAll(Long formid, String formType);
	public int delete(Long id);
	public List<Attachment> findFormAttachment(Long formid, String formType);
}