package com.baplay.dao;

import java.util.List;

import com.baplay.entity.FormType;

public interface IFormTypeDao {

	public FormType findOneByCode(String code, String groupCode, Integer... flowSchemaId);
	
	public FormType add(FormType formType);
	
	public int update(String groupCode, String name, String oldCode, String newCode);
	
	public List<FormType> listAll();
}
