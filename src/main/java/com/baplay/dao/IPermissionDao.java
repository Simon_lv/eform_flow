package com.baplay.dao;

import java.util.List;

import com.baplay.entity.Function;

public interface IPermissionDao {
	/**
	 * 根据角色id查询
	 */
	public List<Function> getFunctions(List<Long> roleIds, int all);

	
	/**
	 * 修改权限状态
	 * 
	 * @param funId
	 * @param status
	 */
	public void updateFunctionStatus(Long funId, Integer status);

	public boolean checkLoginAccountExist(String loginAccount);

}
