package com.baplay.dao;

import java.util.List;

import com.baplay.entity.QcFormTitleField;

public interface IQcFormTitleFieldDao {

	public List<QcFormTitleField> findAll(Long id, Long... otherId);
	public QcFormTitleField add(QcFormTitleField qcFormTitleField);
	public int update(QcFormTitleField qcFormTitleField);
	public int delete(Long id);
}
