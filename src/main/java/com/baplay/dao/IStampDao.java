package com.baplay.dao;

import java.util.List;

import com.baplay.dto.Stamp;

public interface IStampDao {

	/**
	 * 表單蓋章主管
	 * 
	 * @param workFlowId
	 * @return
	 */
	public List<Stamp> findSupervisor(long workFlowId);
	
	/**
	 * 表單承辦人
	 * 
	 * @param workFlowId
	 * @return
	 */
	public Stamp findCreator(long workFlowId);
	
	/**
	 * 退件註銷已簽章紀錄
	 * 
	 * @param work_flow_id
	 * @return
	 */
	public int cancel(long workFlowId);
}