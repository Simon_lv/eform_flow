package com.baplay.dao;

import org.springside.modules.orm.Page;

import com.baplay.entity.ContractReviewForm;
import com.baplay.form.ContractReviewFormForm;

public interface IContractReviewFormDao {
	public ContractReviewForm add(ContractReviewForm contractReviewForm);
	public int update(ContractReviewForm contractReviewForm);
	public ContractReviewForm findOneById(Long id);
	public Page<ContractReviewForm> list(ContractReviewFormForm contractReviewFormForm);
	public int delete(Long id);
	public ContractReviewForm findOneByFlow(Long workFlowId);
}
