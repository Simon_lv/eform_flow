package com.baplay.dao;

import java.util.List;

import com.baplay.entity.FlowRouter;

public interface IFlowRouterDao {

	public List<FlowRouter> findOneById(String id);
}