package com.baplay.dao;

import java.util.List;

import com.baplay.entity.TEfunuser;
import com.baplay.form.UserForm;

public interface IUserDao {

	/**
	 * 添加用户
	 * 
	 * @param form
	 * @return
	 */
	public TEfunuser addUser(UserForm form);

	/**
	 * 为用户添加角色
	 * 
	 * @param userId
	 * @param roleId
	 */
	public void addUserRole(Long userId, List<Long> roleId);
	
	/**
	 * 为用户添加表單角色
	 * 
	 * @param userId
	 * @param formRoleId
	 */
	public void addUserFormRole(Long userId, List<Long> formRoleId, int formRoleDeptLimit);

	/**
	 * 更新用户
	 * 
	 * @param form
	 */
	public void updateUser(UserForm form);

	/**
	 * 更新密码
	 * 
	 * @param userId
	 * @param password
	 */
	public void updatePwd(Long userId, String password);

	/**
	 * 更新状态
	 * 
	 * @param userId
	 * @param status
	 */
	public void updateUserStatus(Long userId, Integer status);

	/**
	 * 按用户名查询
	 * 
	 * @param username
	 * @return
	 */
	public TEfunuser findByUserName(String userName);

	public List<TEfunuser> getAllUser(String userName);

	public UserForm findById(Long id);
	
	public Long findByEmployeeId(Long employeeId);
	
	/**
	 * 更新員工ID
	 * 
	 * @param employeeId
	 * @param userId
	 * @return
	 */
	public int updateEmployeeId(long employeeId, long userId);
}