package com.baplay.dao.mapper;

public class DepartmentTop {

	private long id;
	private String name;
	private String deptNo;
	private long topId;
	private String topName;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	
	public String getDeptNo() {
		return deptNo;
	}
	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}
	public long getTopId() {
		return topId;
	}
	public void setTopId(long topId) {
		this.topId = topId;
	}
	public String getTopName() {
		return topName;
	}
	public void setTopName(String topName) {
		this.topName = topName;
	}	
}