package com.baplay.dao;

import java.util.Date;

import org.springside.modules.orm.Page;

import com.baplay.entity.BusinessTripExpenseForm;
import com.baplay.form.BusinessTripExpenseFormForm;

public interface IBusinessTripExpenseFormDao {
	public BusinessTripExpenseForm add(BusinessTripExpenseForm businessTripExpenseForm);
	public int update(BusinessTripExpenseForm businessTripExpenseForm);
	public int updateTotal(Long id, Long editId, int costType, String formType);
	public BusinessTripExpenseForm findOneById(Long id);
	public Page<BusinessTripExpenseForm> list(BusinessTripExpenseFormForm businessTripExpenseFormForm);
	public int delete(Long id);
	public BusinessTripExpenseForm findOneByFlow(Long workFlowId);
	public int remark(Long workFlowId, String remark);
	public int confirmRemitDate(Long id);
	public int updateRemitDate(Long id, Date remitDate);
}