package com.baplay.dao;

import com.baplay.entity.WorkFlow;

public interface IWorkFlowDao {

	/**
	 * 草稿存檔
	 * 
	 * @param workFlow
	 * @return
	 */
	public WorkFlow save(WorkFlow workFlow);
	
	/**
	 * 更新表單ID
	 * 
	 * @param id
	 * @param formId
	 * @return
	 */
	public int update(Long id, Long formId);
	
	/**
	 * 草稿刪除
	 * 
	 * @param id
	 * @return
	 */
	public int delete(long id);
	
	/**
	 * ID搜尋
	 * 
	 * @param id
	 * @return
	 */
	public WorkFlow findOneById(Long id);
	
	/**
	 * 表單編號搜尋
	 * 
	 * @param serialNo
	 * @return
	 */
	public WorkFlow findOneByNo(String serialNo);		
	
	/**
	 * 用戶鎖定
	 * 
	 * @param id
	 * @param userId
	 * @return
	 */
	public int process(long id, long userId);
	
	/**
	 * 流程下一步驟
	 * 
	 * @param id
	 * @return
	 */
	public int next(long id);
	
	/**
	 * 用戶取消鎖定
	 * 
	 * @param id
	 * @return
	 */
	public int abort(long id);
	
	/**
	 * 流程放行結束
	 * 
	 * @param id
	 * @param userId
	 * @return
	 */
	public int close(long id, long userId);
	
	/**
	 * 草稿送審
	 * 
	 * @param id
	 * @param flowSchemaId
	 * @param serialNo
	 * @return
	 */
	public int submitDraft(long id, long flowSchemaId, String serialNo);
	
	/**
	 * 退件
	 * 
	 * @param id
	 * @return
	 */
	public int reject(long id);
	
	/**
	 * 註銷
	 * 
	 * @param id
	 * @return
	 */
	public int terminate(long id);
	
	/**
	 * 更新(退件=>草稿)
	 * 
	 * @param workFlow
	 * @return
	 */
	public int update(WorkFlow workFlow);
}