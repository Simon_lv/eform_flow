package com.baplay.dao;

import org.springside.modules.orm.Page;

import com.baplay.entity.IssueForm;
import com.baplay.form.IssueFormForm;

public interface IIssueFormDao {
	public IssueForm add(IssueForm issueForm);
	public int update(IssueForm issueForm);
	public IssueForm findOneById(Long id);
	public Page<IssueForm> list(IssueFormForm issueFormForm);
	public int delete(Long id);	
	public IssueForm findOneByFlow(Long workFlowId);
//	public int memo(Long id, Long uid, String memo);
}