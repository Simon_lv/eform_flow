package com.baplay.dao;

import java.util.List;

import com.baplay.dao.mapper.FormUser;

public interface IFormUserDao {

	public List<FormUser> findIssueRepairUser(Integer... roleId);
}