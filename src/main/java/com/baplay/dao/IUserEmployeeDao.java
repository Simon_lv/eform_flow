package com.baplay.dao;

import java.util.List;

import com.baplay.entity.UserEmployee;

public interface IUserEmployeeDao {

	/**
	 * 新增
	 * 
	 * @param userId
	 * @param employeeId
	 * @return
	 */
	public UserEmployee add(long userId, long employeeId);
	
	/**
	 * UID刪除
	 * 
	 * @param userId
	 * @return
	 */
	public int deleteByUID(long userId);
	
	/**
	 * UID查詢
	 * 
	 * @param userId
	 * @return
	 */
	public List<Long> findByUID(long userId);
	
	/**
	 * 全部資料
	 * 
	 * @return
	 */
	public List<UserEmployee> findAll();
}