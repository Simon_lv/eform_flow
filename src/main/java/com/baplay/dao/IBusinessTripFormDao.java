package com.baplay.dao;

import org.springside.modules.orm.Page;

import com.baplay.entity.BusinessTripForm;
import com.baplay.form.BusinessTripFormForm;

public interface IBusinessTripFormDao {
	public BusinessTripForm add(BusinessTripForm businessTripForm);
	public int update(BusinessTripForm businessTripForm);
	public BusinessTripForm findOneById(Long id);
	public Page<BusinessTripForm> list(BusinessTripFormForm businessTripFormForm);
	public int delete(Long id);	
	public BusinessTripForm findOneByFlow(Long workFlowId);	
}
