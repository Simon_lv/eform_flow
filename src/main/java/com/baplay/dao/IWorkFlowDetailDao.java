package com.baplay.dao;

import com.baplay.entity.WorkFlowDetail;

public interface IWorkFlowDetailDao {

	/**
	 * 草稿存檔
	 * 
	 * @param workFlowDetail
	 * @return
	 */
	public WorkFlowDetail save(WorkFlowDetail workFlowDetail);
	
	/**
	 * 草稿刪除
	 * 
	 * @param id
	 * @return
	 */
	public int delete(long workFlowId);
	
	/**
	 * ID搜尋
	 * 
	 * @param id
	 * @return
	 */
	public WorkFlowDetail findOneById(long id);
	
	/**
	 * 流程當前步驟
	 * 
	 * @param workFlowId
	 * @return
	 */
	public WorkFlowDetail findLastOneById(long workFlowId);
	
	/**
	 * 用戶鎖定
	 * 
	 * @param id
	 * @param userId
	 * @return
	 */
	public int process(long id, long userId);		
	
	/**
	 * 用戶取消鎖定
	 * 
	 * @param id
	 * @return
	 */
	public int abort(long id);
	
	/**
	 * 結束步驟
	 * 
	 * @param id
	 * @return
	 */
	public int close(long id);
	
	/**
	 * 退件
	 * 
	 * @param id
	 * @param userId
	 * @param memo
	 * @return
	 */
	public int reject(long id, long userId, String memo);
	
	/**
	 * 註銷
	 * 
	 * @param id
	 * @param userId
	 * @param memo
	 * @return
	 */
	public int terminate(long id, long userId, String memo);
	public WorkFlowDetail terminate(WorkFlowDetail workFlowDetail);
		
	/**
	 * 狀態是否被變更
	 * 
	 * @param id
	 * @return
	 */
	public boolean illegal(long id);
}