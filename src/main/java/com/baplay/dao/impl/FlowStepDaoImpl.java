package com.baplay.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.baplay.dao.IFlowStepDao;
import com.baplay.entity.FlowStep;

@Repository
public class FlowStepDaoImpl extends BaseDao<FlowStep> implements IFlowStepDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}	
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFlowStepDao#findFlowAllStep(long, int, java.lang.Integer[])
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<FlowStep> findFlowAllStep(long flowSchemaId, int currentOrder, Integer... stepType) {
		String whereStepType = "";
		
		if(stepType.length > 0) {
			whereStepType = String.format(" AND step_type in (%s)", StringUtils.join(stepType, ","));
		}
		
		String whereStepOrder = "";
		
		if(currentOrder > 0) {
			whereStepOrder = " AND step_order > " + currentOrder;
		}
		
		String sql = "SELECT * FROM `flow_step` WHERE `flow_schema_id`=?" + whereStepType + whereStepOrder + " ORDER BY step_order";
		return super.queryForList(dsBmsQry, sql, new Object[]{flowSchemaId}, FlowStep.class);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFlowStepDao#findOneById(java.lang.Long)
	 */
	@Override
	public FlowStep findOneById(Long id) {
		String sql = "SELECT fs.*, (SELECT `name` FROM form_role WHERE id=fs.role_id) role_name FROM `flow_step` fs WHERE id=?";
		Object[] args = new Object[]{id};
		
		FlowStep entity = (FlowStep) super.queryForObject(dsBmsQry, sql, args, FlowStep.class);
		return entity;
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFlowStepDao#findFlowStepBySchema(long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<FlowStep> findFlowStepBySchema(long flowSchemaId) {
		StringBuilder sql = new StringBuilder(); 
		sql.append("SELECT fs.*, ");
		sql.append("IF(fs.role_id=-1, '不限', (select `name` FROM form_role WHERE id=fs.role_id)) role,");
		sql.append("IFNULL((SELECT GROUP_CONCAT(`name`) FROM form_role WHERE FIND_IN_SET(id, fs.countersign)), '免') countersign_role,");
		sql.append("IFNULL((SELECT GROUP_CONCAT(description) FROM flow_router WHERE next_step=fs.id), '無') router ");
		sql.append("FROM flow_step fs "); 
		sql.append("WHERE flow_schema_id=? "); 
		sql.append("ORDER BY step_order");
		
		return super.queryForList(dsBmsQry, sql.toString(), new Object[]{flowSchemaId}, FlowStep.class);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFlowStepDao#updateRole(java.lang.Long, java.lang.Long)
	 */
	@Override
	public int updateRole(Long flowStepId, Long formRoleId) {
		String sql = "UPDATE `flow_step` SET `role_id`=? WHERE `id`=?";
		return updateForObject(dsBmsUpd, sql, new Object[]{formRoleId, flowStepId});
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFlowStepDao#updateCountersign(java.lang.Long, java.lang.String)
	 */
	@Override
	public int updateCountersign(Long flowStepId, String countersign) {
		if(StringUtils.isEmpty(countersign)) {
			String sql = "UPDATE `flow_step` SET `countersign`=null WHERE `id`=?";
			return updateForObject(dsBmsUpd, sql, new Object[]{flowStepId});
		}
		else {
			String sql = "UPDATE `flow_step` SET `countersign`=? WHERE `id`=?";
			return updateForObject(dsBmsUpd, sql, new Object[]{countersign, flowStepId});
		}		
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFlowStepDao#findNextOneById(java.lang.Long)
	 */
	@Override
	public FlowStep findNextOneById(Long id) {
		String sql = "select s.* from flow_step s " +  
					 "join (select * from flow_step where id=?) x on x.flow_schema_id=s.flow_schema_id " + 
					 "where s.step_order > x.step_order " + 
					 "order by s.step_order limit 1";
		Object[] args = new Object[]{id};
		
		FlowStep entity = (FlowStep) super.queryForObject(dsBmsQry, sql, args, FlowStep.class);
		return entity;
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFlowStepDao#findFlowStep(long, int)
	 */
	@Override
	public FlowStep findFlowStep(long flowSchemaId, int order) {
		String sql = "SELECT * FROM `flow_step` WHERE `flow_schema_id`=? AND step_order=?";
		FlowStep entity = (FlowStep) super.queryForObject(dsBmsQry, sql, new Object[]{flowSchemaId, order}, FlowStep.class);
		
		return entity;
	}
}