package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import com.baplay.dao.IFlowSchemaDao;
import com.baplay.entity.FlowSchema;
import com.baplay.form.FlowMgrForm;

@Repository
public class FlowSchemaDaoImpl extends BaseDao<FlowSchema> implements IFlowSchemaDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFlowSchemaDao#findOneById(java.lang.Long)
	 */
	@Override
	public FlowSchema findOneById(Long id) {
		String sql = "SELECT * FROM `flow_schema` WHERE id=?";
		Object[] args = new Object[]{id};
		
		FlowSchema entity = (FlowSchema) queryForObject(dsBmsQry, sql, args, FlowSchema.class);
		return entity;
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFlowSchemaDao#findOneById(java.lang.Long, java.lang.String)
	 */
	@Override
	public FlowSchema findOneById(Long companyId, String allId) {
		String sql = "SELECT * FROM `flow_schema` WHERE company_id=? and id in (" + allId + ")";
		Object[] args = new Object[]{companyId};
		
		FlowSchema entity = (FlowSchema) queryForObject(dsBmsQry, sql, args, FlowSchema.class);
		return entity;
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFlowSchemaDao#findAll()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<FlowSchema> findAll(Long... companyId) {
		String sql = "SELECT * FROM `flow_schema`";
		
		if(companyId.length > 0) {
			sql += String.format(" where company_id IN (%s)", StringUtils.join(companyId, ","));
		}
		
		return queryForList(dsBmsQry, sql, new Object[]{}, FlowSchema.class);	
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFlowSchemaDao#list(com.baplay.form.FlowMgrForm)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Page<FlowSchema> list(FlowMgrForm form) {
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		
		if(form.getFlowId() != null){
			sb.append(" and a.id=? ");
			param.add(form.getFlowId());
		}
		
		if(form.getCompanyId() != null){
			sb.append(" and a.company_id=? ");
			param.add(form.getCompanyId());
		}
		
		StringBuilder sql = new StringBuilder("SELECT a.*, u.`login_account` creator_name, c.`name` company_name FROM flow_schema a ");
		sql.append("LEFT JOIN t_erp_user u ON u.id = a.creator ");
		sql.append("LEFT JOIN company c ON c.id = a.company_id ");
		sql.append("WHERE 1=1").append(sb.toString());
		
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM flow_schema as a where 1=1").append(sb.toString());		
		
		if(!form.isExport()) {
			sql.append(" LIMIT ").append((form.getPageNo()-1)*form.getPageSize()).append(",").append(form.getPageSize());
		}
		
		Page<FlowSchema> page = new Page<FlowSchema>(form.getPageSize());
		page = (Page<FlowSchema>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(),
				new BeanPropertyRowMapper<FlowSchema>(FlowSchema.class));
		page.setPageNo(form.getPageNo());
		return page;
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFlowSchemaDao#findOneByName(java.lang.String)
	 */
	@Override
	public FlowSchema findOneByName(String name) {
		String sql = "SELECT * FROM `flow_schema` WHERE `flow_name`=?";
		Object[] args = new Object[]{name};
		
		FlowSchema entity = (FlowSchema) queryForObject(dsBmsQry, sql, args, FlowSchema.class);
		return entity;
	}
}