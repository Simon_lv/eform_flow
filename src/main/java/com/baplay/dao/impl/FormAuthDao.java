package com.baplay.dao.impl;

import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import com.baplay.dao.IFormAuthDao;

@Repository
public class FormAuthDao extends BaseDao implements IFormAuthDao {
	
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}

	/* (non-Javadoc)
	 * @see com.baplay.dao.IFormAuthDao#formAuth(java.util.Set)
	 */
	@Override
	public boolean formAuth(String formType, Set<String> userFormRole) {
		String role = StringUtils.join(userFormRole, "','");
		String sql = "SELECT COUNT(*) FROM form_auth WHERE form_type=? and form_role IN (SELECT id FROM form_role WHERE `name` IN('" + role + "'))";
		return super.queryForInt(dsBmsQry, sql, new Object[]{formType}) > 1;
	}
}