package com.baplay.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import com.baplay.dao.IFlowRouterDao;
import com.baplay.entity.FlowRouter;

@Repository
public class FlowRouterDaoImpl extends BaseDao<FlowRouter> implements IFlowRouterDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFlowRouterDao#findOneById(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<FlowRouter> findOneById(String id) {
		String sql = "SELECT * FROM `flow_router` WHERE id in (" + id + ")";
		
		return super.queryForList(dsBmsQry, sql, new Object[]{}, FlowRouter.class);		
	}	
}