package com.baplay.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import com.baplay.dao.IContractReviewMemoDao;
import com.baplay.entity.ContractReviewMemo;

@Repository
public class ContractReviewMemoDaoImpl extends BaseDao<ContractReviewMemo> implements IContractReviewMemoDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public ContractReviewMemo add(ContractReviewMemo contractReviewMemo) {
		String sql = "INSERT INTO `contract_review_memo` (`contract_review_form_id`, `attachment_id`, `memo`, `creator`, `create_time`) VALUES (?, ?, ?, ?, NOW())";
		return (ContractReviewMemo) super.addForObject(dsBmsUpd, sql, contractReviewMemo, 
				new Object[]{ 
						contractReviewMemo.getContractReviewFormId(), contractReviewMemo.getAttachmentId(),
						contractReviewMemo.getMemo(), contractReviewMemo.getCreator() });
	}

	@Override
	public int deleteAll(Long contractReviewFormId) {
		String sql = "DELETE FROM `contract_review_memo` WHERE (`contract_review_form_id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{contractReviewFormId});
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ContractReviewMemo> findContractReviewMemo(Long contractReviewFormId) {
		String sql = "SELECT crm.*, am.url, am.description, am.create_time as attachmentCreateTime FROM contract_review_memo crm join attachment am ON crm.attachment_id = am.id WHERE crm.contract_review_form_id=?";
		return this.queryForList(dsBmsQry, sql, new Object[]{contractReviewFormId}, ContractReviewMemo.class);
	}

}
