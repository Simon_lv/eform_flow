package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import com.baplay.dao.IReceiptDao;
import com.baplay.entity.Receipt;
import com.baplay.form.ReceiptForm;

@Repository
public class ReceiptDaoImpl extends BaseDao<Receipt> implements IReceiptDao {
	
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public Receipt add(Receipt receipt) {		
		String sql = "INSERT INTO `receipt` (`form_id`, `form_type`, `cost_date`, `cost_item`, `description`, `amount`, `place`, `number`, `currency`, `rate`, `cost_type`) VALUES (?, ?, ?, ?, ?, ?, ?, ? ,? , ?, ?)";
		return (Receipt) super.addForObject(dsBmsUpd, sql, receipt, 
				new Object[]{ receipt.getFormId(), receipt.getFormType(), receipt.getCostDate(), receipt.getCostItem(),
						receipt.getDescription(), receipt.getAmount(), receipt.getPlace(),
						receipt.getNumber(), receipt.getCurrency(), receipt.getRate(),
						receipt.getCostType() });
	}

	@Override
	public int update(Receipt receipt) {
		StringBuffer sql = new StringBuffer("UPDATE `receipt` SET `cost_date`=?, `description`=?, `amount`=?");
		ArrayList<Object> param = new ArrayList<Object>();
		param.add(receipt.getCostDate());		
		param.add(receipt.getDescription());
		param.add(receipt.getAmount());
		
		if(receipt.getCostItem() != null){
			sql.append(", cost_item=?");
			param.add(receipt.getCostItem());			
		}
		if(receipt.getPlace() != null){
			sql.append(", place=?");
			param.add(receipt.getPlace());
		}
		if(receipt.getNumber() != null){
			sql.append(", number=?");
			param.add(receipt.getNumber());
		}
		if(receipt.getCurrency() != null){
			sql.append(", currency=?");
			param.add(receipt.getCurrency());
		}
		if(receipt.getRate() > 0){
			sql.append(", rate=?");
			param.add(receipt.getRate());
		}
		if(receipt.getCostType() != null){
			sql.append(", cost_type=?");
			param.add(receipt.getCostType());
		}
		
		sql.append(" WHERE (`id`=?)");
		param.add(receipt.getId());
		
		return super.updateForObject(dsBmsUpd, sql.toString(), param.toArray());
	}

	@Override
	public Receipt findOneById(Long id) {
		String sql = "SELECT * FROM receipt WHERE id=?";
		Object[] args = new Object[]{id};
		return (Receipt) super.queryForObject(dsBmsQry, sql, args, Receipt.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Page<Receipt> list(ReceiptForm receiptForm) {
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
				
		if(StringUtils.isNotBlank(receiptForm.getRequestDateStart())){		
			sb.append(" AND STR_TO_DATE(b.request_date, '%Y-%m-%d') >= ? ");
			param.add(receiptForm.getRequestDateStart());
		}
		if(StringUtils.isNotBlank(receiptForm.getRequestDateEnd())){
			sb.append(" AND STR_TO_DATE(b.request_date, '%Y-%m-%d') <= ? ");
			param.add(receiptForm.getRequestDateEnd());
		}
		if(receiptForm.getStatus() != null){
			sb.append(" AND b.status = ? ");
			param.add(receiptForm.getStatus());
		}
		
		StringBuilder sql = new StringBuilder("SELECT b.* FROM (SELECT a.*, (SELECT status FROM work_flow WHERE id = a.work_flow_id) AS status FROM receipt a ) as b WHERE 1=1").append(sb.toString());
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM (SELECT a.*, (SELECT status FROM work_flow WHERE id = a.work_flow_id) AS status FROM receipt a ) as b WHERE 1=1").append(sb.toString());
				
		if(!receiptForm.isExport()) {
			sql.append(" LIMIT ").append((receiptForm.getPageNo()-1)*receiptForm.getPageSize()).append(",").append(receiptForm.getPageSize());
		}
		
		Page<Receipt> page = new Page<Receipt>(receiptForm.getPageSize());

		page = (Page<Receipt>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(), 
				new BeanPropertyRowMapper<Receipt>(Receipt.class));
		page.setPageNo(receiptForm.getPageNo());
		return page;
	}

	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM `receipt` WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}
	
	@Override
	public int deleteAll(Long formId, String formType) {
		String sql = "DELETE FROM `receipt` WHERE (`form_id`=? and form_type=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{formId, formType});
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Receipt> findAllByForm(Long id, String formType, Integer costType) {
		StringBuffer sql = new StringBuffer("SELECT * FROM receipt WHERE form_id=? and form_type=?");
		Object ob[] = null;		
		
		if(costType != 0){
			sql.append(" and cost_type=?");
			ob = new Object[]{id,formType,costType};
		}else{
			ob = new Object[]{id,formType};
		}
		
		return this.queryForList(dsBmsQry, sql.toString(), ob, Receipt.class);
	}

	
	
}