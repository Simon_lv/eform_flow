package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import com.baplay.dao.IEmployeeDao;
import com.baplay.entity.Employee;
import com.baplay.form.EmployeeForm;

@Repository
public class EmployeeDaoImpl extends BaseDao<Employee> implements IEmployeeDao {
	
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public Employee add(Employee employee) {
		String sql = "INSERT INTO `employee` (`employee_no`, `name`, `mobile`, `mobile2`, `phone`, `zip_code`, `address`, `phone_ext`, `email`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `creator`, `create_time`, `status`) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), 1)";
		return (Employee) super.addForObject(dsBmsUpd, sql, employee, 
				new Object[]{employee.getEmployeeNo(), employee.getName(), 
				employee.getMobile(), employee.getMobile2(), employee.getPhone(), employee.getZipCode(), employee.getAddress(), 
				employee.getPhoneExt(), employee.getEmail(), employee.getJobTitleId(), employee.getDepartmentId(), 
				employee.getCompanyId(), employee.getOnboardTime(), 
				employee.getQuitTime(), employee.getCreator()});
	}

	@Override
	public int update(Employee employee) {
		String sql = "UPDATE `employee` SET `employee_no`=?, `name`=?, `mobile`=?, `mobile2`=?, `phone`=?, `zip_code`=?, `address`=?, `phone_ext`=?, `email`=?, `job_title_id`=?, `department_id`=?, `onboard_time`=?, `quit_time`=?, `modifier`=?, `update_time`=NOW() WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{
				employee.getEmployeeNo(), employee.getName(), 
				employee.getMobile(), employee.getMobile2(), employee.getPhone(), employee.getZipCode(), employee.getAddress(), 
				employee.getPhoneExt(), employee.getEmail(), employee.getJobTitleId(), employee.getDepartmentId(), 
				employee.getOnboardTime(), employee.getQuitTime(), employee.getModifier(), employee.getId()});
	}

	@Override
	public Employee findOneById(Long id) {
		String sql = "SELECT e.*, (SELECT `name` FROM job_title where id=e.job_title_id) jobTitleName, (SELECT `name` FROM department where id=e.department_id) departmentName, (SELECT `name` FROM company where id=e.company_id) companyName, u.id user_id FROM `employee` e LEFT JOIN t_erp_user u ON u.employee_id=e.id WHERE e.id=?";
		Object[] args = new Object[]{id};
		
		Employee employee = (Employee)super.queryForObject(dsBmsQry, sql, args, Employee.class);
		return employee;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Page<Employee> list(EmployeeForm employeeForm) {
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		
		if(employeeForm.getId() != null) {
			sb.append(" AND id = ? ");
			param.add(employeeForm.getId());
		}
		
		if(StringUtils.isNotBlank(employeeForm.getEmployeeNo())) {
			sb.append(" AND employee_no = ? ");
			param.add(employeeForm.getEmployeeNo());
		}
		
		if(StringUtils.isNotBlank(employeeForm.getName())) {
			sb.append(" AND name LIKE ? ");
			param.add("%"+employeeForm.getName()+"%");
		}
		
		if(StringUtils.isNotBlank(employeeForm.getMobile())) {
			sb.append(" AND mobile = ? ");
			param.add(employeeForm.getMobile());
		}
		
		if(StringUtils.isNotBlank(employeeForm.getPhone())) {
			sb.append(" AND phone = ? ");
			param.add(employeeForm.getPhone());
		}
		
		if(employeeForm.getJobTitleId() != null) {
			sb.append(" AND job_title_id = ? ");
			param.add(employeeForm.getJobTitleId());
		}
		
		if(employeeForm.getDepartmentId() != null) {
			sb.append(" AND department_id = ? ");
			param.add(employeeForm.getDepartmentId());
		}
		
		if(employeeForm.getCompanyId() != null) {
			sb.append(" AND company_id = ? ");
			param.add(employeeForm.getCompanyId());
		}
		
		if(employeeForm.getModifier() != null) {
			sb.append(" AND modifier = ? ");
			param.add(employeeForm.getModifier());
		}
		
		if(employeeForm.getCreator() != null) {
			sb.append(" AND creator = ? ");
			param.add(employeeForm.getCreator());
		}
		
		sb.append(" AND status = 1 ");
		
//		if(StringUtils.isNotBlank(employeeForm.getUpdateTimeStart())) {
//			sb.append(" AND ( update_time >= ?) "); //update_time IS NULL OR
//			param.add(employeeForm.getUpdateTimeStart());
//		}
//		
//		if(StringUtils.isNotBlank(employeeForm.getUpdateTimeEnd())) {
//			sb.append(" AND ( update_time <= ?) "); //update_time IS NULL OR
//			param.add(employeeForm.getUpdateTimeEnd());
//		}
//		
//		if(StringUtils.isNotBlank(employeeForm.getCreateTimeStart())) {
//			sb.append(" AND ( create_time >= ?) "); //create_time IS NULL OR
//			param.add(employeeForm.getCreateTimeStart());
//		}
//		
//		if(StringUtils.isNotBlank(employeeForm.getCreateTimeEnd())) {
//			sb.append(" AND ( create_time <= ?) "); //create_time IS NULL OR
//			param.add(employeeForm.getCreateTimeEnd());
//		}
		
		StringBuilder sql = new StringBuilder("SELECT e.*, "
				+ "(SELECT name FROM department where id=e.department_id) departmentName, "
				+ "(SELECT name FROM job_title where id=e.job_title_id) jobTitleName, "
				+ "(SELECT name FROM company where id=e.company_id) companyName, "
				+ "(select login_account FROM t_erp_user where id = e.creator) as creatorName, "
				+ "(select login_account FROM t_erp_user where id = e.modifier) as modifierName "
				+ "FROM `employee` e WHERE 1=1 ").append(sb.toString());
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM `employee` WHERE 1=1 ").append(sb.toString());
		sql.append(" order by employee_no ");
		if(!employeeForm.isExport()) {
			sql.append(" LIMIT ").append((employeeForm.getPageNo()-1)*employeeForm.getPageSize()).append(",").append(employeeForm.getPageSize());
		}
		Page<Employee> page = new Page<Employee>(employeeForm.getPageSize());

		page = (Page<Employee>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(), 
				new BeanPropertyRowMapper<Employee>(Employee.class));		
		page.setPageNo(employeeForm.getPageNo());
		return page;
	}

	@Override
	public int delete(Long id, Long modifier) {
		String sql = "UPDATE `employee` SET `modifier`=?, `update_time`=NOW(), `status`=2 WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{modifier, id});
	}
	
	@Override
	public int delete(String employeeNo, Long modifier) {
		String sql = "UPDATE `employee` SET `modifier`=?, `update_time`=NOW(), `status`=2 WHERE employee_no=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{modifier, employeeNo});
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Employee> findAllEmployee() {
		String sql = "SELECT * FROM `employee` WHERE `status`=1 ORDER BY CAST(CONVERT(`name` using big5) AS BINARY)";
		return super.queryForList(dsBmsQry, sql, new Object[]{}, Employee.class);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Employee> findAllEmployee(int companyId) {
		String sql = "SELECT * FROM `employee` WHERE company_id=? AND `status`=1 ORDER BY CAST(CONVERT(`name` using big5) AS BINARY)";
		return super.queryForList(dsBmsQry, sql, new Object[]{companyId}, Employee.class);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Employee> findAllEmployee(int companyId, int departmentId) {
		String sql = "SELECT e.*, u.id user_id, (SELECT name FROM job_title where id=e.job_title_id) jobTitleName FROM `employee` e RIGHT JOIN t_erp_user u ON u.employee_id=e.id WHERE company_id=? AND department_id=? AND u.`status`=1 AND employee_no not like 'X%' ORDER BY CAST(CONVERT(`name` using big5) AS BINARY)";
		return super.queryForList(dsBmsQry, sql, new Object[]{companyId, departmentId}, Employee.class);
	}

	@Override
	public boolean checkEmpNoExist(Long compnayId, Long id, String employeeNo) {
		id = id == null? -1:id;
		String sql = "SELECT COUNT(*) FROM `employee` WHERE company_id=? AND employee_no = ? AND id NOT IN (?) AND `status`=1";
		return super.queryForInt(dsBmsQry, sql, new Object[]{compnayId, employeeNo, id}) > 0;
	}

	@Override
	public Employee findOneByName(String name) {
		String sql = "SELECT * FROM `employee` WHERE name=? and `status`=1";
		Object[] args = new Object[]{name};
		
		Employee employee = (Employee)super.queryForObject(dsBmsQry, sql, args, Employee.class);

		return employee;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Employee> findAllEmployee2() {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT dept.`name` departmentName, dept.dept_no departmentNo, jt.`name` jobTitleName, emp.`name`, emp.employee_no FROM employee emp ");
		sql.append("LEFT JOIN department dept on emp.department_id=dept.id ");
		sql.append("LEFT JOIN job_title jt ON emp.job_title_id=jt.id ");
		sql.append("WHERE emp.`status`=1 ");
		sql.append("ORDER BY dept.dept_no, emp.employee_no");
		
		return queryForList(dsBmsQry, sql.toString(), new Object[]{}, Employee.class);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Employee> findEmployeeByRole(int roleId, int...extraParam) {
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		param.add(roleId);
		
		sql.append("SELECT * FROM employee WHERE id in (SELECT employee_id FROM t_erp_user WHERE id in (SELECT user_id FROM form_user_role WHERE role_id=?)) AND status=1");
		
		if(extraParam.length > 0) {
			int departmentId = extraParam[0];
			
			sql.append(" AND department_id=?");
			param.add(departmentId);
		}
		
		return queryForList(dsBmsQry, sql.toString(), param.toArray(), Employee.class);
	}
	
	@Override
	public Employee findOneByUID(int uid) {
		String sql = "SELECT * FROM `employee` WHERE id IN (SELECT employee_id FROM t_erp_user WHERE id=?) and `status`=1";
		Object[] args = new Object[]{uid};
		
		Employee employee = (Employee)super.queryForObject(dsBmsQry, sql, args, Employee.class);

		return employee;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Employee> findSupervisor(Long departmentId) {
		// TODO supervisor role id
		String sql = "SELECT e.*, u.id user_id FROM employee e RIGHT JOIN t_erp_user u ON u.employee_id=e.id RIGHT JOIN (SELECT user_id FROM form_user_role WHERE role_id=4) f ON f.user_id=u.id WHERE e.department_id=?;";
		Object[] args = new Object[]{departmentId};
		
		return queryForList(dsBmsQry, sql.toString(), args, Employee.class);
	}
	
	@Override
	public List<Long> findUserManageDept(Long uid) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT department_id FROM employee WHERE id IN (");
		sql.append("SELECT employee_id FROM t_erp_user WHERE `status`=1 AND id=? "); 
		sql.append("UNION "); 
		sql.append("SELECT employee_id FROM t_erp_user_employee WHERE user_id=?) AND `status`=1");
		
		Object[] args = new Object[]{uid, uid};
		
		return queryForLongList(dsBmsQry, sql.toString(), args);
	}
	
	@Override
	public Employee findOneByEmployeeNo(String employeeNo) {
		String sql = "SELECT * FROM `employee` WHERE employee_no=? and `status`=1";
		Object[] args = new Object[]{employeeNo};
		
		Employee employee = (Employee)super.queryForObject(dsBmsQry, sql, args, Employee.class);

		return employee;
	}
	
	@Override
	public Employee findOneByEmployeeNo(Long companyId, String employeeNo) {
		String sql = "SELECT * FROM `employee` WHERE company_id=? AND employee_no=? and `status`=1";
		Object[] args = new Object[]{companyId, employeeNo};
		
		Employee employee = (Employee)super.queryForObject(dsBmsQry, sql, args, Employee.class);

		return employee;
	}
}