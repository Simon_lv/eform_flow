package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import com.baplay.constant.Flow;
import com.baplay.dao.IFormFlowDao;
import com.baplay.dto.FormFlow;
import com.baplay.entity.Role;
import com.baplay.form.FormMgrForm;

@Repository
public class FormFlowDaoImpl extends BaseDao<FormFlow> implements IFormFlowDao {
	
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}

	/* (non-Javadoc)
	 * @see com.baplay.dao.IFormFlowDao#findRunning(long, long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<FormFlow> findRunning(Set<Long> companyId, Set<Long> departmentId, long userId, List<Role> roles) {
		StringBuilder sql = new StringBuilder();
		// formName(group), count
		sql.append("SELECT (SELECT group_name FROM form where group_code=LEFT(wf.form_type, 2) LIMIT 1) form_name, COUNT(*) count, LEFT(wf.form_type, 2) form_type FROM work_flow wf ");
		// sql.append("LEFT JOIN work_flow_detail wfd ON wfd.work_flow_id=wf.id AND wfd.status in (?,?) "); 
		sql.append("LEFT JOIN (SELECT * from work_flow_detail WHERE (status IN (?,?) OR (status=? AND owner_id=?)) AND stamp_status=1 GROUP BY work_flow_id) wfd ON wfd.work_flow_id=wf.id ");
		sql.append("LEFT JOIN flow_step fs ON fs.id=wfd.step_id AND fs.flow_schema_id=wf.flow_schema_id ");
		// flow schema
		sql.append("LEFT JOIN flow_schema s ON s.id=wf.flow_schema_id ");
		// user flow
		sql.append("LEFT JOIN user_flow_step ufs ON ufs.work_flow_id=wf.id AND ufs.step_id=fs.id ");
		sql.append("WHERE ");
		// user flow
		sql.append("(ufs.user_id=? AND ufs.status=?) OR ");
		// draft		
		sql.append("(wf.flow_schema_id=? AND wf.creator=?) OR ");
		// reject
		sql.append("(wf.status=? AND wf.creator=?) OR "); 
		// locking
		sql.append("(wf.status=? AND wf.step_status=? AND wf.step_owner=?) OR ");
		// running(同公司,flow_schema.company_id != -1)
		sql.append("(wf.status=? AND wf.step_status=? AND IF(s.company_id=-1, TRUE, FIND_IN_SET(wf.company_id, ?)) AND (fs.role_id =-1 OR ");
		// form role 部門		
		sql.append("(wfd.countersign_role IN (SELECT role_id FROM form_user_role WHERE user_id=? AND status=1)) OR ");
		sql.append("(fs.role_id IN (SELECT role_id FROM form_user_role WHERE user_id=? AND status=1) ");
		sql.append("AND IF((SELECT same_department FROM form_user_role WHERE `status`=1 AND user_id=? LIMIT 1)=1, TRUE, FIND_IN_SET(wf.department_id, ?)) ");
		sql.append("AND ufs.user_id IS NULL AND wfd.countersign_role IS NULL AND IF(fs.same_department=?, FIND_IN_SET(wfd.department_id, ?), true)))) ");
		
		sql.append("GROUP BY left(wf.form_type, 2)");  // formName(group)
		
		return super.queryForList(dsBmsQry, sql.toString(), 
				new Object[]{Flow.RUNNING.getValue(), Flow.REJECT.getValue(),
					Flow.LOCK.getValue(), userId,	
					userId, Flow.WAITTING.getValue(),
					Flow.DRAFT_FLOW_SCHEMA.getValue(), userId, 
					Flow.REJECT.getValue(), userId,
					Flow.RUNNING.getValue(), Flow.LOCK.getValue(), userId,
					Flow.RUNNING.getValue(), Flow.WAITTING.getValue(), StringUtils.join(companyId, ","), userId, userId,
					userId, StringUtils.join(departmentId, ","),
					Flow.SAME_DEPARTMENT.getValue(), StringUtils.join(departmentId, ",")}, 
				FormFlow.class);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFormFlowDao#findRunningByType(long, long, java.lang.String)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<FormFlow> findRunningByType(Set<Long> companyId, Set<Long> departmentId, long userId, List<Role> roles, String formType) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT ");
		sql.append("(SELECT name FROM form WHERE code=SUBSTR(wf.form_type, 4) LIMIT 1) form_name, ");
		sql.append("wf.id work_flow_id, wfd.id work_flow_detail_id, wf.flow_schema_id, wf.serial_no, wf.status, IFNULL(fs.step_type, 1) step_type, IFNULL(fs.step_order, 1) step_order, wfd.countersign_role, ");
		sql.append("IF('IS'=?, (SELECT issue_title FROM issue_form WHERE work_flow_id=wf.id), '') description ");
		sql.append("FROM work_flow wf ");
		// sql.append("LEFT JOIN work_flow_detail wfd ON wfd.work_flow_id=wf.id AND wfd.status in (?,?) ");
		sql.append("LEFT JOIN (SELECT MAX(id) id, x.countersign_role, x.step_id, x.department_id, x.work_flow_id FROM work_flow_detail x WHERE (x.status IN (?,?) OR (status=? and owner_id=?)) AND stamp_status=1 GROUP BY work_flow_id) wfd ON wfd.work_flow_id=wf.id ");
		sql.append("LEFT JOIN flow_step fs ON fs.id=wfd.step_id AND fs.flow_schema_id=wf.flow_schema_id "); 
		// user flow
		sql.append("LEFT JOIN user_flow_step ufs ON ufs.work_flow_id=wf.id AND ufs.step_id=fs.id ");
		// flow schema
		sql.append("LEFT JOIN flow_schema s ON s.id=wf.flow_schema_id ");
		sql.append("WHERE LEFT(wf.form_type, 2)=? AND (");
		// user flow
		sql.append("(ufs.user_id=? AND ufs.status=?) OR ");
		// draft
		sql.append("(wf.flow_schema_id=? AND wf.creator=?) OR ");
		// reject
		sql.append("(wf.status=? AND wf.creator=?) OR ");
		// locking
		sql.append("(wf.status=? AND wf.step_status=? AND wf.step_owner=?) OR ");
		// running(同公司,flow_schema.company_id != -1)
		sql.append("(wf.status=? AND wf.step_status=? AND IF(s.company_id=-1, TRUE, FIND_IN_SET(wf.company_id, ?)) AND (fs.role_id =-1 OR ");
		sql.append("(wfd.countersign_role IN (SELECT role_id FROM form_user_role WHERE user_id=? AND status=1)) OR ");		
		sql.append("(fs.role_id IN (SELECT role_id FROM form_user_role WHERE user_id=? AND status=1) AND ufs.user_id IS NULL AND wfd.countersign_role IS NULL AND IF(fs.same_department=?, FIND_IN_SET(wfd.department_id, ?), true))))) ");
		sql.append("AND IF((SELECT same_department FROM form_user_role WHERE `status`=1 AND user_id=? LIMIT 1)=1, TRUE, FIND_IN_SET(wf.department_id, ?)) ");
		sql.append("ORDER BY wf.create_time");
		
		return super.queryForList(dsBmsQry, sql.toString(), 
				new Object[]{formType, Flow.RUNNING.getValue(), Flow.REJECT.getValue(),
					Flow.LOCK.getValue(), userId,
					formType,
					userId, Flow.WAITTING.getValue(),
					Flow.DRAFT_FLOW_SCHEMA.getValue(), userId, 
					Flow.REJECT.getValue(), userId,
					Flow.RUNNING.getValue(), Flow.LOCK.getValue(), userId,
					Flow.RUNNING.getValue(), Flow.WAITTING.getValue(), StringUtils.join(companyId, ","), userId, userId, 
					Flow.SAME_DEPARTMENT.getValue(), StringUtils.join(departmentId, ","),
					userId, StringUtils.join(departmentId, ",")}, 
				FormFlow.class);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFormFlowDao#findHistory(long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<FormFlow> findHistory(long workFlowId) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT fs.step_type, ");
		sql.append("(SELECT name FROM employee WHERE id=(SELECT employee_id FROM t_erp_user WHERE id=wfd.creator)) creator, ");
		sql.append("wfd.create_time, ");
		sql.append("(SELECT name FROM employee WHERE id=(SELECT employee_id FROM t_erp_user WHERE id=wfd.owner_id)) owner, "); 
		sql.append("wfd.process_time, wfd.status, wfd.memo, wfd.countersign_role "); 
		sql.append("FROM work_flow_detail wfd ");
		sql.append("LEFT JOIN flow_step fs ON fs.id=wfd.step_id "); 
		sql.append("WHERE work_flow_id=? AND wfd.step_id!=? ");  // skip draft
		sql.append("ORDER BY create_time"); 
		
		return super.queryForList(dsBmsQry, sql.toString(), 
				new Object[]{workFlowId, Flow.DRAFT_FLOW_STEP.getValue()}, 
				FormFlow.class);
	}		
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFormFlowDao#query(com.baplay.form.FormMgrForm)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Page<FormFlow> list(FormMgrForm form) { 
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		
		sql.append("SELECT wfd.work_flow_id, wfd.id work_flow_detail_id, wf.flow_schema_id, serial_no, f.name form_name, form_type, ");
		sql.append("step_type, step_order, wf.creator, wf.create_time, wfd.owner_id, wfd.process_time, wf.status, step_status, countersign_role, ");
		sql.append("(SELECT name FROM company WHERE id=wf.company_id) company_name, ");
		sql.append("(SELECT name FROM department WHERE id=wf.department_id) department_name, ");
		sql.append("(SELECT name FROM employee WHERE id=(SELECT employee_id FROM t_erp_user WHERE id=wf.creator)) creator_name, ");
		sql.append("IFNULL((SELECT name FROM employee WHERE id=(SELECT employee_id FROM t_erp_user WHERE id=wfd.owner_id)), '') owner_name ");
		sql.append("FROM ");
		sql.append("(SELECT MAX(id) id, work_flow_id FROM work_flow_detail GROUP BY work_flow_id) max ");
		sql.append("INNER JOIN work_flow wf ON wf.id=max.work_flow_id "); 
		sql.append("INNER JOIN form f ON f.flow_schema=wf.flow_schema_id "); 
		sql.append("INNER JOIN work_flow_detail wfd ON wfd.id=max.id AND wfd.work_flow_id=max.work_flow_id ");
		sql.append("INNER JOIN flow_step s ON s.id=wfd.step_id ");
		sql.append("WHERE 1=1 ");
		
		StringBuilder countSql = new StringBuilder("SELECT count(*) FROM work_flow wf WHERE 1=1 ");
		
		if(form.getCompanyId() != null) {
			sql.append(" AND wf.company_id = ? ");
			countSql.append(" AND wf.company_id = ? ");
			
			param.add(form.getCompanyId());
		}
		
		if(form.getRequestDepartment() != null) {
			sql.append(" AND wf.department_id = ? ");
			countSql.append(" AND wf.department_id = ? ");
			
			param.add(form.getRequestDepartment());
		}
		
		if(form.getStatus() != null) {
			sql.append(" AND wf.status = ? ");
			countSql.append(" AND wf.status = ? ");
			
			param.add(form.getStatus());
		}
		
		if(StringUtils.isNotBlank(form.getRequestDateStart())) {		
			sql.append(" AND STR_TO_DATE(wf.create_time, '%Y-%m-%d') >= ? ");
			countSql.append(" AND STR_TO_DATE(wf.create_time, '%Y-%m-%d') >= ? ");
			
			param.add(form.getRequestDateStart());
		}
		
		if(StringUtils.isNotBlank(form.getRequestDateEnd())) {
			sql.append(" AND STR_TO_DATE(wf.create_time, '%Y-%m-%d') <= ? ");
			countSql.append(" AND STR_TO_DATE(wf.create_time, '%Y-%m-%d') <= ? ");
			
			param.add(form.getRequestDateEnd());
		}
		
		if(StringUtils.isNotBlank(form.getFormType())) {
			sql.append(" AND wf.form_type = ? ");
			countSql.append(" AND wf.form_type = ? ");
			
			param.add(form.getFormType());
		}
		
		if(!form.isExport()) {
			sql.append(" LIMIT ").append((form.getPageNo() - 1) * form.getPageSize()).append(",").append(form.getPageSize());
		}
		
		Page<FormFlow> page = new Page<FormFlow>(form.getPageSize());

		page = (Page<FormFlow>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(), new BeanPropertyRowMapper<FormFlow>(FormFlow.class));
		
		if(!form.isExport()) {
			page.setPageNo(form.getPageNo());
		}
		
		return page;		
	}
}