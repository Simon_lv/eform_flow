package com.baplay.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.baplay.dao.IFormRoleDao;
import com.baplay.entity.FormRole;

@Repository
public class FormRoleDaoImpl extends BaseDao<FormRole> implements IFormRoleDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFormRoleDao#getRole(java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<FormRole> getRole(Long userId) {
		return this.queryForList(dsBmsQry,
				"select r.* from form_role r left join form_user_role ur on r.id = ur.role_id where r.status = 1 AND ur.status = 1 AND ur.user_id = ?",
				new Object[] { userId }, new BeanPropertyRowMapper<FormRole>(FormRole.class));
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFormRoleDao#findOneById(int)
	 */
	@Override
	public FormRole findOneById(int roleId) {
		return this.queryForObject(dsBmsQry, "select * from form_role where id=?", new Object[] { roleId }, FormRole.class);		
	}
}