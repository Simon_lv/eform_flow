package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import com.baplay.dao.IQcFormDao;
import com.baplay.entity.QcForm;
import com.baplay.form.QcFormForm;

@Repository
public class QcFormDaoImpl extends BaseDao<QcForm> implements IQcFormDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Page<QcForm> list(QcFormForm qcFormForm) {		
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		
		// Auth
		param.add(qcFormForm.getLoginUser());
		param.add(qcFormForm.getFormType());
		param.add(qcFormForm.getLoginUser());
		param.add(qcFormForm.getLoginUser());
		
		if(qcFormForm.getCompanyId() != null){
			sb.append(" AND b.company_id = ? ");
			param.add(qcFormForm.getCompanyId());
		}
		if(qcFormForm.getStatus() != null){
			sb.append(" AND b.status = ? ");
			param.add(qcFormForm.getStatus());
		}
		if(StringUtils.isNotBlank(qcFormForm.getSerialNo())){
			sb.append(" AND b.serial_no = ? ");
			param.add(qcFormForm.getSerialNo());
		}
		
		if(StringUtils.isNotBlank(qcFormForm.getRequestDateStart())){		
			sb.append(" AND STR_TO_DATE(b.request_date, '%Y-%m-%d') >= ? ");
			param.add(qcFormForm.getRequestDateStart());
		}
		if(StringUtils.isNotBlank(qcFormForm.getRequestDateEnd())){
			sb.append(" AND STR_TO_DATE(b.request_date, '%Y-%m-%d') <= ? ");
			param.add(qcFormForm.getRequestDateEnd());
		}	
		
		// Auth
		String userFormRole = StringUtils.join(qcFormForm.getFormRole(), "','");
		
		StringBuilder sql = new StringBuilder("SELECT b.* FROM (select a.*, group_concat(sub_title) sub_title_list, (SELECT COUNT(*) FROM form_auth WHERE form_type='QC' and form_role IN (SELECT id FROM form_role WHERE `name` IN('" + userFormRole + "'))) as auth, (select form_name from qc_form_schema where id = a.schema_id) as formName, (select name from company where id = a.company_id) as companyName, (select creator from work_flow where id = a.work_flow_id) as creator, (select status from work_flow where id = a.work_flow_id) as STATUS, (select name from employee where id = (select employee_id from t_erp_user where id = (select creator from work_flow where id = a.work_flow_id))) as creatorName from qc_form as a group by a.work_flow_id ) as b ");
		sql.append("left join (select work_flow_id, owner_id from work_flow_detail where owner_id=? and work_flow_id in (select id from work_flow where LEFT(form_type, 2)=?) group by work_flow_id) wfd on wfd.work_flow_id=b.work_flow_id ").append("WHERE (auth>=1 OR creator=? OR owner_id=?)").append(sb.toString());
		StringBuilder countSql = new StringBuilder("select count(*) from (").append(sql).append(") x");		
		
		if(!qcFormForm.isExport()) {
			sql.append(" LIMIT ").append((qcFormForm.getPageNo()-1)*qcFormForm.getPageSize()).append(",").append(qcFormForm.getPageSize());
		}
		
		Page<QcForm> page = new Page<QcForm>(qcFormForm.getPageSize());
		page = (Page<QcForm>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(), 
				new BeanPropertyRowMapper<QcForm>(QcForm.class));
		page.setPageNo(qcFormForm.getPageNo());
		return page;
	}

	@Override
	public QcForm save(QcForm qcForm) {
		String sql = "INSERT INTO `qc_form` (`company_id`, `request_date`, `serial_no`, `schema_id`, `schema_version`, `field_id`, `qc_result`, `pm_result`, `remark`, `sub_title`, `form_id`, `title_id`, `work_flow_id`) VALUES (?, NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		

		return (QcForm) super.addForObject(dsBmsUpd, sql, qcForm, 
				new Object[]{ 
						qcForm.getCompanyId(), qcForm.getSerialNo(),
						qcForm.getSchemaId(), qcForm.getSchemaVersion(), 
						qcForm.getFieldId(), qcForm.getQcResult(),
						qcForm.getPmResult(), qcForm.getRemark(),
						qcForm.getSubTitle(), qcForm.getFormId(), 
						qcForm.getTitleId(), qcForm.getWorkFlowId() });
	}
	
	@Override
	public int update(QcForm qcForm) {
		StringBuffer sql = new StringBuffer("UPDATE `qc_form` SET `company_id`=?, `serial_no`=?, `qc_result`=?, `pm_result`=?, `remark`=?, `sub_title`=? WHERE `form_id`=?");
		ArrayList<Object> param = new ArrayList<Object>();
		param.add(qcForm.getCompanyId());
		param.add(qcForm.getSerialNo());
		param.add(qcForm.getQcResult());
		param.add(qcForm.getPmResult());
		param.add(qcForm.getRemark());
		param.add(qcForm.getSubTitle());
		param.add(qcForm.getFormId());
		
		if(qcForm.getFieldId() != null){
			sql.append(" and `field_id` = ? ");
			param.add(qcForm.getFieldId());
		}
		if(qcForm.getTitleId() != null){
			sql.append(" and `title_id` = ? ");
			param.add(qcForm.getTitleId());
		}
		
		return super.updateForObject(dsBmsUpd, sql.toString(), param.toArray());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<QcForm> findAllByFormId(Long formId) {
		String sql = "select * from qc_form where form_id = ?";
		return this.queryForList(dsBmsQry, sql, new Object[]{formId}, QcForm.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<QcForm> findAllByFlow(Long workFlowId) {
		String sql = "SELECT a.*, (SELECT status FROM work_flow WHERE id = a.work_flow_id) AS status, (SELECT name FROM company where id = a.company_id) as companyName, (select name from employee where id = (select employee_id from t_erp_user where id = (select creator from work_flow where id = a.work_flow_id))) as creatorName FROM qc_form a WHERE a.work_flow_id=? order by field_id, title_id";
		Object[] args = new Object[]{workFlowId};
		//return (QcForm) super.queryForObject(dsBmsQry, sql, args, QcForm.class);
		return super.queryForList(dsBmsQry, sql, args, QcForm.class);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IQcFormDao#findOneByFieldId(java.lang.Long, java.lang.Long)
	 */
	@Override
	public QcForm findOneByFieldId(Long formId, Long fieldId) {
		String sql = "SELECT * FROM qc_form WHERE form_id = ? AND field_id = ?";
		return (QcForm) queryForObject(dsBmsQry, sql, new Object[]{formId, fieldId}, QcForm.class);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IQcFormDao#updateResult(java.lang.Long, java.lang.Long, java.lang.Integer, java.lang.String)
	 */
	@Override
	public int updateResult(Long formId, Long fieldId, Integer result, String remark) {
		StringBuffer sql = new StringBuffer("UPDATE `qc_form` SET `qc_result`=?, `remark`=? WHERE `form_id`=? AND field_id=?");
		ArrayList<Object> param = new ArrayList<Object>();
		param.add(result);
		param.add(remark);
		param.add(formId);
		param.add(fieldId);
		
		return super.updateForObject(dsBmsUpd, sql.toString(), param.toArray());
	}
}