package com.baplay.dao.impl;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import com.baplay.dao.IBudgetFormDao;
import com.baplay.entity.BudgetForm;
import com.baplay.form.BudgetFormForm;

@Repository
public class BudgetFormDaoImpl extends BaseDao<BudgetForm> implements IBudgetFormDao {
	
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public BudgetForm add(BudgetForm budgetForm) {
		String sql = "INSERT INTO `budget_form` (`company_id`, `request_date`, `request_department`, `serial_no`, `year_month`, `total_amount`, `additional_reference_id`, `additional_reason`, `work_flow_id`) VALUES (?, NOW(), ?, ?, ?, ?, ?, ?, ?)";
		return (BudgetForm) super.addForObject(dsBmsUpd, sql, budgetForm, 
				new Object[]{ 
						budgetForm.getCompanyId(), budgetForm.getRequestDepartment(),
						budgetForm.getSerialNo(), budgetForm.getYearMonth(), budgetForm.getTotalAmount(),
						budgetForm.getAdditionalReferenceId(), budgetForm.getAdditionalReason(),
						budgetForm.getWorkFlowId() });
	}

	@Override
	public int update(BudgetForm budgetForm) {
		StringBuilder sql = new StringBuilder("UPDATE `budget_form` SET `company_id`=?, `request_department`=?, `serial_no`=?, `total_amount`=?, `additional_reason`=? [, `year_month`=? ] WHERE (`id`=?)");
		ArrayList<Object> param = new ArrayList<Object>();
		param.add(budgetForm.getCompanyId());
		param.add(budgetForm.getRequestDepartment());
		param.add(budgetForm.getSerialNo());
		param.add(budgetForm.getTotalAmount());
		param.add(budgetForm.getAdditionalReason());		
		param.add(budgetForm.getYearMonth());
		param.add(budgetForm.getId());
		
		return super.updateForObject(dsBmsUpd, sql.toString(), param.toArray());
	}

	@Override
	public BudgetForm findOneById(Long id) {
		String sql = "SELECT a.*, (select name from company where id = a.company_id) as companyName, (select name from department where id = a.request_department) as departmentName, (select name from employee where id = (select employee_id from t_erp_user where id = (select creator from work_flow where id = a.work_flow_id))) as creatorName, (SELECT total_amount from budget_form where id = a.additional_reference_id) AS additionalAmount, (select status from work_flow where id = a.work_flow_id) as status FROM budget_form as a WHERE a.id=?";
		Object[] args = new Object[]{id};
		return (BudgetForm) super.queryForObject(dsBmsQry, sql, args, BudgetForm.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Page<BudgetForm> list(BudgetFormForm budgetFormForm) {
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		
		// Auth
		param.add(budgetFormForm.getLoginUser());
		param.add(budgetFormForm.getFormType());
		param.add(budgetFormForm.getLoginUser());
		param.add(budgetFormForm.getLoginUser());
		
		if(budgetFormForm.getCompanyId() != null){
			sb.append(" AND b.company_id = ? ");
			param.add(budgetFormForm.getCompanyId());
		}
		if(budgetFormForm.getRequestDepartment() != null){
			sb.append(" AND b.request_department = ? ");
			param.add(budgetFormForm.getRequestDepartment());
		}
		if(StringUtils.isNotBlank(budgetFormForm.getSerialNo())){
			sb.append(" AND b.serial_no = ? ");
			param.add(budgetFormForm.getSerialNo());
		}
		if(StringUtils.isNotBlank(budgetFormForm.getRequestDateStart())){		
			sb.append(" AND STR_TO_DATE(b.request_date, '%Y-%m-%d') >= ? ");
			param.add(budgetFormForm.getRequestDateStart());
		}
		if(StringUtils.isNotBlank(budgetFormForm.getRequestDateEnd())){
			sb.append(" AND STR_TO_DATE(b.request_date, '%Y-%m-%d') <= ? ");
			param.add(budgetFormForm.getRequestDateEnd());
		}
		if(budgetFormForm.getStatus() != null){
			sb.append(" AND b.status = ? ");
			param.add(budgetFormForm.getStatus());
		}
		
		// Auth
		String userFormRole = StringUtils.join(budgetFormForm.getFormRole(), "','");
		
		StringBuilder sql = new StringBuilder("SELECT b.* FROM (SELECT a.*, (SELECT COUNT(*) FROM form_auth WHERE form_type='BU' and form_role IN (SELECT id FROM form_role WHERE `name` IN('" + userFormRole + "'))) as auth, (SELECT status FROM work_flow WHERE id = a.work_flow_id) AS status, (select name from company where id = a.company_id) as companyName, (select name from department where id = a.request_department) as departmentName, (select creator from work_flow where id = a.work_flow_id) as creator, (SELECT `name` FROM employee WHERE id=(select employee_id from t_erp_user where id=(select creator from work_flow where id = a.work_flow_id))) as creatorName, (SELECT total_amount from budget_form where id = a.additional_reference_id) AS additionalAmount, (select count(*) from budget_form where additional_reference_id = a.id) as additionalCount FROM budget_form a) as b ");
		sql.append("left join (select work_flow_id, owner_id from work_flow_detail where owner_id=? and work_flow_id in (select id from work_flow where LEFT(form_type, 2)=?) group by work_flow_id) wfd on wfd.work_flow_id=b.work_flow_id ").append("WHERE (auth>=1 OR creator=? OR owner_id=?)").append(sb.toString());
		StringBuilder countSql = new StringBuilder("select count(*) from (").append(sql).append(") x");
				
		if(!budgetFormForm.isExport()) {
			sql.append(" LIMIT ").append((budgetFormForm.getPageNo()-1)*budgetFormForm.getPageSize()).append(",").append(budgetFormForm.getPageSize());
		}
		
		Page<BudgetForm> page = new Page<BudgetForm>(budgetFormForm.getPageSize());

		page = (Page<BudgetForm>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(), 
				new BeanPropertyRowMapper<BudgetForm>(BudgetForm.class));
		
		if(!budgetFormForm.isExport()) {
			page.setPageNo(budgetFormForm.getPageNo());
		}
		
		return page;
	}

	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM `budget_form` WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}

	@Override
	public BudgetForm findOneByFlow(Long workFlowId) {
		String sql = "SELECT * FROM `budget_form` WHERE (`work_flow_id`=?)";
		Object[] args = new Object[]{workFlowId};
		return (BudgetForm) super.queryForObject(dsBmsQry, sql, args, BudgetForm.class);
	}

	@Override
	public int updateTotal(Long id, Long editId) {
		String sql1 = "SELECT SUM(`amount`) FROM `budget_form_detail` WHERE (`budget_form_id`=? and id!=?)";		
		int sum = this.queryForInt(dsBmsQry, sql1, new Object[]{id, editId});
		String sql2 = "UPDATE `budget_form` SET `total_amount`=? WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql2, new Object[]{sum, id});
	}

	@Override
	public boolean isFiestBudget(Long id) {
		String sql = "SELECT COUNT(*) FROM budget_form WHERE additional_reference_id=?";
		return (super.queryForInt(dsBmsQry, sql, new Object[]{id}) > 0);
	}
	
}