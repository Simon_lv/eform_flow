package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import com.baplay.dao.ICompanyDao;
import com.baplay.entity.Company;
import com.baplay.form.CompanyForm;

@Repository
public class CompanyDaoImpl extends BaseDao<Company> implements ICompanyDao {
	
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public Company add(Company company) {
		String sql = "INSERT INTO `company` (`name`, `tax_number`, `address`, `phone`, `owner`, `establish_date`, `creator`, `create_time`, status) VALUES (?, ?, ?, ?, ?, ?, ?, NOW(), 1)";
		return (Company) super.addForObject(dsBmsUpd, sql, company, 
				new Object[]{company.getName(), company.getTaxNumber(), company.getAddress(), company.getPhone(), company.getOwner(), company.getEstablishDate(), company.getCreator()});
	}

	@Override
	public int update(Company company) {
		String sql = "UPDATE`company` SET `name`=?, `tax_number`=?, `address`=?, `phone`=?, `owner`=?, `establish_date`=?, `modifier`=?, `update_time`=NOW() WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, 
				new Object[]{company.getName(), company.getTaxNumber(), company.getAddress(), company.getPhone(), company.getOwner(), company.getEstablishDate(), company.getModifier(), company.getId()});
	}

	@Override
	public Company findOneById(Long id) {
		String sql = "SELECT c.*, (select name from employee where id = (select employee_id from t_erp_user where id=c.id)) creatorName FROM company c WHERE c.id=?";
		Object[] args = new Object[]{id};
		return (Company) super.queryForObject(dsBmsQry, sql, args, Company.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Page<Company> list(CompanyForm company) {
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		
		if(StringUtils.isNotBlank(company.getName())) {
			sb.append(" AND name LIKE ? ");
			param.add("%"+company.getName()+"%");
		}
		
		if(StringUtils.isNotBlank(company.getTaxNumber())) {
			sb.append(" AND tax_number = ? ");
			param.add(company.getTaxNumber());
		}
		
		if(StringUtils.isNotBlank(company.getAddress())) {
			sb.append(" AND phone LIKE ? ");
			param.add("%"+company.getAddress()+"%");
		}
		
		if(StringUtils.isNotBlank(company.getPhone())) {
			sb.append(" AND phone LIKE ? ");
			param.add("%"+company.getPhone()+"%");
		}
		
		if(StringUtils.isNotBlank(company.getOwner())) {
			sb.append(" AND owner = ? ");
			param.add(company.getOwner());
		}
		
		if(StringUtils.isNotBlank(company.getEstablishDateStart())) {
			sb.append(" AND ( establish_date >= ?) ");
			param.add(company.getEstablishDateStart());
		}
		
		if(StringUtils.isNotBlank(company.getEstablishDateEnd())) {
			sb.append(" AND ( establish_date <= ?) ");
			param.add(company.getEstablishDateEnd());
		}

		sb.append(" AND status = 1 ");
		
		StringBuilder sql = new StringBuilder("SELECT a.*, (select login_account from t_erp_user where id = a.creator) as creatorName, (select login_account from t_erp_user where id = a.modifier) as modifierName FROM company as a WHERE 1=1").append(sb.toString());
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM ( ").append(sql).append(") x");
		sql.append(" ORDER BY a.create_time DESC ");
		
		if(!company.isExport()) {
			sql.append(" LIMIT ").append((company.getPageNo()-1)*company.getPageSize()).append(",").append(company.getPageSize());
		}
		
		Page<Company> page = new Page<Company>(company.getPageSize());

		page = (Page<Company>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(), 
				new BeanPropertyRowMapper<Company>(Company.class));
		page.setPageNo(company.getPageNo());
		return page;
	}

	@Override
	public int delete(Long id, Long modifier) {
		String sql = "UPDATE `company` SET `modifier`=?, `update_time`=NOW(), `status`=2 WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{modifier, id});
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Company> findAllCompany() {
		String sql = "SELECT * FROM company WHERE `status`=1";
		return super.queryForList(dsBmsQry, sql, new Object[]{}, Company.class);
	}
	
	@Override
	public Company findOneByName(String keyword, boolean local) {
		String sql = "SELECT c.*, (select name from employee where id = (select employee_id from t_erp_user where id=c.id)) creatorName FROM company c WHERE c.name REGEXP ?";
		
		if(local) {
			sql += " and `name` REGEXP '[\\(.*\\)]+'=0";  // 沒有刮號
		}		
		
		return (Company) super.queryForObject(dsBmsQry, sql, new Object[]{keyword}, Company.class);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Company> findAllGameCompany() {
		String sql = "SELECT * FROM company WHERE `status`=1 AND ID IN (SELECT company_id FROM game_data WHERE `status`=1 AND `online`=1 GROUP BY company_id)";
		return super.queryForList(dsBmsQry, sql, new Object[]{}, Company.class);
	}
}