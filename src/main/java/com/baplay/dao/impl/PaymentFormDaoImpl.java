package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import com.baplay.dao.IPaymentFormDao;
import com.baplay.entity.PaymentForm;
import com.baplay.form.PaymentFormForm;

@Repository
public class PaymentFormDaoImpl extends BaseDao<PaymentForm> implements IPaymentFormDao {
	
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public PaymentForm add(PaymentForm paymentForm) {	
		String sql = "INSERT INTO `payment_form` (`company_id`, `request_department`, `employee_id`, `request_date`, `serial_no`, `payment_way`, `remit_date`, `payee`, `payee_bank`, `payee_account`, `bank_clearing_code`, `message`, `contact`, `contact_tel`, `total_amount`, `work_flow_id`) VALUES (?, ?, ?, NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		return (PaymentForm) super.addForObject(dsBmsUpd, sql, paymentForm, 
				new Object[]{ paymentForm.getCompanyId(), paymentForm.getRequestDepartment(), paymentForm.getEmployeeId(),
						paymentForm.getSerialNo(), paymentForm.getPaymentWay(), paymentForm.getRemitDate(), paymentForm.getPayee(),
						paymentForm.getPayeeBank(), paymentForm.getPayeeAccount(), paymentForm.getBankClearingCode(),
						paymentForm.getMessage(), paymentForm.getContact(), paymentForm.getContactTel(),
						paymentForm.getTotalAmount(), paymentForm.getWorkFlowId() });
	}

	@Override
	public int update(PaymentForm paymentForm) {
		String sql = "UPDATE `payment_form` SET `company_id`=?, `request_department`=?, `employee_id`=?, `serial_no`=?, `payment_way`=?, `remit_date`=?, `payee`=?, `payee_bank`=?, `payee_account`=?, `bank_clearing_code`=?, `message`=?, `contact`=?, `contact_tel`=?, `total_amount`=? WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, 
				new Object[]{ paymentForm.getCompanyId(), paymentForm.getRequestDepartment(), paymentForm.getEmployeeId(), 
						paymentForm.getSerialNo(), paymentForm.getPaymentWay(), paymentForm.getRemitDate(), paymentForm.getPayee(),
						paymentForm.getPayeeBank(), paymentForm.getPayeeAccount(), paymentForm.getBankClearingCode(),
						paymentForm.getMessage(), paymentForm.getContact(), paymentForm.getContactTel(),
						paymentForm.getTotalAmount(), paymentForm.getId() });
	}

	@Override
	public PaymentForm findOneById(Long id) {
		String sql = "SELECT a.*, (SELECT status FROM work_flow WHERE id = a.work_flow_id) AS status, (SELECT name FROM company where id = a.company_id) as companyName, (SELECT name FROM department where id = a.request_department) as departmentName, (select name from employee where id = (select employee_id from t_erp_user where id = (select creator from work_flow where id = a.work_flow_id))) as creatorName, (select name from employee where id = a.employee_id) employee_name FROM payment_form a WHERE a.id=?";
		Object[] args = new Object[]{id};
		return (PaymentForm) super.queryForObject(dsBmsQry, sql, args, PaymentForm.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Page<PaymentForm> list(PaymentFormForm paymentFormForm) {
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		
		// Auth
		param.add(paymentFormForm.getLoginUser());
		param.add(paymentFormForm.getFormType());
		param.add(paymentFormForm.getLoginUser());
		param.add(paymentFormForm.getLoginUser());
		
		if(paymentFormForm.getCompanyId() != null){
			sb.append(" AND b.company_id = ? ");
			param.add(paymentFormForm.getCompanyId());
		}
		if(paymentFormForm.getRequestDepartment() != null){
			sb.append(" AND b.request_department = ? ");
			param.add(paymentFormForm.getRequestDepartment());
		}
		if(StringUtils.isNotBlank(paymentFormForm.getSerialNo())){
			sb.append(" AND b.serial_no = ? ");
			param.add(paymentFormForm.getSerialNo());
		}
		if(StringUtils.isNotBlank(paymentFormForm.getRequestDateStart())){		
			sb.append(" AND STR_TO_DATE(b.request_date, '%Y-%m-%d') >= ? ");
			param.add(paymentFormForm.getRequestDateStart());
		}
		if(StringUtils.isNotBlank(paymentFormForm.getRequestDateEnd())){
			sb.append(" AND STR_TO_DATE(b.request_date, '%Y-%m-%d') <= ? ");
			param.add(paymentFormForm.getRequestDateEnd());
		}		
		if(StringUtils.isNotBlank(paymentFormForm.getRemitDateStart())){		
			sb.append(" AND STR_TO_DATE(b.remit_date, '%Y-%m-%d') >= ? ");
			param.add(paymentFormForm.getRemitDateStart());
		}
		if(StringUtils.isNotBlank(paymentFormForm.getRemitDateEnd())){
			sb.append(" AND STR_TO_DATE(b.remit_date, '%Y-%m-%d') <= ? ");
			param.add(paymentFormForm.getRemitDateEnd());
		}
		if(paymentFormForm.getRemitted() != null){
			sb.append(" AND b.remitted = ? ");
			param.add(paymentFormForm.getRemitted());
		}
		if(paymentFormForm.getStatus() != null){
			sb.append(" AND b.status = ? ");
			param.add(paymentFormForm.getStatus());
		}
		
		// Auth
		String userFormRole = StringUtils.join(paymentFormForm.getFormRole(), "','");
		
		StringBuilder sql = new StringBuilder("SELECT b.* FROM (SELECT a.*, (SELECT COUNT(*) FROM form_auth WHERE form_type='PA' and form_role IN (SELECT id FROM form_role WHERE `name` IN('" + userFormRole + "'))) as auth, (SELECT status FROM work_flow WHERE id = a.work_flow_id) AS status, (SELECT name FROM company where id = a.company_id) as companyName, (SELECT name FROM department where id = a.request_department) as departmentName, (select creator from work_flow where id = a.work_flow_id) as creator, (SELECT `name` FROM employee WHERE id=(select employee_id from t_erp_user where id=(select creator from work_flow where id = a.work_flow_id))) as creatorName, (select name from employee where id = a.employee_id) employee_name FROM payment_form a ) as b ");
		sql.append("left join (select work_flow_id, owner_id from work_flow_detail where owner_id=? and work_flow_id in (select id from work_flow where LEFT(form_type, 2)=?) group by work_flow_id) wfd on wfd.work_flow_id=b.work_flow_id ").append("WHERE (auth>=1 OR creator=? OR owner_id=?)").append(sb.toString());
		StringBuilder countSql = new StringBuilder("select count(*) from (").append(sql).append(") x");
				
		if(!paymentFormForm.isExport()) {
			sql.append(" LIMIT ").append((paymentFormForm.getPageNo()-1)*paymentFormForm.getPageSize()).append(",").append(paymentFormForm.getPageSize());
		}
		
		Page<PaymentForm> page = new Page<PaymentForm>(paymentFormForm.getPageSize());

		page = (Page<PaymentForm>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(), 
				new BeanPropertyRowMapper<PaymentForm>(PaymentForm.class));
		
		if(!paymentFormForm.isExport()) {
			page.setPageNo(paymentFormForm.getPageNo());
		}
		
		return page;
	}

	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM `payment_form` WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}
	
	@Override
	public PaymentForm findOneByFlow(Long id) {
		String sql = "SELECT a.*, (SELECT status FROM work_flow WHERE id = a.work_flow_id) AS status, (SELECT name FROM company where id = a.company_id) as companyName, (SELECT name FROM department where id = a.request_department) as departmentName, (select name from employee where id = (select employee_id from t_erp_user where id = (select creator from work_flow where id = a.work_flow_id))) as creatorName, (select name from employee where id = a.employee_id) employee_name FROM payment_form a WHERE a.work_flow_id=?";
		Object[] args = new Object[]{id};
		return (PaymentForm) super.queryForObject(dsBmsQry, sql, args, PaymentForm.class);
	}
	
	@Override
	public String getSerialNo(String formCode) {
		String sql = "SELECT nextval(?) as serialNo";
		Object[] args = new Object[]{formCode};
		return super.queryForString(dsBmsQry, sql, args);
	}
	
	@Override
	public int confirmRemitDate(Long id) {
		String sql = "UPDATE `payment_form` SET `remitted`=2 WHERE `id`=? AND `remitted`=1";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}
	
	@Override
	public int updateRemitDate(Long id, Date remitDate) {
		String sql = "UPDATE `payment_form` SET `remit_date`=? WHERE `id`=? AND `remitted`=1";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{remitDate, id});
	}
}