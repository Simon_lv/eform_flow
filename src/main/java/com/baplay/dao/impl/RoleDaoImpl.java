/**
 * 
 */
package com.baplay.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.baplay.dao.IRoleDao;
import com.baplay.entity.FormRole;
import com.baplay.entity.Role;
import com.baplay.entity.RoleFunction;

/**
 * @author Haozi
 *
 */
@Repository
public class RoleDaoImpl extends BaseDao<Role> implements IRoleDao {
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IRoleDao#getRole(Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Role> getRole(Long userId) {
		return this.queryForList(dsBmsQry,
				"select r.* from t_erp_role r left join t_erp_user_role ur on r.id = ur.role_id where r.status = 1 AND ur.status = 1 AND ur.user_id = ?",
				new Object[] { userId }, new BeanPropertyRowMapper<Role>(Role.class));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IRoleDao#addRole(String)
	 */
	@Override
	public Long addRole(String name) {
		Role old = (Role) this.queryForObject(dsBmsQry, "select * from t_erp_role where name = ?", new Object[] { name }, Role.class);
		if (old != null) {
			return old.getId();
		}
		Role r = new Role();
		r.setName(name);
		r.setStatus(1);
		r = (Role) this.addForParam(dsBmsUpd, "INSERT INTO t_erp_role (name, status) values(?,?)", r, new String[] { "name", "status" });
		return r.getId();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IRoleDao#addRoleFunction(Long, java.util.List)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void addRoleFunction(Long roleId, List<Long> functionId) {
		List<RoleFunction> old = this.queryForList(dsBmsQry, "select * from t_erp_role_function where role_id = ? AND status=1", new Object[] { roleId },
				new BeanPropertyRowMapper<RoleFunction>(RoleFunction.class));
		for (int i = 0; i < old.size(); i++) {
			this.updateForObject(dsBmsUpd, "update t_erp_role_function set status = ? where id=?", new Object[] { 2, old.get(i).getId() });
		}
		RoleFunction rf = new RoleFunction();
		for (int i = 0; i < functionId.size(); i++) {
			rf.setFunctionId(functionId.get(i));
			rf.setRoleId(roleId);
			this.addForParam(dsBmsUpd, "insert into t_erp_role_function (role_id,function_id,status)values(?,?,1)", rf, new String[] { "roleId", "functionId" });
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IRoleDao#updateRole(Long, String)
	 */
	@Override
	public void updateRole(Long roleId, String name) {
		Role old = (Role) this.queryForObject(dsBmsQry, "select * from t_erp_role where name = ?", new Object[] { name }, Role.class);
		if (old != null) {
			return;
		}
		this.updateForObject(dsBmsUpd, "update t_erp_role set name=? where id=?", new Object[] { name, roleId });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IRoleDao#updateRoleStatus(Long, Integer)
	 */
	@Override
	public void updateRoleStatus(Long roleId, Integer status) {
		this.updateForObject(dsBmsUpd, "update t_erp_role set status=? where id=?", new Object[] { status, roleId });
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Role> getAllRole(int all) {
		if (all == 1) {
			return this.queryForList(dsBmsQry, "select * from t_erp_role where status = 1", null, Role.class);
		} else {
			return this.queryForList(dsBmsQry, "select * from t_erp_role", null, Role.class);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<FormRole> getAllFormRole(int all) {
		if (all == 1) {
			return this.queryForList(dsBmsQry, "select * from form_role where status = 1", null, new BeanPropertyRowMapper(FormRole.class));			
		} else {
			return this.queryForList(dsBmsQry, "select * from form_role", null, new BeanPropertyRowMapper(FormRole.class));
		}
	}

	/* (non-Javadoc)
	 * @see com.baplay.dao.IRoleDao#findOneById(int)
	 */
	@Override
	public Role findOneById(int roleId) {
		return this.queryForObject(dsBmsQry, "select * from t_erp_role where id=?", new Object[] { roleId }, Role.class);		
	}
	
}