package com.baplay.dao.impl;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import com.baplay.dao.IContractReviewFormDao;
import com.baplay.entity.ContractReviewForm;
import com.baplay.form.ContractReviewFormForm;

@Repository
public class ContractReviewFormDaoImpl extends BaseDao<ContractReviewForm> implements IContractReviewFormDao {
	
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public ContractReviewForm add(ContractReviewForm contractReviewForm) {
		String sql = "INSERT INTO `contract_review_form` (`company_id`, `request_date`, `request_department`, `serial_no`, `contract_title`, `contract_target`, `organizer`, `organizer_extension`, `urgent_dispatch`, `urgent_dispatch_reason`, `sign_way`, `definite_term`, `key_point`, `work_flow_id`) VALUES (?, NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		

		return (ContractReviewForm) super.addForObject(dsBmsUpd, sql, contractReviewForm, 
				new Object[]{ 
						contractReviewForm.getCompanyId(), contractReviewForm.getRequestDepartment(),
						 contractReviewForm.getSerialNo(), contractReviewForm.getContractTitle(), 
						 contractReviewForm.getContractTarget(), contractReviewForm.getOrganizer(),
						 contractReviewForm.getOrganizerExtension(), contractReviewForm.getUrgentDispatch(),
						 contractReviewForm.getUrgentDispatchReason(), contractReviewForm.getSignWay(),
						 contractReviewForm.getDefiniteTerm(), contractReviewForm.getKeyPoint(),
						 contractReviewForm.getWorkFlowId() });
	}

	@Override
	public int update(ContractReviewForm contractReviewForm) {
		String sql = "UPDATE `contract_review_form` SET `company_id`=?, `request_department`=?, `serial_no`=?, `contract_title`=?, `contract_target`=?, `organizer_extension`=?, `urgent_dispatch`=?, `urgent_dispatch_reason`=?, `sign_way`=?, `definite_term`=?, `key_point`=? WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, 
				new Object[]{ contractReviewForm.getCompanyId(), contractReviewForm.getRequestDepartment(),						
						 contractReviewForm.getSerialNo(), contractReviewForm.getContractTitle(), contractReviewForm.getContractTarget(),
						 contractReviewForm.getOrganizerExtension(), 
						 contractReviewForm.getUrgentDispatch(), contractReviewForm.getUrgentDispatchReason(),
						 contractReviewForm.getSignWay(), contractReviewForm.getDefiniteTerm(),
						 contractReviewForm.getKeyPoint(), contractReviewForm.getId() });
	}

	@Override
	public ContractReviewForm findOneById(Long id) {
		String sql = "SELECT a.*, (select name from company where id = a.company_id) as companyName, (select name from department where id = a.request_department) as departmentName, (select name from employee where id = (select employee_id from t_erp_user where id = (select creator from work_flow where id = a.work_flow_id))) as creatorName, (select name from employee where id = a.organizer) as organizerName FROM contract_review_form as a WHERE a.id=?";
		Object[] args = new Object[]{id};
		return (ContractReviewForm) super.queryForObject(dsBmsQry, sql, args, ContractReviewForm.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Page<ContractReviewForm> list(ContractReviewFormForm contractReviewFormForm) {
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		
		// Auth
		param.add(contractReviewFormForm.getLoginUser());
		param.add(contractReviewFormForm.getFormType());
		param.add(contractReviewFormForm.getLoginUser());
		param.add(contractReviewFormForm.getLoginUser());
		
		if(contractReviewFormForm.getCompanyId() != null){
			sb.append(" AND b.company_id = ? ");
			param.add(contractReviewFormForm.getCompanyId());
		}
		if(contractReviewFormForm.getRequestDepartment() != null){
			sb.append(" AND b.request_department = ? ");
			param.add(contractReviewFormForm.getRequestDepartment());
		}
		if(StringUtils.isNotBlank(contractReviewFormForm.getSerialNo())){
			sb.append(" AND b.serial_no = ? ");
			param.add(contractReviewFormForm.getSerialNo());
		}
		if(StringUtils.isNotBlank(contractReviewFormForm.getRequestDateStart())){		
			sb.append(" AND STR_TO_DATE(b.request_date, '%Y-%m-%d') >= ? ");
			param.add(contractReviewFormForm.getRequestDateStart());
		}
		if(StringUtils.isNotBlank(contractReviewFormForm.getRequestDateEnd())){
			sb.append(" AND STR_TO_DATE(b.request_date, '%Y-%m-%d') <= ? ");
			param.add(contractReviewFormForm.getRequestDateEnd());
		}
		if(contractReviewFormForm.getStatus() != null){
			sb.append(" AND b.status = ? ");
			param.add(contractReviewFormForm.getStatus());
		}
		
		// Auth
		String userFormRole = StringUtils.join(contractReviewFormForm.getFormRole(), "','");
		
		StringBuilder sql = new StringBuilder("SELECT b.* FROM (SELECT a.*, (SELECT COUNT(*) FROM form_auth WHERE form_type='CO' and form_role IN (SELECT id FROM form_role WHERE `name` IN('" + userFormRole + "'))) as auth, (SELECT status FROM work_flow WHERE id = a.work_flow_id) AS status, (SELECT name FROM company where id = a.company_id) as companyName, (SELECT name FROM department where id = a.request_department) as departmentName, (select creator from work_flow where id = a.work_flow_id) as creator, (SELECT `name` FROM employee WHERE id=(select employee_id from t_erp_user where id=(select creator from work_flow where id = a.work_flow_id))) as creatorName, (select `name` from employee where id=a.organizer) organizerName FROM contract_review_form a ) as b ");
		sql.append("left join (select work_flow_id, owner_id from work_flow_detail where owner_id=? and work_flow_id in (select id from work_flow where LEFT(form_type, 2)=?) group by work_flow_id) wfd on wfd.work_flow_id=b.work_flow_id ").append("WHERE (auth>=1 OR creator=? OR owner_id=?)").append(sb.toString());
		StringBuilder countSql = new StringBuilder("select count(*) from (").append(sql).append(") x");
				
		if(!contractReviewFormForm.isExport()) {
			sql.append(" LIMIT ").append((contractReviewFormForm.getPageNo()-1)*contractReviewFormForm.getPageSize()).append(",").append(contractReviewFormForm.getPageSize());
		}
		
		Page<ContractReviewForm> page = new Page<ContractReviewForm>(contractReviewFormForm.getPageSize());

		page = (Page<ContractReviewForm>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(), 
				new BeanPropertyRowMapper<ContractReviewForm>(ContractReviewForm.class));
		if(!contractReviewFormForm.isExport()) {
			page.setPageNo(contractReviewFormForm.getPageNo());
		}
		
		return page;
	}

	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM `contract_review_form` WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}

	@Override
	public ContractReviewForm findOneByFlow(Long workFlowId) {
		String sql = "SELECT * FROM contract_review_form WHERE work_flow_id=?";
		Object[] args = new Object[]{workFlowId};
		return (ContractReviewForm) super.queryForObject(dsBmsQry, sql, args, ContractReviewForm.class);
	}
	
}