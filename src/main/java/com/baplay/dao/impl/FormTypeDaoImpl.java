package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.baplay.dao.IFormTypeDao;
import com.baplay.entity.FormType;

@Repository
public class FormTypeDaoImpl extends BaseDao<FormType> implements IFormTypeDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFormTypeDao#findOneByGroup(java.lang.String, java.lang.String)
	 */
	@Override
	public FormType findOneByCode(String code, String groupCode, Integer... flowSchemaId) {
		String sql = "";
		ArrayList<Object> param = new ArrayList<Object>();
		
		if(StringUtils.isEmpty(code)) {
			sql = "SELECT * FROM form WHERE `group_code`=? LIMIT 1";
			param.add(groupCode);
		}
		else {
			if(flowSchemaId.length > 0) {
				sql = "SELECT * FROM form WHERE `code`=? AND `group_code`=? AND `flow_schema`=?";
				param.add(code);
				param.add(groupCode);
				param.add(flowSchemaId[0]);
			}
			else {
				sql = "SELECT f.*, GROUP_CONCAT(flow_schema) allFlowSchema FROM form f WHERE `code`=? AND `group_code`=? GROUP BY `code`, `group_code`";
				param.add(code);
				param.add(groupCode);
			}
		}		
		
		FormType entity = (FormType) super.queryForObject(dsBmsQry, sql, param.toArray(), FormType.class);
		return entity;
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFormTypeDao#add(com.baplay.entity.FormType)
	 */
	@Override
	public FormType add(FormType formType) {
		String sql = "INSERT INTO `form` (`name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		return (FormType) super.addForObject(dsBmsUpd, sql, formType, 
				new Object[]{});
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFormTypeDao#update(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public int update(String groupCode, String name, String oldCode, String newCode) {
		String sql = "UPDATE `form` SET `name`=?, `code`=? WHERE `group_code`=? and `code`=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{name, newCode, oldCode, groupCode});
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IFormTypeDao#listAll()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<FormType> listAll() { 
		String sql = "SELECT f.*, CONCAT_WS(',', group_code, code) full_code FROM form f GROUP BY group_code, code ORDER BY CASE group_code WHEN 'QC' THEN 'Z' ELSE group_code END";
				
		return super.queryForList(dsBmsQry, sql, new Object[]{}, FormType.class);
	}
}