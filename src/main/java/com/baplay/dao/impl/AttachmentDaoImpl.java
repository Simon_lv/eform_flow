package com.baplay.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import com.baplay.dao.IAttachmentDao;
import com.baplay.entity.Attachment;

@Repository
public class AttachmentDaoImpl extends BaseDao<Attachment> implements IAttachmentDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public Attachment add(Attachment attachment) {
		String sql = "INSERT INTO `attachment` (`form_id`, `form_type`, `url`, `description`, `create_time`, `creator`) VALUES (?, ?, ?, ?, NOW(), ?)";
		return (Attachment) super.addForObject(dsBmsUpd, sql, attachment, 
				new Object[]{ 
						attachment.getFormId(), attachment.getFormType(), attachment.getUrl(), attachment.getDescription(),
						attachment.getCreator() });
	}

	@Override
	public int deleteAll(Long formid, String formType) {
		String sql = "DELETE FROM `attachment` WHERE (`form_id`=? and `form_type`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{formid, formType});
	}

	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM attachment WHERE id=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Attachment> findFormAttachment(Long formid, String formType) {
		String sql = "SELECT * FROM attachment am WHERE `form_id`=? and `form_type`=?";
		return this.queryForList(dsBmsQry, sql, new Object[]{formid, formType}, Attachment.class);
	}
}