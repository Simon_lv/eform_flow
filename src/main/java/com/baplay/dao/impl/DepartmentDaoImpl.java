package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import com.baplay.dao.IDepartmentDao;
import com.baplay.dao.mapper.DepartmentTop;
import com.baplay.entity.Department;
import com.baplay.form.DepartmentForm;

@Repository
public class DepartmentDaoImpl extends BaseDao<Department> implements IDepartmentDao {
	
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public Department add(Department department) {
		String sql = "INSERT INTO `department` (`dept_no`, `name`, `dept_level`, `parent_id`, `company_id`, `creator`, `create_time`, `status`) VALUES (?, ?, ?, ?, ?, ?, NOW(), 1)";
		return (Department) super.addForObject(dsBmsUpd, sql, department, 
				new Object[]{department.getDeptNo(), department.getName(), department.getDeptLevel(), department.getParentId(), department.getCompanyId(), department.getCreator()});
	}

	@Override
	public int update(Department department) {
		String sql = "UPDATE `department` SET `dept_no`=?, `name`=?, `dept_level`=?, `parent_id`=?, `modifier`=?, `update_time`=NOW() WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, 
				new Object[]{department.getDeptNo(), department.getName(), department.getDeptLevel(), department.getParentId(), department.getModifier(), department.getId()});
	}

	@Override
	public Department findOneById(Long id) {
		String sql = "SELECT a.*, (select name from company where id = a.company_id) as companyName FROM department as a WHERE a.id=?";
		Object[] args = new Object[]{id};
		return (Department) super.queryForObject(dsBmsQry, sql, args, Department.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Page<Department> list(DepartmentForm departmentForm) {
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		
		if(StringUtils.isNotBlank(departmentForm.getName())) {
			sb.append(" AND name LIKE ? ");
			param.add("%" + departmentForm.getName() + "%");
		}
		
		if(StringUtils.isNotBlank(departmentForm.getDeptNo())) {
			sb.append(" AND dept_no = ? ");
			param.add(departmentForm.getDeptNo());
		}
			
		if(departmentForm.getCompanyId() != null) {
			sb.append(" AND company_id = ? ");
			param.add(departmentForm.getCompanyId());
		}		
				
		sb.append(" AND status = 1 ");
		
		StringBuilder sql = new StringBuilder("SELECT d.*, (select name from company where id=d.company_id) companyName, (select login_account from t_erp_user where id = d.creator) as creatorName, (select login_account from t_erp_user where id = d.modifier) as modifierName, (select name from department where id = d.parent_id) as parentName FROM department d WHERE 1=1 ").append(sb.toString());
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM department WHERE 1=1 ").append(sb.toString());
		sql.append(" ORDER BY dept_no ");
		
		if(!departmentForm.isExport()) {
			sql.append(" LIMIT ").append((departmentForm.getPageNo()-1)*departmentForm.getPageSize()).append(",").append(departmentForm.getPageSize());
		}
		
		Page<Department> page = new Page<Department>(departmentForm.getPageSize());
		page = (Page<Department>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(), 
				new BeanPropertyRowMapper<Department>(Department.class));
		page.setPageNo(departmentForm.getPageNo());
		return page;
	}

	@Override
	public int delete(Long id, String modifier) {
		String sql = "UPDATE `department` SET `modifier`=?, `update_time`=NOW(), `status`=2 WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{modifier, id});
	}

	@Override
	public int countLiveChild(Long id) {
		String sql = "SELECT COUNT(*) FROM `department` WHERE `status`=1 AND parent_id = ?  ORDER BY dept_no";
		return super.queryForInt(dsBmsQry, sql, new Object[]{id});
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Department> findAllByLevel(int level) {
		String sql = "SELECT * FROM department WHERE `status`=1 AND `dept_level`=? ORDER BY dept_no";
		return super.queryForList(dsBmsQry, sql, new Object[]{ level }, Department.class);
	}

	@Override
	public boolean checkDeptNoExist(Long companyId, Long id, String deptNo) {
		id = id == null? -1:id;
		String sql = "SELECT COUNT(*) FROM `department` WHERE `company_id`=? AND `dept_no`=? AND id <> ? AND `status`=1";
		return super.queryForInt(dsBmsQry, sql, new Object[]{companyId, deptNo, id}) > 0;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Department> findAllDepartment() {
		String sql = "SELECT a.*, (select name from company where id = a.company_id) as companyName FROM department as a WHERE a.`status`=1 ORDER BY a.company_id, a.dept_no";
		return super.queryForList(dsBmsQry, sql, new Object[]{}, Department.class);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Department> findAllDepartment(Long companyId) {
		String sql = "SELECT a.*, (select name from company where id = a.company_id) as companyName FROM department as a WHERE a.company_id=? AND a.`status`=1 ORDER BY a.company_id, a.dept_no";
		return super.queryForList(dsBmsQry, sql, new Object[]{companyId}, Department.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<DepartmentTop> findAllDepartmentTop() {
		StringBuilder sql = new StringBuilder();
		sql.append("select dh1.id, dh1.dept_no, dh1.`name`, dh1.dept_level, ");
		sql.append("IFNULL(coalesce(dh3.parent_id, dh2.parent_id, dh1.parent_id), dh1.id) as top_id,");
		sql.append("(select `name` from department where id=(IFNULL(coalesce(dh3.parent_id, dh2.parent_id, dh1.parent_id), dh1.id))) top_name, ");
		sql.append("(select `dept_level` from department where id=(IFNULL(coalesce(dh3.parent_id, dh2.parent_id, dh1.parent_id), dh1.id))) top_level, ");
		sql.append("(select `dept_no` from department where id=(IFNULL(coalesce(dh3.parent_id, dh2.parent_id, dh1.parent_id), dh1.id))) top_no ");
		sql.append("from department dh1 left outer join ");
		sql.append("department dh2 ");
		sql.append("on dh1.parent_id = dh2.id left outer join ");
		sql.append("department dh3 ");
		sql.append("on dh2.parent_id = dh3.id "); 
		sql.append("where dh1.`status`=1 ");
		sql.append("order by top_level, top_no, dept_level;");
		
		return queryForList(dsBmsQry, sql.toString(), new Object[]{}, new BeanPropertyRowMapper<DepartmentTop>(DepartmentTop.class));
	}
	
	@Override
	public List<Map<String, Object>> deptList(long companyId) {
		String sql = "SELECT d.`id`, d.`name` FROM `department` d WHERE d.`company_id`=? and `status`=1";
		return this.queryForListMap(dsBmsQry, sql, new Object[] { companyId });
	}

	@Override
	public Department findOneByUserId(Long id) {
		String sql = "SELECT a.*, (select name from company where id = a.company_id) as companyName FROM department as a WHERE a.id=(select department_id from employee where id = (select employee_id from t_erp_user where id = ?))";
		Object[] args = new Object[]{id};
		return (Department) super.queryForObject(dsBmsQry, sql, args, Department.class);		
	}
	
	@Override
	public Department findOneByFullName(String fullName, long companyId) {
		String sql = "SELECT a.*, (select name from company where id = a.company_id) as companyName FROM department as a WHERE a.name=? and a.company_id=? and `status`=1";
		Object[] args = new Object[]{fullName, companyId};
		return (Department) super.queryForObject(dsBmsQry, sql, args, Department.class);
	}
	
	@Override
	public Department findOneByDeptNo(String deptNo, long companyId) {
		String sql = "SELECT a.*, (select name from company where id = a.company_id) as companyName FROM department as a WHERE a.dept_no=? and a.company_id=? and `status`=1";
		Object[] args = new Object[]{deptNo, companyId};
		return (Department) super.queryForObject(dsBmsQry, sql, args, Department.class);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Department> findDepartmentCN() {
		String sql = "SELECT * FROM department WHERE id IN " +
					 "(SELECT department_id FROM employee WHERE " + 
					 "(quit_time IS NULL OR DATE_FORMAT(quit_time,'%Y%m')>=DATE_FORMAT(NOW(), '%Y%m')) " +
					 "AND DATE_FORMAT(onboard_time,'%Y%m')<=DATE_FORMAT(NOW(), '%Y%m') " +  
					 "AND status=1 AND employee_no LIKE 'C%' GROUP BY department_id)";
		return super.queryForList(dsBmsQry, sql, new Object[]{}, Department.class);
	}
}