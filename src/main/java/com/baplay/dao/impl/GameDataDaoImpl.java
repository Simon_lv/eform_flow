package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import com.baplay.dao.IGameDataDao;
import com.baplay.entity.GameData;
import com.baplay.form.GameDataForm;

@Repository
public class GameDataDaoImpl extends BaseDao<GameData> implements IGameDataDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public GameData findOneById(Long id) {
		String sql = "SELECT * FROM `game_data` WHERE id=?";
		Object[] args = new Object[]{id};
		
		GameData entity = (GameData) super.queryForObject(dsBmsQry, sql, args, GameData.class);
		return entity;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<GameData> findAllGameData() {
		String sql = "SELECT g.*, (select `name` from company where id=g.company_id)company_name FROM game_data g WHERE `status`=1 ORDER BY CAST(CONVERT(`name` using big5) AS BINARY);";
		return super.queryForList(dsBmsQry, sql, new Object[]{}, GameData.class);
	}

	@Override
	public GameData add(GameData gameData) {
		String sql = "INSERT INTO `game_data` (`name`, `company_id`, `online`, `status`) VALUES (?, ?)";
		return (GameData) super.addForObject(dsBmsUpd, sql, gameData, 
				new Object[]{ gameData.getName(), gameData.getStatus() });
	}

	@Override
	public int update(GameData gameData) {
		String sql = "UPDATE `game_data` SET `name`=?, `company_id`=?, `online`=?, `status`=? WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, 
				new Object[]{ gameData.getName(), gameData.getStatus(), gameData.getId() });
	}

	@Override
	public int delete(Long id) {
		String sql = "UPDATE `game_data` SET `status`=2 WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, 
				new Object[]{ id });
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<GameData> list(GameDataForm gameDataForm) {
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		
		if(StringUtils.isNotBlank(gameDataForm.getName())){
			sb.append(" and name like ?");
			param.add("%"+gameDataForm.getName()+"%");
		}
		
		StringBuilder sql = new StringBuilder("SELECT * FROM game_data WHERE 1=1").append(sb.toString());
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM game_data WHERE 1=1").append(sb.toString());		
		
		if(!gameDataForm.isExport()) {
			sql.append(" LIMIT ").append((gameDataForm.getPageNo()-1)*gameDataForm.getPageSize()).append(",").append(gameDataForm.getPageSize());
		}
		
		Page<GameData> page = new Page<GameData>(gameDataForm.getPageSize());
		page = (Page<GameData>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(), 
				new BeanPropertyRowMapper<GameData>(GameData.class));
		page.setPageNo(gameDataForm.getPageNo());
		return page;
	}
}