package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import com.baplay.dao.IJobTitleDao;
import com.baplay.entity.JobTitle;
import com.baplay.form.JobTitleForm;

@Repository
public class JobTitleDaoImpl extends BaseDao<JobTitle> implements IJobTitleDao {
	
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public JobTitle add(JobTitle jobTitle) {
		String sql = "INSERT INTO `job_title` (`title_no`, `name`, `level`, `grade`, `company_id`, `creator`, `create_time`, `status`) VALUES (?, ?, ?, ?, ?, ?, NOW(), 1)";
		return (JobTitle) super.addForObject(dsBmsUpd, sql, jobTitle, 
				new Object[]{jobTitle.getTitleNo(), jobTitle.getName(), jobTitle.getLevel(), jobTitle.getGrade(), jobTitle.getCompanyId(), jobTitle.getCreator()});
	}

	@Override
	public int update(JobTitle jobTitle) {
		String sql = "UPDATE `job_title` SET `title_no`=?, `name`=?, `level`=?, `grade`=?, `company_id`=?, `modifier`=?, `update_time`=NOW() WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, 
				new Object[]{jobTitle.getTitleNo(), jobTitle.getName(), jobTitle.getLevel(), jobTitle.getGrade(), jobTitle.getCompanyId(), jobTitle.getModifier(), jobTitle.getId()});
	}

	@Override
	public JobTitle findOneById(Long id) {
		String sql = "SELECT * FROM job_title WHERE id=?";
		Object[] args = new Object[]{id};
		return (JobTitle) super.queryForObject(dsBmsQry, sql, args, JobTitle.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Page<JobTitle> list(JobTitleForm jobTitleForm) {
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		
		//title_no name level
		if(StringUtils.isNotBlank(jobTitleForm.getName())) {
			sb.append(" AND name LIKE ? ");
			param.add("%"+jobTitleForm.getName()+"%");
		}
		
		if(StringUtils.isNotBlank(jobTitleForm.getTitleNo())) {
			sb.append(" AND title_no = ? ");
			param.add(jobTitleForm.getTitleNo());
		}
		
		if(jobTitleForm.getLevel() != null) {
			sb.append(" AND level = ? ");
			param.add(jobTitleForm.getLevel());
		}
		
		if(jobTitleForm.getGrade() != null) {
			sb.append(" AND grade = ? ");
			param.add(jobTitleForm.getGrade());
		}
		
		if(jobTitleForm.getModifier() != null) {
			sb.append(" AND modifier = ? ");
			param.add(jobTitleForm.getModifier());
		}
		
		if(jobTitleForm.getCreator() != null) {
			sb.append(" AND creator = ? ");
			param.add(jobTitleForm.getCreator());
		}
		
		if(jobTitleForm.getCompanyId() != null) {
			sb.append(" AND company_id = ? ");
			param.add(jobTitleForm.getCompanyId());
		}
		
		sb.append(" AND status = 1 ");		
		
		StringBuilder sql = new StringBuilder("SELECT a.*,(select login_account from t_erp_user where id = a.creator) as creatorName, (select login_account from t_erp_user where id = a.modifier) as modifierName, (select name from company where id=a.company_id) companyName FROM job_title as a WHERE 1=1 ").append(sb.toString());
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM job_title WHERE 1=1 ").append(sb.toString());
		sql.append(" ORDER BY title_no ");
		
		if(!jobTitleForm.isExport()) {
			sql.append(" LIMIT ").append((jobTitleForm.getPageNo()-1)*jobTitleForm.getPageSize()).append(",").append(jobTitleForm.getPageSize());
		}
		
		Page<JobTitle> page = new Page<JobTitle>(jobTitleForm.getPageSize());
		page = (Page<JobTitle>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(), 
				new BeanPropertyRowMapper<JobTitle>(JobTitle.class));
		page.setPageNo(jobTitleForm.getPageNo());
		return page;
	}

	@Override
	public int delete(Long id, Long modifier) {
		String sql = "UPDATE `job_title` SET `modifier`=?, `update_time`=NOW(), `status`=2 WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{modifier, id});
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<JobTitle> findAllJobTitle() {
		String sql = "SELECT * FROM job_title WHERE `status`=1 order by company_id, title_no";
		return super.queryForList(dsBmsQry, sql, new Object[]{}, JobTitle.class);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<JobTitle> findAllJobTitle(Long companyId) {
		String sql = "SELECT * FROM job_title WHERE company_id=? AND `status`=1 order by title_no";
		return super.queryForList(dsBmsQry, sql, new Object[]{companyId}, JobTitle.class);
	}

	@Override
	public boolean checkSerialNumberExist(Long id, String titleNo) {
		id = id == null? -1:id;
		String sql = "SELECT COUNT(*) FROM job_title WHERE title_no = ? AND id <> ? AND `status`=1";
		return super.queryForInt(dsBmsQry, sql, new Object[]{titleNo, id}) > 0;
	}
	
	@Override
	public List<Map<String, Object>> jobList(long companyId) {
		String sql = "SELECT j.`id`, j.`name` FROM `job_title` j WHERE j.`company_id`=? and `status`=1";
		return this.queryForListMap(dsBmsQry, sql, new Object[] { companyId });
	}
	
	@Override
	public JobTitle findOneByNo(String titleNo, long companyId) {
		String sql = "SELECT * FROM job_title WHERE title_no=? AND company_id=? AND `status`=1";
		Object[] args = new Object[]{titleNo, companyId};
		return (JobTitle) super.queryForObject(dsBmsQry, sql, args, JobTitle.class);
	}
}