package com.baplay.dao.impl;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import com.baplay.dao.IBusinessCardFormDao;
import com.baplay.entity.BusinessCardForm;
import com.baplay.form.BusinessCardFormForm;

@Repository
public class BusinessCardFormDaoImpl extends BaseDao<BusinessCardForm> implements IBusinessCardFormDao {
	
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public BusinessCardForm add(BusinessCardForm businessCardForm) {
		String sql = "INSERT INTO `business_card_form` (`company_id`, `request_department`, `request_date`, `serial_no`, `contact`, `contact_tel`, `total_amount`, `work_flow_id`) VALUES (?, ?, NOW(), ?, ?, ?, ?, ?)";
		return (BusinessCardForm) super.addForObject(dsBmsUpd, sql, businessCardForm, 
				new Object[]{ businessCardForm.getCompanyId(), businessCardForm.getRequestDepartment(), businessCardForm.getSerialNo(),  
						businessCardForm.getContact(), businessCardForm.getContactTel(), businessCardForm.getTotalAmount(),
						businessCardForm.getWorkFlowId() });
	}

	@Override
	public int update(BusinessCardForm businessCardForm) {
		String sql = "UPDATE `business_card_form` SET `company_id`=?, `request_department`=?, `serial_no`=?, `contact`=?, `contact_tel`=?, `total_amount`=? WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, 
				new Object[]{ businessCardForm.getCompanyId(), businessCardForm.getRequestDepartment(),
					businessCardForm.getSerialNo(), businessCardForm.getContact(), businessCardForm.getContactTel(), businessCardForm.getTotalAmount(),
						businessCardForm.getId() });
	}

	@Override
	public BusinessCardForm findOneById(Long id) {
		String sql = "SELECT a.*, (select name from company where id = a.company_id) as companyName, (select name from department where id = a.request_department) as departmentName, (select name from employee where id = (select employee_id from t_erp_user where id = (select creator from work_flow where id = a.work_flow_id))) as creatorName FROM business_card_form as a WHERE a.id=?";
		Object[] args = new Object[]{id};
		return (BusinessCardForm) super.queryForObject(dsBmsQry, sql, args, BusinessCardForm.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Page<BusinessCardForm> list(BusinessCardFormForm businessCardFormForm) {
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		
		// Auth
		//param.add(StringUtils.join(businessCardFormForm.getEmployeeIdList(), ","));
		param.add(businessCardFormForm.getLoginUser());
		param.add(businessCardFormForm.getFormType());
		param.add(businessCardFormForm.getLoginUser());
		param.add(businessCardFormForm.getLoginUser());
		
		if(businessCardFormForm.getCompanyId() != null){
			sb.append(" AND b.company_id = ? ");
			param.add(businessCardFormForm.getCompanyId());
		}
		if(businessCardFormForm.getRequestDepartment() != null){
			sb.append(" AND b.request_department = ? ");
			param.add(businessCardFormForm.getRequestDepartment());
		}
		if(StringUtils.isNotBlank(businessCardFormForm.getSerialNo())){
			sb.append(" AND b.serial_no = ? ");
			param.add(businessCardFormForm.getSerialNo());
		}
		if(StringUtils.isNotBlank(businessCardFormForm.getRequestDateStart())){		
			sb.append(" AND STR_TO_DATE(b.request_date, '%Y-%m-%d') >= ? ");
			param.add(businessCardFormForm.getRequestDateStart());
		}
		if(StringUtils.isNotBlank(businessCardFormForm.getRequestDateEnd())){
			sb.append(" AND STR_TO_DATE(b.request_date, '%Y-%m-%d') <= ? ");
			param.add(businessCardFormForm.getRequestDateEnd());
		}		
		if(businessCardFormForm.getStatus() != null){
			sb.append(" AND b.status = ? ");
			param.add(businessCardFormForm.getStatus());
		}
		
		// Auth
		String userFormRole = StringUtils.join(businessCardFormForm.getFormRole(), "','");
		
		StringBuilder sql = new StringBuilder("SELECT b.* FROM (SELECT a.*, (SELECT COUNT(*) FROM form_auth WHERE form_type='BC' and form_role IN (SELECT id FROM form_role WHERE `name` IN('" + userFormRole + "'))) as auth, (SELECT status FROM work_flow WHERE id = a.work_flow_id) AS status, (select name from company where id = a.company_id) as companyName, (select name from department where id = a.request_department) as departmentName, (select creator from work_flow where id = a.work_flow_id) as creator, (SELECT `name` FROM employee WHERE id=(select employee_id from t_erp_user where id=(select creator from work_flow where id = a.work_flow_id))) as creatorName FROM business_card_form a) as b ");
		sql.append("left join (select work_flow_id, owner_id from work_flow_detail where owner_id=? and work_flow_id in (select id from work_flow where LEFT(form_type, 2)=?) group by work_flow_id) wfd on wfd.work_flow_id=b.work_flow_id ").append("WHERE (auth>=1 OR creator=? OR owner_id=?)").append(sb.toString());
		StringBuilder countSql = new StringBuilder("select count(*) from (").append(sql).append(") x");
				
		if(!businessCardFormForm.isExport()) {
			sql.append(" LIMIT ").append((businessCardFormForm.getPageNo()-1)*businessCardFormForm.getPageSize()).append(",").append(businessCardFormForm.getPageSize());
		}
		
		Page<BusinessCardForm> page = new Page<BusinessCardForm>(businessCardFormForm.getPageSize());

		page = (Page<BusinessCardForm>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(), 
				new BeanPropertyRowMapper<BusinessCardForm>(BusinessCardForm.class));
		
		if(!businessCardFormForm.isExport()) {
			page.setPageNo(businessCardFormForm.getPageNo());
		}
		
		return page;
	}

	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM `business_card_form` WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}

	@Override
	public BusinessCardForm findOneByFlow(Long workFlowId) {
		String sql = "SELECT a.*, (select name from company where id = a.company_id) as companyName, (select name from department where id = a.request_department) as departmentName, (select name from employee where id = (select employee_id from t_erp_user where id = (select creator from work_flow where id = a.work_flow_id))) as creatorName FROM business_card_form as a WHERE a.work_flow_id=?";
		Object[] args = new Object[]{workFlowId};
		return (BusinessCardForm) super.queryForObject(dsBmsQry, sql, args, BusinessCardForm.class);		
	}

	@Override
	public int updateTotal(Long id, Long editId, String formType) {
		String sql1 = "SELECT SUM(`amount`) FROM `receipt` WHERE `form_id`=? AND `id`!=? and formType=?";
		int sum = super.queryForInt(dsBmsQry, sql1, new Object[]{id, editId, formType});
		String sql2 = "UPDATE business_card_form SET total_amount=? WHERE id=?";		
		return super.updateForObject(dsBmsUpd, sql2, new Object[]{sum, id});
	}	
}