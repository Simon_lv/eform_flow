package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import com.baplay.dao.IQcFormSchemaDao;
import com.baplay.entity.QcFormSchema;
import com.baplay.form.QcFormMgrForm;

@Repository
public class QcFormSchemaDaoImpl extends BaseDao<QcFormSchema> implements IQcFormSchemaDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<QcFormSchema> findAllFormSchema() {
		String sql = "SELECT a.* FROM qc_form_schema a";
		//WHERE a.version = (select max(version) from qc_form_schema where form_name = a.form_name)
		return this.queryForList(dsBmsQry, sql, new Object[]{}, QcFormSchema.class);
	}

	@Override
	public QcFormSchema findOneById(Long id) {
		String sql = "select * from qc_form_schema where id = ?";
		return this.queryForObject(dsBmsQry, sql, new Object[]{id}, QcFormSchema.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<QcFormSchema> list(QcFormMgrForm qcFormMgrForm) {
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		
		if(qcFormMgrForm.getId() != null){
			sb.append(" and a.id=? ");
			param.add(qcFormMgrForm.getId());
		}
		
		StringBuilder sql = new StringBuilder("SELECT a.*, (select login_account from t_erp_user where id = a.creator) as creatorName from qc_form_schema as a where 1=1").append(sb.toString());
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) from qc_form_schema as a where 1=1").append(sb.toString());		
		
		if(!qcFormMgrForm.isExport()) {
			sql.append(" LIMIT ").append((qcFormMgrForm.getPageNo()-1)*qcFormMgrForm.getPageSize()).append(",").append(qcFormMgrForm.getPageSize());
		}
		
		Page<QcFormSchema> page = new Page<QcFormSchema>(qcFormMgrForm.getPageSize());
		page = (Page<QcFormSchema>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(),
				new BeanPropertyRowMapper<QcFormSchema>(QcFormSchema.class));
		page.setPageNo(qcFormMgrForm.getPageNo());
		return page;
	}

	@Override
	public QcFormSchema add(QcFormSchema qcFormSchema) {
		String sql = "INSERT INTO `qc_form_schema` (`form_name`, `form_code`, `flow_type`, `creator`, `create_time`, `version`) VALUES (?, ?, ?, ?, NOW(), ?)";
		return (QcFormSchema) super.addForObject(dsBmsUpd, sql, qcFormSchema, 
				new Object[]{ 
						qcFormSchema.getFormName(), qcFormSchema.getFormCode(), qcFormSchema.getFlowType(),
						qcFormSchema.getCreator(), qcFormSchema.getVersion(),
				});
	}

	@Override
	public int update(QcFormSchema qcFormSchema) {
		String sql = "UPDATE `qc_form_schema` SET `form_name`=?, `form_code`=?, `flow_type`=? WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, 
				new Object[]{ 
						qcFormSchema.getFormName(), qcFormSchema.getFormCode(),qcFormSchema.getFlowType(),
						qcFormSchema.getId()
				});
	}

	@Override
	public QcFormSchema findOneByCode(String code) {
		String sql = "select * from qc_form_schema where form_code = ?";
		return this.queryForObject(dsBmsQry, sql, new Object[]{code}, QcFormSchema.class);
	}
}