package com.baplay.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import com.baplay.constant.Flow;
import com.baplay.dao.IStampDao;
import com.baplay.dto.Stamp;

@Repository
public class StampDaoImpl extends BaseDao<Stamp> implements IStampDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IStampDao#findSupervisor(long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<Stamp> findSupervisor(long workFlowId) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT e.`name` owner_name, d.process_time, d.status, d.owner_id, d.countersign_role, d.step_id FROM work_flow_detail d ");
		sql.append("LEFT JOIN t_erp_user u ON u.id=d.owner_id ");
		sql.append("LEFT JOIN employee e on e.id=u.employee_id ");
		sql.append("WHERE stamp_status=1 AND work_flow_id=? AND step_id!=-1 AND (d.status=? OR d.status=?) ORDER BY process_time"); 
		
		return super.queryForList(dsBmsQry, sql.toString(), 
				new Object[]{workFlowId, Flow.NORMAL_CLOSED.getValue(), Flow.REJECT.getValue()}, 
				Stamp.class);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IStampDao#findCreator(long)
	 */
	@Override
	public Stamp findCreator(long workFlowId) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT e.`name` owner_name, d.create_time process_time FROM work_flow_detail d ");
		sql.append("LEFT JOIN t_erp_user u ON u.id=d.creator ");
		sql.append("LEFT JOIN employee e on e.id=u.employee_id ");
		sql.append("WHERE work_flow_id=? AND IF(d.step_id = -1,1=1,d.step_id!=-1) ");
		sql.append("ORDER BY process_time LIMIT 1"); 
		
		return super.queryForObject(dsBmsQry, sql.toString(), 
				new Object[]{workFlowId}, 
				Stamp.class);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IStampDao#cancel(long)
	 */
	@Override
	public int cancel(long workFlowId) {
		String sql = "UPDATE `work_flow_detail` SET `stamp_status`=0 WHERE `work_flow_id`=? AND `stamp_status`=1";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{workFlowId});
	}
}