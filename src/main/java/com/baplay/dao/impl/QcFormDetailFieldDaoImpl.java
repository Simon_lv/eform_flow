package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.baplay.dao.IQcFormDetailFieldDao;
import com.baplay.entity.QcFormDetailField;

@Repository
public class QcFormDetailFieldDaoImpl extends BaseDao<QcFormDetailField> implements IQcFormDetailFieldDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public int add(HashMap<String, Object> map) {		
		String sql = "INSERT INTO qc_form_detail_field (id, schema_id, schema_version, field_order, group_label, field_label) VALUES (?, ?, ?, ?, ?, ?);";
		return this.updateForObject(dsBmsUpd, sql, new Object[]{map.get("fieldOrder"), map.get("schemaId"), 
				map.get("version"), map.get("fieldOrder"), map.get("group"), map.get("field")});		
				
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<QcFormDetailField> findAll(Long schemaId, Long formId) {		
		String sql = "SELECT a.*, (select qc_result from qc_form where field_id = a.id and form_id = ? and qc_result IS NOT NULL) as qcResult, (select pm_result from qc_form where field_id = a.id and form_id = ? and pm_result IS NOT NULL) as pmResult, (select remark from qc_form where field_id = a.id and form_id = ? and remark IS NOT NULL) as remark FROM qc_form_detail_field as a WHERE a.schema_id = ? ORDER BY COALESCE(a.field_order,999)";
		return super.queryForList(dsBmsQry, sql, new Object[]{formId, formId, formId, schemaId}, QcFormDetailField.class);		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<QcFormDetailField> findAll(Long schemaId) {		
		String sql = "SELECT a.*, '' as qcResult, '' as pmResult, '' as remark FROM qc_form_detail_field as a WHERE a.schema_id = ? ORDER BY COALESCE(a.field_order,999)";
		return super.queryForList(dsBmsQry, sql, new Object[]{schemaId}, QcFormDetailField.class);			
	}

	@Override
	public int findGroupCount(Long id) {
		String sql1 = "SELECT COUNT(*) FROM qc_form_detail_field WHERE schema_id = ? AND group_label != ''";
		int count = this.queryForInt(dsBmsQry, sql1, new Object[]{id});
		
		if(count > 0){
			String sql2 = "SELECT COUNT(DISTINCT group_label) as groupCount FROM qc_form_detail_field WHERE schema_id = ?";
			return this.queryForInt(dsBmsQry, sql2, new Object[]{id});
		}else{
			String sql3 = "SELECT COUNT(id) as groupCount FROM qc_form_detail_field WHERE schema_id = ?";
			return this.queryForInt(dsBmsQry, sql3, new Object[]{id});
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> findAllFieldCount(Long id) {
		String sql1 = "SELECT COUNT(*) FROM qc_form_detail_field WHERE schema_id = ? AND group_label != ''";
		int count = this.queryForInt(dsBmsQry, sql1, new Object[]{id});
		
		if(count > 0){
			String sql2 = "SELECT COUNT(group_label) as fieldCount FROM qc_form_detail_field WHERE schema_id = ? GROUP BY group_label ORDER BY COALESCE(field_order,999)";
			return this.queryForObjectList(dsBmsQry, sql2, new Object[]{id}, Integer.class);
		}else{
			String sql3 = "SELECT COUNT(field_label) as fieldCount FROM qc_form_detail_field WHERE schema_id = ? GROUP BY id ORDER BY COALESCE(field_order,999)";
			return this.queryForObjectList(dsBmsQry, sql3, new Object[]{id}, Integer.class);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<QcFormDetailField> findAllGroupLabel(Long id) {
		String sql1 = "SELECT COUNT(*) FROM qc_form_detail_field WHERE schema_id = ? AND group_label != ''";
		int count = this.queryForInt(dsBmsQry, sql1, new Object[]{id});
		
		if(count > 0){
			String sql2 = "SELECT * FROM qc_form_detail_field WHERE schema_id=? GROUP BY group_label ORDER BY COALESCE(field_order, 999)";
			return this.queryForList(dsBmsQry, sql2, new Object[]{id},  QcFormDetailField.class);
		}else{
			return new ArrayList<QcFormDetailField>();
		}
		
		
	}

	@Override
	public QcFormDetailField add(QcFormDetailField qcFormDetailField) {		
		String sql = "INSERT INTO qc_form_detail_field (schema_id, schema_version, field_order, group_label, field_label) VALUES (?, ?, ?, ?, ?);";
		return (QcFormDetailField) super.addForObject(dsBmsUpd, sql, qcFormDetailField, 
				new Object[]{ 
						qcFormDetailField.getSchemaId(), qcFormDetailField.getSchemaVersion(), 
						qcFormDetailField.getFieldOrder(), qcFormDetailField.getGroupLabel(),
						qcFormDetailField.getFieldLabel()
				});
	}

	@Override
	public int update(QcFormDetailField qcFormDetailField) {		
		String sql = "UPDATE `qc_form_detail_field` SET `field_order`=?, `group_label`=?, `field_label`=? WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, 
				new Object[]{ 
						qcFormDetailField.getFieldOrder(), qcFormDetailField.getGroupLabel(), qcFormDetailField.getFieldLabel(),
						qcFormDetailField.getId()
				});
	}

	@Override
	public int updateGroup(QcFormDetailField qcFormDetailField) {
		String sql1 = "SELECT `id` FROM `qc_form_detail_field` WHERE `schema_id`=? and `group_label`=(select `group_label` from `qc_form_detail_field` where `id` = ?)";
		List<Long> ids = this.queryForLongList(dsBmsQry, sql1, new Object[]{
				qcFormDetailField.getSchemaId(), qcFormDetailField.getId()
			});
		StringBuffer sql2 = new StringBuffer("UPDATE `qc_form_detail_field` SET `group_label`=? WHERE id in ");
		sql2.append(String.format("(%s)", StringUtils.join(ids, ",")));
		return super.updateForObject(dsBmsUpd, sql2.toString(), new Object[]{qcFormDetailField.getGroupLabel()});
	}
	
	@Override
	public int deleteGroup(Long id) {
		String sql1 = "SELECT `id` FROM `qc_form_detail_field` WHERE `schema_id` = (select `schema_id` from `qc_form_detail_field` where `id` = ?) AND `group_label` = (select `group_label` from `qc_form_detail_field` where `id` = ?)";
		List<Long> ids = super.queryForLongList(dsBmsQry, sql1, new Object[]{id, id});
		
		StringBuffer sql2 = new StringBuffer("DELETE FROM `qc_form_detail_field` WHERE `id` IN ");
		sql2.append(String.format("(%s)", StringUtils.join(ids, ",")));
		return super.updateForObject(dsBmsUpd, sql2.toString(), new Object[]{});
	}

	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM `qc_form_detail_field` WHERE `id`=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}

}
