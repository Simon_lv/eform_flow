package com.baplay.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.baplay.dao.IFormUserDao;
import com.baplay.dao.mapper.FormUser;

@Repository
public class FormUserDaoImpl extends BaseDao<FormUser> implements IFormUserDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<FormUser> findIssueRepairUser(Integer... roleId) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT u.id uid, e.name, r.name role FROM form_user_role f ");
		sql.append("JOIN t_erp_user u ON u.id=f.user_id "); 
		sql.append("JOIN employee e ON e.id=u.employee_id "); 
		sql.append("JOIN form_role r ON r.id=f.role_id "); 
		sql.append("WHERE f.role_id IN (" + StringUtils.join(roleId, ",") + ") AND f.status=1;");		
		System.out.println("sql=" + sql);
		return queryForList(dsBmsQry, sql.toString(), new Object[]{}, FormUser.class);
	}
}