package com.baplay.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import com.baplay.dao.IQcFormTitleFieldDao;
import com.baplay.entity.QcFormTitleField;

@Repository
public class QcFormTitleFieldDaoImpl extends BaseDao<QcFormTitleField> implements IQcFormTitleFieldDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<QcFormTitleField> findAll(Long id, Long... otherId) {		
		if(otherId.length == 0 || otherId[0] == null) {
			String sql = "SELECT a.* , '' as subTitle FROM qc_form_title_field a WHERE a.schema_id=? ORDER BY COALESCE(a.field_order,999)";
			return this.queryForList(dsBmsQry, sql, new Object[]{id}, QcFormTitleField.class);
		}
		else {
			long formId = otherId[0];
			String sql = "SELECT a.* , (select sub_title from qc_form where title_id = a.id and form_id=?) as subTitle FROM qc_form_title_field a WHERE a.schema_id=? ORDER BY COALESCE(a.field_order,999)";
			return this.queryForList(dsBmsQry, sql, new Object[]{formId, id}, QcFormTitleField.class);
		}
	}

	@Override
	public QcFormTitleField add(QcFormTitleField qcFormTitleField) {
		String sql = "INSERT INTO `qc_form_title_field` (`schema_id`, `schema_version`, `field_order`, `field_label`) VALUES (?, ?, ?, ?);";
		return (QcFormTitleField)this.addForObject(dsBmsUpd, sql, qcFormTitleField, new Object[]{
				qcFormTitleField.getSchemaId(), qcFormTitleField.getSchemaVersion(), 
				qcFormTitleField.getFieldOrder(), qcFormTitleField.getFieldLabel()
		});
	}

	@Override
	public int update(QcFormTitleField qcFormTitleField) {
		String sql = "UPDATE `qc_form_title_field` SET `field_order`=?, `field_label`=? WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{
				qcFormTitleField.getFieldOrder(), qcFormTitleField.getFieldLabel(),
				qcFormTitleField.getId()
		});
	}

	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM `qc_form_title_field` WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}

}
