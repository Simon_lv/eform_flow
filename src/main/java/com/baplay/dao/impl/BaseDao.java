package com.baplay.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springside.modules.orm.Page;

import com.baplay.dto.BaseDto;

public class BaseDao<T> extends JdbcDaoSupport {
	private static final Logger LOG = LoggerFactory.getLogger(BaseDao.class);

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	protected String fetchCombineSqlString(final String sql, final Object[] args) {
		if (args == null || args.length == 0)
			return sql;

		String result = null;
		Object[] params = new Object[args.length];
		for (int i = 0; i < args.length; i++) {
			Object obj = args[i];
			if (obj instanceof Integer || obj instanceof Long || obj instanceof Float || obj instanceof Double) {
				params[i] = obj;
			} else {
				if (obj instanceof Date || obj instanceof Timestamp) {
					params[i] = "'" + sdf.format(obj) + "'";
				} else {
					params[i] = "'" + obj + "'";
				}
			}
		}

		try {
			result = String.format(sql.replace("%", "%%").replace("?", "%s"), params);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			result = sql;
		}

		return result;
	}
	
	protected Object queryForObject(final DataSource dataSource, final StringBuilder sql, final ArrayList<Object> args, final RowMapper rowMapper) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		Object obj = null;
		ArrayList<Integer> removeParam = new ArrayList<Integer>();
		try {
			int paramCount = 0;
			int sqlCount = 0;		
			int sqlLength = sql.length();			
					
			while(sqlCount < sqlLength){
				int sbStart = sql.indexOf("[");
				int sbEnd = sql.indexOf("]");
				int paramPlace = sql.indexOf("?", sqlCount);
				
				if(paramPlace == -1){					
					break;
				}
				
				if(sbStart == -1 || sbEnd == -1){
					break;
				}
					
				if(paramPlace > sbStart && paramPlace < sbEnd){
					if(args.get(paramCount) == null){
						sql.replace(sbStart, sbEnd+1, "");
						removeParam.add(paramCount);
						
						sqlCount = sbStart+1;
						sqlLength = sql.length();
					}else{						
						sql.replace(sbStart, sbStart+1, "");
						sql.replace(sbEnd-1, sbEnd, "");						
						
						sqlCount = sbEnd+1;
						sqlLength = sql.length();
					}
				}else{
					sqlCount = paramPlace+1;
				}
				
				paramCount++;
			}
			
			for(int i=removeParam.size();i>0;i--){
				int index = removeParam.get(i-1);
				args.remove(index);
			}			
			
			obj = jdbcTemplate.queryForObject(sql.toString(), args.toArray(), rowMapper);
			LOG.info("BaseDao.queryForObject sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForObject sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
			LOG.error("BaseDao.queryForObject Error: {}", ex.getMessage());
		}

		return obj;

	}

	protected Object queryForObject(final DataSource dataSource, final String sql, final Object[] args, final RowMapper rowMapper) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		Object obj = null;
		try {
			obj = jdbcTemplate.queryForObject(sql, args, rowMapper);
			LOG.info("BaseDao.queryForObject sql=" + fetchCombineSqlString(sql, args));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForObject sql=" + fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForObject Error: {}", ex.getMessage());
		}

		return obj;

	}

	protected T queryForObject(final DataSource dataSource, final StringBuilder sql, final ArrayList<Object> args, final Class<T> mappedClass) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		T obj = null;
		ArrayList<Integer> removeParam = new ArrayList<Integer>();
		try {
			int paramCount = 0;
			int sqlCount = 0;		
			int sqlLength = sql.length();			
					
			while(sqlCount < sqlLength){
				int sbStart = sql.indexOf("[");
				int sbEnd = sql.indexOf("]");
				int paramPlace = sql.indexOf("?", sqlCount);
				
				if(paramPlace == -1){					
					break;
				}
				
				if(sbStart == -1 || sbEnd == -1){
					break;
				}
					
				if(paramPlace > sbStart && paramPlace < sbEnd){
					if(args.get(paramCount) == null){
						sql.replace(sbStart, sbEnd+1, "");
						removeParam.add(paramCount);
						
						sqlCount = sbStart+1;
						sqlLength = sql.length();
					}else{						
						sql.replace(sbStart, sbStart+1, "");
						sql.replace(sbEnd-1, sbEnd, "");						
						
						sqlCount = sbEnd+1;
						sqlLength = sql.length();
					}
				}else{
					sqlCount = paramPlace+1;
				}
				
				paramCount++;
			}
			
			for(int i=removeParam.size();i>0;i--){
				int index = removeParam.get(i-1);
				args.remove(index);
			}
			
			obj = jdbcTemplate.queryForObject(sql.toString(), args.toArray(), new BeanPropertyRowMapper<T>(mappedClass));
			LOG.info("BaseDao.queryForObject sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForObject sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
			LOG.error("BaseDao.queryForObject Error: {}", ex.getMessage());
		}

		return obj;

	}
	
	protected T queryForObject(final DataSource dataSource, final String sql, final Object[] args, final Class<T> mappedClass) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		T obj = null;
		try {
			obj = jdbcTemplate.queryForObject(sql, args, new BeanPropertyRowMapper<T>(mappedClass));
			LOG.info("BaseDao.queryForObject sql=" + fetchCombineSqlString(sql, args));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForObject sql=" + fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForObject Error: {}", ex.getMessage());
		}

		return obj;

	}

	protected String queryForString(final DataSource dataSource, final StringBuilder sql, final ArrayList<Object> args) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		String result = null;
		ArrayList<Integer> removeParam = new ArrayList<Integer>();
		try {
			int paramCount = 0;
			int sqlCount = 0;		
			int sqlLength = sql.length();			
					
			while(sqlCount < sqlLength){
				int sbStart = sql.indexOf("[");
				int sbEnd = sql.indexOf("]");
				int paramPlace = sql.indexOf("?", sqlCount);
				
				if(paramPlace == -1){					
					break;
				}
				
				if(sbStart == -1 || sbEnd == -1){
					break;
				}
					
				if(paramPlace > sbStart && paramPlace < sbEnd){
					if(args.get(paramCount) == null){
						sql.replace(sbStart, sbEnd+1, "");
						removeParam.add(paramCount);
						
						sqlCount = sbStart+1;
						sqlLength = sql.length();
					}else{						
						sql.replace(sbStart, sbStart+1, "");
						sql.replace(sbEnd-1, sbEnd, "");						
						
						sqlCount = sbEnd+1;
						sqlLength = sql.length();
					}
				}else{
					sqlCount = paramPlace+1;
				}
				
				paramCount++;
			}
			
			for(int i=removeParam.size();i>0;i--){
				int index = removeParam.get(i-1);
				args.remove(index);
			}
			
			result = (String) jdbcTemplate.queryForObject(sql.toString(), args.toArray(), String.class);
			LOG.info("BaseDao.queryForString sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForString sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
			LOG.error("BaseDao.queryForString Error: {}", ex.getMessage());
		}
		return result;
	}
	
	protected String queryForString(final DataSource dataSource, final String sql, final Object[] args) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		String result = null;
		try {
			result = (String) jdbcTemplate.queryForObject(sql, args, String.class);
			LOG.info("BaseDao.queryForString sql=" + fetchCombineSqlString(sql, args));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForString sql=" + fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForString Error: {}", ex.getMessage());
		}
		return result;
	}
	
	protected long queryForLong(final DataSource dataSource, final StringBuilder sql, final ArrayList<Object> args) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		long obj = 0;
		ArrayList<Integer> removeParam = new ArrayList<Integer>();
		try {
			int paramCount = 0;
			int sqlCount = 0;		
			int sqlLength = sql.length();			
					
			while(sqlCount < sqlLength){
				int sbStart = sql.indexOf("[");
				int sbEnd = sql.indexOf("]");
				int paramPlace = sql.indexOf("?", sqlCount);
				
				if(paramPlace == -1){					
					break;
				}
				
				if(sbStart == -1 || sbEnd == -1){
					break;
				}
					
				if(paramPlace > sbStart && paramPlace < sbEnd){
					if(args.get(paramCount) == null){
						sql.replace(sbStart, sbEnd+1, "");
						removeParam.add(paramCount);
						
						sqlCount = sbStart+1;
						sqlLength = sql.length();
					}else{						
						sql.replace(sbStart, sbStart+1, "");
						sql.replace(sbEnd-1, sbEnd, "");						
						
						sqlCount = sbEnd+1;
						sqlLength = sql.length();
					}
				}else{
					sqlCount = paramPlace+1;
				}
				
				paramCount++;
			}
			
			for(int i=removeParam.size();i>0;i--){
				int index = removeParam.get(i-1);
				args.remove(index);
			}
			
			obj = jdbcTemplate.queryForObject(sql.toString(), args.toArray(), Long.class);
			LOG.info("BaseDao.queryForLong sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForLong sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
			LOG.error("BaseDao.queryForLong Error: {}", ex.getMessage());
		}

		return obj;

	}

	protected long queryForLong(final DataSource dataSource, final String sql, final Object[] args) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		long obj = 0;
		try {
			obj = jdbcTemplate.queryForObject(sql, args, Long.class);
			LOG.info("BaseDao.queryForLong sql=" + fetchCombineSqlString(sql, args));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForLong sql=" + fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForLong Error: {}", ex.getMessage());
		}

		return obj;

	}

	protected int queryForInt(final DataSource dataSource, final StringBuilder sql, final ArrayList<Object> args) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		int obj = 0;
		ArrayList<Integer> removeParam = new ArrayList<Integer>();
		try {
			int paramCount = 0;
			int sqlCount = 0;		
			int sqlLength = sql.length();			
					
			while(sqlCount < sqlLength){
				int sbStart = sql.indexOf("[");
				int sbEnd = sql.indexOf("]");
				int paramPlace = sql.indexOf("?", sqlCount);
				
				if(paramPlace == -1){					
					break;
				}
				
				if(sbStart == -1 || sbEnd == -1){
					break;
				}
					
				if(paramPlace > sbStart && paramPlace < sbEnd){
					if(args.get(paramCount) == null){
						sql.replace(sbStart, sbEnd+1, "");
						removeParam.add(paramCount);
						
						sqlCount = sbStart+1;
						sqlLength = sql.length();
					}else{						
						sql.replace(sbStart, sbStart+1, "");
						sql.replace(sbEnd-1, sbEnd, "");						
						
						sqlCount = sbEnd+1;
						sqlLength = sql.length();
					}
				}else{
					sqlCount = paramPlace+1;
				}
				
				paramCount++;
			}
			
			for(int i=removeParam.size();i>0;i--){
				int index = removeParam.get(i-1);
				args.remove(index);
			}
			
			obj = jdbcTemplate.queryForObject(sql.toString(), args.toArray(), Integer.class);
			LOG.info("BaseDao.queryForInt sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForInt sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
			LOG.error("BaseDao.queryForInt Error: {}", ex.getMessage());
		}

		return obj;

	}
	
	protected int queryForInt(final DataSource dataSource, final String sql, final Object[] args) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		int obj = 0;
		try {
			obj = jdbcTemplate.queryForObject(sql, args, Integer.class);
			LOG.info("BaseDao.queryForInt sql=" + fetchCombineSqlString(sql, args));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForInt sql=" + fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForInt Error: {}", ex.getMessage());
		}

		return obj;

	}

	protected double queryForDouble(final DataSource dataSource, final StringBuilder sql, final ArrayList<Object> args) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		double obj = 0;
		ArrayList<Integer> removeParam = new ArrayList<Integer>();
		try {
			int paramCount = 0;
			int sqlCount = 0;		
			int sqlLength = sql.length();			
					
			while(sqlCount < sqlLength){
				int sbStart = sql.indexOf("[");
				int sbEnd = sql.indexOf("]");
				int paramPlace = sql.indexOf("?", sqlCount);
				
				if(paramPlace == -1){					
					break;
				}
				
				if(sbStart == -1 || sbEnd == -1){
					break;
				}
					
				if(paramPlace > sbStart && paramPlace < sbEnd){
					if(args.get(paramCount) == null){
						sql.replace(sbStart, sbEnd+1, "");
						removeParam.add(paramCount);
						
						sqlCount = sbStart+1;
						sqlLength = sql.length();
					}else{						
						sql.replace(sbStart, sbStart+1, "");
						sql.replace(sbEnd-1, sbEnd, "");						
						
						sqlCount = sbEnd+1;
						sqlLength = sql.length();
					}
				}else{
					sqlCount = paramPlace+1;
				}
				
				paramCount++;
			}
			
			for(int i=removeParam.size();i>0;i--){
				int index = removeParam.get(i-1);
				args.remove(index);
			}
			
			obj = jdbcTemplate.queryForObject(sql.toString(), args.toArray(), Double.class);
			LOG.info("BaseDao.queryForDouble sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForDouble sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
			LOG.error("BaseDao.queryForDouble Error: {}", ex.getMessage());
		}

		return obj;

	}
	
	protected double queryForDouble(final DataSource dataSource, final String sql, final Object[] args) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		double obj = 0;
		try {
			obj = jdbcTemplate.queryForObject(sql, args, Double.class);
			LOG.info("BaseDao.queryForDouble sql=" + fetchCombineSqlString(sql, args));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForDouble sql=" + fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForDouble Error: {}", ex.getMessage());
		}

		return obj;

	}

	protected List queryForList(final DataSource dataSource, final StringBuffer sql, final ArrayList<Object> args, final RowMapper rowMapper) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List list = null;
		ArrayList<Integer> removeParam = new ArrayList<Integer>();
		try {
			int paramCount = 0;
			int sqlCount = 0;		
			int sqlLength = sql.length();			
					
			while(sqlCount < sqlLength){
				int sbStart = sql.indexOf("[");
				int sbEnd = sql.indexOf("]");
				int paramPlace = sql.indexOf("?", sqlCount);
				
				if(paramPlace == -1){					
					break;
				}
				
				if(sbStart == -1 || sbEnd == -1){
					break;
				}
					
				if(paramPlace > sbStart && paramPlace < sbEnd){
					if(args.get(paramCount) == null){
						sql.replace(sbStart, sbEnd+1, "");
						removeParam.add(paramCount);
						
						sqlCount = sbStart+1;
						sqlLength = sql.length();
					}else{						
						sql.replace(sbStart, sbStart+1, "");
						sql.replace(sbEnd-1, sbEnd, "");						
						
						sqlCount = sbEnd+1;
						sqlLength = sql.length();
					}
				}else{
					sqlCount = paramPlace+1;
				}
				
				paramCount++;
			}
			
			for(int i=removeParam.size();i>0;i--){
				int index = removeParam.get(i-1);
				args.remove(index);
			}
			
			list = jdbcTemplate.query(sql.toString(), args.toArray(), rowMapper);
			LOG.info("BaseDao.queryForList sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForList sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
			LOG.error("BaseDao.queryForList Error: {}", ex.getMessage());
			list = new ArrayList();
		}

		return list;

	}	
	
	protected List queryForList(final DataSource dataSource, final String sql, final Object[] args, final RowMapper rowMapper) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List list = null;
		try {
			list = jdbcTemplate.query(sql, args, rowMapper);
			LOG.info("BaseDao.queryForList sql=" + fetchCombineSqlString(sql, args));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForList sql=" + fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForList Error: {}", ex.getMessage());
			list = new ArrayList();
		}

		return list;

	}

	protected List<Map<String, Object>> queryForListMap(final DataSource dataSource, final StringBuilder sql, final ArrayList<Object> args) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List<Map<String, Object>> list = null;
		ArrayList<Integer> removeParam = new ArrayList<Integer>();
		try {
			int paramCount = 0;
			int sqlCount = 0;		
			int sqlLength = sql.length();			
					
			while(sqlCount < sqlLength){
				int sbStart = sql.indexOf("[");
				int sbEnd = sql.indexOf("]");
				int paramPlace = sql.indexOf("?", sqlCount);
				
				if(paramPlace == -1){					
					break;
				}
				
				if(sbStart == -1 || sbEnd == -1){
					break;
				}
					
				if(paramPlace > sbStart && paramPlace < sbEnd){
					if(args.get(paramCount) == null){
						sql.replace(sbStart, sbEnd+1, "");
						removeParam.add(paramCount);
						
						sqlCount = sbStart+1;
						sqlLength = sql.length();
					}else{						
						sql.replace(sbStart, sbStart+1, "");
						sql.replace(sbEnd-1, sbEnd, "");						
						
						sqlCount = sbEnd+1;
						sqlLength = sql.length();
					}
				}else{
					sqlCount = paramPlace+1;
				}
				
				paramCount++;
			}
			
			for(int i=removeParam.size();i>0;i--){
				int index = removeParam.get(i-1);
				args.remove(index);
			}
			
			list = jdbcTemplate.queryForList(sql.toString(), args.toArray());
			LOG.info("BaseDao.queryForListMap sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForListMap sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
			LOG.error("BaseDao.queryForListMap Error: {}", ex.getMessage());
			list = new ArrayList<Map<String, Object>>();
		}

		return list;

	}
	
	protected List<Map<String, Object>> queryForListMap(final DataSource dataSource, final String sql, final Object[] args) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List<Map<String, Object>> list = null;
		try {
			list = jdbcTemplate.queryForList(sql, args);
			LOG.info("BaseDao.queryForListMap sql=" + fetchCombineSqlString(sql, args));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForListMap sql=" + fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForListMap Error: {}", ex.getMessage());
			list = new ArrayList<Map<String, Object>>();
		}

		return list;

	}

	protected List queryForList(final DataSource dataSource, final StringBuilder sql, final ArrayList<Object> args, final Class<T> mappedClass) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List list = null;
		ArrayList<Integer> removeParam = new ArrayList<Integer>();
		try {
			int paramCount = 0;
			int sqlCount = 0;		
			int sqlLength = sql.length();			
					
			while(sqlCount < sqlLength){
				int sbStart = sql.indexOf("[");
				int sbEnd = sql.indexOf("]");
				int paramPlace = sql.indexOf("?", sqlCount);
				
				if(paramPlace == -1){					
					break;
				}
				
				if(sbStart == -1 || sbEnd == -1){
					break;
				}
					
				if(paramPlace > sbStart && paramPlace < sbEnd){
					if(args.get(paramCount) == null){
						sql.replace(sbStart, sbEnd+1, "");
						removeParam.add(paramCount);
						
						sqlCount = sbStart+1;
						sqlLength = sql.length();
					}else{						
						sql.replace(sbStart, sbStart+1, "");
						sql.replace(sbEnd-1, sbEnd, "");						
						
						sqlCount = sbEnd+1;
						sqlLength = sql.length();
					}
				}else{
					sqlCount = paramPlace+1;
				}
				
				paramCount++;
			}
			
			for(int i=removeParam.size();i>0;i--){
				int index = removeParam.get(i-1);
				args.remove(index);
			}
			
			list = jdbcTemplate.query(sql.toString(), args.toArray(), new BeanPropertyRowMapper(mappedClass));
			LOG.info("BaseDao.queryForList sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForList sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
			LOG.error("BaseDao.queryForList Error: {}", ex.getMessage());
			list = new ArrayList();
		}

		return list;

	}
	
	protected List queryForList(final DataSource dataSource, final String sql, final Object[] args, final Class<T> mappedClass) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List list = null;
		try {
			list = jdbcTemplate.query(sql, args, new BeanPropertyRowMapper(mappedClass));
			LOG.info("BaseDao.queryForList sql=" + fetchCombineSqlString(sql, args));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForList sql=" + fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForList Error: {}", ex.getMessage());
			list = new ArrayList();
		}

		return list;

	}

	protected List queryForObjectList(final DataSource dataSource, final StringBuilder sql, final ArrayList<Object> args, final Class clazz) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List list = null;
		ArrayList<Integer> removeParam = new ArrayList<Integer>();
		try {
			int paramCount = 0;
			int sqlCount = 0;		
			int sqlLength = sql.length();			
					
			while(sqlCount < sqlLength){
				int sbStart = sql.indexOf("[");
				int sbEnd = sql.indexOf("]");
				int paramPlace = sql.indexOf("?", sqlCount);
				
				if(paramPlace == -1){					
					break;
				}
				
				if(sbStart == -1 || sbEnd == -1){
					break;
				}
					
				if(paramPlace > sbStart && paramPlace < sbEnd){
					if(args.get(paramCount) == null){
						sql.replace(sbStart, sbEnd+1, "");
						removeParam.add(paramCount);
						
						sqlCount = sbStart+1;
						sqlLength = sql.length();
					}else{						
						sql.replace(sbStart, sbStart+1, "");
						sql.replace(sbEnd-1, sbEnd, "");						
						
						sqlCount = sbEnd+1;
						sqlLength = sql.length();
					}
				}else{
					sqlCount = paramPlace+1;
				}
				
				paramCount++;
			}
			
			for(int i=removeParam.size();i>0;i--){
				int index = removeParam.get(i-1);
				args.remove(index);
			}
			
			list = jdbcTemplate.queryForList(sql.toString(), args.toArray(), clazz);
			LOG.info("BaseDao.queryForObjectList sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForObjectList sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
			LOG.error("BaseDao.queryForObjectList Error: {}", ex.getMessage());
			list = new ArrayList();
		}

		return list;
	}
	
	protected List queryForObjectList(final DataSource dataSource, final String sql, final Object[] args, final Class clazz) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List list = null;
		try {
			list = jdbcTemplate.queryForList(sql, args, clazz);
			LOG.info("BaseDao.queryForObjectList sql=" + fetchCombineSqlString(sql, args));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForObjectList sql=" + fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForObjectList Error: {}", ex.getMessage());
			list = new ArrayList();
		}

		return list;
	}

	protected List<Long> queryForLongList(final DataSource dataSource, final StringBuilder sql, final ArrayList<Object> args) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List<Long> list = null;
		ArrayList<Integer> removeParam = new ArrayList<Integer>();
		try {
			int paramCount = 0;
			int sqlCount = 0;		
			int sqlLength = sql.length();			
					
			while(sqlCount < sqlLength){
				int sbStart = sql.indexOf("[");
				int sbEnd = sql.indexOf("]");
				int paramPlace = sql.indexOf("?", sqlCount);
				
				if(paramPlace == -1){					
					break;
				}
				
				if(sbStart == -1 || sbEnd == -1){
					break;
				}
					
				if(paramPlace > sbStart && paramPlace < sbEnd){
					if(args.get(paramCount) == null){
						sql.replace(sbStart, sbEnd+1, "");
						removeParam.add(paramCount);
						
						sqlCount = sbStart+1;
						sqlLength = sql.length();
					}else{						
						sql.replace(sbStart, sbStart+1, "");
						sql.replace(sbEnd-1, sbEnd, "");						
						
						sqlCount = sbEnd+1;
						sqlLength = sql.length();
					}
				}else{
					sqlCount = paramPlace+1;
				}
				
				paramCount++;
			}
			
			for(int i=removeParam.size();i>0;i--){
				int index = removeParam.get(i-1);
				args.remove(index);
			}
			
			list = jdbcTemplate.queryForList(sql.toString(), args.toArray(), Long.class);
			LOG.info("BaseDao.queryForLongList sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForLongList sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
			LOG.error("BaseDao.queryForLongList Error: {}", ex.getMessage());
			list = new ArrayList<Long>();
		}

		return list;
	}
	
	protected List<Long> queryForLongList(final DataSource dataSource, final String sql, final Object[] args) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List<Long> list = null;
		try {
			list = jdbcTemplate.queryForList(sql, args, Long.class);
			LOG.info("BaseDao.queryForLongList sql=" + fetchCombineSqlString(sql, args));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForLongList sql=" + fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForLongList Error: {}", ex.getMessage());
			list = new ArrayList<Long>();
		}

		return list;
	}
	
	protected Page<?> queryForPage(final DataSource dataSource, Page<?> page, final StringBuilder sql, final StringBuilder countSql, final ArrayList<Object> args,
			final RowMapper rowMapper) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List list = null;
		int totalCount = 0;		
		List<Integer> removeParam = new ArrayList<Integer>();
		try {
			int paramCount = 0;
			int param1Count = 0;
			int sqlCount = 0;
			int sql1Count = 0;			
			int sqlLength = sql.length();
			int countSqlLength = countSql.length();
						
			while(sql1Count < countSqlLength){
				int sbStart = countSql.indexOf("[");
				int sbEnd = countSql.indexOf("]");
				int paramPlace = countSql.indexOf("?", sql1Count);
				
				if(paramPlace == -1){					
					break;
				}
					
				if(paramPlace > sbStart && paramPlace < sbEnd){
					if(args.get(param1Count) == null){
						countSql.replace(sbStart, sbEnd+1, "");						
						
						sql1Count = sbStart+1;
						countSqlLength = countSql.length();
					}else{						
						countSql.replace(sbStart, sbStart+1, "");
						countSql.replace(sbEnd-1, sbEnd, "");						
						
						sql1Count = sbEnd+1;
						countSqlLength = countSql.length();
					}
				}else{
					sql1Count = paramPlace+1;
				}
				
				param1Count++;
			}
			
			while(sqlCount < sqlLength){
				int sbStart = sql.indexOf("[");
				int sbEnd = sql.indexOf("]");
				int paramPlace = sql.indexOf("?", sqlCount);
				
				if(paramPlace == -1){					
					break;
				}
					
				if(paramPlace > sbStart && paramPlace < sbEnd){
					if(args.get(paramCount) == null){
						sql.replace(sbStart, sbEnd+1, "");
						removeParam.add(paramCount);
						
						sqlCount = sbStart+1;
						sqlLength = sql.length();
					}else{						
						sql.replace(sbStart, sbStart+1, "");
						sql.replace(sbEnd-1, sbEnd, "");						
						
						sqlCount = sbEnd+1;
						sqlLength = sql.length();
					}
				}else{
					sqlCount = paramPlace+1;
				}
				
				paramCount++;
			}
			
			for(int i=removeParam.size();i>0;i--){
				int index = removeParam.get(i-1);
				args.remove(index);
			}
			
			list = jdbcTemplate.query(sql.toString(), args.toArray(), rowMapper);
			totalCount = jdbcTemplate.queryForObject(countSql.toString(), args.toArray(), Integer.class);
			LOG.info("BaseDao.queryForPage query sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
			LOG.info("BaseDao.queryForPage totalCount sql=" + fetchCombineSqlString(countSql.toString(), args.toArray()));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForPage sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
			LOG.error("BaseDao.queryForPage Error: {}", ex.getMessage());
			LOG.error("", ex);
		}
		page.setResult(list);
		page.setTotalCount(totalCount);

		return page;
	}
	
	protected Page<?> queryForPage(final DataSource dataSource, Page<?> page, final String sql, final String countSql, final Object[] args,
			final RowMapper rowMapper) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List list = null;
		int totalCount = 0;
		try {			
			list = jdbcTemplate.query(sql, args, rowMapper);
			totalCount = jdbcTemplate.queryForObject(countSql, args, Integer.class);
			LOG.info("BaseDao.queryForPage query sql=" + fetchCombineSqlString(sql, args));
			LOG.info("BaseDao.queryForPage totalCount sql=" + fetchCombineSqlString(countSql, args));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForPage sql=" + fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForPage Error: {}", ex.getMessage());
			LOG.error("", ex);
		}
		page.setResult(list);
		page.setTotalCount(totalCount);

		return page;
	}
	
	protected Page<T> queryForPage(final DataSource dataSource, Page<T> page, final StringBuilder sql, final StringBuilder countSql, final ArrayList<Object> args,
			final Class<T> mappedClass) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List<T> list = null;
		int totalCount = 0;
		ArrayList<Integer> removeParam = new ArrayList<Integer>();
		try {
			int paramCount = 0;
			int param1Count = 0;
			int sqlCount = 0;
			int sql1Count = 0;			
			int sqlLength = sql.length();
			int countSqlLength = countSql.length();
						
			while(sql1Count < countSqlLength){
				int sbStart = countSql.indexOf("[");
				int sbEnd = countSql.indexOf("]");
				int paramPlace = countSql.indexOf("?", sql1Count);
				
				if(paramPlace == -1){					
					break;
				}
					
				if(paramPlace > sbStart && paramPlace < sbEnd){
					if(args.get(param1Count) == null){
						countSql.replace(sbStart, sbEnd+1, "");						
						
						sql1Count = sbStart+1;
						countSqlLength = countSql.length();
					}else{						
						countSql.replace(sbStart, sbStart+1, "");
						countSql.replace(sbEnd-1, sbEnd, "");						
						
						sql1Count = sbEnd+1;
						countSqlLength = countSql.length();
					}
				}else{
					sql1Count = paramPlace+1;
				}
				
				param1Count++;
			}
			
			while(sqlCount < sqlLength){
				int sbStart = sql.indexOf("[");
				int sbEnd = sql.indexOf("]");
				int paramPlace = sql.indexOf("?", sqlCount);
				
				if(paramPlace == -1){					
					break;
				}
					
				if(paramPlace > sbStart && paramPlace < sbEnd){
					if(args.get(paramCount) == null){
						sql.replace(sbStart, sbEnd+1, "");
						removeParam.add(paramCount);
						
						sqlCount = sbStart+1;
						sqlLength = sql.length();
					}else{						
						sql.replace(sbStart, sbStart+1, "");
						sql.replace(sbEnd-1, sbEnd, "");						
						
						sqlCount = sbEnd+1;
						sqlLength = sql.length();
					}
				}else{
					sqlCount = paramPlace+1;
				}
				
				paramCount++;
			}
			
			for(int i=removeParam.size();i>0;i--){
				int index = removeParam.get(i-1);
				args.remove(index);
			}
			
			list = jdbcTemplate.queryForList(sql.toString(), args.toArray(), mappedClass);
			totalCount = jdbcTemplate.queryForObject(countSql.toString(), args.toArray(), Integer.class);
			LOG.info("BaseDao.queryForPage query sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
			LOG.info("BaseDao.queryForPage totalCount sql=" + fetchCombineSqlString(countSql.toString(), args.toArray()));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForPage sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
			LOG.error("BaseDao.queryForPage Error: {}", ex.getMessage());
			LOG.error("", ex);
		}
		page.setResult(list);
		page.setTotalCount(totalCount);

		return page;
	}
	
	protected Page<T> queryForPage(final DataSource dataSource, Page<T> page, final String sql, final String countSql, final Object[] args,
			final Class<T> mappedClass) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List<T> list = null;
		int totalCount = 0;
		try {
			list = jdbcTemplate.queryForList(sql, args, mappedClass);
			totalCount = jdbcTemplate.queryForObject(countSql, args, Integer.class);
			LOG.info("BaseDao.queryForPage query sql=" + fetchCombineSqlString(sql, args));
			LOG.info("BaseDao.queryForPage totalCount sql=" + fetchCombineSqlString(countSql, args));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForPage sql=" + fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForPage Error: {}", ex.getMessage());
			LOG.error("", ex);
		}
		page.setResult(list);
		page.setTotalCount(totalCount);

		return page;
	}

	protected Map queryForMap(final DataSource dataSource, final StringBuilder sql, final ArrayList<Object> args) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		Map map = null;
		ArrayList<Integer> removeParam = new ArrayList<Integer>();
		try {
			int paramCount = 0;
			int sqlCount = 0;		
			int sqlLength = sql.length();			
					
			while(sqlCount < sqlLength){
				int sbStart = sql.indexOf("[");
				int sbEnd = sql.indexOf("]");
				int paramPlace = sql.indexOf("?", sqlCount);
				
				if(paramPlace == -1){					
					break;
				}
				
				if(sbStart == -1 || sbEnd == -1){
					break;
				}
					
				if(paramPlace > sbStart && paramPlace < sbEnd){
					if(args.get(paramCount) == null){
						sql.replace(sbStart, sbEnd+1, "");
						removeParam.add(paramCount);
						
						sqlCount = sbStart+1;
						sqlLength = sql.length();
					}else{						
						sql.replace(sbStart, sbStart+1, "");
						sql.replace(sbEnd-1, sbEnd, "");						
						
						sqlCount = sbEnd+1;
						sqlLength = sql.length();
					}
				}else{
					sqlCount = paramPlace+1;
				}
				
				paramCount++;
			}
			
			for(int i=removeParam.size();i>0;i--){
				int index = removeParam.get(i-1);
				args.remove(index);
			}
			
			map = jdbcTemplate.queryForMap(sql.toString(), args.toArray());
			LOG.info("BaseDao.queryForMap sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForMap sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
			LOG.error("BaseDao.queryForMap Error: {}", ex.getMessage());
			map = new HashMap();
		}

		return map;

	}
	
	protected Map queryForMap(final DataSource dataSource, final String sql, final Object[] args) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		Map map = null;
		try {
			map = jdbcTemplate.queryForMap(sql, args);
			LOG.info("BaseDao.queryForMap sql=" + fetchCombineSqlString(sql, args));
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForMap sql=" + fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForMap Error: {}", ex.getMessage());
			map = new HashMap();
		}

		return map;

	}	
	
	protected BaseDto addForParam(final DataSource dataSource, final String sql, final BaseDto dto, final String[] params) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		KeyHolder keyHolder = new GeneratedKeyHolder();

		jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
				PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				for (int i = 0; i < params.length; i++) {
					try {
						ps.setObject(i + 1, PropertyUtils.getProperty(dto, params[i]));
					} catch (Exception e) {
						LOG.error("BaseDao.addForParam sql=" + sql);
						LOG.error("BaseDao.addForParam Error: {}", e.getMessage());
					}

				}
				return ps;
			}
		}, keyHolder);
		LOG.info("BaseDao.addForParam sql=" + sql);
		dto.setId(keyHolder.getKey().longValue());
		return dto;
	}	
	
	protected BaseDto addForObject(final DataSource dataSource, final String sql, final BaseDto dto, final Object[] args) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		KeyHolder keyHolder = new GeneratedKeyHolder();

		jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
				PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				for (int i = 0; i < args.length; i++) {
					try {
						ps.setObject(i + 1, args[i]);
					} catch (Exception e) {
						LOG.error("BaseDao.addForObject sql=" + fetchCombineSqlString(sql, args));
						LOG.error("BaseDao.addForObject Error: {}", e.getMessage());
					}

				}
				return ps;
			}
		}, keyHolder);
		LOG.info("BaseDao.addForObject sql=" + fetchCombineSqlString(sql, args));
		dto.setId(keyHolder.getKey().longValue());
		return dto;
	}

	protected int updateForParam(final DataSource dataSource, final StringBuilder sql, final BaseDto dto, final ArrayList<String> params) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);		
		ArrayList<Integer> removeParam = new ArrayList<Integer>();
		
		int paramCount = 0;
		int sqlCount = 0;		
		int sqlLength = sql.length();			
				
		while(sqlCount < sqlLength){
			int sbStart = sql.indexOf("[");
			int sbEnd = sql.indexOf("]");
			int paramPlace = sql.indexOf("?", sqlCount);
			
			if(paramPlace == -1){					
				break;
			}
			
			if(sbStart == -1 || sbEnd == -1){
				break;
			}
				
			if(paramPlace > sbStart && paramPlace < sbEnd){
				if(params.get(paramCount) == null){
					sql.replace(sbStart, sbEnd+1, "");
					removeParam.add(paramCount);
					
					sqlCount = sbStart+1;
					sqlLength = sql.length();
				}else{						
					sql.replace(sbStart, sbStart+1, "");
					sql.replace(sbEnd-1, sbEnd, "");						
					
					sqlCount = sbEnd+1;
					sqlLength = sql.length();
				}
			}else{
				sqlCount = paramPlace+1;
			}
			
			paramCount++;
		}
		
		for(int i=removeParam.size();i>0;i--){
			int index = removeParam.get(i-1);
			params.remove(index);
		}
		
		int result = jdbcTemplate.update(sql.toString(), new PreparedStatementSetter() {
			public void setValues(PreparedStatement ps) throws SQLException {
				for (int i = 0; i < params.size(); i++) {
					try {
						ps.setObject(i + 1, PropertyUtils.getProperty(dto, params.get(i)));
					} catch (Exception e) {
						LOG.error("BaseDao.updateForParam sql=" + sql.toString());
						LOG.error("BaseDao.updateForParam Error: {}", e.getMessage());
					}

				}
			}
		});
		LOG.info("BaseDao.updateForParam sql=" + sql.toString());
		return result;
	}
	
	protected int updateForParam(final DataSource dataSource, final String sql, final BaseDto dto, final String[] params) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		int result = jdbcTemplate.update(sql, new PreparedStatementSetter() {
			public void setValues(PreparedStatement ps) throws SQLException {
				for (int i = 0; i < params.length; i++) {
					try {
						ps.setObject(i + 1, PropertyUtils.getProperty(dto, params[i]));
					} catch (Exception e) {
						LOG.error("BaseDao.updateForParam sql=" + sql);
						LOG.error("BaseDao.updateForParam Error: {}", e.getMessage());
					}

				}
			}
		});
		LOG.info("BaseDao.updateForParam sql=" + sql);
		return result;
	}

	protected int updateForObject(final DataSource dataSource, final StringBuilder sql, final ArrayList<Object> args) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		ArrayList<Integer> removeParam = new ArrayList<Integer>();
		
		int paramCount = 0;
		int sqlCount = 0;		
		int sqlLength = sql.length();			
				
		while(sqlCount < sqlLength){
			int sbStart = sql.indexOf("[");
			int sbEnd = sql.indexOf("]");
			int paramPlace = sql.indexOf("?", sqlCount);
			
			if(paramPlace == -1){					
				break;
			}
			
			if(sbStart == -1 || sbEnd == -1){
				break;
			}
				
			if(paramPlace > sbStart && paramPlace < sbEnd){
				if(args.get(paramCount) == null){
					sql.replace(sbStart, sbEnd+1, "");
					removeParam.add(paramCount);
					
					sqlCount = sbStart+1;
					sqlLength = sql.length();
				}else{						
					sql.replace(sbStart, sbStart+1, "");
					sql.replace(sbEnd-1, sbEnd, "");						
					
					sqlCount = sbEnd+1;
					sqlLength = sql.length();
				}
			}else{
				sqlCount = paramPlace+1;
			}
			
			paramCount++;
		}
		
		for(int i=removeParam.size();i>0;i--){
			int index = removeParam.get(i-1);
			args.remove(index);
		}
		
		LOG.info("BaseDao.updateForObject sql=" + fetchCombineSqlString(sql.toString(), args.toArray()));
		return jdbcTemplate.update(sql.toString(), args.toArray());
	}
	
	protected int updateForObject(final DataSource dataSource, final String sql, final Object[] args) {
//		JdbcTemplate jdbcTemplate = getJdbcTemplate();
//		jdbcTemplate.setDataSource(dataSource);
//		LOG.info("BaseDao.updateForObject sql=" + fetchCombineSqlString(sql, args));
//		return jdbcTemplate.update(sql, args);
		return updateForObject(dataSource, new StringBuilder(sql), new ArrayList<Object>(Arrays.asList(args)));
	}

}
