/**
 * 
 */
package com.baplay.dao.impl;

import com.baplay.dao.IFunctionDao;
import com.baplay.entity.Function;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.List;

/**
 * @author Haozi
 *
 */
@Repository
@Transactional
public class FunctionDao extends BaseDao<Function> implements IFunctionDao {
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IFunctionDao#save(com.baplay.dto.Function)
	 */
	@Override
	public void save(Function function) {
		Function old = (Function) this.queryForObject(dsBmsQry, "select * from t_erp_function where name = ?", new Object[] { function.getName() },
				Function.class);
		if (old != null) {
			return;
		}
		this.addForParam(dsBmsUpd, "insert into t_erp_function (name,parent_id,url,type,status) values (?,?,?,?,1)", function, new String[] { "name", "parentId", "url",
				"type" });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IFunctionDao#update(com.baplay.dto.Function)
	 */
	@Override
	public void update(Function function) {
		Function old = (Function) this.queryForObject(dsBmsQry, "select * from t_erp_function where name = ?", new Object[] { function.getName() },
				Function.class);
		if (old != null && !old.getId().equals(function.getId())) {
			return;
		}
		this.updateForParam(dsBmsUpd, "update t_erp_function set name = ? ,parent_id = ?,url = ?,type=? where id = ?", function, new String[] { "name",
				"parentId", "url", "type", "id" });

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IFunctionDao#delete(com.baplay.dto.Function)
	 */
	@Override
	public void delete(Function function) {
		this.updateForParam(dsBmsUpd, "update t_erp_function set status=? where id = ?", function, new String[] { "status", "id" });
		this.updateForParam(dsBmsUpd, "update t_erp_role_function set status=? where function_id = ?", function, new String[] { "status", "id" });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IFunctionDao#query(Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Function> query(Long id) {
		return this.queryForList(dsBmsQry, "select * from t_erp_function where parent_id = ?", new Object[] { id }, new BeanPropertyRowMapper<Function>(
				Function.class));
	}

	@Override
	public Function queryOne(Long id) {
		return (Function) this.queryForObject(dsBmsQry, "select * from t_erp_function where id = ?", new Object[] { id }, new BeanPropertyRowMapper<Function>(
				Function.class));
	}
}
