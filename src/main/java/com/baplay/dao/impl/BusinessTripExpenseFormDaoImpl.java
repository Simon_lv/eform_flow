package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import com.baplay.dao.IBusinessTripExpenseFormDao;
import com.baplay.entity.BusinessTripExpenseForm;
import com.baplay.form.BusinessTripExpenseFormForm;

@Repository
public class BusinessTripExpenseFormDaoImpl extends BaseDao<BusinessTripExpenseForm> implements IBusinessTripExpenseFormDao {
	
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public BusinessTripExpenseForm add(BusinessTripExpenseForm businessTripExpenseForm) {
		String sql = "INSERT INTO `business_trip_expense_form` (`company_id`, `request_department`, `employee_id`, `request_date`, `serial_no`, `remit_date`, `purpose`, `duration_start`, `duration_end`, `payee`, `payee_bank`, `payee_account`, `generic_expenses_total`, `entertainment_expenses_total`, `business_card_consumption`, `amount_prepaid`, `amount_reimbursable`, `work_flow_id`) VALUES (?, ?, ?, NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		return (BusinessTripExpenseForm) super.addForObject(dsBmsUpd, sql, businessTripExpenseForm, 
				new Object[]{ 
						businessTripExpenseForm.getCompanyId(), businessTripExpenseForm.getRequestDepartment(), businessTripExpenseForm.getEmployeeId(),
						businessTripExpenseForm.getSerialNo(), businessTripExpenseForm.getRemitDate(), businessTripExpenseForm.getPurpose(), 
						businessTripExpenseForm.getDurationStart(), businessTripExpenseForm.getDurationEnd(),
						businessTripExpenseForm.getPayee(), businessTripExpenseForm.getPayeeBank(),
						businessTripExpenseForm.getPayeeAccount(),
						businessTripExpenseForm.getGenericExpensesTotal(), businessTripExpenseForm.getEntertainmentExpensesTotal(),
						businessTripExpenseForm.getBusinessCardConsumption(), businessTripExpenseForm.getAmountPrepaid(),
						businessTripExpenseForm.getAmountReimbursable(), businessTripExpenseForm.getWorkFlowId() });
	}

	@Override
	public int update(BusinessTripExpenseForm businessTripExpenseForm) {
		String sql = "UPDATE `business_trip_expense_form` SET `company_id`=?, `request_department`=?, `employee_id`=?, `serial_no`=?, `remit_date`=?, `purpose`=?, `duration_start`=?, `duration_end`=?, `payee`=?, `payee_bank`=?, `payee_account`=?, `generic_expenses_total`=?, `entertainment_expenses_total`=?, `business_card_consumption`=?, `amount_prepaid`=?, `amount_reimbursable`=? WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, 
				new Object[]{ businessTripExpenseForm.getCompanyId(), businessTripExpenseForm.getRequestDepartment(), businessTripExpenseForm.getEmployeeId(),
					businessTripExpenseForm.getSerialNo(), businessTripExpenseForm.getRemitDate(), businessTripExpenseForm.getPurpose(),businessTripExpenseForm.getDurationStart(), 
					businessTripExpenseForm.getDurationEnd(), businessTripExpenseForm.getPayee(),
					businessTripExpenseForm.getPayeeBank(), businessTripExpenseForm.getPayeeAccount(), 
					businessTripExpenseForm.getGenericExpensesTotal(),
					businessTripExpenseForm.getEntertainmentExpensesTotal(), businessTripExpenseForm.getBusinessCardConsumption(),
					businessTripExpenseForm.getAmountPrepaid(), businessTripExpenseForm.getAmountReimbursable(),
					businessTripExpenseForm.getId() });
	}

	@Override
	public BusinessTripExpenseForm findOneById(Long id) {
		String sql = "SELECT a.*, (select name from company where id = a.company_id) as companyName, "
					+ "(select name from department where id = a.request_department) as departmentName, "
					+ "(select name from employee where id = (select employee_id from t_erp_user where id = (select creator from work_flow where id = a.work_flow_id))) as creatorName, "
					+ "(select name from employee where id = a.employee_id) employee_name "
					+ "FROM business_trip_expense_form as a WHERE a.id=?";
		Object[] args = new Object[]{id};
		return (BusinessTripExpenseForm) super.queryForObject(dsBmsQry, sql, args, BusinessTripExpenseForm.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Page<BusinessTripExpenseForm> list(BusinessTripExpenseFormForm businessTripExpenseFormForm) {
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		
		// Auth
		param.add(businessTripExpenseFormForm.getLoginUser());
		param.add(businessTripExpenseFormForm.getFormType());
		param.add(businessTripExpenseFormForm.getLoginUser());
		param.add(businessTripExpenseFormForm.getLoginUser());
		
		if(businessTripExpenseFormForm.getCompanyId() != null){
			sb.append(" AND b.company_id = ? ");
			param.add(businessTripExpenseFormForm.getCompanyId());
		}
		if(businessTripExpenseFormForm.getRequestDepartment() != null){
			sb.append(" AND b.request_department = ? ");
			param.add(businessTripExpenseFormForm.getRequestDepartment());
		}
		if(StringUtils.isNotBlank(businessTripExpenseFormForm.getSerialNo())){
			sb.append(" AND b.serial_no = ? ");
			param.add(businessTripExpenseFormForm.getSerialNo());
		}
		if(StringUtils.isNotBlank(businessTripExpenseFormForm.getRequestDateStart())){		
			sb.append(" AND STR_TO_DATE(b.request_date, '%Y-%m-%d') >= ? ");
			param.add(businessTripExpenseFormForm.getRequestDateStart());
		}
		if(StringUtils.isNotBlank(businessTripExpenseFormForm.getRequestDateEnd())){
			sb.append(" AND STR_TO_DATE(b.request_date, '%Y-%m-%d') <= ? ");
			param.add(businessTripExpenseFormForm.getRequestDateEnd());
		}
		if(StringUtils.isNotBlank(businessTripExpenseFormForm.getPaymentDateStart())){		
			sb.append(" AND STR_TO_DATE(b.payment_date, '%Y-%m-%d') >= ? ");
			param.add(businessTripExpenseFormForm.getPaymentDateStart());
		}
		if(StringUtils.isNotBlank(businessTripExpenseFormForm.getPaymentDateEnd())){
			sb.append(" AND STR_TO_DATE(b.payment_date, '%Y-%m-%d') <= ? ");
			param.add(businessTripExpenseFormForm.getPaymentDateEnd());
		}
		if(businessTripExpenseFormForm.getRemitted() != null){
			sb.append(" AND b.remitted = ? ");
			param.add(businessTripExpenseFormForm.getRemitted());
		}
		if(businessTripExpenseFormForm.getStatus() != null){
			sb.append(" AND b.status = ? ");
			param.add(businessTripExpenseFormForm.getStatus());
		}
		
		// Auth
		String userFormRole = StringUtils.join(businessTripExpenseFormForm.getFormRole(), "','");
		
		StringBuilder sql = new StringBuilder("SELECT b.* FROM (SELECT a.*, (SELECT COUNT(*) FROM form_auth WHERE form_type='PT' and form_role IN (SELECT id FROM form_role WHERE `name` IN('" + userFormRole + "'))) as auth, (select name from company where id = a.company_id) as companyName, (select name from department where id = a.request_department) as departmentName, (select status from work_flow where id = a.work_flow_id) as STATUS, (select creator from work_flow where id = a.work_flow_id) as creator, (SELECT `name` FROM employee WHERE id=(select employee_id from t_erp_user where id=(select creator from work_flow where id = a.work_flow_id))) as creatorName, (select name from employee where id = a.employee_id) employee_name FROM business_trip_expense_form a) as b ");
		sql.append("left join (select work_flow_id, owner_id from work_flow_detail where owner_id=? and work_flow_id in (select id from work_flow where LEFT(form_type, 2)=?) group by work_flow_id) wfd on wfd.work_flow_id=b.work_flow_id ").append("WHERE (auth>=1 OR creator=? OR owner_id=?)").append(sb.toString());
		StringBuilder countSql = new StringBuilder("select count(*) from (").append(sql).append(") x");
				
		if(!businessTripExpenseFormForm.isExport()) {
			sql.append(" LIMIT ").append((businessTripExpenseFormForm.getPageNo()-1)*businessTripExpenseFormForm.getPageSize()).append(",").append(businessTripExpenseFormForm.getPageSize());
		}
		
		Page<BusinessTripExpenseForm> page = new Page<BusinessTripExpenseForm>(businessTripExpenseFormForm.getPageSize());

		page = (Page<BusinessTripExpenseForm>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(), 
				new BeanPropertyRowMapper<BusinessTripExpenseForm>(BusinessTripExpenseForm.class));
		
		if(!businessTripExpenseFormForm.isExport()) {
			page.setPageNo(businessTripExpenseFormForm.getPageNo());
		}
		
		return page;
	}

	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM `business_trip_expense_form` WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}

	@Override
	public BusinessTripExpenseForm findOneByFlow(Long workFlowId) {
		String sql = "SELECT a.*, (select name from company where id = a.company_id) as companyName, (select name from department where id = a.request_department) as departmentName, (select name from employee where id = (select employee_id from t_erp_user where id = (select creator from work_flow where id = a.work_flow_id))) as creatorName, (select name from employee where id = a.employee_id) employee_name FROM business_trip_expense_form as a WHERE a.work_flow_id=?";
		Object[] args = new Object[]{workFlowId};
		return (BusinessTripExpenseForm) super.queryForObject(dsBmsQry, sql, args, BusinessTripExpenseForm.class);
	}

	@Override
	public int updateTotal(Long id, Long editId, int costType, String formType) {
		String sql1 = "SELECT ROUND((amount/rate),0) FROM receipt WHERE form_id=? and id!=? and form_type=? and cost_type=?";
		int sum = super.queryForInt(dsBmsQry, sql1, new Object[]{id, editId, formType, costType});
		
		String fieldName = "";
		if(costType == 1){
			fieldName = "generic_expenses_total";
		}else{
			fieldName = "entertainment_expenses_total";
		}
		
		String sql2 = "UPDATE business_trip_expense_form SET "+fieldName+"=? WHERE id=?";
		return super.updateForObject(dsBmsUpd, sql2, new Object[]{sum, id});
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IBusinessTripExpenseFormDao#remark(java.lang.Long, java.lang.String)
	 */
	@Override
	public int remark(Long workFlowId, String remark) {
		String sql2 = "UPDATE business_trip_expense_form SET `remark`=? WHERE work_flow_id=?";
		return super.updateForObject(dsBmsUpd, sql2, new Object[]{remark, workFlowId});
	}
	
	@Override
	public int confirmRemitDate(Long id) {
		String sql = "UPDATE `business_trip_expense_form` SET `remitted`=2 WHERE `id`=? AND `remitted`=1";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}
	
	@Override
	public int updateRemitDate(Long id, Date remitDate) {
		String sql = "UPDATE `business_trip_expense_form` SET `remit_date`=? WHERE `id`=? AND `remitted`=1";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{remitDate, id});
	}
}