package com.baplay.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import com.baplay.dao.IUserEmployeeDao;
import com.baplay.entity.UserEmployee;

@Repository
public class UserEmployeeDaoImpl extends BaseDao<UserEmployee> implements IUserEmployeeDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}

	/* (non-Javadoc)
	 * @see com.baplay.dao.IUserEmployeeDao#add(long, long)
	 */
	@Override
	public UserEmployee add(long userId, long employeeId) {
		UserEmployee ue = new UserEmployee();
		
		ue.setUserId(userId);
		ue.setEmployeeId(employeeId);		
		
		return (UserEmployee) addForParam(dsBmsUpd, "INSERT INTO t_erp_user_employee (user_id, employee_id) values(?,?)", ue, new String[] { "userId", "employeeId" });
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IUserEmployeeDao#deleteByUID(long)
	 */
	@Override
	public int deleteByUID(long userId) {
		return updateForObject(dsBmsUpd, "DELETE FROM t_erp_user_employee WHERE user_id=?", new Object[] { userId });
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IUserEmployeeDao#findByUID(long)
	 */
	@Override
	public List<Long> findByUID(long userId) {
		return queryForLongList(dsBmsQry, "SELECT employee_id FROM t_erp_user_employee WHERE user_id=?", new Object[] { userId });
	}
	
	@SuppressWarnings("unchecked")
	public List<UserEmployee> findAll() {
		return queryForList(dsBmsQry, "SELECT * FROM t_erp_user_employee order by user_id, employee_id", new Object[] {}, UserEmployee.class);
	}
}