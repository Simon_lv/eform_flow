package com.baplay.dao.impl;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import com.baplay.constant.Flow;
import com.baplay.dao.IWorkFlowDetailDao;
import com.baplay.entity.WorkFlowDetail;

@Repository
public class WorkFlowDetailDaoImpl extends BaseDao<WorkFlowDetail> implements IWorkFlowDetailDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDetailDao#save(com.baplay.entity.WorkFlowDetail)
	 */
	@Override
	public WorkFlowDetail save(WorkFlowDetail workFlowDetail) {
		String processTimeValue = workFlowDetail.getStatus() == Flow.WAITTING.getValue() ? "NULL" : "NOW()";
		String sql = "INSERT INTO `work_flow_detail` (`work_flow_id`, `step_id`, `creator`, `create_time`, `status`, `owner_id`, `process_time`, `memo`, `department_id`, `countersign_role`) VALUES (?, ?, ?, NOW(), ?, ?, " + processTimeValue + ", ?, ?, ?)";
		return (WorkFlowDetail) super.addForObject(dsBmsUpd, sql, workFlowDetail, 
				new Object[]{workFlowDetail.getWorkFlowId(), workFlowDetail.getStepId(), workFlowDetail.getCreator(), workFlowDetail.getStatus(), workFlowDetail.getOwnerId(), workFlowDetail.getMemo(), workFlowDetail.getDepartmentId(), workFlowDetail.getCountersignRole()});
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDetailDao#delete(long)
	 */
	@Override
	public int delete(long work_flow_id) {
		String sql = "DELETE FROM `work_flow_detail` WHERE `work_flow_id`=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{work_flow_id});
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDetailDao#findOneById(java.lang.Long)
	 */
	@Override
	public WorkFlowDetail findOneById(long id) {
		String sql = "SELECT wfd.*, IFNULL(fs.step_type, -1) step_type, IFNULL(fs.iteration, 1) iteration, fs.countersign, wfd.countersign_role FROM `work_flow_detail` wfd LEFT JOIN flow_step fs ON fs.id=wfd.step_id WHERE wfd.id=?";
		Object[] args = new Object[]{id};
		
		WorkFlowDetail entity = (WorkFlowDetail) super.queryForObject(dsBmsQry, sql, args, WorkFlowDetail.class);
		return entity;
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDetailDao#findLastOneById(long)
	 */
	@Override
	public WorkFlowDetail findLastOneById(long workFlowId) {
		String sql = "SELECT * FROM `work_flow_detail` WHERE work_flow_id=? ORDER BY create_time DESC LIMIT 1";
		Object[] args = new Object[]{workFlowId};
		
		WorkFlowDetail entity = (WorkFlowDetail) super.queryForObject(dsBmsQry, sql, args, WorkFlowDetail.class);
		return entity;
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDetailDao#process(long, long)
	 */
	@Override
	public int process(long id, long userId) {		
		String sql = "UPDATE `work_flow_detail` SET `status`=?, `owner_id`=?, `process_time`=NOW() WHERE `id`=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{Flow.LOCK.getValue(), userId, id});		
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDetailDao#abort(long)
	 */
	@Override
	public int abort(long id) {	
		String sql = "UPDATE `work_flow_detail` SET `status`=?, `owner_id`=NULL, `process_time`=NULL WHERE `id`=? and `status`=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{Flow.WAITTING.getValue(), id, Flow.LOCK.getValue()});
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDetailDao#close(long)
	 */
	@Override
	public int close(long id) {		
		String sql = "UPDATE `work_flow_detail` SET `status`=?, `process_time`=NOW() WHERE `id`=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{Flow.NORMAL_CLOSED.getValue(), id});		
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDetailDao#reject(long, long, java.lang.String)
	 */
	public int reject(long id, long userId, String memo) {
		String sql = "UPDATE `work_flow_detail` SET `status`=?, `process_time`=NOW(), `memo`=? WHERE `id`=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{Flow.REJECT.getValue(), memo, id});
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDetailDao#terminate(long, long, java.lang.String)
	 */
	public int terminate(long id, long userId, String memo) {
		String sql = "UPDATE `work_flow_detail` SET `status`=?, `process_time`=NOW(), `memo`=? WHERE `id`=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{Flow.USER_TERMINATE.getValue(), memo, id});
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDetailDao#terminate(com.baplay.entity.WorkFlowDetail)
	 */
	@Override
	public WorkFlowDetail terminate(WorkFlowDetail workFlowDetail) {
		String sql = "INSERT INTO `work_flow_detail` (`work_flow_id`, `step_id`, `creator`, `create_time`, `status`, `owner_id`, `process_time`, `memo`, `department_id`, `countersign_role`) VALUES (?, ?, ?, NOW(), ?, ?, NOW(), ?, ?, ?)";
		return (WorkFlowDetail) super.addForObject(dsBmsUpd, sql, workFlowDetail, 
				new Object[]{workFlowDetail.getWorkFlowId(), workFlowDetail.getStepId(), workFlowDetail.getCreator(), workFlowDetail.getStatus(), workFlowDetail.getOwnerId(), workFlowDetail.getMemo(), workFlowDetail.getDepartmentId(), workFlowDetail.getCountersignRole()});
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDetailDao#illegal(long)
	 */
	public boolean illegal(long id) {
		String sql = "SELECT count(*) FROM `work_flow_detail` WHERE `id`>? AND work_flow_id=(SELECT `work_flow_id` FROM `work_flow_detail` WHERE id=?)";
		return super.queryForInt(dsBmsQry, sql, new Object[]{id, id}) > 0;
	}
}