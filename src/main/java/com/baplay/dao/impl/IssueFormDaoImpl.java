package com.baplay.dao.impl;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import com.baplay.dao.IIssueFormDao;
import com.baplay.entity.IssueForm;
import com.baplay.form.IssueFormForm;

@Repository
public class IssueFormDaoImpl extends BaseDao<IssueForm> implements IIssueFormDao {
	
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public IssueForm add(IssueForm issueForm) {
		String sql = "INSERT INTO `issue_form` (`company_id`, `request_date`, `request_department`, `serial_no`, `issue_title`, `game_data_id`, `level`, `issue_start_time`, `issue_feedback_time`, `issue_feedback_operator`, `issue_feedback_channel`, `issue_feedback_description`, `work_flow_id`) "
				+ "VALUES (?, NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		return (IssueForm) super.addForObject(dsBmsUpd, sql, issueForm, 
				new Object[]{ issueForm.getCompanyId(), issueForm.getRequestDepartment(),
						issueForm.getSerialNo(), issueForm.getIssueTitle(), issueForm.getGameDataId(), issueForm.getLevel(),
						issueForm.getIssueStartTime(), issueForm.getIssueFeedbackTime(),
						issueForm.getIssueFeedbackOperator(), issueForm.getIssueFeedbackChannel(), issueForm.getIssueFeedbackDescription(), 
						/*issueForm.getMemo(), issueForm.getMemoOperator(),*/ issueForm.getWorkFlowId() });
	}

	@Override
	public int update(IssueForm issueForm) {
		String sql = "UPDATE `issue_form` SET `company_id`=?, `request_department`=?, `serial_no`=?, `issue_title`=?, `game_data_id`=?, `level`=?, `issue_start_time`=?, `issue_feedback_time`=?, `issue_feedback_channel`=?, `issue_feedback_description`=?, `repair_time`=?, `repair_feedback_time`=?, `repair_feedback_operator`=?, `repair_feedback_channel`=?, `accountablity`=?, `owner`=?, `issue_description`=?, `repair_description`=?, `prevention_description`=?, `issue_type`=?, `issue_level`=?, `prevention_level`=? WHERE `id`=?";
		return super.updateForObject(dsBmsUpd, sql, 
				new Object[]{ issueForm.getCompanyId(), issueForm.getRequestDepartment(),
						issueForm.getSerialNo(), issueForm.getIssueTitle(), issueForm.getGameDataId(), issueForm.getLevel(), 
						issueForm.getIssueStartTime(), issueForm.getIssueFeedbackTime(),
						issueForm.getIssueFeedbackChannel(), issueForm.getIssueFeedbackDescription(), 
						issueForm.getRepairTime(), issueForm.getRepairFeedbackTime(), issueForm.getRepairFeedbackOperator(), 
						issueForm.getRepairFeedbackChannel(), issueForm.getAccountablity(), issueForm.getOwner(),
						issueForm.getIssueDescription(), issueForm.getRepairDescription(), issueForm.getPreventionDescription(), 
						issueForm.getIssueType(), issueForm.getIssueLevel(), issueForm.getPreventionLevel(), issueForm.getId() });
	}

	@Override
	public IssueForm findOneById(Long id) {
		String sql = "SELECT a.*, (SELECT status FROM work_flow WHERE id = a.work_flow_id) AS status, (SELECT name FROM company where id = a.company_id) as companyName, "
				+ "(SELECT name FROM department where id = a.request_department) as departmentName, "
				+ "(select name from employee where id = (select employee_id from t_erp_user where id = (select creator from work_flow where id = a.work_flow_id))) as creatorName, "
				+ "(SELECT name FROM game_data where id=a.game_data_id) game_name, "
				+ "(select name from employee where id = (select employee_id from t_erp_user where id = (select creator from work_flow_detail where work_flow_id=a.work_flow_id and step_id=-1 and stamp_status=1 order by process_time desc limit 1))) modifier_name "
				+ "FROM issue_form a WHERE a.id=?";
		Object[] args = new Object[]{id};
		return (IssueForm) super.queryForObject(dsBmsQry, sql, args, IssueForm.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Page<IssueForm> list(IssueFormForm issueFormForm) {
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		
		// Auth
		param.add(issueFormForm.getLoginUser());
		param.add(issueFormForm.getFormType());
		param.add(issueFormForm.getLoginUser());
		param.add(issueFormForm.getLoginUser());
		
		if(StringUtils.isNotBlank(issueFormForm.getIssueTitle())){
			sb.append(" AND b.issue_title like '%").append(issueFormForm.getIssueTitle()).append("%'");			
		}
		if(issueFormForm.getCompanyId() != null) {
			sb.append(" AND b.company_id = ? ");
			param.add(issueFormForm.getCompanyId());
		}
		if(issueFormForm.getGameDataId() != 0){
			sb.append(" AND b.game_data_id = ? ");
			param.add(issueFormForm.getGameDataId());
		}		
		if(StringUtils.isNotBlank(issueFormForm.getSerialNo())){
			sb.append(" AND b.serial_no = ? ");
			param.add(issueFormForm.getSerialNo());
		}
		if(StringUtils.isNotBlank(issueFormForm.getIssueTimeStart())){		
			sb.append(" AND STR_TO_DATE(b.issue_start_time, '%Y-%m-%d') >= ? ");
			param.add(issueFormForm.getIssueTimeStart());
		}
		if(StringUtils.isNotBlank(issueFormForm.getIssueTimeEnd())){
			sb.append(" AND STR_TO_DATE(b.issue_start_time, '%Y-%m-%d') <= ? ");
			param.add(issueFormForm.getIssueTimeEnd());
		}
		if(StringUtils.isNotBlank(issueFormForm.getRepairDateStart())){		
			sb.append(" AND STR_TO_DATE(b.repair_time, '%Y-%m-%d') >= ? ");
			param.add(issueFormForm.getRepairDateStart());
		}
		if(StringUtils.isNotBlank(issueFormForm.getRepairDateEnd())){
			sb.append(" AND STR_TO_DATE(b.repair_time, '%Y-%m-%d') <= ? ");
			param.add(issueFormForm.getRepairDateEnd());
		}
		if(issueFormForm.getStatus() != null){
			sb.append(" AND b.status = ? ");
			param.add(issueFormForm.getStatus());
		}
		
		// Auth
		String userFormRole = StringUtils.join(issueFormForm.getFormRole(), "','");
		
		StringBuilder sql = new StringBuilder("SELECT b.*, (SELECT memo FROM issue_memo WHERE issue_form_id=b.id ORDER BY create_time DESC LIMIT 1) memo FROM (SELECT a.*, (SELECT COUNT(*) FROM form_auth WHERE form_type='IS' and form_role IN (SELECT id FROM form_role WHERE `name` IN('" + userFormRole + "'))) as auth, ");
		sql.append("(SELECT status FROM work_flow WHERE id = a.work_flow_id) AS status, (SELECT name FROM company where id = a.company_id) as companyName, (SELECT name FROM department where id = a.request_department) as departmentName, ");
		sql.append("(select creator from work_flow where id = a.work_flow_id) as creator, (SELECT name FROM employee WHERE id = (select employee_id from t_erp_user where id = a.issue_feedback_operator)) as issueOperatorName, (SELECT name FROM employee WHERE id = a.repair_feedback_operator) as repairOperatorName, (SELECT `name` FROM game_data WHERE id=a.game_data_id) game_name ");		
		sql.append("FROM issue_form a ) as b ");
		sql.append("left join (select work_flow_id, owner_id from work_flow_detail where owner_id=? and work_flow_id in (select id from work_flow where LEFT(form_type, 2)=?) group by work_flow_id) wfd on wfd.work_flow_id=b.work_flow_id ").append("WHERE (auth>=1 OR creator=? OR owner_id=?)").append(sb.toString());
		StringBuilder countSql = new StringBuilder("select count(*) from (").append(sql).append(") x");
		
		sql.append(" ORDER BY issue_start_time desc");
				
		if(!issueFormForm.isExport()) {
			sql.append(" LIMIT ").append((issueFormForm.getPageNo()-1)*issueFormForm.getPageSize()).append(",").append(issueFormForm.getPageSize());
		}
		
		Page<IssueForm> page = new Page<IssueForm>(issueFormForm.getPageSize());
				
		page = (Page<IssueForm>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(), 
				new BeanPropertyRowMapper<IssueForm>(IssueForm.class));
		
		if(!issueFormForm.isExport()) {
			page.setPageNo(issueFormForm.getPageNo());
		}
		
		return page;
	}

	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM `issue_form` WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}

	@Override
	public IssueForm findOneByFlow(Long workFlowId) {
		String sql = "SELECT a.*, (SELECT status FROM work_flow WHERE id = a.work_flow_id) AS status, (SELECT name FROM company where id = a.company_id) as companyName, (SELECT name FROM department where id = a.request_department) as departmentName, (select name from employee where id = (select employee_id from t_erp_user where id = (select creator from work_flow where id = a.work_flow_id))) as creatorName, (SELECT name FROM game_data where id=a.game_data_id) game_name FROM issue_form a WHERE a.work_flow_id=?";
		Object[] args = new Object[]{workFlowId};
		return (IssueForm) super.queryForObject(dsBmsQry, sql, args, IssueForm.class);
	}
	
//	@Override
//	public int memo(Long id, Long uid, String memo) {
//		String sql = "UPDATE `issue_form` SET memo=?, memo_operator=? WHERE `id`=?";
//		return super.updateForObject(dsBmsUpd, sql, new Object[]{memo, uid, id});
//	}
}