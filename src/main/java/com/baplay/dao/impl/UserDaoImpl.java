/**
 * 
 */
package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.baplay.dao.IUserDao;
import com.baplay.entity.FormUserRole;
import com.baplay.entity.TEfunuser;
import com.baplay.entity.UserRole;
import com.baplay.form.UserForm;
import com.google.common.base.Strings;

/**
 * @author Haozi
 *
 */
@Repository
public class UserDaoImpl extends BaseDao<TEfunuser> implements IUserDao {
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IUserDao#addUser(com.baplay.form.UserForm)
	 */
	@Override
	public TEfunuser addUser(UserForm form) {
		
		if(form.isDuplicateCheck()) {
			TEfunuser old = (TEfunuser) this.queryForObject(dsBmsQry, "select * from t_erp_user where login_account = ?", new Object[] { form.getLoginAccount() },
					TEfunuser.class);	
			if (old != null) {			
				return old;
			}
		}
		
		TEfunuser user = new TEfunuser();	
		user.setLoginAccount(form.getLoginAccount());
		user.setStatus(1);
		user.setCreator(form.getCreator());
		user.setPassword(form.getPassword());
		user = (TEfunuser) this.addForParam(dsBmsUpd, "INSERT INTO `t_erp_user` (login_account, status, creator, create_time, password) VALUES (?,?,?,NOW(),?)", user,
				new String[] { "loginAccount", "status", "creator", "password" });		
		return user;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IUserDao#addUserRole(Long, java.util.List)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void addUserRole(Long userId, List<Long> roleId) {
		List<UserRole> old = this.queryForList(dsBmsQry, "select * from t_erp_user_role where user_id = ? AND status = 1", new Object[] { userId },
				new BeanPropertyRowMapper<UserRole>(UserRole.class));
		int emptyCnt = 0;
		for(Long id : roleId) {		
			if(id == null) {
				emptyCnt++;
			}
		}	
		
		if(emptyCnt != roleId.size()) {
			for(int i = 0; i < old.size(); i++) {
				this.updateForObject(dsBmsUpd, "update t_erp_user_role set status = ? where id=?", new Object[] { 2, old.get(i).getId() });
			}
		}
		
		UserRole ur = new UserRole();
		for (int i = 0; i < roleId.size(); i++) {		
			if(roleId.get(i) == null){
				continue;
			}
			
			ur.setRoleId(roleId.get(i));
			ur.setUserId(userId);
			this.addForParam(dsBmsUpd, "insert into t_erp_user_role (role_id,user_id,status) values (?,?,1)", ur, new String[] { "roleId", "userId" });
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void addUserFormRole(Long userId, List<Long> formRoleId, int formRoleDeptLimit) {		
		List<UserRole> old = this.queryForList(dsBmsQry, "select * from form_user_role where user_id = ? AND status = 1", new Object[] { userId },
				new BeanPropertyRowMapper<UserRole>(UserRole.class));
		int emptyCnt = 0;
		for(Long id : formRoleId) {		
			if(id == null) {
				emptyCnt++;
			}
		}	
		// Delete history
		if(emptyCnt != formRoleId.size()) {
			for (int i = 0; i < old.size(); i++) {
				this.updateForObject(dsBmsUpd, "update form_user_role set status = ? where id=?", new Object[] { 2, old.get(i).getId() });
			}
		}
		
		UserRole ur = new UserRole();
		for (int i = 0; i < formRoleId.size(); i++) {		
			if(formRoleId.get(i) == null){
				continue;
			}
			
			ur.setRoleId(formRoleId.get(i));
			ur.setUserId(userId);
			this.addForParam(dsBmsUpd, "insert into form_user_role (role_id,user_id,same_department,status) values (?,?," + formRoleDeptLimit + ",1)", ur, new String[] { "roleId", "userId" });
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IUserDao#updateUser(com.baplay.form.UserForm)
	 */
	@Override
	public void updateUser(UserForm form) {
		StringBuilder sql = new StringBuilder("UPDATE `t_erp_user` u SET u.`login_account`=? [,u.`password`=?] WHERE u.`id`=?");
		ArrayList<Object> param = new ArrayList<Object>();
		param.add(form.getLoginAccount());
		param.add(form.getPassword());
		param.add(form.getId());
		
		this.updateForObject(dsBmsUpd, sql, param);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IUserDao#updatePwd(Long, String)
	 */
	@Override
	public void updatePwd(Long userId, String password) {
		String sql = "UPDATE `t_erp_user` u SET u.`password`=? WHERE u.`id`=?";
		this.updateForObject(dsBmsUpd, sql, new Object[] { password, userId });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IUserDao#updateUserStatus(Long, Integer)
	 */
	@Override
	public void updateUserStatus(Long userId, Integer status) {
		String sql = "UPDATE `t_erp_user` u SET u.`status`=? WHERE u.`id`=?";
		this.updateForObject(dsBmsUpd, sql, new Object[] { status, userId });
	}

	@Override
	public TEfunuser findByUserName(String userName) {
		String sql = "select a.*, (select name from employee where id = a.employee_id ) as userName from t_erp_user a where login_account=? and status = 1 LIMIT 1";
		return (TEfunuser) this.queryForObject(dsBmsQry, sql, new Object[] { userName }, new BeanPropertyRowMapper<TEfunuser>(TEfunuser.class));
	}

	@SuppressWarnings("unchecked")
	@Override
	public UserForm findById(Long id) {
		String sql = "select u.* from t_erp_user u where u.id = ? and u.`status` = 1 LIMIT 1";
		UserForm user = (UserForm) this.queryForObject(dsBmsQry, sql, new Object[] { id }, new BeanPropertyRowMapper<UserForm>(UserForm.class));
		
		List<Long> roleIds = this.queryForLongList(dsBmsQry, "select role_id from t_erp_user_role where user_id = ? and status = 1", new Object[] { id });
		List<FormUserRole> formRoleList = this.queryForList(dsBmsQry, "select * from form_user_role where user_id = ? and status = 1", new Object[] { id }, new BeanPropertyRowMapper<FormUserRole>(FormUserRole.class));
		if (roleIds != null) {
			if (roleIds.size() > 0) {
				user.setRoleId1(roleIds.get(0));
			}
			if (roleIds.size() > 1) {
				user.setRoleId2(roleIds.get(1));
			}
		}
		
		if(formRoleList != null){			
			if(formRoleList.size() > 0){
				user.setFormRoleId1(formRoleList.get(0).getRoleId());
			}
			if(formRoleList.size() > 1){
				user.setFormRoleId2(formRoleList.get(1).getRoleId());
			}
			if(formRoleList.size() > 2){
				user.setFormRoleId3(formRoleList.get(2).getRoleId());
			}
			if(formRoleList.size() > 3){
				user.setFormRoleId4(formRoleList.get(3).getRoleId());
			}
			if(formRoleList.size() > 4){
				user.setFormRoleId5(formRoleList.get(4).getRoleId());
			}
			if(formRoleList.size() > 5){
				user.setFormRoleId6(formRoleList.get(5).getRoleId());
			}
			
			boolean deptLimit = formRoleList.stream().anyMatch(role -> role.getSameDepartment() == 2);
			user.setFormRoleDeptLimit(deptLimit ? 2 : 1);
		}				
		
		return user;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TEfunuser> getAllUser(String userName) {
		if (Strings.isNullOrEmpty(userName)) {
			return this
					.queryForList(
							dsBmsQry,
							"select u.id, (select count(*) from form_user_role where role_id=(select id from form_role where `status`=1 and `name`='部門主管') and user_id = u.id and `status`=1) supervisor, (select name from employee where id = u.employee_id ) as userName, GROUP_CONCAT(r.`name`) 'name', u.login_account,u.`status`,u.employee_id from t_erp_user u LEFT JOIN t_erp_user_role ur ON u.id = ur.user_id AND ur.`status` = 1 LEFT JOIN t_erp_role r ON ur.role_id = r.id GROUP BY u.id",
							null, TEfunuser.class);
		} else {
			return this
					.queryForList(
							dsBmsQry,
							"select u.id, (select count(*) from form_user_role where role_id=(select id from form_role where `status`=1 and `name`='部門主管') and user_id = u.id and `status`=1) supervisor, (select name from employee where id = u.employee_id ) as userName, GROUP_CONCAT(r.`name`) 'name', u.login_account,u.`status`,u.employee_id from t_erp_user u LEFT JOIN t_erp_user_role ur ON u.id = ur.user_id AND ur.`status` = 1 LEFT JOIN t_erp_role r ON ur.role_id = r.id WHERE u.login_account = ? GROUP BY u.id", 
							new Object[] { userName }, TEfunuser.class);
		}
	}

	@Override
	public Long findByEmployeeId(Long employeeId) {
		return this.queryForLong(dsBmsQry, "select id from t_erp_user where employee_id=?", new Object[]{employeeId});
	}	

	/* (non-Javadoc)
	 * @see com.baplay.dao.IUserDao#updateEmployeeId(long, long)
	 */
	@Override
	public int updateEmployeeId(long employeeId, long userId) {
		String sql = "UPDATE `t_erp_user` u SET u.`employee_id`=? WHERE u.`id`=?";
		return updateForObject(dsBmsUpd, sql, new Object[] { employeeId, userId });
	}
}