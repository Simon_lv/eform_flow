package com.baplay.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import com.baplay.dao.IBudgetFormDetailDao;
import com.baplay.entity.BudgetFormDetail;

@Repository
public class BudgetFormDetailDaoImpl extends BaseDao<BudgetFormDetail> implements IBudgetFormDetailDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public BudgetFormDetail add(BudgetFormDetail budgetFormDetail) {
		String sql = "INSERT INTO `budget_form_detail` (`budget_form_id`, `item_title`, `item_description`, `amount`, `additional`) VALUES (?, ?, ?, ?, ?)";
		return (BudgetFormDetail) super.addForObject(dsBmsUpd, sql, budgetFormDetail, 
				new Object[]{ budgetFormDetail.getBudgetFormId(), budgetFormDetail.getItemTitle()
						, budgetFormDetail.getItemDescription(), budgetFormDetail.getAmount(),
						budgetFormDetail.getAdditional() });
	}

	@Override
	public int deleteAll(Long budgetFormId) {
		String sql = "DELETE FROM `budget_form_detail` WHERE (`budget_form_id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{budgetFormId});
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BudgetFormDetail> findFormDetail(Long budgetFormId, int additional) {
		String sql = "SELECT * FROM `budget_form_detail` WHERE (`budget_form_id`=? and `additional`=?)";
		return this.queryForList(dsBmsQry, sql, new Object[]{budgetFormId, additional}, BudgetFormDetail.class);
	}

	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM `budget_form_detail` WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}

}
