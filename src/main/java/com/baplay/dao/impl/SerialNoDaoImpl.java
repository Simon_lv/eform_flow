package com.baplay.dao.impl;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import com.baplay.dao.ISerialNoDao;

@Repository
public class SerialNoDaoImpl extends BaseDao<Object> implements ISerialNoDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public String findSerialNo(String formCode) {		
		return this.queryForString(dsBmsQry, "SELECT nextval(?) as serialNo", new Object[]{formCode});
	}

}
