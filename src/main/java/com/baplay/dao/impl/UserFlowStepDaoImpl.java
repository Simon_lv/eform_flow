package com.baplay.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import com.baplay.constant.Flow;
import com.baplay.dao.IUserFlowStepDao;
import com.baplay.entity.UserFlowStep;

@Repository
public class UserFlowStepDaoImpl extends BaseDao<UserFlowStep> implements IUserFlowStepDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IUserFlowStepDao#findOneByStep(java.lang.Long)
	 */
	@Override
	public UserFlowStep findOneByStep(Long workFlowId, Long stepId, Long userId) {
		String sql = "SELECT * FROM `user_flow_step` WHERE work_flow_id=? AND step_id=? AND user_id=? AND `status`=1";
		Object[] args = new Object[]{workFlowId, stepId, userId};
		
		UserFlowStep entity = (UserFlowStep) super.queryForObject(dsBmsQry, sql, args, UserFlowStep.class);
		return entity;
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IUserFlowStepDao#findOneByStep(java.lang.Long, java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<UserFlowStep> findByStep(Long workFlowId, Long stepId) {
		String sql = "SELECT * FROM `user_flow_step` WHERE work_flow_id=? AND step_id=? AND `status`=1";
		Object[] args = new Object[]{workFlowId, stepId};
		
		return super.queryForList(dsBmsQry, sql, args, UserFlowStep.class);		
	}

	/* (non-Javadoc)
	 * @see com.baplay.dao.IUserFlowStepDao#add(com.baplay.entity.UserFlowStep)
	 */
	@Override
	public UserFlowStep add(UserFlowStep userFlowStep) {
		String sql = "INSERT INTO `user_flow_step` (`work_flow_id`, `step_id`, `user_id`) VALUES (?, ?, ?)";
		return (UserFlowStep) super.addForObject(dsBmsUpd, sql, userFlowStep, 
				new Object[]{ userFlowStep.getWorkFlowId(), userFlowStep.getStepId(), userFlowStep.getUserId() });
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IUserFlowStepDao#close(long, long, long)
	 */
	@Override
	public int close(long workFlowId, long stepId, long userId) {
		String sql = "UPDATE `user_flow_step` SET `status`=? WHERE work_flow_id=? AND step_id=? AND user_id=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{
				Flow.NORMAL_CLOSED.getValue(), workFlowId, stepId, userId});
	}
}