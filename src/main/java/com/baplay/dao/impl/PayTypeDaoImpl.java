package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import com.baplay.dao.IPayTypeDao;
import com.baplay.entity.PayType;
import com.baplay.form.PayTypeForm;

@Repository
public class PayTypeDaoImpl extends BaseDao<PayType> implements IPayTypeDao {
	
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}

	@Override
	public PayType findOneById(Long id) {
		String sql = "SELECT * FROM `pay_type` WHERE id=?";
		Object[] args = new Object[]{id};
		
		PayType entity = (PayType) super.queryForObject(dsBmsQry, sql, args, PayType.class);
		return entity;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<PayType> findAllPayType() {
		String sql = "SELECT * FROM pay_type WHERE `status`=1";
		return super.queryForList(dsBmsQry, sql, new Object[]{}, PayType.class);
	}

	@Override
	public PayType add(PayType payType) {		
		String sql = "INSERT INTO `pay_type` (`name`, `status`) VALUES (?, ?)";
		return (PayType) super.addForObject(dsBmsUpd, sql, payType, 
				new Object[]{ payType.getName(), payType.getStatus() });
	}

	@Override
	public int update(PayType payType) {		
		String sql = "UPDATE `pay_type` SET `name`=?, `status`=? WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, 
				new Object[]{ payType.getName(), payType.getStatus(), payType.getId() });
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<PayType> list(PayTypeForm payTypeForm) {		
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		
		if(StringUtils.isNotBlank(payTypeForm.getName())){
			sb.append(" and name like ?");
			param.add("%"+payTypeForm.getName()+"%");
		}
		
		StringBuilder sql = new StringBuilder("SELECT * FROM pay_type WHERE 1=1").append(sb.toString());
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM pay_type WHERE 1=1").append(sb.toString());		
		
		if(!payTypeForm.isExport()) {
			sql.append(" LIMIT ").append((payTypeForm.getPageNo()-1)*payTypeForm.getPageSize()).append(",").append(payTypeForm.getPageSize());
		}
		
		Page<PayType> page = new Page<PayType>(payTypeForm.getPageSize());
		page = (Page<PayType>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(), 
				new BeanPropertyRowMapper<PayType>(PayType.class));
		page.setPageNo(payTypeForm.getPageNo());
		return page;
	}

	@Override
	public int delete(Long id) {
		String sql = "UPDATE `pay_type` SET `status`=2 WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}
}