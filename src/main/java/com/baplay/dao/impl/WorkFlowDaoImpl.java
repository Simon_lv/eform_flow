package com.baplay.dao.impl;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import com.baplay.constant.Flow;
import com.baplay.dao.IWorkFlowDao;
import com.baplay.entity.WorkFlow;

@Repository
public class WorkFlowDaoImpl extends BaseDao<WorkFlow> implements IWorkFlowDao {
	
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}

	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDao#save(com.baplay.entity.WorkFlow)
	 */
	@Override
	public WorkFlow save(WorkFlow workFlow) {
		String sql = "INSERT INTO `work_flow` (`company_id`, `department_id`, `serial_no`, `flow_schema_id`, `status`, `creator`, `create_time`, `form_type`, `step_status`, `step_owner`) VALUES (?, ?, ?, ?, ?, ?, NOW(), ?, ? ,?)";
		return (WorkFlow) super.addForObject(dsBmsUpd, sql, workFlow, 
				new Object[]{workFlow.getCompanyId(), workFlow.getDepartmentId(), workFlow.getSerialNo(), workFlow.getFlowSchemaId(), workFlow.getStatus(), workFlow.getCreator(), workFlow.getFormType(), workFlow.getStepStatus(), workFlow.getStepOwner()});
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDao#update(java.lang.Long, java.lang.Long)
	 */
	@Override
	public int update(Long id, Long formId) {
		String sql = "UPDATE `work_flow` SET form_id=? WHERE `id`=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{formId, id});
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDao#delete(long)
	 */
	@Override
	public int delete(long id) {
		String sql = "DELETE FROM `work_flow` WHERE `id`=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}
		
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDao#findOneById(java.lang.Long)
	 */
	@Override
	public WorkFlow findOneById(Long id) {
		String sql = "SELECT wf.*, (SELECT name FROM form WHERE code=substr(wf.form_type, 4) LIMIT 1) form_name FROM `work_flow` wf WHERE id=?";
		Object[] args = new Object[]{id};
		
		WorkFlow entity = (WorkFlow) super.queryForObject(dsBmsQry, sql, args, WorkFlow.class);
		return entity;
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDao#findOneByNo(java.lang.String)
	 */
	@Override
	public WorkFlow findOneByNo(String serialNo) {
		String sql = "SELECT wf.*, (SELECT name FROM form WHERE code=substr(wf.form_type, 4) LIMIT 1) form_name FROM `work_flow` wf WHERE serial_no=?";
		Object[] args = new Object[]{serialNo};
		
		WorkFlow entity = (WorkFlow) super.queryForObject(dsBmsQry, sql, args, WorkFlow.class);
		return entity;
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDao#process(long, long)
	 */
	@Override
	public int process(long id, long userId) {		
		String sql = "UPDATE `work_flow` SET `step_status`=?, `step_owner`=? WHERE `id`=? AND (`step_status`=? OR (`step_status`=? AND `step_owner`=?))";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{
				Flow.LOCK.getValue(), userId, id, Flow.WAITTING.getValue(),
				Flow.LOCK.getValue(), userId});		
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDao#next(long)
	 */
	@Override
	public int next(long id) {
		String sql = "UPDATE `work_flow` SET `step_status`=?, `step_owner`=NULL WHERE `id`=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{Flow.WAITTING.getValue(), id});
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDao#abort(long)
	 */
	@Override
	public int abort(long id) {
		String sql = "UPDATE `work_flow` SET `step_status`=?, `step_owner`=NULL WHERE `id`=? AND `step_status`=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{Flow.WAITTING.getValue(), id, Flow.LOCK.getValue()});
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDao#close(long, long)
	 */
	@Override
	public int close(long id, long userId) {
		String sql = "UPDATE `work_flow` SET `status`=?, `closer`=?, `close_time`=NOW(), `step_status`=?, `step_owner`=NULL WHERE `id`=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{
				Flow.NORMAL_CLOSED.getValue(), userId, Flow.NORMAL_CLOSED.getValue(), id});
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDao#submitDraft(long, long, java.lang.String)
	 */
	@Override
	public int submitDraft(long id, long flowSchemaId, String serialNo) {
		String sql = "UPDATE `work_flow` SET `flow_schema_id`=?, `status`=?, `serial_no`=?, `step_status`=?, `step_owner`=NULL WHERE `id`=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{
				flowSchemaId, Flow.RUNNING.getValue(), serialNo, Flow.WAITTING.getValue(), id});
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDao#reject(long)
	 */
	@Override
	public int reject(long id) {
		String sql = "UPDATE `work_flow` SET `status`=?, `step_status`=?, `step_owner`=NULL WHERE `id`=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{
				Flow.REJECT.getValue(), Flow.REJECT.getValue(), id});
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDao#terminate(long)
	 */
	@Override
	public int terminate(long id) {
		String sql = "UPDATE `work_flow` SET `status`=?, `step_status`=?, `step_owner`=NULL WHERE `id`=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{
				Flow.USER_TERMINATE.getValue(), Flow.USER_TERMINATE.getValue(), id});
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.dao.IWorkFlowDao#update(com.baplay.entity.WorkFlow)
	 */
	@Override
	public int update(WorkFlow workFlow) {
		String sql = "UPDATE `work_flow` SET `flow_schema_id`=?, `status`=?, `step_status`=?, `step_owner`=? WHERE `id`=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{
				workFlow.getFlowSchemaId(), workFlow.getStatus(), workFlow.getStepStatus(), workFlow.getStepOwner(), workFlow.getId()});
	}
}