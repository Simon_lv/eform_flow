package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import com.baplay.dao.IRefundFormDao;
import com.baplay.entity.RefundForm;
import com.baplay.form.RefundFormForm;

@Repository
public class RefundFormDaoImpl extends BaseDao<RefundForm> implements IRefundFormDao {
	
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public RefundForm add(RefundForm refundForm) {
		String sql = "INSERT INTO `refund_form` (`company_id`, `request_date`, `serial_no`, `remit_date`, `subject`, `reason`, `game_data_id`, `game_account`, `pay_time`, `order_id`, `tw_coin`, `pay_type_id`, `user_name`, `user_id`, `contact_tel`, `contact_address`, `payment_way`, `remark`, `notify_agency`, `ops_process`, `ops_process_memo`, `fin_process`, `fin_process_memo`, `receipt_no`, `acct_process`, `acct_process_memo`, `work_flow_id`) VALUES (?, NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		return (RefundForm) super.addForObject(dsBmsUpd, sql, refundForm, 
				new Object[]{ refundForm.getCompanyId(), refundForm.getSerialNo(), refundForm.getRemitDate(), refundForm.getSubject(),
						refundForm.getReason(), refundForm.getGameDataId(), refundForm.getGameAccount(),
						refundForm.getPayTime(), refundForm.getOrderId(), refundForm.getTwCoin(),
						refundForm.getPayTypeId(), refundForm.getUserName(), refundForm.getUserId(),
						refundForm.getContactTel(), refundForm.getContactAddress(),
						refundForm.getPaymentWay(), refundForm.getRemark(), refundForm.getNotifyAgency(),
						refundForm.getOpsProcess(),refundForm.getOpsProcessMemo(),
						refundForm.getFinProcess(), refundForm.getFinProcessMemo(),
						refundForm.getReceiptNo(), refundForm.getAcctProcess(), refundForm.getAcctProcessMemo(),
						refundForm.getWorkFlowId() });
	}

	@Override
	public int update(RefundForm refundForm) {
		String sql = "UPDATE `refund_form` SET `company_id`=?, `serial_no`=?, `remit_date`=?, `subject`=?, `reason`=?, `game_data_id`=?, `game_account`=?, `pay_time`=?, `order_id`=?, `tw_coin`=?, `pay_type_id`=?, `user_name`=?, `user_id`=?, `contact_tel`=?, `contact_address`=?, `payment_way`=?, `remark`=?, `notify_agency`=?, `ops_process`=?, `ops_process_memo`=?, `fin_process`=?, `fin_process_memo`=?, `receipt_no`=?, `acct_process`=?, `acct_process_memo`=? WHERE `id`=?";
		return super.updateForObject(dsBmsUpd, sql, 
				new Object[]{ refundForm.getCompanyId(),
						refundForm.getSerialNo(), refundForm.getRemitDate(), refundForm.getSubject(), refundForm.getReason(),
						refundForm.getGameDataId(), refundForm.getGameAccount(),
						refundForm.getPayTime(), refundForm.getOrderId(), refundForm.getTwCoin(),
						refundForm.getPayTypeId(), refundForm.getUserName(), refundForm.getUserId(),
						refundForm.getContactTel(), refundForm.getContactAddress(),
						refundForm.getPaymentWay(), refundForm.getRemark(), refundForm.getNotifyAgency(),
						refundForm.getOpsProcess(), refundForm.getOpsProcessMemo(),
						refundForm.getFinProcess(), refundForm.getFinProcessMemo(),
						refundForm.getReceiptNo(), refundForm.getAcctProcess(), refundForm.getAcctProcessMemo(),
						refundForm.getId() });
	}

	@Override
	public RefundForm findOneById(Long id) {
		String sql = "SELECT a.*, (SELECT name from company where id = a.company_id) as companyName, (SELECT status FROM work_flow WHERE id = a.work_flow_id) AS status, (select name from employee where id = (select employee_id from t_erp_user where id = (select creator from work_flow where id = a.work_flow_id))) as creatorName, (SELECT name FROM game_data where id=a.game_data_id) game_name, (SELECT name FROM pay_type where id=a.pay_type_id) pay_type FROM refund_form a WHERE id=?";
		Object[] args = new Object[]{id};
		return (RefundForm) super.queryForObject(dsBmsQry, sql, args, RefundForm.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Page<RefundForm> list(RefundFormForm refundFormForm) {
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		
		// Auth
		param.add(refundFormForm.getLoginUser());
		param.add(refundFormForm.getFormType());
		param.add(refundFormForm.getLoginUser());
		param.add(refundFormForm.getLoginUser());
		
		if(StringUtils.isNotBlank(refundFormForm.getSubject())){
			sb.append(" AND b.subject like '%").append(refundFormForm.getSubject()).append("%'");			
		}
		if(refundFormForm.getCompanyId() != null){
			sb.append(" AND b.company_id = ? ");
			param.add(refundFormForm.getCompanyId());
		}		
		if(StringUtils.isNotBlank(refundFormForm.getSerialNo())){
			sb.append(" AND b.serial_no = ? ");
			param.add(refundFormForm.getSerialNo());
		}
		if(StringUtils.isNotBlank(refundFormForm.getRequestDateStart())){		
			sb.append(" AND STR_TO_DATE(b.request_date, '%Y-%m-%d') >= ? ");
			param.add(refundFormForm.getRequestDateStart());
		}
		if(StringUtils.isNotBlank(refundFormForm.getRequestDateEnd())){
			sb.append(" AND STR_TO_DATE(b.request_date, '%Y-%m-%d') <= ? ");
			param.add(refundFormForm.getRequestDateEnd());
		}
		if(StringUtils.isNotBlank(refundFormForm.getRemitDateStart())){		
			sb.append(" AND STR_TO_DATE(b.remit_date, '%Y-%m-%d') >= ? ");
			param.add(refundFormForm.getRemitDateStart());
		}
		if(StringUtils.isNotBlank(refundFormForm.getRemitDateEnd())){
			sb.append(" AND STR_TO_DATE(b.remit_date, '%Y-%m-%d') <= ? ");
			param.add(refundFormForm.getRemitDateEnd());
		}
		if(refundFormForm.getRemitted() != null){
			sb.append(" AND b.remitted = ? ");
			param.add(refundFormForm.getRemitted());
		}
		if(refundFormForm.getStatus() != null){
			sb.append(" AND b.status = ? ");
			param.add(refundFormForm.getStatus());
		}
		
		// Auth
		String userFormRole = StringUtils.join(refundFormForm.getFormRole(), "','");
		
		StringBuilder sql = new StringBuilder("SELECT b.* FROM (SELECT a.*, (SELECT COUNT(*) FROM form_auth WHERE form_type='RE' and form_role IN (SELECT id FROM form_role WHERE `name` IN('" + userFormRole + "'))) as auth, (SELECT name from company where id = a.company_id) as companyName, (SELECT status FROM work_flow WHERE id = a.work_flow_id) AS status, (select name from game_data where id = a.game_data_id) as gameName, (select name from pay_type where id = a.pay_type_id) as payType, (select creator from work_flow where id = a.work_flow_id) as creator, (SELECT `name` FROM employee WHERE id=(select employee_id from t_erp_user where id=(select creator from work_flow where id = a.work_flow_id))) as creatorName FROM refund_form a ) as b ");
		sql.append("left join (select work_flow_id, owner_id from work_flow_detail where owner_id=? and work_flow_id in (select id from work_flow where LEFT(form_type, 2)=?) group by work_flow_id) wfd on wfd.work_flow_id=b.work_flow_id ").append("WHERE (auth>=1 OR creator=? OR owner_id=?)").append(sb.toString());
		StringBuilder countSql = new StringBuilder("select count(*) from (").append(sql).append(") x");
				
		if(!refundFormForm.isExport()) {
			sql.append(" LIMIT ").append((refundFormForm.getPageNo()-1)*refundFormForm.getPageSize()).append(",").append(refundFormForm.getPageSize());
		}
		
		Page<RefundForm> page = new Page<RefundForm>(refundFormForm.getPageSize());

		page = (Page<RefundForm>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(), 
				new BeanPropertyRowMapper<RefundForm>(RefundForm.class));
		
		if(!refundFormForm.isExport()) {
			page.setPageNo(refundFormForm.getPageNo());
		}
		
		return page;
	}

	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM `refund_form` WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}

	@Override
	public RefundForm findOneByFlow(Long workFlowId) {
		String sql = "SELECT a.*, (SELECT name from company where id = a.company_id) as companyName, (SELECT status FROM work_flow WHERE id = a.work_flow_id) AS status, (select name from employee where id = (select employee_id from t_erp_user where id = (select creator from work_flow where id = a.work_flow_id))) as creatorName, (SELECT name FROM game_data where id=a.game_data_id) game_name, (SELECT name FROM pay_type where id=a.pay_type_id) pay_type FROM refund_form a WHERE work_flow_id=?";
		Object[] args = new Object[]{workFlowId};
		return (RefundForm) super.queryForObject(dsBmsQry, sql, args, RefundForm.class);
	}
	
	@Override
	public int confirmRemitDate(Long id) {
		String sql = "UPDATE `refund_form` SET `remitted`=2 WHERE `id`=? AND `remitted`=1";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}
	
	@Override
	public int updateRemitDate(Long id, Date remitDate) {
		String sql = "UPDATE `refund_form` SET `remit_date`=? WHERE `id`=? AND `remitted`=1";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{remitDate, id});
	}
}