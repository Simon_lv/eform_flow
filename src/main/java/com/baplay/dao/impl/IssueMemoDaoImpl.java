package com.baplay.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import com.baplay.dao.IIssueMemoDao;
import com.baplay.entity.IssueMemo;

@Repository
public class IssueMemoDaoImpl extends BaseDao<IssueMemo> implements IIssueMemoDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public IssueMemo add(IssueMemo issueMemo) {
		String sql = "INSERT INTO `issue_memo` (`issue_form_id`, `memo`, `creator`, `create_time`) VALUES (?, ?, ?, NOW())";
		return (IssueMemo) super.addForObject(dsBmsUpd, sql, issueMemo, 
				new Object[]{ issueMemo.getIssueFormId(), issueMemo.getMemo(), issueMemo.getCreator() });
	}

	@Override
	public int deleteAll(Long issueFormId) {
		String sql = "DELETE FROM `issue_memo` WHERE `issue_form_id`=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{issueFormId});
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<IssueMemo> findMemo(Long issueFormId) {
		String sql = "SELECT im.*, (SELECT `name` FROM employee WHERE id=(SELECT employee_id FROM t_erp_user WHERE id=im.creator)) creator_name FROM issue_memo im WHERE issue_form_id=?";
		return this.queryForList(dsBmsQry, sql, new Object[]{issueFormId}, IssueMemo.class);
	}
}