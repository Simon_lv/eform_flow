package com.baplay.dao.impl;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import com.baplay.dao.IBusinessTripFormDao;
import com.baplay.entity.BusinessTripForm;
import com.baplay.form.BusinessTripFormForm;

@Repository
public class BusinessTripFormDaoImpl extends BaseDao<BusinessTripForm> implements IBusinessTripFormDao {
	
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public BusinessTripForm add(BusinessTripForm businessTripForm) {
		String sql = "INSERT INTO `business_trip_form` (`company_id`, `request_department`, `request_date`, `serial_no`, `oversea`, `job_title_id`, `employee_id`, `trip_matter`, `trip_location`, `outbound_departure_time`, `outbound_flight`, `outbound_flight_no`, `outbound_country`, `outbound_city`, `outbound_landing_time`, `inbound_departure_time`, `inbound_flight`, `inbound_flight_no`, `inbound_country`, `inbound_city`, "
					+ " `inbound_landing_time`, `agent_passport`, `agent_visa`, `agent_mtp`, `estimated_cost_airfare`, `estimated_cost_agent`, `estimated_cost_lodging`, `estimated_cost_pickup`, `cash_advance`, `advance_currency`, `advance_amount`, `daily_allowance`, `memo`, `work_flow_id` ) VALUES (?, ?, NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		return (BusinessTripForm) super.addForObject(dsBmsUpd, sql, businessTripForm, 
				new Object[]{ businessTripForm.getCompanyId(), businessTripForm.getRequestDepartment(),
						businessTripForm.getSerialNo(), businessTripForm.getOversea(),
						businessTripForm.getJobTitleId(), businessTripForm.getEmployeeId(), businessTripForm.getTripMatter(),
						businessTripForm.getTripLocation(), businessTripForm.getOutboundDepartureTime(), businessTripForm.getOutboundFlight(),
						businessTripForm.getOutboundFlightNo(), businessTripForm.getOutboundCountry(), businessTripForm.getOutboundCity(),
						businessTripForm.getOutboundLandingTime(), businessTripForm.getInboundDepartureTime(), businessTripForm.getInboundFlight(),
						businessTripForm.getInboundFlightNo(),businessTripForm.getInboundCountry(), businessTripForm.getInboundCity(), businessTripForm.getInboundLandingTime(),
						businessTripForm.getAgentPassport(), businessTripForm.getAgentVisa(),
						businessTripForm.getAgentMtp(), businessTripForm.getEstimatedCostAirfare(), businessTripForm.getEstimatedCostAgent(),
						businessTripForm.getEstimatedCostLodging(), businessTripForm.getEstimatedCostPickup(), businessTripForm.getCashAdvance(),
						businessTripForm.getAdvanceCurrency(), businessTripForm.getAdvanceAmount(), businessTripForm.getDailyAllowance(),
						businessTripForm.getMemo(), businessTripForm.getWorkFlowId() });
	}

	@Override
	public int update(BusinessTripForm businessTripForm) {
		String sql = "UPDATE `business_trip_form` SET `company_id`=?, `request_department`=?, `job_title_id`=?, `employee_id`=?, `serial_no`=?, `oversea`=?, `trip_matter`=?, `trip_location`=?, `outbound_departure_time`=?, `outbound_flight`=?, `outbound_flight_no`=?, `outbound_country`=?, `outbound_city`=?, `outbound_landing_time`=?, `inbound_departure_time`=?, `inbound_flight`=?, `inbound_flight_no`=?, `inbound_country`=?, `inbound_city`=?, "
					+ " `inbound_landing_time`=?, `agent_passport`=?, `agent_visa`=?, `agent_mtp`=?, `estimated_cost_airfare`=?, `estimated_cost_agent`=?, `estimated_cost_lodging`=?, `estimated_cost_pickup`=?, `cash_advance`=?, `advance_currency`=?, `advance_amount`=?, `daily_allowance`=?, `memo`=? WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, 
				new Object[]{ businessTripForm.getCompanyId(), businessTripForm.getRequestDepartment(), businessTripForm.getJobTitleId(), businessTripForm.getEmployeeId(),
						businessTripForm.getSerialNo(), businessTripForm.getOversea(), businessTripForm.getTripMatter(),
						businessTripForm.getTripLocation(), businessTripForm.getOutboundDepartureTime(), businessTripForm.getOutboundFlight(),
						businessTripForm.getOutboundFlightNo(), businessTripForm.getOutboundCountry(), businessTripForm.getOutboundCity(),
						businessTripForm.getOutboundLandingTime(), businessTripForm.getInboundDepartureTime(), businessTripForm.getInboundFlight(),
						businessTripForm.getInboundFlightNo(), businessTripForm.getInboundCountry(), businessTripForm.getInboundCity(), businessTripForm.getInboundLandingTime(),
						businessTripForm.getAgentPassport(), businessTripForm.getAgentVisa(),
						businessTripForm.getAgentMtp(), businessTripForm.getEstimatedCostAirfare(), businessTripForm.getEstimatedCostAgent(),
						businessTripForm.getEstimatedCostLodging(), businessTripForm.getEstimatedCostPickup(), businessTripForm.getCashAdvance(),
						businessTripForm.getAdvanceCurrency(), businessTripForm.getAdvanceAmount(), businessTripForm.getDailyAllowance(),
						businessTripForm.getMemo(), businessTripForm.getId() });
	}

	@Override
	public BusinessTripForm findOneById(Long id) {
		String sql = "SELECT a.*, (select name from company where id = a.company_id) as companyName, (select name from department where id = a.request_department) as departmentName, (select name from employee where id = (select employee_id from t_erp_user where id = (select creator from work_flow where id = a.work_flow_id))) as creatorName, (select name from employee where id = a.employee_id) employee_name, (select name from job_title where id = a.job_title_id) as jobTitle, (SELECT status FROM work_flow WHERE id = a.work_flow_id) AS status FROM business_trip_form a WHERE a.id=?";
		Object[] args = new Object[]{id};
		return (BusinessTripForm) super.queryForObject(dsBmsQry, sql, args, BusinessTripForm.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Page<BusinessTripForm> list(BusinessTripFormForm businessTripFormForm) {
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		
		// Auth
		//param.add(StringUtils.join(businessTripFormForm.getEmployeeIdList(), ","));
		param.add(businessTripFormForm.getLoginUser());
		param.add(businessTripFormForm.getFormType());
		param.add(businessTripFormForm.getLoginUser());
		param.add(businessTripFormForm.getLoginUser());
		
		if(businessTripFormForm.getCompanyId() != null){
			sb.append(" AND b.company_id = ? ");
			param.add(businessTripFormForm.getCompanyId());
		}
		if(businessTripFormForm.getRequestDepartment() != null){
			sb.append(" AND b.request_department = ? ");
			param.add(businessTripFormForm.getRequestDepartment());
		}
		if(StringUtils.isNotBlank(businessTripFormForm.getSerialNo())){
			sb.append(" AND b.serial_no = ? ");
			param.add(businessTripFormForm.getSerialNo());
		}
		if(StringUtils.isNotBlank(businessTripFormForm.getRequestDateStart())){		
			sb.append(" AND STR_TO_DATE(b.request_date, '%Y-%m-%d') >= ? ");
			param.add(businessTripFormForm.getRequestDateStart());
		}
		if(StringUtils.isNotBlank(businessTripFormForm.getRequestDateEnd())){
			sb.append(" AND STR_TO_DATE(b.request_date, '%Y-%m-%d') <= ? ");
			param.add(businessTripFormForm.getRequestDateEnd());
		}		
		if(businessTripFormForm.getStatus() != null){
			sb.append(" AND b.status = ? ");
			param.add(businessTripFormForm.getStatus());
		}
		
		// Auth
		String userFormRole = StringUtils.join(businessTripFormForm.getFormRole(), "','");
		
		StringBuilder sql = new StringBuilder("SELECT b.* FROM (SELECT a.*, (SELECT COUNT(*) FROM form_auth WHERE form_type='TA' and form_role IN (SELECT id FROM form_role WHERE `name` IN('" + userFormRole + "'))) as auth, (select name from company where id = a.company_id) as companyName, (select name from department where id = a.request_department) as departmentName, (select creator from work_flow where id = a.work_flow_id) as creator, (SELECT status FROM work_flow WHERE id = a.work_flow_id) AS status, (SELECT `name` FROM employee WHERE id=(select employee_id from t_erp_user where id=(select creator from work_flow where id = a.work_flow_id))) as creatorName, (select name from employee where id = a.employee_id) employee_name, (select `name` from job_title where id=a.job_title_id) job_title FROM business_trip_form a ) as b ");
		sql.append("left join (select work_flow_id, owner_id from work_flow_detail where owner_id=? and work_flow_id in (select id from work_flow where LEFT(form_type, 2)=?) group by work_flow_id) wfd on wfd.work_flow_id=b.work_flow_id ").append("WHERE (auth>=1 OR creator=? OR owner_id=?)").append(sb.toString());
		StringBuilder countSql = new StringBuilder("select count(*) from (").append(sql).append(") x"); 
				
		if(!businessTripFormForm.isExport()) {
			sql.append(" LIMIT ").append((businessTripFormForm.getPageNo()-1)*businessTripFormForm.getPageSize()).append(",").append(businessTripFormForm.getPageSize());
		}
		
		Page<BusinessTripForm> page = new Page<BusinessTripForm>(businessTripFormForm.getPageSize());

		page = (Page<BusinessTripForm>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(), 
				new BeanPropertyRowMapper<BusinessTripForm>(BusinessTripForm.class));
		
		if(!businessTripFormForm.isExport()) {
			page.setPageNo(businessTripFormForm.getPageNo());
		}
		
		return page;
	}

	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM `business_trip_form` WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}

	@Override
	public BusinessTripForm findOneByFlow(Long workFlowId) {
		String sql = "SELECT a.*, (select name from department where id = a.request_department) as departmentName, (select name from employee where id = (select employee_id from t_erp_user where id = (select creator from work_flow where id = a.work_flow_id))) as creatorName, (select name from job_title where id = a.job_title_id) as jobTitle, (select name from employee where id = a.employee_id) employee_name FROM business_trip_form as a WHERE a.work_flow_id=?";
		Object[] args = new Object[]{workFlowId};
		return (BusinessTripForm) super.queryForObject(dsBmsQry, sql, args, BusinessTripForm.class);
	}		
}