package com.baplay.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baplay.dao.IPermissionDao;
import com.baplay.entity.Function;
import com.google.common.base.Joiner;

@Repository("PermissionDao")
@Transactional
public class PermissionDao extends BaseDao<Function> implements IPermissionDao {
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Function> getFunctions(List<Long> roleIds, int all) {
		if (all == 1) {
			return this.queryForList(dsBmsQry, "SELECT f.* FROM t_erp_function f where status = 1", null, new BeanPropertyRowMapper<Function>(Function.class));
		} else {
			return this.queryForList(dsBmsQry,
					"SELECT f.* FROM t_erp_function f LEFT JOIN t_erp_role_function rf ON f.id = rf.function_id AND rf.status=1 WHERE f.status = 1 AND rf.role_id in ("
							+ Joiner.on(",").join(roleIds) + ")", null, new BeanPropertyRowMapper<Function>(Function.class));
		}
	}

	@Override
	public void updateFunctionStatus(Long funId, Integer status) {
		this.updateForObject(dsBmsUpd, "update app_function set status=? where id=?", new Object[] { status, funId });

	}

	@Override
	public boolean checkLoginAccountExist(String loginAccount) {
		// TODO Auto-generated method stub
		String sql = "SELECT COUNT(*) FROM t_erp_user WHERE login_account=?";
		return this.queryForInt(dsBmsQry, sql, new Object[]{loginAccount}) > 0;		
	}
	
}
