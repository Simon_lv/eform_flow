package com.baplay.dao;

import java.util.List;

import com.baplay.entity.Function;

public interface IFunctionDao {
	public void save(Function function);
	
	public void update(Function function);
	
	public void delete(Function function);
	
	public List<Function> query(Long parentId);
	
	public Function queryOne(Long id);
}
