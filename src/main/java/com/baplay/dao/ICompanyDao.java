package com.baplay.dao;

import java.util.List;

import org.springside.modules.orm.Page;

import com.baplay.entity.Company;
import com.baplay.form.CompanyForm;

public interface ICompanyDao {
	public Company add(Company company);
	public int update(Company company);
	public Company findOneById(Long id);
	public Page<Company> list(CompanyForm companyForm);
	public int delete(Long id, Long modifier);
	public List<Company> findAllCompany();
	public Company findOneByName(String keyword, boolean local);
	public List<Company> findAllGameCompany();
}