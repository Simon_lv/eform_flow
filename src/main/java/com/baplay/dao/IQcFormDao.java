package com.baplay.dao;

import java.util.List;

import org.springside.modules.orm.Page;

import com.baplay.entity.QcForm;
import com.baplay.form.QcFormForm;

public interface IQcFormDao {

	public Page<QcForm> list(QcFormForm qcFormForm);
	public QcForm save(QcForm qcForm);
	public int update(QcForm qcForm);
	public List<QcForm> findAllByFormId(Long formId);
	public List<QcForm> findAllByFlow(Long workFlowId);
	public QcForm findOneByFieldId(Long formId, Long fieldId);
	public int updateResult(Long formId, Long fieldId, Integer result, String remark);
}
