package com.baplay.dao;

import java.util.List;

import com.baplay.entity.UserFlowStep;

public interface IUserFlowStepDao {

	public UserFlowStep findOneByStep(Long workFlowId, Long stepId, Long userId);
	public List<UserFlowStep> findByStep(Long workFlowId, Long stepId);
	public UserFlowStep add(UserFlowStep userFlowStep);
	public int close(long workFlowId, long stepId, long userId);
}