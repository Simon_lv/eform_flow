package com.baplay.dao;

import java.util.List;

import com.baplay.entity.FormRole;

public interface IFormRoleDao {

	public List<FormRole> getRole(Long userId);
	
	public FormRole findOneById(int roleId);
}
