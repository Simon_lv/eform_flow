package com.baplay.dao;

import java.util.Date;

import org.springside.modules.orm.Page;

import com.baplay.entity.RefundForm;
import com.baplay.form.RefundFormForm;

public interface IRefundFormDao {
	public RefundForm add(RefundForm refundForm);
	public int update(RefundForm refundForm);
	public RefundForm findOneById(Long id);
	public Page<RefundForm> list(RefundFormForm refundFormForm);
	public int delete(Long id);
	public RefundForm findOneByFlow(Long workFlowId);
	public int confirmRemitDate(Long id);
	public int updateRemitDate(Long id, Date remitDate);
}
