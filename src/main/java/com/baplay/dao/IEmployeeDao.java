package com.baplay.dao;

import java.util.List;

import org.springside.modules.orm.Page;

import com.baplay.entity.Employee;
import com.baplay.form.EmployeeForm;

public interface IEmployeeDao {
	public Employee add(Employee employee);
	public int update(Employee employee);
	public Employee findOneById(Long id);
	public Page<Employee> list(EmployeeForm employeeForm);
	public int delete(Long id, Long modifier);
	public int delete(String employeeNo, Long modifier);
	public List<Employee> findAllEmployee();
	public List<Employee> findAllEmployee(int companyId);
	public List<Employee> findAllEmployee(int companyId, int departmentId);
	public boolean checkEmpNoExist(Long compnayId, Long id, String employeeNo);
	public Employee findOneByName(String name);
	public List<Employee> findAllEmployee2();
	public List<Employee> findEmployeeByRole(int roleId, int...extraParam);
	public Employee findOneByUID(int uid);
	public List<Employee> findSupervisor(Long departmentId);
	public List<Long> findUserManageDept(Long uid);
	public Employee findOneByEmployeeNo(String employeeNo);
	public Employee findOneByEmployeeNo(Long companyId, String employeeNo);
}