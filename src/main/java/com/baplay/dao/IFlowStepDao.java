package com.baplay.dao;

import java.util.List;

import com.baplay.entity.FlowStep;

public interface IFlowStepDao {
	
	/**
	 * 步驟明細
	 * 
	 * @param id
	 * @return
	 */
	public FlowStep findOneById(Long id);

	/**
	 * 下一步驟
	 * 
	 * @param flowSchemaId
	 * @param curentOrder
	 * @param stepType
	 * @return
	 */
	public List<FlowStep> findFlowAllStep(long flowSchemaId, int currentOrder, Integer...stepType);
	
	/**
	 * 流程步驟
	 * 
	 * @param flowSchemaId
	 * @return
	 */
	public List<FlowStep> findFlowStepBySchema(long flowSchemaId);
	
	/**
	 * 變更角色
	 * 
	 * @param flowStepId
	 * @param formRoleId
	 * @return
	 */
	public int updateRole(Long flowStepId, Long formRoleId);
	
	/**
	 * 變更會簽
	 * 
	 * @param flowStepId
	 * @param countersign
	 * @return
	 */
	public int updateCountersign(Long flowStepId, String countersign);
	
	/**
	 * 下一步驟
	 * 
	 * @param id
	 * @return
	 */
	public FlowStep findNextOneById(Long id);
	
	/**
	 * 當前步驟
	 * 
	 * @param flowSchemaId
	 * @param order
	 * @return
	 */
	public FlowStep findFlowStep(long flowSchemaId, int order);
}