package com.baplay.dao;

import java.util.List;

import org.springside.modules.orm.Page;

import com.baplay.entity.QcFormSchema;
import com.baplay.form.QcFormMgrForm;

public interface IQcFormSchemaDao {

	public List<QcFormSchema> findAllFormSchema();
	public QcFormSchema findOneById(Long id);
	public Page<QcFormSchema> list(QcFormMgrForm qcFormMgrForm);
	public QcFormSchema add(QcFormSchema qcFormSchema);
	public int update(QcFormSchema qcFormSchema);
	public QcFormSchema findOneByCode(String code);
}
