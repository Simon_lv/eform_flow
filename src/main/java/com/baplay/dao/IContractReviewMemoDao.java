package com.baplay.dao;

import java.util.List;

import com.baplay.entity.ContractReviewMemo;

public interface IContractReviewMemoDao {

	public ContractReviewMemo add(ContractReviewMemo cibtractRevuewNeni);
	public int deleteAll(Long contractReviewFormId);
	public List<ContractReviewMemo> findContractReviewMemo(Long contractReviewFormId);
	
}
