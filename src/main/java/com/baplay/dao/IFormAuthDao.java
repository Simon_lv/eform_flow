package com.baplay.dao;

import java.util.Set;

public interface IFormAuthDao {

	/**
	 * 表單權限
	 * 
	 * @param formType
	 * @param userFormRole
	 * @return
	 */
	public boolean formAuth(String formType, Set<String> userFormRole);
}
