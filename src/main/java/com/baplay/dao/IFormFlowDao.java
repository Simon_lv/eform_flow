package com.baplay.dao;

import java.util.List;
import java.util.Set;

import org.springside.modules.orm.Page;

import com.baplay.dto.FormFlow;
import com.baplay.entity.Role;
import com.baplay.form.FormMgrForm;

public interface IFormFlowDao {

	/**
	 * 全類別代辦事項
	 * 
	 * @param userId
	 * @return
	 */
	public List<FormFlow> findRunning(Set<Long> companyId, Set<Long> departmentId, long userId, List<Role> roles);
	
	/**
	 * 指定類別代辦事項
	 * 
	 * @param userId
	 * @param formType
	 * @return
	 */
	public List<FormFlow> findRunningByType(Set<Long> companyId, Set<Long> departmentId, long userId, List<Role> roles, String formType);
	
	/**
	 * 審核記錄
	 * 
	 * @param workFlowId
	 * @return
	 */
	public List<FormFlow> findHistory(long workFlowId);
	
	/**
	 * 查詢全表單
	 * 
	 * @param form
	 * @return
	 */
	public Page<FormFlow> list(FormMgrForm form);
}