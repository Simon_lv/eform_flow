package com.baplay.dao;

import java.util.Date;

import org.springside.modules.orm.Page;

import com.baplay.entity.PaymentForm;
import com.baplay.form.PaymentFormForm;

public interface IPaymentFormDao {
	public PaymentForm add(PaymentForm paymentForm);
	public int update(PaymentForm paymentForm);
	public PaymentForm findOneById(Long id);
	public Page<PaymentForm> list(PaymentFormForm paymentFormForm);
	public int delete(Long id);
	public PaymentForm findOneByFlow(Long id);
	public String getSerialNo(String formCode);
	public int confirmRemitDate(Long id);
	public int updateRemitDate(Long id, Date remitDate);
}