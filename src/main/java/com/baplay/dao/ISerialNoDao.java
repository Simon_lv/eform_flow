package com.baplay.dao;

public interface ISerialNoDao {

	public String findSerialNo(String formCode);
	
}
