package com.baplay.dao;

import java.util.List;

import com.baplay.entity.IssueMemo;

public interface IIssueMemoDao {

	public IssueMemo add(IssueMemo issueMemo);
	public int deleteAll(Long issueFormId);
	public List<IssueMemo> findMemo(Long issueFormId);	
}