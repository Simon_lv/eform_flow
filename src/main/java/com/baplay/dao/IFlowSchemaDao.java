package com.baplay.dao;

import java.util.List;

import org.springside.modules.orm.Page;

import com.baplay.entity.FlowSchema;
import com.baplay.form.FlowMgrForm;

public interface IFlowSchemaDao {

	public FlowSchema findOneById(Long id);
	
	public FlowSchema findOneById(Long companyId, String allId);
	
	public List<FlowSchema> findAll(Long... companyId);
	
	public Page<FlowSchema> list(FlowMgrForm form);
	
	public FlowSchema findOneByName(String name);
}
