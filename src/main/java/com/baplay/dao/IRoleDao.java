package com.baplay.dao;

import java.util.List;

import com.baplay.entity.FormRole;
import com.baplay.entity.Role;

public interface IRoleDao {
	/**
	 * 查询用户所有的角色
	 * 
	 * @param userId
	 * @return
	 */
	public List<Role> getRole(Long userId);

	/**
	 * 添加角色
	 * 
	 * @param name
	 * @return
	 */
	public Long addRole(String name);

	/**
	 * 为角色增加权限
	 * 
	 * @param roleId
	 * @param functionId
	 */
	public void addRoleFunction(Long roleId, List<Long> functionId);

	/**
	 * 更新角色
	 * 
	 * @param roleId
	 * @param name
	 */
	public void updateRole(Long roleId, String name);

	/**
	 * 更新角色状态
	 * 
	 * @param roleId
	 * @param status
	 */
	public void updateRoleStatus(Long roleId, Integer status);

	/**
	 * 查询全部角色
	 * 
	 * @return
	 */
	public List<Role> getAllRole(int all);
	
	public List<FormRole> getAllFormRole(int all);
	
	public Role findOneById(int roleId);
}