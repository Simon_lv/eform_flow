package com.baplay.dao;

import org.springside.modules.orm.Page;

import com.baplay.entity.BudgetForm;
import com.baplay.form.BudgetFormForm;

public interface IBudgetFormDao {
	public BudgetForm add(BudgetForm budgetForm);
	public int update(BudgetForm budgetForm);
	public BudgetForm findOneById(Long id);
	public Page<BudgetForm> list(BudgetFormForm budgetFormForm);
	public int delete(Long id);
	public BudgetForm findOneByFlow(Long workFlowId);
	public int updateTotal(Long id, Long editId);
	public boolean isFiestBudget(Long id);
}
