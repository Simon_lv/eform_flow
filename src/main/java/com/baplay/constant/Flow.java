package com.baplay.constant;

public enum Flow {

	/** 草稿(-1) */
	DRAFT(-1),
	/** 正常結束(0) */
	NORMAL_CLOSED(0),
	/** 執行中(1) */
	RUNNING(1),
	/** 等待執行(1) */
	WAITTING(1),
	/** 使用者鎖定(2) */
	LOCK(2),	
	/** 註銷(8) */
	USER_TERMINATE(8),
	/** 退件(9) */
	REJECT(9),
	/** 編輯(1) */
	EDIT(1),
	/** 審核(2) */
	VERIFY(2),
	/** 放行(3) */
	PASS(3),
	/** 結束(9) */
	END(9),
	/** 草稿(3) */
	DRAFT_FLOW_SCHEMA(-1),
	/** 草稿(9) */
	DRAFT_FLOW_STEP(-1),
	/** 同一部門 */
	SAME_DEPARTMENT(2),
	/** 通知開關 */
	NOTIFY_NA(0);
	
	private int value;
	
	private Flow(int value) {
        this.value = value;
    }
	
	public int getValue() {
        return this.value;
    }
}