package com.baplay.constant;

public enum Form {
	
	PAYMENT("PA"),  // 請款
	BUSINESS_CARD("BC"),  // 公司卡
	BUSINESS_TRIP("TA"),  // 出差申請
	BUSINESS_TRIP_EXPENSE("PT"),  // 出差報支
	REFUND("RE"),  // 退款
	BUDGET("BU"),  // 部門成本
	ISSUE("IS"),  // 重大
	CONTRACT_REVIEW("CO"),  // 合約
	QUALITY_CONTROL("QC");  // 檢核

	private String value;
	
	private Form(String value) {
        this.value = value;
    }
	
	public String getValue() {
        return this.value;
    }
}