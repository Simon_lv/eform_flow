package com.baplay.constant;

import com.baplay.util.Configuration;

public enum URL {	
	URL_IMG_BUSINESSTRIPFORM("url.images.businessTripForm"), FTP_IMG_BUSINESSTRIPFORM("ftp.images.businessTripForm"),
	URL_IMG_CONTRACTREVIEWFORM("url.images.contractReviewForm"), FTP_IMG_CONTRACTREVIEWFORM("ftp.images.contractReviewForm"),
	URL_IMG_ISSUEFORM("url.images.issueForm"), FTP_IMG_ISSUEFORM("ftp.images.issueForm"),
	URL_IMG_PAYMENTFORM("url.images.paymentForm"), FTP_IMG_PAYMENTFORM("ftp.images.paymentForm")
	;

	private String key;

	private URL(String key) {
		// auto add "test_" prefix if running a test server
		if (isTest())
			this.key = "test." + key;
		else
			this.key = key;
	}

	@Override
	public String toString() {
		return Configuration.getInstance().getProperty(this.key);
	}

	private boolean isTest() {
		return "true".equalsIgnoreCase(Configuration.getInstance().getProperty("isTestServer"));
	}
}