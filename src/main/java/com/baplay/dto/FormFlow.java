package com.baplay.dto;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class FormFlow implements Serializable {

	private static final long serialVersionUID = -7753217451124036641L;
	
	private Long workFlowId;
	private Long workFlowDetailId;
	private Long flowSchemaId;
	private String serialNo;
	private String formName;
	private Integer count;
	private Integer status;
	private String formType;
	private Integer stepType;
	private Integer stepOrder;
	private String creator;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	private String owner;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date processTime;
	private String memo;
	private Integer stepStatus;
	private String countersignRole;
	private String description;
	private String companyName;
	private String departmentName;
	private String creatorName;
	private String ownerName;
	
	public Long getWorkFlowId() {
		return workFlowId;
	}
	public void setWorkFlowId(Long workFlowId) {
		this.workFlowId = workFlowId;
	}
	public Long getWorkFlowDetailId() {
		return workFlowDetailId;
	}
	public void setWorkFlowDetailId(Long workFlowDetailId) {
		this.workFlowDetailId = workFlowDetailId;
	}
	public Long getFlowSchemaId() {
		return flowSchemaId;
	}
	public void setFlowSchemaId(Long flowSchemaId) {
		this.flowSchemaId = flowSchemaId;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getFormType() {
		return formType;
	}
	public void setFormType(String formType) {
		this.formType = formType;
	}
	public Integer getStepType() {
		return stepType;
	}
	public void setStepType(Integer stepType) {
		this.stepType = stepType;
	}
	public Integer getStepOrder() {
		return stepOrder;
	}
	public void setStepOrder(Integer stepOrder) {
		this.stepOrder = stepOrder;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public Date getProcessTime() {
		return processTime;
	}
	public void setProcessTime(Date processTime) {
		this.processTime = processTime;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Integer getStepStatus() {
		return stepStatus;
	}
	public void setStepStatus(Integer stepStatus) {
		this.stepStatus = stepStatus;
	}
	public String getCountersignRole() {
		return countersignRole;
	}
	public void setCountersignRole(String countersignRole) {
		this.countersignRole = countersignRole;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getCreatorName() {
		return creatorName;
	}
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}	
}