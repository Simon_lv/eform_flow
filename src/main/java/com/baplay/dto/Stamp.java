package com.baplay.dto;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Stamp implements Serializable {

	private static final long serialVersionUID = -6699533488120105125L;

	private String ownerName;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date processTime;
	private Integer status;
	private Integer ownerId;
	private String countersignRole;
	private Integer stepId;
	private String info;
	
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public Date getProcessTime() {
		return processTime;
	}
	public void setProcessTime(Date processTime) {
		this.processTime = processTime;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}
	public String getCountersignRole() {
		return countersignRole;
	}
	public void setCountersignRole(String countersignRole) {
		this.countersignRole = countersignRole;
	}
	public Integer getStepId() {
		return stepId;
	}
	public void setStepId(Integer stepId) {
		this.stepId = stepId;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}	
}