package com.baplay.dto;

public interface BaseForm {

	public void setCompanyId(Long id);
	public Long getCompanyId();
	
	public void setRequestDepartment(Integer id);
	public Integer getRequestDepartment();	
}