package com.baplay.dto;

import java.util.Date;

public class UserDto {

	private Long userId;

	private String userName;

	private String userMac;

	private String email;

	private String phoneNo;

	private Date registerTime;

	private String registerGameName;

	private String regitsterGameCode;

	private String lastLoginGameName;

	private String lastLoginGameCode;

	private String imei;
	
	private Date lastLoginTime;

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getRegisterGameName() {
		return registerGameName;
	}

	public void setRegisterGameName(String registerGameName) {
		this.registerGameName = registerGameName;
	}

	public String getRegitsterGameCode() {
		return regitsterGameCode;
	}

	public void setRegitsterGameCode(String regitsterGameCode) {
		this.regitsterGameCode = regitsterGameCode;
	}

	public String getLastLoginGameName() {
		return lastLoginGameName;
	}

	public void setLastLoginGameName(String lastLoginGameName) {
		this.lastLoginGameName = lastLoginGameName;
	}

	public String getLastLoginGameCode() {
		return lastLoginGameCode;
	}

	public void setLastLoginGameCode(String lastLoginGameCode) {
		this.lastLoginGameCode = lastLoginGameCode;
	}

	public Date getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(Date registerTime) {
		this.registerTime = registerTime;
	}

	public String getUserMac() {
		return userMac;
	}

	public void setUserMac(String userMac) {
		this.userMac = userMac;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

}
