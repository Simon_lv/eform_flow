package com.baplay.dto;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * DTO基类的标识
 * */

public class BaseDto implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum STATUS {
		NONE, NORMAL, DELETE
	};

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
