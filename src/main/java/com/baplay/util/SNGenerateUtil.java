package com.baplay.util;

import java.security.MessageDigest;

import org.apache.commons.lang.math.RandomUtils;

/**
 * 序號產生工具
 * @author Times
 * 2015-06-22 19:00:14
 *
 */
public class SNGenerateUtil {
	public static String generateSN(String gameCode) {
		String SN = gameCode.toUpperCase().replace("_", "")
				+ (System.currentTimeMillis());
		for (int j = 0; j < 5; j++) {
			SN = SN + LETTER[RandomUtils.nextInt(26)];
		}
		
		SN = SN + LETTER[RandomUtils.nextInt(26)];
		SN = gameCode.toUpperCase().replace("_", "")+MD5(SN);
		if (SN.length() >= 20) {
			SN = SN.substring(0, 20);
		}
	//	PrintUtil.outputItem("activityId", "gameCode ("+gameCode+")  is "+SN+" !");
		return SN;
	}
	private static final String[] LETTER = new String[] { "A", "B", "C", "D",
		"E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q",
		"R", "S", "T", "U", "V", "W", "X", "Y", "Z" 
		};
	
	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		int count = 1000;
		for(int i=0;i<count;i++) {
			String sn = generateSN("xajh");
			System.out.println(i+":"+sn);
		}
		long endTime = System.currentTimeMillis();
		System.out.println((endTime - startTime)/1000);
	}
	
	private final static String MD5(String s) {
        char hexDigits[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};       
        try {
            byte[] btInput = s.getBytes();
            // 获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            // 使用指定的字节更新摘要
            mdInst.update(btInput);
            // 获得密文
            byte[] md = mdInst.digest();
            // 把密文转换成十六进制的字符串形式
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
	
}
