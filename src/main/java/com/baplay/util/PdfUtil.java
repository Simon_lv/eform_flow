package com.baplay.util;

import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.pdf.PDFCreationListener;

import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfString;

public class PdfUtil {
	
	public static final String ACTION = "PDF"; 

	private String htmlContent;

	public void setHtmlContent(String baseUrl, String title, String content) {
		htmlContent = transfer(baseUrl, title, content);
	}
	
	private String transfer(String baseUrl, String title, String content) {
		String xhtml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
				"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n" + 
				"\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n" + 
				"<html xmlns=\"http://www.w3.org/1999/xhtml\">\n";

		String css = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + baseUrl + "/resources/pdf/css/print.css\" media=\"print\"/>";
		
		return String.format("%s<head>\n<title>%s</title>\n%s\n</head>\n<body>\n%s\n</body>\n</html>", 
				xhtml, title, css, content);
	}
	
	public void export(HttpServletResponse response, String filename, String subject) throws Exception {
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "attachment; filename="
				+ URLEncoder.encode(filename + ".pdf", "utf-8"));
		OutputStream os = response.getOutputStream();
		writeAsPdf(os, filename, subject);
		os.close();
	}

	private void writeAsPdf(OutputStream os, String filename, String subject) throws Exception {
		convertToXHTML();		
		
		ITextRenderer renderer = new ITextRenderer();
		
		CreationListener listener = new CreationListener(filename, subject);
		renderer.setListener(listener);
		
		ITextFontResolver resolver = renderer.getFontResolver();
		// base url
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		String baseUrl = getBaseUrl(request);
		String path = new StringBuilder().append(baseUrl).append("/resources/pdf/fonts/arialuni.ttf").toString();
	    
		resolver.addFont(path, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
		renderer.getSharedContext().setBaseURL(baseUrl);		
		
		renderer.setDocumentFromString(htmlContent);
		renderer.layout();
		renderer.createPDF(os);
	}

	private void convertToXHTML() throws Exception {
		htmlContent = "" + htmlContent + "";
		StringWriter writer = new StringWriter();
		Tidy tidy = new Tidy();
		tidy.setTidyMark(false);
		tidy.setDocType("omit");
		tidy.setXHTML(true);
		tidy.setInputEncoding("utf-8");
		tidy.setOutputEncoding("utf-8");
		tidy.parse(new StringReader(htmlContent), writer);
		writer.close();
		htmlContent = writer.toString();
	}
	
	public String getBaseUrl(HttpServletRequest request) {
		String scheme = request.getScheme();             // http
	    String serverName = request.getServerName();     // hostname.com
	    int serverPort = request.getServerPort();        // 80
	    String baseUrl = new StringBuilder().append(scheme).append("://").append(serverName).append(":").append(serverPort).toString();
	    
	    return baseUrl;
	}
	
	class CreationListener implements PDFCreationListener {
		
		private String title;
		private String subject;
		
		public CreationListener(String title, String subject) {
			this.title = title;
			this.subject = subject;
		}
		
        public void onClose(ITextRenderer renderer) {                
        	PdfString title = new PdfString(this.title, PdfObject.TEXT_UNICODE);                                          
        	renderer.getOutputDevice().getWriter().getInfo().put(PdfName.TITLE, title);

        	PdfString author = new PdfString("伊凡達科技股份有限公司", PdfObject.TEXT_UNICODE);         
        	renderer.getOutputDevice().getWriter().getInfo().put(PdfName.AUTHOR, author);   
            
        	PdfString subject = new PdfString(this.subject, PdfObject.TEXT_UNICODE);
            renderer.getOutputDevice().getWriter().getInfo().put(PdfName.SUBJECT, subject);   
        }
        
		@Override
		public void preOpen(ITextRenderer arg0) {
			// Do Nothing	
		}
	} 
}