package com.baplay.util;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;

public class PrintUtil 
{
	private static final Logger logger = Logger.getLogger(PrintUtil.class);
	private static final int MAX_OUTPUT_LENGTH=500;
	
	public static void printRequest(String tag, String method,HttpServletRequest request) {
		logger.info("*****************************************************************");
		logger.info("input url："+ request.getRequestURI());	
		Enumeration paramNames = request.getParameterNames();
		while(paramNames.hasMoreElements()) {//遍历Enumeration
			String name = (String)paramNames.nextElement();//取出下一个元素
			String value = request.getParameter(name);//获取元素的值
			outputItem(name,value);

		}
		logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	

	public static void printResponse(Iterator it) {
		logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		logger.info("output result：");
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String key = (String)entry.getKey();
			Object value = entry.getValue();
			outputItem(key,value);
		}
		logger.info("*****************************************************************");
	}
	
	public static void printResponse(Map resultMap) {
		Iterator it = resultMap.entrySet().iterator();
		logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		logger.info("output result：");
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String key = (String)entry.getKey();
			Object value = entry.getValue();
			outputItem(key,value,"output");
		}
		logger.info("*****************************************************************");
	}
	
	public static void printResponse(JSONObject obj) {
		
		Iterator it = obj.entrySet().iterator();
		logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		logger.info("output result：");
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String key = (String)entry.getKey();
			Object value = entry.getValue();
			outputItem(key,value,"output");
		}
		logger.info("*****************************************************************");
	}
	
	public static void outputItem(String name,Object value,String source)
	{
		if(name.length()<=9)
		{
			name = name+"		=	";
		}else
		{
			name = name+"	=	";
	
		}
		String val = "";
		if(value!=null)
		{
			val = value.toString();
		}
		int length = val.length();
		if(length>MAX_OUTPUT_LENGTH)
		{
			val = val.substring(0,MAX_OUTPUT_LENGTH)+"......";
		}
		logger.info(name+val);
	}
	

	
	public static void outputItem(String name,Object value)
	{
		if(name.length()<=9)
		{
			name = name+"		=	";
		}else
		{
			name = name+"	=	";
	
		}
		
		logger.info(name+value);
	}
	
	public static void outputItem(String name,String value)
	{
	
		
		
		if(name.length()<=9)
		{
			name = name+"		=	";
		}else
		{
			name = name+"	=	";
	
		}
		logger.info(name+value);
		
	}
	
	public static void outputContent(String content)
	{
		logger.info("			=	"+content);
	}
	
	public static void outputContent(Object content)
	{
		logger.info("			=	"+content);
	}
	
	
	public static HashMap<String, String> printAndReturnRequest(String tag, String method,HttpServletRequest request) 
	{
		logger.info("*****************************************************************");
		HashMap<String, String> params = new HashMap<String, String>();
		logger.info("input url："+ request.getRequestURI());	
		Enumeration paramNames = request.getParameterNames();
		while(paramNames.hasMoreElements()) {//遍历Enumeration
			String name = (String)paramNames.nextElement();//取出下一个元素
			String value = request.getParameter(name);//获取元素的值
			params.put(name, value);
			outputItem(name,value);

		}
		logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		return params;
	}
	
	public static HashMap<String, String> printAndReturnRequest(HttpServletRequest request) 
	{
		logger.info("*****************************************************************");
		HashMap<String, String> params = new HashMap<String, String>();
		logger.info("input url："+ request.getRequestURI());	
		Enumeration paramNames = request.getParameterNames();
		while(paramNames.hasMoreElements()) {//遍历Enumeration
			String name = (String)paramNames.nextElement();//取出下一个元素
			String value = request.getParameter(name);//获取元素的值
			params.put(name, value);
			outputItem(name,value);

		}
		outputItem("params",request.getQueryString());
		outputItem("Referer",request.getHeader("Referer"));
		logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		return params;
	}
	
	
	
//	public static void printRequest(String tag, String method,HttpServletRequest request) {
//		System.out.println("*****************************************************************");
//		System.out.println("input url："+ request.getRequestURI());	
//		Enumeration paramNames = request.getParameterNames();
//		while(paramNames.hasMoreElements()) {//遍历Enumeration
//			String name = (String)paramNames.nextElement();//取出下一个元素
//			String value = request.getParameter(name);//获取元素的值
//			outputItem(name,value);
//
//		}
//		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//	}
//	
//
//	public static void printResponse(Iterator it) {
//		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//		System.out.println("output result：");
//		while (it.hasNext()) {
//			Map.Entry entry = (Map.Entry) it.next();
//			String key = (String)entry.getKey();
//			Object value = entry.getValue();
//			outputItem(key,value);
//		}
//		System.out.println("*****************************************************************");
//	}
//	
//	public static void printResponse(Map resultMap) {
//		Iterator it = resultMap.entrySet().iterator();
//		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//		System.out.println("output result：");
//		while (it.hasNext()) {
//			Map.Entry entry = (Map.Entry) it.next();
//			String key = (String)entry.getKey();
//			Object value = entry.getValue();
//			outputItem(key,value,"output");
//		}
//		System.out.println("*****************************************************************");
//	}
//	
//	public static void printResponse(JSONObject obj) {
//		
//		Iterator it = obj.entrySet().iterator();
//		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//		System.out.println("output result：");
//		while (it.hasNext()) {
//			Map.Entry entry = (Map.Entry) it.next();
//			String key = (String)entry.getKey();
//			Object value = entry.getValue();
//			outputItem(key,value,"output");
//		}
//		System.out.println("*****************************************************************");
//	}
//	
//	public static void outputItem(String name,Object value,String source)
//	{
//		if(name.length()<=5)
//		{
//			name = name+"			=	";
//		}else if(name.length()>13)
//		{
//			name = name+"	=	";
//		}else
//		{
//			name = name+"		=	";
//	
//		}
//		String val = "";
//		if(value!=null)
//		{
//			val = value.toString();
//		}
//		int length = val.length();
//		if(length>MAX_OUTPUT_LENGTH)
//		{
//			val = val.substring(0,MAX_OUTPUT_LENGTH)+"......";
//		}
//		System.out.println(name+val);
//	}
//	
//	
//	public static void outputItem(String name,String value,String source)
//	{
//		if(name.length()<=5)
//		{
//			name = name+"			=	";
//		}else
//		{
//			name = name+"		=	";
//		}
//		int length = (value==null?0:value.length());
//		if(length>MAX_OUTPUT_LENGTH)
//		{
//			value = value.substring(0,MAX_OUTPUT_LENGTH)+"......";
//		}
//		System.out.println(name+value);
//		
//	}
//	
//	public static void outputItem(String name,Object value)
//	{
//		if(name.length()<=5)
//		{
//			name = name+"			=	";
//		}else if(name.length()>13)
//		{
//			name = name+"	=	";
//		}else
//		{
//			name = name+"		=	";
//	
//		}
//		
//		System.out.println(name+value);
//	}
//	
//	public static void outputItem(String name,String value)
//	{
//		if(name.length()<=5)
//		{
//			name = name+"			=	";
//		}else
//		{
//			name = name+"		=	";
//	
//		}
//		System.out.println(name+value);
//		
//	}
//	
//	public static void outputContent(String content)
//	{
//		System.out.println("			=	"+content);
//	}
//	
//	public static void outputContent(Object content)
//	{
//		System.out.println("			=	"+content);
//	}
//	
//	
//	public static HashMap<String, String> printAndReturnRequest(String tag, String method,HttpServletRequest request) 
//	{
//		System.out.println("*****************************************************************");
//		HashMap<String, String> params = new HashMap<String, String>();
//		System.out.println("input url："+ request.getRequestURI());	
//		Enumeration paramNames = request.getParameterNames();
//		while(paramNames.hasMoreElements()) {//遍历Enumeration
//			String name = (String)paramNames.nextElement();//取出下一个元素
//			String value = request.getParameter(name);//获取元素的值
//			params.put(name, value);
//			outputItem(name,value);
//
//		}
//		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//		return params;
//	}
//	
//	public static HashMap<String, String> printAndReturnRequest(HttpServletRequest request) 
//	{
//		System.out.println("*****************************************************************");
//		HashMap<String, String> params = new HashMap<String, String>();
//		System.out.println("input url："+ request.getRequestURI());	
//		Enumeration paramNames = request.getParameterNames();
//		while(paramNames.hasMoreElements()) {//遍历Enumeration
//			String name = (String)paramNames.nextElement();//取出下一个元素
//			String value = request.getParameter(name);//获取元素的值
//			params.put(name, value);
//			outputItem(name,value);
//
//		}
//		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//		return params;
//	}
	


}
