package com.baplay.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.efun.util.MailOperator;

public class MailUtil {
	
	protected static final Logger log = LoggerFactory.getLogger(MailUtil.class);
	
	public static void sendMail(final String[] sendTo, final String mailcontent,final String mailsubject) {
		final String mailfrom = "";
		
		Runnable r = new Runnable() {
			public void run() {
				Boolean isAutheticate = true;
				String protocol = "smtp";
				String host = "127.0.0.1";
				Integer port =25;
				String user = "";
				String password = "";
				String from = "cs@8888play.com";
				String timeOut = "60000";
				String connectionTimeOut = "60000";
				String rmailfrom = null;
				
				try {
					String name = javax.mail.internet.MimeUtility
							.encodeText(mailfrom);
					rmailfrom = name + " <" + from + ">";
				} 
				catch(Exception e) {
					rmailfrom = from;
				}
				
				String rmailsubject = mailsubject;
				String rmailcontent = mailcontent;

				try {
					boolean sendEmail = MailOperator.sendEmail(
							isAutheticate, protocol, host, port, user,
							password, rmailfrom, timeOut,
							connectionTimeOut, sendTo,
							rmailsubject, rmailcontent, null);
					int sendcounts = 0;
					
					while(!sendEmail && sendcounts < 2) {
						sendEmail = MailOperator.sendEmail(isAutheticate,
								protocol, host, port, user, password,
								rmailfrom, timeOut, connectionTimeOut,
								sendTo, rmailsubject,
								rmailcontent, null);
						sendcounts = sendcounts + 1;
					}
				} 
				catch(Exception e) {
					log.info("err", e);
				}
			}
		};
		
		ThreadUtil.services.execute(r);
	}
	
	public static boolean email(String email) {
		if(StringUtils.isEmpty(email)) {
			return true;
		}
		
		return email.length() <= 100
				&& email.matches("^[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-]+)+$");
	}
}