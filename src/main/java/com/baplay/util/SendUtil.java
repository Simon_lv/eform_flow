package com.baplay.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

public final class SendUtil {

	/**
	 * xp post 请求方式
	 * 
	 * @param url
	 * @param urlHead
	 * @param realUrl
	 * @param params
	 * @return
	 */

	public static String sendPost(String realUrl, Map<String, String> params) {

		HttpClient client = null;
		PostMethod post = null;
		String ret = null;
		NameValuePair[] bs = null;
		int i = 0;
		try {

			if (params != null) {
				bs = new NameValuePair[params.size()];
				Set<Map.Entry<String, String>> set = params.entrySet();
				for (Entry<String, String> entry : set) {
					bs[i] = new NameValuePair(entry.getKey(), entry.getValue());
					i++;
				}
			}
			client = new HttpClient();

			post = new PostMethod(realUrl);

			post.setRequestBody(bs);
			client.executeMethod(post);

			InputStream resStream = post.getResponseBodyAsStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(
					resStream, "utf-8"));
			StringBuffer resBuffer = new StringBuffer();
			String resTemp = "";
			while ((resTemp = br.readLine()) != null) {
				resBuffer.append(resTemp);
			}
			ret = resBuffer.toString();

		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		return ret;
	}
}
