package com.baplay.util;

import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

import com.baplay.infra.cache.Cache;
import com.baplay.infra.cache.CacheFactory;

public class CacheUtil {
	
	public static <T> T getCacheByKey(String key, Class<T> classType) {
		Cache<T> cache = CacheFactory.getCommonCache(classType);
		return cache.get(key);
	}
	
	public static <T> boolean addCache(String key, T t, Class<T> classType, Date effectiveDate) {
		Cache<T> cache = CacheFactory.getCommonCache(classType);
		return cache.add(key, t, effectiveDate);
	}
	
	public static <T> boolean addCache(String key, T t, Class<T> classType) {
		Cache<T> cache = CacheFactory.getCommonCache(classType);
		return cache.add(key, t, DateUtils.addDays(new Date(), ReturnUtil.GAME_LIST_TIME));
	}
	
	public static <T> boolean updateCache(String key, T t, Class<T> classType, Date effectiveDate) {
		Cache<T> cache = CacheFactory.getCommonCache(classType);
		return cache.update(key, t, effectiveDate);
	}
	
	public static <T> boolean updateCache(String key, T t, Class<T> classType) {
		Cache<T> cache = CacheFactory.getCommonCache(classType);
		return cache.update(key, t);
	}
	
	public static <T> boolean deleteCache(String key, Class<T> classType) {
		Cache<T> cache = CacheFactory.getCommonCache(classType);
		System.out.println("old Cacahe is"+cache);
		return cache.delete(key);
	}
	
	public static boolean deleteAll(){
		Cache<Object> cache = CacheFactory.getCommonCache(Object.class);
		System.out.println("old Cacahe is"+cache);
		return cache.deleteAll();
		
	}
	
}
