package com.baplay.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFShape;
import org.apache.poi.hssf.usermodel.HSSFSimpleShape;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 
/**
 * 讀取Excel
 * 
 * @author zengwendong
 */
public class ExcelReader {
    private static final Logger logger = LoggerFactory.getLogger(ExcelReader.class);
    protected Workbook wb;
    protected Sheet sheet;
    protected Row row;
    protected DataFormatter objDefaultFormat = new DataFormatter();
    protected FormulaEvaluator objFormulaEvaluator;
    private InputStream is;
 
    public ExcelReader(String filepath) {
        if(filepath == null) {
            return;
        }
        
        String ext = filepath.substring(filepath.lastIndexOf("."));
        
        try {
            is = new FileInputStream(filepath);           
            init(is, ext);
        } 
        catch(FileNotFoundException e) {
            logger.error("FileNotFoundException", e);
        }         
    }
    
    /**
     * FileUpload
     * <p>
     * File.getName() 有可能是暫存檔名
     * 
     * @param file
     * @param fileName
     */
    public ExcelReader(File file, String fileName) {
        if(file == null) {
            return;
        }
        
        String ext = fileName.substring(fileName.lastIndexOf("."));
        
		try {
			is = new FileInputStream(file);
			init(is, ext);
		} 
		catch(FileNotFoundException e) {
			logger.error("FileNotFoundException", e);
		}        
    }
    
    public ExcelReader(InputStream is, String ext) {
    	this.is = is;
    	init(is, ext);
    }
    
    private void init(InputStream is, String ext) {
    	try {
    		if(".xls".equals(ext)) {
                wb = new HSSFWorkbook(is);
                objFormulaEvaluator = new HSSFFormulaEvaluator((HSSFWorkbook) wb);
            }
            else if(".xlsx".equals(ext)) {
                wb = new XSSFWorkbook(is);
                objFormulaEvaluator = new XSSFFormulaEvaluator((XSSFWorkbook) wb);
            }
            else {
                wb = null;
            }            
        } 
        catch(IOException e) {
            logger.error("IOException", e);
        }
    }
     
    /**
     * 讀取Excel表格表頭的內容
     * 
     * @param InputStream
     * @return String 表頭內容的數組
     * @author zengwendong
     */
    public String[] readExcelTitle() throws Exception {
        if(wb == null) {
            throw new Exception("Workbook對像為空！");
        }
        
        sheet = wb.getSheetAt(0);
        row = sheet.getRow(0);
        // 標題總列數
        int colNum = row.getPhysicalNumberOfCells();
        logger.debug("colNum:" + colNum);
        String[] title = new String[colNum];
        
        for(int i = 0; i < colNum; i++) {
            title[i] = row.getCell(i).getCellFormula();
        }
        
        return title;
    }
 
    /**
     * 讀取Excel數據內容<p>
     * <String sheetName, List<cells> rows>
     * 
     * @param InputStream
     * @return Map 包含單元格數據內容的Map對像
     * @author zengwendong
     */
    public Map<String, List<Object[]>> readExcelContent() throws Exception {
        if(wb == null) {
            throw new Exception("Workbook對像為空！");
        }                
        // 全部工作表
        Map<String, List<Object[]>> contents = new LinkedHashMap<String, List<Object[]>>();
        
        Iterator<Sheet> sheets = wb.sheetIterator();
        
        while(sheets.hasNext()) {        	
        	sheet = sheets.next();
        	// 總行數
	        int rowNum = sheet.getPhysicalNumberOfRows();
	        row = sheet.getRow(0);
	        
	        if(row == null) {
	        	continue;
            }
	        // 總欄數
	        int colNum = row.getPhysicalNumberOfCells();
	        DataFormatter formatter = new DataFormatter();
	        // 工作表內容
	        List<Object[]> content = new LinkedList<Object[]>();
	        // 正文內容應該從第二行開始,第一行為表頭的標題
	        for(int i = 0; i < rowNum; i++) {
	        	row = sheet.getRow(i);
	            
	        	if(row == null) {
	            	continue;
	            }
	            
	            int j = 0;
	            Object[] cellValue = new Object[colNum];
	            // 空白欄位數
	            int emptyCol = 0;
	           	            
	            while(j < colNum) {
	            	Object obj = getCellFormatValue(formatter, row.getCell(j));
	            	cellValue[j] = obj;
	                j++;
	                // 空白欄
	                if(StringUtils.isEmpty(obj.toString())) {
	                	emptyCol++;
	                }
	            }
	            // 空白行忽略
	            if(emptyCol == colNum) {
	            	continue;
	            }
	            
	            content.add(cellValue);	            
	        }
	        	       
	        contents.put(sheet.getSheetName(), content);
        }    
        
        return contents;
    }
 
    /**
     * 
     * 根據Cell類型設置數據
     * 
     * @param cell
     * @return
     * @author zengwendong
     */
    protected Object getCellFormatValue(DataFormatter formatter, Cell cell) {    	
    	Object cellvalue = "";
    	
        if(cell != null) {
            // 判斷當前Cell的Type
        	switch(cell.getCellType()) {
	            case Cell.CELL_TYPE_NUMERIC:// 如果當前Cell的Type為NUMERIC
	            	// 判斷當前的cell是否為Date
	                if(DateUtil.isCellDateFormatted(cell)) {
	                    // 如果是Date類型則，轉化為Data格式
	                    // data格式是帶時分秒的：2013-7-10 0:00:00
	                    // cellvalue = cell.getDateCellValue().toLocaleString();
	                    // data格式是不帶帶時分秒的：2013-7-10
	                    Date date = cell.getDateCellValue();
	                    cellvalue = date;
	                }
        			else if (cell.getCellStyle().getDataFormat() == 181) {  // 自定義日期格式(ROC)
        				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
                        double value = cell.getNumericCellValue();  
                        Date date = org.apache.poi.ss.usermodel.DateUtil.getJavaDate(value);  
                        cellvalue = sdf.format(date);  
        			}
	                else {// 如果是純數字
	                	// 取得當前Cell的數值
	                    //cellvalue = String.valueOf(cell.getNumericCellValue());
	                	cellvalue = formatter.formatCellValue(cell);
	                }
	                
	                break;
	            case Cell.CELL_TYPE_FORMULA:
	            	if(cell.getCachedFormulaResultType() == Cell.CELL_TYPE_NUMERIC) {
	            		if(DateUtil.isCellDateFormatted(cell)) {
	            			Date date = cell.getDateCellValue();
		                    cellvalue = date;
		                } 
		                else {		                	
		                    cellvalue = cell.getNumericCellValue();		                    
		                }	            		
	            	}
	            	else if(cell.getCachedFormulaResultType() == Cell.CELL_TYPE_STRING) {
	            		cellvalue = cell.getRichStringCellValue();            		
	            	}
	            	else {
	            		cellvalue = cell.getCellFormula();
	            	}
	            	
	            	break;
	            case Cell.CELL_TYPE_STRING:// 如果當前Cell的Type為STRING
	                // 取得當前的Cell字符串
	                cellvalue = cell.getRichStringCellValue().getString();
	                break;
	            case Cell.CELL_TYPE_BOOLEAN:
	            	cellvalue = cell.getBooleanCellValue();
                    break;    
	            default:// 默認的Cell值
	                cellvalue = "";
	                break;
            }
        } 
        else {
            cellvalue = "";
        }
        
        return cellvalue;        
    }
    
    public void destory() {
    	try {
    		wb.close();
    		is.close();
    	}
    	catch(Exception e) {
    		// Do Nothing
    	}
    }
    
    private void test1() {
    	try {
    	Workbook wb = new HSSFWorkbook();
    	Sheet sheet = wb.createSheet();
    	HSSFPatriarch patriarch = (HSSFPatriarch) sheet.createDrawingPatriarch();

    	/* Here is the thing: the line will go from top left in cell (0,0) to down left 
    	of cell (0,1) */
    	HSSFClientAnchor anchor = new HSSFClientAnchor(
    	  1020, 255, 1020, 255, (short) 1, 2, (short) 1, 5);

    	HSSFSimpleShape shape = patriarch.createSimpleShape(anchor);
    	shape.setShapeType(HSSFSimpleShape.OBJECT_TYPE_LINE);
    	shape.setLineStyleColor(10, 10, 10);
    	shape.setFillColor(90, 10, 200);
    	shape.setLineWidth(HSSFShape.LINEWIDTH_ONE_PT);
    	shape.setLineStyle(HSSFShape.LINESTYLE_SOLID);
    	
    	HSSFClientAnchor a1 = new HSSFClientAnchor(1020, 255, 1020, 255, (short) 0, 0, (short) 2, 2);
    	HSSFSimpleShape rec1 = patriarch.createSimpleShape(a1);
    	//此处设置图形类型为矩形
    	rec1.setShapeType(HSSFSimpleShape.OBJECT_TYPE_RECTANGLE);
    	//设置填充色
    	//rec1.setFillColor(125, 125, 125);
    	//设置边框样式
    	rec1.setLineWidth(HSSFShape.LINEWIDTH_ONE_PT);
    	//设置边框宽度
    	rec1.setLineStyle(HSSFShape.LINESTYLE_SOLID);
    	//设置边框颜色
    	//rec1.SetLineStyleColor(100, 0, 100);
    	rec1.setString(new HSSFRichTextString("營運事業處"));
    	
    	HSSFClientAnchor a2 = new HSSFClientAnchor(1020, 255, 1020, 255, (short) 0, 5, (short) 2, 7);
    	HSSFSimpleShape rec2 = patriarch.createSimpleShape(a2);
    	//此处设置图形类型为矩形
    	rec2.setShapeType(HSSFSimpleShape.OBJECT_TYPE_RECTANGLE);
    	//设置填充色
    	//rec1.setFillColor(125, 125, 125);
    	//设置边框样式
    	rec2.setLineWidth(HSSFShape.LINEWIDTH_ONE_PT);
    	//设置边框宽度
    	rec2.setLineStyle(HSSFShape.LINESTYLE_SOLID);
    	//设置边框颜色
    	//rec1.SetLineStyleColor(100, 0, 100);
    	rec2.setString(new HSSFRichTextString("產品中心"));

    	// you don't even need the cell, but if you also want to write anything...
//    	Row row = sheet.createRow(0);
//    	Cell cell = row.createCell(0);
//    	cell.setCellValue("Test"); 
    	
    	OutputStream os = new FileOutputStream("C:/temp/1.xls"); 
    	wb.write(os);
    	os.close();
    	
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
    }
    
    public static void main(String[] args) {
        try {
        	
            String filepath = "c:\\temp\\1050531.xls";
            ExcelReader excelReader = new ExcelReader(filepath);
            // 對讀取Excel表格標題測試
//          String[] title = excelReader.readExcelTitle();
//          logger.info("獲得Excel表格的標題:");
//          for(String s : title) {
//              System.out.print(s + " ");
//          }
             
            // 對讀取Excel表格內容測試
            Map<String, List<Object[]>> contents = excelReader.readExcelContent();
            logger.info("獲得Excel表格的內容:");
            for(Entry<String, List<Object[]>> content : contents.entrySet()) {
            	String sheetName = content.getKey();
            	List<Object[]> data = content.getValue();
            	
	            for(Object[] o : data) {
	                System.out.println(StringUtils.join(o, ","));
	            }
	            
	            logger.info(sheetName + " Total x " + data.size());
            }
              
        	//ExcelReader er = new ExcelReader(null);
        	//er.test1();
        } 
//        catch(FileNotFoundException e) {
//            System.out.println("未找到指定路徑的文件!");
//            e.printStackTrace();
//        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}