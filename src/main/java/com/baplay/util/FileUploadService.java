package com.baplay.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileUploadService {
	

	public static void toLocalFilePath(File file, String newPath,String newName) {
		File newDir = new File(newPath);
		if(!newDir.exists()) {
			newDir.mkdirs();
		}
		
		String newFile = newDir.getPath().concat("/"+newName);
//		File upFile = new File(newFile);
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
			//改名
			fis = new FileInputStream(file);
			fos = new FileOutputStream(newFile);
			byte[] buffer = new byte[1024];
	        int len = 0;   
	        while ((len = fis.read(buffer)) > 0) {   
	            fos.write(buffer, 0, len);   
	        }
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		finally{
			try {
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}   
	        try {
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void toFtp(File file, String serverPath,String newName) {
		try {
	        FtpUtil.upload(serverPath, file);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		finally{
//			boolean flag = true;
//			int times = 0;
//			do{
//				if(upFile.canExecute()){
//					upFile.deleteOnExit();
				if(!file.delete()) {
					System.out.println(file.getName()+"刪除失敗");
				}
//					flag = !upFile.canExecute();
//				}
//				try {
//					Thread.sleep(500);
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				}
//				times++;
//			}while(times<5 && flag);
		}
	}

	public static void toLocalFilePath(InputStream inputStream, String newPath, String newName) {
		File newDir = new File(newPath);
		if(!newDir.exists()) {
			newDir.mkdirs();
		}
		
		String newFile = newDir.getPath().concat("/"+newName);
		FileOutputStream fos = null;
		try {
			//改名
			fos = new FileOutputStream(newFile);
			byte[] buffer = new byte[1024];
	        int len = 0;   
	        while ((len = inputStream.read(buffer)) > 0) {   
	            fos.write(buffer, 0, len);   
	        }
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		finally{
			try {
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}   
	        try {
	        	inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
