package com.baplay.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringUtil {

	private static final Logger logger = LoggerFactory.getLogger(StringUtil.class);
	public static final String[] CHINESE_NUMBER = {"○", "一", "二", "三", "四", "五", "六", "七", "八", "九"};

	/**
	 * 金额汇总专用格式化tag
	 * 
	 * 只取整数部分并且分组，小数不进位
	 * 
	 * @param bd
	 * @return
	 */
	public static String bigDecimalToFormattedString(BigDecimal bd) {
		String formattedString = null;
		try {
			DecimalFormatSymbols symbols = new DecimalFormatSymbols();
			symbols.setDecimalSeparator('.');
			symbols.setGroupingSeparator(',');

			DecimalFormat chCurrencyFormat = new DecimalFormat("#.##", symbols);
			chCurrencyFormat.setMaximumFractionDigits(2);

			formattedString = chCurrencyFormat.format(bd);
			BigDecimal bd2 = new BigDecimal(formattedString);
			DecimalFormat chCurrencyFormat2 = new DecimalFormat("###,###",
					symbols);
			chCurrencyFormat2.setGroupingSize(3);
			chCurrencyFormat2.setMaximumFractionDigits(0);
			chCurrencyFormat2.setRoundingMode(RoundingMode.DOWN);
			formattedString = chCurrencyFormat2.format(bd2);
		} catch (Exception e) {
			logger.error(String.format(
					"Unable to convert BigDecimal : %s to String. error : %s",
					new Object[] { bd, e.getMessage() }));
		}
		return formattedString;
	}

	/**
	 * 流程狀態顯示文字
	 * 
	 * @param status
	 * @return
	 */
	public static String getFlowStatusDesc(Integer status) {
		if(status == null) {
			return "";
		}
		
		String desc = "";
		
		switch(status){
		case -1:
			desc = "編輯中";
			break;
		case 0:
			desc = "正常結束";
			break;
		case 1:
			desc = "執行中";
			break;
		case 8:
			desc = "註銷";
			break;
		case 9:
			desc = "退件";
			break;
		default:
			desc = "未定義(o.status)";
		}
		
		return desc;
	}
	
	/**
	 * 幣別文字
	 * 
	 * @param cid
	 * @return
	 */
	public static String getCurrencyDesc(String cid) {
		if(cid == null) {
			return "";
		}
		
		String currency = "";
		
		switch(cid){
		case "TWD":
			currency = "新臺幣";
			break;
		case "HKD":
			currency = "港幣";
			break;
		case "USD":
			currency = "美金";
			break;
		case "CNY":
			currency = "人民幣";
			break;
		case "KRW":
			currency = "韓元";
			break;
		case "JPY":
			currency = "日圓";
			break;
		default:
			currency = cid;
		}
		
		return currency;
	}
	
	/**
	 * 顯示是(1)否
	 * 
	 * @param flag
	 * @return
	 */
	public static String getYesNoDesc(Integer flag) {
		if(flag == null) {
			flag = 0;
		}
		
		return flag == 1 ? "是" : "否";
	}
	
	/**
	 * 通報方式
	 * 
	 * @param channel
	 * @return
	 */
	public static String getChannelDesc(Integer channel) {
		if(channel == null) {
			return StringUtils.EMPTY;
		}
		
		String rtn = "";
		
		switch(channel) {
			case 0:
				rtn = "全部";
				break;
			case 1:
				rtn = "電話";
				break;
			case 2:
				rtn = "信件";
				break;
			default:
				rtn = String.format("不明(%s)", channel);
				break;
		}
		
		return rtn;
	}
	
	/**
	 * 中文數字大寫
	 * 
	 * @param number
	 * @return
	 */
	public static String getChineseNumber(int number) {
		return CHINESE_NUMBER[number];		
	}
}