package com.baplay.util;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;


public class CommonUtil {
	
	private static final Logger logger = Logger.getLogger(CommonUtil.class);

	private static RandomString etagGenerator = new RandomString(8);
	
	private static final String regEx_script = "<script[^>]*?>[\\s\\S]*?<\\/script>"; // 定义script的正则表达式
    private static final String regEx_style = "<style[^>]*?>[\\s\\S]*?<\\/style>"; // 定义style的正则表达式
    private static final String regEx_html = "<[^>]+>"; // 定义HTML标签的正则表达式

	/**
	 * 二分法查找 T 在 array 的位置
	 * @param array
	 * @param T
	 * @return
	 */
	public static int binarySearch(int[] array, int T) {
		int low, high, mid;
		low = 0;
		high = array.length - 1;
		while (low <= high) {
			mid = (low + high) / 2;
			if (array[mid] < T) {
				low = mid + 1;
			} else if (array[mid] > T) {
				high = mid - 1;
			} else {
				return mid;
			}
		}
		//System.out.println("high = "+high);
		//System.out.println("low = "+low);
		return high;
	}
	
	/**
	 * toMd5 Method
	 * Method Description :MD5加密算法
	 * @param inputValue 要加密的字符
	 * @param isLower true为小写,false为大写
	 * @return
	 */
	public static String toMd5(String inputValue, boolean isLower){
		if(inputValue==null || "".equals(inputValue))return "";
        try {
        	MessageDigest m=MessageDigest.getInstance("MD5");
			m.update(inputValue.getBytes("UTF8"));
			byte s[ ]=m.digest( );
		    String result="";
		    for(int i=0;i<s.length;i++){
		        result+=Integer.toHexString((0x000000ff & s[i]) | 0xffffff00).substring(6);
		    }
		    if(isLower){
		    	return result.toLowerCase();
		    }
		    return result.toUpperCase();
		} catch (UnsupportedEncodingException e) {
			return "";
		} catch (NoSuchAlgorithmException e) {
			return "";
		}
	}
	
	public static <E extends Enum<E>> boolean isContainEnum(Class<E> elementType, String s) {
		try {
			Enum.valueOf(elementType, s);
		} catch (IllegalArgumentException e) {
			return false;
		}
		return true;
	}
	
	@SuppressWarnings("rawtypes")
	public static <E extends Enum<E>> Enum getEnum(String enumType, Class<E> elementType) {
		try {
			return Enum.valueOf(elementType, enumType);
		} catch (IllegalArgumentException e) {
			return null;
		}
	}
	
	public static String randomUUID32() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
	
	public static String obj2JsonStr(Object obj) throws Exception {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		return ow.writeValueAsString(obj);
	}
	
	public static void delCache(String key, String className) throws Exception {
		String cacheKey = CommonUtil.toMd5(key, true);
		//如果class不存在默认的构造方法则会抛异常,Integer没有默认构造方法，所以需要转换成String
		CacheUtil.deleteCache(cacheKey, Class.forName(className).newInstance().getClass());
	}

	
	/**
	 * 去掉空格 回车 制表符
	 * @param str
	 * @return
	 */
	public static String replaceBlank(String str) {
		String dest = "";
		if (str != null) {
			Pattern p = Pattern.compile("\\s*|\t|\r|\n");
			Matcher m = p.matcher(str);
			dest = m.replaceAll("");
		}
		return dest;
	}
	
	public static String etag() {
        return etagGenerator.nextString();
    }
	
    /**
     * 去掉html标签
     * @param htmlStr
     * @return
     */
    public static String delHTMLTag(String htmlStr) {
        Pattern p_script = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);
        Matcher m_script = p_script.matcher(htmlStr);
        htmlStr = m_script.replaceAll(""); // 过滤script标签

        Pattern p_style = Pattern.compile(regEx_style, Pattern.CASE_INSENSITIVE);
        Matcher m_style = p_style.matcher(htmlStr);
        htmlStr = m_style.replaceAll(""); // 过滤style标签

        Pattern p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
        Matcher m_html = p_html.matcher(htmlStr);
        htmlStr = m_html.replaceAll(""); // 过滤html标签
        
        return htmlStr.trim(); // 返回文本字符串
    }

	/**
	 * 数字转字符， 并带上 “-”、“+”号
	 * @param num
	 * @param sign
	 * @return
	 */
	public static String numberToString(int num, String sign) {
		StringBuffer result = new StringBuffer();
		if (StringUtils.isBlank(sign)) {
			return String.valueOf(num);
		}
		result.append(sign).append(String.valueOf(num));
		return result.toString();
	}
	
	/**
	 * HMAC-SHA1加密
	 * @param data
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static byte[] getSignature(String data, String key) throws Exception {
		byte[] keyBytes = key.getBytes();
		SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");
		Mac mac = Mac.getInstance("HmacSHA1");
		mac.init(signingKey);
		byte[] rawHmac = mac.doFinal(data.getBytes());
		return rawHmac;
	}

	/**
	 * 字节数组转base64编码
	 * @param bytes
	 * @return
	 */
	public static String base64Encode(final byte[] bytes) {
		return new String(Base64.encodeBase64(bytes));
	}

	/**
	 * bean转 map
	 * @param obj
	 * @return
	 */
	public static Map<String, String> transBean2Map(Object obj) {
		if (obj == null) {
			return null;
		}
		Map<String, String> map = new HashMap<String, String>();
		try {
			BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
			PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
			for (PropertyDescriptor property : propertyDescriptors) {
				String key = property.getName();

				// 过滤class属性
				if (!key.equals("class")) {
					// 得到property对应的getter方法
					Method getter = property.getReadMethod();
					String value = (String) getter.invoke(obj);
					map.put(key, value);
				}
			}
		} catch (Exception e) {
			logger.info("transBean2Map Error " + e.getMessage());
		}
		return map;
	}

	/**
	 * map转bean
	 * @param map
	 * @param obj
	 */
	public static void transMap2Bean(Map<String, Object> map, Object obj) {
		try {
			BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
			PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
			for (PropertyDescriptor property : propertyDescriptors) {
				String key = property.getName();

				if (map.containsKey(key)) {
					Object value = map.get(key);
					// 得到property对应的setter方法
					Method setter = property.getWriteMethod();
					setter.invoke(obj, value);
				}
			}
		} catch (Exception e) {
			logger.info("transMap2Bean Error " + e.getMessage());
		}
		return;
	}

	public static boolean objectIsNotNull(Object object) {
		if (object != null && object.toString().trim().length() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 验证用户签名
	 * 
	 * @param platform 平台标识 
	 * @param uid 用户id
	 * @param sign 登录签名
	 * @param timestamp 登录时间戳
	 * @return true/false
	 */
	public static boolean validateSign(String platform, String uid, String sign, String timestamp) {
		Properties props = new Properties();
		String propFileName = "appkeys.properties";
		try {
			props.load(CommonUtil.class.getClassLoader().getResourceAsStream(propFileName));
		} catch (IOException e) {
			logger.info("The property file '" + propFileName + "' not found in the classpath.");
		}
		String appkey = props.getProperty(platform);
		if (StringUtils.isBlank(appkey)) {
			logger.info("The platform '" + platform + "' appkey not found in the property file '" + propFileName + "'.");
			return false;
		}
		String mySign = toMd5(appkey + uid + timestamp, false);
		return mySign.equalsIgnoreCase(sign);
	}


	/**
	 * 通过特定字符分割字符串，返回list
	 * 
	 * @param input
	 * @param reg
	 * @return
	 */
	public static List<String> split2List(String input, String reg) {
		if (input.indexOf(reg) == -1) {
			return null;
		}
		return Arrays.asList(input.split(reg));
	}

	/**
	 * 判断这个list里面是否包含传进来的枚举类，并列出
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <E extends Enum<E>> List<E> list2Enum(List<String> list,
			Class clazz) {
		if (list == null || list.size() == 0) {
			return null;
		}
		List<E> returnList = new ArrayList<E>();
		for (String s : list) {
			if (isContainEnum(clazz, s.toUpperCase())) {
				returnList.add((E) EnumUtils.getEnum(clazz, s.toUpperCase()));

			}
		}
		return returnList;
	}
	
	/**
	 * 验证是台湾手机号
	 * @param phone 手机号
	 */
	public static boolean MPhoneTaiwan(String phone){
		if(StringUtils.isEmpty(phone))return false;
		return phone.matches("09[0-9]{2}[0-9]{3}[0-9]{3}");
	}
	
	/**
	 * 生成随机验证码的方法
	 * @param type 类型 0 数字&字母，1 数字， 2 字母，
	 * 3 大写字母，4 小写字母，
	 * 5 大写字母&数字 6 小写字母&数字
	 * @param length 随机字符长度
	 * @return
	 */
	public static String createRandomCode(int type,int length){
		String d = "123567890";
		String S = "QWERTYUIOPASDFGHJKLZXCVBNM";
		String s = "qwertyuiopasdfghjklzxcvbnm";
		String pr;
		switch (type) {
		case 1:
			pr=d;
			break;
		case 2:
			pr=S+s;
			break;
		case 3:
			pr=S;
			break;
		case 4:
			pr=s;
			break;
		case 5:
			pr=S+d;
			break;
		case 6:
			pr=s+d;
			break;
		default:
			pr=d+s;
			break;
		}
		int prLenth = pr.length();
		StringBuffer result = new StringBuffer();
		Random r = new Random();
		for(int i=0;i<length;i++){
			result.append(pr.charAt(r.nextInt(prLenth)));
		}
		return String.valueOf(result);
	}
	
	/**
	 * 
	 * 生成活動編號 gameCode + currentTime + serverCode 長度最長20
	 * 
	 * @return
	 */
	public static String generateActivityId(String gameCode, String serverCode) {

		String reg = "[0-9]*";
		if(StringUtils.isEmpty(serverCode)||!serverCode.matches(reg)){
			serverCode = RandomUtils.nextInt(10000)+"";
		}
		String activityId = gameCode.toUpperCase().replace("_", "")
				+ (System.currentTimeMillis() + Long.valueOf(serverCode));
		for (int j = 0; j < 5; j++) {
			activityId = activityId + LETTER[RandomUtils.nextInt(26)];
		}
		if (activityId.length() >= 20) {
			activityId = activityId.substring(0, 19);
		}
		activityId = activityId + LETTER[RandomUtils.nextInt(26)];
		PrintUtil.outputItem("activityId", "gameCode ("+gameCode+") and userId ("+serverCode+") is "+activityId+" !");
		return activityId;
	}
	private static final String[] LETTER = new String[] { "A", "B", "C", "D",
		"E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q",
		"R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
	
	/**
	 * Collection contains
	 * 
	 * @param coll
	 * @param o
	 * @return
	 */
	public static boolean contains(Collection<?> coll, Object o) {
	    return coll.contains(o);
	}
}