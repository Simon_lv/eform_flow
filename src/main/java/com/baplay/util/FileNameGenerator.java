package com.baplay.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FileNameGenerator {
	
	private static SimpleDateFormat sdf_img = new SimpleDateFormat("yyyyMMddHHmmss");
	
	public static String byTime(String fileName){
		int idx = fileName.lastIndexOf("/")>0 ?fileName.lastIndexOf("/"): fileName.lastIndexOf("\\")+1;
		StringBuffer imageName = new StringBuffer();
		imageName.append(sdf_img.format(new Date())).append("_").append(fileName.substring(idx));
		return imageName.toString();
	}

}
