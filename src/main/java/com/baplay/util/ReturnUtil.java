package com.baplay.util;

/**
 * 处理controller返回值的常量
 *
 */
public class ReturnUtil {
	/**
	 * 游戏列表缓存时间（单位：天）
	 */
	public static final int GAME_LIST_TIME = 30;
	
	/**
	 * 公告列表缓存时间（单位：小時）
	 */
	public static final int ANNOUNCEMENT_CACHE_TIME = 1;
	
	/**
	 * Banner列表缓存时间（单位：小時）
	 */
	public static final int BANNER_CACHE_TIME = 1;
	
	
}
