package com.baplay.util;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BankNo {
	// https://www.fisc.com.tw/tc/download/twd.txt
	private static final String fileName = "file/twd.txt";  // 財金台幣匯款
	protected static final Logger log = LoggerFactory.getLogger(BankNo.class);
	
	private static BankNo instance = new BankNo();
	// 本國通匯行
	private Map<Bank, LinkedList<Branch>> twBank = new LinkedHashMap<Bank, LinkedList<Branch>>();
	
	private BankNo() {				
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());
		//int lineNo = 0;		
		Bank bank = null;
		
		try(Scanner scanner = new Scanner(file, "Big5")) {
			while(scanner.hasNextLine()) {				
				String line = scanner.nextLine();
				
				//lineNo++;
				//log.debug("line no=" + lineNo);								
				
				if(StringUtils.isBlank(line.trim())) {
					log.debug("empty line ending...");
					break;
				}
				
				String[] data = line.split("(\\s|　)+");
				//log.debug("line data=" + StringUtils.join(data, ","));
				
				String code = data[0];
				String fullName = data[1];
				String shortName = data.length < 3 ? StringUtils.EMPTY : data[2];
				
				if(code.length() == 3) {  // bank
					bank = new Bank(fullName, shortName, code);
					twBank.put(bank, new LinkedList<Branch>());
				}
				else {  // branch
					// rip remark
					fullName = fullName.replaceAll("（.*）", "");
					
					LinkedList<Branch> branchList = twBank.get(bank);
					branchList.add(new Branch(StringUtils.contains(bank.getFullName(), fullName) ? bank.getFullName() : (bank.getFullName() + fullName), fullName, code));
				}					
			}

			scanner.close();						
		}
		catch(Exception e) {
			log.error("err", e);
		}		
	}
	
	public static BankNo getInstance() {
		return instance;
	}
		
	/**
	 * 本國銀行
	 * 
	 * @return
	 */
	public Map<Bank, LinkedList<Branch>> getTwBank() {
		return twBank;
	}
	
	/**
	 * 本國銀行
	 * 
	 * @param bankCode
	 * @return
	 */
	public Bank getTwBank(String bankCode) {
		for(Bank bank : twBank.keySet()) {
			if(StringUtils.equals(bank.getCode(), bankCode)) {
				return bank;
			}
		}
		
		return null;
	}
	
	/**
	 * 本國分行
	 * 
	 * @param bankCode
	 * @return
	 */
	public LinkedList<Branch> getTwBranch(String bankCode) {
		return twBank.get(new Bank(StringUtils.EMPTY, StringUtils.EMPTY, bankCode));
	}	
		
	/**
	 * 銀行
	 * 
	 * @author Eric
	 *
	 */
	class Bank implements Comparable<Bank> {
		String fullName;
		String shortName;
		String code;
		
		public Bank(String fullName, String shortName, String code) {
			this.fullName = fullName;
			this.shortName = shortName;
			this.code = code;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getFullName() {
			return fullName;
		}
		
		public String getShortName() {
			return shortName;
		}
		
		@Override
		public int hashCode() {
			return code.hashCode();
		}
		
		@Override
		public boolean equals(Object o) {
			return code.equals(((Bank)o).getCode());
		}
		
		@Override
		public int compareTo(Bank that) {
	        return getCode().compareTo(that.getCode());
	    }
	}
	
	/**
	 * 分行
	 * 
	 * @author Eric
	 *
	 */
	class Branch {
		String fullName;
		String shortName;
		String code;
		
		public Branch(String fullName, String shortName, String code) {
			this.fullName = fullName;
			this.shortName = shortName;
			this.code = code;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getFullName() {
			return fullName;
		}
		
		public String getShortName() {
			return shortName;
		}
		
		@Override
		public int hashCode() {
			return code.hashCode();
		}
		
		@Override
		public boolean equals(Object o) {
			return code.equals(((Branch)o).getCode());
		}
	}
}