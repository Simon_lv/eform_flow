package com.baplay.service;

import java.util.List;
import java.util.Map;

import org.springside.modules.orm.Page;

import com.baplay.dao.mapper.DepartmentTop;
import com.baplay.entity.Department;
import com.baplay.form.DepartmentForm;

public interface IDepartmentManager {
	public Department add(Department department);
	public int update(Department department);
	public Department findOneById(Long id);
	public Page<Department> list(DepartmentForm departmentForm);
	public int delete(Long id, String modifier);
	public int countLiveChild(Long id);
	public List<Department> findAllByLevel(int level);
	public boolean checkDeptNoExist(Long companyId, Long id, String deptNo);
	public List<Department> findAllDepartment();
	public List<Department> findAllDepartment(Long companyId);
	public Map<String, DepartmentTop> findAllDepartmentTop();
	public List<Map<String, Object>> deptList(long companyId);
	
	public Department findOneByUserId(Long id);
	public Department findOneByFullName(String fullName, long companyId);
	public Department findOneByDeptNo(String deptNo, long companyId);
	public List<Department> findDepartmentCN();
}