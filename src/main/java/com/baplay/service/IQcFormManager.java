package com.baplay.service;

import java.util.List;

import org.springside.modules.orm.Page;

import com.baplay.entity.QcForm;
import com.baplay.entity.TEfunuser;
import com.baplay.form.QcFormForm;

public interface IQcFormManager {

	public Page<QcForm> list(QcFormForm qcFormForm);
	public QcForm add(QcForm qcForm, String[] subTitle, String[] qcResult, String[] pmResult, String[] remark, 
			TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception;
	public int update(QcForm qcForm, String[] subTitle, String[] qcResult, String[] pmResult, String[] remark,
			TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception;
	public List<QcForm> findAllByFormId(Long formId);
	public List<QcForm> findAllByFlow(Long workFlowId);
	public QcForm add(QcForm qcForm);	
	public int update(QcForm qcForm);
	public void update(List<QcForm> formList);
	public QcForm findOneByFieldId(Long formId, Long fieldId);
	public int updateResult(Long formId, Long fieldId, Integer result, String remark);
}