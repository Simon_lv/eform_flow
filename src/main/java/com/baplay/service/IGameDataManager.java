package com.baplay.service;

import java.util.List;

import org.springside.modules.orm.Page;

import com.baplay.entity.GameData;
import com.baplay.form.GameDataForm;

public interface IGameDataManager {

	public GameData findOneById(Long id);
	public List<GameData> findAllGameData();
	public GameData add(GameData gameData);
	public int update(GameData gameData);
	public int delete(Long id);
	public Page<GameData> list(GameDataForm gameDataForm);
}