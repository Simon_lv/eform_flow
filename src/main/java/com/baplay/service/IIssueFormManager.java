package com.baplay.service;

import java.util.List;

import org.springside.modules.orm.Page;

import com.baplay.entity.IssueForm;
import com.baplay.entity.IssueMemo;
import com.baplay.entity.TEfunuser;
import com.baplay.form.IssueFormForm;

public interface IIssueFormManager {
	public IssueForm add(IssueForm form, String[] url, String[] desc, TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception;
	public int update(IssueForm form, String[] url, String[] desc, TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception;
	public IssueForm findOneById(Long id);
	public Page<IssueForm> list(IssueFormForm issueFormForm);
	public int delete(Long id);
	public IssueForm findOneByFlow(Long workFlowId);
	public int update(IssueForm form);
	public void memo(Long issueFormId, Long uid, String memo);
	public List<IssueMemo> findMemo(Long issueFormId);
}