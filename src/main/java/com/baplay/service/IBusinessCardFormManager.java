package com.baplay.service;

import javax.servlet.http.HttpServletRequest;

import org.springside.modules.orm.Page;

import com.baplay.entity.BusinessCardForm;
import com.baplay.entity.TEfunuser;
import com.baplay.form.BusinessCardFormForm;

public interface IBusinessCardFormManager {
	public BusinessCardForm add(HttpServletRequest request,BusinessCardForm businessCardForm, TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception;
	public int update(HttpServletRequest request,BusinessCardForm businessCardForm, TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception;
	public BusinessCardForm findOneById(Long id);
	public Page<BusinessCardForm> list(BusinessCardFormForm businessCardFormForm);
	public int updateTotal(Long id, Long editId, String formType);	
	public BusinessCardForm findOneByFlow(Long workFlowId);
	public int delete(Long id);
}
