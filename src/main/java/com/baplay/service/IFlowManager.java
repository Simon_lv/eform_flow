package com.baplay.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springside.modules.orm.Page;

import com.baplay.dto.BaseForm;
import com.baplay.dto.FormFlow;
import com.baplay.dto.Stamp;
import com.baplay.entity.FlowStep;
import com.baplay.entity.FormType;
import com.baplay.entity.Role;
import com.baplay.entity.TEfunuser;
import com.baplay.entity.UserFlowStep;
import com.baplay.entity.WorkFlow;
import com.baplay.entity.WorkFlowDetail;
import com.baplay.form.FormMgrForm;


public interface IFlowManager {

	/**
	 * 草稿存檔
	 * 
	 * @param creator 用戶ID
	 * @param formType 表單類別
	 * @param formCode 表單簡碼
	 * @param workFlowId (新建草檔時=-1)
	 * @return workFlowId (草稿流程ID)
	 */
	public long saveDraft(BaseForm form, long userId, String formType, String formCode, long workFlowId) throws Exception;
	
	/**
	 * 刪除草稿
	 * 
	 * @param workFlowId
	 */
	public void deleteDraft(long workFlowId) throws Exception;
	
	/**
	 * 鎖定表單
	 * 
	 * @param workFlowDetailId
	 * @param ownerId
	 * @return 
	 */
	public boolean lockFlow(long workFlowId, long workFlowDetailId, long userId) throws Exception;
	
	/**
	 * 取消鎖定
	 * 
	 * @param workFlowDetailId
	 * @return
	 */
	public void lockAbort(long workFlowDetailId) throws Exception;
	
	/**
	 * 發動流程
	 * 
	 * @param formType
	 * @param formCode
	 * @param serialNo
	 * @param user
	 * @param workFlowId (草稿流程ID)
	 * @return workFlowId (正式流程ID)
	 */
	public long startFlow(String formType, String formCode, String serialNo, TEfunuser user, long workFlowId, BaseForm form) throws Exception;
	
	/**
	 * 審核
	 * 
	 * @param workFlowDetailId
	 * @param user
	 */
	public void verifyFlow(long workFlowDetailId, TEfunuser user) throws Exception;
	
	/**
	 * 放行
	 * 
	 * @param workFlowDetailId
	 * @param user
	 */
	public void passFlow(long workFlowDetailId, TEfunuser user) throws Exception;
	
	/**
	 * 會簽
	 * 
	 * @param workFlowDetailId
	 * @param user
	 * @throws Exception
	 */
	public void countersign(long workFlowDetailId, TEfunuser user) throws Exception;
	
	/**
	 * 代辦事項清單
	 * 
	 * @param departmentId
	 * @param userId
	 * @return
	 */
	public List<FormFlow> todoList(Set<Long> companyId, Set<Long> departmentId, long userId, List<Role> roles);
	
	/**
	 * 代辦事項明細
	 * 
	 * @param departmentId
	 * @param userId
	 * @param formType
	 * @return
	 */
	public List<FormFlow> todoListType(Set<Long> companyId, Set<Long> departmentId, long userId, List<Role> roles, String formType);
	
	/**
	 * 退件
	 * 
	 * @param workFlowDetailId
	 * @param userId
	 * @param memo
	 */
	public void rejectFlow(long workFlowDetailId, long userId, String memo) throws Exception;
	
	/**
	 * 註銷
	 * 
	 * @param workFlowDetailId
	 * @param userId
	 */
	public void terminateFlow(long workFlowId, long workFlowDetailId, long userId, String memo) throws Exception;
	
	/**
	 * 審核歷程
	 * 
	 * @param workFlowId
	 * @return
	 */
	public List<FormFlow> flowHistory(long workFlowId);
	
	/**
	 * 表單執行流程
	 * 
	 * @param workFlowId
	 * @return
	 */
	public WorkFlow findFormFlow(long workFlowId);
	
	/**
	 * 表單流程步驟
	 * 
	 * @param workFlowDetailId
	 * @return
	 */
	public WorkFlowDetail findFormFlowStep(long workFlowDetailId);
	
	/**
	 * 表單類別
	 * 
	 * @param groupCode
	 * @return
	 */
	public FormType findFomType(String code, String groupCode, Integer... flowSchemaId);
	
	/**
	 * 蓋章清單(前=>後)
	 * 
	 * @param workFlowId
	 * @return
	 */
	public Map<String, Stamp> findFormStamp(long workFlowId);
	
	/**
	 * FlowStep
	 * 
	 * @param id
	 * @return
	 */
	public FlowStep findFlowStepById(long id);
	
	/**
	 * 最後一筆明細
	 * 
	 * @param workFlowId
	 * @return
	 */
	public WorkFlowDetail findLastOneById(long workFlowId);
	
	/**
	 * 退件重送註銷已用印之簽章
	 * 
	 * @param workFlowId
	 */
	public void cancelStampUsed(long workFlowId);
	
	public UserFlowStep addUserFlowStep(UserFlowStep userFlowStep);
	
	public UserFlowStep findUserFlowStep(long workFlowId, long stepId, long userId);
	
	public FlowStep findNextStepById(Long id);
	
	/**
	 * 查詢全表單
	 * 
	 * @param form
	 * @return
	 */
	public Page<FormFlow> list(FormMgrForm form);
	
	/**
	 * 全表單種類
	 * 
	 * @return
	 */
	public List<FormType> findAllFormType();
}