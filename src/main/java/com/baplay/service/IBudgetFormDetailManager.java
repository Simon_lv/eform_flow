package com.baplay.service;

import java.util.List;

import com.baplay.entity.BudgetFormDetail;

public interface IBudgetFormDetailManager {

	/**
	 * 
	 * @param budgetFormId
	 * @param additional  是否追加  是(1)否(2)
	 * @return
	 */
	public List<BudgetFormDetail> findFormDetail(Long budgetFormId, int additional);
	public int delete(Long id, Long editId);
	
}
