package com.baplay.service;

import java.util.List;
import java.util.Set;

import org.springside.modules.orm.Page;

import com.baplay.dao.mapper.FormUser;
import com.baplay.entity.Employee;
import com.baplay.form.EmployeeForm;

public interface IEmployeeManager {
	public Employee addEmployee(Employee employee);
	public int updateEmployee(Employee employee);
	public Employee findOneById(Long id);
	public Page<Employee> list(EmployeeForm employeeForm);
	public int delete(Long id, Long modifier);
	public int delete(Long userId, String employeeNo, Long modifier);
	public List<Employee> findAllEmployee();
	public List<Employee> findAllEmployee(int companyId);
	public List<Employee> findAllEmployee(int companyId, int departmentId);
	public List<Employee> findAllEmployee2();
	public boolean checkEmpNoExist(Long compnayId, Long id, String employeeNo);
	public Employee findOneByName(String name);
	public Set<String> findEmployeeEmailByRole(int roleId, int...extraParam);
	public Employee findOneByUID(int uid);
	public List<FormUser> findIssueRepairUser(Integer... roleId);
	public List<Employee> findSupervisor(Long departmentId);
	public List<Long> findUserManageDept(Long uid);
	public Employee findOneByEmployeeNo(String employeeNo);
	public Employee findOneByEmployeeNo(Long companyId, String employeeNo);
}