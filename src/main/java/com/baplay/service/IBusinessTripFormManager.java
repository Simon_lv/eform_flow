package com.baplay.service;

import org.springside.modules.orm.Page;

import com.baplay.entity.BusinessTripForm;
import com.baplay.entity.TEfunuser;
import com.baplay.form.BusinessTripFormForm;

public interface IBusinessTripFormManager {
	public BusinessTripForm add(BusinessTripForm businessTripForm, String url[], String desc[], TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception;
	public int update(BusinessTripForm businessTripForm, String url[], String desc[], TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception;
	public BusinessTripForm findOneById(Long id);
	public Page<BusinessTripForm> list(BusinessTripFormForm businessTripFormForm);
	public int delete(Long id);
	public BusinessTripForm findOneByFlow(Long workFlowId);
}