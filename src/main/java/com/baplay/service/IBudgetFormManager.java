package com.baplay.service;

import javax.servlet.http.HttpServletRequest;

import org.springside.modules.orm.Page;

import com.baplay.entity.BudgetForm;
import com.baplay.entity.TEfunuser;
import com.baplay.form.BudgetFormForm;

public interface IBudgetFormManager {
	public BudgetForm add(HttpServletRequest request, BudgetForm budgetForm, TEfunuser user, boolean isSubmit, boolean isAdditional, String formType, String formCode) throws Exception;
	public int update(HttpServletRequest request, BudgetForm budgetForm, TEfunuser user, boolean isSubmit, boolean isAdditional, String formType, String formCode) throws Exception;
	public BudgetForm findOneById(Long id);
	public Page<BudgetForm> list(BudgetFormForm budgetFormForm);
	public int delete(Long id);
	public BudgetForm findOneByFlow(Long workFlowId);
	public boolean isFirstBudget(Long id);
}
