package com.baplay.service;

import java.util.List;

import com.baplay.entity.Function;

public interface IFunctionManager {

	/**
	 * 保存新權限
	 * 
	 * @param fun
	 */
	public void saveFunction(Function fun);

	/**
	 * 更新新權限
	 * 
	 * @param fun
	 */
	public void updateFunction(Function fun);

	/**
	 * 刪除權限
	 * 
	 * @param fun
	 */
	public void deleteFunction(Function fun);

	/**
	 * 查詢二列表
	 * 
	 * @param parentId
	 *            菜單ID為null時可查詢一級菜單
	 * @return
	 */
	public List<Function> queryFunctionList(Long parentId);

	/**
	 * 查詢單個權限
	 * 
	 * @param id
	 * @return
	 */
	public Function queryFunction(Long id);
}
