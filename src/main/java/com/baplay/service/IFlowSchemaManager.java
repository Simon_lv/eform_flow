package com.baplay.service;

import java.util.List;

import org.springside.modules.orm.Page;

import com.baplay.entity.FlowSchema;
import com.baplay.entity.FlowStep;
import com.baplay.form.FlowMgrForm;

public interface IFlowSchemaManager {

	/**
	 * 全部表單流程
	 * 
	 * @return
	 */
	public List<FlowSchema> findAllFlowSchema(Long... companyId);
	
	/**
	 * 分頁查詢
	 * 
	 * @param form
	 * @return
	 */
	public Page<FlowSchema> list(FlowMgrForm form);
	
	/**
	 * 流程步驟
	 * 
	 * @param flowSchemaId
	 * @return
	 */
	public List<FlowStep> findFlowStepBySchema(long flowSchemaId);
	
	/**
	 * 流程定義
	 * 
	 * @param id
	 * @return
	 */
	public FlowSchema findOneById(Long id);
	
	/**
	 * 變更角色
	 * 
	 * @param flowStepId
	 * @param formRoleId
	 * @return
	 */
	public boolean updateRole(Long flowStepId, Long formRoleId);
	
	/**
	 * 變更會簽
	 * 
	 * @param flowStepId
	 * @param countersign
	 * @return
	 */
	public boolean updateCountersign(Long flowStepId, String countersign);
}