package com.baplay.service;

import java.util.List;

import com.baplay.entity.Attachment;

public interface IAttachmentManager {

	public Attachment add(Attachment attachment);
	public int delete(Long id);
	public int deleteAll(Long contractReviewFormid, String formType);
	public List<Attachment> findFormAttachment(Long formid, String formType);
	
	/**
	 * 新增附件
	 * 
	 * @param url
	 * @param desc
	 * @param formId
	 * @param formType
	 * @param uid
	 */	
	public void add(String[] url, String[] desc, Long formId, String formType, Long uid);
}
