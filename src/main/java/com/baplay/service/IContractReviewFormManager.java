package com.baplay.service;

import org.springside.modules.orm.Page;

import com.baplay.entity.ContractReviewForm;
import com.baplay.entity.TEfunuser;
import com.baplay.form.ContractReviewFormForm;

public interface IContractReviewFormManager {
	public ContractReviewForm add(ContractReviewForm contractReviewForm, String url[], String[] desc, String memo[], TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception;
	public int update(ContractReviewForm contractReviewForm, String url[], String[] desc, String memo[], TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception;
	public ContractReviewForm findOneById(Long id);
	public Page<ContractReviewForm> list(ContractReviewFormForm contractReviewFormForm);
	public int delete(Long id);
	public ContractReviewForm findOneByFlow(Long workFlowId);	
}
