package com.baplay.service;

import java.util.Date;

public interface IRemitManager {

	public int confirmRemitDate(String formType, Long id);
	
	public int updateRemitDate(String formType, Long id, Date remitDate);
}
