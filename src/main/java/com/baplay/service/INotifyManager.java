package com.baplay.service;

public interface INotifyManager {

	/**
	 * 流程通知
	 * 
	 * @param workFlowId
	 * @throws Exception
	 */
	public void notify(long workFlowId) throws Exception;
	
	public String getFormContent(String flowFormType, long id, String... params);
}
