package com.baplay.service;

import java.util.List;
import java.util.Map;

import org.springside.modules.orm.Page;

import com.baplay.entity.JobTitle;
import com.baplay.form.JobTitleForm;

public interface IJobTitleManager {
	public JobTitle add(JobTitle jobTitle);
	public int update(JobTitle jobTitle);
	public JobTitle findOneById(Long id);
	public Page<JobTitle> list(JobTitleForm jobTitleForm);
	public int delete(Long id, Long long1);
	public List<JobTitle> findAllJobTitle();
	public List<JobTitle> findAllJobTitle(Long companyId);
	public boolean checkSerialNumberExist(Long id, String titleNo);
	public List<Map<String, Object>> jobList(long companyId);
	public JobTitle findOneByNo(String titleNo, long companyId);
}