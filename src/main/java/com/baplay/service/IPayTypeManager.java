package com.baplay.service;

import java.util.List;

import org.springside.modules.orm.Page;

import com.baplay.entity.PayType;
import com.baplay.form.PayTypeForm;

public interface IPayTypeManager {

	public PayType findOneById(Long id);
	public List<PayType> findAllPayType();
	public PayType add(PayType payType);
	public int update(PayType payType);
	public int delete(Long id);
	public Page<PayType> list(PayTypeForm payTypeForm);
}