package com.baplay.service;

import java.util.List;

import org.springside.modules.orm.Page;

import com.baplay.entity.QcFormSchema;
import com.baplay.form.QcFormMgrForm;

public interface IQcFormSchemaManager {

	public List<QcFormSchema> findAllFormSchema();
	public QcFormSchema findOneById(Long id);
	public Page<QcFormSchema> list(QcFormMgrForm qcFormMgrForm);
	public QcFormSchema add(QcFormSchema qcFormSchema);
	public int update(QcFormSchema qcFormSchema);
	
}
