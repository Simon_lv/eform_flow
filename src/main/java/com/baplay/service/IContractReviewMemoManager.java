package com.baplay.service;

import java.util.List;

import com.baplay.entity.ContractReviewMemo;

public interface IContractReviewMemoManager {

	public ContractReviewMemo add(ContractReviewMemo cibtractRevuewNeni);
	public int deleteAll(Long contractReviewFormId);
	public List<ContractReviewMemo> findContractReviewMemo(Long contractReviewFormId);
	public void add(long formId, long userId, String formCode, String url[], String memo[]);
}