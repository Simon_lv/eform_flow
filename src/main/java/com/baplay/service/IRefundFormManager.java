package com.baplay.service;

import org.springside.modules.orm.Page;

import com.baplay.entity.RefundForm;
import com.baplay.entity.TEfunuser;
import com.baplay.form.RefundFormForm;

public interface IRefundFormManager {
	public RefundForm add(RefundForm refundForm, TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception;
	public int update(RefundForm refundForm, TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception;
	public RefundForm findOneById(Long id);
	public Page<RefundForm> list(RefundFormForm refundFormForm);
	public int delete(Long id);
	public RefundForm findOneByFlow(Long workFlowId);
	public int update(RefundForm refundForm);
}
