package com.baplay.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.baplay.entity.QcFormTitleField;

public interface IQcFormTitleFieldManager {

	public List<QcFormTitleField> findAll(Long id, Long... formId);
	public QcFormTitleField add(HttpServletRequest request, QcFormTitleField qcFormTitleField);
	public int delete(Long id);
	
}
