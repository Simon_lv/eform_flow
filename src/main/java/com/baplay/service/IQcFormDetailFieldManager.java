package com.baplay.service;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.baplay.entity.QcFormDetailField;

public interface IQcFormDetailFieldManager {

	public int add(HashMap<String, Object> map);
	public QcFormDetailField add(HttpServletRequest request, QcFormDetailField qcFormDetailField);
	public List<QcFormDetailField> findAll(Long id, Long formId);
	public List<QcFormDetailField> findAll(Long id);
	public List<QcFormDetailField> findAllGroupLabel(Long id);
	public int findGroupCount(Long id);
	public List<Integer> findAllFieldCount(Long id);
	public int deleteGroup(Long id);
	public int delete(Long id);
	
}
