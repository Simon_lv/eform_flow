package com.baplay.service;

import java.util.List;

import org.springside.modules.orm.Page;

import com.baplay.entity.Receipt;
import com.baplay.form.ReceiptForm;

public interface IReceiptManager {
	public Receipt add(Receipt receipt);
	public int update(Receipt receipt);
	public Receipt findOneById(Long id);
	public Page<Receipt> list(ReceiptForm receiptForm);	
	
	/**
	 * 
	 * @param id
	 * @param formType
	 * @param costType 1:costType=1,2:costType2
	 * @return
	 */
	public List<Receipt> findAllByForm(Long id, String formType, Integer costType, String... params);
}
