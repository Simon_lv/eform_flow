package com.baplay.service;

import java.util.List;
import java.util.Map;

import com.baplay.entity.Employee;
import com.baplay.entity.FormRole;
import com.baplay.entity.Function;
import com.baplay.entity.Role;
import com.baplay.entity.TEfunuser;
import com.baplay.form.RoleForm;
import com.baplay.form.UserForm;

public interface IPermissionManager {
	
	public TEfunuser findByUserName(String userName);

	public List<Function> getFunctions(long roleId, int all);

	public List<Role> getRole(long userId);
	
	public List<FormRole> getFormRole(long userId);

	public void addRole(RoleForm form);

	public void updateRole(RoleForm form);

	public TEfunuser addUser(UserForm form);

	public void updateUser(UserForm form);

	public void updatePwd(long userId, String password);

	public void updateRoleStatus(Long roleId, Integer status);

	public void updateUserStatus(Integer status, Long userId);

	public List<Role> getRoles(int all);
	
	public List<FormRole> getFormRoles(int all);

	public List<TEfunuser> getUsers(String userName);
	
	public UserForm getUser(Long id);
	
	public boolean checkLoginAccountExist(String loginAccount);
	
	public Long findByEmployeeId(Long employeeId);
	
	/**
	 * UID查詢合併多個員工ID
	 * 
	 * @param userId
	 * @return
	 */
	public List<Long> findMoreEmployeeByUID(long userId); 
	
	/**
	 * 新增用戶員工對應
	 * 
	 * @param userId
	 * @param employeeId
	 */
	public void addUserEmpMapping(Long userId, Long employeeId);
	
	/**
	 * 全部用戶員工對應
	 * 
	 * @return
	 */
	public Map<Long, List<Long>> allUserEmpMapping(List<TEfunuser> userList);
	
	/**
	 * 註銷離職員工
	 * 
	 * @param employee
	 */
	public void employeeResigned(Employee employee);
}