package com.baplay.service;

import java.util.Set;

public interface IFormAuthManager {

	/**
	 * 表單權限
	 * 
	 * @param formType
	 * @param userFormRole
	 * @return
	 */
	public boolean formAuth(String formType, Set<String> userFormRole);
}
