package com.baplay.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baplay.dao.IQcFormDetailFieldDao;
import com.baplay.entity.QcFormDetailField;
import com.baplay.service.IQcFormDetailFieldManager;

@Service
@Transactional
public class QcFormDetailFieldManager implements IQcFormDetailFieldManager {

	@Autowired
	private IQcFormDetailFieldDao qcFormDetailFieldDao;
	
	@Override
	public int add(HashMap<String, Object> map) {
		return qcFormDetailFieldDao.add(map);
	}

	@Override
	public List<QcFormDetailField> findAll(Long id, Long formId) {		
		return qcFormDetailFieldDao.findAll(id, formId);
	}
	
	@Override
	public List<QcFormDetailField> findAll(Long id) {		
		return qcFormDetailFieldDao.findAll(id);
	}

	@Override
	public int findGroupCount(Long id) {
		return qcFormDetailFieldDao.findGroupCount(id);
	}

	@Override
	public List<Integer> findAllFieldCount(Long id) {
		return qcFormDetailFieldDao.findAllFieldCount(id);
	}

	@Override
	public List<QcFormDetailField> findAllGroupLabel(Long id) {
		return qcFormDetailFieldDao.findAllGroupLabel(id);
	}	

	@Override
	public QcFormDetailField add(HttpServletRequest request, QcFormDetailField qcFormDetailField) {
		String id[] = request.getParameterValues("id");
		String groupLabel[] = request.getParameterValues("groupLabel");
		String fieldLabel[] = request.getParameterValues("fieldLabel");
		String fieldOrder[] = request.getParameterValues("fieldOrder");
		
		for(int i=0;i<id.length;i++){
			qcFormDetailField.setGroupLabel(groupLabel[i]);
			
			if(StringUtils.isNotBlank(id[i])){
				qcFormDetailField.setId(Long.parseLong(id[i]));
			}else{
				qcFormDetailField.setId(null);
			}
			if(fieldLabel != null){
				qcFormDetailField.setFieldLabel(fieldLabel[i]);
			}else{
				qcFormDetailField.setFieldLabel(null);
			}
			if(fieldOrder != null){
				if(StringUtils.isNotBlank(fieldOrder[i])){
					qcFormDetailField.setFieldOrder(Integer.parseInt(fieldOrder[i]));
				}else{
					qcFormDetailField.setFieldOrder(null);
				}
			}
			
			if(qcFormDetailField.getId() == null){
				qcFormDetailField = qcFormDetailFieldDao.add(qcFormDetailField);
			}else{				
				if(qcFormDetailField.getFieldLabel() == null){
					qcFormDetailFieldDao.updateGroup(qcFormDetailField);
				}else{
					qcFormDetailFieldDao.update(qcFormDetailField);
				}
			}
		}
		
		return qcFormDetailField;
	}

	@Override
	public int deleteGroup(Long id) {
		return qcFormDetailFieldDao.deleteGroup(id);
	}

	@Override
	public int delete(Long id) {
		return qcFormDetailFieldDao.delete(id);
	}

}
