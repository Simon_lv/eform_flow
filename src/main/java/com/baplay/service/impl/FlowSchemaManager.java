package com.baplay.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IFlowSchemaDao;
import com.baplay.dao.IFlowStepDao;
import com.baplay.entity.FlowSchema;
import com.baplay.entity.FlowStep;
import com.baplay.form.FlowMgrForm;
import com.baplay.service.IFlowSchemaManager;

@Service
@Transactional
public class FlowSchemaManager implements IFlowSchemaManager {
	
	protected static final Logger log = LoggerFactory.getLogger(FlowSchemaManager.class);
	
	@Autowired
	private IFlowSchemaDao flowSchemaDao;
	@Autowired
	private	IFlowStepDao flowStepDao;

	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#findAllFlowSchema()
	 */
	@Override
	public List<FlowSchema> findAllFlowSchema(Long... companyId) {
		return flowSchemaDao.findAll(companyId);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowSchemaManager#list(com.baplay.form.FlowMgrForm)
	 */
	@Override
	public Page<FlowSchema> list(FlowMgrForm form) {
		return flowSchemaDao.list(form);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowSchemaManager#findFlowStepBySchema(long)
	 */
	@Override
	public List<FlowStep> findFlowStepBySchema(long flowSchemaId) {
		return flowStepDao.findFlowStepBySchema(flowSchemaId);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowSchemaManager#findOneById(java.lang.Long)
	 */
	@Override
	public FlowSchema findOneById(Long id) {
		return flowSchemaDao.findOneById(id);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowSchemaManager#updateRole(java.lang.Long, java.lang.Long)
	 */
	@Override
	public boolean updateRole(Long flowStepId, Long formRoleId) {
		return flowStepDao.updateRole(flowStepId, formRoleId) > 0;
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowSchemaManager#updateCountersign(java.lang.Long, java.lang.String)
	 */
	@Override
	public boolean updateCountersign(Long flowStepId, String countersign) {
		return flowStepDao.updateCountersign(flowStepId, countersign) > 0;
	}
}