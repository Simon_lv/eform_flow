package com.baplay.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baplay.dao.ISerialNoDao;
import com.baplay.service.ISerialNoManager;

@Service
@Transactional
public class SerialNoManager implements ISerialNoManager {

	@Autowired
	private ISerialNoDao serialNoDao;
	
	@Override
	public String findSerialNo(String formCode) {		
		return serialNoDao.findSerialNo(formCode);
	}

}
