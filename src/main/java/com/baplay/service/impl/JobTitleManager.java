package com.baplay.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IJobTitleDao;
import com.baplay.entity.JobTitle;
import com.baplay.form.JobTitleForm;
import com.baplay.service.IJobTitleManager;

@Service
@Transactional
public class JobTitleManager implements IJobTitleManager {
	@Autowired
	private IJobTitleDao jobTitleDao;
	
	@Override
	public JobTitle add(JobTitle jobTitle) {
		return jobTitleDao.add(jobTitle);
	}

	@Override
	public int update(JobTitle jobTitle) {
		return jobTitleDao.update(jobTitle);
	}

	@Override
	public JobTitle findOneById(Long id) {
		return jobTitleDao.findOneById(id);
	}

	@Override
	public Page<JobTitle> list(JobTitleForm jobTitleForm) {
		return jobTitleDao.list(jobTitleForm);
	}

	@Override
	public int delete(Long id, Long modifier) {
		return jobTitleDao.delete(id, modifier);
	}

	@Override
	public List<JobTitle> findAllJobTitle() {
		return jobTitleDao.findAllJobTitle();
	}
	
	@Override
	public List<JobTitle> findAllJobTitle(Long companyId) {
		return jobTitleDao.findAllJobTitle(companyId);
	}

	@Override
	public boolean checkSerialNumberExist(Long id, String titleNo) {
		return jobTitleDao.checkSerialNumberExist(id, titleNo);
	}

	@Override
	public List<Map<String, Object>> jobList(long companyId) {
		return jobTitleDao.jobList(companyId);
	}
	
	@Override
	public JobTitle findOneByNo(String titleNo, long companyId) {
		return jobTitleDao.findOneByNo(titleNo, companyId);
	}
}