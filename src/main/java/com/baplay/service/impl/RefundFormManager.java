package com.baplay.service.impl;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IRefundFormDao;
import com.baplay.dao.ISerialNoDao;
import com.baplay.dao.IWorkFlowDao;
import com.baplay.entity.RefundForm;
import com.baplay.entity.TEfunuser;
import com.baplay.form.RefundFormForm;
import com.baplay.service.IFlowManager;
import com.baplay.service.IRefundFormManager;

@Service
@Transactional
public class RefundFormManager implements IRefundFormManager {

	protected static final Logger log = LoggerFactory.getLogger(RefundFormManager.class);

	@Autowired
	private IRefundFormDao refundFormDao;
	@Autowired
	private ISerialNoDao serialNoDao;
	@Autowired
	private IFlowManager flowManager;
	@Autowired
	private IWorkFlowDao workFlowDao;

	public RefundForm add(RefundForm refundForm, TEfunuser user, boolean isSubmit,
			String formType, String formCode) throws Exception {
		
		if(StringUtils.isBlank(refundForm.getPaymentWay())){
			refundForm.setPaymentWay("");
		}
		if(StringUtils.isBlank(refundForm.getOpsProcess())){
			refundForm.setOpsProcess("");
		}
		if(StringUtils.isBlank(refundForm.getFinProcess())){
			refundForm.setFinProcess("");
		}
		if(StringUtils.isBlank(refundForm.getAcctProcess())){
			refundForm.setAcctProcess("");
		}		
		if(refundForm.getNotifyAgency() == null){
			refundForm.setNotifyAgency(0);
		}
		
		// Flow
		long workFlowId = -1;

		if (isSubmit) { // 送審
			String serialNo = serialNoDao.findSerialNo(formCode);
			refundForm.setSerialNo(serialNo);

			// flow
			workFlowId = flowManager.startFlow(formType, formCode, serialNo, user, -1, refundForm);			
		} else { // 草稿
			// draft
			refundForm.setSerialNo(StringUtils.EMPTY);
			workFlowId = flowManager.saveDraft(refundForm, user.getId(), formType, formCode, -1);
		}				
		
		refundForm.setWorkFlowId(workFlowId);
		RefundForm rf = refundFormDao.add(refundForm);

		// Update formId to workFlow
		workFlowDao.update(workFlowId, rf.getId());
		
		return rf;
	}

	public int update(RefundForm refundForm, TEfunuser user, boolean isSubmit,
			String formType, String formCode) throws Exception {

		if(StringUtils.isBlank(refundForm.getPaymentWay())){
			refundForm.setPaymentWay("");
		}
		if(StringUtils.isBlank(refundForm.getOpsProcess())){
			refundForm.setOpsProcess("");
		}
		if(StringUtils.isBlank(refundForm.getFinProcess())){
			refundForm.setFinProcess("");
		}
		if(StringUtils.isBlank(refundForm.getAcctProcess())){
			refundForm.setAcctProcess("");
		}		
		if(refundForm.getNotifyAgency() == null){
			refundForm.setNotifyAgency(0);
		}
		
		if(refundForm.getNotifyAgency() == null){
			refundForm.setNotifyAgency(0);
		}
		// Flow
		if (isSubmit) { // 送審
			if(StringUtils.isEmpty(refundForm.getSerialNo())) {
				String serialNo = serialNoDao.findSerialNo(formCode);
				refundForm.setSerialNo(serialNo);
			}
			else {  // 退件重送
				flowManager.cancelStampUsed(refundForm.getWorkFlowId());
			}
			// flow
			flowManager.startFlow(formType, formCode, refundForm.getSerialNo(), user,
					refundForm.getWorkFlowId(), refundForm);
		} else { // 草稿
			if(StringUtils.isEmpty(refundForm.getSerialNo())) {
				refundForm.setSerialNo(StringUtils.EMPTY);
			}
			else {  // 退件重送
				flowManager.cancelStampUsed(refundForm.getWorkFlowId());
			}
			
			flowManager.saveDraft(refundForm, user.getId(), formType, formCode, refundForm.getWorkFlowId());
		}		
		
		return refundFormDao.update(refundForm);
	}

	public RefundForm findOneById(Long id) {
		return refundFormDao.findOneById(id);
	}

	@Transactional(readOnly=true)
	public Page<RefundForm> list(RefundFormForm refundFormForm) {
		return refundFormDao.list(refundFormForm);
	}

	public int delete(Long id) {
		return refundFormDao.delete(id);
	}

	@Override
	public RefundForm findOneByFlow(Long workFlowId) {		
		return refundFormDao.findOneByFlow(workFlowId);
	}

	public int update(RefundForm refundForm) {
		return refundFormDao.update(refundForm);
	}
}