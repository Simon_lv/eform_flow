package com.baplay.service.impl;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.constant.Flow;
import com.baplay.constant.Form;
import com.baplay.dao.IFlowRouterDao;
import com.baplay.dao.IFlowSchemaDao;
import com.baplay.dao.IFlowStepDao;
import com.baplay.dao.IFormFlowDao;
import com.baplay.dao.IFormRoleDao;
import com.baplay.dao.IFormTypeDao;
import com.baplay.dao.IStampDao;
import com.baplay.dao.IUserFlowStepDao;
import com.baplay.dao.IWorkFlowDao;
import com.baplay.dao.IWorkFlowDetailDao;
import com.baplay.dto.BaseForm;
import com.baplay.dto.FormFlow;
import com.baplay.dto.Stamp;
import com.baplay.entity.Employee;
import com.baplay.entity.FlowRouter;
import com.baplay.entity.FlowSchema;
import com.baplay.entity.FlowStep;
import com.baplay.entity.FormRole;
import com.baplay.entity.FormType;
import com.baplay.entity.Role;
import com.baplay.entity.TEfunuser;
import com.baplay.entity.UserFlowStep;
import com.baplay.entity.WorkFlow;
import com.baplay.entity.WorkFlowDetail;
import com.baplay.exception.IllegalException;
import com.baplay.exception.ProcessException;
import com.baplay.form.FormMgrForm;
import com.baplay.service.IBudgetFormManager;
import com.baplay.service.IBusinessCardFormManager;
import com.baplay.service.IBusinessTripExpenseFormManager;
import com.baplay.service.IBusinessTripFormManager;
import com.baplay.service.IContractReviewFormManager;
import com.baplay.service.IEmployeeManager;
import com.baplay.service.IFlowManager;
import com.baplay.service.IIssueFormManager;
import com.baplay.service.IPaymentFormManager;
import com.baplay.service.IRefundFormManager;
import com.baplay.util.DateUtil;

@Service
@Transactional
public class FlowManager implements IFlowManager {
	
	protected static final Logger log = LoggerFactory.getLogger(FlowManager.class);
	
	public static final int DRAFT_FLOW_SCHEMA = -1;
	public static final long DRAFT_FLOW_STEP = -1;
	
	@Autowired
	private IWorkFlowDao workFlowDao;
	@Autowired
	private IWorkFlowDetailDao workFlowDetailDao;
	@Autowired
	private IFlowSchemaDao flowSchemaDao;
	@Autowired
	private IFlowStepDao flowStepDao;
	@Autowired
	private IFormFlowDao formFlowDao;
	@Autowired
	private IFormTypeDao formTypeDao;
	@Autowired
	private IEmployeeManager employeeManager;
	@Autowired
	private IFlowRouterDao flowRouterDao;
	@Autowired
	private IStampDao stampDao;
	@Autowired
	private IFormRoleDao formRoleDao;
	@Autowired
	private IUserFlowStepDao userFlowStepDao;
	@Autowired
	private	IRefundFormManager refundFormManager;
	@Autowired
	private	IPaymentFormManager paymentFormManager;
	@Autowired
	private IBusinessCardFormManager businessCardFormManager;
	@Autowired
	private IBusinessTripFormManager businessTripFormManager;
	@Autowired
	private IBusinessTripExpenseFormManager businessTripExpenseFormManager;
	@Autowired
	private IBudgetFormManager budgetFormManager;
	@Autowired
	private IIssueFormManager issueFormManager;
	@Autowired
	private IContractReviewFormManager contractReviewFormManager;	

	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#saveDraft(long, java.lang.String, java.lang.String, long)
	 */
	@Override
	public long saveDraft(BaseForm form, long userId, String formType, String formCode, long workFlowId) throws IllegalException {
		
		if(workFlowId > 0) {  // 編輯	
			// WorkFlow
			WorkFlow entity = workFlowDao.findOneById(workFlowId);
			
			if(entity.getStatus() == Flow.REJECT.getValue()) {  // 退件
				entity.setFlowSchemaId(DRAFT_FLOW_SCHEMA);
				entity.setStatus(Flow.DRAFT.getValue());
				entity.setStepStatus(Flow.DRAFT.getValue());
				entity.setStepOwner(userId);
				
				workFlowDao.update(entity);
			}
			else {
				if(entity.getStatus() != Flow.DRAFT.getValue()) {
					log.warn("illegal status(not draft)..." + workFlowId);
					throw new IllegalException("1001", "案件狀態已被變更");
				}
			}			
						
			// WorkFlowDetail
			saveDraftDetail(workFlowId, userId);	
			
			return workFlowId;
		}
		else {  // 新建
			// WorkFlow
			WorkFlow entity = new WorkFlow();
			
			entity.setFlowSchemaId(DRAFT_FLOW_SCHEMA);
			entity.setStatus(Flow.DRAFT.getValue());
			entity.setCompanyId(form.getCompanyId());
			entity.setDepartmentId(form.getRequestDepartment());
			entity.setCreator(userId);
			entity.setStepStatus(Flow.DRAFT.getValue());
			entity.setStepOwner(userId);
			entity.setFormType(String.format("%s,%s", formType, formCode));
			
			WorkFlow workFlow = workFlowDao.save(entity);
			
			// WorkFlowDetail
			saveDraftDetail(workFlow.getId(), userId);
			
			return workFlow.getId();
		}		
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#deleteDraft(long)
	 */
	public void deleteDraft(long workFlowId) {
		workFlowDao.delete(workFlowId);
		workFlowDetailDao.delete(workFlowId);
	}
	
	/**
	 * 草稿歷程
	 * 
	 * @param workFlowId
	 * @param creator
	 */
	private void saveDraftDetail(long workFlowId, long userId) {
		// WorkFlowDetail
		WorkFlowDetail detail = new WorkFlowDetail();
		detail.setWorkFlowId(workFlowId);
		detail.setStepId(DRAFT_FLOW_STEP);
		detail.setCreator(userId);
		detail.setStatus(Flow.NORMAL_CLOSED.getValue());
		
		workFlowDetailDao.save(detail);
	}
	
	/**
	 * 結束本步驟
	 * 
	 * @param workFlowDetailId
	 */
	private void closeCurrentStep(long workFlowDetailId) {
		workFlowDetailDao.close(workFlowDetailId);
	}
	
	/**
	 * 結束本步驟(自訂)
	 * 
	 * @param workFlowDetailId
	 */
	private void closeCurrentStep(long workFlowId, long stepId, long userId) {
		userFlowStepDao.close(workFlowId, stepId, userId);
	}
	
	/**
	 * 下一步驟
	 * 
	 * @param workFlowId
	 * @param stepId
	 * @param userId
	 */
	private void saveNextFlowStep(long workFlowId, long stepId, int departmentId, long userId, String countersignRole) {
		// WorkFlowDetail
		WorkFlowDetail detail = new WorkFlowDetail();
		detail.setWorkFlowId(workFlowId);
		detail.setStepId(stepId);
		detail.setCreator(userId);
		detail.setStatus(Flow.WAITTING.getValue());
		
		if(departmentId > 0) {
			detail.setDepartmentId(departmentId);
		}
		
		if(StringUtils.isNotEmpty(countersignRole)) {
			detail.setCountersignRole(Integer.parseInt(countersignRole));
		}
		
		workFlowDetailDao.save(detail);
	}
	
	/**
	 * 查找同部門ID
	 * 
	 * @param nextStep
	 * @param employeeId
	 * @return
	 * @throws Exception
	 */
	private int findDepartment(FlowStep nextStep, long employeeId) throws Exception {
		if(nextStep.getSameDepartment() == Flow.SAME_DEPARTMENT.getValue()) {
			Employee employee = employeeManager.findOneById(employeeId);
			
			if(employee == null) {
				throw new Exception("Employee not found");
			}
			
			return employee.getDepartmentId();
		}
		
		return 0;
	}
	
	/**
	 * 流程查找表單
	 * 
	 * @param workFlow
	 * @return
	 * @throws Exception
	 */
	private Object findFormByFlow(WorkFlow workFlow) throws Exception {
		String formType = workFlow.getFormType();
		Object form = null;
		long id = workFlow.getFormId().longValue();
		
		if(StringUtils.startsWith(formType, Form.PAYMENT.getValue())) {  // 請款單
			form = paymentFormManager.findOneById(id);
		}
		else if(StringUtils.startsWith(formType, Form.BUSINESS_CARD.getValue())) {  // 公司卡消費通知單
			form = businessCardFormManager.findOneById(id);
		}
		else if(StringUtils.startsWith(formType, Form.REFUND.getValue())) {  // 退款申請單
			form = refundFormManager.findOneById(id);
		}
		else if(StringUtils.startsWith(formType, Form.BUSINESS_TRIP.getValue())) {  // 出差申請單
			form = businessTripFormManager.findOneById(id);
		}
		else if(StringUtils.startsWith(formType, Form.BUSINESS_TRIP_EXPENSE.getValue())) {  // 出差報支單
			form = businessTripExpenseFormManager.findOneById(id);
		}
		else if(StringUtils.startsWith(formType, Form.BUDGET.getValue())) {  // 部門固定成本運算表
			form = budgetFormManager.findOneById(id);
		}
		else if(StringUtils.startsWith(formType, Form.ISSUE.getValue())) {  // 重大事故通報單
			form = issueFormManager.findOneById(id);
		}
		else if(StringUtils.startsWith(formType, Form.CONTRACT_REVIEW.getValue())) {  // 合約審閱申請單
			form = contractReviewFormManager.findOneById(id);
		}
		else if(StringUtils.startsWith(formType, Form.QUALITY_CONTROL.getValue())) {  // 測試單
			// TODO 暫不支援
		}
		else {
			throw new Exception("Unknown formType " + formType);
		}
		
		return form;
	}
	
	/**
	 * 流程條件
	 * 
	 * @param workFlow
	 * @param flowRouterList
	 * @return
	 * @throws Exception
	 */
	private long routing(WorkFlow workFlow, List<FlowRouter> flowRouterList) throws Exception {
		for(FlowRouter flowRouter : flowRouterList) {
			if(router(workFlow, flowRouter, null)) {
				return flowRouter.getNextStep().longValue();
			}
		}		
		
		return 0;
	}
	
	private long routing(BaseForm form, List<FlowRouter> flowRouterList) throws Exception {
		for(FlowRouter flowRouter : flowRouterList) {
			if(router(null, flowRouter, form)) {
				return flowRouter.getNextStep().longValue();
			}
		}		
		
		return 0;
	}
	
	/**
	 * 流程條件
	 * 
	 * @param workFlow
	 * @param flowRouter
	 * @return
	 * @throws Exception
	 */
	private boolean router(WorkFlow workFlow, FlowRouter flowRouter, BaseForm baseForm) throws Exception {
		Object form = workFlow == null ? baseForm : findFormByFlow(workFlow);
		
		if(form == null) {
			return false;
		}
				
		// rounter map aaa = bbb
		String[] condition = flowRouter.getCondition().split(" ");			
		log.debug("condition: " + StringUtils.join(condition, " "));
		
		if(condition.length != 3) {
			throw new Exception("Unknown condition " + flowRouter.getCondition());
		}
		
		Method getter = form.getClass().getMethod("get" + WordUtils.capitalize(condition[0]));
		Object value = getter.invoke(form);
		String valueLeft = value == null ? StringUtils.EMPTY : value.toString();			
		
		// operator
		String operator = condition[1];
		String valueRight = condition[2];
		
		log.debug(String.format("%s %s %s", valueLeft, operator, valueRight));
		
		if(StringUtils.equals(operator, "=")) {
			boolean match = false;
			
			for(String r : valueRight.split(",")) {
				if(match) {
					break;
				}
				
				log.debug(String.format("check1 [%s]=[%s]", valueLeft, r));
				for(String l : valueLeft.split(",")) {
					if(StringUtils.equals(l, r)) {
						match = true;
						break;
					}
				}				
			}
			log.debug("match " + match);
			return match;
		}
		else if(StringUtils.equals(operator, "!=")) {
			int match = 0;
			
			for(String r : valueRight.split(",")) {
				if(match > 0) {
					break;
				}
				
				log.debug(String.format("check2 [%s]!=[%s]", valueLeft, r));
				for(String l : valueLeft.split(",")) {
					if(StringUtils.equals(l, r)) {
						match++;				
					}
				}
			}
			log.debug("match count " + match);
			return match == 0;
		}
		else if(StringUtils.equals(operator, ">=")) {
			BigDecimal left = new BigDecimal(valueLeft);
			BigDecimal right = new BigDecimal(valueRight);
			log.debug(String.format("check3 [%s]>=[%s] %s", valueLeft, valueRight, left.compareTo(right) >= 0));
			return left.compareTo(right) >= 0;
		}
		else if(StringUtils.equals(operator, "<=")) {
			BigDecimal left = new BigDecimal(valueLeft);
			BigDecimal right = new BigDecimal(valueRight);
			log.debug(String.format("check4 [%s]<=[%s] %s", valueLeft, valueRight, left.compareTo(right) <= 0));
			return left.compareTo(right) <= 0;
		}
		else if(StringUtils.equals(operator, ">")) {
			BigDecimal left = new BigDecimal(valueLeft);
			BigDecimal right = new BigDecimal(valueRight);
			log.debug(String.format("check5 [%s]>[%s] %s", valueLeft, valueRight, left.compareTo(right) > 0));
			return left.compareTo(right) > 0;
		}
		else if(StringUtils.equals(operator, "<")) {
			BigDecimal left = new BigDecimal(valueLeft);
			BigDecimal right = new BigDecimal(valueRight);
			log.debug(String.format("check6 [%s]<[%s] %s", valueLeft, valueRight, left.compareTo(right) < 0));
			return left.compareTo(right) < 0;
		}
		else {
			throw new Exception("Unknown operator " + operator);				
		}
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#lockFlow(long, long, long)
	 */
	@Override
	public boolean lockFlow(long workFlowId, long workFlowDetailId, long ownerId) throws Exception {
		int cnt = workFlowDao.process(workFlowId, ownerId);
		
		if(cnt > 0) {
			workFlowDetailDao.process(workFlowDetailId, ownerId);
		}		
		
		return cnt == 0 ? false : true;
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#lockAbort(long)
	 */
	@Override
	public void lockAbort(long workFlowDetailId) throws Exception {
		// WorkFlowDetail
		WorkFlowDetail workFlowDetail = workFlowDetailDao.findOneById(workFlowDetailId);
		
		workFlowDao.abort(workFlowDetail.getWorkFlowId());
		workFlowDetailDao.abort(workFlowDetailId);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#startFlow(java.lang.String, java.lang.String, long, long)
	 */
	@Override
	public long startFlow(String formType, String formCode, String serialNo, TEfunuser user, long workFlowId, BaseForm form) throws Exception {
		// FlowSchema
		FlowSchema flowSchema = findFlowSchema(form.getCompanyId(), formType, formCode);
		int currentOrder = 1;  // 草稿=步驟一
		FlowStep nextStep = null;
		// 第一步就有條件
		FlowStep flowStep = flowStepDao.findFlowStep(flowSchema.getId(), currentOrder);
		
		// FlowStep
		List<FlowStep> flowStepList = flowStepDao.findFlowAllStep(flowSchema.getId(), currentOrder,
				Flow.VERIFY.getValue(), Flow.PASS.getValue());
		nextStep = flowStepList.get(0);  // 預設下一步驟(無條件或條件未符合)
		
		if(flowStep.getRouterId() != null) {			
			// router
			List<FlowRouter> flowRouter = flowRouterDao.findOneById(flowStep.getRouterId());
			long routingKey = routing(form, flowRouter);
			
			if(routingKey > 0) {
				nextStep = flowStepDao.findOneById(routingKey);				
			}
		}		
				
		int departmentId = findDepartment(nextStep, user.getEmployeeId());
				
		// Draft
		if(workFlowId > 0) {
			// WorkFlow
			workFlowDao.submitDraft(workFlowId, flowSchema.getId(), serialNo);
			
			// WorkFlowDetail
			saveNextFlowStep(workFlowId, nextStep.getId(), departmentId, user.getId(), null);
			return workFlowId;
		}
		else {
			// WorkFlow
			WorkFlow entity = new WorkFlow();
					
			entity.setFlowSchemaId(flowSchema.getId().intValue());
			entity.setStatus(Flow.RUNNING.getValue());
			entity.setCompanyId(form.getCompanyId());
			entity.setDepartmentId(form.getRequestDepartment());
			entity.setCreator(user.getId());
			entity.setSerialNo(serialNo);
			entity.setFormType(String.format("%s,%s", formType, formCode));
			entity.setStepStatus(Flow.WAITTING.getValue());		
			
			WorkFlow workFlow = workFlowDao.save(entity);
			
			// WorkFlowDetail
			saveNextFlowStep(workFlow.getId(), nextStep.getId(), departmentId, user.getId(), null);
			
			return workFlow.getId();
		}		
	}
			
	/**
	 * 依表單代號找對應流程定義檔
	 * 
	 * @param formType
	 * @param formCode
	 * @return
	 * @throws Exception
	 */
	private FlowSchema findFlowSchema(long companyId, String formType, String... formCode) throws Exception {
		String flowSchemaId = "";
		String groupCode = formType.split(",")[0]; 
		String code = "";
		
		if(formCode.length == 0) {
			code = formType.split(",")[1];
		}
		else {
			code = formCode[0];
		}
		
		FormType form = formTypeDao.findOneByCode(code, groupCode);
		
		if(form != null) {
			flowSchemaId = form.getAllFlowSchema();
		}
		
		FlowSchema flowSchema = flowSchemaDao.findOneById(companyId, flowSchemaId);
		
		if(flowSchema == null) {
			throw new Exception ("FlowSchema not found");
		}
		
		return flowSchema;		
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#verifyFlow(long, long)
	 */
	@Override
	public void verifyFlow(long workFlowDetailId, TEfunuser user) throws IllegalException, ProcessException {
		try {			
			// WorkFlowDetail
			WorkFlowDetail workFlowDetail = workFlowDetailDao.findOneById(workFlowDetailId);
			long currentStep = workFlowDetail.getStepId();
			FlowStep flowStep = flowStepDao.findOneById(currentStep);
			// StepOrder(current)
			int curentOrder = flowStepDao.findOneById(currentStep).getStepOrder();
			WorkFlow workFlow = workFlowDao.findOneById(workFlowDetail.getWorkFlowId());
			// Check status
			validate(workFlowDetailId, workFlow);
			// FlowSchema
			FlowSchema flowSchema = findFlowSchema(workFlow.getCompanyId(), workFlow.getFormType());
			boolean isCountersign = flowStep.getIteration() > 1;
			// UserFlowStep
			List<UserFlowStep> userFlowStepList = userFlowStepDao.findByStep(workFlow.getId(), currentStep);
			
			if(userFlowStepList.size() > 0) {  // UserFlow				
				if(userFlowStepList.size() == 1) {  // Last one
					List<FlowStep> flowStepList = flowStepDao.findFlowAllStep(flowSchema.getId(), curentOrder,
							Flow.VERIFY.getValue(), Flow.PASS.getValue());		
					flowStep = flowStepList.get(0);
				}			
				
				// WorkFlow
				workFlowDao.next(workFlow.getId());
				// WorkFlowDetail
				closeCurrentStep(workFlowDetailId);
				
				saveNextFlowStep(workFlow.getId(), flowStep.getId(), 0, user.getId(), null);
				// UserFlowStep
				closeCurrentStep(workFlow.getId(), currentStep, user.getId());
			}
			else if(isCountersign) {  // Countersign
				// WorkFlow
				workFlowDao.next(workFlow.getId());
				String[] countersign = flowStep.getCountersign().split(",");
				// WorkFlowDetail
				closeCurrentStep(workFlowDetailId);
				saveNextFlowStep(workFlow.getId(), flowStep.getId(), 0, user.getId(), countersign[0]);				
			}
			else {
				// FlowStep(after current)
				List<FlowStep> flowStepList = flowStepDao.findFlowAllStep(flowSchema.getId(), curentOrder,
						Flow.VERIFY.getValue(), Flow.PASS.getValue());		
				FlowStep nextStep = flowStepList.get(0);
				
				if(flowStep.getRouterId() != null) {
					// router
					List<FlowRouter> flowRouter = flowRouterDao.findOneById(flowStep.getRouterId());
					long routingKey = routing(workFlow, flowRouter);
					
					if(routingKey > 0) {
						nextStep = flowStepDao.findOneById(routingKey);				
					}			
				}
						
				int departmentId = findDepartment(nextStep, user.getEmployeeId());
				
				// WorkFlow
				workFlowDao.next(workFlow.getId());
				
				// WorkFlowDetail
				closeCurrentStep(workFlowDetailId);
				saveNextFlowStep(workFlow.getId(), nextStep.getId(), departmentId, user.getId(), null);				
			}
		}
		catch(IllegalException e) {
			throw e;
		}
		catch(Exception e) {
			log.error("err:", e);
			throw new ProcessException("9999", e.getLocalizedMessage(), workFlowDetailId);
		}	
	}
		
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#passFlow(long, long)
	 */
	@Override
	public void passFlow(long workFlowDetailId, TEfunuser user) throws IllegalException, ProcessException {
		try {			
			// WorkFlowDetail
			WorkFlowDetail workFlowDetail = workFlowDetailDao.findOneById(workFlowDetailId);
			long currentStep = workFlowDetail.getStepId();
			FlowStep flowStep = flowStepDao.findOneById(currentStep); 
			int curentOrder = flowStep.getStepOrder();
			// WorkFlow
			WorkFlow workFlow = workFlowDao.findOneById(workFlowDetail.getWorkFlowId());
			// Check
			validate(workFlowDetailId, workFlow);
			// FlowSchema
			FlowSchema flowSchema = findFlowSchema(workFlow.getCompanyId(), workFlow.getFormType());	
			// FlowStep(after current)
			List<FlowStep> flowStepList = flowStepDao.findFlowAllStep(flowSchema.getId(), curentOrder,
					Flow.PASS.getValue());
			
			if(flowStepList.isEmpty()) {
				boolean isCountersign = flowStep.getIteration() > 1;
				// countersign
				if(isCountersign) {
					// WorkFlow
					workFlowDao.next(workFlow.getId());
					String[] countersign = flowStep.getCountersign().split(",");
					// WorkFlowDetail
					closeCurrentStep(workFlowDetailId);
					saveNextFlowStep(workFlow.getId(), flowStep.getId(), 0, user.getId(), countersign[0]);					
				}
				else {
					List<FlowRouter> flowRouter = flowRouterDao.findOneById(flowStep.getRouterId());
					long routingKey = routing(workFlow, flowRouter);
					
					if(routingKey > 0) {  // routing(二放)
						FlowStep routing = flowStepDao.findOneById(routingKey);
						int departmentId = findDepartment(routing, user.getEmployeeId());
						closeCurrentStep(workFlowDetailId);
						saveNextFlowStep(workFlow.getId(), routing.getId(), departmentId, user.getId(), null);
					}
					else {
						// close
						closeCurrentStep(workFlowDetailId);	
						workFlowDao.close(workFlow.getId(), user.getId());
					}
				}	
			}
			else {  // 提前結束判斷				
				FlowStep nextStep = flowStepList.get(0);
				int departmentId = findDepartment(nextStep, user.getEmployeeId());				
				// router
				if(flowStep.getRouterId() == null) {
					workFlowDao.next(workFlow.getId());
					closeCurrentStep(workFlowDetailId);	
					saveNextFlowStep(workFlow.getId(), nextStep.getId(), departmentId, user.getId(), null);					
				}
				else {
					closeCurrentStep(workFlowDetailId);
					
					List<FlowRouter> flowRouter = flowRouterDao.findOneById(flowStep.getRouterId());
					long routingKey = routing(workFlow, flowRouter);
					
					if(routingKey > 0) {  // routing
						// router
						FlowStep routing = flowStepDao.findOneById(routingKey);
						
						if(routing.getStepType() == Flow.END.getValue()) {  // 提前結束
							// close
							workFlowDao.close(workFlow.getId(), user.getId());						
						}
						else {
							departmentId = findDepartment(routing, user.getEmployeeId());
							workFlowDao.next(workFlow.getId());
							saveNextFlowStep(workFlow.getId(), routing.getId(), departmentId, user.getId(), null);						
						}
					}
					else {  // close
						workFlowDao.close(workFlow.getId(), user.getId());
					}
				}
			}
		}
		catch(IllegalException e) {
			throw e;
		}
		catch(Exception e) {
			log.error("err:", e);
			throw new ProcessException("9999", e.getLocalizedMessage(), workFlowDetailId);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#countersign(long, com.baplay.entity.TEfunuser)
	 */
	@Override
	public void countersign(long workFlowDetailId, TEfunuser user) throws Exception {
		// WorkFlowDetail
		WorkFlowDetail workFlowDetail = workFlowDetailDao.findOneById(workFlowDetailId);
		long currentStep = workFlowDetail.getStepId();
		FlowStep flowStep = flowStepDao.findOneById(currentStep); 
		int curentOrder = flowStep.getStepOrder();
		int iteration = flowStep.getIteration();
		// WorkFlow
		WorkFlow workFlow = workFlowDao.findOneById(workFlowDetail.getWorkFlowId());
		// role(1,2,3...)
		String[] countersign = flowStep.getCountersign().split(",");		
		int current = Arrays.asList(countersign).indexOf(workFlowDetail.getCountersignRole().toString());
		// 章數集滿可進下一步驟
		String nextRole = current + 1 == countersign.length || current + 1 >= iteration ? null : countersign[current + 1];
		
		if(StringUtils.isEmpty(nextRole)) {  // next step
			if(flowStep.getStepType() == Flow.VERIFY.getValue()) {
				// FlowSchema
				FlowSchema flowSchema = findFlowSchema(workFlow.getCompanyId(), workFlow.getFormType());	
				// FlowStep(after current)
				List<FlowStep> flowStepList = flowStepDao.findFlowAllStep(flowSchema.getId(), curentOrder,
						Flow.VERIFY.getValue(), Flow.PASS.getValue());		
				FlowStep nextStep = flowStepList.get(0);
				
				if(flowStep.getRouterId() != null) {
					// router
					List<FlowRouter> flowRouter = flowRouterDao.findOneById(flowStep.getRouterId());
										
					long routingKey = routing(workFlow, flowRouter);
					
					if(routingKey > 0) {
						nextStep = flowStepDao.findOneById(routingKey);				
					}
				}
						
				int departmentId = findDepartment(nextStep, user.getEmployeeId());
				
				// WorkFlow
				workFlowDao.next(workFlow.getId());
				
				// WorkFlowDetail
				closeCurrentStep(workFlowDetailId);
				saveNextFlowStep(workFlow.getId(), nextStep.getId(), departmentId, user.getId(), null);				
			}
			else if(flowStep.getStepType() == Flow.PASS.getValue()) {
				// close
				closeCurrentStep(workFlowDetailId);	
				workFlowDao.close(workFlow.getId(), user.getId());			
			}
		}
		else {
			// WorkFlow
			workFlowDao.next(workFlow.getId());			
			// WorkFlowDetail
			closeCurrentStep(workFlowDetailId);
			saveNextFlowStep(workFlow.getId(), flowStep.getId(), 0, user.getId(), nextRole);			
		}		
	}
		
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#rejectFlow(long, long, java.lang.String)
	 */
	@Override
	public void rejectFlow(long workFlowDetailId, long userId, String memo) throws IllegalException, Exception {		
		// WorkFlowDetail
		WorkFlowDetail workFlowDetail = workFlowDetailDao.findOneById(workFlowDetailId);
		// WorkFlow
		WorkFlow workFlow = workFlowDao.findOneById(workFlowDetail.getWorkFlowId());
		// Check
		validate(workFlowDetailId, workFlow);
		
		workFlowDao.reject(workFlow.getId());
		
		// WorkFlowDetail
		workFlowDetailDao.reject(workFlowDetailId, userId, memo);	
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#terminateFlow(long, long, java.lang.String)
	 */
	@Override
	public void terminateFlow(long workFlowId, long workFlowDetailId, long userId, String memo) throws Exception {
		// WorkFlow
		WorkFlow workFlow = workFlowDao.findOneById(workFlowId);
		workFlowDao.terminate(workFlowId);
		
		// WorkFlowDetail
		if(workFlow.getStatus() == Flow.REJECT.getValue()) {
			WorkFlowDetail last = workFlowDetailDao.findOneById(workFlowDetailId);
			WorkFlowDetail detail = new WorkFlowDetail();
			detail.setWorkFlowId(last.getWorkFlowId());
			detail.setStepId(last.getStepId());
			detail.setCreator(userId);
			detail.setStatus(Flow.USER_TERMINATE.getValue());
			detail.setOwnerId(userId);
			
			detail.setMemo(memo);
			
			workFlowDetailDao.terminate(detail);
		}
		else {
			workFlowDetailDao.terminate(workFlowDetailId, userId, memo);
		}	
	}	
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#todoList(long, long)
	 */
	@Override
	public List<FormFlow> todoList(Set<Long> companyId, Set<Long> departmentId, long userId, List<Role> roles) {
		return formFlowDao.findRunning(companyId, departmentId, userId, roles);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#todoListType(long, long, java.lang.String)
	 */
	@Override
	public List<FormFlow> todoListType(Set<Long> companyId, Set<Long> departmentId, long userId, List<Role> roles, String formType) {
		return formFlowDao.findRunningByType(companyId ,departmentId, userId, roles, formType);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#flowHistory(long)
	 */
	@Override
	public List<FormFlow> flowHistory(long workFlowId) {
		return formFlowDao.findHistory(workFlowId);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#findFormFlow(long)
	 */
	@Override
	public WorkFlow findFormFlow(long workFlowId) {
		return workFlowDao.findOneById(workFlowId);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#findFormFlowStep(long)
	 */
	@Override
	public WorkFlowDetail findFormFlowStep(long workFlowDetailId) {
		return workFlowDetailDao.findOneById(workFlowDetailId);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#findFomType(java.lang.String)
	 */
	@Override
	public FormType findFomType(String code, String groupCode, Integer... flowSchemaId) {
		return formTypeDao.findOneByCode(code, groupCode, flowSchemaId);
	}
		
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#findAllStamp(long)
	 */
	@Override
	public Map<String, Stamp> findFormStamp(long workFlowId) {
		WorkFlow workFlow = workFlowDao.findOneById(workFlowId);
		Map<String, Stamp> stampMap = new HashMap<String, Stamp>();
		List<Stamp> stampList = stampDao.findSupervisor(workFlowId);
		StringBuilder sb = new StringBuilder();
		// Supervisor
		for(Stamp stamp : stampList) {
			// Reset
			sb.delete(0, sb.length());
			
			String countersignRole = stamp.getCountersignRole();
			int roleId= -1;
			
			if(StringUtils.isEmpty(countersignRole)) {
				FlowStep flowStep = flowStepDao.findOneById(stamp.getStepId().longValue());
				
				if(flowStep != null) {  // 流程已被刪除
					roleId = flowStep.getRoleId();
				}								
			}
			else {
				roleId = Integer.parseInt(countersignRole);				
			}
			
			// Info
			sb.append(stamp.getOwnerName());
			sb.append(stamp.getStatus() == Flow.REJECT.getValue() ? "<span style='color: red'>退件</span>" : "").append("<br/>");
			sb.append(DateUtil.getStringOfDate(stamp.getProcessTime(), DateUtil.defaultDatePatternStr, null));			
			
			FormRole role = formRoleDao.findOneById(roleId);
			stamp.setInfo(sb.toString());
			
			if(role != null){
				if(stampMap.containsKey(role.getName())) {
					// 3rd,4th...
					int count = 2;
					String key = role.getName() + String.valueOf(count);
					while(stampMap.containsKey(key)){
						count++;
						key = role.getName() + String.valueOf(count);
					}
					
					stampMap.put(key, stamp);
				}
				else {
					String roleName = role.getName();
					// 請款單
					if(StringUtils.contains(workFlow.getFormType(), Form.PAYMENT.getValue())) {
						if(StringUtils.equals(roleName, "副總經理")) {
							roleName = "部門主管";
						}
					}
					
					stampMap.put(roleName, stamp);
				}
			}				
		}
		// Creator
		Stamp stamp = stampDao.findCreator(workFlowId);	
		if(stamp == null){
			stamp = new Stamp();
		}
		
		stamp.setInfo(stamp.getOwnerName() + "<br/>" + DateUtil.getStringOfDate(stamp.getProcessTime() , DateUtil.defaultDatePatternStr, null));
		stampMap.put("申請人", stamp);
		
		return stampMap;
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#findFlowStepById(long)
	 */
	@Override
	public FlowStep findFlowStepById(long id) {
		return flowStepDao.findOneById(id);
	}		
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#findLastOneById(long)
	 */
	@Override
	public WorkFlowDetail findLastOneById(long workFlowId) {
		return workFlowDetailDao.findLastOneById(workFlowId);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#cancelStampUsed(long)
	 */
	@Override
	public void cancelStampUsed(long workFlowId) {
		stampDao.cancel(workFlowId);
	}
		
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#addUserFlowStep(com.baplay.entity.UserFlowStep)
	 */
	@Override
	public UserFlowStep addUserFlowStep(UserFlowStep userFlowStep) {
		return userFlowStepDao.add(userFlowStep);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#findUserFlowStep(long, long, long)
	 */
	@Override
	public UserFlowStep findUserFlowStep(long workFlowId, long stepId, long userId) {
		return userFlowStepDao.findOneByStep(workFlowId, stepId, userId);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#findNextOneById(java.lang.Long)
	 */
	@Override
	public FlowStep findNextStepById(Long id) {
		return flowStepDao.findNextOneById(id);
	}
	
	/**
	 * 檢核狀態
	 * 
	 * @param workFlowDetailId
	 * @throws IllegalException
	 */
	private void validate(long workFlowDetailId, WorkFlow workFlow) throws IllegalException {
		if(workFlowDetailDao.illegal(workFlowDetailId)) {
			log.warn("illegal status(more)..." + workFlowDetailId);
			throw new IllegalException("1001", "狀態已被變更");
		}
		
		if(workFlow.getStatus() == Flow.NORMAL_CLOSED.getValue()) {
			log.warn("illegal status(closed)..." + workFlowDetailId);
			throw new IllegalException("1002", "案件已結束");
		}
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#list(com.baplay.form.FormMgrForm)
	 */
	@Override
	public Page<FormFlow> list(FormMgrForm form) {
		return formFlowDao.list(form);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IFlowManager#findAllFormType()
	 */
	@Override
	public List<FormType> findAllFormType() {
		return formTypeDao.listAll();
	}
}