package com.baplay.service.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.baplay.constant.Flow;
import com.baplay.dao.IFlowStepDao;
import com.baplay.dao.IFormTypeDao;
import com.baplay.dao.IWorkFlowDao;
import com.baplay.dao.IWorkFlowDetailDao;
import com.baplay.entity.Employee;
import com.baplay.entity.FlowStep;
import com.baplay.entity.FormType;
import com.baplay.entity.WorkFlow;
import com.baplay.entity.WorkFlowDetail;
import com.baplay.service.IEmployeeManager;
import com.baplay.service.INotifyManager;
import com.baplay.util.MailUtil;

@Service
@Transactional
public class NofityManager implements INotifyManager {

	protected static final Logger log = LoggerFactory.getLogger(FlowManager.class);
	
	@Autowired
	private IFormTypeDao formTypeDao;
	@Autowired
	private IFlowStepDao flowStepDao;
	@Autowired
	private IWorkFlowDao workFlowDao;
	@Autowired
	private IWorkFlowDetailDao workFlowDetailDao;
	@Autowired
	private IEmployeeManager employeeManager;		
	
	/* (non-Javadoc)
	 * @see com.baplay.service.INotifyManager#notify(long)
	 */
	@Override
	public void notify(long workFlowId) throws Exception {
		WorkFlow workFlow = workFlowDao.findOneById(workFlowId);
		WorkFlowDetail workFlowDetail = workFlowDetailDao.findLastOneById(workFlowId);
		
		if(workFlow.getStatus() == Flow.NORMAL_CLOSED.getValue() ||  // 已放行
				workFlow.getStatus() == Flow.REJECT.getValue()) {  // 退件
			// 原經辦
			notifyCreator(workFlow);
		}
		else {
			FlowStep flowStep = flowStepDao.findOneById(workFlowDetail.getStepId());
			
			// 開關
			if(flowStep.getNotifyFlag() == Flow.NOTIFY_NA.getValue()) {
				return;
			}
			
			if(workFlowDetail.getCountersignRole() != null) {  //會簽
				notifyNext(workFlowDetail.getCountersignRole(), workFlow);	
			}
			else { // 審放
				if(workFlowDetail.getDepartmentId() != null) {  // 部門主管限定
					// 步驟發起人
					Set<String> emailList = employeeManager.findEmployeeEmailByRole(flowStep.getRoleId(), workFlowDetail.getDepartmentId());
					notifyNext(emailList, workFlow);
				}
				else {
					// Role of FlowStep					
					notifyNext(flowStep.getRoleId(), workFlow);				
				}
			}
		}				
	}
	
	/**
	 * 取得 WorkFlow.formType 真正的 formType
	 * 
	 * @param flowFormTyp
	 * @return
	 */
	private String getFormTypeForFlow(String flowFormType) {
		String[] type = flowFormType.split(",");
		
		if(type.length == 2) {
			return type[0];
		}
		else {
			// illegal data
			return flowFormType;
		}
	}
	
	/**
	 * 通知下一手
	 * 
	 * @param roleId
	 * @param workFlow
	 * @throws Exception
	 */
	private void notifyNext(int roleId, WorkFlow workFlow) throws Exception {
		String content = getFormContent(workFlow.getFormType(), workFlow.getFormId());
		
		Set<String> emailList = employeeManager.findEmployeeEmailByRole(roleId);		
		notifyByEmail(emailList.toArray(new String[emailList.size()]), content);
	}
	
	/**
	 * @param emailList
	 * @param workFlow
	 * @throws Exception
	 */
	private void notifyNext(Set<String> emailList, WorkFlow workFlow) throws Exception {
		String content = getFormContent(workFlow.getFormType(), workFlow.getFormId());
		
		notifyByEmail(emailList.toArray(new String[emailList.size()]), content);
	}
	
	/**
	 * 通知原始經辦
	 * 
	 * @param workFlow
	 * @throws Exception
	 */
	private void notifyCreator(WorkFlow workFlow) throws Exception {
		String content = getFormContent(workFlow.getFormType(), workFlow.getFormId());
		
		Employee creator = employeeManager.findOneByUID(workFlow.getCreator().intValue());
		
		if(StringUtils.isNotEmpty(creator.getEmail())) {
			notifyByEmail(new String[]{creator.getEmail()}, content);
		}
	}
	
	/**
	 * Email通知
	 * 
	 * @param sendTo
	 * @param mailcontent
	 * @param mailsubject
	 */
	private void notifyByEmail(String[] sendTo, String content) {
		String subject = "ERP代辦事項提醒";
		
		if(sendTo.length == 0 || StringUtils.isEmpty(content)) {
			return;
		}
		
		MailUtil.sendMail(sendTo, content, subject);
	}	
	
	/**
	 * 檢視頁內容
	 * 
	 * @param formType
	 * @param id
	 * @return
	 */
	public String getFormContent(String flowFormType, long id, String... params) {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		String sessionId = request.getRequestedSessionId();
		FormType form = formTypeDao.findOneByCode(StringUtils.EMPTY, getFormTypeForFlow(flowFormType));
		
		if(form == null) {
			return StringUtils.EMPTY;
		}
		
		String action = "print";
		
		if(params.length > 0) {
			action = params[0].toLowerCase();
		}
		
		String viewPage = form.getViewPage();
		String scheme = request.getScheme();             // http
	    String serverName = request.getServerName();     // hostname
	    int serverPort = request.getServerPort();        // port
		// http://local.8888play.com/paymentForm/get?id=1        
	    String dataUrl = new StringBuilder().append(scheme).append("://").append(serverName).append(":").append(serverPort).append(viewPage).append("?id=").append(id).append("&action=").append(action).toString();
	    log.debug("dataUrl=>" + dataUrl);
	    
        HttpClient httpClient = new HttpClient();
        String content = null;
 
        try {            
            httpClient.getParams().setCookiePolicy(CookiePolicy.BROWSER_COMPATIBILITY);
            GetMethod getMethod = new GetMethod(dataUrl);
            getMethod.setRequestHeader("cookie", "JSESSIONID=" + sessionId);
            httpClient.executeMethod(getMethod);
 
            InputStream is = getMethod.getResponseBodyAsStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line = null;
            
            while ((line = reader.readLine()) != null) {
            	sb.append(line + "\n");
            }
            
            is.close();            
            content = sb.toString();
            
            //log.debug(content);
        } 
        catch(Exception e) {
            log.debug("err:", e);
        }   
    		
		return content;
	}
}