package com.baplay.service.impl;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baplay.dao.IFunctionDao;
import com.baplay.entity.Function;
import com.baplay.service.IFunctionManager;
import com.baplay.shiro.UserRealm;

@Service
@Transactional
public class FunctionManager implements IFunctionManager {

	@Autowired
	private IFunctionDao dao;
	@Autowired
	private UserRealm realm;

	@Override
	public void saveFunction(Function fun) {
		dao.save(fun);
	}

	@Override
	public void updateFunction(Function fun) {
		if (fun.getId() != null) {
			dao.update(fun);
			realm.clearCache(SecurityUtils.getSubject().getPrincipals());
		}
	}

	@Override
	public void deleteFunction(Function fun) {
		if (fun.getId() != null) {
			dao.delete(fun);
		}
	}

	@Override
	public List<Function> queryFunctionList(Long parentId) {
		if (parentId == null) {
			parentId = 0L;
		}
		return dao.query(parentId);
	}

	@Override
	public Function queryFunction(Long id) {
		if (id != null) {
			return dao.queryOne(id);
		}
		return null;
	}

}
