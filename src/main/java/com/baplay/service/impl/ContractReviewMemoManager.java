package com.baplay.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baplay.dao.IContractReviewMemoDao;
import com.baplay.entity.Attachment;
import com.baplay.entity.ContractReviewMemo;
import com.baplay.service.IAttachmentManager;
import com.baplay.service.IContractReviewMemoManager;

@Service
@Transactional
public class ContractReviewMemoManager implements IContractReviewMemoManager {

	@Autowired
	private IContractReviewMemoDao contractReviewMemoDao;
	@Autowired
	private IAttachmentManager attachmentManager;
	
	@Override
	public ContractReviewMemo add(ContractReviewMemo contractReviewMemo) {		
		return contractReviewMemoDao.add(contractReviewMemo);
	}

	@Override
	public int deleteAll(Long contractReviewFormId) {		
		return contractReviewMemoDao.deleteAll(contractReviewFormId);
	}

	@Transactional(readOnly=true)
	@Override
	public List<ContractReviewMemo> findContractReviewMemo(Long contractReviewFormId) {		
		return contractReviewMemoDao.findContractReviewMemo(contractReviewFormId);
	}

	@Override
	public void add(long formId, long userId, String formCode, String url[], String memo[]) {
		// attachment
		Attachment am = new Attachment();
		am.setFormId(formId);
		am.setFormType(formCode);
		am.setCreator(userId);
		// memo
		ContractReviewMemo crvm = new ContractReviewMemo();
		crvm.setContractReviewFormId(formId);
		crvm.setCreator(userId);
		
		for(int i = 0; i < url.length; i++) {
			if(StringUtils.isNotBlank(url[i]) && StringUtils.isNotBlank(memo[i])) {
				am.setUrl(url[i]);
				Attachment attachment = attachmentManager.add(am);
				
				crvm.setAttachmentId(attachment.getId());
				crvm.setMemo(memo[i]);
				contractReviewMemoDao.add(crvm);
			}
		}		
	}
}