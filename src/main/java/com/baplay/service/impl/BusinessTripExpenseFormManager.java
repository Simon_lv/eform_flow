package com.baplay.service.impl;

import java.math.BigDecimal;
import java.sql.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IBusinessTripExpenseFormDao;
import com.baplay.dao.IReceiptDao;
import com.baplay.dao.ISerialNoDao;
import com.baplay.dao.IWorkFlowDao;
import com.baplay.entity.BusinessTripExpenseForm;
import com.baplay.entity.Receipt;
import com.baplay.entity.TEfunuser;
import com.baplay.form.BusinessTripExpenseFormForm;
import com.baplay.service.IBusinessTripExpenseFormManager;
import com.baplay.service.IFlowManager;

@Service
@Transactional
public class BusinessTripExpenseFormManager implements IBusinessTripExpenseFormManager {
	
	protected static final Logger log = LoggerFactory.getLogger(BusinessTripExpenseFormManager.class);
	
	@Autowired
	private IBusinessTripExpenseFormDao businessTripExpenseFormDao;
	@Autowired
	private IReceiptDao receiptDao;
	@Autowired
	private IFlowManager flowManager;
	@Autowired
	private ISerialNoDao serialNoDao;
	@Autowired
	private IWorkFlowDao workFlowDao;

	public BusinessTripExpenseForm add(HttpServletRequest request, BusinessTripExpenseForm businessTripExpenseForm , TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception {
		
		// Flow
		long workFlowId = -1;
		
		if(isSubmit) {  // 送審
			String serialNo = serialNoDao.findSerialNo(formCode);
			businessTripExpenseForm.setSerialNo(serialNo);
			
			// flow
			workFlowId = flowManager.startFlow(formType, formCode, serialNo, user, -1, businessTripExpenseForm);			
		}
		else {  // 草稿
			// draft
			businessTripExpenseForm.setSerialNo(StringUtils.EMPTY);
			workFlowId = flowManager.saveDraft(businessTripExpenseForm, user.getId(), formType, formCode, -1);
		}
		
		businessTripExpenseForm.setWorkFlowId(workFlowId);
		BusinessTripExpenseForm bcf = businessTripExpenseFormDao.add(businessTripExpenseForm);
		// Update formId to workFlow
		workFlowDao.update(workFlowId, bcf.getId());
		
		String costDate[] = request.getParameterValues("costDate");
		String place[] = request.getParameterValues("place");
		String number[] = request.getParameterValues("number");		
		String description[] = request.getParameterValues("description");
		String amount[] = request.getParameterValues("amount");
		String currency[] = request.getParameterValues("currency");
		String rate[] = request.getParameterValues("rate");
		String costType[] = request.getParameterValues("costType");		

		Receipt receipt = new Receipt();
		receipt.setFormId(bcf.getId());
		receipt.setFormType(formCode);
		receipt.setCostItem("");

		receiptDao.deleteAll(receipt.getFormId(), receipt.getFormType());
		
		for (int i = 0; i < costDate.length; i++) {
			if(StringUtils.isNotBlank(costDate[i]) && StringUtils.isNotBlank(place[i]) && StringUtils.isNotBlank(number[i])
				&& StringUtils.isNotBlank(amount[i]) && StringUtils.isNotBlank(currency[i]) && StringUtils.isNotBlank(rate[i])) {
				receipt.setCostDate(Date.valueOf(costDate[i]));
				receipt.setPlace(place[i]);
				receipt.setNumber(number[i]);
				receipt.setDescription(description[i]);
				receipt.setAmount(new BigDecimal(amount[i]));
				receipt.setCurrency(currency[i]);
				receipt.setRate(Double.parseDouble(rate[i]));
				receipt.setCostType(Integer.parseInt(costType[i]));
				receiptDao.add(receipt);
			}			
		}

		return bcf;
	}

	public int update(HttpServletRequest request, BusinessTripExpenseForm businessTripExpenseForm , TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception {
		
		// Flow
		if(isSubmit) {  // 送審
			if(StringUtils.isEmpty(businessTripExpenseForm.getSerialNo())) {
				String serialNo = serialNoDao.findSerialNo(formCode);
				businessTripExpenseForm.setSerialNo(serialNo);
			}
			else {  // 退件重送
				flowManager.cancelStampUsed(businessTripExpenseForm.getWorkFlowId());
			}
			// flow
			flowManager.startFlow(formType, formCode, businessTripExpenseForm.getSerialNo(), user, businessTripExpenseForm.getWorkFlowId(), businessTripExpenseForm);
		}
		else {  // 草稿
			if(StringUtils.isEmpty(businessTripExpenseForm.getSerialNo())) {
				businessTripExpenseForm.setSerialNo(StringUtils.EMPTY);
			}
			else {  // 退件重送
				flowManager.cancelStampUsed(businessTripExpenseForm.getWorkFlowId());
			}
			
			flowManager.saveDraft(businessTripExpenseForm, user.getId(), formType, formCode, businessTripExpenseForm.getWorkFlowId());
		}		
		
		String costDate[] = request.getParameterValues("costDate");
		String place[] = request.getParameterValues("place");
		String number[] = request.getParameterValues("number");		
		String description[] = request.getParameterValues("description");
		String amount[] = request.getParameterValues("amount");
		String currency[] = request.getParameterValues("currency");
		String rate[] = request.getParameterValues("rate");
		String costType[] = request.getParameterValues("costType");

		Receipt receipt = new Receipt();
		receipt.setFormId(businessTripExpenseForm.getId());
		receipt.setFormType(formCode);
		receipt.setCostItem("");

		receiptDao.deleteAll(receipt.getFormId(), receipt.getFormType());
		
		for (int i = 0; i < costDate.length; i++) {
			if(StringUtils.isNotBlank(costDate[i]) && StringUtils.isNotBlank(place[i]) && StringUtils.isNotBlank(number[i])
					&& StringUtils.isNotBlank(amount[i]) && StringUtils.isNotBlank(currency[i]) && StringUtils.isNotBlank(rate[i])) {
				receipt.setCostDate(Date.valueOf(costDate[i]));
				receipt.setPlace(place[i]);
				receipt.setNumber(number[i]);
				receipt.setDescription(description[i]);
				receipt.setAmount(new BigDecimal(amount[i]));
				receipt.setCurrency(currency[i]);
				receipt.setRate(Double.parseDouble(rate[i]));
				receipt.setCostType(Integer.parseInt(costType[i]));
				receiptDao.add(receipt);
			}
		}

		return businessTripExpenseFormDao.update(businessTripExpenseForm);
	}

	public BusinessTripExpenseForm findOneById(Long id) {
		return businessTripExpenseFormDao.findOneById(id);
	}

	@Transactional(readOnly=true)
	public Page<BusinessTripExpenseForm> list(BusinessTripExpenseFormForm businessTripExpenseFormForm) {
		return businessTripExpenseFormDao.list(businessTripExpenseFormForm);
	}

	public int delete(Long id) {
		return businessTripExpenseFormDao.delete(id);
	}

	@Override
	public BusinessTripExpenseForm findOneByFlow(Long workFlowId) {
		return businessTripExpenseFormDao.findOneByFlow(workFlowId);
	}

	@Override
	public int updateTotal(Long id, Long editId, int costType, String formType) {
		receiptDao.delete(editId);
		return businessTripExpenseFormDao.updateTotal(id, editId, costType, formType);
	}
	
	@Override
	public void remark(Long workFlowId, String remark) {
		businessTripExpenseFormDao.remark(workFlowId, remark);
	}
}