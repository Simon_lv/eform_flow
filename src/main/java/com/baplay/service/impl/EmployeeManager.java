package com.baplay.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IEmployeeDao;
import com.baplay.dao.IFormUserDao;
import com.baplay.dao.IUserEmployeeDao;
import com.baplay.dao.mapper.FormUser;
import com.baplay.entity.Employee;
import com.baplay.form.EmployeeForm;
import com.baplay.service.IEmployeeManager;

@Service
@Transactional
public class EmployeeManager implements IEmployeeManager {
	@Autowired
	private IEmployeeDao employeeDao;	
	@Autowired
	private IFormUserDao formUserDao;
	@Autowired
	private	IUserEmployeeDao userEmployeeDao;
	
	@Override
	public Employee addEmployee(Employee employee) {				
		Employee entity = employeeDao.add(employee);				
		return entity;
	}

	@Override
	public int updateEmployee(Employee employee) {		
		return employeeDao.update(employee);
	}

	@Override
	public Employee findOneById(Long id) {
		return employeeDao.findOneById(id);
	}

	@Override
	public Page<Employee> list(EmployeeForm employeeForm) {
		return employeeDao.list(employeeForm);
	}

	@Override
	public int delete(Long id, Long modifier) {
		return employeeDao.delete(id, modifier);
	}
	
	@Override
	public int delete(Long userId, String employeeNo, Long modifier) {
		userEmployeeDao.deleteByUID(userId);
		return employeeDao.delete(employeeNo, modifier);
	}

	@Override
	public List<Employee> findAllEmployee() {
		return employeeDao.findAllEmployee();
	}
	
	@Override
	public List<Employee> findAllEmployee(int companyId) {
		return employeeDao.findAllEmployee(companyId);
	}
	
	@Override
	public List<Employee> findAllEmployee(int companyId, int departmentId) {
		return employeeDao.findAllEmployee(companyId, departmentId);
	}
	
	@Override
	public List<Employee> findAllEmployee2() {
		return employeeDao.findAllEmployee2();
	}

	@Override
	public boolean checkEmpNoExist(Long compnayId, Long id, String employeeNo) {
		return employeeDao.checkEmpNoExist(compnayId, id, employeeNo);
	}

	@Override
	public Employee findOneByName(String name) {
		return employeeDao.findOneByName(name);
	}
	
	@Override
	public Set<String> findEmployeeEmailByRole(int roleId, int...extraParam) {
		Set<String> emailList = new HashSet<String>();
		List<Employee> employeeList = employeeDao.findEmployeeByRole(roleId, extraParam);
		
		for(Employee employee : employeeList) {
			if(StringUtils.isNotEmpty(employee.getEmail())) {
				emailList.add(employee.getEmail());
			}
		}
		
		return emailList;
	}
	
	@Override
	public Employee findOneByUID(int uid) {
		return employeeDao.findOneByUID(uid);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IEmployeeManager#findIssueRepairUser(int[])
	 */
	@Override
	public List<FormUser> findIssueRepairUser(Integer... roleId) {
		return formUserDao.findIssueRepairUser(roleId);
	}
	
	@Override
	public List<Employee> findSupervisor(Long departmentId) {
		return employeeDao.findSupervisor(departmentId);
	}
	
	@Override
	public List<Long> findUserManageDept(Long uid) {
		return employeeDao.findUserManageDept(uid);
	}
	
	@Override
	public Employee findOneByEmployeeNo(String employeeNo) {
		return employeeDao.findOneByEmployeeNo(employeeNo);
	}
	
	@Override
	public Employee findOneByEmployeeNo(Long companyId, String employeeNo) {
		return employeeDao.findOneByEmployeeNo(companyId, employeeNo);
	}
}