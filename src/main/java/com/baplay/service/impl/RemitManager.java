package com.baplay.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baplay.constant.Form;
import com.baplay.dao.IBusinessTripExpenseFormDao;
import com.baplay.dao.IPaymentFormDao;
import com.baplay.dao.IRefundFormDao;
import com.baplay.service.IRemitManager;

@Service
@Transactional
public class RemitManager implements IRemitManager {
	
	@Autowired
	private IPaymentFormDao paymentFormDao;
	
	@Autowired
	private IBusinessTripExpenseFormDao businessTripExpenseFormDao;
	
	@Autowired
	private IRefundFormDao refundFormDao;

	@Override
	public int confirmRemitDate(String formType, Long id) {
		int cnt = 0;
		
		if(Form.PAYMENT.getValue().equalsIgnoreCase(formType)) {
			cnt = paymentFormDao.confirmRemitDate(id);
		}
		
		if(Form.BUSINESS_TRIP_EXPENSE.getValue().equalsIgnoreCase(formType)) {
			cnt = businessTripExpenseFormDao.confirmRemitDate(id);
		}
		
		if(Form.REFUND.getValue().equalsIgnoreCase(formType)) {
			cnt = refundFormDao.confirmRemitDate(id);
		}
		
		return cnt;
	}

	@Override
	public int updateRemitDate(String formType, Long id, Date remitDate) {
		int cnt = 0;
		
		if(Form.PAYMENT.getValue().equalsIgnoreCase(formType)) {
			cnt = paymentFormDao.updateRemitDate(id, remitDate);
		}
		
		if(Form.BUSINESS_TRIP_EXPENSE.getValue().equalsIgnoreCase(formType)) {
			cnt = businessTripExpenseFormDao.updateRemitDate(id, remitDate);
		}
		
		if(Form.REFUND.getValue().equalsIgnoreCase(formType)) {
			cnt = refundFormDao.updateRemitDate(id, remitDate);
		}
		
		return cnt;
	}
}