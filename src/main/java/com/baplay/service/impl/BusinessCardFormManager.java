package com.baplay.service.impl;

import java.math.BigDecimal;
import java.sql.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IBusinessCardFormDao;
import com.baplay.dao.IReceiptDao;
import com.baplay.dao.ISerialNoDao;
import com.baplay.dao.IWorkFlowDao;
import com.baplay.entity.BusinessCardForm;
import com.baplay.entity.Receipt;
import com.baplay.entity.TEfunuser;
import com.baplay.form.BusinessCardFormForm;
import com.baplay.service.IBusinessCardFormManager;
import com.baplay.service.IFlowManager;

@Service
@Transactional
public class BusinessCardFormManager implements IBusinessCardFormManager {
	
	protected static final Logger log = LoggerFactory.getLogger(BusinessCardFormManager.class);
	
	@Autowired
	private IBusinessCardFormDao businessCardFormDao;	
	@Autowired
	private ISerialNoDao serialNoDao;
	@Autowired
	private IFlowManager flowManager;
	@Autowired
	private IWorkFlowDao workFlowDao;
	@Autowired
	private IReceiptDao receiptDao;

	public BusinessCardForm add(HttpServletRequest request, BusinessCardForm businessCardForm, TEfunuser user,
			boolean isSubmit, String formType, String formCode) throws Exception {

		// Flow
		long workFlowId = -1;

		if (isSubmit) { // 送審
			String serialNo = serialNoDao.findSerialNo(formCode);
			businessCardForm.setSerialNo(serialNo);

			// flow
			workFlowId = flowManager.startFlow(formType, formCode, serialNo, user, -1, businessCardForm);
			businessCardForm.setWorkFlowId(workFlowId);
		} else { // 草稿
			// draft
			businessCardForm.setSerialNo(StringUtils.EMPTY);
			workFlowId = flowManager.saveDraft(businessCardForm, user.getId(), formType, formCode, -1);
		}

		String costDate[] = request.getParameterValues("costDate");
		String costItem[] = request.getParameterValues("costItem");
		String description[] = request.getParameterValues("description");
		String amount[] = request.getParameterValues("amount");		

		businessCardForm.setWorkFlowId(workFlowId);		
		BusinessCardForm bcf = businessCardFormDao.add(businessCardForm);
		
		// Update formId to workFlow
		workFlowDao.update(workFlowId, bcf.getId());		
		
		Receipt receipt = new Receipt();
		receipt.setFormId(bcf.getId());
		receipt.setFormType(formCode);
		receipt.setCurrency("TWD");
		receipt.setRate(1d);
		receipt.setCostType(1);

		receiptDao.deleteAll(receipt.getFormId(), receipt.getFormType());
		
		for (int i = 0; i < costDate.length; i++) {
			receipt.setCostDate(Date.valueOf(costDate[i]));
			receipt.setCostItem(costItem[i]);
			receipt.setDescription(description[i]);
			receipt.setAmount(new BigDecimal(amount[i]));
			receiptDao.add(receipt);
		}

		return bcf;
	}

	public int update(HttpServletRequest request, BusinessCardForm businessCardForm, TEfunuser user, boolean isSubmit,
			String formType, String formCode) throws Exception {
		// Flow
		if (isSubmit) { // 送審
			if(StringUtils.isEmpty(businessCardForm.getSerialNo())) {
				String serialNo = serialNoDao.findSerialNo(formCode);
				businessCardForm.setSerialNo(serialNo);
			}
			else {  // 退件重送
				flowManager.cancelStampUsed(businessCardForm.getWorkFlowId());
			}
			// flow
			flowManager.startFlow(formType, formCode, businessCardForm.getSerialNo(), user,
					businessCardForm.getWorkFlowId(), businessCardForm);
		} else { // 草稿
			if(StringUtils.isEmpty(businessCardForm.getSerialNo())) {
				businessCardForm.setSerialNo(StringUtils.EMPTY);
			}
			else {  // 退件重送
				flowManager.cancelStampUsed(businessCardForm.getWorkFlowId());
			}
			
			flowManager.saveDraft(businessCardForm, user.getId(), formType, formCode, businessCardForm.getWorkFlowId());
		}

		String costDate[] = request.getParameterValues("costDate");
		String costItem[] = request.getParameterValues("costItem");
		String description[] = request.getParameterValues("description");
		String amount[] = request.getParameterValues("amount");

		Receipt receipt = new Receipt();
		receipt.setFormId(businessCardForm.getId());
		receipt.setFormType(formCode);
		receipt.setCurrency("TWD");
		receipt.setRate(1d);
		receipt.setCostType(1);		

		receiptDao.deleteAll(receipt.getFormId(), receipt.getFormType());
		
		for (int i = 0; i < costDate.length; i++) {
			receipt.setCostDate(Date.valueOf(costDate[i]));
			receipt.setCostItem(costItem[i]);
			receipt.setDescription(description[i]);
			receipt.setAmount(new BigDecimal(amount[i]));
			receiptDao.add(receipt);
		}

		return businessCardFormDao.update(businessCardForm);
	}

	public BusinessCardForm findOneById(Long id) {
		return businessCardFormDao.findOneById(id);
	}

	@Transactional(readOnly=true)
	public Page<BusinessCardForm> list(BusinessCardFormForm businessCardFormForm) {
		return businessCardFormDao.list(businessCardFormForm);
	}

	public int delete(Long id) {
		return businessCardFormDao.delete(id);
	}

	@Override
	public BusinessCardForm findOneByFlow(Long workFlowId) {
		return businessCardFormDao.findOneByFlow(workFlowId);
	}

	@Override
	public int updateTotal(Long id, Long editId, String formType) {
		receiptDao.delete(editId);
		return businessCardFormDao.updateTotal(id, editId, formType);
	}
	
}