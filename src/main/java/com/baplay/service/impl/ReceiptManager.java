package com.baplay.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IReceiptDao;
import com.baplay.entity.Receipt;
import com.baplay.form.ReceiptForm;
import com.baplay.service.IReceiptManager;

@Service
@Transactional
public class ReceiptManager implements IReceiptManager {
	@Autowired
	private IReceiptDao receiptDao;
	
	public Receipt add(Receipt receipt) {		
		return receiptDao.add(receipt);
	}
	
	public int update(Receipt receipt) {
		return receiptDao.update(receipt);
	}
	
	public Receipt findOneById(Long id) {
		return receiptDao.findOneById(id);
	}
	
	public Page<Receipt> list(ReceiptForm receiptForm) {
		return receiptDao.list(receiptForm);
	}	

	@Override
	public List<Receipt> findAllByForm(Long id, String formType, Integer costType, String... params) {
		List<Receipt> list = receiptDao.findAllByForm(id, formType, costType);
		boolean pdf = false;
		
		if(params.length > 0) {
			pdf = StringUtils.equalsIgnoreCase("PDF", params[0]);
		}
		
		// PDF 單據名稱字串換行
		if(pdf) {
			for(Receipt r : list) {
				if(r.getDescription() == null || r.getDescription().length() <= 15) {
					// Do Nothing
				}
				else {
					StringBuilder sb = new StringBuilder();
					
					for(int i = 0; i < r.getDescription().length(); i++) {
						if(i > 0 && i % 15 == 0) {
							sb.append("<br>");
						}
						
						sb.append(r.getDescription().substring(i, i + 1));
					}
					
					r.setDescription(sb.toString());
				}
			}
		}
		
		return list;
	}	
}