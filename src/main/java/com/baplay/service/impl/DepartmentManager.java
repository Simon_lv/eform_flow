package com.baplay.service.impl;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IDepartmentDao;
import com.baplay.dao.mapper.DepartmentTop;
import com.baplay.entity.Department;
import com.baplay.form.DepartmentForm;
import com.baplay.service.IDepartmentManager;

@Service
@Transactional
public class DepartmentManager implements IDepartmentManager {
	@Autowired
	private IDepartmentDao departmentDao;
	
	@Override
	public Department add(Department department) {
		return departmentDao.add(department);
	}

	@Override
	public int update(Department department) {
		return departmentDao.update(department);
	}

	@Override
	public Department findOneById(Long id) {
		return departmentDao.findOneById(id);
	}

	@Override
	public Page<Department> list(DepartmentForm departmentForm) {
		return departmentDao.list(departmentForm);
	}

	@Override
	public int delete(Long id, String modifier) {
		return departmentDao.delete(id, modifier);
	}

	@Override
	public int countLiveChild(Long id) {
		return departmentDao.countLiveChild(id);
	}

	@Override
	public List<Department> findAllByLevel(int level) {
		return departmentDao.findAllByLevel(level);
	}

	@Override
	public boolean checkDeptNoExist(Long companyId, Long id, String deptNo) {
		return departmentDao.checkDeptNoExist(companyId, id, deptNo);
	}
	
	@Override
	public List<Department> findAllDepartment() {
		return departmentDao.findAllDepartment();
	}
	
	@Override
	public List<Department> findAllDepartment(Long companyId) {
		return departmentDao.findAllDepartment(companyId);
	}

	public Map<String, DepartmentTop> findAllDepartmentTop() {
		List<DepartmentTop> deptTopList = departmentDao.findAllDepartmentTop();
		Map<String, DepartmentTop> map = new LinkedHashMap<String, DepartmentTop>();
		
		for(DepartmentTop deptTop : deptTopList) {
			map.put(deptTop.getDeptNo(), deptTop);
		}
		
		return map;
	}
	
	public List<Map<String, Object>> deptList(long companyId) {
		return departmentDao.deptList(companyId);
	}

	@Override
	public Department findOneByUserId(Long id) {
		return departmentDao.findOneByUserId(id);
	}
	
	@Override
	public Department findOneByFullName(String fullName, long companyId) {
		return departmentDao.findOneByFullName(fullName, companyId);
	}
	
	@Override
	public Department findOneByDeptNo(String deptNo, long companyId) {
		return departmentDao.findOneByDeptNo(deptNo, companyId);
	}
	
	@Override
	public List<Department> findDepartmentCN() {
		return departmentDao.findDepartmentCN();
	}
}