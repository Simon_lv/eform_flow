package com.baplay.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IFlowSchemaDao;
import com.baplay.dao.IFormTypeDao;
import com.baplay.dao.IQcFormSchemaDao;
import com.baplay.entity.FlowSchema;
import com.baplay.entity.FormType;
import com.baplay.entity.QcFormSchema;
import com.baplay.form.QcFormMgrForm;
import com.baplay.service.IQcFormSchemaManager;
import com.baplay.util.StringUtil;

@Service
@Transactional
public class QcFormSchemaManager implements IQcFormSchemaManager {

	@Autowired
	private IQcFormSchemaDao qcFormSchemaDao;
	@Autowired
	private IFormTypeDao formTypeDao;
	@Autowired
	private IFlowSchemaDao flowSchemaDao;
	
	@Override
	public List<QcFormSchema> findAllFormSchema() {		
		return qcFormSchemaDao.findAllFormSchema();
	}

	@Override
	public QcFormSchema findOneById(Long id) {
		return qcFormSchemaDao.findOneById(id);
	}

	@Override
	public Page<QcFormSchema> list(QcFormMgrForm qcFormMgrForm) {
		return qcFormSchemaDao.list(qcFormMgrForm);
	}

	@Override
	public QcFormSchema add(QcFormSchema qcFormSchema) {
		FormType formType = new FormType();
		FormType qc = formTypeDao.findOneByCode("", "QC");
		String flowName = String.format("檢核表(%s)", StringUtil.getChineseNumber(qcFormSchema.getFlowType()));
		FlowSchema schema = flowSchemaDao.findOneByName(flowName);
		
		formType.setName(qcFormSchema.getFormName());
		formType.setCode(qcFormSchema.getFormCode());
		formType.setFlowSchema(schema.getId());
		formType.setGroupName(qc.getGroupName());
		formType.setGroupCode(qc.getGroupCode());
		formType.setInitPage(qc.getInitPage());
		formType.setProcessPage(qc.getProcessPage());
		formType.setViewPage(qc.getViewPage());		
		
		formTypeDao.add(formType);
		return qcFormSchemaDao.add(qcFormSchema);
	}

	@Override
	public int update(QcFormSchema qcFormSchema) {
		QcFormSchema oldSchema = qcFormSchemaDao.findOneById(qcFormSchema.getId());
		formTypeDao.update("QC", qcFormSchema.getFormName(), oldSchema.getFormCode(), qcFormSchema.getFormCode());
		return qcFormSchemaDao.update(qcFormSchema);
	}		
}