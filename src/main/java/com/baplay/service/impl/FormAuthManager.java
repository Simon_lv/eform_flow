package com.baplay.service.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baplay.dao.IFormAuthDao;
import com.baplay.service.IFormAuthManager;

@Service
@Transactional
public class FormAuthManager implements IFormAuthManager {
	
	@Autowired
	private IFormAuthDao formAuthDao;

	/* (non-Javadoc)
	 * @see com.baplay.service.IFormAuthManager#formAuth(java.lang.String, java.util.Set)
	 */
	@Override
	public boolean formAuth(String formType, Set<String> userFormRole) {
		return formAuthDao.formAuth(formType, userFormRole);
	}
}