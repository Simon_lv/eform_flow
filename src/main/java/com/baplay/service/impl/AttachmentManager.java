package com.baplay.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baplay.dao.IAttachmentDao;
import com.baplay.entity.Attachment;
import com.baplay.service.IAttachmentManager;

@Service
@Transactional
public class AttachmentManager implements IAttachmentManager {

	@Autowired
	private IAttachmentDao attachmentDao;
	
	@Override
	public Attachment add(Attachment attachment) {
		return attachmentDao.add(attachment);
	}

	@Override
	public int deleteAll(Long contractReviewFormid, String formType) {		
		return attachmentDao.deleteAll(contractReviewFormid, formType);
	}

	@Override
	public int delete(Long id) {
		return attachmentDao.delete(id);
	}	

	@Override
	public List<Attachment> findFormAttachment(Long formid, String formType) {
		return attachmentDao.findFormAttachment(formid, formType);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IAttachmentManager#add(java.lang.String[], java.lang.String[], java.lang.Long, java.lang.String, java.lang.Long)
	 */
	@Override
	public void add(String[] url, String[] desc, Long formId, String formType, Long uid) {
		if(url == null) {
			return;
		}
		
		for(int i = 0; i < url.length; i++) {			
			if(StringUtils.isEmpty(url[i])) {
				continue;
			}
			
			Attachment am = new Attachment();
			am.setFormId(formId);
			am.setFormType(formType);
			am.setCreator(uid);
			am.setUrl(url[i]);
			am.setDescription(StringUtils.isBlank(desc[i]) ? String.format("附件%s", i + 1) : desc[i]);
			
			attachmentDao.add(am);
		}
	}
}