package com.baplay.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baplay.dao.IQcFormTitleFieldDao;
import com.baplay.entity.QcFormTitleField;
import com.baplay.service.IQcFormTitleFieldManager;

@Service
@Transactional
public class QcFormTitleFieldManager implements IQcFormTitleFieldManager {

	@Autowired
	private IQcFormTitleFieldDao qcFormTitleFieldDao;
	
	@Override
	public List<QcFormTitleField> findAll(Long id, Long... formId) {		
		return qcFormTitleFieldDao.findAll(id, formId);
	}

	@Override
	public QcFormTitleField add(HttpServletRequest request, QcFormTitleField qcFormTitleField) {
		String id[] = request.getParameterValues("id");
		String fieldLabel[] = request.getParameterValues("fieldLabel");
		String fieldOrder[] = request.getParameterValues("fieldOrder");
		
		for(int i=0;i<fieldLabel.length;i++){
			if(StringUtils.isNotBlank(id[i])){
				qcFormTitleField.setId(Long.parseLong(id[i]));
			}else{
				qcFormTitleField.setId(null);
			}
			if(StringUtils.isNotBlank(fieldOrder[i])){
				qcFormTitleField.setFieldOrder(Integer.parseInt(fieldOrder[i]));
			}else{
				qcFormTitleField.setFieldOrder(null);
			}			
			qcFormTitleField.setFieldLabel(fieldLabel[i]);			
			
			if(qcFormTitleField.getId() == null){
				qcFormTitleField = qcFormTitleFieldDao.add(qcFormTitleField);
			}else{
				qcFormTitleFieldDao.update(qcFormTitleField);
			}
		}
		
		return qcFormTitleField;
	}

	@Override
	public int delete(Long id) {
		return qcFormTitleFieldDao.delete(id);
	}	

}
