package com.baplay.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baplay.dao.IBudgetFormDao;
import com.baplay.dao.IBudgetFormDetailDao;
import com.baplay.entity.BudgetFormDetail;
import com.baplay.service.IBudgetFormDetailManager;

@Service
@Transactional
public class BudgetFormDetailManager implements IBudgetFormDetailManager {

	@Autowired
	private IBudgetFormDetailDao budgetFormDetailDao;
	@Autowired
	private IBudgetFormDao budgetFormDao;
	
	@Override
	public List<BudgetFormDetail> findFormDetail(Long budgetFormId, int additional) {		
		return budgetFormDetailDao.findFormDetail(budgetFormId, additional);
	}

	@Override
	public int delete(Long id, Long editId) {
		budgetFormDetailDao.delete(editId);
		return budgetFormDao.updateTotal(id, editId);
	}

}