package com.baplay.service.impl;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IContractReviewFormDao;
import com.baplay.dao.ISerialNoDao;
import com.baplay.dao.IWorkFlowDao;
import com.baplay.entity.Attachment;
import com.baplay.entity.ContractReviewForm;
import com.baplay.entity.ContractReviewMemo;
import com.baplay.entity.TEfunuser;
import com.baplay.form.ContractReviewFormForm;
import com.baplay.service.IAttachmentManager;
import com.baplay.service.IContractReviewFormManager;
import com.baplay.service.IContractReviewMemoManager;
import com.baplay.service.IFlowManager;

@Service
@Transactional
public class ContractReviewFormManager implements IContractReviewFormManager {

	protected static final Logger log = LoggerFactory.getLogger(ContractReviewFormManager.class);

	@Autowired
	private IContractReviewFormDao contractReviewFormDao;
	@Autowired
	private ISerialNoDao serialNoDao;
	@Autowired
	private IFlowManager flowManager;
	@Autowired
	private IWorkFlowDao workFlowDao;
	@Autowired
	private IAttachmentManager attachmentManager;
	@Autowired
	private IContractReviewMemoManager contractReviewMemoManager;

	public ContractReviewForm add(ContractReviewForm contractReviewForm, String url[], String[] desc, String memo[],
			TEfunuser user, boolean isSubmit,
			String formType, String formCode) throws Exception {
		
		contractReviewForm.setOrganizer(user.getEmployeeId());
		// Flow
		long workFlowId = -1;

		if (isSubmit) { // 送審
			String serialNo = serialNoDao.findSerialNo(formCode);
			contractReviewForm.setSerialNo(serialNo);

			// flow
			workFlowId = flowManager.startFlow(formType, formCode, serialNo, user, -1, contractReviewForm);
		} else { // 草稿
			// draft
			contractReviewForm.setSerialNo(StringUtils.EMPTY);
			workFlowId = flowManager.saveDraft(contractReviewForm, user.getId(), formType, formCode, -1);
		}
		contractReviewForm.setWorkFlowId(workFlowId);
		
		ContractReviewForm crf = contractReviewFormDao.add(contractReviewForm);
		// Update formId to workFlow
		workFlowDao.update(workFlowId, crf.getId());		

		Attachment am = new Attachment();
		am.setFormId(crf.getId());
		am.setFormType(formCode);
		am.setCreator(user.getId());
		
		ContractReviewMemo crvm = new ContractReviewMemo();
		crvm.setContractReviewFormId(crf.getId());
		crvm.setCreator(user.getId());
				
		for(int i=0;i<url.length;i++){
			if(StringUtils.isNotBlank(url[i]) && StringUtils.isNotBlank(memo[i])){
				am.setUrl(url[i]);
				am.setDescription(desc[i]);
				Attachment attachment = attachmentManager.add(am);
				
				crvm.setAttachmentId(attachment.getId());
				crvm.setMemo(memo[i]);
				contractReviewMemoManager.add(crvm);
			}
		}
		
		return crf;
	}

	public int update(ContractReviewForm contractReviewForm, String url[], String[] desc, String memo[], TEfunuser user, boolean isSubmit,
			String formType, String formCode) throws Exception {
		// Flow
		if (isSubmit) { // 送審
			if(StringUtils.isEmpty(contractReviewForm.getSerialNo())) {
				String serialNo = serialNoDao.findSerialNo(formCode);
				contractReviewForm.setSerialNo(serialNo);
			}
			else {  // 退件重送
				flowManager.cancelStampUsed(contractReviewForm.getWorkFlowId());
			}
			// flow
			flowManager.startFlow(formType, formCode, contractReviewForm.getSerialNo(), user, contractReviewForm.getWorkFlowId(), contractReviewForm);
		} else { // 草稿
			if(StringUtils.isEmpty(contractReviewForm.getSerialNo())) {
				contractReviewForm.setSerialNo(StringUtils.EMPTY);
			}
			else {  // 退件重送
				flowManager.cancelStampUsed(contractReviewForm.getWorkFlowId());
			}
			
			flowManager.saveDraft(contractReviewForm, user.getId(), formType, formCode, contractReviewForm.getWorkFlowId());
		}
		
		Attachment am = new Attachment();
		am.setFormId(contractReviewForm.getId());
		am.setFormType(formCode);
		am.setCreator(user.getId());
		
		ContractReviewMemo crvm = new ContractReviewMemo();
		crvm.setContractReviewFormId(contractReviewForm.getId());
		crvm.setCreator(user.getId());
		
		attachmentManager.deleteAll(am.getFormId(), am.getFormType());
		contractReviewMemoManager.deleteAll(crvm.getContractReviewFormId());
				
		for(int i=0;i<url.length;i++){
			if(StringUtils.isNotBlank(url[i]) && StringUtils.isNotBlank(memo[i])){
				am.setUrl(url[i]);
				am.setDescription(desc[i]);
				am = attachmentManager.add(am);
				
				crvm.setAttachmentId(am.getId());
				crvm.setMemo(memo[i]);
				contractReviewMemoManager.add(crvm);
			}
		}
		
		return contractReviewFormDao.update(contractReviewForm);
	}

	public ContractReviewForm findOneById(Long id) {
		return contractReviewFormDao.findOneById(id);
	}

	@Transactional(readOnly=true)
	public Page<ContractReviewForm> list(ContractReviewFormForm contractReviewFormForm) {
		return contractReviewFormDao.list(contractReviewFormForm);
	}

	public int delete(Long id) {		
		return contractReviewFormDao.delete(id);
	}

	@Override
	public ContractReviewForm findOneByFlow(Long workFlowId) {		
		return contractReviewFormDao.findOneByFlow(workFlowId);
	}
}