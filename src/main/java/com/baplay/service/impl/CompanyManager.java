package com.baplay.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.ICompanyDao;
import com.baplay.entity.Company;
import com.baplay.form.CompanyForm;
import com.baplay.service.ICompanyManager;

@Service
@Transactional
public class CompanyManager implements ICompanyManager {
	@Autowired
	private ICompanyDao companyDao;
	
	@Override
	public Company add(Company company) {
		return companyDao.add(company);
	}

	@Override
	public int update(Company company) {
		return companyDao.update(company);
	}

	@Override
	public Company findOneById(Long id) {
		return companyDao.findOneById(id);
	}

	@Override
	public Page<Company> list(CompanyForm companyForm) {
		return companyDao.list(companyForm);
	}

	@Override
	public int delete(Long id, Long modifier) {
		return companyDao.delete(id, modifier);
	}

	@Override
	public List<Company> findAllCompany() {
		return companyDao.findAllCompany();
	}

	@Override
	public Company findOneByName(String keyword, boolean local) {
		return companyDao.findOneByName(keyword, local);
	}
	
	@Override
	public List<Company> findAllGameCompany() {
		return companyDao.findAllGameCompany();
	}
}