package com.baplay.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IPayTypeDao;
import com.baplay.entity.PayType;
import com.baplay.form.PayTypeForm;
import com.baplay.service.IPayTypeManager;

@Service
@Transactional
public class PayTypeManager implements IPayTypeManager {
	
	@Autowired
	private IPayTypeDao payTypeDao;

	@Override
	public PayType findOneById(Long id) {
		return payTypeDao.findOneById(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<PayType> findAllPayType() {
		return payTypeDao.findAllPayType();
	}

	@Override
	public PayType add(PayType payType) {		
		return payTypeDao.add(payType);
	}

	@Override
	public int update(PayType payType) {
		return payTypeDao.update(payType);
	}

	@Override
	public int delete(Long id) {
		return payTypeDao.delete(id);
	}

	@Override
	public Page<PayType> list(PayTypeForm payTypeForm) {		
		return payTypeDao.list(payTypeForm);
	}
}