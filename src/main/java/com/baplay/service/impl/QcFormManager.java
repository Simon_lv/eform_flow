package com.baplay.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IQcFormDao;
import com.baplay.dao.ISerialNoDao;
import com.baplay.dao.IWorkFlowDao;
import com.baplay.entity.QcForm;
import com.baplay.entity.QcFormDetailField;
import com.baplay.entity.QcFormTitleField;
import com.baplay.entity.TEfunuser;
import com.baplay.form.QcFormForm;
import com.baplay.service.IFlowManager;
import com.baplay.service.IQcFormDetailFieldManager;
import com.baplay.service.IQcFormManager;
import com.baplay.service.IQcFormTitleFieldManager;

@Service
@Transactional
public class QcFormManager implements IQcFormManager {

	@Autowired
	private IQcFormDao qcFormDao;
	@Autowired
	private IQcFormTitleFieldManager qcFormTitleFieldManager;
	@Autowired
	private IQcFormDetailFieldManager qcFormDetailFieldManager;
	@Autowired
	private ISerialNoDao serialNoDao;
	@Autowired
	private IFlowManager flowManager;
	@Autowired
	private IWorkFlowDao workFlowDao;
	
	@Override
	public Page<QcForm> list(QcFormForm qcFormForm) {		
		return qcFormDao.list(qcFormForm);
	}

	@Override
	public QcForm add(QcForm qcForm, String[] subTitle, String[] qcResult, String[] pmResult, String[] remark, 
			TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception {
		
		// Flow
		long workFlowId = -1;

		if (isSubmit) { // 送審
			String serialNo = serialNoDao.findSerialNo(formCode);
			qcForm.setSerialNo(serialNo);
			// flow
			workFlowId = flowManager.startFlow(formType, formCode, serialNo, user, -1, qcForm);
			qcForm.setWorkFlowId(workFlowId);
		} else { // 草稿
			// draft			
			qcForm.setSerialNo(StringUtils.EMPTY);
			workFlowId = flowManager.saveDraft(qcForm, user.getId(), formType, formCode, -1);
		}
		
		qcForm.setWorkFlowId(workFlowId);
		
		List<QcFormTitleField> formTitleField = qcFormTitleFieldManager.findAll(qcForm.getSchemaId(), 0L);
		List<QcFormDetailField> formDetailField = qcFormDetailFieldManager.findAll(qcForm.getSchemaId(), 0L);
		
		qcForm.setFormId(System.currentTimeMillis());
		
		QcForm qcf = null;
		//save Title_Field
		for(int i=0;i<subTitle.length;i++){
			qcForm.setTitleId(formTitleField.get(i).getId());
			qcForm.setSubTitle(subTitle[i]);
			qcForm.setFieldId(null);
			qcForm.setQcResult(null);
			qcForm.setPmResult(null);
			qcForm.setRemark(null);
			qcf = qcFormDao.save(qcForm);
		}
		
		//save Detail_Field
		for(int i=0;i<formDetailField.size();i++){			
			if(i >= qcResult.length) {
				break;
			}
			
			qcForm.setFieldId(formDetailField.get(i).getId());
			qcForm.setQcResult(Integer.parseInt(qcResult[i]));
			qcForm.setTitleId(null);
			qcForm.setPmResult(null);
			qcForm.setRemark(remark[i]);
			qcForm.setSubTitle(null);
			qcf = qcFormDao.save(qcForm);
		}
		
		// Update formId to workFlow		
		workFlowDao.update(workFlowId, qcForm.getFormId());		
		return qcf;
	}

	@Override
	public int update(QcForm qcForm, String[] subTitle, String[] qcResult, String[] pmResult, String[] remark,
			TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception {		
		// Flow
		if(isSubmit) {  // 送審
			if(StringUtils.isEmpty(qcForm.getSerialNo())) {
				String serialNo = serialNoDao.findSerialNo(formCode);			
				qcForm.setSerialNo(serialNo);
			}
			else {  // 退件重送
				flowManager.cancelStampUsed(qcForm.getWorkFlowId());
			}
			// flow
			flowManager.startFlow(formType, formCode, qcForm.getSerialNo(), user, qcForm.getWorkFlowId(), qcForm);
		}
		else {  // 草稿
			if(StringUtils.isEmpty(qcForm.getSerialNo())) {
				qcForm.setSerialNo(StringUtils.EMPTY);
			}
			else {  // 退件重送
				flowManager.cancelStampUsed(qcForm.getWorkFlowId());
			}
			// draft			
			flowManager.saveDraft(qcForm, user.getId(), formType, formCode, qcForm.getWorkFlowId());
		}
		
		List<QcFormTitleField> formTitleField = qcFormTitleFieldManager.findAll(qcForm.getSchemaId(), 0L);
		List<QcFormDetailField> formDetailField = qcFormDetailFieldManager.findAll(qcForm.getSchemaId(), 0L);
						
		int result = 0;
		// sub title
		for(int i=0;i<subTitle.length;i++){			
			qcForm.setTitleId(formTitleField.get(i).getId());
			qcForm.setSubTitle(subTitle[i]);
			qcForm.setQcResult(null);
			qcForm.setPmResult(null);
			qcForm.setRemark(null);			
			result = qcFormDao.update(qcForm);
		}		
		
		// result
		for(int i=0;i<formDetailField.size();i++){
			if(i >= qcResult.length) {
				break;
			}
			
			qcForm.setTitleId(null);  // reset 避免SQL產生錯誤
			qcForm.setFieldId(formDetailField.get(i).getId());
			qcForm.setQcResult(Integer.parseInt(qcResult[i]));
			// 退件重送不清舊資料
			QcForm history = qcFormDao.findOneByFieldId(qcForm.getFormId(), formDetailField.get(i).getId());
			if(history == null || history.getPmResult() == null) {  
				qcForm.setPmResult(null);
			}
			else {
				qcForm.setPmResult(history.getPmResult());
			}
			
			qcForm.setRemark(remark[i]);
			qcForm.setSubTitle(null);
			result = qcFormDao.update(qcForm);
		}
		
		return result;
	}

	@Override
	public List<QcForm> findAllByFormId(Long formId) {		
		return qcFormDao.findAllByFormId(formId);
	}

	@Override
	public List<QcForm> findAllByFlow(Long workFlowId) {
		return qcFormDao.findAllByFlow(workFlowId);
	}
	
	@Override
	public QcForm add(QcForm qcForm) {
		return qcFormDao.save(qcForm);
	}
	
	@Override
	public int update(QcForm qcForm) {
		return qcFormDao.update(qcForm);
	}
	
	@Override
	public void update(List<QcForm> formList) {
		for(QcForm form : formList) {
			qcFormDao.update(form);
		}	
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IQcFormManager#findOneByFieldId(java.lang.Long, java.lang.Long)
	 */
	@Override
	public QcForm findOneByFieldId(Long formId, Long fieldId) {
		return qcFormDao.findOneByFieldId(formId, fieldId);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IQcFormManager#updateResult(java.lang.Long, java.lang.Long, java.lang.Integer, java.lang.String)
	 */
	@Override
	public int updateResult(Long formId, Long fieldId, Integer result, String remark) {
		return qcFormDao.updateResult(formId, fieldId, result, remark);
	}
}