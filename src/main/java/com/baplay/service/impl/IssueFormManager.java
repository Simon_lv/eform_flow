package com.baplay.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IIssueFormDao;
import com.baplay.dao.IIssueMemoDao;
import com.baplay.dao.ISerialNoDao;
import com.baplay.dao.IWorkFlowDao;
import com.baplay.entity.IssueForm;
import com.baplay.entity.IssueMemo;
import com.baplay.entity.TEfunuser;
import com.baplay.form.IssueFormForm;
import com.baplay.service.IAttachmentManager;
import com.baplay.service.IFlowManager;
import com.baplay.service.IIssueFormManager;

@Service
@Transactional
public class IssueFormManager implements IIssueFormManager {
	@Autowired
	private IIssueFormDao issueFormDao;
	@Autowired
	private ISerialNoDao serialNoDal;
	@Autowired
	private IFlowManager flowManager;
	@Autowired
	private IWorkFlowDao workFlowDao;
	@Autowired
	private IAttachmentManager attachmentManager;
	@Autowired
	private IIssueMemoDao issueMemoDao;	
		
	public IssueForm add(IssueForm issueForm, String[] url, String[] desc, TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception{
		// Flow
		long workFlowId = -1;
		
		if(isSubmit) {  // 送審
			String serialNo = serialNoDal.findSerialNo(formCode);
			issueForm.setSerialNo(serialNo);
			
			// flow
			workFlowId = flowManager.startFlow(formType, formCode, serialNo, user, -1, issueForm);			
		}
		else {  // 草稿
			// draft
			issueForm.setSerialNo(StringUtils.EMPTY);
			workFlowId = flowManager.saveDraft(issueForm, user.getId(), formType, formCode, -1);
		}
		
		issueForm.setWorkFlowId(workFlowId);
		IssueForm isf = issueFormDao.add(issueForm);
		// Update formId to workFlow
		workFlowDao.update(workFlowId, isf.getId());
		
		// attachment
		attachmentManager.add(url, desc, isf.getId(), formCode, user.getId());
		
		// memo
		memo(isf.getId(), issueForm.getMemoOperator(), issueForm.getMemo());
		
		return isf;
	}
	
	public int update(IssueForm issueForm, String[] url, String[] desc, TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception{
		// Flow
		if(isSubmit) {  // 送審
			if(StringUtils.isEmpty(issueForm.getSerialNo())) {
				String serialNo = serialNoDal.findSerialNo(formCode);
				issueForm.setSerialNo(serialNo);
			}
			else {  // 退件重送
				flowManager.cancelStampUsed(issueForm.getWorkFlowId());
			}
			// flow
			flowManager.startFlow(formType, formCode, issueForm.getSerialNo(), user, issueForm.getWorkFlowId(), issueForm);
		}
		else {  // 草稿
			// draft
			if(StringUtils.isEmpty(issueForm.getSerialNo())) {
				issueForm.setSerialNo(StringUtils.EMPTY);
			}
			else {  // 退件重送
				flowManager.cancelStampUsed(issueForm.getWorkFlowId());
			}
			
			flowManager.saveDraft(issueForm, user.getId(), formType, formCode, issueForm.getWorkFlowId());
		}
		
		// attachment
		attachmentManager.deleteAll(issueForm.getId(), formCode);
		
		attachmentManager.add(url, desc, issueForm.getId(), formCode, user.getId());
		
		// memo
		memo(issueForm.getId(), issueForm.getMemoOperator(), issueForm.getMemo());
		
		return issueFormDao.update(issueForm);
	}
	
	public IssueForm findOneById(Long id) {
		return issueFormDao.findOneById(id);
	}
	
	public Page<IssueForm> list(IssueFormForm issueFormForm) {
		return issueFormDao.list(issueFormForm);
	}
	
	public int delete(Long id) {
		return issueFormDao.delete(id);
	}

	@Override
	public IssueForm findOneByFlow(Long workFlowId) {		
		return issueFormDao.findOneByFlow(workFlowId);
	}
	
	public int update(IssueForm form) {
		return issueFormDao.update(form);
	}
	
	@Override
	public void memo(Long issueFormId, Long uid, String memo) {
		if(StringUtils.isEmpty(memo)) {
			return;
		}
		
		IssueMemo issueMemo = new IssueMemo();
		issueMemo.setIssueFormId(issueFormId);
		issueMemo.setMemo(memo);
		issueMemo.setCreator(uid);
		
		issueMemoDao.add(issueMemo);
	}
	
	@Override
	public List<IssueMemo> findMemo(Long issueFormId) {
		return issueMemoDao.findMemo(issueFormId);
	}
}