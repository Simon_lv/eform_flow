package com.baplay.service.impl;

import java.math.BigDecimal;
import java.sql.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IPaymentFormDao;
import com.baplay.dao.IReceiptDao;
import com.baplay.dao.ISerialNoDao;
import com.baplay.dao.IWorkFlowDao;
import com.baplay.entity.PaymentForm;
import com.baplay.entity.Receipt;
import com.baplay.entity.TEfunuser;
import com.baplay.form.PaymentFormForm;
import com.baplay.service.IAttachmentManager;
import com.baplay.service.IFlowManager;
import com.baplay.service.IPaymentFormManager;

@Service
@Transactional
public class PaymentFormManager implements IPaymentFormManager {
	
	protected static final Logger log = LoggerFactory.getLogger(PaymentFormManager.class);
	
	@Autowired
	private IPaymentFormDao paymentFormDao;
	@Autowired
	private IReceiptDao receiptDao;
	@Autowired
	private IFlowManager flowManager;
	@Autowired
	private IWorkFlowDao workFlowDao;
	@Autowired
	private ISerialNoDao serialNoDao;
	@Autowired
	private IAttachmentManager attachmentManager;
		
	public PaymentForm add(HttpServletRequest request, PaymentForm paymentForm, TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception {
		// Flow
		long workFlowId = -1;
		
		if(isSubmit) {  // 送審
			String serialNo = serialNoDao.findSerialNo(formCode);
			paymentForm.setSerialNo(serialNo);
			
			// flow
			workFlowId = flowManager.startFlow(formType, formCode, serialNo, user, -1, paymentForm);			
		}
		else {  // 草稿
			// draft
			paymentForm.setSerialNo(StringUtils.EMPTY);
			workFlowId = flowManager.saveDraft(paymentForm, user.getId(), formType, formCode, -1);
		}		
		
		String costDate[] = request.getParameterValues("costDate");
		String costItem[] = request.getParameterValues("costItem");
		String description[] = request.getParameterValues("description");
		String amount[] = request.getParameterValues("amount");
		String currency[] = request.getParameterValues("currency");
		String rate[] = request.getParameterValues("rate");
		
		paymentForm.setWorkFlowId(workFlowId);		
		PaymentForm pf = paymentFormDao.add(paymentForm);
		// Update formId to workFlow
		workFlowDao.update(workFlowId, pf.getId());		
		
		Receipt receipt = new Receipt();
		receipt.setFormId(pf.getId());
		receipt.setFormType(formType);
		receipt.setCostType(1);
		
		// description nullable
		for(int i=0;i<costDate.length;i++){	
			if(StringUtils.isNotBlank(costDate[i]) && StringUtils.isNotBlank(costItem[i]) && StringUtils.isNotBlank(amount[i])){
				receipt.setCostDate(Date.valueOf(costDate[i]));
				receipt.setCostItem(costItem[i]);
				receipt.setDescription(description[i]);
				receipt.setAmount(new BigDecimal(amount[i]));
				receipt.setCurrency(currency[i]);
				receipt.setRate(Double.parseDouble(rate[i]));
				receiptDao.add(receipt);
			}			
		}
		
		// attachment
		String url[] = request.getParameterValues("url");
		String desc[] = request.getParameterValues("fileName");
		
		attachmentManager.add(url, desc, pf.getId(), formCode, user.getId());
		
		return pf;
	}
	
	public int update(HttpServletRequest request, PaymentForm paymentForm, TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception {
		// Flow
		if(isSubmit) {  // 送審
			if(StringUtils.isEmpty(paymentForm.getSerialNo())) {
				String serialNo = serialNoDao.findSerialNo(formCode);
				paymentForm.setSerialNo(serialNo);
			}
			else {  // 退件重送
				flowManager.cancelStampUsed(paymentForm.getWorkFlowId());
			}
			// flow
			flowManager.startFlow(formType, formCode, paymentForm.getSerialNo(), user, paymentForm.getWorkFlowId(), paymentForm);
		}
		else {  // 草稿
			if(StringUtils.isEmpty(paymentForm.getSerialNo())) {
				paymentForm.setSerialNo(StringUtils.EMPTY);
			}
			else {  // 退件重送
				flowManager.cancelStampUsed(paymentForm.getWorkFlowId());
			}
			
			flowManager.saveDraft(paymentForm, user.getId(), formType, formCode, paymentForm.getWorkFlowId());
		}		
				
		String costDate[] = request.getParameterValues("costDate");
		String costItem[] = request.getParameterValues("costItem");
		String description[] = request.getParameterValues("description");
		String amount[] = request.getParameterValues("amount");
		String currency[] = request.getParameterValues("currency");
		String rate[] = request.getParameterValues("rate");
				
		Receipt receipt = new Receipt();
		receipt.setFormId(paymentForm.getId());
		receipt.setFormType(formType);		
		receipt.setCostType(1);
				
		receiptDao.deleteAll(receipt.getFormId(), receipt.getFormType());
		
		for(int i=0;i<costDate.length;i++){
			if(StringUtils.isNotBlank(costDate[i]) && StringUtils.isNotBlank(costItem[i]) && StringUtils.isNotBlank(amount[i])){
				receipt.setCostDate(Date.valueOf(costDate[i]));
				receipt.setCostItem(costItem[i]);
				receipt.setDescription(description[i]);
				receipt.setAmount(new BigDecimal(amount[i]));
				receipt.setCurrency(currency[i]);
				receipt.setRate(Double.parseDouble(rate[i]));
				receiptDao.add(receipt);
			}
		}
		
		// attachment
		String url[] = request.getParameterValues("url");
		String desc[] = request.getParameterValues("fileName");
		// delete history
		attachmentManager.deleteAll(paymentForm.getId(), formCode);
		
		attachmentManager.add(url, desc, paymentForm.getId(), formCode, user.getId());			
		
		return paymentFormDao.update(paymentForm);
	}
	
	public PaymentForm findOneById(Long id) {
		return paymentFormDao.findOneById(id);
	}
	
	@Transactional(readOnly=true)
	public Page<PaymentForm> list(PaymentFormForm paymentFormForm) {
		return paymentFormDao.list(paymentFormForm);
	}
	
	public int delete(Long id, String formType) {		
		receiptDao.deleteAll(id, formType);
		return paymentFormDao.delete(id);
	}

	public PaymentForm findOneByFlow(Long id) {
		return paymentFormDao.findOneByFlow(id);
	}		
}