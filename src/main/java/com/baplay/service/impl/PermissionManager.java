package com.baplay.service.impl;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baplay.dao.IFormRoleDao;
import com.baplay.dao.IPermissionDao;
import com.baplay.dao.IRoleDao;
import com.baplay.dao.IUserDao;
import com.baplay.dao.IUserEmployeeDao;
import com.baplay.entity.Employee;
import com.baplay.entity.FormRole;
import com.baplay.entity.Function;
import com.baplay.entity.Role;
import com.baplay.entity.RoleFunction;
import com.baplay.entity.TEfunuser;
import com.baplay.entity.UserEmployee;
import com.baplay.form.RoleForm;
import com.baplay.form.UserForm;
import com.baplay.service.IEmployeeManager;
import com.baplay.service.IPermissionManager;
import com.baplay.shiro.UserRealm;
import com.baplay.util.MD5Util;
import com.google.common.collect.Lists;

@Service("PermissionManager")
@Transactional
public class PermissionManager implements IPermissionManager {
	protected static final Logger log = LoggerFactory.getLogger(PermissionManager.class);
	
	@Autowired
	private IPermissionDao permissionDao;

	@Autowired
	private IUserDao userDao;

	@Autowired
	private IRoleDao roleDao;
	
	@Autowired
	private IFormRoleDao formRoleDao;
	
	@Autowired
	private IUserEmployeeDao userEmployeeDao;

	@Autowired
	private UserRealm realm;
	
	@Autowired
	private	IEmployeeManager employeeManager;

	@Override
	@Transactional(readOnly = true)
	public List<Function> getFunctions(long roleId, int all) {
		List<Long> roleIds = Lists.newArrayList();
		roleIds.add(roleId);
		return permissionDao.getFunctions(roleIds, all);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Role> getRole(long userId) {
		return roleDao.getRole(userId);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<FormRole> getFormRole(long userId) {
		return formRoleDao.getRole(userId);
	}

	@Override
	public void addRole(RoleForm form) {
		long roleId = roleDao.addRole(form.getName());
		List<Long> funIds = Lists.newArrayList();
		
		for (RoleFunction rf : form.getRoleFunctions()) {
			if(rf.getFunctionId() != null) {
				funIds.add(rf.getFunctionId());
			}
		}
		roleDao.addRoleFunction(roleId, funIds);
	}

	@Override
	public void updateRole(RoleForm form) {
		roleDao.updateRole(form.getRoleId(), form.getName());
		List<Long> funIds = Lists.newArrayList();
		for (RoleFunction rf : form.getRoleFunctions()) {
			if (rf.getFunctionId() != null) {
				funIds.add(rf.getFunctionId());
			}
		}
		roleDao.addRoleFunction(form.getRoleId(), funIds);
		realm.clearCache(SecurityUtils.getSubject().getPrincipals());
	}

	public UserForm getUser(Long id) {
		return userDao.findById(id);
	}

	@Override
	public TEfunuser addUser(UserForm form) {
		form.setPassword(MD5Util.MD5(form.getPassword()));
		TEfunuser user = userDao.addUser(form);
		
		List<Long> roles = Lists.newArrayList();
		roles.add(form.getRoleId1());
		roles.add(form.getRoleId2());
		userDao.addUserRole(user.getId(), roles);
		
		List<Long> froles = Lists.newArrayList();
		froles.add(form.getFormRoleId1());
		froles.add(form.getFormRoleId2());
		froles.add(form.getFormRoleId3());
		froles.add(form.getFormRoleId4());
		froles.add(form.getFormRoleId5());
		froles.add(form.getFormRoleId6());
		userDao.addUserFormRole(user.getId(), froles, form.getFormRoleDeptLimit());
		
		// employee mapping
		List<Long> mapping = form.getEmployeeMapping();
		int count = 0;
		
		if(mapping != null) {
			for(Long employeeId : mapping) {
				if(employeeId == null) {
					continue;
				}
				
				if(count == 0) {
					userDao.updateEmployeeId(employeeId, user.getId());
					count++;
				}
				else {
					userEmployeeDao.add(user.getId(), employeeId);
				}	
			}
		}
		
		realm.clearCache(SecurityUtils.getSubject().getPrincipals());
		
		return user;
	}

	@Override
	public void updateUser(UserForm form) {
		// password
		if(StringUtils.isNotBlank(form.getPassword())) {
			form.setPassword(MD5Util.MD5(form.getPassword()));
		} 
		else {
			form.setPassword(null);
		}			
		
		userDao.updateUser(form);
		List<Long> roles = Lists.newArrayList();
		roles.add(form.getRoleId1());
		roles.add(form.getRoleId2());
		userDao.addUserRole(form.getId(), roles);
		
		List<Long> froles = Lists.newArrayList();
		froles.add(form.getFormRoleId1());
		froles.add(form.getFormRoleId2());
		froles.add(form.getFormRoleId3());
		froles.add(form.getFormRoleId4());
		froles.add(form.getFormRoleId5());
		froles.add(form.getFormRoleId6());
		userDao.addUserFormRole(form.getId(), froles, form.getFormRoleDeptLimit());
		
		// employee mapping
		userEmployeeDao.deleteByUID(form.getId());  // delete original
		int count = 0;
		
		List<Long> mapping = form.getEmployeeMapping();
		
		if(mapping != null) {
			for(Long employeeId : mapping) {
				if(employeeId == null) {
					continue;
				}
				
				if(count == 0) {
					userDao.updateEmployeeId(employeeId, form.getId());
					count++;
				}
				else {
					userEmployeeDao.add(form.getId(), employeeId);
				}	
			}
		}
		
		realm.clearCache(SecurityUtils.getSubject().getPrincipals());
	}

	@Override
	public void updatePwd(long userId, String password) {
		userDao.updatePwd(userId, password);
	}

	@Override
	public void updateRoleStatus(Long roleId, Integer status) {
		roleDao.updateRoleStatus(roleId, status);
		realm.clearCache(SecurityUtils.getSubject().getPrincipals());
	}

	@Override
	public void updateUserStatus(Integer status, Long userId) {
		userDao.updateUserStatus(userId, status);
		realm.clearCache(SecurityUtils.getSubject().getPrincipals());
	}

	@Override
	public TEfunuser findByUserName(String userName) {
		return userDao.findByUserName(userName);
	}

	@Override
	public List<Role> getRoles(int all) {
		return roleDao.getAllRole(all);
	}
	
	@Override
	public List<FormRole> getFormRoles(int all) {		
		return roleDao.getAllFormRole(all);
	}

	@Override
	public List<TEfunuser> getUsers(String userName) {
		return userDao.getAllUser(userName);
	}

	@Override
	public boolean checkLoginAccountExist(String loginAccount) {
		// TODO Auto-generated method stub		
		return permissionDao.checkLoginAccountExist(loginAccount);
	}

	@Override
	public Long findByEmployeeId(Long employeeId) {
		return userDao.findByEmployeeId(employeeId);
	}	

	/* (non-Javadoc)
	 * @see com.baplay.service.IPermissionManager#findMoreEmployeeByUID(long)
	 */
	@Override
	public List<Long> findMoreEmployeeByUID(long userId) {
		return userEmployeeDao.findByUID(userId);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IPermissionManager#addUserEmpMapping(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void addUserEmpMapping(Long userId, Long employeeId) {
		userEmployeeDao.add(userId, employeeId);
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IPermissionManager#allUserEmpMapping()
	 */
	@Override
	public Map<Long, List<Long>> allUserEmpMapping(List<TEfunuser> userList) {
		if(userList == null) {
			userList = userDao.getAllUser(null);
		}
		
		List<UserEmployee> list = userEmployeeDao.findAll();
		Map<Long, List<Long>> mapping = new LinkedHashMap<Long, List<Long>>();
		
		// init by t_efun_user
		for(TEfunuser user : userList) {
			List<Long> empList = new LinkedList<Long>();
			empList.add(user.getEmployeeId());
			mapping.put(user.getId(), empList);
		}
		
		// merge
		for(UserEmployee m : list) {
			List<Long> empList = mapping.get(m.getUserId());
			
			if(empList == null) {
				continue;  // illegal
			}
			
			empList.add(m.getEmployeeId());			
		}
		
		return mapping;
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IPermissionManager#employeeResigned(com.baplay.entity.Employee)
	 */
	@Override
	public void employeeResigned(Employee employee) {
		userDao.updateUserStatus(employee.getUserId(), 2);
		employeeManager.delete(employee.getUserId(), "X" + employee.getEmployeeNo(), 1L);
	}
}