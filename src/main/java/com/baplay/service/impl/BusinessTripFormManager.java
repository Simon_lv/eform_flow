package com.baplay.service.impl;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IBusinessTripFormDao;
import com.baplay.dao.ISerialNoDao;
import com.baplay.dao.IWorkFlowDao;
import com.baplay.entity.BusinessTripForm;
import com.baplay.entity.TEfunuser;
import com.baplay.form.BusinessTripFormForm;
import com.baplay.service.IAttachmentManager;
import com.baplay.service.IBusinessTripFormManager;
import com.baplay.service.IFlowManager;

@Service
@Transactional
public class BusinessTripFormManager implements IBusinessTripFormManager {
	
	protected static final Logger log = LoggerFactory.getLogger(BusinessTripFormManager.class);
	
	@Autowired
	private IBusinessTripFormDao businessTripFormDao;
	@Autowired
	private ISerialNoDao serialNoDao;
	@Autowired
	private IFlowManager flowManager;
	@Autowired
	private IWorkFlowDao workFlowDao;
	@Autowired
	private IAttachmentManager attachmentManager;
		
	public BusinessTripForm add(BusinessTripForm businessTripForm, String url[], String desc[], TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception {		
		// Flow
		long workFlowId = -1;
		
		if(isSubmit) {  // 送審
			String serialNo = serialNoDao.findSerialNo(formCode);
			businessTripForm.setSerialNo(serialNo);
			
			// flow
			workFlowId = flowManager.startFlow(formType, formCode, serialNo, user, -1, businessTripForm);
			businessTripForm.setWorkFlowId(workFlowId);
		}
		else {  // 草稿
			// draft
			businessTripForm.setSerialNo(StringUtils.EMPTY);
			workFlowId = flowManager.saveDraft(businessTripForm, user.getId(), formType, formCode, -1);
		}
		
		businessTripForm.setWorkFlowId(workFlowId);
		BusinessTripForm btf = businessTripFormDao.add(businessTripForm);
		// Update formId to workFlow
		workFlowDao.update(workFlowId, btf.getId());
		
		// attachment
		attachmentManager.add(url, desc, btf.getId(), formCode, user.getId());				
		
		return btf;
	}
	
	public int update(BusinessTripForm businessTripForm, String url[], String desc[], TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception {		
		// Flow
		if(isSubmit) {  // 送審
			if(StringUtils.isEmpty(businessTripForm.getSerialNo())) {
				String serialNo = serialNoDao.findSerialNo(formCode);
				businessTripForm.setSerialNo(serialNo);
			}
			else {  // 退件重送
				flowManager.cancelStampUsed(businessTripForm.getWorkFlowId());
			}
			// flow
			flowManager.startFlow(formType, formCode, businessTripForm.getSerialNo(), user, businessTripForm.getWorkFlowId(), businessTripForm);
		}
		else {  // 草稿
			// draft
			if(StringUtils.isEmpty(businessTripForm.getSerialNo())) {
				businessTripForm.setSerialNo(StringUtils.EMPTY);
			}
			else {  // 退件重送
				flowManager.cancelStampUsed(businessTripForm.getWorkFlowId());
			}
			
			flowManager.saveDraft(businessTripForm, user.getId(), formType, formCode, businessTripForm.getWorkFlowId());
		}
		
		// attachment
		// delete history
		attachmentManager.deleteAll(businessTripForm.getId(), formCode);
		
		attachmentManager.add(url, desc, businessTripForm.getId(), formCode, user.getId());							
		
		return businessTripFormDao.update(businessTripForm);
	}
	
	public BusinessTripForm findOneById(Long id) {
		return businessTripFormDao.findOneById(id);
	}
	
	@Transactional(readOnly=true)
	public Page<BusinessTripForm> list(BusinessTripFormForm businessTripFormForm) {
		return businessTripFormDao.list(businessTripFormForm);
	}
	
	public int delete(Long id) {
		return businessTripFormDao.delete(id);
	}

	@Override
	public BusinessTripForm findOneByFlow(Long workFlowId) {		
		return businessTripFormDao.findOneByFlow(workFlowId);
	}	
}