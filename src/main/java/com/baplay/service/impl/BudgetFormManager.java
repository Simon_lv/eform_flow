package com.baplay.service.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IBudgetFormDao;
import com.baplay.dao.IBudgetFormDetailDao;
import com.baplay.dao.ISerialNoDao;
import com.baplay.dao.IWorkFlowDao;
import com.baplay.entity.BudgetForm;
import com.baplay.entity.BudgetFormDetail;
import com.baplay.entity.TEfunuser;
import com.baplay.form.BudgetFormForm;
import com.baplay.service.IBudgetFormManager;
import com.baplay.service.IFlowManager;

@Service
@Transactional
public class BudgetFormManager implements IBudgetFormManager {

	protected static final Logger log = LoggerFactory.getLogger(BudgetFormManager.class);

	@Autowired
	private IBudgetFormDao budgetFormDao;
	@Autowired
	private ISerialNoDao serialNoDao;
	@Autowired
	private IFlowManager flowManager;
	@Autowired
	private IWorkFlowDao workFlowDao;
	@Autowired
	private IBudgetFormDetailDao budgetFormDetailDao;

	public BudgetForm add(HttpServletRequest request, BudgetForm budgetForm, TEfunuser user, boolean isSubmit,
			boolean isAdditional, String formType, String formCode) throws Exception {
		
		budgetForm.setYearMonth(budgetForm.getYearMonth().replaceAll("-", ""));
		
		// Flow
		long workFlowId = -1;

		if (isSubmit) { // 送審
			String serialNo = serialNoDao.findSerialNo(formCode);
			budgetForm.setSerialNo(serialNo);

			// flow
			workFlowId = flowManager.startFlow(formType, formCode, serialNo, user, -1, budgetForm);
		} else { // 草稿
			// draft
			budgetForm.setSerialNo(StringUtils.EMPTY);
			workFlowId = flowManager.saveDraft(budgetForm, user.getId(), formType, formCode, -1);
		}
		budgetForm.setWorkFlowId(workFlowId);
		
		BudgetForm bf = budgetFormDao.add(budgetForm);
		// Update formId to workFlow
		workFlowDao.update(workFlowId, bf.getId());

		String itemTitle[] = request.getParameterValues("itemTitle");
		String itemDescription[] = request.getParameterValues("itemDescription");
		String amount[] = request.getParameterValues("amount");		

		BudgetFormDetail bfd = new BudgetFormDetail();
		bfd.setBudgetFormId(bf.getId());

		budgetFormDetailDao.deleteAll(bfd.getBudgetFormId());

		if(isAdditional){
			bfd.setAdditional(1);
		}else{
			bfd.setAdditional(2);
		}
		
		for (int i = 0; i < itemTitle.length; i++) {
			if (StringUtils.isNotBlank(itemTitle[i]) && StringUtils.isNotBlank(amount[i])) {
				bfd.setItemTitle(itemTitle[i]);
				bfd.setItemDescription(itemDescription[i]);
				bfd.setAmount(Integer.parseInt(amount[i]));				
				budgetFormDetailDao.add(bfd);
			}
		}

		return bf;
	}

	public int update(HttpServletRequest request, BudgetForm budgetForm, TEfunuser user, boolean isSubmit,
			boolean isAdditional, String formType, String formCode) throws Exception {
		
		if(!isAdditional){
			budgetForm.setYearMonth(budgetForm.getYearMonth().replaceAll("-", ""));
		}
		
		// Flow
		if (isSubmit) { // 送審
			if(StringUtils.isEmpty(budgetForm.getSerialNo())) {
				String serialNo = serialNoDao.findSerialNo(formCode);
				budgetForm.setSerialNo(serialNo);
			}
			else {  // 退件重送
				flowManager.cancelStampUsed(budgetForm.getWorkFlowId());
			}
			// flow
			flowManager.startFlow(formType, formCode, budgetForm.getSerialNo(), user, budgetForm.getWorkFlowId(), budgetForm);
		} else { // 草稿
			if(StringUtils.isEmpty(budgetForm.getSerialNo())) {
				budgetForm.setSerialNo(StringUtils.EMPTY);
			}
			else {  // 退件重送
				flowManager.cancelStampUsed(budgetForm.getWorkFlowId());
			}
			
			flowManager.saveDraft(budgetForm, user.getId(), formType, formCode, budgetForm.getWorkFlowId());
		}
		
		String itemTitle[] = request.getParameterValues("itemTitle");
		String itemDescription[] = request.getParameterValues("itemDescription");
		String amount[] = request.getParameterValues("amount");		

		BudgetFormDetail bfd = new BudgetFormDetail();
		bfd.setBudgetFormId(budgetForm.getId());

		if(isAdditional){
			bfd.setAdditional(1);
		}else{
			bfd.setAdditional(2);
		}
		
		budgetFormDetailDao.deleteAll(bfd.getBudgetFormId());

		for (int i = 0; i < itemTitle.length; i++) {
			if (StringUtils.isNotBlank(itemTitle[i]) && StringUtils.isNotBlank(amount[i])) {
				bfd.setItemTitle(itemTitle[i]);
				bfd.setItemDescription(itemDescription[i]);
				bfd.setAmount(Integer.parseInt(amount[i]));				
				budgetFormDetailDao.add(bfd);
			}
		}
				
		return budgetFormDao.update(budgetForm);
	}

	public BudgetForm findOneById(Long id) {
		return budgetFormDao.findOneById(id);
	}

	public Page<BudgetForm> list(BudgetFormForm budgetFormForm) {
		return budgetFormDao.list(budgetFormForm);
	}

	public int delete(Long id) {
		budgetFormDetailDao.deleteAll(id);
		return budgetFormDao.delete(id);
	}

	@Override
	public BudgetForm findOneByFlow(Long workFlowId) {		
		return budgetFormDao.findOneByFlow(workFlowId);
	}

	@Override
	public boolean isFirstBudget(Long id) {		
		return budgetFormDao.isFiestBudget(id);
	}

}
