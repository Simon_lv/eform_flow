package com.baplay.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IGameDataDao;
import com.baplay.entity.GameData;
import com.baplay.form.GameDataForm;
import com.baplay.service.IGameDataManager;

@Service
@Transactional
public class GameDataManager implements IGameDataManager {
	
	@Autowired
	private IGameDataDao gameDataDao;

	/* (non-Javadoc)
	 * @see com.baplay.service.IGameDataManager#findAllGameData()
	 */
	@Override
	@Transactional(readOnly=true)
	public List<GameData> findAllGameData() {
		return gameDataDao.findAllGameData();
	}
	
	/* (non-Javadoc)
	 * @see com.baplay.service.IGameDataManager#findOneById(java.lang.Long)
	 */
	@Override
	public GameData findOneById(Long id) {
		return gameDataDao.findOneById(id);
	}

	@Override
	public GameData add(GameData gameData) {		
		return gameDataDao.add(gameData);
	}

	@Override
	public int update(GameData gameData) {		
		return gameDataDao.update(gameData);
	}

	@Override
	public int delete(Long id) {		
		return gameDataDao.delete(id);
	}

	@Override
	public Page<GameData> list(GameDataForm gameDataForm) {		
		return gameDataDao.list(gameDataForm);
	}
}