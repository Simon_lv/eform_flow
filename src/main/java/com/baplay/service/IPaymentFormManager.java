package com.baplay.service;

import javax.servlet.http.HttpServletRequest;

import org.springside.modules.orm.Page;

import com.baplay.entity.PaymentForm;
import com.baplay.entity.TEfunuser;
import com.baplay.form.PaymentFormForm;

public interface IPaymentFormManager {
	public PaymentForm add(HttpServletRequest request,PaymentForm paymentForm, TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception;
	public int update(HttpServletRequest request,PaymentForm paymentForm, TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception;
	public PaymentForm findOneById(Long id);
	public Page<PaymentForm> list(PaymentFormForm paymentFormForm);
	public int delete(Long id, String formType);
	public PaymentForm findOneByFlow(Long id);
}
