package com.baplay.service;

import javax.servlet.http.HttpServletRequest;

import org.springside.modules.orm.Page;

import com.baplay.entity.BusinessTripExpenseForm;
import com.baplay.entity.TEfunuser;
import com.baplay.form.BusinessTripExpenseFormForm;

public interface IBusinessTripExpenseFormManager {
	public BusinessTripExpenseForm add(HttpServletRequest request,BusinessTripExpenseForm businessTripExpenseForm, TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception;
	public int update(HttpServletRequest request,BusinessTripExpenseForm businessTripExpenseForm, TEfunuser user, boolean isSubmit, String formType, String formCode) throws Exception;
	public int updateTotal(Long id, Long editId, int costType, String formType);
	public BusinessTripExpenseForm findOneById(Long id);
	public Page<BusinessTripExpenseForm> list(BusinessTripExpenseFormForm businessTripExpenseFormForm);
	public int delete(Long id);
	public BusinessTripExpenseForm findOneByFlow(Long workFlowId);
	public void remark(Long workFlowId, String remark);
}