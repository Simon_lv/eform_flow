package com.baplay.schedule;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.baplay.entity.Employee;
import com.baplay.infra.utils.DateUtil;
import com.baplay.service.IEmployeeManager;
import com.baplay.service.IPermissionManager;

@Component
public class EmployeeResignedTask {

	private static Logger log = LoggerFactory.getLogger(EmployeeResignedTask.class);
	@Autowired
	private IEmployeeManager employeeManager;
	@Autowired
	private IPermissionManager permissionManager;
	
	/**
	 * 員工離職(每日5PM)
	 */
	@Scheduled(cron = "0 0 17 * * ?")
	public void employeeResigned() throws Exception {		
		// 全部員工
		List<Employee> employeeList = employeeManager.findAllEmployee();					
		// 本日離職清單
		String pattern = "yyyy-MM-dd";
		Date today = DateUtil.parse("2017-04-30", pattern);//new Date();
		
		List<Employee> todayResignedList = employeeList.stream().
				filter(emp -> emp.getQuitTime() != null && StringUtils.equals(
						DateUtil.getStringOfDate(today, pattern, null), 
						DateUtil.getStringOfDate(emp.getQuitTime(), pattern, null))).
				collect(Collectors.toList());
		
		log.info("resigned list=" + todayResignedList);		
		// 停權
		for(Employee employee : todayResignedList) {
			// 登入帳號停權 + 複製員工(X)刪除
			Long uid = permissionManager.findByEmployeeId(employee.getId());
			
			if(uid == 0) {
				log.info("employee resigned already..." + employee.getName());
				continue;
			}
			
			employee.setUserId(uid);
			permissionManager.employeeResigned(employee);
			log.info("resigned employee=" + employee.getName());	
		}		
	}
}