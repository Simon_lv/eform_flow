package com.efun.bean;

import org.apache.commons.lang3.StringUtils;

import com.efun.entity.Users;


public class UserBean extends Users{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4631961322945499080L;
	
	private String partner;
	
	public final String getPartner() {
		if(StringUtils.isEmpty(partner)){
			return "efun";
		}
		return partner;
	}

	public final void setPartner(String partner) {
		this.partner = partner;
	}
	public static UserBean toUserBean(Users user){
		if(user==null){
			return null;
		}
		UserBean userBean = new UserBean();
		userBean.setAddress(user.getAddress());
		userBean.setAdvertiser(user.getAdvertiser());
		userBean.setEmail(user.getEmail());
		userBean.setFlag(user.getFlag());
		userBean.setGameCode(user.getGameCode());
		userBean.setGpid(user.getGpid());
		userBean.setIdCard(user.getIdCard());
		userBean.setLastLoginIp(user.getLastLoginIp());
		userBean.setLastLoginTime(user.getLastLoginTime());
		userBean.setLoginIp(user.getLoginIp());
		userBean.setLoginTime(user.getLoginTime());
		userBean.setModifiedTime(user.getModifiedTime());
		userBean.setMsnqq(user.getMsnqq());
		userBean.setPlatForm(user.getPlatForm());
		userBean.setRegisterIp(user.getRegisterIp());
		userBean.setRegisterTime(user.getRegisterTime());
		userBean.setTelephone(user.getTelephone());
		userBean.setThirdPlate(user.getThirdPlate());
		userBean.setUid(user.getUid());
		userBean.setUserid(user.getUserid());
		userBean.setUserMac(user.getUserMac());
		userBean.setUserName(user.getUserName());
		userBean.setUserPwd(user.getUserPwd());
		userBean.setAppPlatform(user.getAppPlatform());
		return userBean;
	}
}
