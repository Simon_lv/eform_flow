<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<header class="header">
   <a href="${ctx}/index" class="logo">8888play ERP</a>
   <nav class="navbar navbar-static-top" role="navigation">
       <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
           <span class="sr-only">Toggle navigation</span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
       </a>
       <div class="navbar-right">
           <ul class="nav navbar-nav">
               <li class="dropdown user user-menu">
                   <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                       <i class="glyphicon glyphicon-user"></i>
                       <span>${loginUser.loginAccount}<i class="caret"></i></span>
                   </a>
                   <ul class="dropdown-menu">
                       <!-- User image -->
                       <li class="user-header bg-light-blue">
                           <img src="/images/avatar-default.gif" class="img-circle" alt="User Image" />
                           <p>
                         
                            ${loginUser.loginAccount }
                               <small>${loginUser.createTime}</small>
                           </p>
                       </li>
                       <!-- Menu Body -->
                       <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-right">
                               <%--  <a href="${pageContext.request.contextPath }/passwd" class="btn btn-default btn-flat">修改密碼</a> --%>
                                <a href="${pageContext.request.contextPath }/logout" class="btn btn-primary btn-flat">登 出</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>