<%@ page language="java" pageEncoding="UTF-8" isErrorPage="true"%>
<html>
<head>
<title>Exception!</title>
</head>
<body>
	<%
		Exception ex = (Exception) request.getAttribute("exceptionObj");
	%>
	<H2>
		Exception:
		<%=ex.getMessage()%></H2>
	<P />
	<%
		ex.printStackTrace(new java.io.PrintWriter(out));
	%>
</body>
</html>