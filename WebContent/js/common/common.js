var Common = {};
Common.toPage = function(pageNo) {
	if (pageNo) {
		$('input#pageNo').val(pageNo);
	}
	$('#listForm').ajaxSubmit({
		success : function(data) {
			$('#listContainer').html(data);
		},
		error : function() {
			alert('連接網絡失敗');
		}
	});
	$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
}
$(document).on('click', '.page', function() {
	var pageNo = $(this).attr("data-page");
	if (pageNo == "pageNubmer") {
		pageNo = $("#pageNumber").val();
	}
	Common.toPage(pageNo);
});
module.exports = Common;
