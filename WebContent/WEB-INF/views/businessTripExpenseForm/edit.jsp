<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/businessTripExpenseForm/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${cp.id}" />
		<input type="hidden" name="workFlowId" value="${cp.workFlowId}" />
		<!-- 退件重送 -->
		<input type="hidden" name="serialNo" value="${cp.serialNo}" />
		<div class="table-responsive">
			
			<div style="text-align: center;"><h2 id="title"></h2></div>
			<div style="text-align: center;"><h3>差旅費報支單 Travel Expense Report</h3></div>
			
			<div style="text-align: right;"><label>申請日期:</label><fmt:formatDate value="${cp.requestDate }" type="date" pattern="yyyy-MM-dd" /></div>
			
			<table class="table">
				<tr>
					<td colspan="2" width="50%">
						<label>Dept. 部門</label>					
						<select id="companyId" name="companyId">
                           	<c:forEach items="${sessionScope.company}" var="company">
                           	<option value="${company.id}"
                           		<c:if test="${cp.companyId == company.id}">selected</c:if>
                           		>${company.name}</option>
                           	</c:forEach>
                        </select>
						<span id="department">
						<c:if test="${not empty cp}">${cp.departmentName }</c:if>
						</span>
						<input type="hidden" id="requestDepartment" name="requestDepartment" value="${cp.requestDepartment}" />						
					</td>
					<td colspan="2">
						<label>Purpose of Trip. 差旅計畫</label>
						<input name="purpose" value="${cp.purpose }"/>
					</td>						
				</tr>
				<tr>					
					<td>
						<label>出差員工</label>
						<select name="employeeId" onchange="getEmployeeInfo(this.value)">							
			            <c:forEach items="${companyEmployee}" var="employee">
			                <option value="${employee.id}" <c:if test="${employee.id == (empty cp ? sessionScope.employee.id : cp.employeeId)}">selected="selected"</c:if>>${employee.name}</option>
			            </c:forEach>
			            </select>
					</td>
					<td><label>Duration. 出國期間</label></td>
					<td colspan="2">
						<input type="text" value="<fmt:formatDate value="${cp.durationStart }" type="date" pattern="yyyy-MM-dd" />" 
								onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
								class="Wdate" name="durationStart" id="durationStart">
						~
						<input type="text" value="<fmt:formatDate value="${cp.durationEnd }" type="date" pattern="yyyy-MM-dd" />" 
								onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
								class="Wdate" name="durationEnd" id="durationEnd">
					</td>					
				</tr>
				<tr>
					<td><label>匯款資訊</label></td>
					<td><label>受款人:</label></td>
					<td><label>銀行別:</label></td>
					<td><label>銀行帳號:</label></td>
				</tr>				
				<tr>	
					<td></td>
					<td><input name="payee" value="${cp.payee }"/></td>							
					<td>
						<select id="twBank">
                          	<option value="">請選擇銀行</option>                          	
                        </select>
                        <select id="twBranch">
                          	<option value="">請選擇分行</option>                          
                        </select>
						<input id="payeeBank" name="payeeBank" size="50" value="${cp.payeeBank }"/>
					</td>					
					<td><input name="payeeAccount" value="${cp.payeeAccount }"/></td>						
				</tr>
				<tr>
					<td>出款日</td>					
					<td colspan="3">
						<fmt:formatDate var="remitDate" value="${cp.remitDate}" type="date" pattern="yyyy-MM-dd" />
						<select id="remitDate" name="remitDate">
                           	<c:forEach items="${remitDateItems}" var="d">
                           	<option value="${d}"
                           		<c:if test="${remitDate == d}">selected</c:if>
                           		>${d}</option>
                           	</c:forEach>
                        </select>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<table class="table">
							<tr>
								<td>
									<div><label>Date</label></div>
									<div><label>日期</label></div>
								</td>
								<td>
									<div><label>Place</label></div>
									<div><label>地點</label></div>
								</td>
								<td>
									<div><label>Number</label></div>
									<div><label>單據編號</label></div>
								</td>
								<td>
									<div><label>Description</label></div>
									<div><label>雜項費用說明</label></div>
								</td>
								<td>
									<div><label>Amount</label></div>
									<div><label>金額</label></div>
								</td>
								<td>
									<div><label>Currency</label></div>
									<div><label>幣別</label></div>
								</td>
								<td>
									<div><label>Exchange Rate</label></div>
									<div><label>匯率</label></div>
								</td>
								<td>
									<div><label>Total</label></div>
									<div><label>總計</label></div>
								</td>								
								<td><input type="button" class="btn btn-success" value="增加" onclick="addColumn(1)" /></td>
							</tr>
							<tbody id="costType1">							
							<c:forEach items="${receiptCostType1 }" var="rt1">
							<tr>								
								<td>
									<input type="hidden" name="receiptId" value="${rt1.id }"/>
									<input type="hidden" name="costType" value="1"/>
									<input type="text" style="width: 100px;" value="<fmt:formatDate value="${rt1.costDate }" type="date" pattern="yyyy-MM-dd" />"
											onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
											class="Wdate" name="costDate" id="costDate">
								</td>								
								<td><input style="width: 100px;" name="place" value="${rt1.place }"/></td>
								<td><input style="width: 100px;" name="number" value="${rt1.number }"/></td>
								<td><input style="width: 100px;" name="description" value="${rt1.description }"/></td>
								<td>
									<fmt:parseNumber value="${rt1.amount}" var="amount"/>
									<input type="number" name="amount" style="width: 120px;" onblur="doTotal()"  
										value="<fmt:formatNumber value="${amount}" pattern="#.##"/>"/>									
								</td>
								<td>
									<select id="currency" name="currency">
										<option value="">請選擇</option>
										<option value="TWD" <c:if test="${rt1.currency == 'TWD' }">selected="selected"</c:if>>新臺幣(TWD)</option>
										<option value="HKD" <c:if test="${rt1.currency == 'HKD' }">selected="selected"</c:if>>港幣(HKD)</option>
										<option value="USD" <c:if test="${rt1.currency == 'USD' }">selected="selected"</c:if>>美金(USD)</option>
										<option value="CNY" <c:if test="${rt1.currency == 'CNY' }">selected="selected"</c:if>>人民幣(CNY)</option>
										<option value="KRW" <c:if test="${rt1.currency == 'KRW' }">selected="selected"</c:if>>韓元(KRW)</option>
										<option value="JPY" <c:if test="${rt1.currency == 'JPY' }">selected="selected"</c:if>>日圓(JPY)</option>
									</select>
								</td>
								<td>
									<input type="number" style="width: 55px;" name="rate" id="rate" onblur="doTotal()" 
										value="<fmt:parseNumber integerOnly="${rt1.rate % 1 == 0}" type="number" value="${rt1.rate}" />"/>
								</td>
								<td><div class="TWDollor1"></div></td>
								<td><input class="btn" type="button" value="刪除" onclick="deleteReceipt('${rt1.id}', '${cp.id }', '1')"/></td>
							</tr>
							</c:forEach>
							</tbody>							
							<tr>
								<td colspan="6"><label>Total 總計</label></td>								
								<td colspan="2">
									<input type="hidden" name="genericExpensesTotal" id="genericExpensesTotal">
									<div id="TWDollor1Total">NT$ ${cp.genericExpensesTotal }</div>
								</td>
							</tr>
						</table>						
					</td>
				</tr>									
			</table>
			
			<div><label>Entertainment Expenses   交際費</label></div>
			<table class="table">
				<tr>
					<td>
						<div><label>Date</label></div>
						<div><label>日期</label></div>
					</td>
					<td>
						<div><label>Place</label></div>
						<div><label>地點</label></div>
					</td>
					<td>
						<div><label>Number</label></div>
						<div><label>單據編號</label></div>
					</td>
					<td>
						<div><label>Person/Company Entertained</label></div>
						<div><label>對象</label></div>
					</td>
					<td>
						<div><label>Amount</label></div>
						<div><label>金額</label></div>
					</td>
					<td>
						<div><label>Currency</label></div>
						<div><label>幣別</label></div>
					</td>
					<td>
						<div><label>Exchange Rate</label></div>
						<div><label>匯率</label></div>
					</td>
					<td>
						<div><label>Total</label></div>
						<div><label>總計</label></div>
					</td>
					<td><input type="button" class="btn btn-success" value="增加" onclick="addColumn(2)"/></td>
				</tr>
				<tbody id="costType2">				
				<c:forEach items="${receiptCostType2 }" var="rt2">
				<tr>
					<td>
						<input type="hidden" name="receiptId" value="${rt2.id }"/>
						<input type="hidden" name="costType" value="2"/>
						<input type="text" style="width: 100px;" value="<fmt:formatDate value="${rt2.costDate }" type="date" pattern="yyyy-MM-dd" />"
								onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
								class="Wdate" name="costDate" id="costDate">
					</td>
					<td><input style="width: 100px;" name="place" value="${rt2.place }"/></td>
					<td><input style="width: 100px;" name="number" value="${rt2.number }"/></td>
					<td><input style="width: 100px;" name="description" value="${rt2.description }"/></td>
					<td>
						<fmt:parseNumber value="${rt2.amount}" var="amount"/>
						<input type="number" name="amount" style="width: 120px;" 
							value="<fmt:formatNumber value="${amount}" pattern="#.##"/>" onblur="doTotal()"/>						
					</td>
					<td>
						<select id="currency" name="currency">
							<option value="">請選擇</option>
							<option value="TWD" <c:if test="${rt2.currency == 'TWD' }">selected="selected"</c:if>>新臺幣(TWD)</option>
							<option value="HKD" <c:if test="${rt2.currency == 'HKD' }">selected="selected"</c:if>>港幣(HKD)</option>
							<option value="USD" <c:if test="${rt2.currency == 'USD' }">selected="selected"</c:if>>美金(USD)</option>
							<option value="CNY" <c:if test="${rt2.currency == 'CNY' }">selected="selected"</c:if>>人民幣(CNY)</option>
							<option value="KRW" <c:if test="${rt2.currency == 'KRW' }">selected="selected"</c:if>>韓元(KRW)</option>
							<option value="JPY" <c:if test="${rt2.currency == 'JPY' }">selected="selected"</c:if>>日圓(JPY)</option>
						</select>
					</td>
					<td>
						<input type="number" style="width: 55px;" id="rate" name="rate" onblur="doTotal()" 
							value="<fmt:parseNumber integerOnly="${rt2.rate % 1 == 0}" type="number" value="${rt2.rate}" />"/>
					</td>
					<td><div class="TWDollor2"></div></td>
					<td><input class="btn" type="button" value="刪除" onclick="deleteReceipt('${rt2.id}', '${cp.id }', '2')"/></td>
				</tr>
				</c:forEach>
				</tbody>				
				<tr>
					<td colspan="6"><label>Total 總計</label></td>					
					<td colspan="2">
						<input type="hidden" name="entertainmentExpensesTotal" id="entertainmentExpensesTotal">
						<div id="TWDollor2Total">NT$ ${cp.entertainmentExpensesTotal }</div>
					</td>
				</tr>
			</table>
			
			<table class="table">
				<tr>
					<td><label>Consumption 公司卡消費金額：</label></td>
					<td>
						<input type="number" name="businessCardConsumption" onblur="doTotal()"
						<c:if test="${empty cp }">value="0"</c:if>
						<c:if test="${not empty cp }">value="${cp.businessCardConsumption }"</c:if>
						/>
					</td>
					<td rowspan="2"><label>AMOUNT REIMBURSEABLE 應付金額</label></td>
					<td>
						<input type="number" name="amountReimbursable" readOnly
						<c:if test="${empty cp }">value="0"</c:if>
						<c:if test="${not empty cp }">value="${cp.amountReimbursable }"</c:if>
						/>
					</td>
				</tr>
				<tr>
					<td><label>Prepaid 預支金額：</label></td>
					<td>
						<input type="number" name="amountPrepaid" onblur="doTotal()"
						<c:if test="${empty cp }">value="0"</c:if>
						<c:if test="${not empty cp }">value="${cp.amountPrepaid }"</c:if>
						/>
					</td>
				</tr>
			</table>
			
			<table class="table">
				<tr>
					<td><label>出納覆核</label></td>
					<td><label>部門主管</label></td>
					<td><label>申請人</label></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td>${empty cp.creatorName ? sessionScope.employee.name : cp.creatorName}</td>
				</tr>
			</table>			
		</div>
	</form>
	<input type="button" value="保存" id="save" />	
	<input type="button" value="送審" id="submit"/>
</div>

<script type="text/javascript">

function addColumn(costType){
	var costType1Count = 0;
	var costType2Count = 0;
	var count;
	
	if(costType == 1){
		count = costType1Count;
		costType1Count++;
	}else{
		count = costType2Count;
		costType2Count++;
	}
	
	var html = "";
	html += '<tr>';	
	html += '<td>';
	html += '<input type="hidden" name="costType" value="'+costType+'"/>';
	html += '<input type="hidden" name="receiptId" value=""/><input type="text" style="width: 100px;" value="" onfocus="WdatePicker({startDate:';
    html += "'%y-%M-%d'";
    html += ", dateFmt:";
    html += "'yyyy-MM-dd'";
    html += '})" class="Wdate" name="costDate" id="costDate'+costType+''+count+'"></td>';
    html += '<td><input style="width: 100px;" name="place"/></td>';
    html += '<td><input style="width: 100px;" name="number"/></td>';
    html += '<td><input style="width: 100px;" name="description"/></td>';
    html += '<td><input style="width: 120px;" type="number" name="amount" onblur="doTotal()"/></td>';
    html += '<td>';
    html += '<select name="currency" onchange="fixedRate(this)">';
    html += '<option value="">請選擇</option>';
    html += '<option value="TWD">新臺幣(TWD)</option>';
    html += '<option value="HKD">港幣(HKD)</option>';
    html += '<option value="USD">美金(USD)</option>';
    html += '<option value="CNY">人民幣(CNY)</option>';
    html += '<option value="KRW">韓元(KRW)</option>';
    html += '<option value="JPY">日圓(JPY)</option>';            
    html += '</select>';
    html += '</td>';
    html += '<td><input type="number" style="width: 55px;" name="rate" onblur="doTotal()"/></td>';
    html += '<td><div class="TWDollor'+costType+'"></div></td>';
    html += '<td><input class="btn delete" type="button" value="刪除"></td>';
    html += '</tr>';
   
    $('#costType'+costType).append(html);
}

function fixedRate(obj) {
	var cid = $(obj).val();
	
	if(cid == "TWD") {
		$(obj).closest('td').next().find('[name=rate]').val("1");
	}
	
	doTotal();
}

function doTotal() {
	// 雜項
	var total1 = 0;	
	$('#costType1').find('[name=amount]').each(function() {
		var cal = 0;
				
		var rate =  $(this).closest('td').next().next().find('[name=rate]').val();
		var currency = $(this).closest('td').next().find('[name=currency]').val();
		var amount = $(this).val();
		console.log("type1=" + rate + "," + currency + "," + amount);
		
		if(amount != "" && rate != "" && currency != "") {
			cal = (amount*rate).toFixed(currency == "TWD" || currency == "JPY" || currency == "KRW" ? 0 : 2);
			$(this).closest('td').next().next().next().find('.TWDollor1').html(cal);
			total1 += Math.round(cal);
			return;
		}					
	});
	$('#genericExpensesTotal').val(total1);
	$('#TWDollor1Total').html('NT$'+total1);
	// 交際費
	var total2 = 0;
	$('#costType2').find('[name=amount]').each(function() {
		var cal = 0;		
		
		var rate =  $(this).closest('td').next().next().find('[name=rate]').val();
		var currency = $(this).closest('td').next().find('[name=currency]').val();
		var amount = $(this).val();
		console.log("type2=" + rate + "," + currency + "," + amount);
		
		if(amount != "" && rate != "" && currency != "") {
			cal = (amount*rate).toFixed(currency == "TWD" || currency == "JPY" || currency == "KRW" ? 0 : 2);
			$(this).closest('td').next().next().next().find('.TWDollor2').html(cal);
			
			total2 += Math.round(cal);
			return;
		}		
	});
	$('#entertainmentExpensesTotal').val(total2);
	$('#TWDollor2Total').html('NT$'+total2);
	
	var consumption = $('input[name=businessCardConsumption]').val() || 0;
	var prepaid = $('input[name=amountPrepaid]').val() || 0;		
	var amountReimbursable = total1+total2-consumption-prepaid;
	
	$('input[name=amountReimbursable]').val(amountReimbursable);
}

function deleteReceipt(editId, id, type){
	$.get("/businessTripExpenseForm/deleteReceipt?id="+id+"&editId="+editId+"&costType="+type, function(e){
		$.get("/businessTripExpenseForm/edit?id="+id, function(e) {
			$("#myModalBody").html(e);
		});
	});
	doTotal();
}

function twBank() {
	$.ajax({
		url : '${ctx}/bank/twBank',
		dataType : 'json',
		data : {},
		success : function(data) {
			$('#twBank').empty();							
			$('#twBank').append('<option value="">請選擇銀行</option>');
			
			for(var i in data) {
				$('#twBank').append(
					'<option value="' + data[i].code+'">'
					+ data[i].code + (data[i].shortName == '' ? data[i].fullName : data[i].shortName)
					+ '</option>');
			}
		},
		error : function() {
			alert('網絡連接失敗，請聯繫管理員');
		}
	});
}

$(function(){
	twBank();
	
	$('#twBank').change(function() {
		var bankCode = $(this).val();
		
		if(bankCode == '') {
			$('#twBranch').empty();					
			$('#twBranch').append('<option value="">請選擇分行</option>');
		}
		else {
			$.ajax({
				url : '${ctx}/bank/twBranch',
				dataType : 'json',
				data : {
					'code' : bankCode
				},
				success : function(data) {
					$('#twBranch').empty();							
					$('#twBranch').append('<option value="">請選擇分行</option>');
					
					for(var i in data) {
						$('#twBranch').append(
							'<option value="' + data[i].fullName +'">'
							+ (data[i].shortName == '' ? data[i].fullName : data[i].shortName)
							+ '</option>');
					}
				},
				error : function() {
					alert('網絡連接失敗，請聯繫管理員');
				}
			});
		}
	});
	
	$('#twBranch').change(function() {
		var branch = $(this).val();
		$("#payeeBank").val(branch);
		
	});
	
	var total1 = 0;
	var total2 = 0;
	
	$('.TWDollor1').each(function(){
		if($(this).html() != ''){
			total1 += parseFloat($(this).html());
		}			
	});
	$('#genericExpensesTotal').val(total1);
	$('#TWDollor1Total').html("NT$"+total1);
	
	$('.TWDollor2').each(function(){
		if($(this).html() != ''){
			total2 += parseFloat($(this).html());
		}
	});
	$('#entertainmentExpensesTotal').val(total2);	
	$('#TWDollor2Total').html("NT$"+total2);
	
	$('#save').click(function() {		
		if(checkValidate()){
			$("#editForm").ajaxSubmit({
	            success: function(e) {
	            	if(e.code == "0000") {
	            		alert(e.message), $("#myModal").modal("toggle");
		                $('#search').click();
	            	}
	            	else {
	            		alert(e.message);
	            	}
	            },
	            error: function() {
	                alert("網路連接失敗，請聯繫管理員");
	            }
	        });
		}				
	});
	
	$('#submit').click(function() {
		if(checkValidate()){
			$('#editForm').attr('action', '${ctx}/businessTripExpenseForm/submit');
			$("#editForm").ajaxSubmit({
	            success: function(e) {
	                alert(e.message), $("#myModal").modal("toggle");
	                $('#search').click();
	            },
	            error: function() {
	                alert("網路連接失敗，請聯繫管理員");
	            }
	        });			
		}
	});
	
	$('#myModalBody').on('click', '.delete', function(){
		$(this).parent().parent().remove();
		doTotal();
	});
	
	$('#companyId').change(function() {
		var cid = $(this).val();
		var name = $(this).find("option:selected").text();
		$("#title").text(name);
		// department
		getUserInfo(cid);
	});
	
	// default title
	$("#title").text($('#companyId').find("option:selected").text());
	
	if($("#department").text().trim() == "") {
		getUserInfo($('#companyId').val());
	}
	
	doTotal();
});

function checkValidate(){
	var pass = true;
	var html = "<font class='warning' color='red'>請輸入此欄位</font>";
	
	if($('input[name=purpose]').val() == ''){
		pass = false;
		$('input[name=purpose]').parent().find('.warning').remove();
		$('input[name=purpose]').parent().append(html);		
	}else{
		$('input[name=purpose]').parent().find('.warning').remove();
	}
	
	if($('input[name=payee]').val() == ''){
		pass = false;
		$('input[name=payee]').parent().find('.warning').remove();
		$('input[name=payee]').parent().append(html);		
	}else{
		$('input[name=payee]').parent().find('.warning').remove();
	}
	
	if($('input[name=payeeBank]').val().trim() == ''){
		pass = false;
		$('input[name=payeeBank]').parent().find('.warning').remove();
		$('input[name=payeeBank]').parent().append(html);		
	}else{
		// HK validate
		var company = "${sessionScope.employee.companyName}";		
		var isHK = company.search("香港");
		var pattern = /(\w|\s)+/;
		
		if((isHK > 0) && !pattern.test($('input[name=payeeBank]').val())) {
			pass = false;
			$('input[name=payeeBank]').parent().find('.warning').remove();
			$('input[name=payeeBank]').parent().append("欄位內容英數限定");
		}
		else {
			$('input[name=payeeBank]').parent().find('.warning').remove();
		}		
	}
	
	if($('input[name=payeeAccount]').val() == ''){
		pass = false;
		$('input[name=payeeAccount]').parent().find('.warning').remove();
		$('input[name=payeeAccount]').parent().append(html);		
	}else{
		$('input[name=payeeAccount]').parent().find('.warning').remove();
	}
	
	if($('#durationStart').val() == ''){
		pass = false;
		$('#durationStart').parent().find('.warning').remove();
		$('#durationStart').parent().append(html);		
	}else{
		$('#durationStart').parent().find('.warning').remove();
	}
	
	if($('#durationEnd').val() == ''){
		pass = false;
		$('#durationEnd').parent().find('.warning').remove();
		$('#durationEnd').parent().append(html);		
	}else{
		$('#durationEnd').parent().find('.warning').remove();
	}
	
	if($('input[name=purpose]').val() == ''){
		pass = false;
		$('input[name=purpose]').parent().find('.warning').remove();
		$('input[name=purpose]').parent().append(html);		
	}else{
		$('input[name=purpose]').parent().find('.warning').remove();
	}
	
	// 明細
	var costDate = $("input[name='costDate']").map(function(){return $(this).val();}).get();
	// description 不檢核
	var place = $("input[name='place']").map(function(){return $(this).val();}).get();
	var number = $("input[name='number']").map(function(){return $(this).val();}).get();
	var amount = $("input[name='amount']").map(function(){return $(this).val();}).get();
	var currency = $("[name='currency']").map(function(){return $(this).val();}).get();
	var rate = $("input[name='rate']").map(function(){return $(this).val();}).get();
	var costType = $("input[name='costType']").map(function(){return $(this).val();}).get();
	var receiptCnt = 0;
	
	for(x in costDate) {
		if(costDate[x] == "" || place[x] == "" || number[x] == "" || amount[x] == "" || currency[x] == "" || rate[x] == "") {
			var costDesc = costType == "1" ? "雜項費用" : "交際費";
			alert(costDesc + "不完整");
			return false;
		}
		
		receiptCnt++;
	}
	
	if(receiptCnt == 0) {
		alert("至少輸入一項雜項費用或交際費");
		return false;
	}
		
	doTotal();
	
	if(!pass){
		alert("表單未填寫完整!");
	}
	
	return pass;	
}

function getUserInfo(cid) {	
	$.ajax({
		url : '${ctx}/employee/userInfo',
		dataType : 'json',
		data : {
			'id' : cid
		},
		success : function(data) {
			var dept = data.split(",");			
			$("#department").text(dept[1]);
			$("#requestDepartment").val(dept[0]);
		},
		error : function() {
			alert('網絡連接失敗，請聯繫管理員');
		}
	});
}

function getEmployeeInfo(eid) {
	$.ajax({
		url : '${ctx}/employee/employeeInfo',
		dataType : 'json',
		data : {
			'id' : eid
		},
		success : function(data) {
			var dept = data.split(",");			
			$("#department").text(dept[1]);
			$("#requestDepartment").val(dept[0]);
		},
		error : function() {
			alert('網絡連接失敗，請聯繫管理員');
		}
	});
}
</script>