<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
<head>
<title>8888play ERP</title>
</head>
<body>
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div class="modal-body" id="myModalBody"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="modalSubmit">關閉</button>
				</div>
			</div>
		</div>
	</div>
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				表單資料 <small>出差報支單</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="${ctx}/index"><i class="fa fa-dashboard"></i>8888play ERP</a></li>
				<li class="active">出差報支單</li>
			</ol>
		</section>
		<!-- Main content -->
		<div class="container-fluid">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body">
						<form action="${ctx}/businessTripExpenseForm/list" id="listForm" method="post">
							<input type="hidden" id="pageNo" name="pageNo" value="" />
							<input type="hidden" id="formId" name="formId" value="${formId}" />
							<div class="table-responsive">						
								<div class="form-group col-sm-12 col-md-4 col-lg-3">
	                            	<label for="changeCompany">公司:</label>
	                            	<select name="companyId" id="changeCompany">
	                            	<option value="">請選擇</option>
	                            	<c:forEach items="${companys }" var="cps">
	                            	<option value="${cps.id }">${cps.name }</option>
	                            	</c:forEach>
	                            	</select>	                                
	                            </div>
	                            <div class="form-group col-sm-12 col-md-4 col-lg-3">
	                            	<label for="requestDepartment">部門:</label>
	                            	<select name="requestDepartment" id="requestDepartment">
	                            	<option value="">請選擇</option>
	                            	</select>	                               
	                            </div>
	                            <div class="form-group col-sm-12 col-md-4 col-lg-3">
	                            	<label for="inputserialNo">表單編號:</label>
	                                <input class="form-control" id="inputserialNo" name="serialNo" type="text">
	                            </div>
	                            <div class="form-group col-sm-12 col-md-4 col-lg-3">
	                            	<label for="requestDateStart">申請起日:</label>
	                                <input type="text" value=""
										onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
										class="Wdate" name="requestDateStart" id="requestDateStart">
	                            </div>
	                            <div class="form-group col-sm-12 col-md-4 col-lg-3">
	                            	<label for="requestDateEnd">申請迄日:</label>
	                                <input type="text" value=""
										onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
										class="Wdate" name="requestDateEnd" id="requestDateEnd">
	                            </div>
	                            <div class="form-group col-sm-12 col-md-4 col-lg-3">
	                            	<label for="remitDateStart">出款起日:</label>
	                                <input type="text" value=""
										onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
										class="Wdate" name="remitDateStart" id="remitDateStart">
	                            </div>
	                            <div class="form-group col-sm-12 col-md-4 col-lg-3">
	                            	<label for="remitDateEnd">出款迄日:</label>
	                                <input type="text" value=""
										onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
										class="Wdate" name="remitDateEnd" id="remitDateEnd">
	                            </div>
	                            <div class="form-group col-sm-12 col-md-4 col-lg-3">
	                            	<label for="status">流程狀態:</label>
	                                <select name="status" id="status">
		                                <option value="">全部</option>
		                                <option value="0">正常結束</option>
		                                <option value="1">執行中</option>
		                                <option value="8">註銷</option>
		                                <option value="9">退件</option>
		                                <option value="-1">草稿</option>
	                                </select>
	                            </div>
	                            <div class="form-group col-sm-12 col-md-4 col-lg-3">
	                            	<label for="remitted">出款狀態:</label>
	                                <select name="remitted" id="remitted">
		                                <option value="">全部</option>
		                                <option value="1">未出款</option>
		                                <option value="2">已出款</option>		                                
	                                </select>
	                            </div>                           
	                            <hr>
								<div class="btn-group col-sm-12 col-md-12 col-lg-6" role="group">
		                        	<input class="btn btn-primary" type="button" id="search" value="搜尋"/>
		                            <input class="btn btn-success" type="button" id="add" value="新增"/>
		                            <input class="btn btn-danger" type="button" id="export" value="導出列表"/>
		                        </div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div id="listContainer"></div>
		</div>
	</aside>

	<script type="text/javascript" src="${ctx }/js/jquery.form.js"></script>
	<script type="text/javascript">
		$(function() {
			
			$('#changeCompany').change(function(){
				var cid = $(this).val();
				
				if(cid == ''){
					$('#requestDepartment').empty();					
					$('#requestDepartment').append('<option value="">請選擇</option>');
				}else{
					$.ajax({
						url : '${ctx}/department/deptList',
						dataType : 'json',
						data : {
							'id' : cid
						},
						success : function(data) {
							$('#requestDepartment').empty();							
							$('#requestDepartment').append('<option value="">請選擇</option>');
							
							for(var i in data) {
								$('#requestDepartment').append(
									'<option value="' + data[i].id+'">'
									+ data[i].name
									+ '</option>');
							}
						},
						error : function() {
							alert('網絡連接失敗，請聯繫管理員');
						}
					});
				}
			});
			
			$("#add").click(
				function() {
					$(".modal-dialog").css("width", "1000px"),
					$(".modal-dialog").css("maxheight",	"800px"),
					$("#myModal").modal("toggle"),
					$("#myModalLabel").html("新增"),
					$("#myModalBody").html("<h3>正在打開。。。</h3>"),
					$.get("/businessTripExpenseForm/edit?id=", function(e) {
						$("#myModalBody").html(e);
					})
				});

			$('#search').click(function() {
				var flag = dayCheck();
				
				if(!flag) {
					return;
				}
				
				$('#pageNo').val("1");
				$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
				$('#listForm').ajaxSubmit({
					success : function(data) {
						$('#listContainer').html(data);
					}
				});
			});
			
			$('#export').click(function() {
				if(!dayCheck()) {
					return;
				}
				
				var temp = $('#listForm').attr('action');
				$('#listForm').attr('action', '${ctx}/businessTripExpenseForm/export');
				$('#listForm').submit();
				$('#listForm').attr('action', temp);
			});
			
			$('#modalSubmit').click(function() {
				$('#myModal').modal('toggle');
				$('#search').click();
			});
			
			// Draft
			var formId = $("#formId").val();
			
			if(formId > 0) {
				edit(formId);
			}
		});		
		
		function toPage(pageNo) {
			if(!dayCheck()) {
				return;
			}
			
			if (!pageNo || pageNo <= 0) {
				return;
			}
			
			$('#pageNo').val(pageNo);
			$('#listForm').ajaxSubmit({
				success : function(data) {
					$('#listContainer').html(data);
				},
				error : function() {
					alert('連接網路失敗');
				}
			});
			$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
		}
		
		function dayCheck() {
			var beginTime = $("#establishDateStart").val();
			
			if(!beginTime) {
				var beginTimeDate = new Date(beginTime);
				var endTime = $("#establishDateEnd").val();
				var endTimeDate = new Date();
				
				if(endTime) {
					endTimeDate = new Date(endTime);
				}
				
				if(beginTimeDate > endTimeDate) {
					alert("結束時間不能大於開始時間!");
					return false;
				}
			}
			return true;
		}

		
		
		function edit(id) {
			$(".modal-dialog").css("width", "1000px"), $(".modal-dialog").css(
				"maxheight", "800px"), $("#myModal").modal("toggle"), 
				$("#myModalLabel").html("編輯"), $("#myModalBody").html(
				"<h3>正在打開。。。</h3>"), $.get("/businessTripExpenseForm/edit?id=" + id, function(e) {
					$("#myModalBody").html(e);
				})
		}
		
		function get(id) {
			$(".modal-dialog").css("width", "1000px"), $(".modal-dialog").css(
				"maxheight", "800px"), $("#myModal").modal("toggle"), 
				$("#myModalLabel").html("檢視"), $("#myModalBody").html(
				"<h3>正在打開。。。</h3>"), $.get("/businessTripExpenseForm/get?id=" + id, function(e) {
					$("#myModalBody").html(e);
				})
		}

		function deleteBTE(id) {
			if(!confirm("確定刪除?")) {
				return;
			}
			
			$.ajax({
				url : '${ctx}/businessTripExpenseForm/delete',
				dataType : 'json',
				data : {
					'id' : id
				},
				success : function(data) {
					$("#myModal").modal("toggle");
					$('#search').click();
				},
				error : function() {
					alert('網路連接失敗，請聯繫管理員');
				}
			});
		}
		
		function terminate(id) {
			if(!confirm("確定撤銷?")) {
				return;
			}
			
			$.ajax({
				url : '${ctx}/flow/terminate',
				dataType : 'json',
				data : {
					'id1' : id
				},
				success : function(data) {
					alert("撤銷成功");
					$('#search').click();
				},
				error : function() {
					alert('網路連接失敗，請聯繫管理員');
				}
			});
		}
		
		function pdf(id) {
			console.log(id);
			$("#formId").val(id);
			var temp = $('#listForm').attr('action');
			$('#listForm').attr('action', '${ctx}/businessTripExpenseForm/pdf');
			$('#listForm').submit();
			
			$("#formId").val("");
			$('#listForm').attr('action', temp);
		}
		
		// 審核歷程
		function history(id) {
			$(".modal-dialog").css("width", "900px"), 
			$(".modal-dialog").css("maxheight", "800px"), 
			$("#myModal").modal("toggle"), 
			$("#myModalLabel").html("審核歷程"), 
			$("#myModalBody").html("<h3>正在打開。。。</h3>"), 
			$.get("/flow/list?id=" + id, 
				function(e) {
					$("#myModalBody").html(e);
				})
		}
		
		// 出款
		function confirmRemitDate(id) {
			if(!confirm("確定出款?")) {
				return;
			}
			
			$.ajax({
				url : '${ctx}/remit/confirmRemitDate',
				dataType : 'json',
				data : {
					'formType' : 'PT',
					'id' : id
				},
				success : function(data) {
					if(data.code == "0000") {
						alert("出款成功");
						$('#search').click();
					}
					else {
						alert("出款失敗");
					}
				},
				error : function() {
					alert('網路連接失敗，請聯繫管理員');
				}
			});
		}
		
		// 出帳日變更
		function updateRemitDate(id) {
			$(".modal-dialog").css("width", "900px"), 
			$(".modal-dialog").css("maxheight", "800px"), 
			$("#myModal").modal("toggle"), 
			$("#myModalLabel").html("檢視"), 
			$("#myModalBody").html("<h3>正在打開。。。</h3>"), 
			$.get("/remit/openRemitDate?formType=" + "PT" + "&id=" + id, 
				function(e) {
					$("#myModalBody").html(e);
				})
		}
	</script>
</body>
</html>