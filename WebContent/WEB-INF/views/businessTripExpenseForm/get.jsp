<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<div class="table-responsive">
		
		<div style="text-align: center;"><h2>${form.companyName}</h2></div>
		<div style="text-align: center;"><h3>出差旅費報支單 Travel Expense Report</h3></div>
		
		<div style="width:100%">
			<div style="float:left; text-align:left;width:50%">
				<label>編號&nbsp;</label>${cp.serialNo}
			</div>
			<div style="float:left; text-align:right;width:50%">
				<label>申請日期&nbsp;</label><fmt:formatDate value="${cp.requestDate }" type="date" pattern="yyyy-MM-dd" />			
			</div>
		</div>
		<div>&nbsp;</div>
		<table class="table">
			<tr>
				<td width="20%"><label>Dept. 部門</label></td>
				<td width="20%">${cp.departmentName }</td>
				<td width="20%"><label>Purpose of Trip</label></td>	
				<td width="40%">${cp.purpose }</td>
			</tr>
			<tr>
				<td><label>出差員工</label></td>
				<td>${cp.employeeName}</td>
				<td><label>Duration</label></td>
				<td>
					<fmt:formatDate value="${cp.durationStart }" type="date" pattern="yyyy-MM-dd" />
					~
					<fmt:formatDate value="${cp.durationEnd }" type="date" pattern="yyyy-MM-dd" />
				</td>
			</tr>
			<tr>
				<td><label>匯款資訊</label></td>
				<td colspan="3">						
					<table class="table">
						<tr>
							<td><label>受款人:</label></td>
							<td>${cp.payee }</td>
							<td><label>銀行別:</label></td>
							<td>${cp.payeeBank }</td>
							<td><label>銀行帳號:</label></td>
							<td>${cp.payeeAccount }</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td><label>出款日</label></td>
				<td colspan="3"><fmt:formatDate value="${cp.remitDate}" type="date" pattern="yyyy-MM-dd" /></td>
			</tr>
			<tr>
				<td colspan="4">
					<table class="table">
						<tr>
							<td>
								<div><label>Date</label></div>
								<div><label>日期</label></div>
							</td>
							<td>
								<div><label>Place</label></div>
								<div><label>地點</label></div>
							</td>
							<td>
								<div><label>Number</label></div>
								<div><label>單據編號</label></div>
							</td>
							<td>
								<div><label>Description</label></div>
								<div><label>雜項費用說明</label></div>
							</td>
							<td>
								<div><label>Amount</label></div>
								<div><label>金額</label></div>
							</td>
							<td>
								<div><label>Currency</label></div>
								<div><label>幣別</label></div>
							</td>
							<td>
								<div><label>Exchange Rate</label></div>
								<div><label>匯率</label></div>
							</td>
							<td>
								<div><label>Total</label></div>
								<div><label>總計</label></div>
							</td>								
							<td></td>
						</tr>						
						<c:forEach items="${receiptCostType1 }" var="rt1">
						<tr>								
							<td>
								<fmt:formatDate value="${rt1.costDate }" type="date" pattern="yyyy-MM-dd" />
							</td>								
							<td>${rt1.place }</td>
							<td>${rt1.number }</td>
							<td>${rt1.description }</td>
							<td>
								<fmt:parseNumber value="${rt1.amount}" var="amount"/>
								<fmt:formatNumber value="${amount}" pattern="###,###.##"/>								
							</td>
							<td>${rt1.currency }</td>
							<td><fmt:parseNumber integerOnly="${rt1.rate % 1 == 0}" type="number" value="${rt1.rate}" /></td>
							<td><div class="TWDollor1"><fmt:formatNumber pattern="###,###.##" type="number" value="${rt1.amount*rt1.rate}" /></div></td>
						</tr>
						</c:forEach>											
						<tr>
							<td colspan="6"><label>Total 總計</label></td>								
							<td colspan="2">
								<input type="hidden" name="genericExpensesTotal" id="genericExpensesTotal">
								<div id="TWDollor1Total"><fmt:formatNumber value="${cp.genericExpensesTotal }" type="currency" maxFractionDigits="0"/></div>
							</td>
						</tr>
					</table>						
				</td>
			</tr>									
		</table>
		
		<div><label>Entertainment Expenses   交際費</label></div>
		<table class="table">
			<tr>
				<td>
					<div><label>Date</label></div>
					<div><label>日期</label></div>
				</td>
				<td>
					<div><label>Place</label></div>
					<div><label>地點</label></div>
				</td>
				<td>
					<div><label>Number</label></div>
					<div><label>單據編號</label></div>
				</td>
				<td>
					<div><label>Person/Company Entertained</label></div>
					<div><label>對象</label></div>
				</td>
				<td>
					<div><label>Amount</label></div>
					<div><label>金額</label></div>
				</td>
				<td>
					<div><label>Currency</label></div>
					<div><label>幣別</label></div>
				</td>
				<td>
					<div><label>Exchange Rate</label></div>
					<div><label>匯率</label></div>
				</td>
				<td>
					<div><label>Total</label></div>
					<div><label>總計</label></div>
				</td>
				<td></td>
			</tr>				
			<c:forEach items="${receiptCostType2 }" var="rt2">
			<tr>
				<td>						
					<fmt:formatDate value="${rt2.costDate }" type="date" pattern="yyyy-MM-dd" />
				</td>
				<td>${rt2.place }</td>
				<td>${rt2.number }</td>
				<td>${rt2.description }</td>
				<td>
					<fmt:parseNumber value="${rt2.amount}" var="amount"/>
					<fmt:formatNumber value="${amount}" pattern="###,###.##"/>					
				</td>
				<td>${rt2.currency }</td>
				<td><fmt:parseNumber integerOnly="${rt2.rate % 1 == 0}" type="number" value="${rt2.rate}" /></td>
				<td><div class="TWDollor2"><fmt:formatNumber pattern="###,###.##" type="number" value="${rt2.amount*rt2.rate}" /></div></td>
			</tr>
			</c:forEach>						
			<tr>
				<td colspan="6"><label>Total 總計</label></td>					
				<td colspan="2">
					<input type="hidden" name="entertainmentExpensesTotal" id="entertainmentExpensesTotal">
					<div id="TWDollor2Total">NT$ <fmt:formatNumber value="${cp.entertainmentExpensesTotal}" pattern="###,###.##"/></div>
				</td>
			</tr>
		</table>
		
		<table class="table">
			<tr>
				<td><label>Consumption 公司卡消費金額：</label></td>
				<td>${cp.businessCardConsumption }</td>
				<td rowspan="2"><label>AMOUNT REIMBURSEABLE 應付金額</label></td>
				<td<fmt:formatNumber value="${cp.amountReimbursable}" pattern="###,###.##"/></td>
			</tr>
			<tr>
				<td><label>Prepaid 預支金額：</label></td>
				<td><fmt:formatNumber value="${cp.amountPrepaid}" pattern="###,###.##"/></td>
			</tr>
		</table>
		
		<table class="table">
			<tr>
				<td colspan="4">
					<label>出納覆核備註</label>			
					${cp.remark}				
				</td>
			</tr>
			<tr>
				<td><label>出納覆核</label></td>
				<c:if test="${not empty stamp['總經理']}">
				<td><label>總經理</label></td>
				</c:if>
				<td><label>部門主管</label></td>
				<td><label>申請人</label></td>
			</tr>
			<tr>
				<td>${stamp['出納'].info}</td>
				<c:if test="${not empty stamp['總經理']}">
				<td>${stamp['總經理'].info}</td>
				</c:if>
				<td>${stamp['部門主管'].info}${stamp['副總經理'].info}</td>				
				<td>${stamp['申請人'].info}</td>
			</tr>
		</table>		
	</div>	
</div>