<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->	
	<input type="hidden" name="id1" id="id1" value="${param.id1}" />	
	<input type="hidden" name="id2" id="id2" value="${param.id2}" />
	<div class="table-responsive">
		
		<div style="text-align: center;"><h2>${form.companyName}</h2></div>
		<div style="text-align: center;"><h3>出差申請單</h3></div>
		
		<div style="width:100%">
			<div style="float:left; text-align:left;width:50%">
				<label>
					<c:if test="${form.oversea == '1' }">國內</c:if>
					<c:if test="${form.oversea == '2' }">國外</c:if>
				</label>
			</div>
			<div style="float:left; text-align:right;width:50%">
				<label>編號&nbsp;</label>${form.serialNo}			
			</div>
		</div>
			
		<table class="table">
			<tr>
				<td width="13%" class="td1"><label>部門</label></td>
				<td width="27%">${form.departmentName }</td>
				<td width="20%" class="td1"><label>申請日期</label></td>
				<td width="40%" colspan="2"><fmt:formatDate value="${form.requestDate}" type="date" pattern="yyyy-MM-dd" /></td>
			</tr>
			<tr>
				<td class="td1"><label>職稱</label></td>
				<td>${form.jobTitle }</td>
				<td class="td1"><label>姓名</label></td>
				<td colspan="2">${form.employeeName }</td>
			</tr>
			<tr>
				<td class="td1"><label>出差事由</label></td>
				<td colspan="2">${form.tripMatter }</td>
				<td class="td1"><label>出差地點</label></td>
				<td width="20%">${form.tripLocation }</td>
			</tr>
			<tr>
				<td class="td1"><label>出差日期</label></td>
				<td colspan="4">
					<div>※請事先向採購/總務部提出需求，先進行機票及住宿安排詢價作業，以利權責主管核准參考。</div>
					<table class="table">
						<tr>
							<td><label>去程</label></td>
							<td>
								<div>
									<label>飛機班機(起)日期:</label>
									<fmt:formatDate value="${form.outboundDepartureTime}" type="date" pattern="yyyy-MM-dd" />
									<label>航班:</label>${form.outboundFlight }
									<label>航班班次號碼:</label>${form.outboundFlightNo }
								</div>
								<div>
									<label>到達國家:</label>${form.outboundCountry }
									<label>到達城市:</label>${form.outboundCity }
									<label>時間:</label><fmt:formatDate value="${form.outboundLandingTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss" />
								</div>
							</td>								
						</tr>
						<tr>
							<td><label>回程</label></td>
							<td>
								<div>
									<label>飛機班機(起)日期:</label>
									<fmt:formatDate value="${form.inboundDepartureTime}" type="date" pattern="yyyy-MM-dd" />
									<label>航班:</label>${form.inboundFlight }
									<label>航班班次號碼:</label>${form.inboundFlightNo }
								</div>
								<div>
									<label>到達國家:</label>${form.inboundCountry }
									<label>到達城市:</label>${form.inboundCity }
									<label>時間:</label><fmt:formatDate value="${form.inboundLandingTime }" type="date" pattern="yyyy-MM-dd HH:mm:ss" />
								</div>
							</td>
						</tr>
					</table>						
					<div>
						備註:請附上旅行社預訂機票旅客行程單。
						<c:forEach items="${attachment}" var="a" varStatus="loop">
						<a href="${a.url}" target="_blank"><label>附件_${loop.count}</label></a>								
						</c:forEach>
					</div>
				</td>
			</tr>				
			<tr>
				<td class="td1"><label>辦理文件</label></td>
				<td>
					護照:
					<input type="radio" name="agentPassport" value="1" <c:if test="${form.agentPassport == '1' }">checked="checked"</c:if>/>是
					<input type="radio" name="agentPassport" value="2" <c:if test="${form.agentPassport == '2' }">checked="checked"</c:if>/>否
				</td>
				<td>
					簽證:
					<input type="radio" name="agentVisa" value="1" <c:if test="${form.agentVisa == '1' }">checked="checked"</c:if>/>是
					<input type="radio" name="agentVisa" value="2" <c:if test="${form.agentVisa == '2' }">checked="checked"</c:if>/>否
				</td>
				<td>
					台胞證:
					<input type="radio" name="agentMtp" value="1" <c:if test="${form.agentMtp == '1' }">checked="checked"</c:if>/>是
					<input type="radio" name="agentMtp" value="2" <c:if test="${form.agentMtp == '2' }">checked="checked"</c:if>/>否
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="td1"><label>出差費用預估</label></td>
				<td>
					<div>
						<label>機票費用:</label>${form.estimatedCostAirfare }							
					</div>
					<div>
						<label>辦理文件費:</label>${form.estimatedCostAgent }
					</div>												
					<div>
						<label>住宿費用:</label>${form.estimatedCostLodging }
					</div>
					<div>
						<label>(台灣)機場接送安排費用:</label>${form.estimatedCostPickup }
					</div>
				</td>
				<td>
					<label>出差費用預估合計</label>
					${form.estimatedCostAirfare + form.estimatedCostAgent + form.estimatedCostLodging + form.estimatedCostPickup}
				</td>
				<td>
					<span style="opacity: 0.3;filter: alpha(opacity = 30);"><label>出差安排承辦人簽名</label></span>
				</td>
				<td>
					<label>總務</label><br>${stamp['總務'].info}						
				</td>					
			</tr>				
			<tr>
				<td class="td1"><label>預支費用</label></td>
				<td colspan="4">						
					<input type="radio" name="cashAdvance" value="1" <c:if test="${form.cashAdvance == '1' }">checked="checked"</c:if>/>是，幣別：
					${form.advanceCurrency }
					${form.advanceAmount }
					元(請於三個工作天前提出申請)
					<input type="radio" name="cashAdvance" value="2" <c:if test="${form.cashAdvance == '2' }">checked="checked"</c:if>/>否
				</td>
			</tr>
			<tr>
				<td colspan="5"><label>國內/外 出差日支費補助</label></td>
			</tr>
			<tr>
				<td class="td1"><label>出差日支費</label> </td>						
				<td colspan="4">
					<div>
						<input type="radio" name="dailyAllowance" value="1" <c:if test="${form.dailyAllowance == '1' }">checked="checked"</c:if>/>國內:因公出差跨日，額外給予600元(台幣)/天出差日支費補助。
					</div>
					<div>
						<input type="radio" name="dailyAllowance" value="2" <c:if test="${form.dailyAllowance == '2' }">checked="checked"</c:if>/>國外:因公出差，額外給予1,000元(台幣)/天出差日支費補助。
					</div>
				</td>
			</tr>				
			<tr>
				<td class="td1"><label>備註:</label></td>
				<td colspan="4">${form.memo }</td>
			</tr>			
		</table>
		<table class="table">
			<tr>
				<td colspan="2" class="td2" style="text-align: center;"><label>協辦單位</label></td>					
				<td colspan="3" class="td2" style="text-align: center;"><label>出差單位</label></td>
			</tr>
			<tr>
				<td><label>會計</label></td>					
				<td><label>人資</label></td>
				<c:if test="${not empty stamp['總經理']}">
				<td><label>總經理</label></td>
				</c:if>					
				<td><label>部門主管</label></td>					
				<td><label>申請人</label></td>					
			</tr>
			<tr>					
				<td>${stamp['會計'].info}</td>
				<td>${stamp['人資'].info}</td>
				<c:if test="${not empty stamp['總經理']}">
				<td>${stamp['總經理'].info}</td>
				</c:if>
				<td>${stamp['部門主管'].info}${stamp['副總經理'].info}</td>					
				<td>${stamp['申請人'].info}</td>
			</tr>
		</table>
		<!-- 退件原因 -->
		<table class="table">
			<tr>
				<td>
					<label>退件原因：</label><input id="memo" name="memo" size="30"/>
					<input type="button" value="退件" id="reject"/>
				</td>
			</tr>
		</table>
		
		<!-- flow button -->
		<input type="button" value="取消" id="abort" />
		
		<c:if test="${isVerify}">
			<input type="button" value="審核" id="verify"/>
		</c:if>
		
		<c:if test="${isCountersign}">
		<input type="button" value="會簽" id="countersign"/>
		</c:if>
		
		<c:if test="${isPass}">
			<input type="button" value="放行" id="pass"/>
		</c:if>			
	</div>	
</div>

<script type="text/javascript">
$(function(){
	$('#abort').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
			
		$.ajax({
			url : '${ctx}/businessTripForm/abort',
			dataType : 'json',
			data : {
				'id' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#verify').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		
		$.ajax({
			url : '${ctx}/businessTripForm/verify',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#countersign').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		
		$.ajax({
			url : '${ctx}/businessTripForm/countersign',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#pass').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		
		$.ajax({
			url : '${ctx}/businessTripForm/pass',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#reject').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		var memo = $("#memo").val();
		
		$.ajax({
			url : '${ctx}/businessTripForm/reject',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2,
				'memo' : memo
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});
	
	$('input[type=radio]').each(function(){
		$(this).attr("disabled", true);		
	});
	
});
</script>