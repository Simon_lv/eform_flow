<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/businessTripForm/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${cp.id}" />
		<div class="table-responsive">
			
			<div style="text-align: center;"><h2>${form.companyName}</h2></div>
			<div style="text-align: center;"><h3>出差申請單</h3></div>
			
			<div style="width:100%">
				<div style="float:left; text-align:left;width:50%">
					<label>
						<c:if test="${cp.oversea == '1' }">國內</c:if>
						<c:if test="${cp.oversea == '2' }">國外</c:if>
					</label>
				</div>
				<div style="float:left; text-align:right;width:50%">
					<label>編號&nbsp;</label>${cp.serialNo}			
				</div>
			</div>
			<div>&nbsp;</div>
			<table class="table">
				<tr>
					<td width="13%" class="td1"><label>部門</label></td>
					<td width="27%">${cp.departmentName }</td>
					<td width="20%" class="td1"><label>申請日期</label></td>
					<td width="40%" colspan="2"><fmt:formatDate value="${cp.requestDate}" type="date" pattern="yyyy-MM-dd" /></td>
				</tr>
				<tr>
					<td class="td1"><label>職稱</label></td>
					<td>${cp.jobTitle }</td>
					<td class="td1"><label>姓名</label></td>
					<td colspan="2">${cp.employeeName }</td>
				</tr>
				<tr>
					<td class="td1"><label>出差事由</label></td>
					<td colspan="2">${cp.tripMatter }</td>
					<td class="td1"><label>出差地點</label></td>
					<td width="20%">${cp.tripLocation }</td>
				</tr>
				<tr>
					<td class="td1"><label>出差日期</label></td>
					<td colspan="4">
						<div>※請事先向採購/總務部提出需求，先進行機票及住宿安排詢價作業，以利權責主管核准參考。</div>
						<table class="table">
							<tr>
								<td><label>去程</label></td>
								<td>
									<div>
										<label>飛機班機(起)日期:</label>
										<fmt:formatDate value="${cp.outboundDepartureTime}" type="date" pattern="yyyy-MM-dd" />
										<label>航班:</label>${cp.outboundFlight }
										<label>航班班次號碼:</label>${cp.outboundFlightNo }
									</div>
									<div>
										<label>到達國家:</label>${cp.outboundCountry }
										<label>到達城市:</label>${cp.outboundCity }
										<label>時間:</label><fmt:formatDate value="${cp.outboundLandingTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss" />
									</div>
								</td>								
							</tr>
							<tr>
								<td><label>回程</label></td>
								<td>
									<div>
										<label>飛機班機(起)日期:</label>
										<fmt:formatDate value="${cp.inboundDepartureTime}" type="date" pattern="yyyy-MM-dd" />
										<label>航班:</label>${cp.inboundFlight }
										<label>航班班次號碼:</label>${cp.inboundFlightNo }
									</div>
									<div>
										<label>到達國家:</label>${cp.inboundCountry }
										<label>到達城市:</label>${cp.inboundCity }
										<label>時間:</label><fmt:formatDate value="${cp.inboundLandingTime }" type="date" pattern="yyyy-MM-dd HH:mm:ss" />
									</div>
								</td>
							</tr>
						</table>						
						<div>
							備註:請附上旅行社預訂機票旅客行程單。
							<c:if test="${empty param.action}">																	
							<c:forEach items="${attachment}" var="a" varStatus="loop">
							<a href="${a.url}" target="_blank"><label>附件_${loop.count}</label></a>								
							</c:forEach>																		
							</c:if>
						</div>
					</td>
				</tr>				
				<tr>
					<td class="td1"><label>辦理文件</label></td>
					<td>
						護照:
						<c:if test="${cp.agentPassport == '1' }">是</c:if>
						<c:if test="${cp.agentPassport == '2' }">否</c:if>
					</td>
					<td>
						簽證:
						<c:if test="${cp.agentVisa == '1' }">是</c:if>
						<c:if test="${cp.agentVisa == '2' }">否</c:if>
					</td>
					<td>					
						台胞證:
						<c:if test="${cp.agentMtp == '1' }">是</c:if>
						<c:if test="${cp.agentMtp == '2' }">否</c:if>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td class="td1"><label>出差費用預估</label></td>
					<td>
						<div>
							<label>機票費用:</label>${cp.estimatedCostAirfare }														
						</div>
						<div>
							<label>辦理文件費:</label>${cp.estimatedCostAgent }
						</div>												
						<div>
							<label>住宿費用:</label>${cp.estimatedCostLodging }
						</div>
						<div>
							<label>(台灣)機場接送安排費用:</label>${cp.estimatedCostPickup }
						</div>
					</td>
					<td>
						<label>出差費用預估合計</label> 
						${cp.estimatedCostAirfare + cp.estimatedCostAgent + cp.estimatedCostLodging + cp.estimatedCostPickup}
					</td>
					<td>
						<span style="opacity: 0.3;filter: alpha(opacity = 30);">出差安排承辦人簽名</span>
					</td>
					<td>
						<label>總務</label><br>${stamp['總務'].info}						
					</td>
				</tr>				
				<tr>
					<td class="td1"><label>預支費用</label></td>
					<td colspan="4">						
						<c:if test="${cp.cashAdvance == '1' }">
						是，幣別：${cp.advanceCurrency }
						${cp.advanceAmount }
						元(請於三個工作天前提出申請)
						</c:if>
						<c:if test="${cp.cashAdvance == '2' }">否</c:if>
					</td>
				</tr>
				<tr>
					<td colspan="5"><label>國內/外 出差日支費補助</label></td>
				</tr>
				<tr>
					<td class="td1"><label>出差日支費 </label></td>						
					<td colspan="4">
						<div>
							<c:if test="${cp.dailyAllowance == '1' }">國內:因公出差跨日，額外給予600元(台幣)/天出差日支費補助。</c:if>						
							<c:if test="${cp.dailyAllowance == '2' }">國外:因公出差，額外給予1,000元(台幣)/天出差日支費補助。</c:if>
						</div>
					</td>
				</tr>				
				<tr>
					<td class="td1"><label>備註:</label></td>
					<td colspan="4">${cp.memo }</td>
				</tr>				
			</table>
			<table class="table">
				<tr>
					<td colspan="2" class="td2" style="text-align: center;"><label>協辦單位</label></td>					
					<td colspan="3" class="td2" style="text-align: center;"><label>出差單位</label></td>
				</tr>
				<tr>
					<td><label>會計</label></td>					
					<td><label>人資</label></td>
					<c:if test="${not empty stamp['總經理']}">
					<td><label>總經理</label></td>
					</c:if>					
					<td><label>部門主管</label></td>					
					<td><label>申請人</label></td>					
				</tr>
				<tr>					
					<td>${stamp['會計'].info}</td>
					<td>${stamp['人資'].info}</td>
					<c:if test="${not empty stamp['總經理']}">
					<td>${stamp['總經理'].info}</td>
					</c:if>
					<td>${stamp['部門主管'].info}${stamp['副總經理'].info}</td>					
					<td>${stamp['申請人'].info}</td>
				</tr>
			</table>
			<c:if test="${not empty param.action && fn:length(attachment) > 0}">
			<div style="page-break-before: always; ">&nbsp;</div>										
				<c:forEach items="${attachment}" var="a" varStatus="loop">
				<label>附件_${loop.count}</label><br/>
				<img alt="" src="${a.url}"/><br/>
				</c:forEach>														
			</c:if>
		</div>
	</form>
</div>