<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/businessTripForm/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${cp.id}" />
		<input type="hidden" name="workFlowId" value="${cp.workFlowId}" />
		<!-- 退件重送 -->
		<input type="hidden" name="serialNo" value="${cp.serialNo}" />
		<div class="table-responsive">
			
			<div style="text-align: center;"><h2 id="title"></h2></div>
			<div style="text-align: center;"><h3>出差申請單</h3></div>
			
			<div>
				<input type="radio" name="oversea" value="1" <c:if test="${cp.oversea == '1' }">checked="checked"</c:if>>國內
				<input type="radio" name="oversea" value="2" <c:if test="${cp.oversea == '2' }">checked="checked"</c:if>>國外
			</div>
			
			<table class="table">
				<tr>
					<td width="13%" class="td1"><label>部門</label></td>
					<td colspan="2" width="27%">
						<select id="companyId" name="companyId">
                           	<c:forEach items="${sessionScope.company}" var="company">
                           	<option value="${company.id}"
                           		<c:if test="${cp.companyId == company.id}">selected</c:if>
                           		>${company.name}</option>
                           	</c:forEach>
                        </select>
						<span id="department">
						<c:if test="${not empty cp}">${cp.departmentName }</c:if>
						</span>
						<input type="hidden" id="requestDepartment" name="requestDepartment" value="${cp.requestDepartment}" />
					</td>
					<td width="20%" class="td1"><label>申請日期</label></td>
					<td><fmt:formatDate value="${cp.requestDate}" type="date" pattern="yyyy-MM-dd" /></td>
				</tr>
				<tr>
					<td class="td1"><label>職稱</label></td>
					<td>
						<span id="jobTitle">
						<c:if test="${not empty cp}">${cp.jobTitle }</c:if>
						</span>
						<input type="hidden" id="jobTitleId" name="jobTitleId" value="${cp.jobTitleId}" />
					</td>
					<td class="td1"><label>姓名</label></td>
					<td colspan="2">
						<select name="employeeId" onchange="getEmployeeInfo(this.value)">							
			            <c:forEach items="${companyEmployee}" var="employee">
			                <option value="${employee.id}" <c:if test="${employee.id == (empty cp ? sessionScope.employee.id : cp.employeeId)}">selected="selected"</c:if>>${employee.name}</option>
			            </c:forEach>
			            </select>						
					</td>
				</tr>
				<tr>
					<td class="td1"><label>出差事由</label></td>
					<td colspan="2"><input name="tripMatter" id="tripMatter" value="${cp.tripMatter }"/></td>
					<td class="td1"><label>出差地點</label></td>
					<td width="20%"><input name="tripLocation" id="tripLocation" value="${cp.tripLocation }"/></td>
				</tr>
				<tr>
					<td class="td1"><label>出差日期</label></td>
					<td colspan="4">
						<div>※請事先向採購/總務部提出需求，先進行機票及住宿安排詢價作業，以利權責主管核准參考。</div>
						<table class="table">
							<tr>
								<td><label>去程</label></td>								
								<td>
									<div>
										<label>飛機班機(起)日期:</label>
										<input type="text" value="<fmt:formatDate value="${cp.outboundDepartureTime}" type="date" pattern="yyyy-MM-dd" />"
													onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
													class="Wdate" name="outboundDepartureTime" id="outboundDepartureTime"/>
										<label>航班:</label><input name="outboundFlight" style="width: 100px;" value="${cp.outboundFlight }"/>
										<label>航班班次號碼:</label><input name="outboundFlightNo" style="width: 100px;" value="${cp.outboundFlightNo }"/>
									</div>
									<div>
										<label>到達國家:</label>
											<input name="outboundCountry" id="outboundCountry" value="${cp.outboundCountry }"/>
										<label>到達城市:</label>
											<input name="outboundCity" id="outboundCity" value="${cp.outboundCity }"/>
										<label>時間:</label><input type="text" value="<fmt:formatDate value="${cp.outboundLandingTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss" />"
												onfocus="WdatePicker({startDate:'%y-%M-%d 23:59:59', dateFmt:'yyyy-MM-dd HH:mm:ss'})"
												class="Wdate" name="outboundLandingTime" id="outboundLandingTime"/>
									</div>
								</td>								
							</tr>
							<tr>
								<td class="FT"><label>回程</label></td>
								<td>
									<div>
										<label>飛機班機(迄)日期:</label>
										<input type="text" value="<fmt:formatDate value="${cp.inboundDepartureTime}" type="date" pattern="yyyy-MM-dd" />"
													onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
													class="Wdate" name="inboundDepartureTime" id="inboundDepartureTime"/>
										<label>航班:</label><input name="inboundFlight" style="width: 100px;" value="${cp.inboundFlight }"/>
										<label>航班班次號碼:</label><input name="inboundFlightNo" style="width: 100px;" value="${cp.inboundFlightNo }"/>
									</div>
									<div>
										<label>到達國家:</label>
											<input name="inboundCountry" id="inboundCountry" value="${cp.inboundCountry }"/>
										<label>到達城市:</label>	
											<input name="inboundCity" id="inboundCity" value="${cp.inboundCity }"/>
										<label>時間:</label><input type="text" value="<fmt:formatDate value="${cp.inboundLandingTime }" type="date" pattern="yyyy-MM-dd HH:mm:ss" />"
												onfocus="WdatePicker({startDate:'%y-%M-%d 23:59:59', dateFmt:'yyyy-MM-dd HH:mm:ss'})"
												class="Wdate" name="inboundLandingTime" id="inboundLandingTime"/>
									</div>
								</td>
							</tr>
						</table>						
						<div>
							備註:請附上旅行社預訂機票旅客行程單。								
							<table class="table">				
								<tr>					
									<td><label>附件</label></td>
									<td><input class="btn btn-success" type="button" value="增加" id="addAttachment"/></td>
								</tr>				
								<tbody id="attachmentBody">
								<c:if test="${empty cp}">
								<tr>					
									<td>
										<input type="hidden" name="url" value=""/>
										<input name="fileName" value="" size="50"/>									
										<input class="upload" type="button" value="上傳"/>
									</td>
									<td></td>					
								</tr>
								</c:if>
								<c:forEach items="${attachment}" var="a">
								<tr>					
									<td>
										<input type="hidden" name="url" value="${a.url}"/>
										<input name="fileName" value="${a.description}" size="50"/>									
										<input type="button" value="上傳" onclick="$('#fileBrowse').click();"/>
									</td>
									<td><input class="btn" type="button" value="刪除" onclick="deleteAttachment('${a.id}', '${cp.id}')"/></td>
								</tr>
								</c:forEach>
								</tbody>
							</table>								
						</div>
					</td>
				</tr>				
				<tr>
					<td class="td1"><label>辦理文件</label></td>
					<td colspan="4">
						<span>
						<label>護照:</label>
						<input type="radio" name="agentPassport" value="1" <c:if test="${cp.agentPassport == '1' }">checked="checked"</c:if>/>是
						<input type="radio" name="agentPassport" value="2" <c:if test="${cp.agentPassport == '2' }">checked="checked"</c:if>/>否
						</span>
						/
						<span>
						<label>簽證:</label>
						<input type="radio" name="agentVisa" value="1" <c:if test="${cp.agentVisa == '1' }">checked="checked"</c:if>/>是
						<input type="radio" name="agentVisa" value="2" <c:if test="${cp.agentVisa == '2' }">checked="checked"</c:if>/>否
						</span>
						/
						<span>
						<label>台胞證:</label>
						<input type="radio" name="agentMtp" value="1" <c:if test="${cp.agentMtp == '1' }">checked="checked"</c:if>/>是
						<input type="radio" name="agentMtp" value="2" <c:if test="${cp.agentMtp == '2' }">checked="checked"</c:if>/>否
						</span>
					</td>
				</tr>
				<tr>
					<td rowspan="2"><label>出差費用預估</label></td>
					<td rowspan="2">
						<div>
							<label>機票費用:</label><input type="number" style="width: 60px;" name="estimatedCostAirfare" onblur="doEstimatedTotal()"
							<c:if test="${empty cp }">value="0"</c:if><c:if test="${not empty cp }">value="${cp.estimatedCostAirfare }"</c:if>/>							
						</div>
						<div>
							<label>辦理文件費:</label><input type="number" style="width: 60px;" name="estimatedCostAgent" onblur="doEstimatedTotal()"
							<c:if test="${empty cp }">value="0"</c:if><c:if test="${not empty cp }">value="${cp.estimatedCostAgent }"</c:if>/>
						</div>												
						<div>
							<label>住宿費用:</label><input type="number" style="width: 60px;" name="estimatedCostLodging" onblur="doEstimatedTotal()"
							<c:if test="${empty cp }">value="0"</c:if><c:if test="${not empty cp }">value="${cp.estimatedCostLodging }"</c:if>/>
						</div>
						<div>
							<label>(台灣)機場接送安排費用:</label><input type="number" style="width: 60px;" name="estimatedCostPickup" onblur="doEstimatedTotal()"
								<c:if test="${empty cp }">value="0"</c:if><c:if test="${not empty cp }">value="${cp.estimatedCostPickup }"</c:if>/>
						</div>
					</td>
					<td><label>出差費用預估合計</label></td>
					<td rowspan="2"><label>出差安排承辦人簽名</label></td>
					<td rowspan="2"></td>								
				</tr>
				<tr>
					<td><div id="businessTotal"></div></td>
				</tr>
				<tr>
					<td class="td1"><label>預支費用</label></td>
					<td colspan="4">						
						<input type="radio" name="cashAdvance" value="1" onClick="ableCashAdvance()" <c:if test="${cp.cashAdvance == '1' }">checked="checked"</c:if>/>是，幣別：
						<span>
						<select name="advanceCurrency"  <c:if test="${empty cp }">disabled="disabled"</c:if>>
							<option value="">請選擇</option>
							<option value="TWD" <c:if test="${cp.advanceCurrency == 'TWD' }">selected="selected"</c:if>>新臺幣(TWD)</option>
							<option value="HKD" <c:if test="${cp.advanceCurrency == 'HKD' }">selected="selected"</c:if>>港幣(HKD)</option>
							<option value="USD" <c:if test="${cp.advanceCurrency == 'USD' }">selected="selected"</c:if>>美金(USD)</option>
							<option value="CNY" <c:if test="${cp.advanceCurrency == 'CNY' }">selected="selected"</c:if>>人民幣(CNY)</option>
							<option value="KRW" <c:if test="${cp.advanceCurrency == 'KRW' }">selected="selected"</c:if>>韓元(KRW)</option>
							<option value="JPY" <c:if test="${cp.advanceCurrency == 'JPY' }">selected="selected"</c:if>>日圓(JPY)</option>
						</select></span>；
						<span><input type="number" name="advanceAmount" <c:if test="${empty cp }">disabled="disabled"</c:if> <c:if test="${empty cp }">value="0"</c:if>
						<c:if test="${not empty cp }">value="${cp.advanceAmount }"</c:if>/></span>
						元(請於三個工作天前提出申請)
						<input type="radio" name="cashAdvance" value="2" onclick="resetCashAdvance()" <c:if test="${empty cp }">checked="checked"</c:if> <c:if test="${cp.cashAdvance == '2' }">checked="checked"</c:if>/>否
					</td>
				</tr>
				<tr>
					<td colspan="5"><label>國內/外 出差日支費補助</label></td>
				</tr>
				<tr>
					<td class="td1"><label>出差日支費</label> </td>						
					<td colspan="4">
						<div>
							<input type="radio" name="dailyAllowance" value="1" <c:if test="${cp.dailyAllowance == '1' }">checked="checked"</c:if>/>國內:因公出差跨日，額外給予600元(台幣)/天出差日支費補助。
						</div>
						<div>
							<input type="radio" name="dailyAllowance" value="2" <c:if test="${cp.dailyAllowance == '2' }">checked="checked"</c:if>/>國外:因公出差，額外給予1,000元(台幣)/天出差日支費補助。
						</div>
					</td>
				</tr>				
				<tr>
					<td class="td1"><label>備註:</label></td>
					<td colspan="4"><input name="memo" value="${cp.memo }"/></td>
				</tr>				
			</table>
			<table class="table">
				<tr>
					<td colspan="2" class="td2" style="text-align:center;"><label>協辦單位</label></td>					
					<td colspan="2" class="td2" style="text-align:center;"><label>出差單位</label></td>
				</tr>
				<tr>
					<td><label>會計</label></td>					
					<td><label>人資</label></td>					
					<td><label>部門主管</label></td>				
					<td><label>申請人</label></td>					
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td>${empty cp.creatorName ? sessionScope.employee.name : cp.creatorName}</td>
				</tr>
			</table>
		</div>
	</form>
	<input type="button" value="保存" id="save" />	
	<input type="button" value="送審" id="submit"/>
	<form name="upload" id="fileUploadForm" enctype="multipart/form-data" method="post">
		<div style="display: none">
			<input type="file" id="fileBrowse" name="file" style="border: 0;" />
		</div>
	</form>
</div>

<script type="text/javascript">
doEstimatedTotal();

function doEstimatedTotal(){
	var total = 0;
	
	var airfare = $('input[name=estimatedCostAirfare]').val();
	var agent = $('input[name=estimatedCostAgent]').val();
	var lodging = $('input[name=estimatedCostLodging]').val();
	var pickup = $('input[name=estimatedCostPickup]').val();
	
	if(airfare != ''){
		total += parseInt(airfare, 10);		
	}
	if(agent != ''){
		total += parseInt(agent, 10);
	}
	if(lodging != ''){
		total += parseInt(lodging, 10);
	}
	if(pickup != ''){
		total += parseInt(pickup, 10);		
	}
	
	$('#businessTotal').html(total);
}

$(function(){	
	$('#save').click(function() {
		if(checkValidate()){
			$("#editForm").ajaxSubmit({
	            success: function(e) {
	                alert(e.message), $("#myModal").modal("toggle");
	                $('#search').click();
	            },
	            error: function() {
	                alert("網路連接失敗，請聯繫管理員");
	            }
	        });
		}		
	});
	
	$('#submit').click(function() {
		if(checkValidate()){
			$('#editForm').attr('action', '${ctx}/businessTripForm/submit');
			$("#editForm").ajaxSubmit({
	            success: function(e) {
	            	if(e.code == "0000") {
	            		alert(e.message), $("#myModal").modal("toggle");
		                $('#search').click();
	            	}
	            	else {
	            		alert(e.message);
	            	}
	            },
	            error: function() {
	                alert("網路連接失敗，請聯繫管理員");
	            }
	        });			
		}
	});
	
	$('#addAttachment').click(function(){
		var html = '';
		html += '<tr>';
		html += '<td><input type="hidden" name="url" value=""><input name="fileName" value="" size="50">';		
		html += '<input class="upload" type="button" value="上傳"></td>';
		html += '<td><input class="btn delete" type="button" value="刪除"></td>';
		html += '</tr>';
		
		$('#attachmentBody').append(html);
	});
	
	var target;  // fileName
	$('#attachmentBody').on('click', '.upload', function(){
		target = $(this).prev();
		$('#fileBrowse').click();
	});
	
	$('#fileUploadForm').on('submit', (function(e) {		
		e.preventDefault();
		
		var formData = new FormData(this);
		$.ajax({
			async : false,
			type : 'POST',
			url : '${ctx}/businessTripForm/upload.do',
			data : formData,
			cache : false,
			contentType : false,
			processData : false,
			success : function(data) {
				if (data == "overSize") {
					alert("檔案超過1MB請更換圖片");
					$("#fileBrowse").val("");
					return;
				}				
				//$("#url").val(data.url);
				target.val(data.description);
				target.prev().val(data.url);
			},
			error : function(data) {
				alert("系統錯誤，請稍後再試");
			}
		});
	}));
	
	$("#fileBrowse").on("change", function() {
		if (this.files[0].size >= 1048576) {
			alert("檔案超過1MB請更換檔案");
			return;
		}	
		$("#fileUploadForm").submit();
	});		
	
	$('#attachmentBody').on('click', '.delete', function(){
		$(this).parent().parent().remove();
	});	
	
	// 日支費同步
	$("input[name=oversea]:radio").change(function() {       
	    $("input[name=dailyAllowance]").val([this.value]);
	});
		
	$("input[name=dailyAllowance]:radio").change(function() {       
	    $("input[name=oversea]").val([this.value]);
	});
	
	$('#companyId').change(function() {
		var cid = $(this).val();
		var name = $(this).find("option:selected").text();
		$("#title").text(name);
		// department
		getUserInfo(cid);
	});
	
	// default title
	$("#title").text($('#companyId').find("option:selected").text());
	
	if($("#department").text().trim() == "") {
		getUserInfo($('#companyId').val());
	}		
});

function checkValidate(){
	var pass = true;
	var html = "<font class='warning' color='red'>請輸入此欄位</font>";
	
	if(!$('input[name=oversea]').is(':checked')){
		pass = false;
		$('input[name=oversea]').parent().find('.warning').remove();
		$('input[name=oversea]').parent().append(html);		
	}else{
		$('input[name=oversea]').parent().find('.warning').remove();
	}
	
	if($('#tripMatter').val() == ''){
		pass = false;
		$('#tripMatter').parent().find('.warning').remove();
		$('#tripMatter').parent().append(html);		
	}else{
		$('#tripMatter').parent().find('.warning').remove();
	}
	
	if($('#tripLocation').val() == ''){
		pass = false;
		$('#tripLocation').parent().find('.warning').remove();
		$('#tripLocation').parent().append(html);		
	}else{
		$('#tripLocation').parent().find('.warning').remove();
	}
	
	if($('input[name=cashAdvance]:checked').val() == '1'){
		if($('select[name=advanceCurrency]').val() == ''){
			pass = false;
			$('select[name=advanceCurrency]').parent().find('.warning').remove();
			$('select[name=advanceCurrency]').parent().append(html);
		}else{
			$('select[name=advanceCurrency]').parent().find('.warning').remove();
		}
		
		if($('input[name=advanceAmount]').val() == '0'){
			pass = false;
			$('input[name=advanceAmount]').parent().find('.warning').remove();
			$('input[name=advanceAmount]').parent().append(html);
		}else{
			$('input[name=advanceAmount]').parent().find('.warning').remove();
		}	
	}
	
	if($('#outboundCountry').val() == ''){
		pass = false;
		$('#outboundCountry').parent().find('.warning').remove();
		$('#outboundCountry').parent().append(html);		
	}else{
		$('#outboundCountry').parent().find('.warning').remove();
	}
	if($('#outboundCity').val() == ''){
		pass = false;
		$('#outboundCity').parent().find('.warning').remove();
		$('#outboundCity').parent().append(html);		
	}else{
		$('#outboundCity').parent().find('.warning').remove();
	}
	if($('#outboundLandingTime').val() == ''){
		pass = false;
		$('#outboundLandingTime').parent().find('.warning').remove();
		$('#outboundLandingTime').parent().append(html);		
	}else{
		$('#outboundLandingTime').parent().find('.warning').remove();
	}

	if($('#inboundCountry').val() == ''){
		pass = false;
		$('#inboundCountry').parent().find('.warning').remove();
		$('#inboundCountry').parent().append(html);		
	}else{
		$('#inboundCountry').parent().find('.warning').remove();
	}
	if($('#inboundCity').val() == ''){
		pass = false;
		$('#inboundCity').parent().find('.warning').remove();
		$('#inboundCity').parent().append(html);		
	}else{
		$('#inboundCity').parent().find('.warning').remove();
	}
	if($('#inboundLandingTime').val() == ''){
		pass = false;
		$('#inboundLandingTime').parent().find('.warning').remove();
		$('#inboundLandingTime').parent().append(html);		
	}else{
		$('#inboundLandingTime').parent().find('.warning').remove();
	}
	if(!$('input[name=agentPassport]').is(':checked')){
		pass = false;
		$('input[name=agentPassport]').parent().find('.warning').remove();
		$('input[name=agentPassport]').parent().append(html);		
	}else{
		$('input[name=agentPassport]').parent().find('.warning').remove();
	}
	if(!$('input[name=agentVisa]').is(':checked')){
		pass = false;
		$('input[name=agentVisa]').parent().find('.warning').remove();
		$('input[name=agentVisa]').parent().append(html);		
	}else{
		$('input[name=agentVisa]').parent().find('.warning').remove();
	}
	if(!$('input[name=agentMtp]').is(':checked')){
		pass = false;
		$('input[name=agentMtp]').parent().find('.warning').remove();
		$('input[name=agentMtp]').parent().append(html);		
	}else{
		$('input[name=agentMtp]').parent().find('.warning').remove();
	}
	
	if($('#paymentDate').val() == ''){
		pass = false;
		$('input[name=paymentDate]').parent().find('.warning').remove();
		$('input[name=paymentDate]').parent().append(html);		
	}else{
		$('input[name=paymentDate]').parent().find('.warning').remove();
	}
	
	if($('#remitDate').val() == ''){
		pass = false;
		$('input[name=remitDate]').parent().find('.warning').remove();
		$('input[name=remitDate]').parent().append(html);		
	}else{
		$('input[name=remitDate]').parent().find('.warning').remove();
	}
	
	if(!pass){
		alert("表單未填寫完整!");
	}
	
	return pass;
}

var cashAdvance = $('input[name=cashAdvance]:checked').val();

if(cashAdvance == '1'){
	ableCashAdvance();
}else if(cashAdvance == '2'){
	resetCashAdvance();
}

// 預支費用重設
function resetCashAdvance() {
	$('select[name=advanceCurrency]').val("");
	$('input[name=advanceAmount]').val("0");
	$('select[name=advanceCurrency]').attr("disabled", true);
	$('input[name=advanceAmount]').attr("disabled", true);
}

function ableCashAdvance(){
	$('select[name=advanceCurrency]').attr("disabled", false);
	$('input[name=advanceAmount]').attr("disabled", false);
}

function getUserInfo(cid) {
	$.ajax({
		url : '${ctx}/employee/userInfo',
		dataType : 'json',
		data : {
			'id' : cid
		},
		success : function(data) {
			var dept = data.split(",");
			// dept
			$("#department").text(dept[1]);
			$("#requestDepartment").val(dept[0]);
			// job			
			$("#jobTitle").text(dept[3]);
			$("#jobTitleId").val(dept[2]);			
			// employee
			$("#employeeName").text(dept[5]);
			$("#employeeId").val(dept[4]);
		},
		error : function() {
			alert('網絡連接失敗，請聯繫管理員');
		}
	});
}

function getEmployeeInfo(eid) {
	$.ajax({
		url : '${ctx}/employee/employeeInfo',
		dataType : 'json',
		data : {
			'id' : eid
		},
		success : function(data) {
			var dept = data.split(",");			
			$("#department").text(dept[1]);
			$("#requestDepartment").val(dept[0]);
			$("#jobTitle").text(dept[3]);
			$("#jobTitleId").val(dept[2]);
		},
		error : function() {
			alert('網絡連接失敗，請聯繫管理員');
		}
	});
}
</script>