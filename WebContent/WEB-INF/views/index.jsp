<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<title>8888play ERP</title>
</head>
<body>
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>首頁</h1>
			<ol class="breadcrumb">
				<li class="active">
					<a href="#"><i class="fa fa-home"></i>首頁</a>
				</li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<!-- Main row -->
			<div class="row">
				<div class="col-xs-12" style="padding: 100px 0 100px 0;">
					<!-- /.box-header -->
					<shiro:authenticated>
						<h1 class="text-center">歡迎${loginUser.loginAccount}登入8888play ERP</h1>
						<c:if test="${loginUser.loginAccount != 'admin'}">						
						<!-- 權限 -->
						<h5 class="text-center">
						<c:forEach items="${role}" var="r" varStatus="loop">${r.name}<c:if test="${fn:length(role) gt loop.count}">&nbsp;</c:if></c:forEach>
						(<c:forEach items="${formRole}" var="role" varStatus="loop">${role}<c:if test="${fn:length(formRole) gt loop.count}">&nbsp;</c:if></c:forEach>)						
						</h5>
						<!-- 員工 -->
						<h5 class="text-center">${employee.name} / ${employee.jobTitleName} / ${employee.departmentName} / ${employee.companyName}</h5>
						<!-- 分身 -->
						<c:forEach items="${moreEmployee}" var="clone">
						<h5 class="text-center">${clone.jobTitleName} / ${clone.departmentName} / ${clone.companyName}</h5>   
						</c:forEach>
						<p>待辦事項：<p>
						<c:forEach items="${todoList}" var="o">
							<a href="/flow/init?type=${o.formType}">${o.formName}<span style="color:red;">(${o.count})</span></a>
							<br>
						</c:forEach>
						</c:if>
					</shiro:authenticated>
					<!-- /.box-body -->
					<!-- /.box -->					
				</div>
			</div>
			<!-- /.row (main row) -->
		</section>
		<!-- /.content -->
	</aside>	
</body>
</html>