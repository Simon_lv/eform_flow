<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
<head>
<title>8888play ERP</title>
<link href="${ctx}/resources/multiple-select/multiple-select.css" rel="stylesheet" type="text/css">
<style>
.flowStep9 {
	padding: 10px;
	border-radius: 25px;
	display: inline-block;
	width: 250px;
  	height: 200px;
  	margin: 10px 10px 10px 20px;
	position: relative;
	background: #88b7d5;
	border: 4px solid #000000;
}

.flowStep {
	padding: 10px;
	border-radius: 25px;
	display: inline-block;
	width: 250px;
  	height: 200px;
  	margin: 10px 10px 10px 20px;
	position: relative;
	background: #88b7d5;
	border: 4px solid #000000;
}

.flowStep:after, .flowStep:before {
	left: 100%;
	top: 50%;
	border: solid transparent;
	content: " ";
	height: 0;
	width: 0;
	position: absolute;
	pointer-events: none;
}

.flowStep:after {
	border-color: rgba(136, 183, 213, 0);
	border-left-color: #88b7d5;
	border-width: 20px;
	margin-top: -20px;
}

.flowStep:before {
	border-color: rgba(0, 0, 0, 0);
	border-left-color: #000000;
	border-width: 26px;
	margin-top: -26px;
}

.highlight {
    display: inline;
    background: red;
    color: white;     
}
</style>
</head>
<body>
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div class="modal-body" id="myModalBody"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="modalSubmit">關閉</button>
				</div>
			</div>
		</div>
	</div>
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				表單管理 <small>流程管理</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="${ctx}/index"><i class="fa fa-dashboard"></i>8888play ERP</a></li>
				<li class="active">流程管理</li>
			</ol>
		</section>
		<!-- Main content -->
		<div class="container-fluid">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body">
						<form action="${ctx}/flowMgr/list" id="listForm" method="post">
							<input type="hidden" id="pageNo" name="pageNo" value="" />
							<div class="table-responsive">						
								<div class="form-group col-sm-12 col-md-4 col-lg-3">
	                            	<label for="inputid">流程:</label>
	                            	<select class="form-control" name="companyId" id="companyId">
		                            	<option value="">全部</option>
		                            	<c:forEach items="${companyList}" var="c">
		                            	<option value="${c.id}">${c.name}</option>
		                            	</c:forEach>
	                            	</select>
	                            	<select class="form-control" name="flowId" id="flowId">
		                            	<option value="">全部</option>		                            	
	                            	</select>
	                            </div>	                                                      
	                            <hr>
								<div class="btn-group col-sm-12 col-md-12 col-lg-6" role="group">
		                        	<input class="btn btn-primary" type="button" id="search" value="搜尋"/>
		                            <!-- 
		                            <input class="btn btn-success" type="button" id="add" value="新增"/>
		                            -->		                                                       
		                        </div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div id="listContainer"></div>
		</div>
	</aside>

	<script type="text/javascript" src="${ctx }/js/jquery.form.js"></script>
	<script type="text/javascript" src="${ctx }/resources/multiple-select/jquery.multiple.select.js"></script>
	<script type="text/javascript">
		$(function() {									
			$("#add").click(function() {
				$(".modal-dialog").css("width", "900px"), 
				$(".modal-dialog").css("maxheight", "800px"), 
				$("#myModal").modal("toggle"), 
				$("#myModalLabel").html("編輯"), 
				$("#myModalBody").html(
					"<h3>正在打開。。。</h3>"), $.get("/qcFormMgr/edit?id=", function(e) {
						$("#myModalBody").html(e);
				});
			});

			$('#search').click(function() {
				$('#pageNo').val("1");
				$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
				$('#listForm').ajaxSubmit({
					success : function(data) {
						$('#listContainer').html(data);
					}
				});
			});
			
			$('#modalSubmit').click(function() {
				$('#myModal').modal('toggle');
				$('#search').click();
			});
			
			$('#companyId').change(function() {
				var cid = $(this).val();
				
				if(cid == '') {
					$('#flowId').empty();					
					$('#flowId').append('<option value="">全部</option>');
				}
				else {
					$.ajax({
						url : '${ctx}/flowMgr/flowList',
						dataType : 'json',
						data : {
							'id' : cid
						},
						success : function(data) {
							$('#flowId').empty();							
							$('#flowId').append('<option value="">全部</option>');
							
							for(var i in data) {
								$('#flowId').append(
									'<option value="' + data[i].id+'">'
									+ data[i].flowName
									+ '</option>');
							}
						},
						error : function() {
							alert('網絡連接失敗，請聯繫管理員');
						}
					});
				}
			});
		});				
		
		function toPage(pageNo) {
			if (!pageNo || pageNo <= 0) {
				return;
			}
			
			$('#pageNo').val(pageNo);
			$('#listForm').ajaxSubmit({
				success : function(data) {
					$('#listContainer').html(data);
				},
				error : function() {
					alert('連接網路失敗');
				}
			});
			$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
		}				
		
		function edit(id) {			
			$(".modal-dialog").css("width", "900px"), $(".modal-dialog").css(
				"maxheight", "800px"), $("#myModal").modal("toggle"), 
				$("#myModalLabel").html("編輯"), $("#myModalBody").html(
				"<h3>正在打開。。。</h3>"), $.get("/flowMgr/edit?id=" + id, function(e) {
					$("#myModalBody").html(e);
				});
		}		
		
		function get(id) {
			$(".modal-dialog").css("width", "900px"), $(".modal-dialog").css(
				"maxheight", "800px"), $("#myModal").modal("toggle"), 
				$("#myModalLabel").html("檢視"), $("#myModalBody").html(
				"<h3>正在打開。。。</h3>"), $.get("/flowMgr/get?id=" + id, function(e) {
					$("#myModalBody").html(e);
				})
		}
		
		function companyFlow() {
			
		}
	</script>
</body>
</html>