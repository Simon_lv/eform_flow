<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	
	<table
		class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>
				<c:set var="totalStep" value="${fn:length(flowStepList)}"></c:set>
				<th colspan="${totalStep}">
					流程名稱：${flowSchema.flowName}
				</th>											
			</tr>
		</thead>
		<tbody>			
			<tr>
				<td>
					<c:forEach items="${flowStepList}" var="o" varStatus="counter">
						<div class="flowStep${o.stepType}">
							<!-- order -->
							順序：${o.stepOrder}
							<br>
							<!-- stepType -->
							<b>
							<c:choose>
							<c:when test="${o.stepType == 1}">編輯</c:when>
							<c:when test="${o.stepType == 2}">審核</c:when>
							<c:when test="${o.stepType == 3}">放行</c:when>
							<c:when test="${o.stepType == 9}">結束</c:when>
							</c:choose>
							</b>
							<br>
							<!-- role -->							
							角色：
							<c:if test="${o.stepType == 1 || o.stepType == 9}">${o.role}</c:if>
							<c:if test="${o.stepType == 2 || o.stepType == 3}">
							<select id="formRoleId_${o.id}">
								<c:forEach items="${formRoles}" var="r">
								<option value="${r.id}" <c:if test="${o.roleId == r.id}">selected="selected"</c:if>>${r.name}</option>
								</c:forEach>
							</select>
							<a href="javascript:;" class="btn btn-primary" onclick="updateRole(${o.id})">變更</a>
							</c:if>							
							<br>
							<!-- sameDept -->
							與前一經手人同部門：
							<c:choose>
							<c:when test="${o.sameDepartment == 2}">是</c:when>
							<c:otherwise>否</c:otherwise>
							</c:choose>
							<br>
							<!-- router -->
							限定條件：${o.router}
							<br>
							<c:if test="${o.stepType == 2 || o.stepType == 3}">
							<!-- iteration -->
							集章數：${o.iteration}
							<br>
							<!-- countersign -->
							會簽：<span id="countersign_span_${o.id}">${o.countersignRole}</span>
							<a href="javascript:;" id="link_countersign_${o.id}" class="btn btn-primary" onclick="updateCountersign(${o.id})">變更</a>																				
							</c:if>
							<c:if test="${o.stepType == 1 || o.stepType == 9}">
							&nbsp;<br>&nbsp;<br>&nbsp;
							</c:if>							
						</div>						
					</c:forEach>				
				</td>
			</tr>			
		</tbody>
	</table>
</div>

<script type="text/javascript">
// Update FlowStep Role ID
function updateRole(id) {
	$.ajax({
		url : '${ctx}/flowMgr/updateRole',
		dataType : 'json',
		data : {
			'id1' : id,
			'id2' : $("#formRoleId_" + id).val()
		},
		success : function(data) {
			if(data.code == "0000") {
				alert("變更成功");						
			}
			else {
				alert(data.message);
			}			
		},
		error : function() {
			alert('網絡連接失敗，請聯繫管理員');
		}
	});
}
// Update countersign
function updateCountersign(id) {
	var link = $("#link_countersign_" + id);
	var span = $("#countersign_span_" + id);
		
	if(link.text() == "確定") {
		var combo = $("#countersign_" + id);
		var countersign =  combo.multipleSelect("getSelects");
		var text = combo.multipleSelect("getSelects", "text");
		
		if(text == '') {
			text = '免';
		}
		
		$.ajax({
			url : '${ctx}/flowMgr/saveCountersign',
			dataType : 'json',
			data : {
				'id1' : id,
				'id2' : countersign.join()				
			},
			success : function(data) {
				if(data.code == "0000") {
					alert("變更成功");
					span.html(text);
					link.text("變更");
				}
				else {
					alert(data.message);
				}			
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	}
	else {
		$.ajax({
			url : '${ctx}/flowMgr/updateCountersign',
			dataType : 'json',
			data : {
				'id' : id
			},
			success : function(data) {
				if(data.code == "0000") {
					link.text("確定");
					
					var s = $('<select id="countersign_' + id + '" width="100px"/>');

					for(var r in data.formRoles) {
						var role = data.formRoles[r];
						$('<option />', {value: role.id, text: role.name}).appendTo(s);
					}

					span.html(s);
					
					$("#countersign_" + id).multipleSelect({
						selectAll: false,
						allSelected : "全部",
						countSelected : "#個已選擇"
					});
										
					if(data.flowStep.countersign == '') {
						$("#countersign_" + id).multipleSelect("uncheckAll");						
					}
					else {
						$("#countersign_" + id).multipleSelect("setSelects", data.flowStep.countersign.split(","));
					}					
				}
				else {
					alert(data.message);
				}			
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});		
	}		
}
</script>