<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	
	<table
		class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>
				<c:set var="totalStep" value="${fn:length(flowStepList)}"></c:set>
				<th colspan="${totalStep}">
					流程名稱：${flowSchema.flowName}
				</th>											
			</tr>
		</thead>
		<tbody>			
			<tr>
				<td>
					<c:forEach items="${flowStepList}" var="o" varStatus="counter">
						<div class="flowStep${o.stepType == 9 ? '9' : ''}">
							<!-- order -->
							順序：${o.stepOrder}
							<br>
							<!-- stepType -->
							<b>
							<c:choose>
							<c:when test="${o.stepType == 1}">編輯</c:when>
							<c:when test="${o.stepType == 2}">審核</c:when>
							<c:when test="${o.stepType == 3}">放行</c:when>
							<c:when test="${o.stepType == 9}">結束</c:when>
							</c:choose>
							</b>
							<br>
							<!-- role -->
							角色：${o.role}
							<br>
							<c:if test="${o.stepType == 2 || o.stepType == 3}">
							<!-- sameDept -->
							與前一經手人同部門：
							<c:choose>
							<c:when test="${o.sameDepartment == 2}">是</c:when>
							<c:otherwise>否</c:otherwise>
							</c:choose>
							<br>
							<!-- router -->
							<span class="highlight">限定條件：${o.router}</span>
							<br>
							<!-- iteration -->
							集章數：${o.iteration}
							<br>
							<!-- countersign -->
							會簽：${o.countersignRole}
							<br>							
							</c:if>
							<c:if test="${o.stepType == 1 || o.stepType == 9}">
							&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;
							</c:if>							
						</div>												
					</c:forEach>				
				</td>
			</tr>			
		</tbody>
	</table>
</div>