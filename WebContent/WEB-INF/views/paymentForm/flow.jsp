<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<input type="hidden" name="id1" id="id1" value="${param.id1}" />	
	<input type="hidden" name="id2" id="id2" value="${param.id2}" />
	<div class="table-responsive">
		
		<div style="text-align: center;"><h2>${form.companyName}</h2></div>
		<div style="text-align: center;"><h3>請款單 Payment Request Form</h3></div>
		
		<div style="text-align: left;"><label>編號&nbsp;</label>${form.serialNo}</div>
		<table class="table">
			<tr>
				<td colspan="7" style="text-align: center;">請款者 Requester Profile</td>									
			</tr>
			<tr>
				<td>申請日期<br/>Date Requested</td>
				<td colspan="2"><fmt:formatDate value="${form.requestDate}" type="date" pattern="yyyy-MM-dd" /></td>
				<td></td>
				<td>部門<br/>Division/Department</td>
				<td>${form.departmentName}</td>					
				<td>${form.employeeName}</td>
			</tr>
			<tr>
				<td colspan="7">請款資訊 Payment Information</td>					
			</tr>
			<tr>
				<td colspan="7">
					付款方式 Payment Way：
					<input type="radio" name="paymentWay" value="1" <c:if test="${form.paymentWay == 1 }">checked=checked</c:if>>現金
					<input type="radio" name="paymentWay" value="2" <c:if test="${form.paymentWay == 2 }">checked=checked</c:if>>匯款
					<input type="radio" name="paymentWay" value="3" <c:if test="${form.paymentWay == 3 }">checked=checked</c:if>>支票
					<input type="radio" name="paymentWay" value="4" <c:if test="${form.paymentWay == 4 }">checked=checked</c:if>>沖預付
				</td>	
			</tr>
			<tr>
				<td>
					<div>出款日</div>
					<div>Payment Date</div>
				</td>
				<td colspan="6"><fmt:formatDate value="${form.remitDate}" type="date" pattern="yyyy-MM-dd" /></td>
			</tr>
			<tr>
				<td>受款人<br/>Name of Beneficiary</td>					
				<td colspan="6">${form.payee }</td>
			</tr>
			<tr>
				<td>受款人銀行<br/>Name of Beneficiary Bank</td>					
				<td colspan="6">${form.payeeBank }</td>
			</tr>
			<tr>
				<td>受款人帳號<br/>Beneficiary's Account No</td>					
				<td colspan="6">${form.payeeAccount }</td>
			</tr>
			<tr>
				<td>銀行交換代號<br/>Bank Clearing Code</td>					
				<td colspan="6">${form.bankClearingCode }</td>
			</tr>
			<tr>
				<td>附言或指示<br/>Message or Instructions</td>					
				<td colspan="6">${form.message }</td>
			</tr>
			<tr>
				<td>聯絡人<br/>Contact person</td>					
				<td colspan="2">${form.contact }</td>
				<td></td>
				<td>電話<br/>Tel</td>					
				<td colspan="2">${form.contactTel }</td>
			</tr>
			<tr>
				<td colspan="7">
					<table width="100%">
						<tr>					
							<td width="15%">憑證日期<br/>Date</td>
							<td width="10%">項目<br/>Item</td>
							<td width="35%">費用說明<br/>Description</td>
							<td width="10%">金額<br/>Amount</td>
							<td width="10%">幣別<br/>Currency</td>
							<td width="10%">匯率<br/>Exchange Rate</td>
							<td width="10%">總計<br/>Total</td>
						</tr>							
						<c:forEach items="${receipts }" var="rc">						
						<tr>
							<td><fmt:formatDate value="${rc.costDate}" type="date" pattern="yyyy-MM-dd" /></td>
							<td>${rc.costItem }</td>
							<td>${rc.description }</td>
							<td align="center">
								<fmt:parseNumber value="${rc.amount}" var="amount"/>
								<fmt:formatNumber value="${amount}" pattern="###,###.##"/></td>
							<td>					
								<c:if test="${rc.currency == 'TWD' }">新臺幣(TWD)</c:if>
								<c:if test="${rc.currency == 'HKD' }">港幣(HKD)</c:if>
								<c:if test="${rc.currency == 'USD' }">美金(USD)</c:if>
								<c:if test="${rc.currency == 'CNY' }">人民幣(CNY)</c:if>
								<c:if test="${rc.currency == 'KRW' }">韓元(KRW)</c:if>
								<c:if test="${rc.currency == 'JPY' }">日圓(JPY)</c:if>												
							</td>
							<td align="center"><fmt:parseNumber integerOnly="${rc.rate % 1 == 0}" type="number" value="${rc.rate}" /></td>
							<td align="center"><fmt:formatNumber pattern="###,###.##" type="number" value="${rc.amount * rc.rate}" /></td>
						</tr>
						</c:forEach>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="6" style="text-align: center;">總計Total</td>
				<td style="text-align: center;">							
					<div id="totalDiv"><fmt:formatNumber value="${form.totalAmount }" type="currency" maxFractionDigits="0"/></div>
				</td>
			</tr>
			<tr>
				<td colspan="7">										
					<c:forEach items="${attachment}" var="a" varStatus="loop">
					<a href="${a.url}" target="_blank"><label>附件_${loop.count}</label></a>								
					</c:forEach>										
				</td>
			</tr>
			<tr>
				<td colspan="7">
					<table width="100%">
						<tr>
							<c:if test="${not empty stamp['總經理']}">
							<td style="text-align: center;">總經理</td>
							</c:if>				
							<c:if test="${not empty stamp['營運主管']}">
							<td style="text-align: center;">營運主管</td>
							</c:if>
							<td style="text-align: center;">部門主管</td>												
							<td style="text-align: center;">申請人</td>
						</tr>
						<tr>
							<c:if test="${not empty stamp['總經理']}">
							<td style="text-align: center;">${stamp['總經理'].info}</td>							
							</c:if>
							<c:if test="${not empty stamp['營運主管']}">
							<td style="text-align: center;">${stamp['營運主管'].info}</td>							
							</c:if>
							<td style="text-align: center;">${stamp['部門主管'].info}${stamp['行政主管'].info}</td>			
							<td style="text-align: center;">${stamp['申請人'].info}</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="7">請檢附已書名統一編號的發票或是收據</td>
			</tr>						
		</table>
		<!-- 退件原因 -->
		<table class="table">
			<tr>
				<td>
					退件原因：<input id="memo" name="memo" size="30"/>
					<input type="button" value="退件" id="reject"/>
				</td>
			</tr>
		</table>
		
		<!-- flow button -->
		<input type="button" value="取消" id="abort" />
		
		<c:if test="${isVerify}">
			<input type="button" value="審核" id="verify"/>
		</c:if>
		
		<c:if test="${isCountersign}">
		<input type="button" value="會簽" id="countersign"/>
		</c:if>
		
		<c:if test="${isPass}">
			<input type="button" value="放行" id="pass"/>			
		</c:if>				
	</div>			
</div>

<!-- Modal -->
<div class="modal fade" id="confirm" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body" id="confirmMsg" style="border: 2px solid black">
				確定放行？<br>
				<input type="button" value="確定" onclick="passConfirm(true)"/>
				<input type="button" value="取消" onclick="passConfirm(false)"/>
			</div>				
		</div>
	</div>
</div>

<script type="text/javascript">
// Fix 2nd modal closed then 1st scroll bar not work 
$(document).on('hidden.bs.modal', '.modal', function () {
    $('.modal:visible').length && $(document.body).addClass('modal-open');
});

$(function(){
	$('#abort').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
			
		$.ajax({
			url : '${ctx}/paymentForm/abort',
			dataType : 'json',
			data : {
				'id' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#verify').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		
		$.ajax({
			url : '${ctx}/paymentForm/verify',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#countersign').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		
		$.ajax({
			url : '${ctx}/paymentForm/countersign',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});		
		
	$('#pass').click(function() {
		var posY = $(this).position().top;
		$('#confirm').css("top", posY);						
		$('#confirm .modal-dialog').css("width", "200px");
		$('#confirm .modal-dialog').css("height", "100px");
		$("#confirm").modal("show");					
	});	
		
	$('#reject').click(function() {		
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		var memo = $("#memo").val();
		
		$.ajax({
			url : '${ctx}/paymentForm/reject',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2,
				'memo' : memo
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});		
	});	
	
	$('input[type=radio]').each(function(){
		$(this).attr("disabled", true);		
	});		
});

function passConfirm(allow) {
	$("#confirm").modal("hide");
	
	if(allow) {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		console.log("dump:" + id1 + "," + id2);
		$.ajax({
			url : '${ctx}/paymentForm/pass',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
		
		$("#myModal").modal("toggle");
	}	
}
</script>