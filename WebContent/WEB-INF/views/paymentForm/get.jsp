<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->	
	<div class="table-responsive">
		
		<div style="text-align: center;"><h2>${cp.companyName}</h2></div>
		<div style="text-align: center;"><h3>請款單 Payment Request Form</h3></div>
		
		<div style="text-align: left;"><label>編號&nbsp;</label>${cp.serialNo}</div>
		<table class="table">
			<tr>
				<td colspan="7" style="text-align: center;">請款者 Requester Profile</td>									
			</tr>
			<tr>
				<td>申請日期<br/>Date Requested</td>
				<td colspan="2">
					<fmt:formatDate value="${cp.requestDate}" type="date" pattern="yyyy-MM-dd" />
				</td>
				<td></td>				
				<td>部門<br/>Division/Department</td>
				<td>${cp.departmentName}</td>					
				<td>${cp.employeeName}</td>
			</tr>
			<tr>
				<td colspan="7">請款資訊 Payment Information</td>					
			</tr>
			<tr>	
				<td>付款方式 Payment Way<br/>Date Requested</td>
				<td colspan="6">
					<c:if test="${cp.paymentWay == 1 }">現金</c:if>
					<c:if test="${cp.paymentWay == 2 }">匯款</c:if>
					<c:if test="${cp.paymentWay == 3 }">支票</c:if>
					<c:if test="${cp.paymentWay == 4 }">沖預付</c:if>
				</td>	
			</tr>
			<tr>
				<td>
					<div>出款日</div>
					<div>Payment Date</div>
				</td>
				<td colspan="6"><fmt:formatDate value="${cp.remitDate}" type="date" pattern="yyyy-MM-dd" /></td>
			</tr>
			<tr>
				<td>
					<div>受款人</div>
					<div>Name of Beneficiary</div>
				</td>					
				<td colspan="6">${cp.payee }</td>
			</tr>
			<tr>
				<td>
					<div>受款人銀行</div>
					<div>Name of Beneficiary Bank</div>
				</td>					
				<td colspan="6">${cp.payeeBank }</td>
			</tr>
			<tr>
				<td>
					<div>受款人帳號</div>
					<div>Beneficiary's Account No</div>
				</td>					
				<td colspan="6">${cp.payeeAccount }</td>
			</tr>
			<tr>
				<td>
					<div>銀行交換代號</div>
					<div>Bank Clearing Code</div>
				</td>					
				<td colspan="6">${cp.bankClearingCode }</td>
			</tr>
			<tr>
				<td>
					<div>附言或指示</div>
					<div>Message or Instructions</div>
				</td>					
				<td colspan="6">${cp.message }</td>
			</tr>
			<tr>
				<td>
					<div>聯絡人</div>
					<div>Contact person</div>
				</td>					
				<td colspan="2">${cp.contact }</td>
				<td></td>
				<td>
					<div>電話</div>
					<div>Tel</div>
				</td>					
				<td colspan="2">${cp.contactTel }</td>
			</tr>
			<tr>
				<td colspan="7">
					<table width="100%">
						<tr>					
							<td width="15%">憑證日期<br/>Date</td>
							<td width="10%">項目<br/>Item</td>
							<td width="35%">費用說明<br/>Description</td>
							<td width="10%">金額<br/>Amount</td>
							<td width="10%">幣別<br/>Currency</td>
							<td width="10%">匯率<br/>Exchange Rate</td>
							<td width="10%">總計<br/>Total</td>
						</tr>							
						<c:forEach items="${receipts }" var="rc">						
						<tr>
							<td><fmt:formatDate value="${rc.costDate}" type="date" pattern="yyyy-MM-dd" /></td>
							<td>${rc.costItem }</td>
							<td>${rc.description }</td>
							<td align="center">
								<fmt:parseNumber value="${rc.amount}" var="amount"/>
								<fmt:formatNumber value="${amount}" pattern="###,###.##"/></td>
							<td>					
								<c:if test="${rc.currency == 'TWD' }">新臺幣(TWD)</c:if>
								<c:if test="${rc.currency == 'HKD' }">港幣(HKD)</c:if>
								<c:if test="${rc.currency == 'USD' }">美金(USD)</c:if>
								<c:if test="${rc.currency == 'CNY' }">人民幣(CNY)</c:if>
								<c:if test="${rc.currency == 'KRW' }">韓元(KRW)</c:if>
								<c:if test="${rc.currency == 'JPY' }">日圓(JPY)</c:if>												
							</td>
							<td align="center"><fmt:parseNumber integerOnly="${rc.rate % 1 == 0}" type="number" value="${rc.rate}" /></td>
							<td align="center"><fmt:formatNumber pattern="###,###.##" type="number" value="${rc.amount * rc.rate}" /></td>
						</tr>
						</c:forEach>
					</table>
				</td>
			</tr>						
			<tr>
				<td colspan="6" style="text-align: center;">總計Total</td>
				<td style="text-align: center;">							
					<div id="totalDiv"><fmt:formatNumber value="${cp.totalAmount }" type="currency" maxFractionDigits="0"/></div>
				</td>
			</tr>
			<c:if test="${empty param.action}">
			<tr>
				<td colspan="7">										
					<c:forEach items="${attachment}" var="a" varStatus="loop">
					<a href="${a.url}" target="_blank"><label>附件_${loop.count}</label></a>								
					</c:forEach>										
				</td>
			</tr>
			</c:if>
			<tr>
				<td colspan="7">
					<table width="100%">
						<tr>
							<c:if test="${not empty stamp['總經理']}">
							<td style="text-align: center;">總經理</td>
							</c:if>				
							<c:if test="${not empty stamp['營運主管']}">
							<td style="text-align: center;">營運主管</td>
							</c:if>
							<td style="text-align: center;">部門主管</td>												
							<td style="text-align: center;">申請人</td>
						</tr>
						<tr>
							<c:if test="${not empty stamp['總經理']}">
							<td style="text-align: center;">${stamp['總經理'].info}</td>							
							</c:if>
							<c:if test="${not empty stamp['營運主管']}">
							<td style="text-align: center;">${stamp['營運主管'].info}</td>							
							</c:if>
							<td style="text-align: center;">${stamp['部門主管'].info}${stamp['行政主管'].info}</td>			
							<td style="text-align: center;">${stamp['申請人'].info}</td>
						</tr>
					</table>
				</td>
			</tr>						
			<tr>
				<td colspan="7">請檢附已書名統一編號的發票或是收據</td>
			</tr>
		</table>
		<c:if test="${not empty param.action && fn:length(attachment) > 0}">
		<div style="page-break-before: always; ">&nbsp;</div>										
			<c:forEach items="${attachment}" var="a" varStatus="loop">
			<label>附件_${loop.count}</label><br/>
			<img style='height: 100%; width: 100%; object-fit: contain' alt="" src="${a.url}"/><br/>
			</c:forEach>														
		</c:if>			
	</div>	
</div>