<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/paymentForm/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${cp.id}" />
		<input type="hidden" name="workFlowId" value="${cp.workFlowId}" />
		<!-- 退件重送 -->
		<input type="hidden" name="serialNo" value="${cp.serialNo}" />
		<div class="table-responsive">
			
			<div style="text-align: center;"><h2 id="title"></h2></div>
			<div style="text-align: center;"><h3>請款單 Payment Request Form</h3></div>
			
			<table class="table">
				<tr>
					<td colspan="5" style="text-align: center;">請款者 Requester Profile</td>									
				</tr>
				<tr>
					<td>
						<select id="companyId" name="companyId">
                           	<c:forEach items="${sessionScope.company}" var="company">
                           	<option value="${company.id}"
                           		<c:if test="${cp.companyId == company.id}">selected</c:if>
                           		>${company.name}</option>
                           	</c:forEach>
                        </select>
					</td>
					<td class="td3">申請日期<br/>Date Requested</td>
					<td><fmt:formatDate value="${cp.requestDate}" type="date" pattern="yyyy-MM-dd" /></td>
					<td class="td3">部門<br/>Division/Department</td>
					<td>
						<span id="department">
						<c:if test="${not empty cp}">${cp.departmentName }</c:if>
						</span>
						<input type="hidden" id="requestDepartment" name="requestDepartment" value="${cp.requestDepartment}" />
						<select id="employeeId" name="employeeId" onchange="getEmployeeInfo(this.value)">							
			            <c:forEach items="${companyEmployee}" var="employee">
			                <option value="${employee.id}" <c:if test="${employee.id == (empty cp ? sessionScope.employee.id : cp.employeeId)}">selected="selected"</c:if>>${employee.name}</option>
			            </c:forEach>
			            </select>	
					</td>
				</tr>
				<tr>
					<td colspan="5" class="td3">請款資訊 Payment Information</td>					
				</tr>
				<tr>
					<td colspan="5" class="td3">
						付款方式 Payment Way：
						<input type="radio" name="paymentWay" value="1" <c:if test="${cp.paymentWay == 1 }">checked=checked</c:if>>現金
						<input type="radio" name="paymentWay" value="2" <c:if test="${cp.paymentWay == 2 }">checked=checked</c:if>>匯款
						<input type="radio" name="paymentWay" value="3" <c:if test="${cp.paymentWay == 3 }">checked=checked</c:if>>支票
						<input type="radio" name="paymentWay" value="4" <c:if test="${cp.paymentWay == 4 }">checked=checked</c:if>>沖預付
					</td>	
				</tr>
				<tr>
					<td class="td3">
						<div>出款日</div>
						<div>Remit Date</div>
					</td>					
					<td colspan="4">
						<fmt:formatDate var="remitDate" value="${cp.remitDate}" type="date" pattern="yyyy-MM-dd" />
						<select id="remitDate" name="remitDate">
                           	<c:forEach items="${remitDateItems}" var="d">
                           	<option value="${d}"
                           		<c:if test="${remitDate == d}">selected</c:if>
                           		>${d}</option>
                           	</c:forEach>
                        </select>						
					</td>
				</tr>
				<tr>
					<td class="td3">
						<div>受款人</div>
						<div>Name of Beneficiary</div>
					</td>					
					<td colspan="4"><input name="payee" id="payee" value="${cp.payee }"/></td>
				</tr>
				<tr>
					<td class="td3">
						<div>受款人銀行</div>
						<div>Name of Beneficiary Bank</div>
					</td>					
					<td colspan="4">						
                        <input name="payeeBank" id="payeeBank" size="50" value="${cp.payeeBank }"/>
                        <br>
                        <label for="twBank">快速選單</label>
                        <select id="twBank">
                          	<option value="">請選擇銀行</option>                          	
                        </select>
                        <select id="twBranch">
                          	<option value="">請選擇分行</option>                          
                        </select>
					</td>
				</tr>
				<tr>
					<td class="td3">
						<div>受款人帳號</div>
						<div>Beneficiary's Account No</div>
					</td>					
					<td colspan="4"><input name="payeeAccount" id="payeeAccount" value="${cp.payeeAccount }"/></td>
				</tr>
				<tr>
					<td class="td3">
						<div>銀行交換代號</div>
						<div>Bank Clearing Code</div>
					</td>					
					<td colspan="4"><input name="bankClearingCode" id="bankClearingCode" value="${cp.bankClearingCode }"/></td>
				</tr>
				<tr>
					<td class="td3">
						<div>附言或指示</div>
						<div>Message or Instructions</div>
					</td>					
					<td colspan="4"><input name="message" id="message" value="${cp.message }"/></td>
				</tr>
				<tr>
					<td class="td3">
						<div>聯絡人</div>
						<div>Contact person</div>
					</td>					
					<td><input name="contact" id="contact" value="${cp.contact }"/></td>
					<td></td>
					<td class="td3">
						<div>電話</div>
						<div>Tel</div>
					</td>					
					<td><input name="contactTel" id="contactTel" value="${cp.contactTel }"/></td>
				</tr>
				<tr>
					<td colspan="5">
						<table style="width: 100%">
							<tr>
								<td class="td2">憑證日期<br/>Date</td>
								<td class="td2">項目<br/>Item</td>
								<td class="td2">費用說明<br/>Description</td>
								<td class="td2">金額<br/>Amount</td>
								<td class="td2">幣別<br/>Currency</td>
								<td class="td2">匯率<br/>Exchange Rate</td>
								<td class="td2">總計<br/>Total</td>
								<td class="td2"><input type="button" id="addColumn" class="btn btn-danger" value="增加"></td>
							</tr>
							<tbody id="receiptData">
							<c:if test="${empty receipts }">
							<tr>
								<td>									
									<input type="text" value=""
											onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
											class="Wdate" name="costDate" id="costDate" size="12">
								</td>
								<td><input name="costItem" value=""/></td>
								<td><input name="description"/></td>
								<td><input type="number" name="amount" style="width: 120px;" onblur="doTotal()"/></td>
								<td>
									<select name="currency" onchange="fixedRate(this)">
										<option value="">請選擇</option>
										<option value="TWD">新臺幣(TWD)</option>
										<option value="HKD">港幣(HKD)</option>
										<option value="USD">美金(USD)</option>
										<option value="CNY">人民幣(CNY)</option>
										<option value="KRW">韓元(KRW)</option>
										<option value="JPY">日圓(JPY)</option>
									</select>
								</td>
								<td>
									<input type="number" style="width: 55px;" name="rate" onblur="doTotal()"/>
								</td>
								<td><div class="TWDollor1"></div></td>
							</tr>
							</c:if>							
							<c:if test="${not empty receipts }">
							<c:forEach items="${receipts }" var="rc">
							<tr>
								<td>									
									<input type="text" value="<fmt:formatDate value="${rc.costDate}" type="date" pattern="yyyy-MM-dd" />"
											onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
											class="Wdate" name="costDate" id="costDate" size="12">
								</td>
								<td><input name="costItem" value="${rc.costItem }"/></td>
								<td><input name="description" value="${rc.description }"/></td>
								<td>
									<fmt:parseNumber value="${rc.amount}" var="amount"/>
									<input type="number" name="amount" style="width: 120px;" 
										value="<fmt:formatNumber value="${amount}" pattern="#.##"/>" onblur="doTotal()"/>
								</td>
								<td>									
									<select name="currency" onchange="fixedRate(this)">
										<option value="">請選擇</option>
										<option value="TWD" <c:if test="${rc.currency == 'TWD' }">selected="selected"</c:if>>新臺幣(TWD)</option>
										<option value="HKD" <c:if test="${rc.currency == 'HKD' }">selected="selected"</c:if>>港幣(HKD)</option>
										<option value="USD" <c:if test="${rc.currency == 'USD' }">selected="selected"</c:if>>美金(USD)</option>
										<option value="CNY" <c:if test="${rc.currency == 'CNY' }">selected="selected"</c:if>>人民幣(CNY)</option>
										<option value="KRW" <c:if test="${rc.currency == 'KRW' }">selected="selected"</c:if>>韓元(KRW)</option>
										<option value="JPY" <c:if test="${rc.currency == 'JPY' }">selected="selected"</c:if>>日圓(JPY)</option>
									</select>
								</td>
								<td>
									<input type="number" style="width: 55px;" name="rate" 
										value="<fmt:parseNumber integerOnly="${rc.rate % 1 == 0}" type="number" value="${rc.rate}" />" onblur="doTotal()"/>
								</td>
								<td><div class="TWDollor1"></div></td>							
							</tr>
							</c:forEach>
							</c:if>
							</tbody>
						</table> 
					</td>
				</tr>
				<tr>
					<td colspan="4" style="text-align: center;" class="td2">總計Total</td>
					<td style="text-align: center;" class="td2">
						<input type="hidden" id="totalAmount" name="totalAmount" value="${cp.totalAmount }">
									
						<div id="TWDollor1Total"><fmt:formatNumber value="${cp.totalAmount }" currencySymbol="NT$" type="currency"/></div>
					</td>
				</tr>
				<tr>
					<td colspan="5">
						<table class="table">				
							<tr>					
								<td><label>附件</label></td>
								<td><input class="btn btn-success" type="button" value="增加" id="addAttachment"/></td>
							</tr>				
							<tbody id="attachmentBody">
							<c:if test="${empty cp}">
							<tr>					
								<td>
									<input type="hidden" name="url" value=""/>
									<input name="fileName" value="" size="50"/>									
									<input class="upload" type="button" value="上傳"/>
								</td>
								<td></td>					
							</tr>
							</c:if>
							<c:forEach items="${attachment}" var="a">
							<tr>					
								<td>
									<input type="hidden" name="url" value="${a.url}"/>
									<input name="fileName" value="${a.description}" size="50"/>									
									<input type="button" value="上傳" onclick="$('#fileBrowse').click();"/>
								</td>
								<td><input class="btn" type="button" value="刪除" onclick="deleteAttachment('${a.id}', '${cp.id}')"/></td>
							</tr>
							</c:forEach>
							</tbody>
						</table>
					</td>
				</tr>
			</table>
			<table class="table">
				<tr>
					<td colspan="2" style="text-align: center;" class="td1">部門主管</td>
					<td colspan="2" style="text-align: center;" class="td1">申請人</td>
				</tr>
				<tr>
					<td colspan="2"></td>
					<td colspan="2" style="text-align: center;">${empty cp.creatorName ? sessionScope.employee.name : cp.creatorName}</td>
				</tr>
				<tr>
					<td colspan="4">請檢附已書名統一編號的發票或是收據</td>
				</tr>
			</table>			
		</div>
	</form>	
	<input type="button" value="保存" id="save"/>
	<c:if test="${not empty cp }">
	<input type="button" value="刪除" onclick="deletePF(${cp.id})"/>
	</c:if>
	<input type="button" value="送審" id="submit"/>
	
	<form name="upload" id="fileUploadForm" enctype="multipart/form-data" method="post">		
		<div style="display: none">
			<input type="file" id="fileBrowse" name="file" style="border: 0;" />
		</div>
	</form>
</div>

<script type="text/javascript">

function doTotal() {
	var total = 0;	
	$('#receiptData').find('[name=amount]').each(function() {
		var cal = 0;
				
		var rate =  $(this).closest('td').next().next().find('[name=rate]').val();
		var currency = $(this).closest('td').next().find('[name=currency]').val();
		var amount = $(this).val();
		console.log("type1=" + rate + "," + currency + "," + amount);
		
		if(amount != "" && rate != "" && currency != "") {
			cal = (amount*rate).toFixed(currency == "TWD" || currency == "JPY" || currency == "KRW" ? 0 : 2);
			$(this).closest('td').next().next().next().find('.TWDollor1').html(cal);
			
			total += Math.round(cal);
			return;
		}					
	});
	
	$('#TWDollor1Total').html('NT$'+total);
	$('#totalAmount').val(total);
}

function twBank() {
	$.ajax({
		url : '${ctx}/bank/twBank',
		dataType : 'json',
		data : {},
		success : function(data) {
			$('#twBank').empty();							
			$('#twBank').append('<option value="">請選擇銀行</option>');
			
			for(var i in data) {
				$('#twBank').append(
					'<option value="' + data[i].code+'">'
					+ data[i].code + (data[i].shortName == '' ? data[i].fullName : data[i].shortName)
					+ '</option>');
			}
		},
		error : function() {
			alert('網絡連接失敗，請聯繫管理員');
		}
	});
}

$(function() {		
	twBank();
	
	$('#twBank').change(function() {
		var bankCode = $(this).val();
		
		if(bankCode == '') {
			$('#twBranch').empty();					
			$('#twBranch').append('<option value="">請選擇分行</option>');
		}
		else {
			$.ajax({
				url : '${ctx}/bank/twBranch',
				dataType : 'json',
				data : {
					'code' : bankCode
				},
				success : function(data) {
					$('#twBranch').empty();							
					$('#twBranch').append('<option value="">請選擇分行</option>');
					
					for(var i in data) {
						$('#twBranch').append(
							'<option value="' + data[i].code + ";" + data[i].fullName +'">'
							+ (data[i].shortName == '' ? data[i].fullName : data[i].shortName)
							+ '</option>');
					}
				},
				error : function() {
					alert('網絡連接失敗，請聯繫管理員');
				}
			});
		}
	});
	
	$('#twBranch').change(function() {
		var val = $(this).val().split(";");
		var branchId = val[0];
		var branchName = val[1];
		$("#payeeBank").val(branchName);
		$("#bankClearingCode").val(branchId);
	});
	
	$('#save').click(function() {		
		if(checkValidate()){
			$("#editForm").ajaxSubmit({
	            success: function(e) {
	            	if(e.code == "0000") {
	            		alert(e.message), $("#myModal").modal("toggle");
		                $('#search').click();
	            	}
	            	else {
	            		alert(e.message);
	            	}
	            },
	            error: function() {
	                alert("網路連接失敗，請聯繫管理員");
	            }
	        });
		}		
	});
	
	$('#submit').click(function() {
		if(checkValidate()){
			$('#editForm').attr('action', '${ctx}/paymentForm/submit');
			$("#editForm").ajaxSubmit({
	            success: function(e) {
	                alert(e.message), $("#myModal").modal("toggle");
	                $('#search').click();
	            },
	            error: function() {
	                alert("網路連接失敗，請聯繫管理員");
	            }
	        });			
		}
	});
	
	$('#addColumn').click(function() {
		var currency = $("[name=currency]").first().val();
		var rate = $("[name=rate]").first().val();
		
		var count = 0;
		var html = "";
		html = '<tr>';
        html += '<td><input type="text" value="" onfocus="WdatePicker({startDate:';
        html += "'%y-%M-%d'";
        html += ", dateFmt:";
        html += "'yyyy-MM-dd'";
        html += '})" class="Wdate" name="costDate" size="12" id="costDate'+count+'"></td>';
        html += '<td><input name="costItem"/></td>';
        html += '<td><input name="description"/></td>';
        html += '<td><input type="number" name="amount" style="width: 120px;" onblur="doTotal()"/></td>';
        
        html += '<td>';
		html += '<select name="currency" onchange="fixedRate(this)">';
		html += '<option value="">請選擇</option>';
		html += '<option value="TWD"' + (currency == 'TWD' ? 'selected'  : '') + '>新臺幣(TWD)</option>';
		html += '<option value="HKD"' + (currency == 'HKD' ? 'selected'  : '') + '>港幣(HKD)</option>';
		html += '<option value="USD"' + (currency == 'USD' ? 'selected'  : '') + '>美金(USD)</option>';
		html += '<option value="CNY"' + (currency == 'CNY' ? 'selected'  : '') + '>人民幣(CNY)</option>';
		html += '<option value="KRW"' + (currency == 'KRW' ? 'selected'  : '') + '>韓元(KRW)</option>';
		html += '<option value="JPY"' + (currency == 'JPY' ? 'selected'  : '') + '>日圓(JPY)</option>';
		html += '</select>';
		html += '</td>';
		html += '<td>';
		html += '<input type="number" style="width: 55px;" value="' + rate + '" name="rate" onblur="doTotal()"/>';
		html += '</td>';
		html += '<td><div class="TWDollor1"></div></td>';
        html += '<td><input class="btn delete" type="button" value="刪除">';
        html += '</tr>';
        count++;        
        
        $('#receiptData').append(html);
	});
	
	$('#receiptData').on('click', '.delete', function(){
		$(this).parent().parent().remove();
		doTotal();
	});		
	
	$('#companyId').change(function() {
		var cid = $(this).val();
		var name = $(this).find("option:selected").text();
		$("#title").text(name);
		// department
		getUserInfo(cid);
	});
	
	$('#addAttachment').click(function(){
		var html = '';
		html += '<tr>';
		html += '<td><input type="hidden" name="url" value=""><input name="fileName" value="" size="50">';		
		html += '<input class="upload" type="button" value="上傳"></td>';
		html += '<td><input class="btn delete" type="button" value="刪除"></td>';
		html += '</tr>';
		
		$('#attachmentBody').append(html);
	});
	
	var target;  // fileName
	$('#attachmentBody').on('click', '.upload', function(){
		target = $(this).prev();
		$('#fileBrowse').click();
	});
	
	$('#fileUploadForm').on('submit', (function(e) {		
		e.preventDefault();
		
		var formData = new FormData(this);
		$.ajax({
			async : false,
			type : 'POST',
			url : '${ctx}/paymentForm/upload.do',
			data : formData,
			cache : false,
			contentType : false,
			processData : false,
			success : function(data) {
				if (data == "overSize") {
					alert("檔案超過1MB請更換圖片");
					$("#fileBrowse").val("");
					return;
				}				
				//$("#url").val(data.url);
				target.val(data.description);
				target.prev().val(data.url);
			},
			error : function(data) {
				alert("系統錯誤，請稍後再試");
			}
		});
	}));
	
	$("#fileBrowse").on("change", function() {
		if (this.files[0].size >= 1048576) {
			alert("檔案超過1MB請更換檔案");
			return;
		}	
		$("#fileUploadForm").submit();
	});		
	
	$('#attachmentBody').on('click', '.delete', function(){
		$(this).parent().parent().remove();
	});
	
	// default title
	$("#title").text($('#companyId').find("option:selected").text());
	
	if($("#department").text().trim() == "") {
		getUserInfo($('#companyId').val());
	}
	
	doTotal();
});

function checkValidate(){
	var pass = true;
	var html = "<font class='warning' color='red'>請輸入此欄位</font>";
	
	if($('#paymentDate').val() == ''){
		pass = false;
		$('input[name=paymentDate]').parent().find('.warning').remove();
		$('input[name=paymentDate]').parent().append(html);		
	}else{
		$('input[name=paymentDate]').parent().find('.warning').remove();
	}
	
	if(!$('input[name=paymentWay]').is(':checked')){
		pass = false;
		$('input[name=paymentWay]').parent().find('.warning').remove();
		$('input[name=paymentWay]').parent().append(html);		
	}else{
		$('input[name=paymentWay]').parent().find('.warning').remove();
	}
	
	if($('#remitDate').val() == ''){
		pass = false;
		$('input[name=remitDate]').parent().find('.warning').remove();
		$('input[name=remitDate]').parent().append(html);		
	}else{
		$('input[name=remitDate]').parent().find('.warning').remove();
	}
	
	if($('#payee').val() == ''){
		pass = false;
		$('#payee').parent().find('.warning').remove();
		$('#payee').parent().append(html);		
	}else{
		$('#payee').parent().find('.warning').remove();
	}
	
	if($('#payeeBank').val().trim() == ''){
		pass = false;
		$('#payeeBank').parent().find('.warning').remove();
		$('#payeeBank').parent().append(html);		
	}else{
		// HK validate
		var company = "${sessionScope.employee.companyName}";
		var isHK = company.search("香港") >= 0;
		var pattern = /(\w|\s)+/;
		
		if(isHK && !pattern.test($('#payeeBank').val())) {
			pass = false;
			$('#payeeBank').parent().find('.warning').remove();
			$('#payeeBank').parent().append("欄位內容英數限定");
		}
		else {
			$('#payeeBank').parent().find('.warning').remove();
		}
	}
	
	if($('#payeeAccount').val() == ''){
		pass = false;
		$('#payeeAccount').parent().find('.warning').remove();
		$('#payeeAccount').parent().append(html);		
	}else{
		$('#payeeAccount').parent().find('.warning').remove();
	}
	
	$('#receiptData').find('input[name=costDate]').each(function(){	
		if($(this).val() == ''){
			pass = false;
			$(this).parent().find('.warning').remove();
			$(this).parent().append(html);		
		}else{
			$(this).parent().find('.warning').remove();
		}
	});
	
	$('#receiptData').find('input[name=costItem]').each(function(){	
		if($(this).val() == ''){
			pass = false;
			$(this).parent().find('.warning').remove();
			$(this).parent().append(html);		
		}else{
			$(this).parent().find('.warning').remove();
		}
	});		
	
	$('#receiptData').find('input[name=amount]').each(function(){	
		if($(this).val() == ''){
			pass = false;
			$(this).parent().find('.warning').remove();
			$(this).parent().append(html);		
		}else{
			$(this).parent().find('.warning').remove();
		}
	});
	
	$('#receiptData').find('[name=currency]').each(function(){	
		if($(this).val() == ''){
			pass = false;
			$(this).parent().find('.warning').remove();
			$(this).parent().append(html);		
		}else{
			$(this).parent().find('.warning').remove();
		}
	});
	
	$('#receiptData').find('input[name=rate]').each(function(){	
		if($(this).val() == ''){
			pass = false;
			$(this).parent().find('.warning').remove();
			$(this).parent().append(html);		
		}else{
			$(this).parent().find('.warning').remove();
		}
	});
	
	if(!pass){
		alert("表單未填寫完整!");
	}
	
	doTotal();
	return pass;
}

function getUserInfo(cid) {
	$.ajax({
		url : '${ctx}/employee/userInfo',
		dataType : 'json',
		data : {
			'id' : cid
		},
		success : function(data) {
			console.log(data);
			var dept = data.split(",");
			$("#department").text(dept[1]);
			$("#requestDepartment").val(dept[0]);
			
			companyEmployee(cid, dept[4]);
		},
		error : function() {
			alert('網絡連接失敗，請聯繫管理員');
		}
	});
}

function fixedRate(obj) {
	var cid = $(obj).val();
	
	if(cid == "TWD") {
		$(obj).closest('td').next().find('[name=rate]').val("1");
	}
	
	doTotal();
}

function getEmployeeInfo(eid) {
	$.ajax({
		url : '${ctx}/employee/employeeInfo',
		dataType : 'json',
		data : {
			'id' : eid
		},
		success : function(data) {
			var dept = data.split(",");			
			$("#department").text(dept[1]);
			$("#requestDepartment").val(dept[0]);			
		},
		error : function() {
			alert('網絡連接失敗，請聯繫管理員');
		}
	});
}

function companyEmployee(cid, eid) {
	$.ajax({
		url : '${ctx}/employee/companyEmployee',
		dataType : 'json',
		data : {
			'id' : cid
		},
		success : function(data) {
			$('#employeeId').empty();
			
			for(var i in data) {
				$('#employeeId').append(
					'<option value="' + data[i].id + '"' + (data[i].id == eid ? 'selected' : '') + '>'
					+ data[i].name
					+ '</option>');
			}
		},
		error : function() {
			alert('網絡連接失敗，請聯繫管理員');
		}
	});
}
</script>