<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	
	<table class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>				
				<th>表單編號</th>				
				<th>公司</th>
				<th>部門</th>
				<th>申請日期</th>
				<th>申請人</th>
				<th>付款方式</th>
				<th>出款日</th>
				<th>受款人</th>				
				<th>受款人銀行帳號</th>
				<th>憑證總金額</th>	
				<th>狀態</th>	
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
				<tr>
					<td>${o.serialNo}</td>					
					<td>${o.companyName}</td>
					<td>${o.departmentName}</td>
					<td><fmt:formatDate value="${o.requestDate}" type="date" pattern="yyyy-MM-dd" /></td>
					<td>${o.creatorName}</td>
					<td>
						<c:choose>
							<c:when test="${o.paymentWay == 1 }">現金</c:when>
							<c:when test="${o.paymentWay == 2 }">匯款</c:when>
							<c:when test="${o.paymentWay == 3 }">支票</c:when>
							<c:when test="${o.paymentWay == 4 }">沖預付</c:when>
						</c:choose>					
					</td>					
					<td>
						<span <c:if test="${o.status == 0 && o.remitted == 1}">style="color: red;"</c:if>>
							<fmt:formatDate value="${o.remitDate}" type="date" pattern="yyyy-MM-dd" />
						</span>
					</td>
					<td>${o.payee}</td>
					<td>${o.payeeBank}<br>${o.payeeAccount}</td>					
					<td><fmt:formatNumber value="${o.totalAmount }" currencySymbol="" type="currency"/></td>					
					<td>
						<c:choose>
						    <c:when test="${o.status == -1}">編輯中</c:when>
						    <c:when test="${o.status == 0}">正常結束</c:when>
						    <c:when test="${o.status == 1}">執行中</c:when>
						    <c:when test="${o.status == 8}">註銷</c:when>
						    <c:when test="${o.status == 9}">退件</c:when>
						    <c:otherwise>未定義(o.status)</c:otherwise>
						</c:choose>
					</td>
					<td>
						<!-- 出款 -->
						<c:if test="${o.status == 0 && o.remitted == 1 && fn:contains(formRole, '會計')}">
							<a href="javascript:;" onclick="confirmRemitDate(${o.id})">出款</a>
							<a href="javascript:;" onclick="updateRemitDate(${o.id})">出款日</a>
						</c:if>
						<!-- 審核歷程 -->									
						<c:if test="${o.status >= 0}">
							<a href="javascript:;" onclick="history(${o.workFlowId})">審核歷程</a>
						</c:if>
						<c:if test="${o.status == -1 && o.creator == sessionScope.loginUser.id }">
						<a href="javascript:;" onclick="edit(${o.id})">編輯</a>
						</c:if>						
						<c:if test="${o.status == 1 && o.creator == sessionScope.loginUser.id}">
						<a href="javascript:;" onclick="terminate(${o.workFlowId})">撤銷</a>
						</c:if>
						<a href="javascript:;" onclick="get(${o.id})">檢視</a>
						<c:if test="${o.status == 0}">
						<a href="javascript:;" onclick="pdf(${o.id})">PDF</a>
						</c:if>																		
					</td>
				</tr>		
			</c:forEach>
		</tbody>
	</table>
</div>
<my:pagination page="${page}" />