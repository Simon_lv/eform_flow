<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
<head>
<title>8888play ERP</title>
</head>
<body>
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div class="modal-body" id="myModalBody"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="modalSubmit">關閉</button>
				</div>
			</div>
		</div>
	</div>
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				表單管理 <small>測試單管理</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="${ctx}/index"><i class="fa fa-dashboard"></i>8888play ERP</a></li>
				<li class="active">測試單管理</li>
			</ol>
		</section>
		<!-- Main content -->
		<div class="container-fluid">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body">
						<form action="${ctx}/qcFormMgr/list" id="listForm" method="post">
							<input type="hidden" id="pageNo" name="pageNo" value="" />
							<div class="table-responsive">						
								<div class="form-group col-sm-12 col-md-4 col-lg-3">
	                            	<label for="inputid">測試單:</label>
	                            	<select class="form-control" name="id" id="id">
	                            	<option value="">全部</option>
	                            	<c:forEach items="${formSchemas }" var="fs">
	                            	<option value="${fs.id }">${fs.formName }</option>
	                            	</c:forEach>
	                            	</select>
	                            </div>	                                                      
	                            <hr>
								<div class="btn-group col-sm-12 col-md-12 col-lg-6" role="group">
		                        	<input class="btn btn-primary" type="button" id="search" value="搜尋"/>
		                            <input class="btn btn-success" type="button" id="add" value="新增"/>
		                            <!--  
		                            <input class="btn btn-danger" type="button" id="export" value="導出列表"/>
		                            -->		                            
		                        </div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div id="listContainer"></div>
		</div>
	</aside>

	<script type="text/javascript" src="${ctx }/js/jquery.form.js"></script>
	<script type="text/javascript">
		$(function() {			
						
			$("#add").click(function() {
				$(".modal-dialog").css("width", "900px"), $(".modal-dialog").css(
						"maxheight", "800px"), $("#myModal").modal("toggle"), 
						$("#myModalLabel").html("編輯"), $("#myModalBody").html(
						"<h3>正在打開。。。</h3>"), $.get("/qcFormMgr/edit?id=", function(e) {
							$("#myModalBody").html(e);
						});
			});

			$('#search').click(function() {
				$('#pageNo').val("1");
				$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
				$('#listForm').ajaxSubmit({
					success : function(data) {
						$('#listContainer').html(data);
					}
				});
			});
			/*
			$('#export').click(function() {
				var temp = $('#listForm').attr('action');
				$('#listForm').attr('action', '${ctx}/qcFormMgr/export');
				$('#listForm').submit();
				$('#listForm').attr('action', temp);
			});
			*/
			
			$('#modalSubmit').click(function() {
				$('#myModal').modal('toggle');
				$('#search').click();
			});
			
			$('#myModalBody').on('click', '.delete', function(){
				$(this).parent().parent().remove();
			});
			
			$('#myModalBody').on('click', '#saveSchema', function(){				
				$("#schemaForm").ajaxSubmit({
		            success: function(e) {		            	
		                alert(e.message);
		                
		                $.get("/qcFormMgr/edit?id="+e.id, function(e) {
							$("#myModalBody").html(e);
						});
		            },
		            error: function() {
		                alert("網路連接失敗，請聯繫管理員");
		            }
		        });
			});
			
			$('#myModalBody').on('click', '#saveTitle', function(){				
				$("#titleForm").ajaxSubmit({
		            success: function(e) {		            	
		                alert(e.message);
		                
		                $.get("/qcFormMgr/edit?id="+e.id, function(e) {
							$("#myModalBody").html(e);
						});
		            },
		            error: function() {
		                alert("網路連接失敗，請聯繫管理員");
		            }
		        });
			});
			
			$('#myModalBody').on('click', '#saveGroup', function(){				
				$("#groupForm").ajaxSubmit({
		            success: function(e) {		            	
		                alert(e.message);
		                
		                $.get("/qcFormMgr/edit?id="+e.id, function(e) {
							$("#myModalBody").html(e);
						});
		            },
		            error: function() {
		                alert("網路連接失敗，請聯繫管理員");
		            }
		        });
			});	
			
			$('#myModalBody').on('click', '#saveField', function(){				
				$("#fieldForm").ajaxSubmit({
		            success: function(e) {		            	
		                alert(e.message);
		                
		                $.get("/qcFormMgr/edit?id="+e.id, function(e) {
							$("#myModalBody").html(e);
						});
		            },
		            error: function() {
		                alert("網路連接失敗，請聯繫管理員");
		            }
		        });
			});			
			
		});
		
		function addColumn(bodyId, id){	
			var html = "";
			
			if(bodyId == 'titleBody'){
				html += "<tr>";
				html += "<td></td>";
				html += "<td><input type='hidden' name='id' value=''/><input name='fieldLabel' value='' size='15'/></td>";					
				html += "<td><input type='number' name='fieldOrder' value='' style='width: 50px;'/></td>";
				html += "<td><input class='btn delete' type='button' value='刪除'/></td>";
				html += "</tr>";
				$('#'+bodyId).append(html);
			}else if(bodyId == 'groupBody'){
				html += "<tr>";
				html += "<td></td>";
				html += "<td><input type='hidden' name='id' value=''/><input name='groupLabel' value='' size='50'/></td>";									
				html += "<td><input class='btn delete' type='button' value='刪除'/></td>";
				html += "</tr>";
				$('#'+bodyId).append(html);
			}else if(bodyId == 'fieldBody'){				
				$.get("/qcFormMgr/addField?id=" + id, function(e) {					
					$('#'+bodyId).append(e);
				});		
			}			
		}
		
		function deleteData(target, editId, id){
			if(target == 'sub'){
				$.get("/qcFormMgr/deleteTitle?id="+editId, function(e){
					$.get("/qcFormMgr/edit?id="+id, function(e) {
						$("#myModalBody").html(e);
					});
				});
			}else if(target == 'group'){
				$.get("/qcFormMgr/deleteGroup?id="+editId, function(e){
					$.get("/qcFormMgr/edit?id="+id, function(e) {
						$("#myModalBody").html(e);
					});
				});
			}else if(target == 'detail'){
				$.get("/qcFormMgr/deleteDetail?id="+editId, function(e){
					$.get("/qcFormMgr/edit?id="+id, function(e) {
						$("#myModalBody").html(e);
					});
				});
			}
			
			
		}
		
		function toPage(pageNo) {
			if (!pageNo || pageNo <= 0) {
				return;
			}
			
			$('#pageNo').val(pageNo);
			$('#listForm').ajaxSubmit({
				success : function(data) {
					$('#listContainer').html(data);
				},
				error : function() {
					alert('連接網路失敗');
				}
			});
			$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
		}
				
		function edit(id) {			
			$(".modal-dialog").css("width", "900px"), $(".modal-dialog").css(
				"maxheight", "800px"), $("#myModal").modal("toggle"), 
				$("#myModalLabel").html("編輯"), $("#myModalBody").html(
				"<h3>正在打開。。。</h3>"), $.get("/qcFormMgr/edit?id=" + id, function(e) {
					$("#myModalBody").html(e);
				});
		}		
		
		function get(id) {			
			$(".modal-dialog").css("width", "900px"), $(".modal-dialog").css(
				"maxheight", "800px"), $("#myModal").modal("toggle"), 
				$("#myModalLabel").html("檢視"), $("#myModalBody").html(
				"<h3>正在打開。。。</h3>"), $.get("/qcFormMgr/get?id=" + id, function(e) {
					$("#myModalBody").html(e);
				});
		}
	</script>
</body>
</html>