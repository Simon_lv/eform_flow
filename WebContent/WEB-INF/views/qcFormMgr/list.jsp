<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	
	<table
		class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>
				<th>ID</th>								
				<th>測試單名稱</th>				
				<th>測試單代碼</th>
				<th>流程階段</th>
				<th>版本</th>
				<th>建立人</th>
				<th>建立時間</th>
				<th></th>				
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
				<tr>
					<td>${o.id }</td>
					<td>${o.formName}</td>					
					<td>${o.formCode }</td>
					<td>
						<c:choose>
						<c:when test="${o.flowType == 1 }">一階</c:when>
						<c:when test="${o.flowType == 2 }">二階</c:when>
						</c:choose>
					</td>
					<td>${o.version }</td>
					<td>${o.creatorName }</td>
					<td><fmt:formatDate value="${o.createTime}" type="date" pattern="yyyy-MM-dd hh:mm:dd" /></td>
					<td>						
						<a href="javascript:;" onclick="edit('${o.id}')">編輯</a>
						<a href="javascript:;" onclick="get('${o.id}')">檢視</a>							
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<my:pagination page="${page}" />