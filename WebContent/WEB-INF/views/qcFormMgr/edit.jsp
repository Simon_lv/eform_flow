<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/qcFormMgr/saveSchema" method="post" id="schemaForm">
		<input type="hidden" name="id" value="${schema.id}" />
				
		<div class="table-responsive">
			<table class="table">
				<tr>
					<th>測試單名稱</th>
					<th>測試單代碼</th>
					<th>流程階段</th>
				</tr>
				<tr>
					<td><input name="formName" value="${schema.formName }"/></td>
					<td><input name="formCode" value="${schema.formCode }"/></td>
					<td>
						<select name="flowType">
						<option value="1" <c:if test="${schema.flowType == 1 }">selected="selected"</c:if>>一階</option>
						<option value="2" <c:if test="${schema.flowType == 2 }">selected="selected"</c:if>>二階</option>
						</select>
					</td>
				</tr>
			</table>
			<div style=" text-align: right;"><input class="btn btn-danger" type="button" id="saveSchema" value="存檔"/></div>
		</div>
	</form>
	
	<c:if test="${not empty schema }">	
		<div class="table-responsive">		
			<table class="table">			
				<tr>
					<td style="vertical-align: text-top;">
						<form action="${ctx}/qcFormMgr/saveTitle" method="post" id="titleForm">
						<input type="hidden" name="schemaId" value="${schema.id}" />
						<input type="hidden" name="schemaVersion" value="${schema.version }"/>
						<input class="btn btn-success" type="button" value="增加" onclick="addColumn('titleBody')"/>
						<div style="height: 300px; overflow-y: auto;">
							<table class="table">
								<tr>
									<th></th>
									<th>側標題</th>
									<th>排列順序</th>
									<th></th>
								</tr>
									
								<tbody id="titleBody">
								<c:if test="${empty titleField }">
								<tr>
									<td></td>
									<td>
										<input type="hidden" name="id" value=""/>
										<input name="fieldLabel" value="" size="15"/>
									</td>					
									<td><input type="number" name="fieldOrder" value="" style="width: 50px;"/></td>
									<td></td>
								</tr>
								</c:if>
								
								<c:forEach items="${titleField }" var="tf" varStatus="loop">
								<tr>
									<td>${loop.index+1 }</td>
									<td>
										<input type="hidden" name="id" value="${tf.id }"/>
										<input name="fieldLabel" value="${tf.fieldLabel }" size="15"/>
									</td>					
									<td><input type="number" name="fieldOrder" value="${tf.fieldOrder }" style="width: 50px;"/></td>
									<td><input class="btn" type="button" value="刪除" onclick="deleteData('sub', '${tf.id }', '${schema.id}', '${schema.id}')"/></td>
								</tr>
								</c:forEach>
								</tbody>
									
							</table>							
						</div>
						<div style="text-align: right;"><input class="btn btn-danger" type="button" id="saveTitle" value="存檔"/></div>
						</form>
					</td>
					<td style="vertical-align: text-top;">
						<form action="${ctx}/qcFormMgr/saveDetail" method="post" id="groupForm">
						<input type="hidden" name="schemaId" value="${schema.id}" />
						<input type="hidden" name="schemaVersion" value="${schema.version }"/>
						<input class="btn btn-success" type="button" value="增加" onclick="addColumn('groupBody')"/>
						<div style="height: 300px; overflow-y: auto;">
							<table class="table">
								<tr>
									<th></th>
									<th>大標題</th>									
									<th></th>
								</tr>
																
								<tbody id="groupBody">
								
								<c:if test="${empty groupLabels }">
								<tr>
									<td></td>
									<td>
										<input type="hidden" name="id" value=""/>
										<input name="groupLabel" value="" size="50"/>
									</td>									
									<td></td>
								</tr>
								</c:if>
								
								<c:forEach items="${groupLabels }" var="gls" varStatus="loop">
								<tr>
									<td>${loop.index+1 }</td>
									<td>
										<input type="hidden" name="id" value="${gls.id }"/>
										<input name="groupLabel" value="${gls.groupLabel }" size="50"/>
									</td>									
									<td><input class="btn" type="button" value="刪除" onclick="deleteData('group', '${gls.id }', '${schema.id}')"/></td>
								</tr>
								</c:forEach>
								</tbody>
								
							</table>
						</div>
						<div style="text-align: right;"><input class="btn btn-danger" type="button" id="saveGroup" value="存檔"/></div>
						</form>
					</td>
				</tr>
			</table>
			
			<form action="${ctx}/qcFormMgr/saveDetail" method="post" id="fieldForm">
			<input type="hidden" name="schemaId" value="${schema.id}" />
			<input type="hidden" name="schemaVersion" value="${schema.version }"/>
			<input class="btn btn-success" type="button" value="增加" onclick="addColumn('fieldBody', ${schema.id })"/>
			<div style="height: 300px; overflow-y: auto;">			
				<table class="table">
					<tr>
						<td></td>
						<th>大標題</th>
						<th>小標題</th>
						<th>排列順序</th>
						<th></th>
					</tr>
					
					<tbody id="fieldBody" >
					
					<c:if test="${empty fieldCount }">
					<tr>
						<td></td>				
						<td>
							<input type="hidden" name="id" value=""/>
							<select name="groupLabel">
							<option value="">請選擇</option>
							<c:forEach items="${groupLabels }" var="gls">
							<option value="">${gls.groupLabel }</option>
							</c:forEach>
							</select>
						</td>
						<td><input name="fieldLabel" value="${fieldLabels[count].fieldLabel }" size="60"/></td>
						<td><input type="number" name="fieldOrder" value="${fieldLabels[count].fieldOrder }" style="width: 50px;"/></td>
						<td></td>								
					</tr>
					</c:if>
					
					<c:set var="count" value="0"/>
					<c:if test="${fn:length(groupLabels) > 0 }">
						<c:forEach begin="0" end="${fn:length(groupLabels)-1 }" varStatus="loop">
							<c:forEach begin="1" end="${fieldCount[loop.index] }">
							<tr>
								<td>${count+1 }</td>				
								<td>
									<input type="hidden" name="id" value="${fieldLabels[count].id }"/>
									<select name="groupLabel">
									<option value="">請選擇</option>
									<c:forEach items="${groupLabels }" var="gls">
									<option value="${gls.groupLabel }" <c:if test="${fieldLabels[count].groupLabel == gls.groupLabel }">selected="selected"</c:if>>${gls.groupLabel }</option>
									</c:forEach>
									</select>
								</td>
								<td><input name="fieldLabel" value="${fieldLabels[count].fieldLabel }" size="60"/></td>
								<td><input type="number" name="fieldOrder" value="${fieldLabels[count].fieldOrder }" style="width: 50px;"/></td>
								<td><input class="btn" type="button" value="刪除" onclick="deleteData('detail', '${fieldLabels[count].id }', '${schema.id}')"/></td>								
							</tr>
							<c:set var="count" value="${count+1 }"/>
							</c:forEach>
						</c:forEach>
					</c:if>
					<c:if test="${fn:length(groupLabels) == 0 }">
						<c:forEach items="${fieldCount }" var="fc" varStatus="loop">
							<c:forEach begin="1" end="${fc }"/>
							<tr>
								<td>${count+1 }</td>				
								<td>
									<input type="hidden" name="id" value="${fieldLabels[count].id }"/>									
									<select name="groupLabel">
									<option value="">請選擇</option>
									<c:forEach items="${groupLabels }" var="gls">
									<option value="${gls.groupLabel }" <c:if test="${fieldLabels[count].groupLabel == gls.groupLabel }">selected="selected"</c:if>>${gls.groupLabel }</option>
									</c:forEach>
									</select>
								</td>
								<td><input name="fieldLabel" value="${fieldLabels[count].fieldLabel }" size="60"/></td>
								<td><input type="number" name="fieldOrder" value="${fieldLabels[count].fieldOrder }" style="width: 50px;"/></td>
								<td><input class="btn" type="button" value="刪除" onclick="deleteData('detail', '${fieldLabels[count].id }', '${schema.id}')"/></td>
								
							</tr>
							<c:set var="count" value="${count+1 }"/>
						</c:forEach>
					</c:if>
					</tbody>
					
				</table>					
			</div>
			<div style="text-align: right;"><input class="btn btn-danger" type="button" id="saveField" value="存檔"/></div>
			</form>					
		</div>
	</c:if>
</div>