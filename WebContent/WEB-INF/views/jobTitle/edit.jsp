<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/jobTitle/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${o.id}" />
		<div class="table-responsive">
			<table class="table">
				<tr>
					<td>公司</td>
					<td>
						<select name="companyId">
						<c:forEach items="${company}" var="cp">
						<option value="${cp.id }" <c:if test="${cp.id == o.companyId }">selected="selected"</c:if>>${cp.name }</option>
						</c:forEach>						
						</select>
					</td>
				</tr>
				<tr>
					<td>職稱編號：</td>
					<td><input name="titleNo" value="${o.titleNo}" /></td>
				</tr>
				<tr>
					<td>職稱：</td>
					<td><input name="name" value="${o.name}" /></td>
				</tr>
				<tr>
					<td>職級：</td>
					<td><input name="level" value="${o.level}" type="number" /></td>
				</tr>
				<tr>
					<td>職等：</td>
					<td><input name="grade" value="${o.grade}" type="number" /></td>
				</tr>					
			</table>
		</div>
	</form>
	<input type="button" value="保存" id="save" />
</div>

<script type="text/javascript">
$(function(){
	$('#save').click(function() {
		if($("input[name='titleNo']").val() == '') {
			alert("請輸入職稱編號");
			return;
		}
		
		if($("input[name='name']").val() == '') {
			alert("請輸入職稱");
			return;
		}
		
		if($("select[name='level']").val() == '') {
			alert("請輸入職級");
			return;
		}
		
		if($("select[name='grade']").val() == '') {
			alert("請輸入職等");
			return;
		}
		
		$("#editForm").ajaxSubmit({
            success: function(e) {
                alert(e.message);
                if("0000" == e.code) {
                	$("#myModal").modal("toggle");
                	$('#search').click();
                }
            },
            error: function() {
                alert("網路連接失敗，請聯繫管理員");
            }
        })
	});
});
</script>