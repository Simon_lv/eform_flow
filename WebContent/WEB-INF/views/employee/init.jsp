<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
<head>
<title>8888play ERP</title>
</head>
<body>
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div class="modal-body" id="myModalBody"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="modalSubmit">關閉</button>
				</div>
			</div>
		</div>
	</div>
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				基本資料 <small>員工資料</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="${ctx}/index"><i class="fa fa-dashboard"></i>8888play ERP</a></li>
				<li class="active">員工資料</li>
			</ol>
		</section>
		<!-- Main content -->
		<div class="container-fluid">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body">
						<form action="${ctx}/employee/list" id="listForm" method="post">
							<input type="hidden" id="pageNo" name="pageNo" value="" />
							<div class="table-responsive">
								<table class="table">
									<tr>
										<td>員工編號：</td>
										<td><input name="employeeNo"></td>
										<td>員工姓名：</td>
										<td><input name="name"></td>
										<td>手機：</td>
										<td><input name="mobile"></td>
									</tr>
									<tr>	
										<td>職稱：</td>
										<td>
											<select id="jobTitleId" name="jobTitleId">
												<option value="">全部</option>												
											</select>
										</td>
										<td>部門：</td>
										<td>
											<select id="departmentId" name="departmentId">
												<option value="">全部</option>												
											</select>
										</td>
										<td>公司：</td>
										<td>
											<select name="companyId" onchange="getOrganigram(this.value, 'listForm')">
												<option value="">全部</option>
												<c:forEach items="${allCompany}" var="a">
												<option value="${a.id}"> ${a.name}</option>
												</c:forEach>
											</select>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<input type="button" id="search" value="搜尋">
											<input id="add" type="button" value="新增" />
											<input type="button" id="export" value="導出列表">
											<c:if test="${loginUser.loginAccount == 'admin'}">
											<input id="clone" type="button" value="複製" />
											</c:if>
										</td>										
									</tr>
								</table>
							</div>
						</form>
						<form action="${ctx}/employee/importExcel" id="importExcelForm" method="post" enctype="multipart/form-data">
							<div class="table-responsive">
								<table class="table">
									<tr>
										<td>
											Excel匯入：工作表名稱：人事資料(更新)<br>
											<span style="color: red;">匯入後，用戶權限請由管理員操作</span>
											<input type="file" name="excelFile" />
											<input id="import" type="button" value="上傳" />
										</td>
									</tr>									
								</table>
							</div>
						</form>						
					</div>
				</div>
			</div>
			<div id="listContainer"></div>
		</div>
	</aside>

	<script type="text/javascript" src="${ctx }/js/jquery.form.js"></script>
	<script type="text/javascript">
		$(function() {
			$("#add").click(
				function() {
					$(".modal-dialog").css("width", "900px"),
					$(".modal-dialog").css("maxheight", "800px"),
					$("#myModal").modal("toggle"),
					$("#myModalLabel").html("新增"),
					$("#myModalBody").html(
						"<h3>正在打開。。。</h3>"),
					$.get("/employee/edit?id=", function(e) {
						$("#myModalBody").html(e);
					})
				});

			$('#search').click(function() {
				var flag = dayCheck();
				
				if(!flag) {
					return;
				}
				
				$('#pageNo').val("1");
				$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
				$('#listForm').ajaxSubmit({
					success : function(data) {
						$('#listContainer').html(data);
					}
				});
			});
			
			$('#export').click(function() {
				if(!dayCheck()) {
					return;
				}
				
				var temp = $('#listForm').attr('action');				
				$('#listForm').attr('action', '${ctx}/employee/export');
				$('#listForm').submit();
				$('#listForm').attr('action', temp);
			});
			
			$('#modalSubmit').click(function() {
				$('#myModal').modal('toggle');
				$('#search').click();
			});

			$("#import").click(function() {
				$('#importExcelForm').ajaxSubmit({
					success : function(data) {
						alert(data.message);
						$('#search').click();
					}
				});
				
				return false;
			});
			
			$("#clone").click(
				function() {
					$(".modal-dialog").css("width", "900px"),
					$(".modal-dialog").css("maxheight", "800px"),
					$("#myModal").modal("toggle"),
					$("#myModalLabel").html("複製"),
					$("#myModalBody").html(
						"<h3>正在打開。。。</h3>"),
					$.get("/employee/clone", function(e) {
						$("#myModalBody").html(e);
					})
				});
		});

		function toPage(pageNo) {
			if(!dayCheck()) {
				return;
			}
			
			if(!pageNo || pageNo <= 0) {
				return;
			}
			
			$('#pageNo').val(pageNo);
			$('#listForm').ajaxSubmit({
				success : function(data) {
					$('#listContainer').html(data);
				},
				error : function() {
					alert('連接網路失敗');
				}
			});
			$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
		}
		
		// TODO
		function dayCheck() {
			var beginTime = $("#beginTime").val();
			if (!beginTime) {
				var beginTimeDate = new Date(beginTime);
				var endTime = $("#endTime").val();
				var endTimeDate = new Date();
				if (endTime) {
					endTimeDate = new Date(endTime);
				}
				if (beginTimeDate > endTimeDate) {
					alert("結束時間不能大於開始時間!");
					return false;
				}
			}
			return true;
		}

		function edit(id) {
			$(".modal-dialog").css("width", "900px"), $(".modal-dialog").css(
				"maxheight", "800px"), $("#myModal").modal("toggle"), $(
				"#myModalLabel").html("編輯"), $("#myModalBody").html(
				"<h3>正在打開。。。</h3>"), $.get("/employee/edit?id=" + id, function(e) {
					$("#myModalBody").html(e);
				})
		}			

		function deleteObject(name, id) {
			if(!confirm("確定刪除" + name + "?")) {
				return;
			}
			
			$.ajax({
				url : '${ctx}/employee/delete',
				dataType : 'json',
				data : {
					'id' : id
				},
				success : function(data) {
					alert(data.message);
					$('#search').click();
				},
				error : function() {
					alert('網絡連接失敗，請聯繫管理員');
				}
			});
		}
		
		$('#changeCompany').change(function(){
			var cid = $(this).val();
			
			if(cid == ''){
				$('#requestDepartment').empty();					
				$('#requestDepartment').append('<option value="">請選擇</option>');
			}else{
				$.ajax({
					url : '${ctx}/department/deptList',
					dataType : 'json',
					data : {
						'id' : cid
					},
					success : function(data) {
						$('#requestDepartment').empty();							
						$('#requestDepartment').append('<option value="">請選擇</option>');
						
						for(var i in data) {
							$('#requestDepartment').append(
								'<option value="' + data[i].id+'">'
								+ data[i].name
								+ '</option>');
						}
					},
					error : function() {
						alert('網絡連接失敗，請聯繫管理員');
					}
				});
			}
		});
		
		function getOrganigram(cid, form) {
			if(cid != ''){
				$.ajax({
					url : '${ctx}/jobTitle/jobList',
					dataType : 'json',
					data : {
						'id' : cid
					},
					success : function(data) {
						$('#' + form + ' #jobTitleId').empty();						
						$('#' + form + ' #jobTitleId').append('<option value="">全部</option>');
						
						for(var i in data) {
							$('#' + form + ' #jobTitleId').append(
								'<option value="' + data[i].id+'">'
								+ data[i].name
								+ '</option>');
						}
					},
					error : function() {
						alert('網絡連接失敗，請聯繫管理員');
					}
				});
				
				$.ajax({
					url : '${ctx}/department/deptList',
					dataType : 'json',
					data : {
						'id' : cid
					},
					success : function(data) {
						$('#' + form + ' #departmentId').empty();
						
						$('#' + form + ' #departmentId').append('<option value="">全部</option>');
						
						for(var i in data) {
							$('#' + form + ' #departmentId').append(
								'<option value="' + data[i].id+'">'
								+ data[i].name
								+ '</option>');
						}
					},
					error : function() {
						alert('網絡連接失敗，請聯繫管理員');
					}
				});	
			}		
		}				
	</script>
</body>
</html>