<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	
	<table
		class="table table-bordered table-full-width"
		id="list">
		<thead>
			<tr>
				<th width="8%">員工編號<br>員工姓名</th>
				<th width="12%">手機</th>
				<th>家裡電話<br>地址</th>
				<th width="3%">分機</th>
				<th width="8%">職稱</th>
				<th width="8%">部門</th>
				<th>公司</th>				
				<th width="10%">到職時間<br>離職時間</th>
				<th width="10%">修改者<br>修改時間</th>
				<th width="10%">建立者<br>建立時間</th>
				<th width="5%">操作</th>
			</tr>			
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
				<tr>
					<td>${o.name}<br>${o.employeeNo}</td>
					<td>${o.mobile}<c:if test="${not empty o.mobile2}"><br><span style="color:blue;">${o.mobile2}</span></c:if></td>
					<td>${o.phone}<br>${o.address}</td>
					<td>${o.phoneExt}</td>
					<td>${o.jobTitleName}</td>
					<td>${o.departmentName}</td>
					<td>${o.companyName}</td>					
					<td>
						<fmt:formatDate pattern="yyyy-MM-dd" value="${o.onboardTime}" />					
						<br>
						<span style="color:red;">
						<fmt:formatDate pattern="yyyy-MM-dd" value="${o.quitTime}" />
						</span>
					</td>
					<td>
						${o.modifierName}
						<br>
						<fmt:formatDate pattern="yyyy-MM-dd" value="${o.updateTime}" />
					</td>
					<td>
						${o.creatorName}
						<br>
						<fmt:formatDate pattern="yyyy-MM-dd" value="${o.createTime}" />
					</td>
					<td>
						<a href="javascript:;" onclick="edit(${o.id})">編輯</a>
						<a href="javascript:;" onclick="deleteObject('${o.name}', ${o.id})">刪除</a>
					</td>
				</tr>		
			</c:forEach>
		</tbody>
	</table>
</div>
<my:pagination page="${page}" />