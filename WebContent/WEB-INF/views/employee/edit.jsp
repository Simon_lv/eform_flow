<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/employee/saveEmployee" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${o.id}" />
		<div class="table-responsive">
			<table class="table">
				<tr>
					<td>員工編號：</td>
					<td><input name="employeeNo" value="${o.employeeNo}" /></td>								
				</tr>
				<tr>
					<td>員工姓名：</td>
					<td><input name="name" value="${o.name}" /></td>									
				</tr>				
				<tr>
					<td>手機：</td>
					<td><input name="mobile" value="${o.mobile}" /></td>								
				</tr>
				<tr>
					<td>家裡電話：</td>
					<td><input name="phone" value="${o.phone}" /></td>							
				</tr>
				<tr>
					<td>郵遞區號：</td>
					<td>
						<input name="zipCode" value="${o.zipCode}"/>						
					</td>								
				</tr>
				<tr>
					<td>地址：</td>
					<td>
						<input name="address" value="${o.address}" size="50"/>
					</td>								
				</tr>
				<tr>
					<td>分機：</td>					
					<td><input name="phoneExt" value="${o.phoneExt}" /></td>								
				</tr>
				<tr>
					<td>email：</td>
					<td><input name="email" value="${o.email}" size="50"/></td>								
				</tr>
				<tr>
					<td>公司：</td>
					<td>
						<c:if test="${empty o.companyId}">
						<select name="companyId" onchange="getOrganigram(this.value, 'editForm')">
							<option value="">請選擇公司</option>
							<c:forEach items="${allCompany}" var="a">
							<option value="${a.id}" <c:if test="${a.id == o.companyId}">selected="selected"</c:if> > ${a.name}</option>
							</c:forEach>
						</select>
						</c:if>
						<!-- 唯讀 -->
						<c:if test="${not empty o.companyId}">
							${o.companyName}
							<input type="hidden" name="companyId" value="${o.companyId}" />
						</c:if>
					</td>							
				</tr>
				<tr>
					<td>部門：</td>
					<td>
						<select name="departmentId" id="departmentId">
							<option value="">請選擇部門</option>
							<c:forEach items="${allDepartment}" var="a">
							<option value="${a.id}" <c:if test="${a.id == o.departmentId}">selected="selected"</c:if> > ${a.name}</option>
							</c:forEach>
						</select>
					</td>							
				</tr>
				<tr>
					<td>職稱：</td>
					<td>
						<select name="jobTitleId" id="jobTitleId">
							<option value="">請選擇職稱</option>
							<c:forEach items="${allJobTitle}" var="a">
							<option value="${a.id}" <c:if test="${a.id == o.jobTitleId}">selected="selected"</c:if> > ${a.name}</option>
							</c:forEach>
						</select>
					</td>									
				</tr>				
				<tr>
					<td>到職時間：</td>
					<td>
						<input onFocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" type="text"
							name="onboardTime" id="onboardTime" class="Wdate" 
							value="<fmt:formatDate pattern="yyyy-MM-dd" value="${o.onboardTime}" />" >
					</td>							
				</tr>				
				<tr>
					<td>離職時間：</td>
					<td>
						<input onFocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" type="text"
							name="quitTime" id="quitTime" class="Wdate" 
							value="<fmt:formatDate pattern="yyyy-MM-dd" value="${o.quitTime}" />" >
					</td>					
				</tr>				
			</table>
		</div>
	</form>
	<input type="button" value="保存" id="save" />
</div>

<script type="text/javascript">
$(function(){
	$('#save').click(function() {
		if($("#editForm input[name='employeeNo']").val() == '') {
			alert("請輸入員工編號");
			return;
		}
		
		if($("#editForm input[name='name']").val() == '') {
			alert("請輸入員工姓名");
			return;
		}
		
		if($("#editForm select[name='jobTitleId']").val() == '') {
			alert("請輸入職稱");
			return;
		}
		
		if($("#editForm select[name='departmentId']").val() == '') {
			alert("請輸入部門");
			return;
		}
		
		if($("#editForm select[name='companyId']").length > 0 && $("#editForm select[name='companyId']").val() == '') {
			alert("請輸入公司");
			return;
		}
		
		if($("#editForm input[name='onboardTime']").val() == '') {
			alert("請輸入到職時間");
			return;
		}
		
		$("#editForm").ajaxSubmit({
            success: function(e) {
                alert(e.message);
                if("0000" == e.code) {
                	$("#myModal").modal("toggle");
                	$('#search').click();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
            	console.log(textStatus, errorThrown);
                alert("網路連接失敗，請聯繫管理員");
            }
        })
	});
});
</script>