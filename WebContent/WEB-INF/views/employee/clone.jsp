<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/employee/cloneEmployee" method="post" id="editForm">
		<div class="table-responsive">
			<table class="table">
				<tr>
					<td colspan="6">來源</td>
				</tr>				
				<tr>
					<td>公司：</td>
					<td>
						<select name="srcComId" id="srcComId" onchange="changeCompany(this.value, 'srcDptId', false)">
							<option value="">請選擇公司</option>
							<c:forEach items="${allCompany}" var="a">
							<option value="${a.id}">${a.name}</option>
							</c:forEach>
						</select>						
					</td>											
					<td>部門：</td>
					<td>
						<select name="srcDptId" id="srcDptId" onchange="changeDepartment(this.value)">
							<option value="">請選擇部門</option>							
						</select>
					</td>
					<td>員工：</td>
					<td>
						<select name="srcEmpId" id="srcEmpId">
							<option value="">全部</option>							
						</select>
					</td>							
				</tr>
				<tr>
					<td colspan="6">目的</td>
				</tr>				
				<tr>
					<td>公司：</td>
					<td>
						<select name="destComId" id="destComId" onchange="changeCompany(this.value, 'destDptId', true)">
							<option value="">請選擇公司</option>
							<c:forEach items="${allCompany}" var="a">
							<option value="${a.id}">${a.name}</option>
							</c:forEach>
						</select>						
					</td>											
					<td>部門：</td>
					<td>
						<select name="destDptId" id="destDptId">
							<option value="">同來源部門</option>							
						</select>
					</td>
					<td></td>
					<td></td>							
				</tr>			
			</table>
		</div>
	</form>
	<input type="button" value="保存" id="save" />
</div>

<script type="text/javascript">
$(function(){
	$('#save').click(function() {
		if($("#srcComId").val() == '') {
			alert("請輸入來源公司");
			return;
		}
		
		if($("#srcDptId").val() == '') {
			alert("請輸入來源部門");
			return;
		}
		
		if($("#destComId").val() == '') {
			alert("請輸入目的公司");
			return;
		}		
		
		$("#editForm").ajaxSubmit({
            success: function(e) {
                alert(e.message);
                if("0000" == e.code) {
                	$("#myModal").modal("toggle");
                	$('#search').click();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
            	console.log(textStatus, errorThrown);
                alert("網路連接失敗，請聯繫管理員");
            }
        })
	});
});

function changeCompany(cid, target, isDest) {
	var combo = $("#" + target);
	
	$.ajax({
		url : '${ctx}/department/deptList',
		dataType : 'json',
		data : {
			'id' : cid
		},
		success : function(data) {
			combo.empty();							
			combo.append('<option value="">' + (isDest ? '同來源部門' : '請選擇') + '</option>');
			
			for(var i in data) {
				combo.append(
					'<option value="' + data[i].id+'">'
					+ data[i].name
					+ '</option>');
			}
		},
		error : function() {
			alert('網絡連接失敗，請聯繫管理員');
		}
	});
}

function changeDepartment(did) {
	var cid = $("#srcComId").val();
	
	$.ajax({
		url : '${ctx}/employee/employeeList',
		dataType : 'json',
		data : {
			'cid' : cid,
			'did' : did
		},
		success : function(data) {
			$('#srcEmpId').empty();							
			$('#srcEmpId').append('<option value="">全部</option>');
			
			for(var i in data) {
				$('#srcEmpId').append(
					'<option value="' + data[i].id+'">'
					+ data[i].name
					+ '</option>');
			}
		},
		error : function() {
			alert('網絡連接失敗，請聯繫管理員');
		}
	});
}
</script>