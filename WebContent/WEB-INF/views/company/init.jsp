<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
<head>
<title>8888play ERP</title>
</head>
<body>
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div class="modal-body" id="myModalBody"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="modalSubmit">關閉</button>
				</div>
			</div>
		</div>
	</div>
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				基本資料 <small>公司資料</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="${ctx}/index"><i class="fa fa-dashboard"></i>8888play ERP</a></li>
				<li class="active">公司資料</li>
			</ol>
		</section>
		<!-- Main content -->
		<div class="container-fluid">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body">
						<form action="${ctx}/company/list" id="listForm" method="post">
							<input type="hidden" id="pageNo" name="pageNo" value="" />
							<div class="table-responsive">
								<table class="table">
									<tr>
										<td>公司名稱：</td>
										<td><input name="name"></td>
										<td>公司電話：</td>
										<td><input name="phone"></td>										
									</tr>
									<tr>
										<td>公司統編：</td>
										<td><input name="taxNumber"></td>
										<td>公司負責人：</td>
										<td><input name="owner"></td>
									</tr>
									<tr>
										<td>成立時間起：</td>
										<td>
											<input type="text" value=""
												onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
												class="Wdate" name="establishDateStart" id="establishDateStart">
										</td>
										<td>成立時間迄：</td>
										<td>
											<input type="text" value=""
												onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
												class="Wdate" name="establishDateEnd" id="establishDateEnd">
										</td>
									</tr>																		
									<tr>
										<td>
											<input type="button" id="search" value="搜尋"> 
											<input id="add" type="button" value="新增" />
											<input type="button" id="export" value="導出列表">
										</td>
									</tr>
								</table>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div id="listContainer"></div>
		</div>
	</aside>

	<script type="text/javascript" src="${ctx }/js/jquery.form.js"></script>
	<script type="text/javascript">
		$(function() {
			$("#add").click(
				function() {
					$(".modal-dialog").css("width", "900px"),
					$(".modal-dialog").css("maxheight",	"800px"),
					$("#myModal").modal("toggle"),
					$("#myModalLabel").html("新增"),
					$("#myModalBody").html("<h3>正在打開。。。</h3>"),
					$.get("/company/edit?id=", function(e) {
						$("#myModalBody").html(e);
					})
				});

			$('#search').click(function() {
				var flag = dayCheck();
				
				if(!flag) {
					return;
				}
				
				$('#pageNo').val("1");
				$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
				$('#listForm').ajaxSubmit({
					success : function(data) {
						$('#listContainer').html(data);
					}
				});
			});
			
			$('#export').click(function() {
				if(!dayCheck()) {
					return;
				}
				
				var temp = $('#listForm').attr('action');
				$('#listForm').attr('action', '${ctx}/company/export');
				$('#listForm').submit();
				$('#listForm').attr('action', temp);
			});
			
			$('#modalSubmit').click(function() {
				$('#myModal').modal('toggle');
				$('#search').click();
			});
		});

		function toPage(pageNo) {
			if(!dayCheck()) {
				return;
			}
			
			if (!pageNo || pageNo <= 0) {
				return;
			}
			
			$('#pageNo').val(pageNo);
			$('#listForm').ajaxSubmit({
				success : function(data) {
					$('#listContainer').html(data);
				},
				error : function() {
					alert('連接網路失敗');
				}
			});
			$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
		}
		
		function dayCheck() {
			var beginTime = $("#establishDateStart").val();
			
			if(!beginTime) {
				var beginTimeDate = new Date(beginTime);
				var endTime = $("#establishDateEnd").val();
				var endTimeDate = new Date();
				
				if(endTime) {
					endTimeDate = new Date(endTime);
				}
				
				if(beginTimeDate > endTimeDate) {
					alert("結束時間不能大於開始時間!");
					return false;
				}
			}
			return true;
		}

		function edit(id) {
			$(".modal-dialog").css("width", "900px"), $(".modal-dialog").css(
				"maxheight", "800px"), $("#myModal").modal("toggle"), 
				$("#myModalLabel").html("編輯"), $("#myModalBody").html(
				"<h3>正在打開。。。</h3>"), $.get("/company/edit?id=" + id, function(e) {
					$("#myModalBody").html(e);
				})
		}

		function deleteCP(name, id) {
			if(!confirm("確定刪除" + name + "?")) {
				return;
			}
			
			$.ajax({
				url : '${ctx}/company/deleteCP',
				dataType : 'json',
				data : {
					'id' : id
				},
				success : function(data) {
					alert(data.message);
					$('#search').click();
				},
				error : function() {
					alert('網路連接失敗，請聯繫管理員');
				}
			});
		}
	</script>
</body>
</html>