<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	
	<table
		class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>
				<th>ID</th>
				<th>名稱</th>
				<th>統編</th>
				<th>地址</th>
				<th>電話</th>
				<th>負責人</th>
				<th>成立時間</th>
				<th>修改者</th>
				<th>修改時間</th>
				<th>建立者</th>
				<th>建立時間</th>			
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
				<tr>
					<td>${o.id}</td>
					<td>${o.name}</td>
					<td>${o.taxNumber}</td>
					<td>${o.address}</td>
					<td>${o.phone}</td>
					<td>${o.owner}</td>
					<td><fmt:formatDate value="${o.establishDate}" type="date" pattern="yyyy-MM-dd" /></td>
					<td>${o.modifierName}</td>
					<td>${o.updateTime}</td>
					<td>${o.creatorName}</td>
					<td>${o.createTime}</td>
					<td>
						<a href="javascript:;" onclick="edit(${o.id})">編輯</a>
						<a href="javascript:;" onclick="deleteCP('${o.name}', ${o.id})">刪除</a>
					</td>
				</tr>		
			</c:forEach>
		</tbody>
	</table>
</div>
<my:pagination page="${page}" />