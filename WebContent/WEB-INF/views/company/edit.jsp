<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/company/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${cp.id}" />
		<div class="table-responsive">
			<table class="table">
				<tr>
					<td width="10%">名稱：</td>
					<td><input name="name" value="${cp.name}" /></td>
					<td width="10%">統編：</td>
					<td><input name="taxNumber" value="${cp.taxNumber}" /></td>
				</tr>
				<tr>
					<td>地址：</td>
					<td><input name="address" value="${cp.address}" /></td>				
					<td>電話：</td>
					<td><input name="phone" value="${cp.phone}" /></td>
				</tr>
				<tr>
					<td>負責人：</td>
					<td><input name="owner" value="${cp.owner}" /></td>
					<td>成立時間：</td>
					<td>
						<fmt:formatDate value="${cp.establishDate}" type="date" pattern="yyyy-MM-dd" var="establishDate" />
						<input type="text" value="${establishDate}"
							onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
							class="Wdate" name="establishDate" id="establishDate">
					</td>
				</tr>
			</table>
		</div>
	</form>
	<input type="button" value="保存" id="save" />
</div>

<script type="text/javascript">
$(function(){
	$('#save').click(function() {
		if($("input[name='name']").val() == '') {
			alert("請輸入名稱");
			return;
		}
		if($("input[name='taxNumber']").val() == '') {
			alert("請輸入統編");
			return;
		}
		if($("input[name='address']").val() == '') {
			alert("請輸入地址");
			return;
		}
		if($("input[name='phone']").val() == '') {
			alert("請輸入電話");
			return;
		}
		if($("input[name='owner']").val() == '') {
			alert("請輸入負責人");
			return;
		}
		if($("input[name='establishDate']").val() == '') {
			alert("請輸入成立時間");
			return;
		}
		$("#editForm").ajaxSubmit({
            success: function(e) {
                alert(e.message), $("#myModal").modal("toggle");
                $('#search').click();
            },
            error: function() {
                alert("網路連接失敗，請聯繫管理員");
            }
        })
	});
});
</script>