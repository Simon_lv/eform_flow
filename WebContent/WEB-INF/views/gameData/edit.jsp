<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/gameData/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${cp.id}" />
		<div class="table-responsive">
			<table class="table">
				<tr>
					<td>遊戲名稱：</td>
					<td><input name="name" value="${cp.name}" /></td>
				</tr>
				<tr>
					<td>公司：</td>
					<td>
						<select name="companyId">
                           	<c:forEach items="${allCompany}" var="company">
                           	<option value="${company.id}"
                           		<c:if test="${cp.companyId == company.id}">selected</c:if>
                           		>${company.name}</option>
                           	</c:forEach>
                        </select>
                    </td>   
				</tr>
				<tr>
					<td>營運：</td>
					<td>
						<select name="online">
							<option value="1" <c:if test="${cp.online == 1 }">selected="selected"</c:if>>上架中</option>
							<option value="2" <c:if test="${cp.online == 2 }">selected="selected"</c:if>>已下架</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>狀態：</td>
					<td>
						<select name="status">
							<option value="1" <c:if test="${cp.status == 1 }">selected="selected"</c:if>>正常</option>
							<option value="2" <c:if test="${cp.status == 2 }">selected="selected"</c:if>>停權</option>
						</select>
					</td>
				</tr>								
			</table>
		</div>
	</form>
	<input type="button" value="保存" id="save" />
</div>

<script type="text/javascript">
$(function(){
	$('#save').click(function() {
		if($("input[name='name']").val() == '') {
			alert("請輸入遊戲名稱");
			return;
		}	
		
		$("#editForm").ajaxSubmit({
            success: function(e) {
                alert(e.message);
                
                if("0000" == e.code) {
                	$("#myModal").modal("toggle");
                	$('#search').click();
                }
            },
            error: function() {
                alert("網路連接失敗，請聯繫管理員");
            }
        })
	});
});
</script>