<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	
	<table class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>				
				<th>表單編號</th>				
				<th>公司</th>
				<th>部門</th>
				<th>申請日期</th>
				<th>申請人</th>
				<th>狀態</th>	
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
				<tr>
					<td>${o.serialNo}</td>					
					<td>${o.companyName}</td>
					<td>${o.departmentName}</td>
					<td><fmt:formatDate value="${o.createTime}" type="date" pattern="yyyy-MM-dd" /></td>
					<td>${o.creatorName}</td>
					<td>
						<c:choose>
						    <c:when test="${o.status == -1}">編輯中</c:when>
						    <c:when test="${o.status == 0}">正常結束</c:when>
						    <c:when test="${o.status == 1 && o.stepStatus == 1}">執行中</c:when>
						    <c:when test="${o.status == 1 && o.stepStatus == 2}">執行中(<span style="color:red;">${o.ownerName}</span>鎖定)</c:when>
						    <c:when test="${o.status == 8}">註銷</c:when>
						    <c:when test="${o.status == 9}">退件</c:when>
						    <c:otherwise>未定義(o.status)</c:otherwise>
						</c:choose>
					</td>
					<td>
						<!-- 審核歷程 -->									
						<c:if test="${o.status >= 0}">
							<a href="javascript:;" onclick="history(${o.workFlowId})">審核歷程</a>
						</c:if>
						<c:if test="${o.status == 1 && o.stepStatus == 2}">
						<a href="javascript:;" onclick="unlock(${o.workFlowDetailId})">解除鎖定</a>
						</c:if>
						<a href="javascript:;" onclick="get(${o.workFlowId})">檢視</a>																							
					</td>
				</tr>		
			</c:forEach>
		</tbody>
	</table>
</div>
<my:pagination page="${page}" />