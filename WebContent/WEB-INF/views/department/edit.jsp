<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/department/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${o.id}" />
		<div class="table-responsive">
			<table class="table">
				<tr>
					<td>部門編號：</td>
					<td><input name="deptNo" value="${o.deptNo}" /></td>
				</tr>
				<tr>
					<td>部門名稱：</td>
					<td><input name="name" value="${o.name}" /></td>
				</tr>
				<tr>
					<td>部門階層：</td>
					<td>
						<select name="deptLevel" onchange="changeParent()">
							<option value="">請選擇</option>
							<option value="1" <c:if test="${o.deptLevel == 1}">selected="selected"</c:if>>處</option>
							<option value="2" <c:if test="${o.deptLevel == 2}">selected="selected"</c:if>>中心</option>
							<option value="3" <c:if test="${o.deptLevel == 3}">selected="selected"</c:if>>部門</option>
						</select>
					</td>
				</tr>				
				<tr>
					<td>父部門：</td>
					<td>
						<select name="parentId">
							<option value="">不設定</option>
							<c:forEach items="${parents}" var="a">
							<option value="${a.id}" <c:if test="${a.id == o.parentId}">selected="selected"</c:if>> ${a.name}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td>公司：</td>
					<td>
						<c:if test="${empty o.companyId}">
						<select name="companyId">
							<c:forEach items="${companys}" var="cp">
							<option value="${cp.id}"> ${cp.name}</option>
							</c:forEach>
						</select>
						</c:if>
						<!-- 唯讀 -->
						<c:if test="${not empty o.companyId}">							
							${o.companyName}
						</c:if>
					</td>
				</tr>				
			</table>
		</div>
	</form>
	<input type="button" value="保存" id="save" />
</div>

<script type="text/javascript">
$(function(){
	$('#save').click(function() {
		if($("input[name='deptNo']").val() == '') {
			alert("請輸入部門編號");
			return;
		}
		
		if($("input[name='name']").val() == '') {
			alert("請輸入部門名稱");
			return;
		}
		
		if($("select[name='deptLevel']").val() == '') {
			alert("請選擇部門階層");
			return;
		}		
		
		if(${empty o.companyId}){		
			if($("select[name='companyId']").length > 0 && $("select[name='companyId']").val() == '') {
				alert("請選擇所屬公司");
				return;
			}
		}
		
		$("#editForm").ajaxSubmit({
            success: function(e) {
                alert(e.message);
                
                if("0000" == e.code) {
                	$("#myModal").modal("toggle");
                	$('#search').click();
                }
            },
            error: function() {
                alert("網路連接失敗，請聯繫管理員");
            }
        })
	});
});
</script>