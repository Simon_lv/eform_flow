<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	
	<table
		class="table table-bordered table-full-width"
		id="list">
		<thead>
			<tr>
				<th>ID</th>
				<th>部門編號</th>
				<th>部門名稱</th>
				<th>部門階層</th>
				<th>父部門名稱</th>
				<th>公司名稱</th>
				<th>修改者</th>
				<th>修改時間</th>
				<th>建立者</th>
				<th>建立時間</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
				<tr>
					<td>${o.id}</td>
					<td>${o.deptNo}</td>
					<td>${o.name}</td>
					<td><c:choose>
						<c:when test="${o.deptLevel == 1}">處</c:when>
						<c:when test="${o.deptLevel == 2}">中心</c:when>
						<c:when test="${o.deptLevel == 3}">部門</c:when>
					</c:choose></td>
					<td>${o.parentName}</td>
					<td>${o.companyName}</td>
					<td>${o.modifierName}</td>
					<td>${o.updateTime}</td>
					<td>${o.creatorName}</td>
					<td>${o.createTime}</td>
					<td>
						<a href="javascript:;" onclick="edit(${o.id})">編輯</a>
						<a href="javascript:;" onclick="deleteObject('${o.name}', ${o.id})">刪除</a>
					</td>
				</tr>		
			</c:forEach>
		</tbody>

	</table>
</div>
<my:pagination page="${page}" />