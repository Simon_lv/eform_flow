<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
<head>
<title>8888play ERP</title>
<link href="${ctx}/resources/multiple-select/multiple-select.css" rel="stylesheet" type="text/css">
<style type="text/css">
#myModal {
	overflow-x: auto;
}
</style>
</head>
<body>
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div class="modal-body" id="myModalBody"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="modalSubmit">關閉</button>
				</div>
			</div>
		</div>
	</div>
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				待辦事項 <small></small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="${ctx}/index"><i class="fa fa-dashboard"></i>8888play ERP</a></li>
				<li class="active">待辦事項</li>
			</ol>
		</section>
		<!-- Main content -->
		<div class="container-fluid">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body">
					<form action="${ctx}/flow/draft" id="editForm" method="post">
						待處理清單
						<input type="hidden" id="formId" name="id" value="" />
						<input type="hidden" name="formType" value="${formType}" />												
					</form>
					</div>
				</div>
			</div>
			<div id="listContainer">
				<table class="table table-bordered table-full-width" id="list">
					<thead>
						<tr>
							<th>表單名稱</th>
							<c:if test="${formType == 'IS'}"><th>重大事故標題</th></c:if>
							<th>表單編號</th>
							<th>流程狀態</th>
							<th>審核歷程</th>							
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${todoListType}" var="o">
							<tr>				
								<td>${o.formName}</td>
								<c:if test="${formType == 'IS'}"><td>${o.description}</td></c:if>
								<td>${o.serialNo}</td>
								<td>
									<c:choose>
									    <c:when test="${o.status == -1}">編輯中</c:when>
									    <c:when test="${o.status == 0}">正常結束</c:when>
									    <c:when test="${o.status == 1}">執行中</c:when>
									    <c:when test="${o.status == 8}">註銷</c:when>
									    <c:when test="${o.status == 9}">退件</c:when>
									    <c:otherwise>未定義(o.status)</c:otherwise>
									</c:choose>
								</td>
								<td>
									<!-- 草稿 -->
									<c:if test="${o.status == -1}">
										<a href="javascript:;" onclick="edit(${o.workFlowId})">編輯</a>										
									</c:if>
									<!-- 流程中 -->
									<c:if test="${o.status == 1}">
									<c:choose>										
										<c:when test="${not empty o.countersignRole}">									    	
									    	<a href="javascript:;" onclick="process(${o.workFlowId}, ${o.workFlowDetailId})">會簽</a>
									    </c:when>
									    <c:when test="${o.stepType == 2}">									    	
									    	<a href="javascript:;" onclick="process(${o.workFlowId}, ${o.workFlowDetailId})">審核</a>
									    </c:when>
									    <c:when test="${o.stepType == 3}">									    	
									    	<a href="javascript:;" onclick="process(${o.workFlowId}, ${o.workFlowDetailId})">放行</a>
									    </c:when>									    									   
									</c:choose>										
									</c:if>
									<!-- 退件 -->
									<c:if test="${o.status == 9}">
										<a href="javascript:;" onclick="terminate(${o.workFlowId}, ${o.workFlowDetailId})">註銷</a>
										<a href="javascript:;" onclick="edit(${o.workFlowId})">重送</a>
									</c:if>
									<!-- 檢視 -->
									<a href="javascript:;" onclick="detail(${o.workFlowId})">檢視</a>										
									<!-- 審核歷程 -->									
									<c:if test="${o.status >= 0}">
										<a href="javascript:;" onclick="history(${o.workFlowId})">審核歷程</a>
									</c:if>																
								</td>
							</tr>		
						</c:forEach>
					</tbody>
				</table>
				<input type="hidden" id="processId" value="" />					
			</div>
		</div>
	</aside>

	<script type="text/javascript" src="${ctx }/js/jquery.form.js"></script>
	<script type="text/javascript" src="${ctx }/resources/multiple-select/jquery.multiple.select.js"></script>
	<script type="text/javascript">
		$(window).on('beforeunload', function() {
			var lock = $('#myModal').hasClass('in');
			
			if(lock) {
				// 返回鍵
				return "尚有案件開啟中";  // custum message not work
			}  	
		});
		
		$(function() {
			// 關閉彈出視窗
			$('#myModal').on('hidden.bs.modal', function () {
				// pass confirm modal close
				var confirm = $('#myModal').hasClass('in');
				console.log("abort..." + confirm);
				
				if(confirm) {
					return;
				}
				
				abort();
			});
			
			$('#modalSubmit').click(function() {
				$('#myModal').modal('toggle');
			});			
		});
		
		// 審核歷程
		function history(id) {
			$(".modal-dialog").css("width", "900px"), 
			$(".modal-dialog").css("maxheight", "800px"), 
			$("#myModal").modal("toggle"), 
			$("#myModalLabel").html("審核歷程"), 
			$("#myModalBody").html("<h3>正在打開。。。</h3>"), 
			$.get("/flow/list?id=" + id, 
				function(e) {
					$("#myModalBody").html(e);
				})
		}

		// 審放
		function process(id1, id2) {
			// detected locked already by someone
			$.ajax({
				url : '${ctx}/flow/lock',
				dataType : 'json',
				data : {
					'id1' : id1,
					'id2' : id2
				},
				success : function(data) {
					
					if(data.code == "0000") {						
						$("#processId").val(id2);
						
						$(".modal-dialog").css("width", "900px"), 
						$(".modal-dialog").css("maxheight", "800px"), 
						$("#myModal").modal("toggle"), 
						$("#myModalLabel").html("待辦事項"), 
						$("#myModalBody").html("<h3>正在打開。。。</h3>"), 
						$.get("/flow/process?id1=" + id1 + "&id2=" + id2, 
							function(e) {
								$("#myModalBody").html(e);
							})
					}
					else {
						alert(data.message);
					}
				},
				error : function() {
					alert('網絡連接失敗，請聯繫管理員');
				}
			});			
		}
		
		// 草稿或重送
		function edit(id) {
			// redirect
			$("#formId").val(id);			
			$('#editForm').submit();			
		}				
		
		// 畫面重整
		function refresh() {
			location.reload();
		}
		
		// 檢視
		function detail(id) {
			$(".modal-dialog").css("width", "900px"), 
			$(".modal-dialog").css("maxheight", "800px"), 
			$("#myModal").modal("toggle"), 
			$("#myModalLabel").html("檢視"), 
			$("#myModalBody").html("<h3>正在打開。。。</h3>"), 
			$.get("/flow/get?id=" + id, 
				function(e) {
					$("#myModalBody").html(e);
				})
		}
		
		// 註銷
		function terminate(id1, id2) {
			$.ajax({
				url : '${ctx}/flow/terminate',
				dataType : 'json',
				data : {
					'id1' : id1,
					'id2' : id2
				},
				success : function(data) {
					if(data.code == "0000") {
						alert("註銷成功");						
					}
					else {
						alert(data.message);
					}
					
					// refresh
					refresh();
				},
				error : function() {
					alert('網絡連接失敗，請聯繫管理員');
				}
			});
		}
		
		function abort() {
			var id = $("#processId").val();
			
			if(id == "") {				
				return;
			}
			// abort
			$.ajax({
				url : '${ctx}/flow/abort',
				dataType : 'json',
				data : {
					'id' : id
				},
				success : function(data) {
					// clear
					$("#processId").val("");
				},
				error : function() {
					alert('網絡連接失敗，請聯繫管理員');
				}
			});
		}				
	</script>
</body>
</html>