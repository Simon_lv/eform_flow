<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	
	<table class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>
				<th colspan="2">表單名稱</th>
				<th colspan="2">表單編號</th>
			</tr>
			<tr>
				<th colspan="2">${workFlow.formName}</th>
				<th colspan="2">${workFlow.serialNo}</th>
			</tr>
			<tr>				
				<th>流程步驟</th>
				<th>建立人員</th>
				<th>建立時間</th>
				<th>處理人員</th>
				<th>處理時間</th>
				<th>處理結果</th>
				<th>備註</th>				
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${list}" var="o">
				<tr>				
					<td>
						<c:choose>							
							<c:when test="${not empty o.countersignRole}">會簽</c:when>
							<c:when test="${o.stepType == 1}">編輯</c:when>
							<c:when test="${o.stepType == 2}">審核</c:when>
							<c:when test="${o.stepType == 3}">放行</c:when>
							<c:otherwise>未定義(o.stepType)</c:otherwise>
						</c:choose>
					</td>
					<td>${o.creator}</td>
					<td>${o.createTime}</td>
					<td>${o.owner}</td>
					<td>${o.processTime}</td>
					<td>
						<c:choose>
							<c:when test="${o.status == 0}">正常結束</c:when>
							<c:when test="${o.status == 1}">等待執行</c:when>
							<c:when test="${o.status == 2}">用戶鎖定</c:when>
							<c:when test="${o.status == 8}">註銷</c:when>
							<c:when test="${o.status == 9}">退件</c:when>
							<c:otherwise>未定義(${o.stepType})</c:otherwise>
						</c:choose>
					</td>					
					<td>${o.memo}</td>									
				</tr>		
			</c:forEach>
		</tbody>
	</table>
</div>