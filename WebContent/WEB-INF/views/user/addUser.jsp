<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
    <head>
        <title>后台管理 - 新增用戶</title>
    </head>
    <body>
   	<aside class="right-side">
      <!-- Content Header (Page header) -->
      <section class="content-header">
          <h1>
              	用戶列表
              <small>新增用戶</small>
          </h1>
          <ol class="breadcrumb">
              <li><a href="${ctx }/user/list"><i class="fa fa-dashboard"></i> 用戶列表</a></li>
              <li class="active">新增用戶</li>
          </ol>
      </section>
      <!-- Main content -->
      <section class="content">
          <div class="row">
              
              <div class="col-md-6" style="margin-left: 300px;">
              	<c:if test="${not empty msg }">
              		<div class="alert alert-warning alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <strong>提示!</strong> ${msg }
					</div>
              	</c:if>
                  <div class="box box-primary" >
                      <div class="box-header">
                          <h3 class="box-title">用戶信息</h3>
                      </div>
                      <div class="box-body">
                      	<form id="addForm" name="addForm" method="post" action="${ctx }/user/insertuser">
                          <div class="form-group">
                              <label>用戶名:</label>
                              <div class="input-group">
                                  <div class="input-group-addon">
                                      <i class="fa fa-user"></i>
                                  </div>
                                  <input type="text" id="username" name="username" class="form-control"  data-container="body" data-trigger="manual" data-toggle="popover" data-placement="right"  />
                              </div>
                              <label>暱稱:</label>
                              <div class="input-group">
                                  <div class="input-group-addon">
                                      <i class="fa fa-phone"></i>
                                  </div>
                                  <input type="text" id="nickname" name="nickname" class="form-control" data-container="body" data-trigger="manual" data-toggle="popover" data-placement="right"  />
                              </div>
                              <label>郵箱:</label>
                              <div class="input-group">
                                  <div class="input-group-addon">
                                      <i class="fa fa-envelope"></i>
                                  </div>
                                  <input type="text" autocomplete="off" id="email" name="email" class="form-control" data-container="body" data-trigger="manual" data-toggle="popover" data-placement="right"  />
                              </div>
                               <label>密碼:</label>
                              <div class="input-group">
                                  <div class="input-group-addon">
                                      <i class="fa fa-lock"></i>
                                  </div>
                                  <input type="password" autocomplete="off" id="password" name="password" class="form-control" data-container="body" data-trigger="manual" data-toggle="popover" data-placement="right"  />
                              </div>
                              <label>確認密碼:</label>
                              <div class="input-group">
                                  <div class="input-group-addon">
                                      <i class="fa fa-lock"></i>
                                  </div>
                                  <input type="password" id="passwordcfm" name="passwordcfm" class="form-control" data-container="body" data-trigger="manual" data-toggle="popover" data-placement="right"  />
                              </div>
                              <label>用戶組:</label>
                              <div class="input-group">
                                  <div class="input-group-addon">
                                      <i class="fa fa-users"></i>
                                  </div>
                                  <select id="group" name="group" class="form-control">
                                  	<c:forEach var="group" items="${groups }">
                                  		<option value="${group.id }">${group.name }</option>
                                  	</c:forEach>
                                  </select>
                              </div>
                              <label>角色:</label>
                              <div class="input-group">
                                  <div class="input-group-addon">
                                      <i class="fa fa-users"></i>
                                  </div>
                                  <select id="role" name="role" class="form-control">
                                  	<option value="3">普通用戶</option>
                                  	<option value="2">用戶組組長</option>
                                  	<option value="1">系統管理員</option>
                                  </select>
                              </div>
                          </div>
                      	</form>
                          <div class="input-group-btn" style="text-align: center;">
                          	<button id="add" class="btn btn-flat btn-primary">新 增</button>
                          </div>
                      </div>
                  </div>

              </div>
          </div>
      </section>
  </aside>
  <script type="text/javascript" src="${ctx }/resources/boostrap/js/tooltip.js"></script>
<script type="text/javascript" src="${ctx }/resources/boostrap/js/popover.js"></script>
<script type="text/javascript" src="${ctx }/js/validate/jquery.validate.js" ></script>
<script type="text/javascript" src="${ctx }/js/validate/messages_cn.js" ></script>
  	<script type="text/javascript">
  		$(document).ready(function(){
  			
  			$("#usermanageLi").addClass("active");
  			$("#userListLi").addClass("active");
  			
  			var e;
   			jQuery.validator.addMethod("isUserName", function(value, element) {
   				return this.optional(element) || /^\w{3,15}$/.test(value);
   			}, "請修正帳號");
   			//註冊validate函數
   			$("#addForm").validate({
   				rules : {
   					username : {
   						required : true,
   						rangelength :[3,15],
   						isUserName : true,
   						remote : "${ctx}/user/check"
   					},
   					nickname : {
   						required : true,
   						rangelength : [3,15],
   					},
   					password : {
   						required : true,
   						minlength : 6
   					},
   					passwordcfm : {
   						required : true,
   						minlength : 6,
   						equalTo : "#password"
   					},
   					email : {
   						required : true,
   						email : true,
   						remote : "${ctx}/user/check"
   					}
   					
   				},
   				messages : {
   					username : {
   						required : "請輸入用戶名",
   						rangelength : "用戶名長度為3-15位",
   						isUserName : "用戶名格式有誤(3-15位數字字母組成)",
   						remote : "用戶名已存在"
   					},
   					nickname : {
   						required : "請輸入你的暱稱",
   						rangelength : "暱稱長度為3-15位"
   					},
   					password : {
   						required : "請輸入密碼",
   						minlength : "密碼至少為6位"
   					},
   					passwordcfm : {
   						required : "請確認密碼",
   						minlength : "確認密碼至少為为6位",
   						equalTo	: "兩次密碼不一致"
   					},
   					email : {
   						required : "請輸入郵箱地址",
   						email : "請輸入正確郵箱地址",
   						remote : "郵箱已被註冊"
   					}
   				},
   				errorPlacement: function(error, element) {
   					e = element.attr('id');
   					$("#"+e).parent().addClass("has-error");
   					$("#"+e).attr("data-content",error.html());
   					$("#"+e).popover('show');
   				},
   				success: function(label) {
   					$("#"+e).parent().removeClass("has-error");
   					$("#"+e).popover('hide');
   				}
   			});
 			//新增 			
  			$("#add").click(function(){
  				if ($("#addForm").valid()){
  					document.getElementById("addForm").submit();
  				}
  			});
  		});
  	</script>
    </body>
</html>