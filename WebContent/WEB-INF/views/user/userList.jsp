<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
    <head>
        <title>后台管理</title>
    </head>
    <body>
   	<aside class="right-side">
          <!-- Content Header (Page header) -->
          <section class="content-header">
              <h1>
                  	用戶管理
                  <small>用戶列表</small>
              </h1>
              <ol class="breadcrumb">
                  <li><a href="#"><i class="fa fa-dashboard"></i> 用戶管理</a></li>
                  <li class="active">用戶列表</li>
              </ol>
          </section>
          <!-- Main content -->
          <section class="content">
              <!-- Main row -->
              <div class="row">
                  <div class="col-xs-12">
                      <div class="box">
                          <div class="box-header">
                              <h3 class="box-title">用戶列表(已激活用戶)</h3>
                              <div class="box-tools">
                                  <div class="input-group">
                                  	<form id="searchForm" action="${ctx }/user/list/" method="post">
                                      <input type="text" value="${pageBean.queryStr }" id="search" name="search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                                  	</form>
                                      <div class="input-group-btn">
                                          <button id="searchBtn" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                                      </div>
                                      <div class="input-group-btn">
                                          <button id="addUser" style="margin-left: 5px;" class="btn btn-sm btn-info">新增用戶</button>
                                          <button id="lockUser" style="margin-left: 5px;" class="btn btn-sm btn-warning">禁用用戶</button>
                                          <button id="promoteUser" style="margin-left: 5px;" class="btn btn-sm btn-primary">提升組長</button>
                                          <button id="moveUser" style="margin-left: 5px;" class="btn btn-sm btn-success">用户转移组</button>
                                      	  <button id="deleteUser" style="margin-left: 5px;" class="btn btn-sm btn-danger">刪除用户</button>
                                      	  <button id="modifyPwd" style="margin-left: 5px;" class="btn btn-sm btn-default">修改密码</button>
                                      </div>
                                  </div>
                              </div>
                          </div><!-- /.box-header -->
                          <div class="box-body table-responsive no-padding">
                              <form name="submitForm" id="submitForm">
	                              <table class="table table-hover">
	                                  <tr>
	                                  	  <th><input type="checkbox" id="selectAll" /></th>
	                                      <th>編號</th>
	                                      <th>用戶名</th>
	                                      <th>所屬組</th>
	                                      <th>角色名</th>
	                                      <th>暱稱</th>
	                                      <th>郵箱</th>
	                                      <th>創建時間</th>
	                                      <th>最後登錄時間</th>
	                                      <th>狀態</th>
	                                      <th>登錄次數</th>
	                                  </tr>
	                                  <c:forEach var="user" varStatus="i" items="${pageBean.result }">
		                                  <tr>
		                                  	  <td style="width: 15px;">
		                                  	  	<input name="checkbox" id="checkbox" value="${user.id }" type="checkbox" />
		                                  	  </td>
		                                      <td>${i.index + 1 }</td>
		                                      <td>
		                                      	<c:choose>
		                                      		<c:when test="${fn:containsIgnoreCase(user.loginName,search) && not empty search }">
		                                      			<font color="red">${user.loginName }</font>
		                                      		</c:when>
		                                      		<c:otherwise>
		                                      			${user.loginName }
		                                      		</c:otherwise>
		                                      	</c:choose>
		                                      </td>
		                                      <td>${user.groupName }</td>
		                                      <td>${user.roleName }</td>
		                                      <td>${user.nickname }</td>
		                                      <td>
		                                      	<c:choose>
		                                      		<c:when test="${fn:containsIgnoreCase(user.email,search) && not empty search }">
		                                      			<font color="red">${user.email }</font>
		                                      		</c:when>
		                                      		<c:otherwise>${user.email }</c:otherwise>
		                                      	</c:choose>
		                                      	
		                                      </td>
		                                      <td>${user.createTime }</td>
		                                      <td>${user.lastLogin }</td>
		                                      <td>
		                                      	<c:if test="${user.status == 0 }">未激活</c:if>
		                                      	<c:if test="${user.status == 1 }">已激活</c:if>
		                                      </td>
		                                      <td>${user.loginCount }</td>
		                                  </tr>
	                                  </c:forEach>
	                              </table>
	                              <input type="hidden" id="roleId" name="roleId" value="" />
	                              <input type="hidden" id="groupId" name="groupId" value="" />
                              </form>
                              <div class="row">
								<div class="col-xs-6">
									<div class="dataTables_info" id="example2_info">
									第  ${pageBean.page } 頁 共  ${pageBean.totalPage } 頁 總共 ${pageBean.totalCount } 條記錄
									</div>
								</div>
								<div class="col-xs-6">
									<c:if test="${not empty pageBean.result }">
										<div class="dataTables_paginate paging_bootstrap">
											<ul class="pagination">
												<c:choose>
													<c:when test="${pageBean.page == 1 }">
														<li class="disabled"><a>首頁</a></li>
														<li class="prev disabled"><a>← 上一頁</a></li>
													</c:when>
													<c:otherwise>
														<li><a href="${ctx }/user/list/1">首頁</a></li>
														<li><a href="${ctx }/user/list/${pageBean.page - 1}">← 上一頁</a></li>
													</c:otherwise>
												</c:choose>
												<c:choose>
													<c:when test="${pageBean.page == pageBean.totalPage }">
														<li class="disabled"><a>下一頁 → </a></li>
														<li class="disabled"><a>末頁</a></li>
													</c:when>
													<c:otherwise>
														<li><a href="${ctx }/user/list/${pageBean.page + 1}">下一頁  →</a></li>
														<li><a href="${ctx }/user/list/${pageBean.totalPage}">末頁</a></li>
													</c:otherwise>
												</c:choose>
											</ul>
										</div>
									</c:if>
								</div>
							</div>
                          </div>
                      </div>
                  </div>
              </div>
          </section>
      </aside>
      <!-- 用户组Modal -->
      <div class="modal fade" id="userGroupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">修改用戶組</h4>
		      </div>
		      <div class="modal-body">
		      	<div  class="form-horizontal">
			      	<div class="form-group">
				    <div class="col-sm-12">
				      	用戶組名称：
				      	<select id="group" name="group" class="form-control">
                        	<c:forEach var="group" items="${groups }">
                        		<option value="${group.id }">${group.name }</option>
                        	</c:forEach>
                        </select>
                                                                        角色：
				      	<select id="role" name="role" class="form-control">
                        	<c:forEach var="role" items="${roles }">
                        		<option value="${role.id }">${role.name }</option>
                        	</c:forEach>
                        </select>
				    </div>
				  </div>
				  <p class="text-danger" id="updateGroupNameInfo">
		           	  </p>
		      	</div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
		        <button type="button" id="updateGroup" class="btn btn-primary">保存</button>
		      </div>
		    </div>
		  </div>
		</div>
		
		<!-- 修改密码Modal -->
		<div class="modal fade" id="userPwdModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">修改用戶密碼</h4>
		      </div>
		      <div class="modal-body">
		      	<div  class="form-horizontal">
			      	<div class="form-group">
				    <div class="col-sm-12">
				      	<form id="savePwdForm" name="savePwdForm">
					      	新密碼：
					      	<input name="password" id="password" type="password" class="form-control" />
	                                                                        確認新密碼：
					      	<input name="passwordcfm" id="passwordcfm" type="password" class="form-control" />
				      		<input name="userId" id="userId" type="hidden" />
				      	</form>
				    </div>
				  </div>
				  <p class="text-danger" id="updateNameInfo">
		          </p>
		      	</div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
		        <button type="button" id="updatePwd" class="btn btn-primary">保存</button>
		      </div>
		    </div>
		  </div>
		</div>
      <script type="text/javascript" src="${ctx }/js/jquery.form.js" ></script>
      <script type="text/javascript">
      		$(document).ready(function(){
      			$("#usermanageLi").addClass("active");
      			$("#userListLi").addClass("active");
      			
      			$("#selectAll").click(function(){
      				if ($(this).attr("checked")){
    	  				$("input[name='checkbox']").attr("checked",true);
      				}else{
      					$("input[name='checkbox']").attr("checked",false);
      				}
      			});
      			
      			//查詢
      			$("#searchBtn").click(function(){
      				var search = $.trim($("#search").val());
      				if (search == ''){
      					alert("請輸入查詢信息");
      					return;
      				}
      				$("#searchForm").submit();
      			});
      			
      			//禁用
      			$("#lockUser").click(function(){
      				var length = $("input[name='checkbox']:checked").size();
      				if (length <= 0){
      					alert("請至少選擇一項進行操作");
      					return;
      				}
      				
      			    //異步提交表單
    				$("#submitForm").ajaxSubmit({
    					url : "${ctx }/user/lockusers",
    					type : "POST",
    	   				dataType : "text",
    	   				success : function(data){
    	   					switch (data){
    	   					case "nullparam" :
    	   						alert("請至少選擇一項進行操作");
    	   						break;
    	   					case "success" :
    	   						window.location.reload(); 
    	   						break;
    	   					case "error" :
    	   						alert("禁用用戶失敗");
    	   						break;
    	   						default:
    	   							alert(data);
    	   					}
    	   				}
    				});
      			});
      			
      			//提升组长
      			$("#promoteUser").click(function(){
      				var length = $("input[name='checkbox']:checked").size();
      				if (length <= 0){
      					alert("請至少選擇一項進行操作");
      					return;
      				}
      				 //異步提交表單
    				if (confirm("確認將選取用戶提升為所屬組組長")){
    					$("#submitForm").ajaxSubmit({
        					url : "${ctx }/user/promoteusers",
        					type : "POST",
        	   				dataType : "text",
        	   				success : function(data){
        	   					switch (data){
        	   					case "nullparam" :
        	   						alert("請至少選擇一項進行操作");
        	   						break;
        	   					case "success" :
        	   						window.location.reload(); 
        	   						break;
        	   					case "error" :
        	   						alert("提升組長失敗");
        	   						break;
        	   						default:
        	   							alert(data);
        	   					}
        	   				}
        				});
    				}
      			});
      		    //刪除用戶
      			$("#deleteUser").click(function(){
      				var length = $("input[name='checkbox']:checked").size();
      				if (length == 0){
      					alert("請至少選擇一條記錄進行操作");
      					return;
      				}
      				if (confirm("刪除用戶，數據將無法恢復？")){
      				//異步提交表單
      					$("#submitForm").ajaxSubmit({
      						url : "${ctx }/user/deleteusers",
      						type : "POST",
      		   				dataType : "text",
      		   				success : function(data){
      		   					switch (data){
      		   					case "nullparam" :
      		   						alert("請至少選擇一項進行操作");
      		   						break;
      		   					case "success" :
      		   						window.location.reload(); 
      		   						break;
      		   					case "error" :
      		   						alert("刪除用戶失敗");
      		   						break;
      		   						default:
      		   							alert(data);
      		   					}
      		   				}
      					});
      				}
      			});
      		    //新增用戶按鈕
      		    $("#addUser").click(function(){
      		    	window.location.href = "${ctx}/user/adduser"
      		    });
      		    
      		    //用户组转移
      		    $("#moveUser").click(function(){
      		    	var length = $("input[name='checkbox']:checked").size();
      				if (length == 0){
      					alert("請至少選擇一條記錄進行操作");
      					return;
      				}
      				$("#userGroupModal").modal();
      		    });
      		    
      		    //模態框打開時，將值存入隱藏域中
      		    $("#userGroupModal").on('show.bs.modal',function(){
      		    	var group = $("#group").val();
      		    	var role = $("#role").val();
      		    	$("#roleId").val(role);
      		    	$("#groupId").val(group);
      		    });
      		    
      			//模態框關閉時，取消隱藏域保存值
      		    $("#userGroupModal").on('hidden.bs.modal',function(){
      		    	$("#roleId").val("");
      		    	$("#groupId").val("");
      		    });
      			
      			$("#group").change(function(){
      				var group = $(this).val();
      				$("#groupId").val(group);
      			});
      			
      			$("#role").change(function(){
      				var role = $(this).val();
      				$("#roleId").val(role);
      			});
      			
      			//保存用戶組信息
      			$("#updateGroup").click(function(){
      				if (confirm("確認將選中用戶轉移到改組？")){
      					$("#submitForm").ajaxSubmit({
      						url : "${ctx }/user/changegroup",
      						type : "POST",
      		   				dataType : "text",
      		   				success : function(data){
      		   					switch (data){
      		   					case "notexist" :
      		   						alert("該分組或角色不存在");
      		   						break;
      		   					case "success" :
      		   						window.location.reload(); 
      		   						break;
      		   					case "error" :
      		   						alert("更新用戶組失敗");
      		   						break;
      		   						default:
      		   							alert(data);
      		   					}
      		   				}
      					});
      				}
      			});
      			
      			$("#modifyPwd").click(function(){
      				var length = $("input[name='checkbox']:checked").size();
      				if (length == 1){
      					var id = $("input[name='checkbox']:checked").val();
      					$("#userId").val(id);
	      				$("#userPwdModal").modal();
      				}else{
      					alert("請最多選擇一條記錄進行操作");
      					
      				}
      			});
      			
      			$("#userPwdModal").on('hidden.bs.modal',function(){
      				$("#userId").val('');
      				$("#password").val('');
      				$("#passwordcfm").val('');
      				$("#updateNameInfo").html('');
      			});
      			
      			
      			$("#updatePwd").click(function(){
      				var password = $.trim($("#password").val());
      				var passwordcfm = $.trim($("#passwordcfm").val());
      				
      				if (password.length < 6){
      					$("#updateNameInfo").html("請輸入至少6位新密碼");
      					return;
      				}
      				
      				if (passwordcfm.length < 6){
      					$("#updateNameInfo").html("請確認至少6位新密碼");
      					return;
      				}
      				
      				if (password != passwordcfm){
      					$("#updateNameInfo").html("兩次密碼不一致");
      					return;
      				}
      				
      				$("#savePwdForm").ajaxSubmit({
  						url : "${ctx }/user/changepwd",
  						type : "POST",
  						clearForm : true,
  		   				resetForm : true,
  		   				dataType : "text",
  		   				success : function(data){
  		   					switch (data){
  		   					case "nullparam" :
  		   						$("#updateNameInfo").html("請確認新密碼是否有誤");
  		   						break;
  		   					case "differ" :
  		   						$("#updateNameInfo").html("兩次密碼不一致"); 
  		   						break;
  		   					case "faith" :
  		   						$("#updateNameInfo").html("更新密碼失敗");
  		   						break;
  		   					case "success" :
  		   						$("#updateNameInfo").html("密碼修改成功");
  		   						break;
  		   					case "error" :
  		   						$("#updateNameInfo").html("密碼修改出錯");
  		   						break;
  		   						default:
  		   							alert(data);
  		   					}
  		   				}
  					});
      				
      			});
      		    
      		});
      </script>
    </body>
</html>