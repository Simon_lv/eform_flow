<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	
	<table class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>
				<th>表單編號</th>				
				<th>公司</th>
				<th>部門</th>				
				<th>申請日期</th>								
				<th>預算年月</th>
				<th>預算總金額</th>
				<th>追加總金額</th>
				<td></td>			
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
				<tr>
					<td>${o.serialNo }</td>
					<td>${o.companyName}</td>					
					<td>${o.departmentName}</td>					
					<td><fmt:formatDate value="${o.requestDate}" type="date" pattern="yyyy-MM-dd" /></td>
					<td>${o.yearMonth}</td>
					<td>
						<c:if test="${empty o.additionalReferenceId }">
						<fmt:formatNumber value="${o.totalAmount }" currencySymbol="" type="currency"/>
						</c:if>
						<c:if test="${not empty o.additionalReferenceId}">
						<fmt:formatNumber value="${o.additionalAmount }" currencySymbol="" type="currency"/>
						</c:if>						
					</td>
					<td>
						<c:if test="${empty o.additionalReferenceId }">
						<fmt:formatNumber value="${o.additionalAmount }" currencySymbol="" type="currency"/>
						</c:if>
						<c:if test="${not empty o.additionalReferenceId}">
						<fmt:formatNumber value="${o.totalAmount }" currencySymbol="" type="currency"/>
						</c:if>
					</td>					
					<td>
						<!-- 審核歷程 -->									
						<c:if test="${o.status >= 0}">
							<a href="javascript:;" onclick="history(${o.workFlowId})">審核歷程</a>
						</c:if>
						<c:if test="${o.status == -1 && o.creator == sessionScope.loginUser.id }">
							<c:if test="${empty o.additionalReferenceId }">
							<a href="javascript:;" onclick="edit(${o.id}, false)">編輯</a>
							</c:if>
							<c:if test="${not empty o.additionalReferenceId }">
							<a href="javascript:;" onclick="edit(${o.id}, true)">編輯</a>
							</c:if>
						</c:if>
						<c:if test="${o.status == 0 && o.additionalCount == 0 && empty o.additionalReferenceId && o.creator == sessionScope.loginUser.id }">
						<a href="javascript:;" onclick="edit(${o.id}, true)">追加</a>
						</c:if>
						<a href="javascript:;" onclick="get(${o.id})">檢視</a>
					</td>
				</tr>		
			</c:forEach>
		</tbody>
	</table>
</div>
<my:pagination page="${page}" />