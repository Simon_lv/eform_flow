<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->	
		<input type="hidden" name="id1" id="id1" value="${param.id1}" />	
		<input type="hidden" name="id2" id="id2" value="${param.id2}" />			
		<div class="table-responsive">
			
			<div style="text-align: center;"><h2>${form.companyName}</h2></div>
			<div style="text-align: center;"><h3>部門固定成本預算表</h3></div>
			
			<table class="table">
				<tr>
					<td><label>部門：</label></td>
					<td>${budgetForm[0].cp.departmentName }</td>
					<td><label>編號：</label></td>
					<td>${budgetForm[0].cp.serialNo }</td>					
				</tr>
				<tr>
					<td><label>申請日期：</label></td>
					<td><fmt:formatDate value="${budgetForm[0].cp.requestDate}" type="date" pattern="yyyy-MM-dd" /></td>
					<td><label>預算年月：</label></td>
					<td>${budgetForm[0].cp.yearMonth }</td>
				</tr>				
			</table>
			<table class="table">
				<tr>
					<th colspan="4"><label>初次預算金額：</label></th>
				</tr>
				<tr>
					<td></td>
					<td><label>項目</label></td>
					<td><label>說明</label></td>
					<td><label>初次預算金額</label></td>					
				</tr>				
				
				<c:forEach items="${budgetForm[0].notAdditional }" var="na" varStatus="loop">
				<tr>
					<td>${loop.index+1 }</td>
					<td>${na.itemTitle }</td>
					<td>${na.itemDescription }</td>
					<td>${na.amount }</td>							
				</tr>
				</c:forEach>			
				
				<tr>
					<td colspan="3"><label>總計:新台幣</label></td>
					<td>						
						<div>
							${budgetForm[0].cp.totalAmount }
						</div>			
					</td>
				</tr>				
			</table>
			
			<c:forEach items="${budgetForm }" begin="1" var="bf">			
			<table class="table">
				<tr>
					<th colspan="4"><label>追加預算金額：</label></th>
				</tr>
				<tr>
					<td></td>
					<td><label>項目</label></td>
					<td><label>說明</label></td>
					<td><label>追加預算金額</label></td>					
				</tr>						
				<c:forEach items="${bf.isAdditional }" var="ia" varStatus="loop">
				<tr>
					<td>${loop.index+1 }</td>
					<td>${ia.itemTitle }</td>
					<td>${ia.itemDescription }</td>
					<td>${ia.amount }</td>					
				</tr>
				</c:forEach>				
				<tr>
					<td colspan="3"><label>總計:新台幣</label></td>
					<td>
						<div>${bf.cp.totalAmount }</div>
					</td>
				</tr>			
			</table>
			<table class="table">
				<tr>
					<td><label>追加預算原因說明：</label></td>					
				</tr>
				<tr>
					<td>${bf.cp.additionalReason }</td>
				</tr>
			</table>			
			</c:forEach>
				
			<table class="table">
				<tr>
					<td colspan="5"><label>初次預算簽核</label></td>
				</tr>
				<tr>
					<td><label>會簽(會計)</label></td>
					<td><label>總經理</label></td>
					<td><label>副總經理</label></td>
					<td><label>部門主管</label></td>
					<td><label>申請人</label></td>
				</tr>
				<tr>
					<td>${budgetForm[0].stamp['會計'].info}</td>
					<td>${budgetForm[0].stamp['總經理'].info}</td>
					<td>
						<c:if test="${empty budgetForm[0].stamp['部門主管'].info}">
							${budgetForm[0].stamp['副總經理2'].info}
						</c:if>
						<c:if test="${not empty budgetForm[0].stamp['部門主管'].info}">
							${budgetForm[0].stamp['副總經理'].info}
						</c:if>						
					</td>
					<td>
						${budgetForm[0].stamp['部門主管'].info}
						<c:if test="${empty budgetForm[0].stamp['部門主管'].info}">
							${budgetForm[0].stamp['副總經理'].info}
						</c:if>					
					</td>
					<td>${budgetForm[0].stamp['申請人'].info}</td>
				</tr>
				<c:forEach items="${budgetForm }" begin="1" var="bf">
				<tr>
					<td colspan="5"><label>追加預算簽核</label></td>
				</tr>
				<tr>
					<td><label>會簽(會計部)</label></td>
					<td><label>總經理</label></td>
					<td><label>副總經理</label></td>
					<td><label>部門主管</label></td>
					<td><label>申請人</label></td>
				</tr>
				<tr>
					<td>${bf.stamp['會計'].info}</td>
					<td>${bf.stamp['總經理'].info}</td>
					<td>
						<c:if test="${empty bf.stamp['部門主管'].info}">
							${bf.stamp['副總經理2'].info}
						</c:if>
						<c:if test="${not empty bf.stamp['部門主管'].info}">
							${bf.stamp['副總經理'].info}
						</c:if>						
					</td>
					<td>
						${bf.stamp['部門主管'].info}
						<c:if test="${empty bf.stamp['部門主管'].info}">
							${bf.stamp['副總經理'].info}
						</c:if>
					</td>
					<td>${bf.stamp['申請人'].info}</td>
				</tr>
				</c:forEach>				
			</table>			
		</div>
		<!-- 退件原因 -->
		<table class="table">
			<tr>
				<td>
					退件原因：<input id="memo" name="memo" size="30"/>
					<input type="button" value="退件" id="reject"/>
				</td>
			</tr>
		</table>

		<!-- flow button -->
		<input type="button" value="取消" id="abort" />
		
		<c:if test="${isVerify}">
			<input type="button" value="審核" id="verify"/>
		</c:if>
		
		<c:if test="${isCountersign}">
		<input type="button" value="會簽" id="countersign"/>
		</c:if>
		
		<c:if test="${isPass}">
			<input type="button" value="放行" id="pass"/>
		</c:if>			
</div>

<script type="text/javascript">
$(function(){
	$('#abort').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();	
			
		$.ajax({
			url : '${ctx}/budgetForm/abort',
			dataType : 'json',
			data : {
				'id' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#verify').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		
		$.ajax({
			url : '${ctx}/budgetForm/verify',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#countersign').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		
		$.ajax({
			url : '${ctx}/budgetForm/countersign',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#pass').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		
		$.ajax({
			url : '${ctx}/budgetForm/pass',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#reject').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		var memo = $("#memo").val();
		
		$.ajax({
			url : '${ctx}/budgetForm/reject',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2,
				'memo' : memo
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});		
});
</script>