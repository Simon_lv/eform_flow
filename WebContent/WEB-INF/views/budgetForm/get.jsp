<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->	
	<div class="table-responsive">
		
		<div style="text-align: center;"><h2>${form.companyName}</h2></div>
		<div style="text-align: center;"><h3>部門固定成本預算表</h3></div>
		
		<table class="table">
			<tr>
				<td><label>部門：</label></td>
				<td>${budgetForm[0].cp.departmentName }</td>
				<td><label>編號：</label></td>
				<td>${budgetForm[0].cp.serialNo }</td>					
			</tr>
			<tr>
				<td><label>申請日期：</label></td>
				<td><fmt:formatDate value="${budgetForm[0].cp.requestDate}" type="date" pattern="yyyy-MM-dd" /></td>
				<td><label>預算年月：</label></td>
				<td>${budgetForm[0].cp.yearMonth }</td>
			</tr>				
		</table>
		<table class="table">
			<tr>
				<th colspan="4"><label>初次預算金額：</label></th>
			</tr>
			<tr>
				<td></td>
				<td><label>項目</label></td>
				<td><label>說明</label></td>
				<td><label>初次預算金額</label></td>					
			</tr>				
			
			<c:forEach items="${budgetForm[0].notAdditional }" var="na" varStatus="loop">
			<tr>
				<td>${loop.index+1 }</td>
				<td>${na.itemTitle }</td>
				<td>${na.itemDescription }</td>
				<td>${na.amount }</td>							
			</tr>
			</c:forEach>			
			
			<tr>
				<td colspan="3"><label>總計:新台幣</label></td>
				<td>						
					<div>
						NT$							
						${budgetForm[0].cp.totalAmount }
					</div>			
				</td>
			</tr>				
		</table>
		
		<c:forEach items="${budgetForm }" begin="1" var="bf">			
		<table class="table">
			<tr>
				<th colspan="4"><label>追加預算金額：</label></th>
			</tr>
			<tr>
				<td></td>
				<td><label>項目</label></td>
				<td><label>說明</label></td>
				<td><label>追加預算金額</label></td>					
			</tr>						
			<c:forEach items="${bf.isAdditional }" var="ia" varStatus="loop">
			<tr>
				<td>${loop.index+1 }</td>
				<td>${ia.itemTitle }</td>
				<td>${ia.itemDescription }</td>
				<td>${ia.amount }</td>					
			</tr>
			</c:forEach>				
			<tr>
				<td colspan="3"><label>總計:新台幣</label></td>
				<td>
					<div>NT$ ${bf.cp.totalAmount }</div>
				</td>
			</tr>			
		</table>
		<table class="table">
			<tr>
				<td><label>追加預算原因說明：</label></td>					
			</tr>
			<tr>
				<td>${bf.cp.additionalReason }</td>
			</tr>
		</table>			
		</c:forEach>
			
		<table class="table">
			<tr>
				<td colspan="5"><label>初次預算簽核</label></td>
			</tr>
			<tr>
				<td><label>會簽(會計)</label></td>
				<td><label>總經理</label></td>
				<td><label>副總經理</label></td>
				<td><label>部門主管</label></td>
				<td><label>申請人</label></td>
			</tr>
			<tr>
				<td>${budgetForm[0].stamp['會計'].info}</td>
				<td>${budgetForm[0].stamp['總經理'].info}</td>
				<td>
					<c:if test="${empty budgetForm[0].stamp['部門主管'].info}">
						${budgetForm[0].stamp['副總經理2'].info}
					</c:if>
					<c:if test="${not empty budgetForm[0].stamp['部門主管'].info}">
						${budgetForm[0].stamp['副總經理'].info}
					</c:if>						
				</td>
				<td>
					${budgetForm[0].stamp['部門主管'].info}
					<c:if test="${empty budgetForm[0].stamp['部門主管'].info}">
						${budgetForm[0].stamp['副總經理'].info}
					</c:if>					
				</td>
				<td>${budgetForm[0].stamp['申請人'].info}</td>
			</tr>
			<c:forEach items="${budgetForm }" begin="1" var="bf">
			<tr>
				<td colspan="5"><label>追加預算簽核</label></td>
			</tr>
			<tr>
				<td><label>會簽(會計)</label></td>
				<td><label>總經理</label></td>
				<td><label>副總經理</label></td>
				<td><label>部門主管</label></td>
				<td><label>申請人</label></td>
			</tr>
			<tr>
				<td>${bf.stamp['會計'].info}</td>
				<td>${bf.stamp['總經理'].info}</td>
				<td>
					<c:if test="${empty bf.stamp['部門主管'].info}">
						${bf.stamp['副總經理2'].info}
					</c:if>
					<c:if test="${not empty bf.stamp['部門主管'].info}">
						${bf.stamp['副總經理'].info}
					</c:if>						
				</td>
				<td>
					${bf.stamp['部門主管'].info}
					<c:if test="${empty bf.stamp['部門主管'].info}">
						${bf.stamp['副總經理'].info}
					</c:if>
				</td>
				<td>${bf.stamp['申請人'].info}</td>
			</tr>
			</c:forEach>				
		</table>			
	</div>	
</div>