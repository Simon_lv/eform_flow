<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/budgetForm/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${id}" />
		<input type="hidden" name="workFlowId" value="${workFlowId}" />
		<input type="hidden" name="additionalReferenceId" value="${additionalReferenceId }"/>
		<input type="hidden" name="additional" value="${additional }"/>
		<!-- 退件重送 -->
		<input type="hidden" name="serialNo" value="${serialNo}" />		
		<div class="table-responsive">	
			<div style="text-align: center;"><h2 id="title"></h2></div>
			<div style="text-align: center;"><h3>部門固定成本預算表</h3></div>
			
			<table class="table">
				<tr>
					<td>
						<select id="companyId" name="companyId">
                           	<c:forEach items="${sessionScope.company}" var="company">
                           	<option value="${company.id}"
                           		<c:if test="${cp.companyId == company.id}">selected</c:if>
                           		>${company.name}</option>
                           	</c:forEach>
                        </select>
					</td>
					<td><label>部門：</label></td>
					<td>
						<span id="department">
						<c:if test="${not empty cp}">${cp.departmentName }</c:if>
						</span>
						<input type="hidden" id="requestDepartment" name="requestDepartment" value="${cp.requestDepartment}" />
					</td>
					<td><label>編號：</label></td>
					<td>${budgetForm[0].cp.serialNo }</td>					
				</tr>
				<tr>
					<td><label>申請日期：</label></td>
					<td><fmt:formatDate value="${budgetForm[0].cp.requestDate}" type="date" pattern="yyyy-MM-dd" /></td>
					<td><label>預算年月：</label></td>
					<td>
						<c:if test="${additional }">
						${budgetForm[0].cp.yearMonth }
						</c:if>
						<c:if test="${!additional }">
						<input type="text" value="${budgetForm[0].cp.yearMonth }"
								onfocus="WdatePicker({yearMonth:'%y-%M', dateFmt:'yyyyMM'})"
								class="Wdate" name="yearMonth" id="yearMonth">
						</c:if>						
					</td>
				</tr>				
			</table>
			<table class="table">
				<tr>
					<th colspan="4"><label>初次預算金額：</label></th>
				</tr>
				<tr>
					<td></td>
					<td><label>項目</label></td>
					<td><label>說明</label></td>
					<td><label>初次預算金額</label></td>
					<c:if test="${!additional }">
					<td>						
						<input class="btn btn-success" type="button" value="增加欄位" onclick="addColumn()"/>						
					</td>
					</c:if>
				</tr>
				
				<c:if test="${additional }">
				<c:forEach items="${budgetForm[0].notAdditional }" var="na" varStatus="loop">
				<tr>
					<td>${loop.index+1 }</td>
					<td>${na.itemTitle }</td>
					<td>${na.itemDescription }</td>
					<td>${na.amount }</td>							
				</tr>
				</c:forEach>				
				</c:if>
				<c:if test="${!additional }">			
				<tbody id="detailBody">
				<c:if test="${empty budgetForm[0].notAdditional }">
				<c:forEach var="num" begin="1" end="9" step="1">
				<tr>
					<td>${num }</td>
					<td><input name="itemTitle" value=""/></td>
					<td><input name="itemDescription" value=""/></td>
					<td><input type="number" name="amount" value="0" onblur="doTotal()"></td>
					<td></td>
				</tr>
				</c:forEach>
				</c:if>
				<c:if test="${not empty budgetForm[0].notAdditional }">
				<c:forEach items="${budgetForm[0].notAdditional }" var="na" varStatus="loop">
				<tr>
					<td>${loop.index+1 }</td>
					<td><input name="itemTitle" value="${na.itemTitle }"/></td>
					<td><input name="itemDescription" value="${na.itemDescription }"/></td>
					<td><input type="number" name="amount" value="${na.amount }" onblur="doTotal()"/></td>
					<td><input class="btn" type="button" value="刪除" onclick="deleteDetail('${na.id}', '${id }', '${additional }')"/></td>			
				</tr>
				</c:forEach>
				</c:if>				
				</tbody>
				</c:if>
				<tr>
					<td colspan="3"><label>總計:新台幣</label></td>
					<td>
						<c:if test="${additional }">
						<div>
							NT$ ${budgetForm[0].cp.totalAmount }
						</div>
						</c:if>
						<c:if test="${!additional }">
						<div id="totalDiv"></div>
						<input type="hidden" name="totalAmount" value="${budgetForm[0].cp.totalAmount }"/>
						</c:if>						
					</td>
				</tr>				
			</table>
			
			<c:set var="count" value="0"/>
			<c:if test="${fn:length(budgetForm) > 2 }">
			<c:forEach items="${budgetForm }" begin="1" end="${fn:length(budgetForm)-1 }" var="bf" varStatus="loop">
			<c:set var="count" value="${loop.index }"/>			
			<table class="table">
				<tr>
					<th colspan="4"><label>追加預算金額：</label></th>
				</tr>
				<tr>
					<td></td>
					<td><label>項目${end }</label></td>
					<td><label>說明</label></td>
					<td><label>追加預算金額</label></td>									
				</tr>						
				<c:forEach items="${bf.isAdditional }" var="ia" varStatus="loop">
				<tr>
					<td>${loop.index+1 }</td>
					<td>${ia.itemTitle }</td>
					<td>${ia.itemDescription }</td>
					<td>${ia.amount }</td>					
				</tr>
				</c:forEach>				
				<tr>
					<td colspan="3"><label>總計:新台幣</label></td>
					<td>
						<div>NT$ ${bf.cp.totalAmount }</div>
					</td>
				</tr>			
			</table>
			<table class="table">
				<tr>
					<td><label>追加預算原因說明：</label></td>					
				</tr>
				<tr>
					<td>${bf.cp.additionalReason }</td>
				</tr>
			</table>			
			</c:forEach>
			</c:if>
			
			<c:if test="${additional }">
			<table class="table">
				<tr>
					<th colspan="4"><label>追加預算金額：</label></th>
				</tr>
				<tr>
					<td></td>
					<td><label>項目</label></td>
					<td><label>說明</label></td>
					<td><label>追加預算金額</label></td>
					<td><input class="btn btn-success" type="button" value="增加欄位" onclick="addColumn()"/></td>
				</tr>
				
				<tbody id="detailBody">
				
				<c:if test="${empty budgetForm[count+1].isAdditional }">
				<c:forEach var="num" begin="1" end="9" step="1">
				<tr>
					<td>${num }</td>
					<td><input name="itemTitle" value=""/></td>
					<td><input name="itemDescription" value=""/></td>
					<td><input type="number" name="amount" value="0" onblur="doTotal()"></td>
					<td></td>
				</tr>
				</c:forEach>
				</c:if>
				<c:if test="${not empty budgetForm[count+1].isAdditional }">
				<c:forEach items="${budgetForm[count+1].isAdditional }" var="ia" varStatus="loop">
				<tr>
					<td>${loop.index+1 }</td>
					<td><input name="itemTitle" value="${ia.itemTitle }"/></td>
					<td><input name="itemDescription" value="${ia.itemDescription }"/></td>
					<td><input type="number" name="amount" value="${ia.amount }"></td>
					<td><input class="btn" type="button" value="刪除" onclick="deleteDetail('${ia.id}','${id }', '${additional }')"/></td>
				</tr>
				</c:forEach>
				</c:if>
				
				</tbody>
				<tr>
					<td colspan="3"><label>總計:新台幣</label></td>
					<td>
						<div id="totalDiv"></div>
						<input type="hidden" name="totalAmount" value="${budgetForm[count+1].cp.totalAmount }"/>
					</td>
				</tr>			
			</table>
			<table class="table">
				<tr>
					<td colspan="5"><label>追加預算原因說明：</label></td>
				</tr>
				<tr>
					<td colspan="5"><textarea name="additionalReason" style="width: 70%;">${budgetForm[count+1].cp.additionalReason }</textarea></td>
				</tr>
				<tr>
					<td colspan="5"><label>初次預算簽核</label></td>
				</tr>
				<tr>
					<td><label>會簽(會計部)</label></td>
					<td><label>總經理</label></td>
					<td><label>副總經理</label></td>
					<td><label>部門主管</label></td>
					<td><label>申請人</label></td>
				</tr>
				<tr>
					<td>${budgetForm[0].stamp['會計'].info}</td>
					<td>${budgetForm[0].stamp['總經理'].info}</td>
					<td>${budgetForm[0].stamp['副總經理'].info}</td>
					<td>${budgetForm[0].stamp['部門主管'].info}</td>
					<td>${budgetForm[0].stamp['申請人'].info}</td>
				</tr>
				<c:if test="${fn:length(budgetForm) > 2 }">
				<c:forEach items="${budgetForm }" begin="1" end="${fn:length(budgetForm)-1 }" var="bf" varStatus="loop">
				<tr>
					<td colspan="5"><label>追加預算簽核</label></td>
				</tr>
				<tr>
					<td><label>會簽(會計)</label></td>
					<td><label>總經理</label></td>
					<td><label>副總經理</label></td>
					<td><label>部門主管</label></td>
					<td><label>申請人</label></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>${empty cp.creatorName ? sessionScope.employee.name : cp.creatorName}</td>
				</tr>
				</c:forEach>
				</c:if>
			</table>
			</c:if>
			<c:if test="${additional }">
			<table class="table">
				<tr>
					<td colspan="5"><label>追加預算簽核</label></td>
				</tr>
				<tr>
					<td><label>會簽(會計)</label></td>
					<td><label>總經理</label></td>
					<td><label>副總經理</label></td>
					<td><label>部門主管</label></td>
					<td><label>申請人</label></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>${empty cp.creatorName ? sessionScope.employee.name : cp.creatorName}</td>
				</tr>
			</table>
			</c:if>
		</div>
	</form>
	<input type="button" value="保存" id="save" />	
	<input type="button" value="送審" id="submit"/>
</div>

<script type="text/javascript">

function doTotal(){	
	var total = 0;
	
	$('#detailBody').find('input[name=amount]').map(function(){
		var amount = $(this).val();
		if(amount != ''){
			total += parseInt(amount, 10);
		}
	});	
	$('input[name=totalAmount]').val(total);
	$('#totalDiv').html(total);
}

function addColumn(){
	var html = "";	
	html += '<tr>';
	html += '<td></td>';
	html += '<td><input name="itemTitle" value=""/></td>';
	html += '<td><input name="itemDescription" value=""/></td>';
	html += '<td><input type="number" name="amount" value="0" onblur="doTotal()"></td>';
	html += '<td><input class="btn delete" type="button" value="刪除"/></td>';
	html += '</tr>';
	
	$('#detailBody').append(html);
}

$(function(){
	
	$('#save').click(function() {		
		if(checkValidate()){
			$("#editForm").ajaxSubmit({
	            success: function(e) {
	                alert(e.message), $("#myModal").modal("toggle");
	                $('#search').click();
	            },
	            error: function() {
	                alert("網路連接失敗，請聯繫管理員");
	            }
	        });
		}
	});
	
	$('#detailBody').on('click', '.delete', function(){
		$(this).parent().parent().remove();
		doTotal();
	});
	
	$('#submit').click(function() {
		if(checkValidate()){
			$('#editForm').attr('action', '${ctx}/budgetForm/submit');
			$("#editForm").ajaxSubmit({
	            success: function(e) {
	            	if(e.code == "0000") {
	            		alert(e.message), $("#myModal").modal("toggle");
		                $('#search').click();
	            	}
	            	else {
	            		alert(e.message);
	            	}	                
	            },
	            error: function() {
	                alert("網路連接失敗，請聯繫管理員");
	            }
	        });			
		}
	});
	
	doTotal();
	
	// default title
	$("#title").text($('#companyId').find("option:selected").text());
	
	if($("#department").text().trim() == "") {
		getUserInfo($('#companyId').val());
	}
});

function checkValidate(){
	var pass = true;
	var html = "<font class='warning' color='red'>請輸入此欄位</font>";
	
	if($('input[name=yearMonth]').val() == ''){
		pass = false;
		$('input[name=yearMonth]').parent().find('.warning').remove();
		$('input[name=yearMonth]').parent().append(html);		
	}else{
		$('input[name=yearMonth]').parent().find('.warning').remove();
	}
	
	$('#detailBody').find('[name=amount]').each(function(){
		var item = $(this).closest('td').prev().prev().find('input[name=itemTitle]');		
		var amount = $(this);
		
		if((amount.val() != '0' && amount.val() != '')  && item.val() == ''){
			pass = false;
			item.parent().find('.warning').remove();
			item.parent().append(html);
		}else{
			item.parent().find('.warning').remove();
		}
		if(amount.val() == '0' && item.val() != ''){
			pass = false;
			amount.parent().find('.warning').remove();
			amount.parent().append(html);
		}else{
			amount.parent().find('.warning').remove();
		}
	});
	
	if(!pass){
		alert("表單未填寫完整!");
	}
	
	doTotal();
	return pass;
}

function getUserInfo(cid) {
	$.ajax({
		url : '${ctx}/employee/userInfo',
		dataType : 'json',
		data : {
			'id' : cid
		},
		success : function(data) {
			var dept = data.split(",");
			$("#department").text(dept[1]);
			$("#requestDepartment").val(dept[0]);
		},
		error : function() {
			alert('網絡連接失敗，請聯繫管理員');
		}
	});
}
</script>