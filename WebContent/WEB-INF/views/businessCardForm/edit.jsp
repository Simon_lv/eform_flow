<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/businessCardForm/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${cp.id}" />
		<input type="hidden" name="workFlowId" value="${cp.workFlowId}" />
		<!-- 退件重送 -->
		<input type="hidden" name="serialNo" value="${cp.serialNo}" />
		<div class="table-responsive">
			
			<div style="text-align: center;"><h2 id="title"></h2></div>
			<div style="text-align: center;"><h3>公司卡消費通知單 Consumption Notice</h3></div>
			
			<table class="table">
				<tr>
					<td colspan="4" style="text-align: center;">請款者 Requester Profile</td>					
				</tr>
				<tr>
					<td>
						<select id="companyId" name="companyId">
                           	<c:forEach items="${sessionScope.company}" var="company">
                           	<option value="${company.id}"
                           		<c:if test="${cp.companyId == company.id}">selected</c:if>
                           		>${company.name}</option>
                           	</c:forEach>
                        </select>
					</td>
					<td class="td3">申請日期<br/>Date Requested</td>
					<td><fmt:formatDate value="${cp.requestDate}" type="date" pattern="yyyy-MM-dd" /></td>
					<td class="td3">部門<br/>Division/Department</td>
					<td>
						<span id="department">
						<c:if test="${not empty cp}">${cp.departmentName }</c:if>
						</span>
						<input type="hidden" id="requestDepartment" name="requestDepartment" value="${cp.requestDepartment}" />
					</td>
				</tr>
				<tr>
					<td class="td3">聯絡人<br/>Contact person</td>					
					<td><input name="contact" value="${cp.contact }"/></td>
					<td class="td3">電話<br/>Tel</td>					
					<td><input name="contactTel" value="${cp.contactTel }"/></td>
				</tr>
				<tr>
					<td colspan="4">
						<table class="table" style="width: 100%">
							<tr>
								<td class="td2">憑證日期<br/>Date</td>
								<td class="td2">項目<br/>Item</td>
								<td class="td2">費用說明<br/>Description</td>
								<td class="td2">金額<br/>Amount</td>
								<td class="td2"><input type="button" id="addColumn" class="btn btn-danger" value="增加"></td>
							</tr>
							<tbody id="receiptData">
							<c:if test="${empty receipts }">
							<tr>
								<td>									
									<input type="text" value=""
											onfocus="WdatePicker({costDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
											class="Wdate" name="costDate" id="costDate">
								</td>
								<td><input name="costItem" value=""/></td>
								<td><input name="description"/></td>
								<td><input type="number" name="amount" onblur="doTotal()"/></td>
								<td></td>
							</tr>
							</c:if>							
							<c:if test="${not empty receipts }">
							<c:forEach items="${receipts }" var="rc">
							<tr>
								<td>									
									<input type="text" value="<fmt:formatDate value="${rc.costDate}" type="date" pattern="yyyy-MM-dd" />"
											onfocus="WdatePicker({costDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
											class="Wdate" name="costDate" id="costDate">
								</td>
								<td><input name="costItem" value="${rc.costItem }"/></td>
								<td><input name="description" value="${rc.description }"/></td>
								<td>
									<fmt:parseNumber value="${rc.amount}" var="amount"/>
									<input type="number" name="amount"  
										value="<fmt:formatNumber value="${amount}" pattern="#.##"/>" onblur="doTotal()"/>									
								</td>
								<td><input class="btn" type="button" value="刪除" onclick="deleteReceipt('${rc.id}', '${cp.id }')"/></td>								
							</tr>
							</c:forEach>
							</c:if>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="3" style="text-align: center;" class="td2">總計Total</td>
					<td style="text-align: center;" class="td2">
						<input type="hidden" id="totalAmount" name="totalAmount" value="${cp.totalAmount }">
									
						<div id="totalDiv"><fmt:formatNumber value="${cp.totalAmount }" currencySymbol="NT$" type="currency"/></div>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center;" class="td1">部門主管</td>
					<td colspan="2" style="text-align: center;" class="td1">申請人</td>
				</tr>
				<tr>
					<td colspan="2"></td>
					<td colspan="2" style="text-align: center;">${empty cp.creatorName ? sessionScope.employee.name : cp.creatorName }</td>
				</tr>
				<tr>
					<td colspan="4" style="text-align: center;">請檢附已書名統一編號的發票或是收據</td>
				</tr>
			</table>
		</div>
	</form>
	<input type="button" value="保存" id="save" />	
	<input type="button" value="送審" id="submit"/>
</div>

<script type="text/javascript">

doTotal();

function doTotal(){	
	var total = 0;
	
	$('#receiptData').find('input[name=amount]').each(function(){
		var money = $(this).val();
		if(money != ''){
			total += parseInt(money,10);
		}
	});	
	
	$('#totalDiv').html("NT$"+total);
	$('#totalAmount').val(total);
}

$(function(){
	
	$('#addColumn').click(function(){
		var count = 0;
		var html = "";
		html = '<tr>';
        html += '<td><input type="text" value="" onfocus="WdatePicker({costDate'+count+':';
        html += "'%y-%M-%d'";
        html += ", dateFmt:";
        html += "'yyyy-MM-dd'";
        html += '})" class="Wdate" name="costDate" id="costDate'+count+'"></td>';
        html += '<td><input name="costItem"/></td>';
        html += '<td><input name="description"/></td>';
        html += '<td><input type="number" name="amount" onblur="doTotal()"/></td>';
        html += '<td><input class="btn delete" type="button" value="刪除"></td>';
        html += '</tr>';
        count++;        
        
        $('#receiptData').append(html);
	});
	
	$('#receiptData').on('click', '.delete', function(){
		$(this).parent().parent().remove();
		doTotal();
	});
	
	$('#save').click(function() {		
		if(checkValidate()){			
			$("#editForm").ajaxSubmit({
	            success: function(e) {
	            	if(e.code == "0000") {
	            		alert(e.message), $("#myModal").modal("toggle");
		                $('#search').click();
	            	}
	            	else {
	            		alert(e.message);
	            	}
	            },
	            error: function() {
	                alert("網路連接失敗，請聯繫管理員");
	            }
	        });
		}
	});
	
	$('#submit').click(function() {
		if(checkValidate()){
			$('#editForm').attr('action', '${ctx}/businessCardForm/submit');
			$("#editForm").ajaxSubmit({
	            success: function(e) {
	                alert(e.message), $("#myModal").modal("toggle");
	                $('#search').click();
	            },
	            error: function() {
	                alert("網路連接失敗，請聯繫管理員");
	            }
	        });			
		}
	});
	
	$('#companyId').change(function() {
		var cid = $(this).val();
		var name = $(this).find("option:selected").text();
		$("#title").text(name);
		// department
		getUserInfo(cid);
	});
	
	// default title
	$("#title").text($('#companyId').find("option:selected").text());
	
	if($("#department").text().trim() == "") {
		getUserInfo($('#companyId').val());
	}
});

function checkValidate(){
	var pass = true;
	var html = "<font class='warning' color='red'>請輸入此欄位</font>";
	
	if($('input[name=contact]').val() == ''){
		pass = false;
		$('input[name=contact]').parent().find('.warning').remove();
		$('input[name=contact]').parent().append(html);		
	}else{
		$('input[name=contact]').parent().find('.warning').remove();
	}
	if($('input[name=contactTel]').val() == ''){
		pass = false;
		$('input[name=contactTel]').parent().find('.warning').remove();
		$('input[name=contactTel]').parent().append(html);		
	}else{
		$('input[name=contactTel]').parent().find('.warning').remove();
	}
	
	$('#receiptData').find('input[name=costDate]').each(function(){
		if($(this).val() == ''){
			pass = false;
			$(this).parent().find('.warning').remove();
			$(this).parent().append(html);		
		}else{
			$(this).parent().find('.warning').remove();
		}
	});
	
	$('#receiptData').find('input[name=costItem]').each(function(){
		if($(this).val() == ''){
			pass = false;
			$(this).parent().find('.warning').remove();
			$(this).parent().append(html);		
		}else{
			$(this).parent().find('.warning').remove();
		}	
	});
	
	$('#receiptData').find('input[name=amount]').each(function(){
		if($(this).val() == ''){
			pass = false;
			$(this).parent().find('.warning').remove();
			$(this).parent().append(html);		
		}else{
			$(this).parent().find('.warning').remove();
		}	
	});	
	
	if(!pass){
		alert("表單未填寫完整!");
	}
	
	return pass;
}

function getUserInfo(cid) {
	$.ajax({
		url : '${ctx}/employee/userInfo',
		dataType : 'json',
		data : {
			'id' : cid
		},
		success : function(data) {
			console.log("data=" + data);
			var dept = data.split(",");
			$("#department").text(dept[1]);
			$("#requestDepartment").val(dept[0]);
		},
		error : function() {
			alert('網絡連接失敗，請聯繫管理員');
		}
	});
}
</script>