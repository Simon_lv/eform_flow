<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->	
	<input type="hidden" name="id1" id="id1" value="${param.id1}" />	
	<input type="hidden" name="id2" id="id2" value="${param.id2}" />
	<div class="table-responsive">
		
		<div style="text-align: center;"><h2>${form.companyName}</h2></div>
		<div style="text-align: center;"><h3>公司卡消費通知單 Consumption Notice</h3></div>
		
		<div style="text-align: left;"><label>編號&nbsp;</label>${form.serialNo}</div>
		<table class="table">
			<tr>
				<td colspan="4" style="text-align: center;">請款者 Requester Profile</td>					
			</tr>
			<tr>
				<td class="td3">申請日期<br/>Date Requested</td>
				<td><fmt:formatDate value="${form.requestDate}" type="date" pattern="yyyy-MM-dd" /></td>
				<td class="td3">部門<br/>Division/Department</td>
				<td>${form.companyName }-${form.departmentName }</td>
			</tr>
			<tr>
				<td class="td3">聯絡人<br/>Contact person</td>					
				<td>${form.contact }</td>
				<td class="td3">電話<br/>Tel</td>					
				<td>${form.contactTel }</td>
			</tr>							
			<c:forEach items="${receipts }" var="rc">
			<tr>
				<td>
					<input type="hidden" name="receiptId" value="${rc.id }"/>
					<fmt:formatDate value="${rc.costDate}" type="date" pattern="yyyy-MM-dd" />
				</td>
				<td>${rc.costItem }</td>
				<td>${rc.description }</td>
				<td><fmt:formatNumber value="${rc.amount}" pattern="###,###"/></td>								
			</tr>
			</c:forEach>				
			<tr>
				<td colspan="3" style="text-align: center;" class="td2">總計Total</td>
				<td style="text-align: center;" class="td2">
					<input type="hidden" id="totalAmount" name="totalAmount" value="${form.totalAmount }">									
					<div id="totalDiv"><fmt:formatNumber value="${form.totalAmount }" type="currency" maxFractionDigits="0"/></div>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<table width="100%">
						<tr>
							<c:if test="${not empty stamp['總經理']}">
							<td style="text-align: center;">總經理</td>
							</c:if>
							<td style="text-align: center;" class="td1">部門主管</td>
							<td style="text-align: center;" class="td1">申請人</td>
						</tr>
						<tr>
							<c:if test="${not empty stamp['總經理']}">
							<td style="text-align: center;">${stamp['總經理'].info}</td>							
							</c:if>
							<td style="text-align: center;">${stamp['部門主管'].info}${stamp['副總經理'].info}</td>
							<td style="text-align: center;">${stamp['申請人'].info}</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="4" style="text-align: center;">請檢附已書名統一編號的發票或是收據</td>
			</tr>			
		</table>
		<!-- 退件原因 -->
		<table class="table">
			<tr>
				<td>
					退件原因：<input id="memo" name="memo" size="30"/>
					<input type="button" value="退件" id="reject"/>
				</td>
			</tr>
		</table>
		
		<!-- flow button -->
		<input type="button" value="取消" id="abort" />
		
		<c:if test="${isVerify}">
			<input type="button" value="審核" id="verify"/>
		</c:if>
		
		<c:if test="${isCountersign}">
		<input type="button" value="會簽" id="countersign"/>
		</c:if>
		
		<c:if test="${isPass}">
			<input type="button" value="放行" id="pass"/>
		</c:if>		
	</div>	
</div>

<script type="text/javascript">
$(function(){
	$('#abort').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
			
		$.ajax({
			url : '${ctx}/businessCardForm/abort',
			dataType : 'json',
			data : {
				'id' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#verify').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		
		$.ajax({
			url : '${ctx}/businessCardForm/verify',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#countersign').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		
		$.ajax({
			url : '${ctx}/businessCardForm/countersign',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#pass').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		
		$.ajax({
			url : '${ctx}/businessCardForm/pass',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#reject').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		var memo = $("#memo").val();
		
		$.ajax({
			url : '${ctx}/businessCardForm/reject',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2,
				'memo' : memo
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
});
</script>