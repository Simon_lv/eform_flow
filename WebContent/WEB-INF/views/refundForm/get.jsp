<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->	
	<div class="table-responsive">
		
		<div style="text-align: center;"><h2>${form.companyName}</h2></div>
		<div style="text-align: center;"><h3>退款申請單</h3></div>
		
		<table class="table">
			<tr>					
				<td>填表日期：</td>
				<td>${cp.requestDate}</td>
				<td>編號：</td>
				<td>${cp.serialNo}</td>
				<td>遊戲：</td>
				<td>${cp.gameName}</td>
			</tr>
		</table>
					
		<table class="table">
			<tr>
				<td rowspan="10">會員資料</td>	
				<td>遊戲帳號</td>		
				<td>${cp.gameAccount}</td>
				<td>說明</td>		
			</tr>
			<tr>
				<td>儲值時間</td>
				<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${cp.payTime}" /></td>
				<td rowspan="9">
					<div>信件標題:${cp.subject }</div>
					<div>退款原因:
						<div>${cp.reason}</div>
					</div>
				</td>					
			</tr>
			<tr>
				<td>訂單編號</td>
				<td>${cp.orderId}</td>
			</tr>
			<tr>
				<td>儲值金額</td>
				<td>${cp.twCoin}</td>
			</tr>
			<tr>
				<td>金流通路</td>
				<td>${cp.payType}</td>
			</tr>
			<tr>
				<td>會員姓名</td>
				<td>${cp.userName}</td>
			</tr>
			<tr>
				<td>身分證字號</td>
				<td>${cp.userId}</td>
			</tr>
			<tr>
				<td>聯絡電話</td>
				<td>${cp.contactTel}</td>
			</tr>
			<tr>
				<td>聯絡地址</td>
				<td>${cp.contactAddress}</td>
			</tr>
			<tr>
				<td>退款方式</td>
				<td>
					<div><input type="checkbox" name="paymentWay" value="1" <c:if test="${fn:contains(cp.paymentWay, '1')}">checked="checked"</c:if>/>匯款帳號</div>
					<div><input type="checkbox" name="paymentWay" value="2" <c:if test="${fn:contains(cp.paymentWay, '2')}">checked="checked"</c:if>/>信用卡刷退</div>
					<div><input type="checkbox" name="paymentWay" value="3" <c:if test="${fn:contains(cp.paymentWay, '3')}">checked="checked"</c:if>/>收回折讓單</div>
					<div><input type="checkbox" name="paymentWay" value="4" <c:if test="${fn:contains(cp.paymentWay, '4')}">checked="checked"</c:if>/>填請款單</div>
					<div><input type="checkbox" name="paymentWay" value="5" <c:if test="${fn:contains(cp.paymentWay, '5')}">checked="checked"</c:if>/>收取手續費</div>
					<div><input type="checkbox" name="paymentWay" value="9" <c:if test="${fn:contains(cp.paymentWay, '9')}">checked="checked"</c:if>/>其他:
					${cp.remark}</div>
				</td>
			</tr>
			<tr>
				<td>出款日</td>
				<td><fmt:formatDate value="${cp.remitDate}" type="date" pattern="yyyy-MM-dd" /></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>
					<input type="checkbox" name="notifyAgency" value="1" <c:if test="${cp.notifyAgency == 1 }">checked="checked"</c:if>>通知金流商，並確認完成
				</td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2">部門主管</td>
				<td colspan="2">申請人</td>
			</tr>
			<tr>
				<td colspan="2">${stamp['部門主管'].info}</td>
				<td colspan="2">${stamp['申請人'].info}</td>
			</tr>
		</table>
		<hr/>
		<table class="table">
			<tr>
				<td rowspan="4">退款流程</td>
				<td>部門</td>
				<td>處理事項</td>
				<td>經辦</td>
				<td>部門主管覆核</td>
			</tr>
			<tr>
				<td>營運</td>
				<td>
					<div>
						<input type="checkbox" name="opsProcess" value="1" <c:if test="${fn:contains(cp.opsProcess, '1')}">checked="checked"</c:if>/>客服資料確認
					</div>
					<div>
						<input type="checkbox" name="opsProcess" value="2" <c:if test="${fn:contains(cp.opsProcess, '2')}">checked="checked"</c:if>/>收回遊戲幣及道具
					</div>
					<div>
						<input type="checkbox" name="opsProcess" value="9" <c:if test="${fn:contains(cp.opsProcess, '9')}">checked="checked"</c:if>/>其他:
						${cp.opsProcessMemo}
					</div>
				</td>
				<td>${stamp['產品'].info}</td>
				<td>${stamp['營運主管'].info}</td>
			</tr>
			<tr>
				<td>財務</td>
				<td>
					<div><input type="checkbox" name="finProcess" value="1" <c:if test="${fn:contains(cp.finProcess, '1')}">checked="checked"</c:if>/>客服資料確認</div>
					<div><input type="checkbox" name="finProcess" value="2" <c:if test="${fn:contains(cp.finProcess, '2')}">checked="checked"</c:if>/>後台退款操作</div>
					<div><input type="checkbox" name="finProcess" value="3" <c:if test="${fn:contains(cp.finProcess, '3')}">checked="checked"</c:if>/>發票作廢或開立折讓單</div>
					<div>
						<input type="checkbox" name="finProcess" value="4" <c:if test="${fn:contains(cp.finProcess, '4')}">checked="checked"</c:if>/>開立手續費發票 
						<font color="red">發票號碼</font>${cp.receiptNo }
					</div>
					<div>
						<input type="checkbox" name="finProcess" value="9" <c:if test="${fn:contains(cp.finProcess, '9')}">checked="checked"</c:if>/>其他:
						${cp.finProcessMemo }
					</div>
					<div><input type="checkbox" name="finProcess" value="5" <c:if test="${fn:contains(cp.finProcess, '5')}">checked="checked"</c:if>/>轉會計</div>
				</td>
				<td>${stamp['出納'].info}</td>
				<td>${stamp['行政主管'].info}</td>
			</tr>
			<tr>
				<td>會計</td>
				<td>
					<div><input type="checkbox" name="acctProcess" value="1" <c:if test="${fn:contains(cp.acctProcess, '1')}">checked="checked"</c:if>/>退款或扣帳款</div>
					<div>
						<input type="checkbox" name="acctProcess" value="9" <c:if test="${fn:contains(cp.acctProcess, '9')}">checked="checked"</c:if>/>其他:
						${cp.acctProcessMemo }
					</div>
				</td>
				<td>${stamp['會計專員'].info}</td>
				<td>${stamp['會計'].info}</td>
			</tr>
		</table>
	</div>	
</div>

<script type="text/javascript">

$(function(){
	$('input[type=checkbox]').each(function(){
		$(this).attr("disabled", true);		
	});
});
</script>