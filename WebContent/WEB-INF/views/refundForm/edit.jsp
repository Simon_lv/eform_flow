<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/refundForm/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${cp.id}" />
		<input type="hidden" name="workFlowId" value="${cp.workFlowId}" />
		<!-- 退件重送 -->
		<input type="hidden" name="serialNo" value="${cp.serialNo}" />
		<div class="table-responsive">
			
			<div style="text-align: center;"><h2 id="title"></h2></div>
			<div style="text-align: center;"><h3>退款申請單</h3></div>
			
			<table class="table">
				<tr>		
					<td><label>公司：</label></td>
					<td>
						<select id="companyId" name="companyId">
                           	<c:forEach items="${companys}" var="company">
                           	<option value="${company.id}"
                           		<c:if test="${cp.companyId == company.id}">selected</c:if>
                           		>${company.name}</option>
                           	</c:forEach>
                        </select>
					</td>
					<td><label>填表日期：</label></td>
					<td></td>
					<td><label>編號：</label></td>
					<td></td>
					<td><label>遊戲：</label></td>
					<td>
						<select name="gameDataId" id="gameDataId">
							<option value="">請選擇遊戲</option>
							<c:forEach items="${allGameData}" var="a">
							<option value="${a.id}" <c:if test="${a.id == cp.gameDataId}">selected="selected"</c:if> > ${a.name}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
			</table>
						
			<table class="table">
				<tr>
					<td rowspan="10"><label>會員資料</label></td>	
					<td><label>遊戲帳號</label></td>		
					<td><input name="gameAccount" id="gameAccount" value="${cp.gameAccount }"/></td>
					<td><label>說明</label></td>		
				</tr>
				<tr>
					<td><label>儲值時間</label></td>
					<td>						
						<input type="text" value="<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${cp.payTime}" />" onfocus="WdatePicker({payTime:'%y-%M-%d 23:59:59', dateFmt:'yyyy-MM-dd HH:mm:ss'})"
								class="Wdate" name="payTime" id="payTime">								
					</td>
					<td rowspan="9">
						<div>
							<label>信件標題:</label>
							<input name="subject" value="${cp.subject }"/>
						</div>
						<div><label>退款原因:</label></div>
						<div>							
							<textarea style="width: 400px; height: 200px;" id="reason" name="reason">${cp.reason}</textarea>
						</div>
					</td>					
				</tr>
				<tr>
					<td><label>訂單編號</label></td>
					<td><input name="orderId" id="orderId" value="${cp.orderId }"/></td>
				</tr>
				<tr>
					<td><label>儲值金額</label></td>
					<td><input type="number" name="twCoin" id="twCoin" value="${cp.twCoin }"/></td>
				</tr>
				<tr>
					<td><label>金流通路</label></td>
					<td>
						<select name="payTypeId" id="payTypeId">
							<option value="">請選擇通路</option>
							<c:forEach items="${allPayType}" var="a">
							<option value="${a.id}" <c:if test="${a.id == cp.payTypeId}">selected="selected"</c:if> > ${a.name}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td><label>會員姓名</label></td>
					<td><input name="userName" id="userName" value="${cp.userName }"/></td>
				</tr>
				<tr>
					<td><label>身分證字號</label></td>
					<td><input name="userId" id="userId" value="${cp.userId }"/></td>
				</tr>
				<tr>
					<td><label>聯絡電話</label></td>
					<td><input name="contactTel" id="contactTel" value="${cp.contactTel }"/></td>
				</tr>
				<tr>
					<td><label>聯絡地址</label></td>
					<td><input name="contactAddress" id="contactAddress" value="${cp.contactAddress }" size="50"/></td>
				</tr>
				<tr>
					<td><label>退款方式</label></td>
					<td>
						<div><input type="checkbox" name="paymentWay" value="1" <c:if test="${fn:contains(cp.paymentWay, '1')}">checked="checked"</c:if>/>匯款帳號</div>
						<div><input type="checkbox" name="paymentWay" value="2" <c:if test="${fn:contains(cp.paymentWay, '2')}">checked="checked"</c:if>/>信用卡刷退</div>
						<div><input type="checkbox" name="paymentWay" value="3" <c:if test="${fn:contains(cp.paymentWay, '3')}">checked="checked"</c:if>/>收回折讓單</div>
						<div><input type="checkbox" name="paymentWay" value="4" <c:if test="${fn:contains(cp.paymentWay, '4')}">checked="checked"</c:if>/>填請款單</div>
						<div><input type="checkbox" name="paymentWay" value="5" <c:if test="${fn:contains(cp.paymentWay, '5')}">checked="checked"</c:if>/>收取手續費</div>
						<div><input type="checkbox" name="paymentWay" value="9" <c:if test="${fn:contains(cp.paymentWay, '9')}">checked="checked"</c:if>/>其他:
						<input name="remark" value="${cp.remark }"/></div>
					</td>
				</tr>
				<tr>
					<td><label>出款日</label></td>
					<td>
						<fmt:formatDate var="remitDate" value="${cp.remitDate}" type="date" pattern="yyyy-MM-dd" />
						<select id="remitDate" name="remitDate">
                           	<c:forEach items="${remitDateItems}" var="d">
                           	<option value="${d}"
                           		<c:if test="${remitDate == d}">selected</c:if>
                           		>${d}</option>
                           	</c:forEach>
                        </select>
					</td>
				</tr>	
				<tr>
					<td></td>
					<td></td>
					<td>
						<input type="checkbox" name="notifyAgency" value="1" <c:if test="${cp.notifyAgency == 1 }">checked="checked"</c:if>>通知金流商，並確認完成
					</td>
					<td></td>
				</tr>
			</table>
			<table class="table">
				<tr>
					<td><label>部門主管</label></td>
					<td><label>申請人</label></td>
				</tr>
				<tr>
					<td></td>
					<td>${empty cp.creatorName ? sessionScope.employee.name : cp.creatorName}</td>
				</tr>
			</table>					
		</div>
		<!-- 限定建檔單位 -->
		<input type="hidden" id="requestDepartment" name="requestDepartment" value="" />
	</form>
	<input type="button" value="保存" id="save" />	
	<input type="button" value="送審" id="submit"/>
</div>

<script type="text/javascript">

$(function(){
	keditor = KindEditor.create('textarea[name="reason"]', {
		minHeight : 200,
		minWidth : 400,
		resizeType : 0,
		resizeType : 1,
		langType : 'zh_TW',
		allowPreviewEmoticons : false,
		filterMode : false,
		allowFileManager : true,
		items : [ 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline', 'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist', 'insertunorderedlist', '|', 'image',
				'link', '|', 'source', 'fullscreen', '|', 'preview' ]
	});
	keditor.html($("#reason").val());
	
	$('#save').click(function() {	
		if(checkValidate()){
			$("#editForm").ajaxSubmit({
	            success: function(e) {
	                alert(e.message), $("#myModal").modal("toggle");
	                $('#search').click();
	            },
	            error: function() {
	                alert("網路連接失敗，請聯繫管理員");
	            }
	        });
		}
	});
	
	$('#submit').click(function() {
		if(checkValidate()){
			$('#editForm').attr('action', '${ctx}/refundForm/submit');
			$("#editForm").ajaxSubmit({
	            success: function(e) {
	            	if(e.code == "0000") {
	            		alert(e.message), $("#myModal").modal("toggle");
		                $('#search').click();
	            	}
	            	else {
	            		alert(e.message);
	            	}
	            },
	            error: function() {
	                alert("網路連接失敗，請聯繫管理員");
	            }
	        });			
		}
	});
	
	$('#companyId').change(function() {
		var name = $(this).find("option:selected").text();
		$("#title").text(name);		
	});
	
	// default title
	$("#title").text($('#companyId').find("option:selected").text());
	// department
	getUserInfo($('#companyId').val());	
		
});

function getUserInfo(cid) {
	$.ajax({
		url : '${ctx}/employee/userInfo',
		dataType : 'json',
		data : {
			'id' : cid
		},
		success : function(data) {
			console.log(data);
			var dept = data.split(",");
			
			$("#requestDepartment").val(dept[0]);						
		},
		error : function() {
			alert('網絡連接失敗，請聯繫管理員');
		}
	});
}

function checkValidate(){
	var pass = true;
	var html = "<font class='warning' color='red'>請輸入此欄位</font>";
	
	if($('#gameDataId').val() == ''){
		pass = false;
		$('#gameDataId').parent().find('.warning').remove();
		$('#gameDataId').parent().append(html);		
	}else{
		$('#gameDataId').parent().find('.warning').remove();
	}
	
	if($('input[name=subject]').val() == ''){
		pass = false;
		$('input[name=subject]').parent().find('.warning').remove();
		$('input[name=subject]').parent().append(html);		
	}else{
		$('input[name=subject]').parent().find('.warning').remove();
	}
	
	// sync
	keditor.sync("#reason");
	
	if($('textarea[name=reason]').val() == ''){
		pass = false;
		$('textarea[name=reason]').parent().find('.warning').remove();
		$('textarea[name=reason]').parent().append(html);		
	}else{
		$('textarea[name=reason]').parent().find('.warning').remove();
	}
	
	if($("#reason").val().length > 1500) {
		pass = false;
		$('textarea[name=reason]').parent().find('.warning').remove();
		$('textarea[name=reason]').parent().append("<font class='warning' color='red'>內容太長了喲~</font>");		
	}else{
		$('textarea[name=reason]').parent().find('.warning').remove();
	}
	
	if($('#gameAccount').val() == ''){
		pass = false;
		$('#gameAccount').parent().find('.warning').remove();
		$('#gameAccount').parent().append(html);		
	}else{
		$('#gameAccount').parent().find('.warning').remove();
	}
	
	if($('#payTime').val() == ''){
		pass = false;
		$('#payTime').parent().find('.warning').remove();
		$('#payTime').parent().append(html);		
	}else{
		$('#payTime').parent().find('.warning').remove();
	}
	
	if($('#orderId').val() == ''){
		pass = false;
		$('#orderId').parent().find('.warning').remove();
		$('#orderId').parent().append(html);		
	}else{
		$('#orderId').parent().find('.warning').remove();
	}
	
	if($('#twCoin').val() == ''){
		pass = false;
		$('#twCoin').parent().find('.warning').remove();
		$('#twCoin').parent().append(html);		
	}else{
		$('#twCoin').parent().find('.warning').remove();
	}
	
	if($('#payTypeId').val() == ''){
		pass = false;
		$('#payTypeId').parent().find('.warning').remove();
		$('#payTypeId').parent().append(html);		
	}else{
		$('#payTypeId').parent().find('.warning').remove();
	}
	
	if($('#userName').val() == ''){
		pass = false;
		$('#userName').parent().find('.warning').remove();
		$('#userName').parent().append(html);		
	}else{
		$('#userName').parent().find('.warning').remove();
	}
	
	if($('#userId').val() == ''){
		pass = false;
		$('#userId').parent().find('.warning').remove();
		$('#userId').parent().append(html);		
	}else{
		$('#userId').parent().find('.warning').remove();
	}
	
	if($('#contactTel').val() == '' && $('#contactAddress').val() == ''){
		pass = false;
		$('#contactTel').parent().find('.warning').remove();
		$('#contactAddress').parent().find('.warning').remove();
		$('#contactTel').parent().append("<font class='warning' color='red'>聯絡電話和聯絡地址擇一填寫</font>");
		$('#contactAddress').parent().append("<font class='warning' color='red'>聯絡電話和聯絡地址擇一填寫</font>");
	}else{
		$('#contactTel').parent().find('.warning').remove();
		$('#contactAddress').parent().find('.warning').remove();
	}
	
	if(!$('[name=paymentWay]').is(':checked')){
		pass = false;
		$('[name=paymentWay]').parent().parent().find('.warning').remove();
		$('[name=paymentWay]').parent().parent().append(html);
	}else{
		$('[name=paymentWay]').parent().parent().find('.warning').remove();
		
		$('[name=paymentWay]:checked').each(function(){
			if($(this).val() == '9'){
				if($('input[name=remark]').val() == ''){
					pass = false;
					$('input[name=remark]').parent().find('.warning').remove();
					$('input[name=remark]').parent().append(html);		
				}else{
					$('input[name=remark]').parent().find('.warning').remove();
				}
			}
		});	
	}
	
	if(!pass){
		alert("表單未填寫完整!");
	}
	
	return pass;
}

</script>