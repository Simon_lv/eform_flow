<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->	
	<input type="hidden" name="id1" id="id1" value="${param.id1}" />	
	<input type="hidden" name="id2" id="id2" value="${param.id2}" />
	<div class="table-responsive">
		
		<div style="text-align: center;"><h2>${form.companyName}</h2></div>
		<div style="text-align: center;"><h3>退款申請單</h3></div>
		
		<table class="table">
			<tr>					
				<td>填表日期：</td>
				<td>${form.requestDate}</td>
				<td>編號：</td>
				<td>${form.serialNo}</td>
				<td>遊戲：</td>
				<td>${form.gameName}</td>
			</tr>
		</table>
					
		<table class="table">
			<tr>
				<td rowspan="10">會員資料</td>	
				<td>遊戲帳號</td>		
				<td>${form.gameAccount}</td>
				<td>說明</td>		
			</tr>
			<tr>
				<td>儲值時間</td>
				<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${form.payTime}" /></td>
				<td rowspan="9">
					<div>信件標題:${form.subject }</div>
					<div>退款原因:
						<div>${form.reason}</div>
					</div>
				</td>					
			</tr>
			<tr>
				<td>訂單編號</td>
				<td>${form.orderId}</td>
			</tr>
			<tr>
				<td>儲值金額</td>
				<td>${form.twCoin}</td>
			</tr>
			<tr>
				<td>金流通路</td>
				<td>${form.payType}</td>
			</tr>
			<tr>
				<td>會員姓名</td>
				<td>${form.userName}</td>
			</tr>
			<tr>
				<td>身分證字號</td>
				<td>${form.userId}</td>
			</tr>
			<tr>
				<td>聯絡電話</td>
				<td>${form.contactTel}</td>
			</tr>
			<tr>
				<td>聯絡地址</td>
				<td>${form.contactAddress}</td>
			</tr>
			<tr>
				<td>退款方式</td>
				<td>
					<div><input type="checkbox" name="paymentWay" value="1" <c:if test="${fn:contains(form.paymentWay, '1')}">checked="checked"</c:if> disabled/>匯款帳號</div>
					<div><input type="checkbox" name="paymentWay" value="2" <c:if test="${fn:contains(form.paymentWay, '2')}">checked="checked"</c:if> disabled/>信用卡刷退</div>
					<div><input type="checkbox" name="paymentWay" value="3" <c:if test="${fn:contains(form.paymentWay, '3')}">checked="checked"</c:if> disabled/>收回折讓單</div>
					<div><input type="checkbox" name="paymentWay" value="4" <c:if test="${fn:contains(form.paymentWay, '4')}">checked="checked"</c:if> disabled/>填請款單</div>
					<div><input type="checkbox" name="paymentWay" value="5" <c:if test="${fn:contains(form.paymentWay, '5')}">checked="checked"</c:if> disabled/>收取手續費</div>
					<div><input type="checkbox" name="paymentWay" value="9" <c:if test="${fn:contains(form.paymentWay, '9')}">checked="checked"</c:if> disabled/>其他:
					${form.remark}</div>
				</td>
			</tr>
			<tr>
				<td>出款日</td>
				<td><fmt:formatDate value="${form.remitDate}" type="date" pattern="yyyy-MM-dd" /></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>
					<input type="checkbox" name="notifyAgency" value="1" <c:if test="${form.notifyAgency == 1 }">checked="checked"</c:if> disabled>通知金流商，並確認完成
				</td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2">部門主管</td>
				<td colspan="2">申請人</td>
			</tr>
			<tr>
				<td colspan="2">${stamp['部門主管'].info}</td>
				<td colspan="2">${stamp['申請人'].info}</td>
			</tr>
		</table>
		<hr/>
		<table class="table">
			<tr>
				<td rowspan="4">退款流程</td>
				<td>部門</td>
				<td>處理事項</td>
				<td>經辦</td>
				<td>部門主管覆核</td>
			</tr>
			<c:set var="opsProcess" scope="page" value="${sessionScope.formRole.contains('產品') && empty stamp['產品']}"/>				
			<tr>
				<td>營運</td>
				<td>
					<div>
						<input type="checkbox" name="opsProcess" value="1" <c:if test="${fn:contains(form.opsProcess, '1')}">checked="checked"</c:if> <c:if test="${!opsProcess}">disabled</c:if>/>客服資料確認
					</div>
					<div>
						<input type="checkbox" name="opsProcess" value="2" <c:if test="${fn:contains(form.opsProcess, '2')}">checked="checked"</c:if> <c:if test="${!opsProcess}">disabled</c:if>/>收回遊戲幣及道具
					</div>
					<div>
						<input type="checkbox" name="opsProcess" value="9" <c:if test="${fn:contains(form.opsProcess, '9')}">checked="checked"</c:if> <c:if test="${!opsProcess}">disabled</c:if>/>其他:
						<c:if test="${opsProcess}">
							<input name="opsProcessMemo" value=""/>
						</c:if>
						<c:if test="${!opsProcess}">
							${form.opsProcessMemo }
						</c:if>							
					</div>
				</td>
				<td>${stamp['產品'].info}</td>
				<td>${stamp['營運主管'].info}</td>
			</tr>
			<c:set var="finProcess" scope="page" value="${sessionScope.formRole.contains('出納') && empty stamp['出納']}"/>
			<tr>
				<td>財務</td>
				<td>
					<div><input type="checkbox" name="finProcess" value="1" <c:if test="${fn:contains(form.finProcess, '1')}">checked="checked"</c:if> <c:if test="${!finProcess}">disabled</c:if>/>客服資料確認</div>
					<div><input type="checkbox" name="finProcess" value="2" <c:if test="${fn:contains(form.finProcess, '2')}">checked="checked"</c:if> <c:if test="${!finProcess}">disabled</c:if>/>後台退款操作</div>
					<div><input type="checkbox" name="finProcess" value="3" <c:if test="${fn:contains(form.finProcess, '3')}">checked="checked"</c:if> <c:if test="${!finProcess}">disabled</c:if>/>發票作廢或開立折讓單</div>
					<div>
						<input type="checkbox" name="finProcess" value="4" <c:if test="${fn:contains(form.finProcess, '4')}">checked="checked"</c:if> <c:if test="${!finProcess}">disabled</c:if>/>開立手續費發票 
						<font color="red">(有選此欄需 鍵入發票號碼)</font>
						<c:if test="${finProcess}">
							<input name="receiptNo" value=""/>
						</c:if>
						<c:if test="${!finProcess}">
							${form.receiptNo }
						</c:if>							
					</div>
					<div>
						<input type="checkbox" name="finProcess" value="9" <c:if test="${fn:contains(form.finProcess, '9')}">checked="checked"</c:if> <c:if test="${!finProcess}">disabled</c:if>/>其他:
						<c:if test="${finProcess}">
							<input name="finProcessMemo" value=""/>
						</c:if>
						<c:if test="${!finProcess}">
							${form.finProcessMemo }
						</c:if>							
					</div>
					<div><input type="checkbox" name="finProcess" value="5" <c:if test="${fn:contains(form.finProcess, '5')}">checked="checked"</c:if> <c:if test="${!finProcess}">disabled</c:if>/>轉會計</div>
				</td>
				<td>${stamp['出納'].info}</td>
				<td>${stamp['行政主管'].info}</td>
			</tr>
			<c:set var="acctProcess" scope="page" value="${sessionScope.formRole.contains('會計專員') && empty stamp['會計專員']}"/>
			<tr>
				<td><label>會計</label></td>
				<td>
					<div><input type="checkbox" name="acctProcess" value="1" <c:if test="${fn:contains(form.acctProcess, '1')}">checked="checked"</c:if> <c:if test="${!acctProcess}">disabled</c:if>/>退款或扣帳款</div>
					<div>
						<input type="checkbox" name="acctProcess" value="9" <c:if test="${fn:contains(form.acctProcess, '9')}">checked="checked"</c:if> <c:if test="${!acctProcess}">disabled</c:if>/>其他:
						<c:if test="${acctProcess}">
							<input name="acctProcessMemo" value=""/>
						</c:if>
						<c:if test="${!acctProcess}">
							${form.acctProcessMemo }
						</c:if>							
					</div>
				</td>
				<td>${stamp['會計專員'].info}</td>
				<td>${stamp['會計主管'].info}</td>
			</tr>				
		</table>
		<!-- 退件原因 -->
		<table class="table">
			<tr>
				<td>
					退件原因：<input id="memo" name="memo" size="30"/>
					<input type="button" value="退件" id="reject"/>
				</td>
			</tr>
		</table>
		
		<!-- flow button -->
		<input type="button" value="取消" id="abort" />
		
		<c:if test="${isVerify}">
			<input type="button" value="審核" id="verify"/>
		</c:if>
		
		<c:if test="${isCountersign}">
		<input type="button" value="會簽" id="countersign"/>
		</c:if>
		
		<c:if test="${isPass}">
			<input type="button" value="放行" id="pass"/>
		</c:if>			
	</div>	
</div>

<script type="text/javascript">
$(function(){
	$('#abort').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();	
			
		$.ajax({
			url : '${ctx}/refundForm/abort',
			dataType : 'json',
			data : {
				'id' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#verify').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		var opsProcess = ${opsProcess};
		var finProcess = ${finProcess};
		var acctProcess = ${acctProcess};
		console.log("acctProcess=" + acctProcess);
		
		var data = "";
		
		if(opsProcess) {
			if(!checkValidate(1)) {
				return;
			}	
			
	        data = {
	        	'id1' : id1,
				'id2' : id2,	
	            'opsProcess': $('input[name=opsProcess]:checked').map(function() { return $(this).val().toString(); } ).get().sort().join(","),
	            'opsProcessMemo': $("input[name=opsProcessMemo]").val()
	        };
	    } 
	    else if(finProcess) {
	    	if(!checkValidate(2)) {
				return;
			}
	    	
	    	data = {
	    		'id1' : id1,
				'id2' : id2,	
	            'finProcess': $('input[name=finProcess]:checked').map(function() { return $(this).val().toString(); } ).get().sort().join(","),
	            'finProcessMemo': $("input[name=finProcessMemo]").val(),
	            'receiptNo': $("input[name=receiptNo]").val()
	        };
	    }
	    else if(acctProcess) {
			if(!checkValidate(3)) {
				return;
			}
			
	    	data = {
	    		'id1' : id1,
				'id2' : id2,	
	            'acctProcess': $('input[name=acctProcess]:checked').map(function() { return $(this).val().toString(); } ).get().sort().join(","),
	            'acctProcessMemo': $("input[name=acctProcessMemo]").val()
	        };
	    }
	    else {
	    	data = {
	    		'id1' : id1,
				'id2' : id2
	    	};
	    }
	    
	    console.log(JSON.stringify(data, null, 2));
	    
		$.ajax({
			url : '${ctx}/refundForm/verify',
			dataType : 'json',
			data : data,
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});		
	});	
	
	$('#countersign').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		
		$.ajax({
			url : '${ctx}/refundForm/countersign',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#pass').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();		
		
		$.ajax({
			url : '${ctx}/refundForm/pass',
			dataType : 'json',
			data : {
	    		'id1' : id1,
				'id2' : id2
	    	},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#reject').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		var memo = $("#memo").val();
		
		$.ajax({
			url : '${ctx}/refundForm/reject',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2,
				'memo' : memo
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
});

function checkValidate(type) {
	var pass = true;
	var html = "<font class='warning' color='red'>請輸入此欄位</font>";
	
	if(type == 1) {
		if($('[name=opsProcess]:checked').length == 0) {
			alert("請至少勾選一項");
			return false;
		}
		
		$('[name=opsProcess]:checked').each(function(){
			if($(this).val() == '9' && $(this).is(':enabled')){
				if($('input[name=opsProcessMemo]').val() == ''){
					pass = false;
					$('input[name=opsProcessMemo]').parent().find('.warning').remove();
					$('input[name=opsProcessMemo]').parent().append(html);		
				}else{
					$('input[name=opsProcessMemo]').parent().find('.warning').remove();
				}
			}
		});
	}
	
	if(type == 2) {
		if($('[name=finProcess]:checked').length == 0) {
			alert("請至少勾選一項");
			return false;
		}
		
		$('[name=finProcess]:checked').each(function(){
			if($(this).val() == '4' && $(this).is(':enabled')){
				if($('input[name=receiptNo]').val() == ''){
					pass = false;
					$('input[name=receiptNo]').parent().find('.warning').remove();
					$('input[name=receiptNo]').parent().append(html);		
				}else{
					$('input[name=receiptNo]').parent().find('.warning').remove();
				}
			}
			
			if($(this).val() == '9' && $(this).is(':enabled')){
				if($('input[name=finProcessMemo]').val() == ''){
					pass = false;
					$('input[name=finProcessMemo]').parent().find('.warning').remove();
					$('input[name=finProcessMemo]').parent().append(html);		
				}else{
					$('input[name=finProcessMemo]').parent().find('.warning').remove();
				}
			}
		});
	}
	
	if(type == 3) {
		if($('[name=acctProcess]:checked').length == 0) {
			alert("請至少勾選一項");
			return false;
		}
		
		$('[name=acctProcess]:checked').each(function(){
			if($(this).val() == '9' && $(this).is(':enabled')){
				if($('input[name=acctProcessMemo]').val() == ''){
					pass = false;
					$('input[name=acctProcessMemo]').parent().find('.warning').remove();
					$('input[name=acctProcessMemo]').parent().append(html);		
				}else{
					$('input[name=acctProcessMemo]').parent().find('.warning').remove();
				}
			}
		});
	}
	
	if(!pass){
		alert("退款流程未填寫完整!");
	}
	
	return pass;
}
</script>