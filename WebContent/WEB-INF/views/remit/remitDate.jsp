<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/remit/updateRemitDate" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${id}" />
		<input type="hidden" name="formType" id="formType" value="${formType}" />
		<div class="table-responsive">
			<table class="table">
				<tr>
					<td width=25%">編號</td>
					<td>${serialNo}</td>
				</tr>
				<tr>
					<td>公司</td>
					<td>${company}</td>
				</tr>
				<tr>
					<td>部門</td>
					<td>${department}</td>
				</tr>
				<tr>
					<td>員工</td>
					<td>${employee}</td>
				</tr>
				<tr>
					<td>出款日&nbsp;(原訂 ${remitDate})</td>
					<td>
						<input type="text" value=""
							onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd', disabledDates:['10$','25$'], disabledDays:[0,6]})"
							class="Wdate" name="remitDate" id="remitDate">
					</td>
				</tr>
				<c:forEach var="amount" items="${amounts}">
				<tr>
					<td>${amount.key}</td>
					<td>${amount.value}</td>
				</tr>				
				</c:forEach>
				<tr>
					<td colspan="2">${description}</td>
				</tr>								
			</table>
		</div>
	</form>
	<input type="button" value="保存" id="save" />
</div>

<script type="text/javascript">
$(function(){
	$('#save').click(function() {
		if($("input[name='remitDate']").val() == '') {
			alert("請輸入出款日");
			return;
		}	
		
		$("#editForm").ajaxSubmit({
            success: function(data) {
            	if(data.code == "0000") {
            		alert("修改成功");                    
            	}
            	else {
            		alert("修改失敗");                    
            	}
            	
            	$("#myModal").modal("toggle");
                $('#search').click(); 
            },
            error: function() {
                alert("網路連接失敗，請聯繫管理員");
            }
        })
	});
});
</script>