<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/issueForm/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${cp.id}" />
		<input type="hidden" name="workFlowId" value="${cp.workFlowId}" />
		<div class="table-responsive">
			
			<div style="text-align: center;"><h2>${cp.companyName}</h2></div>
			<div style="text-align: center;"><h3>重大事故通報單</h3></div>
			
			<div style="text-align: left;"><label>編號&nbsp;</label>${cp.serialNo}</div>
			<table class="table">
				<tr>
					<td><label>重大事故標題</label></td>
					<td>${cp.issueTitle2 }</td>
					<td>${cp.gameDataId == 0 ? '平台' : cp.gameName}</td>
					<td>
						<c:if test="${cp.level == 1}">一級重大</c:if>
						<c:if test="${cp.level == 2}">二級重大</c:if>
					</td>
					<td colspan="2">							
				</tr>
				<tr>
					<td rowspan="2"></td>
					<td><label>開始時間</label></td>
					<td><fmt:formatDate value="${cp.issueStartTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					<td rowspan="2"><label>通報客服</label></td>
					<td>${cp.creatorName}</td>			
				</tr>
				<tr>
					<td><label>通報時間</label></td>
					<td><fmt:formatDate value="${cp.issueFeedbackTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					<td>
						<input type="checkbox" name="issueFeedbackChannel" value="1" <c:if test="${cp.issueFeedbackChannel == 1 || cp.issueFeedbackChannel == 0}">checked="checked"</c:if>/>電話
						<input type="checkbox" name="issueFeedbackChannel" value="2" <c:if test="${cp.issueFeedbackChannel == 2 || cp.issueFeedbackChannel == 0 }">checked="checked"</c:if>/>信件
					</td>
				</tr>
				<tr>
					<td><label>事故描述</label></td>
					<td colspan="4">${cp.issueFeedbackDescription2}</td>
				</tr>
				<tr>
					<td colspan="5">										
						<c:forEach items="${attachment}" var="a" varStatus="loop">
						<a href="${a.url}" target="_blank"><label>附件_${loop.count}</label></a>								
						</c:forEach>										
					</td>
				</tr>
				<tr>
					<td rowspan="2"></td>
					<td><label>修復時間</label></td>
					<td><fmt:formatDate value="${cp.repairTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					<td rowspan="2"><label>修復通報人</label></td>
					<td>${stamp['重大負責人'].info}</td>
				</tr>
				<tr>
					<td><label>通報時間</label></td>
					<td><fmt:formatDate value="${cp.repairFeedbackTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					<td>
						<input type="checkbox" name="repairFeedbackChannel" value="1" <c:if test="${cp.repairFeedbackChannel == 1 || cp.repairFeedbackChannel == 0}">checked="checked"</c:if>/>電話
						<input type="checkbox" name="repairFeedbackChannel" value="2" <c:if test="${cp.repairFeedbackChannel == 2 || cp.repairFeedbackChannel == 0}">checked="checked"</c:if>/>信件
					</td>
				</tr>
				<tr>
					<td colspan="4"><label>事故原因說明</label></td>
					<td><label>負責人簽名</label></td>
				</tr>
				<tr>
					<td colspan="4">
						${cp.issueDescription2 }
					</td>
					<td>${stamp['重大負責人'].info}</td>
				</tr>
				<tr>
					<td><label>責任歸屬</label></td>
					<td colspan="4">
						${cp.accountablity}
						&nbsp;
						${cp.owner}
					</td>
				<tr>
					<td colspan="4"><label>修復方式</label></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="4">
						${cp.repairDescription2 }
					</td>
					<td>						
						<c:forEach var="x" items="${stamp}">
						    <c:if test="${fn:startsWith(x.key, '重大修復人')}">
						    	${x.value.info}<br>
						    </c:if>
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td colspan="4"><label>未來預防方式</label></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="4">
						${cp.preventionDescription2 }
					</td>
					<td>${stamp['重大執行人'].info}</td>
				</tr>
			</table>
			<table class="table">
				<tr>
					<td rowspan="3"><label>原因分類</label></td>
					<td>
						<div><input type="radio" name="issueType" value="1" <c:if test="${cp.issueType == '1' }">checked="checked"</c:if>/>外部人為疏失</div>
						<div>(如原廠、機房、金流、通路)  </div>
					</td>
					<td>
						<div><label>疏失等級：</label></div>
						<div>
							<input type="radio" name="issueLevel" value="1" <c:if test="${cp.issueType == '1' && cp.issueLevel == '1' }">checked="checked"</c:if>/>重大
							<input type="radio" name="issueLevel" value="2" <c:if test="${cp.issueType == '1' && cp.issueLevel == '2' }">checked="checked"</c:if>/>一般
							<input type="radio" name="issueLevel" value="3" <c:if test="${cp.issueType == '1' && cp.issueLevel == '3' }">checked="checked"</c:if>/>輕微
						</div>
					</td>
					<td>
						<div><label>未來預防信心度：</label></div>
						<div>
							<input type="radio" name="preventionLevel" value="1" <c:if test="${cp.issueType == '1' && cp.preventionLevel == '1' }">checked="checked"</c:if>/>高
							<input type="radio" name="preventionLevel" value="2" <c:if test="${cp.issueType == '1' && cp.preventionLevel == '2' }">checked="checked"</c:if>/>中
							<input type="radio" name="preventionLevel" value="3" <c:if test="${cp.issueType == '1' && cp.preventionLevel == '3' }">checked="checked"</c:if>/>低
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div><input type="radio" name="issueType" value="2" <c:if test="${cp.issueType == '2' }">checked="checked"</c:if>/>內部人為疏失</div>						
					</td>
					<td>
						<div><label>疏失等級：</label></div>
						<div>
							<input type="radio" name="issueLevel" value="1" <c:if test="${cp.issueType == '2' && cp.issueLevel == '1' }">checked="checked"</c:if>/>重大
							<input type="radio" name="issueLevel" value="2" <c:if test="${cp.issueType == '2' && cp.issueLevel == '2' }">checked="checked"</c:if>/>一般
							<input type="radio" name="issueLevel" value="3" <c:if test="${cp.issueType == '2' && cp.issueLevel == '3' }">checked="checked"</c:if>/>輕微
						</div>
					</td>
					<td>
						<div><label>未來預防信心度：</label></div>
						<div>
							<input type="radio" name="preventionLevel" value="1" <c:if test="${cp.issueType == '2' && cp.preventionLevel == '1' }">checked="checked"</c:if>/>高
							<input type="radio" name="preventionLevel" value="2" <c:if test="${cp.issueType == '2' && cp.preventionLevel == '2' }">checked="checked"</c:if>/>中
							<input type="radio" name="preventionLevel" value="3" <c:if test="${cp.issueType == '2' && cp.preventionLevel == '3' }">checked="checked"</c:if>/>低
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div><input type="radio" name="issueType" value="3" <c:if test="${cp.issueType == '3'}">checked="checked"</c:if>/>外部駭客攻擊</div>						
					</td>
					<td>
						<div><label>攻擊強度：</label></div>
						<div>
							<input type="radio" name="issueLevel" value="1" <c:if test="${cp.issueType == '3' && cp.issueLevel == '1' }">checked="checked"</c:if>/>重大
							<input type="radio" name="issueLevel" value="2" <c:if test="${cp.issueType == '3' && cp.issueLevel == '2' }">checked="checked"</c:if>/>一般
							<input type="radio" name="issueLevel" value="3" <c:if test="${cp.issueType == '3' && cp.issueLevel == '3' }">checked="checked"</c:if>/>輕微
						</div>
					</td>
					<td>
						<div><label>防備能力：</label></div>
						<div>
							<input type="radio" name="preventionLevel" value="1" <c:if test="${cp.issueType == '3' && cp.preventionLevel == '1' }">checked="checked"</c:if>/>完整
							<input type="radio" name="preventionLevel" value="2" <c:if test="${cp.issueType == '3' && cp.preventionLevel == '2' }">checked="checked"</c:if>/>一般
							<input type="radio" name="preventionLevel" value="3" <c:if test="${cp.issueType == '3' && cp.preventionLevel == '3' }">checked="checked"</c:if>/>不足
						</div>
					</td>
				</tr>
			</table>
			<table class="table">
				<tr>
					<td><label>總經理簽核</label></td>
					<td><label>副總經理簽核</label></td>
					<td><label>技術主管簽名</label></td>
					<td><label>營運主管簽名</label></td>
				</tr>
				<tr>
					<td>${stamp['總經理'].info}</td>
					<td>${stamp['副總經理'].info}</td>
					<td>${stamp['技術主管'].info}</td>
					<td>${stamp['營運主管'].info}</td>
				</tr>
			</table>
		</div>
		<c:if test="${not empty memo}">
		<div><span style="color: red;">${memo}</span></div>
		</c:if>
	</form>	
	
</div>
<script type="text/javascript">

$(function(){
	$('input[type=checkbox]').each(function(){
		$(this).attr("disabled", true);		
	});
	
	$('input[type=radio]').each(function(){
		$(this).attr("disabled", true);		
	});
});
</script>