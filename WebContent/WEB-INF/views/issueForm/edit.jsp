<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/issueForm/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${cp.id}" />
		<input type="hidden" name="workFlowId" value="${cp.workFlowId}" />
		<!-- 退件重送 -->
		<input type="hidden" name="serialNo" value="${cp.serialNo}" />
		<div class="table-responsive">
			
			<div style="text-align: center;"><h2 id="title"></h2></div>
			<div style="text-align: center;"><h3>重大事故通報單</h3></div>
			
			<table class="table">
				<tr>
					<td><label>公司</label></td>
					<td>
						<select id="companyId" name="companyId">
                           	<c:forEach items="${companys}" var="company">
                           	<option value="${company.id}"
                           		<c:if test="${cp.companyId == company.id}">selected</c:if>
                           		>${company.name}</option>
                           	</c:forEach>
                        </select>
                        <input type="hidden" id="requestDepartment" name="requestDepartment" value="${empty cp ? requestDepartment : cp.requestDepartment}" />
					</td>
					<td><label>重大事故標題</label></td>
					<td><input name="issueTitle" id="issueTitle" value="${cp.issueTitle }"/></td>
					<td>
						<label>遊戲</label>
						<select name="gameDataId" id="gameDataId">
							<option value="0">平台</option>
							<c:forEach items="${allGameData}" var="a">
							<option value="${a.id}" <c:if test="${a.id == cp.gameDataId}">selected="selected"</c:if> > ${a.name}</option>
							</c:forEach>
						</select>
					</td>
					<td>
						<label>重大等級</label>
						<select name="level">
							<option value="1" <c:if test="${cp.level == 1}">selected="selected"</c:if> >一級</option>							
							<option value="2" <c:if test="${cp.level == 2}">selected="selected"</c:if> >二級</option>
						</select>
					</td>									
				</tr>
				<tr>
					<td rowspan="2"></td>
					<td><label>開始時間</label></td>
					<td>
						<input type="text" value="<fmt:formatDate value="${cp.issueStartTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss" />"
									onfocus="WdatePicker({issueStartTime:'%y-%M-%d 23:59:59', dateFmt:'yyyy-MM-dd HH:mm:ss'})"
									class="Wdate" name="issueStartTime" id="issueStartTime">
					</td>
					<td rowspan="2"><label>通報客服</label></td>
					<td colspan="2">${empty cp ? feedbackOperator : cp.creatorName}</td>			
				</tr>
				<tr>
					<td><label>通報時間</label></td>
					<td>
						<input type="text" value="<fmt:formatDate value="${cp.issueFeedbackTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss" />"
									onfocus="WdatePicker({issueFeedbackTime:'%y-%M-%d 23:59:59', dateFmt:'yyyy-MM-dd HH:mm:ss'})"
									class="Wdate" name="issueFeedbackTime" id="issueFeedbackTime">
					</td>
					<td colspan="2">
						<input type="checkbox" name="feedbackChannel" value="1" <c:if test="${cp.issueFeedbackChannel == 1 || cp.issueFeedbackChannel == 0 }">checked="checked"</c:if>/>電話
						<input type="checkbox" name="feedbackChannel" value="2" <c:if test="${cp.issueFeedbackChannel == 2 || cp.issueFeedbackChannel == 0 }">checked="checked"</c:if>/>信件
						<input type="hidden" name="issueFeedbackChannel" id="issueFeedbackChannel" value="${form.issueFeedbackChannel}" />						
					</td>
				</tr>
				<tr>
					<td><label>事故描述</label></td>
					<td colspan="5">
						<textarea name="issueFeedbackDescription" style="width: 70%;">${cp.issueFeedbackDescription }</textarea>
					</td>					
				</tr>
				<tr>
					<td colspan="6">
						<table class="table">				
							<tr>					
								<td>
									<label>附件</label>
									<c:forEach items="${attachment}" var="a" varStatus="loop">
									<a href="${a.url}" target="_blank"><label>附件_${loop.count}</label></a>								
									</c:forEach>
								</td>
								<td><input class="btn btn-success" type="button" value="增加" id="addColumn"/></td>
							</tr>				
							<tbody id="attachmentBody">
							<c:if test="${empty cp}">
							<tr>					
								<td>
									<input type="hidden" name="url" value=""/>
									<input name="fileName" value="" size="50"/>
									<input class="upload" type="button" value="上傳"/>
								</td>
								<td></td>					
							</tr>
							</c:if>
							<c:forEach items="${attachment}" var="a">
							<tr>					
								<td>
									<input type="hidden" name="url" value="${a.url}"/>
									<input name="fileName" value="" size="50"/>
									<input type="button" value="上傳" onclick="$('#fileBrowse').click();"/>
								</td>
								<td><input class="btn" type="button" value="刪除" onclick="deleteAttachment('${a.id}', '${cp.id}')"/></td>
							</tr>
							</c:forEach>
							</tbody>
						</table>
					</td>
				</tr>					
				<tr>
					<td><label>備註</label></td>
					<td colspan="5">
						<input type="text" name="memo" value="" size="100" maxlength="100">
					</td>
				</tr>				
				<c:forEach items="${memo}" var="m">
				<tr>
					<td colspan="3">${m.memo}</td>
					<td>${m.creatorName}</td>
					<td colspan="2"><fmt:formatDate value="${m.createTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss" /></td>
				</tr>
				</c:forEach>			
			</table>			
		</div>
		
		<!-- 退件重送 -->
		<input type="hidden" name="repairTime" value="${cp.repairTime}" />
		<input type="hidden" name="repairFeedbackTime" value="${cp.repairFeedbackTime}" />
		<input type="hidden" name="repairFeedbackChannel" value="${cp.repairFeedbackChannel}" />
		<input type="hidden" name="issueDescription" value="${cp.issueDescription}" />
		<input type="hidden" name="accountablity" value="${cp.accountablity}" />
		<input type="hidden" name="owner" value="${cp.owner}" />
		<input type="hidden" name="repairDescription" value="${cp.repairDescription}" />
		<input type="hidden" name="preventionDescription" value="${cp.preventionDescription}" />
		<input type="hidden" name="issueType" value="${cp.issueType}" />
		<input type="hidden" name="issueLevel" value="${cp.issueLevel}" />
		<input type="hidden" name="preventionLevel" value="${cp.preventionLevel}" />
	</form>	
	<input type="button" value="保存" id="save"/>	
	<input type="button" value="送審" id="submit"/>
	
	<form name="upload" id="fileUploadForm" enctype="multipart/form-data" method="post">		
		<div style="display: none">
			<input type="file" id="fileBrowse" name="file" style="border: 0;" />
		</div>
	</form>
</div>

<script type="text/javascript">
$(function() {
	$('#save').click(function() {		
		if(checkValidate()){
			$("#editForm").ajaxSubmit({
	            success: function(e) {
	                alert(e.message), $("#myModal").modal("toggle");
	                $('#search').click();
	            },
	            error: function() {
	                alert("網路連接失敗，請聯繫管理員");
	            }
	        });
		}		
	});
	
	$('#addColumn').click(function(){
		var html = '';
		html += '<tr>';
		html += '<td><input type="hidden" name="url" value=""><input name="fileName" value="" size="50">';
		html += '<input class="upload" type="button" value="上傳"></td>';
		html += '<td><input class="btn delete" type="button" value="刪除"></td>';
		html += '</tr>';
		
		$('#attachmentBody').append(html);
	});
	
	var target;  // fileName
	$('#attachmentBody').on('click', '.upload', function(){
		target = $(this).prev();
		$('#fileBrowse').click();
	});	
	
	$('#submit').click(function() {
		if(checkValidate()){
			$('#editForm').attr('action', '${ctx}/issueForm/submit');
			$("#editForm").ajaxSubmit({
	            success: function(e) {
	            	if(e.code == "0000") {
	            		alert(e.message), $("#myModal").modal("toggle");
		                $('#search').click();
	            	}
	            	else {
	            		alert(e.message);
	            	}
	            },
	            error: function() {
	                alert("網路連接失敗，請聯繫管理員");
	            }
	        });			
		}
	});
	
	$('#fileUploadForm').on('submit', (function(e) {		
		e.preventDefault();
		
		var formData = new FormData(this);
		$.ajax({
			async : false,
			type : 'POST',
			url : '${ctx}/issueForm/upload.do',
			data : formData,
			cache : false,
			contentType : false,
			processData : false,
			success : function(data) {
				if (data == "overSize") {
					alert("檔案超過1MB請更換圖片");
					$("#fileBrowse").val("");
					return;
				}				
				//$("#url").val(data.url);
				target.val(data.description);
				target.prev().val(data.url);
			},
			error : function(data) {
				alert("系統錯誤，請稍後再試");
			}
		});
	}));
	
	$("#fileBrowse").on("change", function() {
		if (this.files[0].size >= 1048576) {
			alert("檔案超過1MB請更換檔案");
			return;
		}	
		$("#fileUploadForm").submit();
	});		
	
	$('#attachmentBody').on('click', '.delete', function(){
		$(this).parent().parent().remove();
	});
	
	$('#companyId').change(function() {
		var cid = $(this).val();
		var name = $(this).find("option:selected").text();
		$("#title").text(name);		
	});
	
	// default title
	$("#title").text($('#companyId').find("option:selected").text());	
});

function checkValidate(){
	var pass = true;
	var html = "<font class='warning' color='red'>請輸入此欄位</font>";
	
	if($('#issueTitle').val() == ''){
		pass = false;
		$('#issueTitle').parent().find('.warning').remove();
		$('#issueTitle').parent().append(html);		
	}else{
		$('#issueTitle').parent().find('.warning').remove();
	}
	
	if($('#issueStartTime').val() == ''){
		pass = false;
		$('#issueStartTime').parent().find('.warning').remove();
		$('#issueStartTime').parent().append(html);		
	}else{
		$('#issueStartTime').parent().find('.warning').remove();
	}
	if($('#issueFeedbackTime').val() == ''){
		pass = false;
		$('#issueFeedbackTime').parent().find('.warning').remove();
		$('#issueFeedbackTime').parent().append(html);		
	}else{
		$('#issueFeedbackTime').parent().find('.warning').remove();
	}
	if($('[name=issueFeedbackOperator]').val() == ''){
		pass = false;
		$('[name=issueFeedbackOperator]').parent().find('.warning').remove();
		$('[name=issueFeedbackOperator]').parent().append(html);		
	}else{
		$('[name=issueFeedbackOperator]').parent().find('.warning').remove();
	}
	if(!$('[name=feedbackChannel]').is(':checked')){
		pass = false;
		$('[name=feedbackChannel]').parent().find('.warning').remove();
		$('[name=feedbackChannel]').parent().append(html);		
	}else{
		$('[name=feedbackChannel]').parent().find('.warning').remove();
		
		var items = 0;
		$("input[name='feedbackChannel']:checked").each(function(){items += parseInt($(this).val());});
		console.log(items);
		
		$("#issueFeedbackChannel").val(items > 2 ? 0 : items);		
	}
	if($('[name=issueFeedbackDescription]').val() == ''){
		pass = false;
		$('[name=issueFeedbackDescription]').parent().find('.warning').remove();
		$('[name=issueFeedbackDescription]').parent().append(html);		
	}else{
		$('[name=issueFeedbackDescription]').parent().find('.warning').remove();
	}	
	
	return pass;
}

</script>