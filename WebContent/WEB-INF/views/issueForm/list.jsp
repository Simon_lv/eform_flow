<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	
	<table class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>								
				<th>表單編號</th>
				<th>公司</th>
				<th>重大事故標題</th>
				<th>遊戲</th>
				<th>級別</th>
				<th>事故開始時間</th>
				<th>事故通報時間</th>
				<th>修復時間</th>				
				<th>修復通報時間</th>
				<th>備註</th>
				<th>狀態</th>	
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
				<tr>
					<td>${o.serialNo}</td>
					<td>${o.companyName}</td>
					<td>${o.issueTitle}</td>
					<td>${empty o.gameName ? '平台' : o.gameName}</td>
					<td>
						<c:if test="${o.level == 1}">一級重大</c:if>
						<c:if test="${o.level == 2}">二級重大</c:if>
					</td>
					<td><fmt:formatDate value="${o.issueStartTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					<td><fmt:formatDate value="${o.issueFeedbackTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					<td><fmt:formatDate value="${o.repairTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					<td><fmt:formatDate value="${o.repairFeedbackTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					<td>
						<c:if test="${empty o.memo}">&nbsp;</c:if>
						<c:if test="${not empty o.memo}"><a href="javascript:;" onclick="memoList(${o.id})">${o.memo}</a></c:if>
					</td>					
					<td>
						<c:choose>
						    <c:when test="${o.status == -1}">編輯中</c:when>
						    <c:when test="${o.status == 0}">正常結束</c:when>
						    <c:when test="${o.status == 1}">執行中</c:when>
						    <c:when test="${o.status == 8}">註銷</c:when>
						    <c:when test="${o.status == 9}">退件</c:when>
						    <c:otherwise>未定義(o.status)</c:otherwise>
						</c:choose>
					</td>														
					<td>
						<!-- 審核歷程 -->									
						<c:if test="${o.status >= 0}">
							<a href="javascript:;" onclick="history(${o.workFlowId})">審核歷程</a>
						</c:if>
						<c:if test="${o.status == -1 && (sessionScope.formRole.contains('客服') || sessionScope.formRole.contains('客服主管')) }">
						<a href="javascript:;" onclick="edit(${o.id})">編輯</a>
						</c:if>
						<c:if test="${o.status == 1 && o.creator == sessionScope.loginUser.id}">
						<a href="javascript:;" onclick="terminate(${o.workFlowId})">撤銷</a>
						</c:if>
						<c:if test="${o.status == 1 && (sessionScope.formRole.contains('客服') || sessionScope.formRole.contains('客服主管')) }">
						<a href="javascript:;" onclick="memo(${o.id})">備註</a>
						</c:if>
						<a href="javascript:;" onclick="get(${o.id})">檢視</a>												
					</td>
				</tr>		
			</c:forEach>
		</tbody>
	</table>
</div>
<my:pagination page="${page}" />