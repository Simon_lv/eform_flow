<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->	
	<input type="hidden" name="id1" id="id1" value="${param.id1}" />	
	<input type="hidden" name="id2" id="id2" value="${param.id2}" />	
	<div class="table-responsive">
		
		<div style="text-align: center;"><h2>${form.companyName}</h2></div>
		<div style="text-align: center;"><h3>重大事故通報單</h3></div>
		
		<div style="text-align: left;"><label>編號&nbsp;</label>${form.serialNo}</div>
		<table class="table">
			<tr>
				<td><label>重大事故標題</label></td>
				<td>${form.issueTitle2}</td>
				<td>${form.gameDataId == 0 ? '平台' : form.gameName}</td>
				<td>
					<c:if test="${form.level == 1}">一級重大</c:if>
					<c:if test="${form.level == 2}">二級重大</c:if>
				</td>
				<td colspan="2">											
			</tr>
			<tr>
				<td rowspan="2"></td>
				<td><label>開始時間</label></td>
				<td><fmt:formatDate value="${form.issueStartTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss" /></td>
				<td rowspan="2"><label>通報客服</label></td>
				<td>${form.creatorName}</td>			
			</tr>
			<tr>
				<td><label>通報時間</label></td>
				<td><fmt:formatDate value="${form.issueFeedbackTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss" /></td>
				<td>
					<input type="checkbox" name="channel1" value="1" <c:if test="${form.issueFeedbackChannel == 1 || form.issueFeedbackChannel == 0}">checked="checked"</c:if> disabled/>電話
					<input type="checkbox" name="channel1" value="2" <c:if test="${form.issueFeedbackChannel == 2 || form.issueFeedbackChannel == 0}">checked="checked"</c:if> disabled/>信件					
				</td>
			</tr>
			<tr>
				<td><label>事故描述</label></td>
				<td colspan="4">${form.issueFeedbackDescription2}</td>				
			</tr>
			<tr>
				<td colspan="5">										
					<c:forEach items="${attachment}" var="a" varStatus="loop">
					<a href="${a.url}" target="_blank"><label>附件_${loop.count}</label></a>								
					</c:forEach>										
				</td>
			</tr>
			<c:set var="ownerProcess" scope="page" value="${sessionScope.formRole.contains('重大負責人') && empty stamp['重大負責人']}"/>
			<tr>
				<td rowspan="2"></td>
				<td><label>修復時間</label></td>
				<td>
					<c:if test="${ownerProcess}">
					<input type="text" value="<fmt:formatDate value="${form.repairTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss" />"
						onfocus="WdatePicker({repairTime:'%y-%M-%d 23:59:59', dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						class="Wdate" name="repairTime" id="repairTime">
					</c:if>
					<c:if test="${!ownerProcess}">
					${form.repairTime}
					</c:if>	
				</td>
				<td rowspan="2"><label>修復通報人</label></td>
				<td>${stamp['重大負責人'].info}</td>
			</tr>
			<tr>
				<td><label>通報時間</label></td>
				<td>
					<c:if test="${ownerProcess}">
					<input type="text" value="<fmt:formatDate value="${form.repairFeedbackTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss" />"
						onfocus="WdatePicker({repairFeedbackTime:'%y-%M-%d 23:59:59', dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						class="Wdate" name="repairFeedbackTime" id="repairFeedbackTime">
					</c:if>
					<c:if test="${!ownerProcess}">
					${form.repairFeedbackTime}
					</c:if>	
				</td>
				<td>
					<input type="checkbox" name="feedbackChannel" value="1" <c:if test="${form.repairFeedbackChannel == 1 || form.repairFeedbackChannel == 0}">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>電話
					<input type="checkbox" name="feedbackChannel" value="2" <c:if test="${form.repairFeedbackChannel == 2 || form.repairFeedbackChannel == 0}">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>信件
					<input type="hidden" name="repairFeedbackChannel" id="repairFeedbackChannel" value="" />
				</td>
			</tr>			
			<tr>
				<td colspan="4"><label>事故原因說明</label></td>
				<td><label>負責人簽名</label></td>
			</tr>
			<tr>
				<td colspan="4">
					<c:if test="${ownerProcess}">
					<textarea name="issueDescription" style="width: 70%;">${form.issueDescription}</textarea>
					</c:if>
					<c:if test="${!ownerProcess}">
					${form.issueDescription2}
					</c:if>
				</td>
				<td>${stamp['重大負責人'].info}</td>
			</tr>			
			<tr>
				<td><label>責任歸屬</label></td>
				<td colspan="4">
					<c:if test="${ownerProcess}">
					<select name="accountablity">
						<option value="">請選擇</option>
						<option value="行銷">行銷</option>						
						<option value="研發">研發</option>
						<option value="營運">營運</option>
						<option value="原廠">原廠</option>
						<option value="運維">運維</option>
						<option value="測試">測試</option>
					</select>
					<input type="text" name="owner">
					</c:if>
					<c:if test="${!ownerProcess}">
					${form.accountablity}
					&nbsp;
					${form.owner}
					</c:if>
				</td>
			</tr>			
			<!-- ownerProcess 
			<tr>
				<td><label>修復人指派</label></td>
				<td colspan="4">
					<input type="checkbox" name="formRole" value="13">產品
					<input type="checkbox" name="formRole" value="14">技術
					<input type="checkbox" name="formRole" value="26">資訊
					<input type="checkbox" name="formRole" value="5">行政主管
					<a href="javascript:;" id="assign" class="btn btn-primary" onclick="assign()">指派</a>
					<span id="assign_span"></span>
				</td>
			</tr>
			-->			
			<c:set var="repairProcess" scope="page" value="${(sessionScope.formRole.contains('重大修復人') && empty stamp['重大修復人']) || assign == '重大修復人'}"/>			
			<tr>
				<td colspan="4"><label>修復方式</label></td>
				<td><label>修復人簽名</td>
			</tr>
			<tr>
				<td colspan="4">
					<c:choose>					
					<c:when test="${repairProcess && !ownerProcess}">
					<!-- TODO 多個修復人時已前面修復人輸入資料是否開放後面修復人編輯 -->
					<textarea name="repairDescription" style="width: 70%;">${form.repairDescription}</textarea>
					</c:when>
					<c:otherwise>
					${form.repairDescription2}
					</c:otherwise>
					</c:choose>					
				</td>
				<td>
					<c:forEach var="x" items="${stamp}">
					    <c:if test="${fn:startsWith(x.key, '重大修復人')}">
					    	${x.value.info}<br>
					    </c:if>
					</c:forEach>
				</td>
			</tr>
			<c:set var="preventionProcess" scope="page" value="${sessionScope.formRole.contains('重大執行人') && empty stamp['重大執行人']}"/>
			<tr>
				<td colspan="4"><label>未來預防方式</label></td>
				<td><label>執行人簽名</label></td>
			</tr>
			<tr>
				<td colspan="4">
					<c:choose>
					<c:when test="${preventionProcess && !repairProcess}">
					<textarea name="preventionDescription" style="width: 70%;">${form.preventionDescription}</textarea>
					</c:when>
					<c:otherwise>
					${form.preventionDescription2}
					</c:otherwise>
					</c:choose>							
				</td>
				<td>${stamp['重大執行人'].info}</td>
			</tr>
		</table>		
		<table class="table">
			<tr>
				<td rowspan="3"><label>原因分類</label></td>
				<td>
					<div><input type="radio" name="issueType" value="1" <c:if test="${form.issueType == '1' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>外部人為疏失</div>
					<div>(如原廠、機房、金流、通路)  </div>
				</td>
				<td>
					<div><label>疏失等級：</label></div>
					<div>
						<input type="radio" name="issueLevel" value="1" <c:if test="${form.issueType == '1' && form.issueLevel == '1' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>重大
						<input type="radio" name="issueLevel" value="2" <c:if test="${form.issueType == '1' && form.issueLevel == '2' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>一般
						<input type="radio" name="issueLevel" value="3" <c:if test="${form.issueType == '1' && form.issueLevel == '3' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>輕微
					</div>
				</td>
				<td>
					<div><label>未來預防信心度：</label></div>
					<div>
						<input type="radio" name="preventionLevel" value="1" <c:if test="${form.issueType == '1' && form.preventionLevel == '1' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>高
						<input type="radio" name="preventionLevel" value="2" <c:if test="${form.issueType == '1' && form.preventionLevel == '2' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>中
						<input type="radio" name="preventionLevel" value="3" <c:if test="${form.issueType == '1' && form.preventionLevel == '3' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>低
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div><input type="radio" name="issueType" value="2" <c:if test="${form.issueType == '2' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>內部人為疏失</div>						
				</td>
				<td>
					<div><label>疏失等級：</label></div>
					<div>
						<input type="radio" name="issueLevel" value="1" <c:if test="${form.issueType == '2' && form.issueLevel == '1' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>重大
						<input type="radio" name="issueLevel" value="2" <c:if test="${form.issueType == '2' && form.issueLevel == '2' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>一般
						<input type="radio" name="issueLevel" value="3" <c:if test="${form.issueType == '2' && form.issueLevel == '3' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>輕微
					</div>
				</td>
				<td>
					<div><label>未來預防信心度：</label></div>
					<div>
						<input type="radio" name="preventionLevel" value="1" <c:if test="${form.issueType == '2' && form.preventionLevel == '1' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>高
						<input type="radio" name="preventionLevel" value="2" <c:if test="${form.issueType == '2' && form.preventionLevel == '2' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>中
						<input type="radio" name="preventionLevel" value="3" <c:if test="${form.issueType == '2' && form.preventionLevel == '3' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>低
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div><input type="radio" name="issueType" value="3" <c:if test="${form.issueType == '3'}">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>外部駭客攻擊</div>						
				</td>
				<td>
					<div><label>攻擊強度：</label></div>
					<div>
						<input type="radio" name="issueLevel" value="1" <c:if test="${form.issueType == '3' && form.issueLevel == '1' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>重大
						<input type="radio" name="issueLevel" value="2" <c:if test="${form.issueType == '3' && form.issueLevel == '2' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>一般
						<input type="radio" name="issueLevel" value="3" <c:if test="${form.issueType == '3' && form.issueLevel == '3' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>輕微
					</div>
				</td>
				<td>
					<div><label>防備能力：</label></div>
					<div>
						<input type="radio" name="preventionLevel" value="1" <c:if test="${form.issueType == '3' && form.preventionLevel == '1' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>完整
						<input type="radio" name="preventionLevel" value="2" <c:if test="${form.issueType == '3' && form.preventionLevel == '2' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>一般
						<input type="radio" name="preventionLevel" value="3" <c:if test="${form.issueType == '3' && form.preventionLevel == '3' }">checked="checked"</c:if> <c:if test="${!ownerProcess}">disabled</c:if>/>不足
					</div>
				</td>
			</tr>
		</table>
		<table class="table">
			<tr>
				<td><label>總經理簽核</label></td>
				<td><label>副總經理簽核</label></td>
				<td><label>技術主管簽名</label></td>
				<td><label>營運主管簽名</label></td>
			</tr>
			<tr>
				<td>${stamp['總經理'].info}</td>
				<td>${stamp['副總經理'].info}</td>
				<td>${stamp['技術主管'].info}</td>
				<td>${stamp['營運主管'].info}</td>
			</tr>
		</table>
		
		<!-- 退件原因 -->
		<table class="table">
			<tr>
				<td>
					退件原因：<input id="memo" name="memo" size="30"/>
					<input type="button" value="退件" id="reject"/>
				</td>
			</tr>
		</table>
		<!-- flow button -->
		<input type="button" value="取消" id="abort" />
		
		<c:if test="${isVerify}">
			<input type="button" value="審核" id="verify"/>
		</c:if>
		
		<c:if test="${isCountersign}">
		<input type="button" value="會簽" id="countersign"/>
		</c:if>
		
		<c:if test="${isPass}">
			<input type="button" value="放行" id="pass"/>
		</c:if>		
	</div>		
</div>

<script type="text/javascript">
$(function(){
	$('#abort').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();	
			
		$.ajax({
			url : '${ctx}/issueForm/abort',
			dataType : 'json',
			data : {
				'id' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#verify').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		var ownerProcess = ${ownerProcess};
		var repairProcess = ${repairProcess && !ownerProcess};
		var preventionProcess = ${preventionProcess && !repairProcess};
		
		var data = "";
		
		if(ownerProcess) {
	    	if(!checkValidate(1) || !checkValidate(2)) {
				return;
			}
	    	// assign
	    	var assignUser = "";
	    	/*
	    	if(!$("#assignList").length) {
	    		if(!confirm("確定不指派修復人員")) {
	    			return;
	    		}
	    	}
	    	else {
	    		assignUser = $("#assignList").multipleSelect("getSelects").join();
	    		console.log("assignUser=" + assignUser);
	    	}
	    	*/
	    	
	    	data = {
	    		'id1' : id1,
				'id2' : id2,
				'repairTime': $("#repairTime").val(),
	            'repairFeedbackTime': $("#repairFeedbackTime").val(),
	            'repairFeedbackChannel': $('#repairFeedbackChannel').val(),
	            'issueDescription': $('[name=issueDescription]').val(),
	            'issueType': $('[name=issueType]:checked').val(),
	            'issueLevel': $('[name=issueLevel]:checked').val(),
	            'preventionLevel': $('[name=preventionLevel]:checked').val(),
	            'accountablity': $('[name=accountablity]').val(),
	            'owner': $('[name=owner]').val(),
	            'assignUser': assignUser
	        };
	    }
		else if(repairProcess) {
	    	if(!checkValidate(3)) {
				return;
			}
	    	
	    	data = {
	    		'id1' : id1,
				'id2' : id2,
	            'repairDescription': $('[name=repairDescription]').val()
	        };
	    }
		else if(preventionProcess) {
	    	if(!checkValidate(4)) {
				return;
			}
	    	
	    	data = {
	    		'id1' : id1,
				'id2' : id2,
	            'preventionDescription': $('[name=preventionDescription]').val()
	        };
	    }		
	    else {
	    	data = {
	    		'id1' : id1,
				'id2' : id2
			};
	    }
		
		console.log(JSON.stringify(data, null, 2));
		
		$.ajax({
			url : '${ctx}/issueForm/verify',
			dataType : 'json',
			data : data,
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});		
	});	
	
	$('#countersign').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		
		$.ajax({
			url : '${ctx}/issueForm/countersign',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#pass').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		
		$.ajax({
			url : '${ctx}/issueForm/pass',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#reject').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		var memo = $("#memo").val();
		
		$.ajax({
			url : '${ctx}/issueForm/reject',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2,
				'memo' : memo
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});
});

function checkValidate(type) {
	console.log("check type=" + type);
	var pass = true;
	var html = "<font class='warning' color='red'>請輸入此欄位</font>";
	
	if(type == 1) {
		if(!$('[name=feedbackChannel]').is(':checked')) {
			pass = false;
			$('[name=feedbackChannel]').parent().find('.warning').remove();
			$('[name=feedbackChannel]').parent().append(html);		
		}
		else {
			$('[name=feedbackChannel]').parent().find('.warning').remove();
			
			var items = 0;
			$("input[name='feedbackChannel']:checked").each(function(){items += parseInt($(this).val());});
			console.log(items);
			
			$("#repairFeedbackChannel").val(items > 2 ? 0 : items);
		}
				
		if($('#repairTime').val() == '') {
			pass = false;
			$('#repairTime').parent().find('.warning').remove();
			$('#repairTime').parent().append(html);		
		}
		else {
			$('#repairTime').parent().find('.warning').remove();
		}
		
		if($('#repairFeedbackTime').val() == '') {
			pass = false;
			$('#repairFeedbackTime').parent().find('.warning').remove();
			$('#repairFeedbackTime').parent().append(html);		
		}
		else {
			$('#repairFeedbackTime').parent().find('.warning').remove();
		}				
	}
	
	if(type == 2) {
		if($('[name=issueDescription]').val() == '') {
			pass = false;
			$('[name=issueDescription]').parent().find('.warning').remove();
			$('[name=issueDescription]').parent().append(html);		
		}
		else {
			$('[name=issueDescription]').parent().find('.warning').remove();
		}
		
		if(!$('[name=issueType]').is(':checked')) {
			pass = false;
			$('[name=issueType]').parent().find('.warning').remove();
			$('[name=issueType]').parent().append(html);		
		}
		else {
			$('[name=issueType]').parent().find('.warning').remove();
		}
		
		if(!$('[name=issueLevel]').is(':checked')) {
			pass = false;
			$('[name=issueLevel]').parent().find('.warning').remove();
			$('[name=issueLevel]').parent().append(html);		
		}
		else {
			$('[name=issueLevel]').parent().find('.warning').remove();
		}
		
		if(!$('[name=preventionLevel]').is(':checked')) {
			pass = false;
			$('[name=preventionLevel]').parent().find('.warning').remove();
			$('[name=preventionLevel]').parent().append(html);		
		}
		else {
			$('[name=preventionLevel]').parent().find('.warning').remove();
		}					
		
		if($('[name=accountablity]').val() == '' || $('[name=owner]').val() == '') {
			pass = false;
			$('[name=owner]').parent().find('.warning').remove();
			$('[name=owner]').parent().append(html);		
		}
		else {
			$('[name=owner]').parent().find('.warning').remove();
		}
	}
	
	if(type == 3) {
		if($('[name=repairDescription]').val() == '') {
			pass = false;
			$('[name=repairDescription]').parent().find('.warning').remove();
			$('[name=repairDescription]').parent().append(html);		
		}
		else {
			$('[name=repairDescription]').parent().find('.warning').remove();
		}
	}
	
	if(type == 4) {
		if($('[name=preventionDescription]').val() == '') {
			pass = false;
			$('[name=preventionDescription]').parent().find('.warning').remove();
			$('[name=preventionDescription]').parent().append(html);		
		}
		else {
			$('[name=preventionDescription]').parent().find('.warning').remove();
		}
	}
		
	return pass;	
}

function assign() {
	var link = $("#assign");
	var span = $("#assign_span");
	
	var role = [];
	$('input[name=formRole]:checked').map(function() {
		role.push($(this).val());
	});
	console.log(role);
	
	if(role.length == 0) {
		alert("請選擇指派角色");
		return false;
	}
			
	$.ajax({
		url : '${ctx}/issueForm/assignList',
		dataType : 'json',
		data : {
			'role' : role
		},
		success : function(data) {
			if(data.code == "0000") {
				var s = $('<select id="assignList" width="100px"/>');

				for(var r in data.formUsers) {
					var user = data.formUsers[r];
					$('<option />', {value: user.uid, text: user.name + "(" + user.role + ")"}).appendTo(s);
				}

				span.html(s);
				
				$("#assignList").multipleSelect({
					selectAll: false,
					allSelected : "全部",
					countSelected : "#個已選擇"
				});
									
				$("#assignList").multipleSelect("uncheckAll");
				
				link.hide();
			}
			else {
				alert(data.message);
			}			
		},
		error : function() {
			alert('網絡連接失敗，請聯繫管理員');
		}
	});
}
</script>