<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
<head>
<title>8888play ERP</title>
</head>
<body>
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				系統管理 <small>修改密碼</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="${ctx}/index"><i class="fa fa-dashboard"></i>8888play ERP</a></li>
				<li class="active">修改密碼</li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="panel panel-default">
				<div class="panel-body">
					<form action="${ctx}/permission/updateUserPwd" method="post"
						id="pwdForm">
						<div class="table-responsive">
							<table class="table" style="width: 600px;">
								<tr>
									<td>新密碼：</td>
									<td><input type="password" id="password"></td>
								</tr>
								<tr>
									<td>確認密碼：</td>
									<td><input type="password" id="confirmPwd"></td>
								</tr>
								<tr>
									<td><input type="submit" value="確定"></td>
								</tr>
							</table>
						</div>
						<input type="hidden" name="userId" value="${loginUser.id}">
						<input type="hidden" name="password" id="realPassword">
					</form>
				</div>
			</div>
		</section>
	</aside>

	<script type="text/javascript" src="${ctx }/js/jquery.form.js"></script>
	<script type="text/javascript" src="${ctx }/js/md5.min.js"></script>
	<script type="text/javascript">
		$(function() {
			$('#pwdForm').submit(function() {
				var password = $('#password').val();
				console.log(password);
				password = md5(password).toUpperCase();
				console.log(password);
				$('#realPassword').val(password);
				$(this).ajaxSubmit({
					beforeSubmit : function() {
						if ($('#password').val() == '') {
							alert('請輸入密碼');
							return false;
						}
						if ($('#confirmPwd').val() == '') {
							alert('請確認密碼');
							return false;
						}
						if ($('#password').val() != $('#confirmPwd').val()) {
							alert('兩次輸入密碼不一致');
							return false;
						}

					},
					success : function(data) {
						if (data.code == 0) {
							alert('操作成功！');
						}
					},
					error : function() {
						alert('連接服務器失敗');
					}
				});
				return false;
			});
		});
	</script>
</body>
</html>