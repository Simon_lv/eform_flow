<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<form id="userForm" action="${ctx}/permission/saveOrUpdate"
	method="post">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<h4 class="modal-title" id="myModalLabel"></h4>
	</div>
	<div class="modal-body">
		<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<tr>
					<td>職務權限</td>
					<td>
						<c:if test="${user==null || user.roleId1!=null}">
							身份1：
							<select name="roleId1">
							<option value="">請選擇</option>
							<c:forEach items="${roles}" var="r">
							<option value="${r.id}"
							<c:if test="${user.roleId1==r.id}">selected="selected"</c:if>>${r.name}</option>
							</c:forEach>
							</select>
						</c:if>
						<c:if test="${user==null || user.roleId2!=null}">
							身份2：
							<select name="roleId2">
							<option value="">請選擇</option>
							<c:forEach items="${roles}" var="r2">
							<option value="${r2.id}" <c:if test="${user.roleId2==r2.id}">selected="selected"</c:if>>${r2.name}</option>
							</c:forEach>
							</select>
						</c:if>
						<c:if test="${user!= null&&user.roleId2==null && user.roleId2==null}">
							身份：
							<select name="roleId1">
							<option value="">請選擇</option>
							<c:forEach items="${roles}" var="r2">
							<option value="${r2.id}">${r2.name}</option>
							</c:forEach>
							</select>
						</c:if>
					</td>
				</tr>
				<tr>
					<td>表單權限</td>
					<td>
						身份1：
						<select name="formRoleId1">
						<option value="">請選擇</option>
						<c:forEach items="${froles}" var="r">
						<option value="${r.id}" <c:if test="${user.formRoleId1 == r.id }">selected="selected"</c:if>>${r.name}</option>
						</c:forEach>
						</select>
						
						身份2：
						<select name="formRoleId2">
						<option value="">請選擇</option>
						<c:forEach items="${froles}" var="r2">
						<option value="${r2.id}" <c:if test="${user.formRoleId2 == r2.id }">selected="selected"</c:if>>${r2.name}</option>
						</c:forEach>
						</select>
						<br>
						身份3：
						<select name="formRoleId3">
						<option value="">請選擇</option>
						<c:forEach items="${froles}" var="r3">
						<option value="${r3.id}" <c:if test="${user.formRoleId3 == r3.id }">selected="selected"</c:if>>${r3.name}</option>
						</c:forEach>
						</select>
						
						身份4：
						<select name="formRoleId4">
						<option value="">請選擇</option>
						<c:forEach items="${froles}" var="r4">
						<option value="${r4.id}" <c:if test="${user.formRoleId4 == r4.id }">selected="selected"</c:if>>${r4.name}</option>
						</c:forEach>
						</select>
						<br>
						身份5：
						<select name="formRoleId5">
						<option value="">請選擇</option>
						<c:forEach items="${froles}" var="r5">
						<option value="${r5.id}" <c:if test="${user.formRoleId5 == r5.id }">selected="selected"</c:if>>${r5.name}</option>
						</c:forEach>
						</select>
						
						身份6：
						<select name="formRoleId6">
						<option value="">請選擇</option>
						<c:forEach items="${froles}" var="r6">
						<option value="${r6.id}" <c:if test="${user.formRoleId6 == r6.id }">selected="selected"</c:if>>${r6.name}</option>
						</c:forEach>
						</select>
						<p>
						同部門作業限定：
						<input type="checkbox" name="formRoleDeptLimit" value="2" <c:if test="${user.formRoleDeptLimit == 2 }">checked="checked"</c:if>/>
					</td>
				</tr>				
				<tr>
					<td>帳號</td>
					<td><input name="loginAccount" id="loginAccount" value="${empty user ? '' : user.loginAccount}" /></td>
				</tr>
				<tr>
					<td>密碼</td>
					<td><input name="password" type="password" id="password" /></td>
				</tr>
				<tr>
					<td>員工帳號</td>
					<td>
						<table border="0">
							<c:forEach var="entry" items="${companyEmployeeList}">
								<c:set var="companyEmployee" value="${entry.value}"></c:set>
								<c:if test="${not empty companyEmployee}">
								<tr>
									<td><c:out value="${entry.key}"/></td>
									<td>
										<select name="employeeMapping">
											<option value="">請選擇</option>
							            <c:forEach items="${companyEmployee}" var="employee">
							                <option value="${employee.id}" <c:if test="${user.employeeMapping.contains(employee.id)}">selected="selected"</c:if>>${employee.name}</option>
							            </c:forEach>
							            </select>
						            </td>
						        </tr>    
					            </c:if>
					    	</c:forEach>
						</table>						
					</td>
				</tr>				
			</table>
			<span style="color:red;">管理員帳號(admin)請勿綁定任何表單權限及員工帳號</span>
		</div>		
		<input type="hidden" name="id" id="id" value="${user.id}" />
	</div>
</form>