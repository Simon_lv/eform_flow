<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="myfn" uri="/WEB-INF/mytag.tld"%>

<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<form id="departmentForm" action="${ctx}/permission/updateUserManageDept" method="post">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<h4 class="modal-title" id="myModalLabel"></h4>
	</div>
	<div class="modal-body">
		<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<c:forEach items="${company}" var="c">
				<tr>
					<td>${c.name}</td>
					<td>
						<c:forEach items="${c.department}" var="d">
						<input type="checkbox" name="department" id="dept_${d.id}" value="${d.id}" <c:if test="${myfn:contains(managed, d.id)}">checked</c:if>>
						<label for="dept_${d.id}">${d.name}</label><br>	
						</c:forEach>												
					</td>
				</tr>
				</c:forEach>				
			</table>			
		</div>		
		<input type="hidden" name="uid" value="${uid}" />
	</div>
</form>