<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->	
	<input type="hidden" name="id1" id="id1" value="${param.id1}" />	
	<input type="hidden" name="id2" id="id2" value="${param.id2}" />	
	<div class="table-responsive">
		
		<div style="text-align: center;"><h2>${form.companyName}</h2></div>
		<div style="text-align: center;"><h3>合約審閱申請單</h3></div>
		
		<div style="text-align: left;"><label>編號&nbsp;</label>${form.serialNo}</div>
		<table class="table">
			<tr>
				<td><label>合約名稱：</label></td>
				<td colspan="5">${form.contractTitle }</td>
			</tr>
			<tr>
				<td><label>簽約對象：</label></td>
				<td colspan="5">${form.contractTarget }</td>
			</tr>
			<tr>
				<td><label>送簽單位：</label></td>
				<td>${form.departmentName }</td>
				<td><label>承辦人：</label></td>
				<td>
					<div>${form.organizerName }</div>
					<div>(<label>分機：</label>${form.organizerExtension })</div>
				</td>
				<td><label>送簽日：</label></td>
				<td><fmt:formatDate value="${form.requestDate }" type="date" pattern="yyyy-MM-dd" /></td>
			</tr>
			<tr>
				<td colspan="2">						
					<input type="radio" name="urgentDispatch" value="1" <c:if test="${form.urgentDispatch == 1 }">checked="checked"</c:if>/>特急件
					<input type="radio" name="urgentDispatch" value="2" <c:if test="${form.urgentDispatch == 2 }">checked="checked"</c:if>/>急件
					<input type="radio" name="urgentDispatch" value="3" <c:if test="${form.urgentDispatch == 3 }">checked="checked"</c:if>/>普通
				</td>
				<td colspan="2">
					<label>簽署方式：</label>
					<input type="checkbox"  name="signWay" value="1" <c:if test="${fn:contains(form.signWay, '1') }">checked="checked"</c:if>/>經濟部大小章
					<input type="checkbox"  name="signWay" value="2" <c:if test="${fn:contains(form.signWay, '2') }">checked="checked"</c:if>/>授權代表簽名
					<input type="checkbox"  name="signWay" value="3" <c:if test="${fn:contains(form.signWay, '3') }">checked="checked"</c:if>/>發票章
				</td>
				<td><label>特定日期要求：</label></td>
				<td>
					<fmt:formatDate value="${form.definiteTerm }" type="date" pattern="yyyy-MM-dd" />
				</td>
			</tr>
			<tr>
				<td><label>理由：</label></td>
				<td colspan="5">${form.urgentDispatchReason }</td>
			</tr>	
		</table>
		<table class="table">
			<tr>					
				<td width="30%"><label>夾帶檔案</label></td>
				<td><label>意見</label><td>
				<td><label>日期</label></td>
				<td>
					<c:if test="${isVerify}">
					<input class="btn btn-success" type="button" value="增加" id="addColumn"/>
					</c:if>
				</td>
			</tr>
			<tbody id="attachmentBody">
			<tr>
			<c:forEach items="${crm}" var="crm">
			<tr>					
				<td><a href="${crm.url}" target="_blank"><fmt:formatDate value="${crm.attachmentCreateTime }" type="date" pattern="yyyy-MM-dd" /></a></td>
				<td>${crm.memo }</td>
				<td><fmt:formatDate value="${crm.createTime }" type="date" pattern="yyyy-MM-dd" /></td>
				<td></td>
			</tr>
			</c:forEach>
			</tr>
			<c:if test="${isVerify}">
			<tr>					
				<td>
					<input name="attachment_url" value="" size="50"/>
					<input class="upload" type="button" value="上傳"/>
				</td>
				<td></td>
				<td><textarea name="attachment_memo" style="width: 90%;"></textarea></td>
				<td></td>				
			</tr>
			</c:if>
			</tbody>
		</table>
		<table class="table">
			<tr>
				<td width="15%"><label>重點條款說明：</label></td>
				<td>${form.keyPoint }</td>
			</tr>
		</table>
		<table class="table">
			<tr>
				<td><label>總經理</label></td>
				<td><label>副總經理</label></td>
				<td><label>法務</label></td>
				<td><label>行政主管</label></td>
				<td><label>部門主管</label></td>
				<td><label>承辦人</label></td>
			</tr>
			<tr>
				<td>${stamp['總經理'].info}</td>
				<td>					
					<c:if test="${empty stamp['部門主管'].info}">
						${stamp['副總經理2'].info}
					</c:if>
					<c:if test="${not empty stamp['部門主管'].info}">
						${stamp['副總經理'].info}
					</c:if>
				</td>
				<td>${stamp['法務'].info}</td>
				<td>${stamp['行政主管'].info}</td>
				<td>
					${stamp['部門主管'].info}
					<c:if test="${empty stamp['部門主管'].info}">
						${stamp['副總經理'].info}
					</c:if>
				</td>
				<td>${stamp['申請人'].info}</td>
			</tr>
		</table>
		
		<form name="upload" id="fileUploadForm" enctype="multipart/form-data" method="post">		
			<div style="display: none">
				<input type="file" id="fileBrowse" name="file" style="border: 0;" />
			</div>
		</form>
		
		<!-- 退件原因 -->
		<table class="table">
			<tr>
				<td>
					退件原因：<input id="memo" name="memo" size="30"/>
					<input type="button" value="退件" id="reject"/>
				</td>
			</tr>
		</table>
		
		<!-- flow button -->
		<input type="button" value="取消" id="abort" />
	
		<c:if test="${isVerify}">
			<input type="button" value="審核" id="verify"/>
		</c:if>
		
		<c:if test="${isCountersign}">
		<input type="button" value="會簽" id="countersign"/>
		</c:if>
		
		<c:if test="${isPass}">
			<input type="button" value="放行" id="pass"/>
		</c:if>					
	</div>		
</div>

<script type="text/javascript">
$(function(){
	$('#abort').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
			
		$.ajax({
			url : '${ctx}/contractReviewForm/abort',
			dataType : 'json',
			data : {
				'id' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#verify').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		
		var multi_url = "";
		$("input[name='attachment_url']").each(function() {
			if(multi_url == "") {
				multi_url = $(this).val();
			}
			else {
				multi_url = "," + $(this).val();
			}			
		});
		
		var multi_memo = "";
		$("textarea[name='attachment_memo']").each(function() {
			if(multi_memo == "") {
				multi_memo = $(this).val();
			}
			else {
				multi_memo = "," + $(this).val();
			}
		});
		
		$.ajax({
			url : '${ctx}/contractReviewForm/verify',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2,
				'url': multi_url,
				'memo' : multi_memo
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#countersign').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		
		$.ajax({
			url : '${ctx}/contractReviewForm/countersign',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#pass').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		
		$.ajax({
			url : '${ctx}/contractReviewForm/pass',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#reject').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		var memo = $("#memo").val();
		
		$.ajax({
			url : '${ctx}/contractReviewForm/reject',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2,
				'memo' : memo
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});
	
	$('#addColumn').click(function(){
		var html = '';
		html += '<tr>';
		html += '<td><input name="url" value="">';
		html += '<input class="upload" type="button" value="上傳"></td>';
		html += '<td></td>';
		html += '<td><textarea name="memo" style="width: 90%;"></textarea></td>';
		html += '<td></td>';
		html += '<td><input class="btn delete" type="button" value="刪除"></td>';
		html += '</tr>';
		
		$('#attachmentBody').append(html);
	});
	
	var target;
	$('#attachmentBody').on('click', '.upload', function(){
		target = $(this).prev();
		$('#fileBrowse').click();
	});
	
	$('#fileUploadForm').on('submit', (function(e) {		
		e.preventDefault();
		
		var formData = new FormData(this);
		$.ajax({
			async : false,
			type : 'POST',
			url : '${ctx}/contractReviewForm/upload.do',
			data : formData,
			cache : false,
			contentType : false,
			processData : false,
			success : function(data) {
				if (data == "overSize") {
					alert("檔案超過1MB請更換圖片");
					$("#fileBrowse").val("");
					return;
				}				
				//$("#url").val(data.url);
				target.val(data.url);
			},
			error : function(data) {
				alert("系統錯誤，請稍後再試");
			}
		});
	}));
	
	$("#fileBrowse").on("change", function() {
		if (this.files[0].size >= 1048576) {
			alert("檔案超過1MB請更換檔案");
			return;
		}	
		$("#fileUploadForm").submit();
	});
	
	$('#attachmentBody').on('click', '.delete', function(){
		$(this).parent().parent().remove();
	});
	
	$('input[type=radio]').each(function(){
		$(this).attr("disabled", true);		
	});
	
	$('input[type=checkbox]').each(function(){
		$(this).attr("disabled", true);		
	});
});
</script>