<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->	
	<div class="table-responsive">
		
		<div style="text-align: center;"><h2>${form.companyName}</h2></div>
		<div style="text-align: center;"><h3>合約審閱申請單</h3></div>
		
		<div style="text-align: left;"><label>編號&nbsp;</label>${cp.serialNo}</div>
		<table class="table">
			<tr>
				<td><label>合約名稱：</label></td>
				<td colspan="5">${cp.contractTitle }</td>
			</tr>
			<tr>
				<td><label>簽約對象：</label></td>
				<td colspan="5">${cp.contractTarget }</td>
			</tr>
			<tr>
				<td><label>送簽單位：</label></td>
				<td>${cp.departmentName }</td>
				<td><label>承辦人：</label></td>
				<td>
					<div>${cp.organizerName }</div>
					<div>(<label>分機：</label>${cp.organizerExtension })</div>
				</td>
				<td><label>送簽日：</label></td>
				<td><fmt:formatDate value="${cp.requestDate }" type="date" pattern="yyyy-MM-dd" /></td>
			</tr>
			<tr>
				<td colspan="2">						
					<input type="radio" name="urgentDispatch" value="1" <c:if test="${cp.urgentDispatch == 1 }">checked="checked"</c:if>/>特急件
					<input type="radio" name="urgentDispatch" value="2" <c:if test="${cp.urgentDispatch == 2 }">checked="checked"</c:if>/>急件
					<input type="radio" name="urgentDispatch" value="3" <c:if test="${cp.urgentDispatch == 3 }">checked="checked"</c:if>/>普通
				</td>
				<td colspan="2">
					<label>簽署方式：</label>
					<input type="checkbox"  name="signWay" value="1" <c:if test="${fn:contains(cp.signWay, '1') }">checked="checked"</c:if>/>經濟部大小章
					<input type="checkbox"  name="signWay" value="2" <c:if test="${fn:contains(cp.signWay, '2') }">checked="checked"</c:if>/>授權代表簽名
					<input type="checkbox"  name="signWay" value="3" <c:if test="${fn:contains(cp.signWay, '3') }">checked="checked"</c:if>/>發票章
				</td>
				<td><label>特定日期要求：</label></td>
				<td>
					<fmt:formatDate value="${cp.definiteTerm }" type="date" pattern="yyyy-MM-dd" />
				</td>
			</tr>
			<tr>
				<td><label>理由：</label></td>
				<td colspan="5">${cp.urgentDispatchReason }</td>
			</tr>	
		</table>
		<table class="table">
			<tr>					
				<td width="30%"><label>夾帶檔案</label></td>
				<td><label>意見</label><td>
				<td><label>日期</label></td>
			</tr>				
			<c:forEach items="${crm }" var="crm">
			<tr>					
				<td><a href="${crm.url}" target="_blank"><fmt:formatDate value="${crm.attachmentCreateTime }" type="date" pattern="yyyy-MM-dd" /></a></td>
				<td>${crm.memo }</td>
				<td><fmt:formatDate value="${crm.createTime }" type="date" pattern="yyyy-MM-dd" /></td>
			</tr>
			</c:forEach>				
		</table>
		<table class="table">
			<tr>
				<td width="15%"><label>重點條款說明：</label></td>
				<td>${cp.keyPoint }</td>
			</tr>
		</table>
		<table class="table">
			<tr>
				<td><label>總經理</label></td>
				<td><label>副總經理</label></td>
				<td><label>法務</label></td>
				<td><label>行政主管</label></td>
				<td><label>部門主管</label></td>
				<td><label>承辦人</label></td>
			</tr>
			<tr>
				<td>${stamp['總經理'].info}</td>
				<td>					
					<c:if test="${empty stamp['部門主管'].info}">
						${stamp['副總經理2'].info}
					</c:if>
					<c:if test="${not empty stamp['部門主管'].info}">
						${stamp['副總經理'].info}
					</c:if>
				</td>
				<td>${stamp['法務'].info}</td>
				<td>${stamp['行政主管'].info}</td>
				<td>
					${stamp['部門主管'].info}
					<c:if test="${empty stamp['部門主管'].info}">
						${stamp['副總經理'].info}
					</c:if>
				</td>
				<td>${stamp['申請人'].info}</td>
			</tr>
		</table>			
	</div>	
</div>
<script type="text/javascript">
$(function(){
	$('input[type=radio]').each(function(){
		$(this).attr("disabled", true);		
	});
	
	$('input[type=checkbox]').each(function(){
		$(this).attr("disabled", true);		
	});
});
</script>