<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>

<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/contractReviewForm/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${cp.id}" />
		<input type="hidden" name="workFlowId" value="${cp.workFlowId}" />
		<input type="hidden" name="temp" id="temp" value=""/>
		<!-- 退件重送 -->
		<input type="hidden" name="serialNo" value="${cp.serialNo}" />
		<div class="table-responsive">
			
			<div style="text-align: center;"><h2 id="title"></h2></div>
			<div style="text-align: center;"><h3>合約審閱申請單</h3></div>
			
			<table class="table">
				<tr>
					<td>
						<select id="companyId" name="companyId">
                           	<c:forEach items="${sessionScope.company}" var="company">
                           	<option value="${company.id}"
                           		<c:if test="${cp.companyId == company.id}">selected</c:if>
                           		>${company.name}</option>
                           	</c:forEach>
                        </select>
					</td>
					<td colspan="6">
						<label>合約名稱：</label>
						<input maxlength="200" size="70" name="contractTitle" value="${cp.contractTitle }"/>
					</td>
				</tr>
				<tr>
					<td><label>簽約對象：</label></td>
					<td colspan="5"><input size="70" maxlength="200" name="contractTarget" value="${cp.contractTarget }"/></td>
				</tr>
				<tr>
					<td><label>送簽單位：</label></td>
					<td>
						<span id="department">
						<c:if test="${not empty cp }">${cp.departmentName }</c:if>
						</span>
						<input type="hidden" id="requestDepartment" name="requestDepartment" value="${cp.requestDepartment}" />
					</td>
					<td><label>承辦人：</label></td>
					<td>
						<div>
							<c:if test="${empty cp }">${sessionScope.employee.name }</c:if>
							<c:if test="${not empty cp }">${cp.organizerName }</c:if>
						</div>
						<div>
							(<label>分機：	</label>						
							<input name="organizerExtension" 
							<c:if test="${empty cp }">value="${sessionScope.employee.phoneExt }"</c:if>
							<c:if test="${not empty cp }">value="${cp.organizerExtension }"</c:if>							
							/>)
						</div>
					</td>
					<td><label>送簽日：</label></td>
					<td><fmt:formatDate value="${cp.requestDate }" type="date" pattern="yyyy-MM-dd" /></td>
				</tr>
				<tr>
					<td>						
						<input type="radio"  name="urgentDispatch" value="1" <c:if test="${cp.urgentDispatch == 1 }">checked="checked"</c:if>/>特急件
						<input type="radio"  name="urgentDispatch" value="2" <c:if test="${cp.urgentDispatch == 2 }">checked="checked"</c:if>/>急件
						<input type="radio"  name="urgentDispatch" value="3" <c:if test="${cp.urgentDispatch == 3 }">checked="checked"</c:if>/>普通
					</td>
					<td colspan="3">
						<label>簽署方式：</label>
						<input type="checkbox"  name="signWay" value="1" <c:if test="${fn:contains(cp.signWay, '1') }">checked="checked"</c:if>/>經濟部大小章
						<input type="checkbox"  name="signWay" value="2" <c:if test="${fn:contains(cp.signWay, '2') }">checked="checked"</c:if>/>授權代表簽名
						<input type="checkbox"  name="signWay" value="3" <c:if test="${fn:contains(cp.signWay, '3') }">checked="checked"</c:if>/>發票章
					</td>
					<td><label>特定日期要求：</label></td>
					<td>
						<input type="text" value="<fmt:formatDate value="${cp.definiteTerm }" type="date" pattern="yyyy-MM-dd" />"
							onfocus="WdatePicker({definiteTerm:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
							class="Wdate" name="definiteTerm" id="definiteTerm">
					</td>
				</tr>
				<tr>
					<td colspan="6">
						<label>理由：</label>
						<input name="urgentDispatchReason" value="${cp.urgentDispatchReason }"/>
					</td>
				</tr>	
			</table>
			<table class="table">				
				<tr>					
					<td><label>夾帶檔案</label></td>
					<td><label>日期</label></td>
					<td><label>意見</label></td>
					<td><label>日期</label></td>
					<td><input class="btn btn-success" type="button" value="增加" id="addColumn"/></td>
				</tr>				
				<tbody id="attachmentBody">
				<c:if test="${empty cp }">
				<tr>					
					<td>
						<input type="hidden" name="url" value=""/>
						<input name="fileName" value="" size="50"/>
						<input class="upload" type="button" value="上傳"/>
					</td>
					<td></td>
					<td><textarea name="memo" style="width: 100%;"></textarea></td>
					<td></td>
					<td></td>
				</tr>
				</c:if>
				
				<c:forEach items="${crm }" var="crm">
				<tr>					
					<td>
						<input type="hidden" name="url" value="${crm.url }" size="50"/>
						<input name="fileName" value="${crm.description}" size="50"/>
						<input type="button" value="上傳" onclick="$('#fileBrowse').click();"/>
					</td>
					<td><fmt:formatDate value="${crm.attachmentCreateTime }" type="date" pattern="yyyy-MM-dd" /></td>
					<td><textarea name="memo" style="width: 90%;">${crm.memo }</textarea></td>
					<td><fmt:formatDate value="${crm.createTime }" type="date" pattern="yyyy-MM-dd" /></td>
					<td><input class="btn" type="button" value="刪除" onclick="deleteAttachment('${crm.id}', '${cp.id }')"/></td>
				</tr>
				</c:forEach>
				
				</tbody>
			</table>
			<table class="table">
				<tr>
					<td width="15%"><label>重點條款說明：</label></td>
					<td>
						<textarea name="keyPoint" style="width: 90%;">${cp.keyPoint }</textarea>
					</td>
				</tr>
			</table>
			<table class="table">
			<tr>
				<td><label>總經理</label></td>
				<td><label>副總經理</label></td>
				<td><label>法務</label></td>
				<td><label>行政主管</label></td>
				<td><label>部門主管</label></td>
				<td><label>承辦人</label></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>${empty cp.creatorName ? sessionScope.employee.name : cp.organizerName}</td>
			</tr>
		</table>			
		</div>
	</form>
	<input type="button" value="保存" id="save"/>	
	<input type="button" value="送審" id="submit"/>
	
	<form name="upload" id="fileUploadForm" enctype="multipart/form-data" method="post">		
		<div style="display: none">
			<input type="file" id="fileBrowse" name="file" style="border: 0;" />
		</div>
	</form>
	
</div>

<script type="text/javascript">

$(function() {
	
	$('#save').click(function() {
		
		if(checkValidate()){			
			$("#editForm").ajaxSubmit({
	            success: function(e) {
	                alert(e.message), $("#myModal").modal("toggle");
	                $('#search').click();
	            },
	            error: function() {
	                alert("網路連接失敗，請聯繫管理員");
	            }
	        });	
		}
	});
	
	$('#addColumn').click(function(){
		var html = '';
		html += '<tr>';
		html += '<td><input type="hidden" name="url" value=""><input name="fileName" value="" size="50">';
		html += '<input class="upload" type="button" value="上傳"></td>';
		html += '<td></td>';
		html += '<td><textarea name="memo" style="width: 90%;"></textarea></td>';
		html += '<td></td>';
		html += '<td><input class="btn delete" type="button" value="刪除"></td>';
		html += '</tr>';
		
		$('#attachmentBody').append(html);
	});
	
	var target;  // fileName
	$('#attachmentBody').on('click', '.upload', function(){
		target = $(this).prev();
		$('#fileBrowse').click();
	});		
	
	$('#submit').click(function() {		
		if(checkValidate()){
			$('#editForm').attr('action', '${ctx}/contractReviewForm/submit');
			$("#editForm").ajaxSubmit({
	            success: function(e) {
	            	if(e.code == "0000") {
	            		alert(e.message), $("#myModal").modal("toggle");
		                $('#search').click();
	            	}
	            	else {
	            		alert(e.message);
	            	}
	            },
	            error: function() {
	                alert("網路連接失敗，請聯繫管理員");
	            }
	        });			
		}
	});
	
	$('#fileUploadForm').on('submit', (function(e) {		
		e.preventDefault();
		
		var formData = new FormData(this);
		$.ajax({
			async : false,
			type : 'POST',
			url : '${ctx}/contractReviewForm/upload.do',
			data : formData,
			cache : false,
			contentType : false,
			processData : false,
			success : function(data) {
				if (data == "overSize") {
					alert("檔案超過1MB請更換圖片");
					$("#fileBrowse").val("");
					return;
				}				
				//$("#url").val(data.url);
				target.val(data.description);
				target.prev().val(data.url);
			},
			error : function(data) {
				alert("系統錯誤，請稍後再試");
			}
		});
	}));
	
	$("#fileBrowse").on("change", function() {
		if (this.files[0].size >= 1048576) {
			alert("檔案超過1MB請更換檔案");
			return;
		}	
		$("#fileUploadForm").submit();
	});		
	
	$('#attachmentBody').on('click', '.delete', function(){
		$(this).parent().parent().remove();
	});
	
	$('#companyId').change(function() {
		var cid = $(this).val();
		var name = $(this).find("option:selected").text();
		$("#title").text(name);
		
		// department
		getUserInfo(cid);
	});
	
	// default title
	$("#title").text($('#companyId').find("option:selected").text());
	getUserInfo($('#companyId').val());	
});

function checkValidate(){
	var pass = true;
	var html = "<font class='warning' color='red'>請輸入此欄位</font>";
	
	var contractTitle = $('input[name=contractTitle]');
	if(contractTitle.val() == ''){
		pass = false;
		contractTitle.parent().find('.warning').remove();
		contractTitle.parent().append(html);		
	}else{
		contractTitle.parent().find('.warning').remove();
	}
	
	var contractTarget = $('input[name=contractTarget]');
	if(contractTarget.val() == ''){
		pass = false;
		contractTarget.parent().find('.warning').remove();
		contractTarget.parent().append(html);
	}else{
		contractTarget.parent().find('.warning').remove();
	}
	
	var organizer = $('input[name=organizer]');
	if(organizer.val() == ''){
		pass = false;
		organizer.parent().find('.warning').remove();
		organizer.parent().append(html);
	}else{
		organizer.parent().find('.warning').remove();
	}
	
	var urgentDispatch = $('input[name=urgentDispatch]');
	if(!urgentDispatch.is(":checked")){
		pass = false;
		urgentDispatch.parent().find('.warning').remove();
		urgentDispatch.parent().append(html);
	}else{
		urgentDispatch.parent().find('.warning').remove();
	}
		
	var signWay = $('input[name=signWay]');
	if(!signWay.is(":checked")){
		pass = false;
		signWay.parent().find('.warning').remove();
		signWay.parent().append(html);
	}else{
		signWay.parent().find('.warning').remove();
	}
	
	$('#attachmentBody').find('input[name=url]').each(function(){		
		if($(this).val() == ''){
			pass = false;
			$(this).parent().find('.warning').remove();
			$(this).parent().append(html);
		}else{
			$(this).parent().find('.warning').remove();			
		}		
	});
	
	$('#attachmentBody').find('textarea[name=memo]').each(function(){		
		if($(this).val() == ''){
			pass = false;
			$(this).parent().find('.warning').remove();
			$(this).parent().append(html);
		}else{
			$(this).parent().find('.warning').remove();			
		}		
	});
	
	if(!pass){
		alert("表單未填寫完整!");
	}
	
	return pass;
}

function getUserInfo(cid) {
	$.ajax({
		url : '${ctx}/employee/userInfo',
		dataType : 'json',
		data : {
			'id' : cid
		},
		success : function(data) {
			var dept = data.split(",");
			$("#department").text(dept[1]);
			$("#requestDepartment").val(dept[0]);
		},
		error : function() {
			alert('網絡連接失敗，請聯繫管理員');
		}
	});
}
</script>