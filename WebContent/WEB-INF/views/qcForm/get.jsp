<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->		
	<div class="table-responsive">
		<table class="table">
			<tr>
				<td colspan="2">${companyName}</td>
				<td>編號</td>
				<td>${serialNo}</td>
			</tr>
			<c:if test="${fn:length(formTitleField) > 0 }">
			<tr>					
				<td rowspan="${fn:length(formTitleField) }">
					<div style="text-align: center;"><h2>${formSchema.formName }</h2></div>
				</td>
				<td><label>${formTitleField[0].fieldLabel }</label></td>
				<td>${formTitleField[0].subTitle }</td>					
			</tr>					
			<c:forEach items="${formTitleField }" begin="1" var="ftf" >
			<tr>
				<td><label>${ftf.fieldLabel }</label></td>
				<td>${ftf.subTitle }</td>
			</tr>					
			</c:forEach>
			</c:if>
				
			<c:if test="${fn:length(forTitleField) == 0 }">
			<tr>
				<td><div style="text-align: center;"><h2>${formScheam.formName }</h2></div></td>										
			</tr>
			</c:if>
		</table>	
		
		<table class="table">
			<tr>
				<td colspan="3">測項</td>
				<td>測試結果</td>
				<td>PM</td>
				<td>備註</td>
			</tr>
			
			<c:set var="count" value="0"/>
			<c:set var="groupCount" value="${fn:length(fieldCount) }"/>
			<c:if test="${formSchema.flowType == 2 }">
				<c:set var="groupCount" value="${groupCount-1 }"/>					
			</c:if>
							
			<c:forEach begin="0" end="${groupCount-1 }" varStatus="loop">
			<tr>
				<td>${count+1 }</td>
				<td rowspan="${fieldCount[loop.index] }">${formDetailField[count].groupLabel }</td>
				<td>${formDetailField[count].fieldLabel }</td>
				<td>
					<c:if test="${formDetailField[count].qcResult == 0 }">N/A</c:if>
					<c:if test="${formDetailField[count].qcResult == 1 }">O</c:if>
					<c:if test="${formDetailField[count].qcResult == 2 }">X</c:if>										
				</td>
				<td>
					<c:if test="${formDetailField[count].pmResult == 0 }">N/A</c:if>
					<c:if test="${formDetailField[count].pmResult == 1 }">O</c:if>
					<c:if test="${formDetailField[count].pmResult == 2 }">X</c:if>										
				</td>
				<td>${formDetailField[count].remark }</td>
			</tr>
			
			<c:forEach items="${formDetailField }" begin="${count+1 }" end="${count+fieldCount[loop.index]-1 }" var="fdf" varStatus="loop2">
			
			<c:set var="count" value="${count+1 }"/>
			<tr>
				<td>${count+1 }</td>					
				<td>${fdf.fieldLabel }</td>
				<td>
					<c:if test="${fdf.qcResult == 0 }">N/A</c:if>
					<c:if test="${fdf.qcResult == 1 }">O</c:if>
					<c:if test="${fdf.qcResult == 2 }">X</c:if>										
				</td>
				<td>
					<c:if test="${fdf.pmResult == 0 }">N/A</c:if>
					<c:if test="${fdf.pmResult == 1 }">O</c:if>
					<c:if test="${fdf.pmResult == 2 }">X</c:if>					
				</td>
				<td>${fdf.remark }</td>
			</tr>
			</c:forEach>
			<c:set var="count" value="${count+1 }"/>
			</c:forEach>
		</table>
		<table class="table">
			<tr>
				<td><label>營運主管</label></td>
				<td><label>產品</label></td>
				<td><label>技術主管</label></td>
				<td><label>測試人員</label></td>
			</tr>
			<tr>
				<td>${stamp['營運主管'].info}</td>
				<td>${stamp['產品'].info}</td>
				<td>${stamp['技術主管'].info}</td>
				<td>${stamp['申請人'].info}</td>
			</tr>				
		</table>
		
		<c:if test="${formSchema.flowType == 2 }">
		<table class="table">
			<tr>
				<td colspan="3">測項</td>
				<td>測試結果</td>
				<td>PM</td>
				<td>備註</td>
			</tr>
		
			<tr>
				<td>${count+1 }</td>
				<td>${formDetailField[count].groupLabel }</td>
				<td>${formDetailField[count].fieldLabel }</td>
				<td>					
					<c:if test="${formDetailField[count].qcResult == 0 }">N/A</c:if>
					<c:if test="${formDetailField[count].qcResult == 1 }">O</c:if>
					<c:if test="${formDetailField[count].qcResult == 2 }">X</c:if>								
				</td>
				<td>
					<c:if test="${formDetailField[count].pmResult == 0 }">N/A</c:if>
					<c:if test="${formDetailField[count].pmResult == 1 }">O</c:if>
					<c:if test="${formDetailField[count].pmResult == 2 }">X</c:if>					
				</td>
				<td>${formDetailField[count].remark }</td>
			</tr>
		</table>
		
		<table class="table">
			<tr>						
				<td><label>營運主管</label></td>
				<td><label>產品</label></td>
				<td><label>技術主管</label></td>
				<td><label>測試人員</label></td>
			</tr>
			<tr>
				<td>${stamp['營運主管2'].info}</td>
				<td>${stamp['產品2'].info}</td>
				<td>${stamp['技術主管2'].info}</td>
				<td>${stamp['測試'].info}</td>		
			</tr>
		</table>
		</c:if>		
	</div>	
</div>