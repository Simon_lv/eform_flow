<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	
	<table class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>
				<th>表單編號</th>				
				<th>公司</th>				
				<th>申請人</th>
				<th>測試內容</th>
				<th>表單名稱</th>
				<th>狀態</th>	
				<th>操作</th>				
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
				<tr>
					<td>${o.serialNo }</td>
					<td>${o.companyName}</td>					
					<td>${o.creatorName }</td>
					<td>${o.subTitleList}</td>
					<td>${o.formName }</td>				
					<td>
						<c:choose>
						    <c:when test="${o.status == -1}">編輯中</c:when>
						    <c:when test="${o.status == 0}">正常結束</c:when>
						    <c:when test="${o.status == 1}">執行中</c:when>
						    <c:when test="${o.status == 8}">註銷</c:when>
						    <c:when test="${o.status == 9}">退件</c:when>
						    <c:otherwise>未定義(o.status)</c:otherwise>
						</c:choose>
					</td>			
					<td>
						<!-- 審核歷程 -->									
						<c:if test="${o.status >= 0}">
							<a href="javascript:;" onclick="history(${o.workFlowId})">審核歷程</a>
						</c:if>
						<c:if test="${o.status == -1 && o.creator == sessionScope.loginUser.id }">
						<a href="javascript:;" onclick="edit('${o.formId}')">編輯</a>
						</c:if>
						<c:if test="${o.status == 1 && o.creator == sessionScope.loginUser.id}">
						<a href="javascript:;" onclick="terminate(${o.workFlowId})">撤銷</a>
						</c:if>
						<a href="javascript:;" onclick="get('${o.formId}')">檢視</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<my:pagination page="${page}" />