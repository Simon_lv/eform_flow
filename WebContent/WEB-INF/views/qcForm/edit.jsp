<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/qcForm/save" method="post" id="editForm">		
		<input type="hidden" name="workFlowId" value="${workFlowId}" />
		<!-- 退件重送 -->
		<input type="hidden" name="serialNo" value="${serialNo}" />		
		<div class="table-responsive">
			<c:if test="${not empty formSchemas }">
			<label>請選擇表單:</label>
			<select name="schemaId" id="schemaId" onchange="changeSchemaId(this.value)">
				<option value="">請選擇</option>
				<c:forEach items="${formSchemas }" var="fs">
				<option value="${fs.id }">${fs.formName }</option>
			</c:forEach>
			</select>
			<select id="companyId" name="companyId">
               	<c:forEach items="${sessionScope.company}" var="company">
               	<option value="${company.id}">${company.name}</option>
               	</c:forEach>
            </select>
			
			<div id="qcForm"></div>			
			</c:if>
			
			<c:if test="${empty formSchemas }">
			<input type="hidden" name="schemaId" value="${formSchema.id }">
			<input type="hidden" name="formCode" value="${formSchema.formCode }">
			<input type="hidden" name="formId" value="${formId }">			
			<input type="hidden" id="requestDepartment" name="requestDepartment" value="${departmentId}" />
			
			<table class="table">
				<c:if test="${not empty formId }">
				<tr>
					<td colspan="3">
						<select id="companyId" name="companyId" onchange="getUserInfo(this.vlaue)">
			               	<c:forEach items="${sessionScope.company}" var="company">
			               	<option value="${company.id}"
			               		<c:if test="${companyId == company.id}">selected</c:if>
			               		>${company.name}</option>
			               	</c:forEach>
			            </select>
					</td>
				</tr>
				</c:if>
				<c:if test="${fn:length(formTitleField) > 0 }">
				<tr>					
					<td rowspan="${fn:length(formTitleField)}">
						<div style="text-align: center;"><h2>${formSchema.formName }</h2></div>
					</td>
					<td><label>${formTitleField[0].fieldLabel }</label></td>
					<td>					
						<c:if test="${fn:contains(formTitleField[0].fieldLabel, '日期') }">
						<input type="text" value="${formTitleField[0].subTitle }" onfocus="WdatePicker({subTitle:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})" class="Wdate" name="subTitle" id="subTitle">
						</c:if>
						
						<c:if test="${!fn:contains(formTitleField[0].fieldLabel, '日期') }">
						<c:if test="${fn:contains(formTitleField[0].fieldLabel, '產品') || fn:contains(formTitleField[0].fieldLabel, '遊戲') }">
						<select name="subTitle" id="gameDataId">
							<option value="">請選擇</option>
							<c:forEach items="${allGameData}" var="a">
							<option value="${a.name}" <c:if test="${a.name == formTitleField[0].subTitle}">selected="selected"</c:if> > ${a.name}</option>
							</c:forEach>
						</select>
						</c:if>
						<c:if test="${!fn:contains(formTitleField[0].fieldLabel, '產品') && !fn:contains(formTitleField[0].fieldLabel, '遊戲') }">
						<input name="subTitle" value="${formTitleField[0].subTitle }">
						</c:if>						
						</c:if>						
					</td>					
				</tr>					
				<c:forEach items="${formTitleField }" begin="1" var="ftf" >
				<tr>
					<td><label>${ftf.fieldLabel }</label></td>
					<td>					
						<c:if test="${fn:contains(ftf.fieldLabel, '日期') }">
						<input type="text" value="${ftf.subTitle }" onfocus="WdatePicker({subTitle:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})" class="Wdate" name="subTitle" id="subTitle">
						</c:if>
						<c:if test="${!fn:contains(ftf.fieldLabel, '日期') }">
						<c:if test="${fn:contains(ftf.fieldLabel, '產品') || fn:contains(ftf.fieldLabel, '遊戲') }">
						<select name="subTitle" id="gameDataId">
							<c:forEach items="${allGameData}" var="a">
							<option value="${a.name}" <c:if test="${a.name == ftf.subTitle}">selected="selected"</c:if> > ${a.name}</option>
							</c:forEach>
						</select>
						</c:if>
						<c:if test="${!fn:contains(ftf.fieldLabel, '產品') && !fn:contains(ftf.fieldLabel, '遊戲') }">
						<input name="subTitle" value="${ftf.subTitle }">
						</c:if>
						</c:if>						
					</td>
				</tr>					
				</c:forEach>
				</c:if>
					
				<c:if test="${fn:length(formTitleField) == 0 }">
				<tr>
					<td><div style="text-align: center;"><h2>${formScheam.formName }</h2></div></td>										
				</tr>
				</c:if>
			</table>	
			
			<table class="table">
				<tr>
					<td colspan="3">測項</td>
					<td>測試結果</td>
					<td>PM</td>
					<td>備註</td>
				</tr>
				
				<c:set var="count" value="0"/>
				<c:set var="groupCount" value="${fn:length(fieldCount) }"/>				
				<c:if test="${formSchema.flowType == 2 }">
					<c:set var="groupCount" value="${groupCount-1 }"/>					
				</c:if>
								
				<c:forEach begin="0" end="${groupCount-1 }" varStatus="loop">
				<tr>
					<td>${count+1 }</td>
					<td rowspan="${fieldCount[loop.index] }">${formDetailField[count].groupLabel }</td>
					<td>${formDetailField[count].fieldLabel }</td>
					<td>
						<select name="qcResult">
							<option value="0">N/A</option>
							<option value="1" <c:if test="${formDetailField[count].qcResult == 1 }">selected="selected"</c:if>>O</option>
							<option value="2" <c:if test="${formDetailField[count].qcResult == 2 }">selected="selected"</c:if>>X</option>
						</select>						
					</td>
					<td></td>
					<td><input name="remark" value="${formDetailField[count].remark }"></td>
				</tr>
				
				<c:forEach items="${formDetailField }" begin="${count+1 }" end="${count+fieldCount[loop.index]-1 }" var="fdf" varStatus="loop2">
				
				<c:set var="count" value="${count+1 }"/>
				<tr>
					<td>${count+1 }</td>					
					<td>${fdf.fieldLabel }</td>
					<td>
						<select name="qcResult">
							<option value="0">N/A</option>
							<option value="1" <c:if test="${fdf.qcResult == 1 }">selected="selected"</c:if>>O</option>
							<option value="2" <c:if test="${fdf.qcResult == 2 }">selected="selected"</c:if>>X</option>
						</select>						
					</td>
					<td></td>
					<td><input name="remark" value="${fdf.remark }"></td>
				</tr>
				</c:forEach>
				<c:set var="count" value="${count+1 }"/>
				</c:forEach>
			</table>
			<table class="table">
				<tr>
					<td><label>營運中心主管簽核</label></td>
					<td><label>產品PM</label></td>
					<td><label>技術中心主管簽核</label></td>
					<td><label>測試人員</label></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td>${empty cp.creatorName ? sessionScope.employee.name : cp.creatorName}</td>		
				</tr>				
			</table>
			
			<c:if test="${formSchema.flowType == 2 }">
			<table class="table">
				<tr>
					<td colspan="3">測項</td>
					<td>測試結果</td>
					<td>PM</td>
					<td>備註</td>
				</tr>			
				<tr>
					<td>${count+1 }</td>
					<td>${formDetailField[count].groupLabel }</td>
					<td>${formDetailField[count].fieldLabel }</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
			
			<table class="table">
				<tr>						
					<td><label>主管簽核</label></td>
					<td><label>產品PM</label></td>
					<td><label>技術中心主管</label></td>
					<td><label>測試人員</label></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>		
				</tr>
			</table>
			</c:if>
			
			<input type="button" value="保存" id="save"/>
			<input type="button" value="送審" id="submit"/>
			</c:if>					
		</div>
	</form>	
</div>