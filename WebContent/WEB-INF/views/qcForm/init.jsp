<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
<head>
<title>8888play ERP</title>
</head>
<body>
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div class="modal-body" id="myModalBody"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="modalSubmit">關閉</button>
				</div>
			</div>
		</div>
	</div>
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				表單資料 <small>測試單</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="${ctx}/index"><i class="fa fa-dashboard"></i>8888play ERP</a></li>
				<li class="active">測試單</li>
			</ol>
		</section>
		<!-- Main content -->
		<div class="container-fluid">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body">
						<form action="${ctx}/qcForm/list" id="listForm" method="post">
							<input type="hidden" id="pageNo" name="pageNo" value="" />
							<input type="hidden" id="formId" name="formId" value="${formId}" />
							<div class="table-responsive">						
								<div class="form-group col-sm-12 col-md-4 col-lg-3">
	                            	<label for="inputid">公司:</label>
	                            	<select class="form-control" name="companyId" id="changeCompany">
	                            	<option value="">請選擇</option>
	                            	<c:forEach items="${companys }" var="cps">
	                            	<option value="${cps.id }">${cps.name }</option>
	                            	</c:forEach>
	                            	</select>	                                
	                            </div>
	                            <div class="form-group col-sm-12 col-md-4 col-lg-3">
	                            	<label for="inputid">表單編號:</label>
	                                <input class="form-control" id="inputserialNo" name="serialNo" type="text">
	                            </div>
	                            <div class="form-group col-sm-12 col-md-4 col-lg-3">
	                            	<label for="inputid">申請起日:</label>
	                                <input type="text" value=""
												onfocus="WdatePicker({requestDateStart:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
												class="Wdate" name="requestDateStart" id="requestDateStart">
	                            </div>
	                            <div class="form-group col-sm-12 col-md-4 col-lg-3">
	                            	<label for="inputid">申請迄日:</label>
	                                <input type="text" value=""
												onfocus="WdatePicker({requestDateEnd:'%y-%M-%d', dateFmt:'yyyy-MM-dd'})"
												class="Wdate" name="requestDateEnd" id="requestDateEnd">
	                            </div>
	                            <div class="form-group col-sm-12 col-md-4 col-lg-3">
	                            	<label for="inputid">流程狀態:</label>
	                                <select class="form-control" name="status">
	                                <option value="">全部</option>
	                                <option value="0">正常結束</option>
	                                <option value="1">執行中</option>
	                                <option value="8">註銷</option>
	                                <option value="9">退件</option>
	                                <option value="-1">草稿</option>
	                                </select>
	                            </div>                           
	                            <hr>
								<div class="btn-group col-sm-12 col-md-12 col-lg-6" role="group">
		                        	<input class="btn btn-primary" type="button" id="search" value="搜尋"/>
		                            <input class="btn btn-success" type="button" id="add" value="新增"/>
		                            <!--  
		                            <input class="btn btn-danger" type="button" id="export" value="導出列表"/>
		                            -->		                            
		                        </div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div id="listContainer"></div>
		</div>
	</aside>

	<script type="text/javascript" src="${ctx }/js/jquery.form.js"></script>
	<script type="text/javascript">
		$(function() {		
			$("#add").click(function() {
				$(".modal-dialog").css("width", "900px"), $(".modal-dialog").css(
						"maxheight", "800px"), $("#myModal").modal("toggle"), 
						$("#myModalLabel").html("編輯"), $("#myModalBody").html(
						"<h3>正在打開。。。</h3>"), $.get("/qcForm/edit?formId=", function(e) {
							$("#myModalBody").html(e);
						});
			});

			$('#search').click(function() {
				var flag = dayCheck();
				
				if(!flag) {
					return;
				}
				
				$('#pageNo').val("1");
				$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
				$('#listForm').ajaxSubmit({
					success : function(data) {
						$('#listContainer').html(data);
					}
				});
			});
			/*
			$('#export').click(function() {
				if(!dayCheck()) {
					return;
				}
				
				var temp = $('#listForm').attr('action');
				$('#listForm').attr('action', '${ctx}/qcForm/export');
				$('#listForm').submit();
				$('#listForm').attr('action', temp);
			});
			*/
			$('#modalSubmit').click(function() {
				$('#myModal').modal('toggle');
				$('#search').click();
			});						
			
			$('#myModalBody').on('click', '#save', function(){
				// validate
				if(!validate()) {
					return false;
				}
				
				$("#editForm").ajaxSubmit({
		            success: function(e) {
		                alert(e.message), $("#myModal").modal("toggle");
		                $('#search').click();
		            },
		            error: function() {
		                alert("網路連接失敗，請聯繫管理員");
		            }
		        });
			});
			
			$('#myModalBody').on('click', '#submit', function(){
				// validate
				if(!validate()) {
					return false;
				}
				
				$('#editForm').attr('action', '${ctx}/qcForm/submit');
				$("#editForm").ajaxSubmit({
			           success: function(e) {
			        	   if(e.code == "0000") {
			            		alert(e.message), $("#myModal").modal("toggle");
				                $('#search').click();
			            	}
			            	else {
			            		alert(e.message);
			            	}
			           },
			           error: function() {
			               alert("網路連接失敗，請聯繫管理員");
			           }
			    });
			});
			
			// Draft
			var formId = $("#formId").val();
			
			if(formId > 0) {
				edit(formId);
			}			
		});		
		
		function changeSchemaId(schemaId){
			if(schemaId == "") {
				$("#qcForm").empty();
				return;
			}
			
			$('#qcForm').html('<h1>正在搜尋，請稍候。。。</h1>');		
			var company = $("#companyId").val();
			console.log("company=" + company);
			
			$.get("/qcForm/edit?schemaId="+schemaId + "&company=" + company, function(e) {
				
				$("#qcForm").html(e);
			});
		}		
		
		function toPage(pageNo) {
			if(!dayCheck()) {
				return;
			}
			
			if (!pageNo || pageNo <= 0) {
				return;
			}
			
			$('#pageNo').val(pageNo);
			$('#listForm').ajaxSubmit({
				success : function(data) {
					$('#listContainer').html(data);
				},
				error : function() {
					alert('連接網路失敗');
				}
			});
			$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
		}
		
		function dayCheck() {
			var beginTime = $("#establishDateStart").val();
			
			if(!beginTime) {
				var beginTimeDate = new Date(beginTime);
				var endTime = $("#establishDateEnd").val();
				var endTimeDate = new Date();
				
				if(endTime) {
					endTimeDate = new Date(endTime);
				}
				
				if(beginTimeDate > endTimeDate) {
					alert("結束時間不能大於開始時間!");
					return false;
				}
			}
			return true;
		}		
		
		function edit(formId) {			
			$(".modal-dialog").css("width", "900px"), $(".modal-dialog").css(
				"maxheight", "800px"), $("#myModal").modal("toggle"), 
				$("#myModalLabel").html("編輯"), $("#myModalBody").html(
				"<h3>正在打開。。。</h3>"), $.get("/qcForm/edit?id=" + formId, function(e) {
					$("#myModalBody").html(e);
				});
			
			
		}
		
		function get(formId) {
			$(".modal-dialog").css("width", "900px"), $(".modal-dialog").css(
				"maxheight", "800px"), $("#myModal").modal("toggle"), 
				$("#myModalLabel").html("檢視"), $("#myModalBody").html(
				"<h3>正在打開。。。</h3>"), $.get("/qcForm/get?id=" + formId, function(e) {
					$("#myModalBody").html(e);
				});
		}
		
		function showForm(schemaId){
			$("#formContent").html("<h3>正在打開。。。</h3>"),			
			$.get("/qcForm/showForm?schemaId=" + schemaId, function(e) {
				$("#formContent").html(e);
			});
		}
		
		// 表單檢核
		function validate() {
			var html = "<font class='warning' color='red'>請輸入此欄位</font>";
			var pass = true;
			// TODO
			$("select[name='subTitle']").each(function() {
			  	if(this.value == "") {
			  		$(this).parent().find('.warning').remove();
					$(this).parent().append(html);
			  		pass = false;
			  	}
			  	else {
			  		$(this).parent().find('.warning').remove();
			  	}
			});
			
			$("input[name='subTitle']").each(function() {
				if(this.value == "") {
					$(this).parent().find('.warning').remove();
					$(this).parent().append(html);
			  		pass = false;
			  	}
				else {
					$(this).parent().find('.warning').remove();
				}
			});
			
			if(!pass){
				alert("表單未填寫完整!");
			}
			
			return pass;
		}
		
		function terminate(id) {
			if(!confirm("確定撤銷?")) {
				return;
			}
			
			$.ajax({
				url : '${ctx}/flow/terminate',
				dataType : 'json',
				data : {
					'id1' : id
				},
				success : function(data) {
					alert("撤銷成功");
					$('#search').click();
				},
				error : function() {
					alert('網路連接失敗，請聯繫管理員');
				}
			});
		}
		
		// 審核歷程
		function history(id) {
			$(".modal-dialog").css("width", "900px"), 
			$(".modal-dialog").css("maxheight", "800px"), 
			$("#myModal").modal("toggle"), 
			$("#myModalLabel").html("審核歷程"), 
			$("#myModalBody").html("<h3>正在打開。。。</h3>"), 
			$.get("/flow/list?id=" + id, 
				function(e) {
					$("#myModalBody").html(e);
				})
		}
		
		function getUserInfo(cid) {
			$.ajax({
				url : '${ctx}/employee/userInfo',
				dataType : 'json',
				data : {
					'id' : cid
				},
				success : function(data) {
					console.log(data);
					var dept = data.split(",");
					
					$("#editForm #requestDepartment").val(dept[0]);			
				},
				error : function() {
					alert('網絡連接失敗，請聯繫管理員');
				}
			});
		}
	</script>
</body>
</html>