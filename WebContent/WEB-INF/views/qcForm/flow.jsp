<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->	
	<input type="hidden" name="id1" id="id1" value="${param.id1}" />	
	<input type="hidden" name="id2" id="id2" value="${param.id2}" />
	<input type="hidden" name="formId" value="${formId}">		
	<div class="table-responsive">
		<table class="table">
			<tr>
				<td colspan="2">${companyName}</td>
				<td>編號</td>
				<td>${serialNo}</td>
			</tr>
			<c:if test="${fn:length(formTitleField) > 0 }">
			<tr>					
				<td rowspan="${fn:length(formTitleField) }">
					<div style="text-align: center;"><h2>${formSchema.formName }</h2></div>
				</td>
				<td><label>${formTitleField[0].fieldLabel }</label></td>
				<td>${formTitleField[0].subTitle }</td>					
			</tr>					
			<c:forEach items="${formTitleField }" begin="1" var="ftf" >
			<tr>
				<td><label>${ftf.fieldLabel }</label></td>
				<td>${ftf.subTitle }</td>
			</tr>					
			</c:forEach>
			</c:if>
				
			<c:if test="${fn:length(forTitleField) == 0 }">
			<tr>
				<td><div style="text-align: center;"><h2>${formScheam.formName }</h2></div></td>										
			</tr>
			</c:if>						
		</table>	
		<c:set var="pmProcess" scope="page" value="${sessionScope.formRole.contains('產品') && empty stamp['產品']}"/>
		<table class="table">
			<tr>
				<td colspan="3">測項</td>
				<td>測試結果</td>
				<td>PM</td>
				<td>備註</td>
			</tr>
			
			<c:set var="count" value="0"/>
			<c:set var="groupCount" value="${fn:length(fieldCount) }"/>
			<c:if test="${formSchema.flowType == 2 }">
				<c:set var="groupCount" value="${groupCount-1 }"/>					
			</c:if>
							
			<c:forEach begin="0" end="${groupCount-1 }" varStatus="loop">
			<tr>
				<td>${count+1 }</td>
				<td rowspan="${fieldCount[loop.index] }">${formDetailField[count].groupLabel }</td>
				<td>${formDetailField[count].fieldLabel }</td>
				<td>
					<c:if test="${formDetailField[count].qcResult == 0 }">N/A</c:if>
					<c:if test="${formDetailField[count].qcResult == 1 }">O</c:if>
					<c:if test="${formDetailField[count].qcResult == 2 }">X</c:if>										
				</td>
				<td>
					<c:if test="${pmProcess}">
					<select name="pmResult">
						<option value="0">N/A</option>
						<option value="1" <c:if test="${formDetailField[count].pmResult == 1}">selected="selected"</c:if>>O</option>
						<option value="2" <c:if test="${formDetailField[count].pmResult == 2}">selected="selected"</c:if>>X</option>
					</select>
					</c:if>
					<c:if test="${!pmProcess}">
					<c:if test="${formDetailField[count].pmResult == 0 }">N/A</c:if>
					<c:if test="${formDetailField[count].pmResult == 1 }">O</c:if>
					<c:if test="${formDetailField[count].pmResult == 2 }">X</c:if>
					</c:if>										
				</td>
				<td>
					<c:if test="${pmProcess}">
					<input name="remark" value="${formDetailField[count].remark }">
					</c:if>	
					<c:if test="${!pmProcess}">
					${formDetailField[count].remark }
					</c:if>	
				</td>
			</tr>
			
			<c:forEach items="${formDetailField }" begin="${count+1 }" end="${count+fieldCount[loop.index]-1 }" var="fdf" varStatus="loop2">
			
			<c:set var="count" value="${count+1 }"/>
			<tr>
				<td>${count+1 }</td>					
				<td>${fdf.fieldLabel }</td>
				<td>
					<c:if test="${fdf.qcResult == 0 }">N/A</c:if>
					<c:if test="${fdf.qcResult == 1 }">O</c:if>
					<c:if test="${fdf.qcResult == 2 }">X</c:if>										
				</td>
				<td>
					<c:if test="${pmProcess}">
					<select name="pmResult">
						<option value="0">N/A</option>
						<option value="1" <c:if test="${fdf.pmResult == 1}">selected="selected"</c:if>>O</option>
						<option value="2" <c:if test="${fdf.pmResult == 2}">selected="selected"</c:if>>X</option>
					</select>
					</c:if>
					<c:if test="${!pmProcess}">
					<c:if test="${fdf.pmResult == 0 }">N/A</c:if>
					<c:if test="${fdf.pmResult == 1 }">O</c:if>
					<c:if test="${fdf.pmResult == 2 }">X</c:if>
					</c:if>					
				</td>
				<td>
					<c:if test="${pmProcess}">
					<input name="remark" value="${fdf.remark }">
					</c:if>
					<c:if test="${!pmProcess}">
					${fdf.remark }
					</c:if>
				</td>
			</tr>
			</c:forEach>
			<c:set var="count" value="${count+1 }"/>
			</c:forEach>
		</table>
		<table class="table">
			<tr>
				<td><label>營運主管</label></td>
				<td><label>產品</label></td>
				<td><label>技術主管</label></td>
				<td><label>測試人員</label></td>
			</tr>
			<tr>
				<td>${stamp['營運主管'].info}</td>
				<td>${stamp['產品'].info}</td>
				<td>${stamp['技術主管'].info}</td>
				<td>${stamp['申請人'].info}</td>
			</tr>				
		</table>
		<c:set var="qc2ndProcess" scope="page" value="${sessionScope.formRole.contains('測試') && empty stamp['測試']}"/>
		<c:set var="pm2ndProcess" scope="page" value="${sessionScope.formRole.contains('產品') && not empty stamp['測試'] && empty stamp['產品2']}"/>
		<c:if test="${formSchema.flowType == 2}">
		<table class="table">
			<tr>
				<td colspan="3">測項</td>
				<td>測試結果</td>
				<td>PM</td>
				<td>備註</td>
			</tr>		
			<tr>
				<td>${count+1 }</td>
				<td>${formDetailField[count].groupLabel }</td>
				<td>${formDetailField[count].fieldLabel }</td>
				<td>
					<c:if test="${qc2ndProcess}">					
					<select name="qcResult">
						<option value="0">N/A</option>
						<option value="1" <c:if test="${formDetailField[count].qcResult == 1}">selected="selected"</c:if>>O</option>
						<option value="2" <c:if test="${formDetailField[count].qcResult == 2}">selected="selected"</c:if>>X</option>
					</select>
					</c:if>
					<c:if test="${!qc2ndProcess}">
					<c:if test="${formDetailField[count].qcResult == 0 }">N/A</c:if>
					<c:if test="${formDetailField[count].qcResult == 1 }">O</c:if>
					<c:if test="${formDetailField[count].qcResult == 2 }">X</c:if>
					</c:if>
				</td>
				<td>
					<c:if test="${pm2ndProcess}">
					<select name="pmResult">
						<option value="0">N/A</option>
						<option value="1" <c:if test="${formDetailField[count].pmResult == 1}">selected="selected"</c:if>>O</option>
						<option value="2" <c:if test="${formDetailField[count].pmResult == 2}">selected="selected"</c:if>>X</option>
					</select>
					</c:if>
					<c:if test="${!pm2ndProcess}">
					<c:if test="${formDetailField[count].pmResult == 0 }">N/A</c:if>
					<c:if test="${formDetailField[count].pmResult == 1 }">O</c:if>
					<c:if test="${formDetailField[count].pmResult == 2 }">X</c:if>
					</c:if>
				</td>
				<td>
					<c:if test="${qc2ndProcess || pm2ndProcess}">
					<input name="remark" value="${formDetailField[count].remark }">
					</c:if>
					<c:if test="${!(qc2ndProcess || pm2ndProcess)}">
					${formDetailField[count].remark }
					</c:if>					
					<input type="hidden" name="fieldId" value="${formDetailField[count].id}">
				</td>
			</tr>
		</table>
		
		<table class="table">
			<tr>						
				<td><label>營運主管</label></td>
				<td><label>產品</label></td>
				<td><label>技術主管</label></td>
				<td><label>測試人員</label></td>
			</tr>
			<tr>
				<td>${stamp['營運主管2'].info}</td>
				<td>${stamp['產品2'].info}</td>
				<td>${stamp['技術主管2'].info}</td>
				<td>${stamp['測試'].info}</td>	
			</tr>
		</table>
		</c:if>
	
		<!-- 退件原因 -->
		<table class="table">
			<tr>
				<td>
					退件原因：<input id="memo" name="memo" size="30"/>
					<input type="button" value="退件" id="reject"/>
				</td>
			</tr>
		</table>
		
		<!-- flow button -->
		<input type="button" value="取消" id="abort" />
		
		<c:if test="${isVerify}">
			<input type="button" value="審核" id="verify"/>
		</c:if>
		
		<c:if test="${isCountersign}">
		<input type="button" value="會簽" id="countersign"/>
		</c:if>
		
		<c:if test="${isPass}">
			<input type="button" value="放行" id="pass"/>
		</c:if>					
	</div>
</div>

<script type="text/javascript">
$(function(){
	$('#abort').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();	
			
		$.ajax({
			url : '${ctx}/qcForm/abort',
			dataType : 'json',
			data : {
				'id' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#verify').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		var qc2ndProcess = ${qc2ndProcess};
		var pm2ndProcess = ${pm2ndProcess};
		var pmProcess = ${pmProcess};
		
		var data = "";
		
		if(pmProcess) {
			data = {
				'id1' : id1,
				'id2' : id2,
	        	'pmResult': $('select[name=pmResult]').map(function() { return $(this).val().toString(); } ).get().join('|'),
	        	'remark': $('input[name=remark]').map(function() { return $(this).val().toString(); } ).get().join('|')
	        };
		}
		else if(qc2ndProcess) {
			data = {
				'id1' : id1,
				'id2' : id2,	
	            'qcResult': $('select[name=qcResult]').val(),
	            'remark': $('input[name=remark]').val(),
	            'fieldId': $('input[name=fieldId]').val()
	        };			
		}
		else if(pm2ndProcess) {
			data = {
				'id1' : id1,
				'id2' : id2,	
	            'pmResult': $('select[name=pmResult]').val(),
	            'remark': $('input[name=remark]').val(),
	            'fieldId': $('input[name=fieldId]').val()
	        };
		}
	    else {
	    	data = {
	    		'id1' : id1,
				'id2' : id2
	    	};
	    }
		
		console.log(JSON.stringify(data, null, 2));
		
		$.ajax({
			url : '${ctx}/qcForm/verify',
			dataType : 'json',
			data : data,
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});		
	});	
	
	$('#countersign').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		
		$.ajax({
			url : '${ctx}/qcForm/countersign',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#pass').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		
		$.ajax({
			url : '${ctx}/qcForm/pass',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
	$('#reject').click(function() {
		var id1 = $("#id1").val();		
		var id2 = $("#id2").val();
		var memo = $("#memo").val();
		
		$.ajax({
			url : '${ctx}/qcForm/reject',
			dataType : 'json',
			data : {
				'id1' : id1,
				'id2' : id2,
				'memo' : memo
			},
			success : function(data) {
				alert(data.message);
				$("#myModal").modal("toggle");
				refresh();
			},
			error : function() {
				alert('網絡連接失敗，請聯繫管理員');
			}
		});
	});	
	
});
</script>